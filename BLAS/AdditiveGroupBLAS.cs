﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.BLAS
{
    internal class AdditiveGroupBLAS<T> : IBLAS<T> where T : AdditiveGroup<T>, new()
    {
        internal override T One => throw new NotImplementedException();
        internal override T Zero => new T().AdditiveIdentity;

        internal override T Add(T a, T b) => a.Add(b);
        internal override T Divide(T a, T b) => throw new NotImplementedException();
        internal override T Multiply(T a, T b) => throw new NotImplementedException();
        internal override T Product(T[] x) => throw new NotImplementedException();
        internal override T LogProduct(T[] x) => throw new NotImplementedException();
        internal override T Subtract(T a, T b) => a.Subtract(b);
        internal override T Sqrt(T a) => throw new NotImplementedException();
        internal override T Negate(T a) => a.AdditiveInverse();
        internal override bool IsNegative(T a) => throw new NotImplementedException();

        internal override void AXPY(T[] x, T[] y, T alpha, int start, int end) => throw new NotImplementedException();
        internal override void AXPY(Dictionary<long, T> y, Dictionary<long, T> x, T alpha, long start, long end) => throw new NotImplementedException();
        internal override T DOT(T[] u, T[] v, int start, int end) => throw new NotImplementedException();
        internal override T DOT(Dictionary<long, T> u, Dictionary<long, T> v, long start, long end) => throw new NotImplementedException();
        internal override void SCAL(T[] x, T s, int start, int end) => throw new NotImplementedException();
        internal override void SCAL(Dictionary<long, T> u, T s, long start, long end) => throw new NotImplementedException();
        internal override void YSAX(T[] y, T[] x, T alpha, int start, int end) => throw new NotImplementedException();
        internal override void YSAX(Dictionary<long, T> y, Dictionary<long, T> x, T alpha, long start, long end) => throw new NotImplementedException();
        internal override T AMULT(T[] x, T[][] A, int column, int start, int end) => throw new NotImplementedException();

        internal override void HouseholderTransform(T[][] A, T[][] H, T[] v, T[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            throw new NotImplementedException();
        }
        internal override void HouseholderTransformParallel(T[][] A, T[][] H, T[] v, T[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            throw new NotImplementedException();
        }

    }
}
