﻿using LinearLite.Matrices;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.BLAS
{
    internal class ComplexBLAS : IBLAS<Complex>
    {
        internal static Action<Complex[], double, int, int> dSCAL = (x, s, start, end) =>
        {
            for (int i = start; i < end; ++i)
            {
                x[i].MultiplyEquals(s);
            }
        };

        internal override Complex One => Complex.One;
        internal override Complex Zero => Complex.Zero;

        internal override Complex Add(Complex u, Complex v) => u.Add(v);
        internal override Complex Divide(Complex u, Complex v) => u.Divide(v);
        internal override Complex Multiply(Complex u, Complex v) => u.Multiply(v);
        internal override Complex Product(Complex[] x)
        {
            double logMagnitude = 1;
            double argument = 0;

            foreach (Complex c in x)
            {
                double modulus = c.Modulus();
                if (modulus <= 0)
                {
                    return Complex.Zero;
                }
                logMagnitude += Math.Log(modulus);
                argument += c.Argument();
            }

            return Complex.FromPolar(Math.Exp(logMagnitude), argument % (2 * Math.PI));
        }
        internal override Complex LogProduct(Complex[] x)
        {
            double logMagnitude = 1;
            double argument = 0;

            foreach (Complex c in x)
            {
                double modulus = c.Modulus();
                if (modulus <= 0)
                {
                    return Complex.Zero;
                }
                logMagnitude += Math.Log(modulus);
                argument += c.Argument();
            }

            return Complex.FromPolar(Math.Exp(logMagnitude), argument % (2 * Math.PI));
        }
        internal override Complex Subtract(Complex u, Complex v) => u.Subtract(v);
        internal override Complex Sqrt(Complex a) => Complex.Sqrt(a);
        internal override Complex Negate(Complex a) => new Complex(-a.Real, -a.Imaginary);
        internal override bool IsNegative(Complex a) => a.Real < 0.0;

        internal override void AXPY(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            double re = alpha.Real, im = alpha.Imaginary;
            for (int i = start; i < end; ++i)
            {
                Complex c = x[i];
                y[i].Real += (c.Real * re - c.Imaginary * im);
                y[i].Imaginary += (c.Imaginary * re + c.Real * im);
            }
        }
        internal override void AXPY(Dictionary<long, Complex> y, Dictionary<long, Complex> x, Complex alpha, long start, long end)
        {
            double re = alpha.Real, im = alpha.Imaginary;
            foreach (KeyValuePair<long, Complex> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        Complex c = pair.Value, _y = y[index];
                        y[index] = new Complex(
                            _y.Real + c.Real * re - c.Imaginary * im,
                            _y.Imaginary + c.Imaginary * re + c.Real * im
                        );
                    }
                    else
                    {
                        Complex c = pair.Value;
                        y[index] = new Complex(
                            c.Real * re - c.Imaginary * im,
                            c.Imaginary * re + c.Real * im
                        );
                    }
                }
            }
        }
        internal override Complex DOT(Complex[] u, Complex[] v, int start, int end)
        {
            double re = 0.0, im = 0.0;
            for (int i = start; i < end; ++i)
            {
                Complex a = u[i], b = v[i];
                re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                im += a.Imaginary * b.Real + a.Real * b.Imaginary;
            }
            return new Complex(re, im);
        }
        internal override Complex DOT(Dictionary<long, Complex> u, Dictionary<long, Complex> v, long start, long end)
        {
            double re = 0.0, im = 0.0;
            foreach (KeyValuePair<long, Complex> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    Complex a = pair.Value, b = v[index];
                    re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                    im += a.Imaginary * b.Real + a.Real * b.Imaginary;
                }
            }
            return new Complex(re, im);
        }
        internal override void SCAL(Complex[] x, Complex s, int start, int end)
        {
            double re = s.Real, im = s.Imaginary;
            for (int i = start; i < end; ++i)
            {
                Complex c = x[i];
                double u_re = c.Real, u_im = c.Imaginary;
                x[i].Real = u_re * re - u_im * im;
                x[i].Imaginary = u_im * re + u_re * im;
            }
        }
        internal override void SCAL(Dictionary<long, Complex> u, Complex s, long start, long end)
        {
            double re = s.Real, im = s.Imaginary;
            foreach (KeyValuePair<long, Complex> pair in u)
            {
                double _re = pair.Value.Real, _im = pair.Value.Imaginary;
                u[pair.Key] = new Complex(_re * re - _im * im, _re * im + re * _im);
            }
        }
        internal override void YSAX(Complex[] y, Complex[] x, Complex alpha, int start, int end)
        {
            double re = alpha.Real, im = alpha.Imaginary;
            for (int i = start; i < end; ++i)
            {
                Complex c = x[i];
                y[i].Real -= (c.Real * re - c.Imaginary * im);
                y[i].Imaginary -= (c.Imaginary * re + c.Real * im);
            }
        }
        internal override void YSAX(Dictionary<long, Complex> y, Dictionary<long, Complex> x, Complex alpha, long start, long end)
        {
            double re = alpha.Real, im = alpha.Imaginary;
            foreach (KeyValuePair<long, Complex> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        Complex c = pair.Value, _y = y[index];
                        y[index] = new Complex(
                            _y.Real - c.Real * re + c.Imaginary * im,
                            _y.Imaginary - c.Imaginary * re - c.Real * im
                        );
                    }
                    else
                    {
                        Complex c = pair.Value;
                        y[index] = new Complex(
                            -c.Real * re + c.Imaginary * im,
                            -c.Imaginary * re - c.Real * im
                        );
                    }
                }
            }
        }
        internal override Complex AMULT(Complex[] x, Complex[][] A, int column, int start, int end)
        {
            double re = 0.0, im = 0.0;
            for (int i = start; i < end; ++i)
            {
                Complex a = x[i], b = A[i][column];
                re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                im += a.Imaginary * b.Real + a.Real * b.Imaginary;
            }
            return new Complex(re, im);
        }

        internal override void HouseholderTransform(Complex[][] A, Complex[][] H, Complex[] v, Complex[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Householder.Transform(A, H, v, w, columnIndex, columns, rowIndex, rows, isColumn);
        }
        internal override void HouseholderTransformParallel(Complex[][] A, Complex[][] H, Complex[] v, Complex[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Householder.TransformParallel(A, H, v, w, columnIndex, columns, rowIndex, rows, isColumn);
        }

    }
}
