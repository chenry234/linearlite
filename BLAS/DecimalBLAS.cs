﻿using LinearLite.Global;
using LinearLite.Helpers;
using LinearLite.Matrices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.BLAS
{
    internal class DecimalBLAS : IBLAS<decimal>
    {
        internal override decimal One => 1.0M;
        internal override decimal Zero => 0.0M;

        internal override decimal Add(decimal a, decimal b) => a + b;
        internal override decimal Divide(decimal a, decimal b) => a / b;
        internal override decimal Multiply(decimal a, decimal b) => a * b;
        internal override decimal Product(decimal[] x)
        {
            int sign = 1;
            decimal logProduct = 0.0M;
            foreach (decimal d in x)
            {
                if (d == 0.0M)
                {
                    return 0.0M;
                }

                logProduct += DecimalMath.Log(Math.Abs(d));
                if (d < 0.0M)
                {
                    sign = -sign;
                }
            }
            return sign * DecimalMath.Exp(logProduct);
        }
        internal override decimal LogProduct(decimal[] x)
        {
            decimal logProduct = 0.0M;
            foreach (decimal d in x)
            {
                if (d == 0.0M)
                {
                    return decimal.MinValue;
                }

                logProduct += DecimalMath.Log(Math.Abs(d));
            }
            return logProduct;
        }
        internal override decimal Subtract(decimal a, decimal b) => a - b;
        internal override decimal Sqrt(decimal a) => DecimalMath.Sqrt(a);
        internal override decimal Negate(decimal a) => -a;
        internal override bool IsNegative(decimal a) => a < 0.0m;

        internal override void AXPY(decimal[] y, decimal[] x, decimal alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        internal override void AXPY(Dictionary<long, decimal> y, Dictionary<long, decimal> x, decimal alpha, long start, long end)
        {
            foreach (KeyValuePair<long, decimal> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] += alpha * pair.Value;
                    }
                    else
                    {
                        y[index] = alpha * pair.Value;
                    }
                }
            }
        }
        internal override decimal DOT(decimal[] u, decimal[] v, int start, int end)
        {
            decimal sum = 0.0m;
            for (int i = start; i < end; ++i)
            {
                sum += u[i] * v[i];
            }
            return sum;
        }
        internal override decimal DOT(Dictionary<long, decimal> u, Dictionary<long, decimal> v, long start, long end)
        {
            decimal sum = 0.0m;
            foreach (KeyValuePair<long, decimal> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum += pair.Value * v[index];
                }
            }
            return sum;
        }
        internal override void SCAL(decimal[] x, decimal s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] *= s;
            }
        }
        internal override void SCAL(Dictionary<long, decimal> u, decimal s, long start, long end)
        {
            foreach (long key in u.Keys)
            {
                u[key] *= s;
            }
        }
        internal override void YSAX(decimal[] y, decimal[] x, decimal alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] -= alpha * x[i];
            }
        }
        internal override void YSAX(Dictionary<long, decimal> y, Dictionary<long, decimal> x, decimal alpha, long start, long end) => AXPY(y, x, -alpha, start, end);
        internal override decimal AMULT(decimal[] x, decimal[][] A, int column, int start, int end)
        {
            decimal prod = 0.0m;
            for (int i = start; i < end; ++i) prod += x[i] * A[i][column];
            return prod;
        }

        internal override void HouseholderTransform(decimal[][] A, decimal[][] H, decimal[] v, decimal[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Householder.Transform(A, H, v, w, columnIndex, columns, rowIndex, rows, isColumn);
        }
        internal override void HouseholderTransformParallel(decimal[][] A, decimal[][] H, decimal[] v, decimal[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Householder.TransformParallel(A, H, v, w, columnIndex, columns, rowIndex, rows, isColumn);
        }

    }
}
