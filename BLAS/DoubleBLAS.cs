﻿using LinearLite.Matrices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.BLAS
{
    internal class DoubleBLAS : IBLAS<double>
    {
        internal override double One => 1.0;
        internal override double Zero => 0.0;

        internal override double Add(double a, double b) => a + b;
        internal override double Divide(double a, double b) => a / b;
        internal override double Multiply(double a, double b) => a * b;
        internal override double Product(double[] x)
        {
            int sign = 1;
            double logProduct = 0.0;
            foreach (double d in x)
            {
                if (d == 0.0)
                {
                    return 0.0;
                }

                logProduct += Math.Log(Math.Abs(d));
                if (d < 0.0)
                {
                    sign = -sign;
                }
            }
            return sign * Math.Exp(logProduct);
        }
        internal override double LogProduct(double[] x)
        {
            double logProduct = 0.0;
            foreach (double d in x)
            {
                if (d == 0.0)
                {
                    return double.NegativeInfinity;
                }
                logProduct += Math.Log(Math.Abs(d));
            }
            return logProduct;
        }
        internal override double Subtract(double a, double b) => a - b;
        internal override double Sqrt(double a) => Math.Sqrt(a);
        internal override double Negate(double a) => -a;
        internal override bool IsNegative(double a) => a < 0.0;

        internal override void AXPY(double[] y, double[] x, double alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        internal override void AXPY(Dictionary<long, double> y, Dictionary<long, double> x, double alpha, long start, long end)
        {
            foreach (KeyValuePair<long, double> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] += alpha * pair.Value;
                    }
                    else
                    {
                        y[index] = alpha * pair.Value;
                    }
                }
            }
        }
        internal override double DOT(double[] u, double[] v, int start, int end)
        {
            double sum = 0.0;
            for (int i = start; i < end; ++i)
            {
                sum += u[i] * v[i];
            }
            return sum;
        }
        internal override double DOT(Dictionary<long, double> u, Dictionary<long, double> v, long start, long end)
        {
            double sum = 0.0;
            foreach (KeyValuePair<long, double> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum += pair.Value * v[index];
                }
            }
            return sum;
        }
        internal override void SCAL(double[] x, double s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] *= s;
            }
        }
        internal override void SCAL(Dictionary<long, double> u, double s, long start, long end)
        {
            foreach (long key in u.Keys)
            {
                u[key] *= s;
            }
        }
        internal override void YSAX(double[] y, double[] x, double alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] -= alpha * x[i];
            }
        }
        internal override void YSAX(Dictionary<long, double> y, Dictionary<long, double> x, double alpha, long start, long end) => AXPY(y, x, -alpha, start, end);
        internal override double AMULT(double[] x, double[][] A, int column, int start, int end)
        {
            double prod = 0.0;
            for (int i = start; i < end; ++i) prod += x[i] * A[i][column];
            return prod;
        }

        internal override void HouseholderTransform(double[][] A, double[][] H, double[] v, double[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Householder.Transform(A, H, v, w, columnIndex, columns, rowIndex, rows, isColumn);
        }
        internal override void HouseholderTransformParallel(double[][] A, double[][] H, double[] v, double[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Householder.TransformParallel(A, H, v, w, columnIndex, columns, rowIndex, rows, isColumn);
        }

    }
}
