﻿using LinearLite.Matrices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.BLAS
{
    /// <summary>
    /// This class is not a full implementation of BLAS. Only a few subroutines are 
    /// implemented and are designed to be used internally for other methods. This library
    /// has full support of the BLAS collection of algorithms via the vector/matrix namespaces. 
    /// </summary>
    internal class FloatBLAS : IBLAS<float>
    {
        internal static Action<float[], double, int, int> dSCAL = (x, s, start, end) =>
        {
            float f = (float)s;
            for (int i = start; i < end; ++i)
            {
                x[i] *= f;
            }
        };
        
        internal override float One => 1.0f;
        internal override float Zero => 0.0f;

        internal override float Add(float a, float b) => a + b;
        internal override float Divide(float a, float b) => a / b;
        internal override float Multiply(float a, float b) => a * b;
        internal override float Product(float[] x)
        {
            int sign = 1;
            double logProduct = 0.0;
            foreach (float d in x)
            {
                if (d == 0.0f)
                {
                    return 0.0f;
                }

                logProduct += Math.Log(Math.Abs(d));
                if (d < 0.0)
                {
                    sign = -sign;
                }
            }
            return sign * (float)Math.Exp(logProduct);
        }
        internal override float LogProduct(float[] x)
        {
            float logProduct = 0.0f;
            foreach (float d in x)
            {
                if (d == 0.0f)
                {
                    return float.NegativeInfinity;
                }
                logProduct += (float)Math.Log(Math.Abs(d));
            }
            return logProduct;
        }
        internal override float Subtract(float a, float b) => a - b;
        internal override float Sqrt(float a) => (float)Math.Sqrt(a);
        internal override float Negate(float a) => -a;
        internal override bool IsNegative(float a) => a < 0.0f;

        internal override void AXPY(float[] x, float[] y, float alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        internal override void AXPY(Dictionary<long, float> y, Dictionary<long, float> x, float alpha, long start, long end)
        {
            foreach (KeyValuePair<long, float> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] += alpha * pair.Value;
                    }
                    else
                    {
                        y[index] = alpha * pair.Value;
                    }
                }
            }
        }
        internal override float DOT(float[] u, float[] v, int start, int end)
        {
            float sum = 0.0f;
            for (int i = start; i < end; ++i)
            {
                sum += u[i] * v[i];
            }
            return sum;
        }
        internal override float DOT(Dictionary<long, float> u, Dictionary<long, float> v, long start, long end)
        {
            float sum = 0.0f;
            foreach (KeyValuePair<long, float> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum += pair.Value * v[index];
                }
            }
            return sum;
        }
        internal override void SCAL(float[] x, float s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] *= s;
            }
        }
        internal override void SCAL(Dictionary<long, float> u, float s, long start, long end)
        {
            foreach (long key in u.Keys)
            {
                u[key] *= s;
            }
        }
        internal override void YSAX(float[] y, float[] x, float alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] -= alpha * x[i];
            }
        }
        internal override void YSAX(Dictionary<long, float> y, Dictionary<long, float> x, float alpha, long start, long end) => AXPY(y, x, -alpha, start, end);
        internal override float AMULT(float[] x, float[][] A, int column, int start, int end)
        {
            float prod = 0.0f;
            for (int i = start; i < end; ++i) prod += x[i] * A[i][column];
            return prod;
        }

        internal override void HouseholderTransform(float[][] A, float[][] H, float[] v, float[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Householder.Transform(A, H, v, w, columnIndex, columns, rowIndex, rows, isColumn);
        }
        internal override void HouseholderTransformParallel(float[][] A, float[][] H, float[] v, float[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            Householder.TransformParallel(A, H, v, w, columnIndex, columns, rowIndex, rows, isColumn);
        }


    }
}
