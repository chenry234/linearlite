﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.BLAS
{
    internal abstract class IBLAS<T>
    {
        internal abstract T One { get; }
        internal abstract T Zero { get; }

        /// <summary>
        /// AXPY: y <- y + alpha * x
        /// Add scalar multiple of vector x to vector y, storing the result in y.
        /// </summary>
        internal abstract void AXPY(T[] y, T[] x, T alpha, int start, int end);
        internal abstract void AXPY(Dictionary<long, T> y, Dictionary<long, T> x, T alpha, long start, long end);

        /// <summary>
        /// YSAX: y <- y - alpha * x
        /// Subtract scalar multiple of vector x from vector y, storing the result in y.
        /// </summary>
        internal abstract void YSAX(T[] y, T[] x, T alpha, int start, int end);
        internal abstract void YSAX(Dictionary<long, T> y, Dictionary<long, T> x, T alpha, long start, long end);

        /// <summary>
        /// DOT: dot <- u.v, returning dot
        /// Calculate and return the dot product of vectors u and v. u and v unchanged.
        /// </summary>
        internal abstract T DOT(T[] u, T[] v, int start, int end);
        internal abstract T DOT(Dictionary<long, T> u, Dictionary<long, T> v, long start, long end);

        /// <summary>
        /// SCAL: x <- x * s
        /// Scale the vector x by a scalar s, rewriting x
        /// </summary>
        internal abstract void SCAL(T[] x, T s, int start, int end);
        internal abstract void SCAL(Dictionary<long, T> u, T s, long start, long end);

        /// <summary>
        /// AMULT: amult <- x^T . A[column]
        /// Calculates a vector multiplied by a column of a matrix
        /// </summary>
        internal abstract T AMULT(T[] x, T[][] A, int column, int start, int end);

        internal abstract T Add(T a, T b);
        internal abstract T Subtract(T a, T b);
        internal abstract T Multiply(T a, T b);
        /// <summary>
        /// Numerically stable multiplication of a large number of elements
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        internal abstract T Product(T[] x);
        internal abstract T LogProduct(T[] x);
        internal abstract T Divide(T a, T b);
        internal abstract T Sqrt(T a);
        internal abstract T Negate(T a);
        internal abstract bool IsNegative(T a);

        internal abstract void HouseholderTransform(T[][] A, T[][] H, T[] v, T[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn);
        internal abstract void HouseholderTransformParallel(T[][] A, T[][] H, T[] v, T[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn);
    }
}
