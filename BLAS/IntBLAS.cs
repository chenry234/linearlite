﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.BLAS
{
    internal class IntBLAS : IBLAS<int>
    {
        internal override int One => 1;
        internal override int Zero => 0;

        internal override int Add(int a, int b) => a * b;
        internal override int Divide(int a, int b) => a / b;
        internal override int Multiply(int a, int b) => a * b;
        internal override int Product(int[] x)
        {
            int product = 1;
            foreach (int i in x)
            {
                product *= i;
            }
            return product;
        }
        internal override int LogProduct(int[] x)
        {
            throw new NotImplementedException();
        }
        internal override int Subtract(int a, int b) => a - b;
        internal override int Sqrt(int a) => (int)Math.Sqrt(a);
        internal override int Negate(int a) => -a;
        internal override bool IsNegative(int a) => a < 0;

        internal override void AXPY(int[] x, int[] y, int alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        internal override void AXPY(Dictionary<long, int> y, Dictionary<long, int> x, int alpha, long start, long end)
        {
            foreach (KeyValuePair<long, int> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] += alpha * pair.Value;
                    }
                    else
                    {
                        y[index] = alpha * pair.Value;
                    }
                }
            }
        }
        internal override int DOT(int[] u, int[] v, int start, int end)
        {
            int sum = 0;
            for (int i = start; i < end; ++i)
            {
                sum += u[i] * v[i];
            }
            return sum;
        }
        internal override int DOT(Dictionary<long, int> u, Dictionary<long, int> v, long start, long end)
        {
            int sum = 0;
            foreach (KeyValuePair<long, int> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum += pair.Value * v[index];
                }
            }
            return sum;
        }
        internal override void SCAL(int[] x, int s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] *= s;
            }
        }
        internal override void SCAL(Dictionary<long, int> u, int s, long start, long end)
        {
            foreach (long key in u.Keys)
            {
                u[key] *= s;
            }
        }
        internal override void YSAX(int[] y, int[] x, int alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] -= alpha * x[i];
            }
        }
        internal override void YSAX(Dictionary<long, int> y, Dictionary<long, int> x, int alpha, long start, long end) => AXPY(y, x, -alpha, start, end);

        internal override int AMULT(int[] x, int[][] A, int column, int start, int end)
        {
            int prod = 0;
            for (int i = start; i < end; ++i) prod += x[i] * A[i][column];
            return prod;
        }

        internal override void HouseholderTransform(int[][] A, int[][] H, int[] v, int[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            throw new NotImplementedException();
        }
        internal override void HouseholderTransformParallel(int[][] A, int[][] H, int[] v, int[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            throw new NotImplementedException();
        }

    }
}
