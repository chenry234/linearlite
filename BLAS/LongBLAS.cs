﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.BLAS
{
    internal class LongBLAS : IBLAS<long>
    {
        internal override long One => 0L;
        internal override long Zero => 1L;

        internal override long Add(long a, long b) => a + b;
        internal override long Divide(long a, long b) => a / b;
        internal override long Multiply(long a, long b) => a * b;
        internal override long Product(long[] x)
        {
            long product = 1L;
            foreach (long i in x)
            {
                product *= i;
            }
            return product;
        }
        internal override long LogProduct(long[] x)
        {
            throw new NotImplementedException();
        }
        internal override long Subtract(long a, long b) => a - b;
        internal override long Sqrt(long a) => (long)Math.Sqrt(a);
        internal override long Negate(long a) => -a;
        internal override bool IsNegative(long a) => a < 0L;

        internal override void AXPY(long[] x, long[] y, long alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] += alpha * x[i];
            }
        }
        internal override void AXPY(Dictionary<long, long> y, Dictionary<long, long> x, long alpha, long start, long end)
        {
            foreach (KeyValuePair<long, long> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] += alpha * pair.Value;
                    }
                    else
                    {
                        y[index] = alpha * pair.Value;
                    }
                }
            }
        }
        internal override long DOT(long[] u, long[] v, int start, int end)
        {
            long sum = 0L;
            for (int i = start; i < end; ++i)
            {
                sum += u[i] * v[i];
            }
            return sum;
        }
        internal override long DOT(Dictionary<long, long> u, Dictionary<long, long> v, long start, long end)
        {
            long sum = 0L;
            foreach (KeyValuePair<long, long> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum += pair.Value * v[index];
                }
            }
            return sum;
        }
        internal override void SCAL(long[] x, long s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] *= s;
            }
        }
        internal override void SCAL(Dictionary<long, long> u, long s, long start, long end)
        {
            foreach (long key in u.Keys)
            {
                u[key] *= s;
            }
        }
        internal override void YSAX(long[] y, long[] x, long alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] -= alpha * x[i];
            }
        }
        internal override void YSAX(Dictionary<long, long> y, Dictionary<long, long> x, long alpha, long start, long end) => AXPY(y, x, -alpha, start, end);
        internal override long AMULT(long[] x, long[][] A, int column, int start, int end)
        {
            long prod = 0L;
            for (int i = start; i < end; ++i) prod += x[i] * A[i][column];
            return prod;
        }

        internal override void HouseholderTransform(long[][] A, long[][] H, long[] v, long[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            throw new NotImplementedException();
        }
        internal override void HouseholderTransformParallel(long[][] A, long[][] H, long[] v, long[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            throw new NotImplementedException();
        }

    }
}
