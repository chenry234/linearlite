﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.BLAS
{
    internal class RingBLAS<T> : IBLAS<T> where T : Ring<T>, new()
    {
        internal override T One => new T().MultiplicativeIdentity;
        internal override T Zero => new T().AdditiveIdentity;

        internal override T Add(T a, T b) => a.Add(b);
        internal override T Divide(T a, T b) => throw new NotImplementedException();
        internal override T Multiply(T a, T b) => a.Multiply(b);
        internal override T Product(T[] x)
        {
            T product = One;
            foreach (T e in x)
            {
                product = product.Multiply(e);
            }
            return product;
        }
        internal override T LogProduct(T[] x)
        {
            throw new NotImplementedException();
        }
        internal override T Subtract(T a, T b) => a.Subtract(b);
        internal override T Sqrt(T a) => throw new NotImplementedException();
        internal override T Negate(T a) => a.AdditiveInverse();
        internal override bool IsNegative(T a) => throw new NotImplementedException();

        internal override void AXPY(T[] x, T[] y, T alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] = y[i].Add(alpha.Multiply(x[i]));
            }
        }
        internal override void AXPY(Dictionary<long, T> y, Dictionary<long, T> x, T alpha, long start, long end)
        {
            foreach (KeyValuePair<long, T> pair in x)
            {
                long index = pair.Key;
                if (index >= start && index < end)
                {
                    if (y.ContainsKey(index))
                    {
                        y[index] = y[index].Add(alpha.Multiply(pair.Value));
                    }
                    else
                    {
                        y[index] = alpha.Multiply(pair.Value);
                    }
                }
            }
        }
        internal override T DOT(T[] u, T[] v, int start, int end)
        {
            T sum = Zero;
            for (int i = start; i < end; ++i)
            {
                sum = sum.Add(u[i].Multiply(v[i]));
            }
            return sum;
        }
        internal override T DOT(Dictionary<long, T> u, Dictionary<long, T> v, long start, long end)
        {
            T sum = new T().AdditiveIdentity;
            foreach (KeyValuePair<long, T> pair in u)
            {
                long index = pair.Key;
                if (index >= start && index < end && v.ContainsKey(index))
                {
                    sum = sum.Add(pair.Value.Multiply(v[index]));
                }
            }
            return sum;
        }
        internal override void SCAL(T[] x, T s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                x[i] = x[i].Multiply(s);
            }
        }
        internal override void SCAL(Dictionary<long, T> u, T s, long start, long end)
        {
            foreach (long key in u.Keys)
            {
                u[key] = u[key].Multiply(s);
            }
        }
        internal override void YSAX(T[] y, T[] x, T alpha, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                y[i] = y[i].Subtract(alpha.Multiply(x[i]));
            }
        }
        internal override void YSAX(Dictionary<long, T> y, Dictionary<long, T> x, T alpha, long start, long end) => AXPY(y, x, alpha.AdditiveInverse(), start, end);
        internal override T AMULT(T[] x, T[][] A, int column, int start, int end)
        {
            T prod = Zero;
            for (int i = start; i < end; ++i) prod = prod.Add(x[i].Multiply(A[i][column]));
            return prod;
        }

        internal override void HouseholderTransform(T[][] A, T[][] H, T[] v, T[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            throw new NotImplementedException();
        }
        internal override void HouseholderTransformParallel(T[][] A, T[][] H, T[] v, T[] w, int columnIndex, int columns, int rowIndex, int rows, bool isColumn)
        {
            throw new NotImplementedException();
        }

    }
}
