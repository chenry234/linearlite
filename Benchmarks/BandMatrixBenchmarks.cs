﻿using LinearLite.Matrices;
using LinearLite.Matrices.Band;
using LinearLite.Structs;
using LinearLite.Structs.Matrix;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Benchmarks
{
    public static class BandMatrixBenchmarks
    {
        public static void Multiplication()
        {
            int[] sizes = { 256, 512, 1024, 2048, 4096, 8192, 16384, 32768 };
            double bandwidth = 0.1;
            Stopwatch sw = new Stopwatch();

            foreach (int n in sizes)
            {
                int width = (int)(bandwidth * n);
                BandMatrix<double> A = BandMatrix.Random<double>(n, n, width, width), B = BandMatrix.Random<double>(n, n, width, width);

                double btime = 0.0, dtime = 0.0;

                sw.Restart();
                A.Multiply(B);
                sw.Stop();
                btime = sw.ElapsedMilliseconds;

                DenseMatrix<double> _A = A.ToDense();
                A = null;
                DenseMatrix<double> _B = B.ToDense();
                B = null;

                sw.Restart();
                _A.Multiply(_B);
                sw.Stop();
                dtime = sw.ElapsedMilliseconds;

                Debug.WriteLine(n + "\t" + btime + "\t" + dtime);
            }
        }
        public static void ParallelMultiplication()
        {
            int[] sizes = { 256, 512, 1024, 2048, 4096, 8192, 16384, 32768 };
            double bandwidth = 0.1;
            Stopwatch sw = new Stopwatch();

            foreach (int n in sizes)
            {
                int width = (int)(bandwidth * n);
                BandMatrix<double> A = BandMatrix.Random<double>(n, n, width, width), B = BandMatrix.Random<double>(n, n, width, width);

                double time = 0.0, ptime = 0.0;

                sw.Restart();
                BandMatrix<double> AB = A.Multiply(B);
                sw.Stop();
                time = sw.ElapsedMilliseconds;

                sw.Restart();
                BandMatrix<double> _AB = A.MultiplyParallel(B);
                sw.Stop();
                ptime = sw.ElapsedMilliseconds;

                Debug.WriteLine(n + "\t" + time + "\t" + ptime);
                Debug.Assert(AB.ApproximatelyEquals(AB));
            }
        }
    }
}
