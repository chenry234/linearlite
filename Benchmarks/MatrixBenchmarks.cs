﻿using LinearLite.Decomposition;
using LinearLite.Global;
using LinearLite.Matrices;
using LinearLite.Matrices.Decompositions;
using LinearLite.Matrices.Strassen;
using LinearLite.Solvers;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace LinearLite.Benchmarks
{
    public class MatrixBenchmarks
    {
        private static void Test(Action<int> TestCase, params int[] sizes)
        {
            Stopwatch sw = new Stopwatch();
            foreach (int n in sizes)
            {
                sw.Restart();
                TestCase(n);
                sw.Stop();
                Debug.WriteLine(n + "\t" + sw.ElapsedMilliseconds + "\tms");
            }
        }
        public static void MatrixAdditionTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes = { 256, 1024, 4096 };
            List<Type> types = new List<Type>() { typeof(float), typeof(double), typeof(decimal), typeof(Complex) };

            foreach (int n in sizes)
            {
                double ftime = 0, dtime = 0, mtime = 0, ctime = 0;
                if (types.Contains(typeof(float)))
                {

                    float[,] A = RectangularMatrix.Random<float>(n, n), B = RectangularMatrix.Random<float>(n, n);
                    sw.Restart();
                    float[,] C1 = A.Add(B);
                    sw.Stop();
                    ftime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(double)))
                {
                    double[,] A = RectangularMatrix.Random<double>(n, n), B = RectangularMatrix.Random<double>(n, n);
                    sw.Restart();
                    double[,] C1 = A.Add(B);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(decimal)))
                {
                    decimal[,] A = RectangularMatrix.Random<decimal>(n, n), B = RectangularMatrix.Random<decimal>(n, n);
                    sw.Restart();
                    decimal[,] C1 = A.Add(B);
                    sw.Stop();
                    mtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(Complex)))
                {
                    Complex[,] A = RectangularMatrix.Random<Complex>(n, n), B = RectangularMatrix.Random<Complex>(n, n);
                    sw.Restart();
                    Complex[,] C1 = A.Add(B);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;
                }
                Debug.WriteLine(n + "\t" + ftime + "\t" + dtime + "\t" + mtime + "\t" + ctime);
            }
        }
        public static void MatrixTranspositionTest()
        {
            int[] sizes = { 1024, 2048, 4096, 8192 };
            Stopwatch sw = new Stopwatch();
            foreach (int n in sizes)
            {
                DenseMatrix<double> B = DenseMatrix.Random<double>(n, n);
                DenseMatrix<double> Bt = new DenseMatrix<double>(n, n);
                sw.Restart();
                //Bt.Values.CopyTransposeFrom(B.Values, 0, 0, n, n);
                Bt.Values.CopyTransposeFrom(B.Values, 0, 0, n, n, 32);
                sw.Stop();

                Debug.WriteLine(n + "\t" + sw.ElapsedMilliseconds);
            }
        }
        public static void MatrixMultiplicationTest()
        {
            Stopwatch sw = new Stopwatch();

            int[] sizes = { 256, 1024, 4096 };
            List<Type> types = new List<Type>() { typeof(float), typeof(double), typeof(Complex) };

            foreach (int n in sizes)
            {
                double ftime = 0, dtime = 0, ctime = 0;
                if (types.Contains(typeof(float)))
                {
                    float[,] A = RectangularMatrix.Random<float>(n, n), B = RectangularMatrix.Random<float>(n, n);
                    sw.Restart();
                    float[,] C1 = A.Multiply(B);
                    sw.Stop();
                    ftime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(double)))
                {
                    double[,] A = RectangularMatrix.Random<double>(n, n), B = RectangularMatrix.Random<double>(n, n);
                    sw.Restart();
                    double[,] C1 = A.Multiply(B);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(Complex)))
                {
                    Complex[,] A = RectangularMatrix.Random<Complex>(n, n), B = RectangularMatrix.Random<Complex>(n, n);
                    sw.Restart();
                    Complex[,] C1 = A.Multiply(B);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;
                }
                Debug.WriteLine(n + "\t" + ftime + "\t" + dtime + "\t" + ctime);
            }
        }
        public static void MatrixParallelMultiplicationTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes = { 256, 1024, 4096 };
            List<Type> types = new List<Type>() { typeof(double), typeof(Complex) };

            double dtime = 0, ctime = 0;
            foreach (int n in sizes)
            {
                if (types.Contains(typeof(double)))
                {
                    double[,] A = RectangularMatrix.Random<double>(n, n), B = RectangularMatrix.Random<double>(n, n);
                    sw.Start();
                    A.ParallelMultiply(B);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(Complex)))
                {
                    Complex[,] A = RectangularMatrix.Random<Complex>(n, n), B = RectangularMatrix.Random<Complex>(n, n);
                    sw.Start();
                    A.ParallelMultiply(B);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;
                }

                Debug.WriteLine(n + "\t" + dtime + "\t" + ctime);
            }
        }
        public static void JaggedMatrixBlockMultiplicationTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes = { 256, 512, 1024, 2048, 4096 };
            List<Type> types = new List<Type>()
            {
                typeof(double),
                //typeof(Complex)
            };

            double d_block = 0, d = 0, c_block = 0, c = 0;
            foreach (int n in sizes)
            {
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> A = DenseMatrix.Random<double>(n, n), 
                                        B = DenseMatrix.Random<double>(n, n),
                                        Bt = new DenseMatrix<double>(n, n),
                                        C1 = new DenseMatrix<double>(n, n),
                                        C2 = new DenseMatrix<double>(n, n);
                    sw.Restart();
                    //A.Values.multiply_unsafe(B.Values, C1.Values, 0, 0, 0, 0, 0, 0, n, n, n, false);
                    //A.Values.multiply_unsafe(B.Values, Bt.Values, C1.Values, 0, 0, 0, 0, 0, 0, n, n, n, false);
                    A.Values.multiply_unsafe_ptr(B.Values, C1.Values);
                    //A.Values.multiply_block_unsafe(B.Values, C1.Values);
                    sw.Stop();
                    d_block = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.Values.multiply_unsafe(B.Values, C2.Values);
                    sw.Stop();
                    d = sw.ElapsedMilliseconds;

                    if (!C1.ApproximatelyEquals(C2))
                    {
                        Debug.WriteLine(C1.SubtractParallel(C2).ElementwiseNorm(1, 1) / (n * n));
                    }
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, n), 
                                        B = DenseMatrix.Random<Complex>(n, n),
                                        C = new DenseMatrix<Complex>(n, n);
                    sw.Restart();
                    A.Values.multiply_unsafe(B.Values, C.Values, 0, 0, 0, 0, 0, 0, n, n, n, false);
                    sw.Stop();
                    c_block = sw.ElapsedMilliseconds;
                }

                Debug.WriteLine(n + "\t" + d + "\t" + d_block + "\t" + c_block);
            }
        }
        public static void DenseMatrixParallelMultiplicationTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes = { 256, 1024, 4096 };
            List<Type> types = new List<Type>() { typeof(double), typeof(Complex) };

            double dtime = 0, ctime = 0;
            foreach (int n in sizes)
            {
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> A = DenseMatrix.Random<double>(n, n), B = DenseMatrix.Random<double>(n, n);
                    sw.Start();
                    A.MultiplyParallel(B);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, n), B = DenseMatrix.Random<Complex>(n, n);
                    sw.Restart();
                    A.MultiplyParallel(B);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;
                }

                Debug.WriteLine(n + "\t" + dtime + "\t" + ctime);
            }
        }
        public static void SquareMatrixStrassenMultiplicationTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes = { 256, 512, 1024, 2048, 4096 };
            //int[] sizes = { 256, 384, 576, 864, 1296, 1944, 2916 };
            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                //typeof(decimal),
                typeof(Complex)
            };
            bool useWinograd = true;
            double ftime = 0, dtime = 0, dttime = 0, ctime = 0;
            foreach (int n in sizes)
            {
                if (types.Contains(typeof(float)))
                {
                    float[][] A = RectangularMatrix.Random<float>(n, n).ToJagged();
                    float[][] B = RectangularMatrix.Random<float>(n, n).ToJagged();
                    float[][] C = MatrixInternalExtensions.JMatrix<float>(n, n);

                    sw.Restart();
                    var algo = new SquareStrassenMultiplication<float>(new FloatStrassenInstructionSet(), n, useWinograd);
                    algo.Multiply(A, B, C);
                    sw.Stop();
                    ftime = sw.ElapsedMilliseconds;

                    float[][] _C = MatrixInternalExtensions.JMatrix<float>(n, n);
                    A.multiply_unsafe(B, _C);
                    Debug.WriteLine(new DenseMatrix<float>(_C).Subtract(new DenseMatrix<float>(C)).ElementwiseNorm(1, 1) / (n * n));
                }
                if (types.Contains(typeof(double)))
                {
                    double[][] A = RectangularMatrix.Random<double>(n, n).ToJagged();
                    double[][] B = RectangularMatrix.Random<double>(n, n).ToJagged();
                    double[][] Bt = B.Transpose();
                    double[][] C = MatrixInternalExtensions.JMatrix<double>(n, n);
                    double[][] C1 = MatrixInternalExtensions.JMatrix<double>(n, n);

                    sw.Restart();
                    var algo = new SquareStrassenMultiplication<double>(new DoubleStrassenInstructionSet(), n, useWinograd);
                    algo.Multiply(A, B, C);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    var algot = new SquareStrassenMultiplication<double>(new DoubleStrassenInstructionSet(), n, useWinograd, 32, true);
                    algot.Multiply(A, Bt, C1);
                    sw.Stop();
                    dttime = sw.ElapsedMilliseconds;

                    Debug.WriteLine("Double error\t" + new DenseMatrix<double>(A.Multiply(B)).Subtract(new DenseMatrix<double>(C)).ElementwiseNorm(1, 1) / (n * n));
                    Debug.WriteLine("Double transposed error\t" + new DenseMatrix<double>(A.Multiply(B)).Subtract(new DenseMatrix<double>(C1)).ElementwiseNorm(1, 1) / (n * n));
                }
                if (types.Contains(typeof(decimal)))
                {
                    decimal[][] A = RectangularMatrix.Random<decimal>(n, n).ToJagged();
                    decimal[][] B = RectangularMatrix.Random<decimal>(n, n).ToJagged();
                    decimal[][] C = MatrixInternalExtensions.JMatrix<decimal>(n, n);

                    sw.Restart();
                    var algo = new SquareStrassenMultiplication<decimal>(new DecimalStrassenInstructionSet(), n, useWinograd);
                    algo.Multiply(A, B, C);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;

                    decimal[][] _C = MatrixInternalExtensions.JMatrix<decimal>(n, n);
                    A.multiply_unsafe(B, _C);
                    Debug.WriteLine(new DenseMatrix<decimal>(_C).Subtract(new DenseMatrix<decimal>(C)).ElementwiseNorm(1, 1) / (n * n));
                }
                if (types.Contains(typeof(Complex)))
                {
                    Complex[][] A = RectangularMatrix.Random<Complex>(n, n).ToJagged();
                    Complex[][] B = RectangularMatrix.Random<Complex>(n, n).ToJagged();
                    Complex[][] C = MatrixInternalExtensions.JMatrix<Complex>(n, n);

                    sw.Restart();
                    var algo = new SquareStrassenMultiplication<Complex>(new ComplexStrassenInstructionSet(), n, useWinograd);
                    algo.Multiply(A, B, C);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;

                    Complex[][] _C = MatrixInternalExtensions.JMatrix<Complex>(n, n);
                    A.multiply_unsafe(B, _C);
                    Debug.WriteLine(new DenseMatrix<Complex>(_C).Subtract(new DenseMatrix<Complex>(C)).ElementwiseNorm(1, 1) / (n * n));
                }
                Debug.WriteLine(n + "\t" + ftime + "\t" + dtime + "\t" + dttime + "\t" + ctime);
            }
        }
        public static void RectangularMatrixStrassenMultiplicationTest()
        {
            int[] sizes = { 256, 512, 1024, 2048, 4096 };
            double aspectRatio = Math.PI + 0.5;
            List<Type> types = new List<Type>()
            {
                //typeof(float),
                typeof(double),
                //typeof(decimal),
                //typeof(Complex)
            };

            Stopwatch sw = new Stopwatch();
            bool useWinograd = true;
            bool transpose = false;
            double ftime = 0, dtime = 0, mtime = 0, ctime = 0;

            foreach (int m in sizes)
            {
                int n = (int)(aspectRatio * m);
                int p = (int)(m / aspectRatio);
                if (types.Contains(typeof(float)))
                {
                    float[][] A = RectangularMatrix.Random<float>(m, n).ToJagged();
                    float[][] B = RectangularMatrix.Random<float>(n, p).ToJagged(), Bt = B.Transpose();
                    float[][] C = MatrixInternalExtensions.JMatrix<float>(m, p);

                    var algo = new IndefiniteSizeRectStrassenMultiplication<float>(new FloatStrassenInstructionSet(), m, n, p, useWinograd, 32, transpose);
                    sw.Restart();
                    if (transpose) algo.Multiply(A, Bt, C);
                    else algo.Multiply(A, B, C);
                    sw.Stop();
                    ftime = sw.ElapsedMilliseconds;

                    float[][] _C = MatrixInternalExtensions.JMatrix<float>(m, p);
                    A.multiply_unsafe(B, _C);
                    Debug.WriteLine(new DenseMatrix<float>(A).Subtract(new DenseMatrix<float>(C)).ElementwiseNorm(1, 1) / (m * p));
                }
                if (types.Contains(typeof(double)))
                {
                    double[][] A = RectangularMatrix.Random<double>(m, n).ToJagged();
                    double[][] B = RectangularMatrix.Random<double>(n, p).ToJagged(), Bt =  B.Transpose();
                    double[][] C = MatrixInternalExtensions.JMatrix<double>(m, p);

                    var algo = new IndefiniteSizeRectStrassenMultiplication<double>(new DoubleStrassenInstructionSet(), m, n, p, useWinograd, 32, transpose);
                    sw.Restart();
                    if (transpose) algo.Multiply(A, Bt, C);
                    else algo.Multiply(A, B, C);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;

                    double[][] _C = MatrixInternalExtensions.JMatrix<double>(m, p);
                    A.multiply_unsafe(B, _C);
                    Debug.WriteLine(new DenseMatrix<double>(_C).Subtract(new DenseMatrix<double>(C)).ElementwiseNorm(1, 1) / (m * p));
                }
                if (types.Contains(typeof(decimal)))
                {
                    decimal[][] A = RectangularMatrix.Random<decimal>(m, n).ToJagged();
                    decimal[][] B = RectangularMatrix.Random<decimal>(n, p).ToJagged(), Bt = B.Transpose();
                    decimal[][] C = MatrixInternalExtensions.JMatrix<decimal>(m, p);

                    var algo = new IndefiniteSizeRectStrassenMultiplication<decimal>(new DecimalStrassenInstructionSet(), m, n, p, useWinograd, 32, transpose);
                    sw.Restart();
                    if (transpose) algo.Multiply(A, Bt, C);
                    else algo.Multiply(A, B, C);
                    sw.Stop();
                    mtime = sw.ElapsedMilliseconds;

                    decimal[][] _C = MatrixInternalExtensions.JMatrix<decimal>(m, p);
                    A.multiply_unsafe(B, _C);
                    Debug.WriteLine(new DenseMatrix<decimal>(_C).Subtract(new DenseMatrix<decimal>(C)).ElementwiseNorm(1, 1) / (m * p));
                }
                if (types.Contains(typeof(Complex)))
                {
                    Complex[][] A = RectangularMatrix.Random<Complex>(m, n).ToJagged();
                    Complex[][] B = RectangularMatrix.Random<Complex>(n, p).ToJagged(), Bt = B.Transpose();
                    Complex[][] C = MatrixInternalExtensions.JMatrix<Complex>(m, p);

                    var algo = new IndefiniteSizeRectStrassenMultiplication<Complex>(new ComplexStrassenInstructionSet(), m, n, p, useWinograd, 32, transpose);
                    sw.Restart();
                    if (transpose) algo.Multiply(A, Bt, C);
                    else algo.Multiply(A, B, C);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;

                    Complex[][] _C = MatrixInternalExtensions.JMatrix<Complex>(m, p);
                    A.multiply_unsafe(B, _C);
                    Debug.WriteLine(new DenseMatrix<Complex>(_C).Subtract(new DenseMatrix<Complex>(C)).ElementwiseNorm(1, 1) / (m * p));
                }
                Debug.WriteLine(m + ", " + n + ", " + p + "\t" + ftime + "\t" + dtime + "\t" + mtime + "\t" + ctime);
            }
        }
        public static void DenseMatrixParallelStrassenMultiplicationTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes = { 256, 512, 1024, 2048, 4096, 8192 };
            List<Type> types = new List<Type>() {
                typeof(double),
                //typeof(Complex)
            };

            double dtime = 0, _dtime = 0, ctime = 0, _ctime = 0;
            foreach (int n in sizes)
            {
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> A = DenseMatrix.Random<double>(n, n), B = DenseMatrix.Random<double>(n, n);
                    sw.Restart();
                    DenseMatrix<double> result = A.MultiplyStrassen(B, true, true, 512);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    DenseMatrix<double> AB = A.MultiplyParallel(B);
                    sw.Stop();
                    _dtime = sw.ElapsedMilliseconds;

                    bool equal = result.ApproximatelyEquals(AB);
                    if (!equal)
                    {
                        Debug.WriteLine("Warning: strassen != multiply parallel. average norm of |A - B|:\t" + result.Subtract(AB).ElementwiseNorm(1, 1) / (n * n));
                    }
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, n), B = DenseMatrix.Random<Complex>(n, n);
                    sw.Restart();
                    DenseMatrix<Complex> sAB = A.MultiplyStrassen(B, true, true, 512);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    DenseMatrix<Complex> AB = A.MultiplyParallel(B);
                    sw.Stop();
                    _ctime = sw.ElapsedMilliseconds;

                    Debug.Assert(sAB.ApproximatelyEquals(AB));
                }

                Debug.WriteLine(n + "\t" + dtime + "\t" + _dtime + "\t" + ctime + "\t" + _ctime);
            }
        }
        public static void MatrixInversionTest()
        {
            int[] sizes = { 200, 400, 800, 1600, 3200 };
            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                typeof(decimal),
                typeof(Complex)
            };

            Stopwatch sw = new Stopwatch();

            double floatTime = 0, doubleTime = 0, decimalTime = 0, complexTime = 0;
            foreach (int n in sizes)
            {
                if (types.Contains(typeof(float)))
                {
                    DenseMatrix<float> A = RectangularMatrix.Random<float>(n, n);
                    sw.Restart();
                    DenseMatrix<float> Ainv = A.Invert();
                    sw.Stop();
                    floatTime = sw.ElapsedMilliseconds;
                    Debug.Assert(Ainv.Multiply(A).ApproximatelyEquals(RectangularMatrix.Identity<float>(n), 1e-3f));
                }
                
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> B = RectangularMatrix.Random<double>(n, n);
                    sw.Restart();
                    DenseMatrix<double> Binv = B.Invert();
                    sw.Stop();
                    doubleTime = sw.ElapsedMilliseconds;
                    Debug.Assert(Binv.Multiply(B).ApproximatelyEquals(RectangularMatrix.Identity<double>(n)));
                }
                
                if (types.Contains(typeof(decimal)))
                {
                    DenseMatrix<decimal> C = RectangularMatrix.Random<decimal>(n, n);
                    sw.Restart();
                    DenseMatrix<decimal> Cinv = C.Invert();
                    sw.Stop();
                    decimalTime = sw.ElapsedMilliseconds;
                    Debug.Assert(Cinv.Multiply(C).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(n), 1e-8m));
                }
                
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> matrix = RectangularMatrix.Random<Complex>(n, n);
                    sw.Restart();
                    DenseMatrix<Complex> inverse = matrix.Invert();
                    sw.Stop();
                    complexTime = sw.ElapsedMilliseconds;
                    Debug.Assert(matrix.Multiply(inverse).IsIdentity(1e-6));
                }

                Debug.WriteLine($"{n}\t{doubleTime}\tme\t{floatTime}\tms\t{decimalTime}\tms\t{complexTime}\tms");

            }
        }
        public static void MatrixStrassenInversionTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes = { 256, 1024, 4096 };
            List<Type> types = new List<Type>() { typeof(float), typeof(double), typeof(decimal), typeof(Complex) };

            foreach (int n in sizes)
            {
                double dtime = 0, ctime = 0;
                System.Threading.Thread.Sleep(1000);

                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> B = RectangularMatrix.Random<double>(n, n);
                    sw.Restart();
                    DenseMatrix<double> Binv = B.Invert();
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> B = RectangularMatrix.Random<Complex>(n, n);
                    sw.Restart();
                    DenseMatrix<Complex> Binv = B.Invert();
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;
                }

                Debug.WriteLine(n + "\t" + dtime + "\t" + ctime);
            }
        }
        public static void SVDecompositionTest()
        {
            List<Type> types = new List<Type>()
            {
                //typeof(float),
                typeof(double),
                //typeof(decimal),
                typeof(Complex)
            };

            int[] sizes = { 128, 256, 512, 1024, 2048, 4096 };
            Stopwatch sw = new Stopwatch();

            foreach (int n in sizes)
            {
                double ftime = 0.0, dtime = 0.0, mtime = 0.0, ctime = 0.0;
                if (types.Contains(typeof(float)))
                {
                    sw.Restart();
                    DenseMatrix<float> A = DenseMatrix.Random<float>(n, n);
                    A.SingularValueDecompose(out DenseMatrix<float> U, out DenseMatrix<float> D, out DenseMatrix<float> V);
                    ftime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(double)))
                {
                    sw.Restart();
                    DenseMatrix<double> A = DenseMatrix.Random<double>(n, n);
                    A.SingularValueDecompose(out DenseMatrix<double> U, out DenseMatrix<double> D, out DenseMatrix<double> V);
                    dtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(decimal)))
                {
                    sw.Restart();
                    DenseMatrix<decimal> A = DenseMatrix.Random<decimal>(n, n);
                    A.SingularValueDecompose(out DenseMatrix<decimal> U, out DenseMatrix<decimal> D, out DenseMatrix<decimal> V);
                    mtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(Complex)))
                {
                    sw.Restart();
                    DenseMatrix<Complex> A = DenseMatrix.Random<Complex>(n, n);
                    A.SingularValueDecompose(out DenseMatrix<Complex> U, out DenseMatrix<Complex> D, out DenseMatrix<Complex> V);
                    ctime = sw.ElapsedMilliseconds;
                }

                Debug.WriteLine(n + "\t" + ftime + "\t" + dtime + "\t" + mtime + "\t" + ctime);
            }
            
        }

        public static void QRDecompositionTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes = 
            {
                128,
                256,
                512,
                1024,
                2048,
                4096,
                8192
            };
            List<Type> types = new List<Type>()
            {
                typeof(double),
                //typeof(Complex)
            };
            List<QRDecompositionMethod> methods = new List<QRDecompositionMethod>() 
            { 
                //QRDecompositionMethod.GIVENS_ROTATIONS, 
                //QRDecompositionMethod.HOUSEHOLDER_TRANSFORM,
                QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM
            };

            double dtime = 0, dgtime = 0, _dtime = 0, _dgtime = 0, 
                bdtime = 0, _bdtime = 0,
                ctime = 0, cgtime = 0, _ctime = 0, _cgtime = 0;

            foreach (int n in sizes)
            {
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> matrix = DenseMatrix.Random<double>(n, n);
                    if (methods.Contains(QRDecompositionMethod.HOUSEHOLDER_TRANSFORM))
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.QRDecompose(out DenseMatrix<double> Q, out DenseMatrix<double> R, false, QRDecompositionMethod.HOUSEHOLDER_TRANSFORM);
                        sw.Stop();
                        dtime = sw.ElapsedMilliseconds;

                        sw.Restart();
                        matrix.QRDecomposeParallel(out DenseMatrix<double> _Q, out DenseMatrix<double> _R, false, QRDecompositionMethod.HOUSEHOLDER_TRANSFORM);
                        sw.Stop();
                        _dtime = sw.ElapsedMilliseconds;

                        DenseMatrix<double> QR = Q.MultiplyParallel(R), _QR = _Q.MultiplyParallel(_R);
                        if (!QR.ApproximatelyEquals(matrix))
                        {
                            Debug.WriteLine("Householder error:\t" + QR.SubtractParallel(matrix).ElementwiseNorm(1, 1) / (n * n));
                        }
                        if (!_QR.ApproximatelyEquals(matrix))
                        {
                            Debug.WriteLine("Parallel householder error:\t" + _QR.SubtractParallel(matrix).ElementwiseNorm(1, 1) / (n * n));
                        }
                    }

                    if (methods.Contains(QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM))
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.QRDecompose(out DenseMatrix<double> Q, out DenseMatrix<double> R, false, QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM);
                        sw.Stop();
                        bdtime = sw.ElapsedMilliseconds;

                        sw.Restart();
                        matrix.QRDecomposeParallel(out DenseMatrix<double> _Q, out DenseMatrix<double> _R, false, QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM);
                        sw.Stop();
                        _bdtime = sw.ElapsedMilliseconds;

                        DenseMatrix<double> QR = Q.MultiplyParallel(R), _QR = _Q.MultiplyParallel(_R);
                        if (!QR.ApproximatelyEquals(matrix))
                        {
                            Debug.WriteLine("Block householder error:\t" + QR.SubtractParallel(matrix).ElementwiseNorm(1, 1) / (n * n));
                        }
                        if (!_QR.ApproximatelyEquals(matrix))
                        {
                            Debug.WriteLine("Parallel block householder error:\t" + _QR.SubtractParallel(matrix).ElementwiseNorm(1, 1) / (n * n));
                        }
                    }

                    if (methods.Contains(QRDecompositionMethod.GIVENS_ROTATIONS))
                    {
                        sw.Restart();
                        matrix.QRDecompose(out DenseMatrix<double> Q, out DenseMatrix<double> R, false, QRDecompositionMethod.GIVENS_ROTATIONS);
                        sw.Stop();
                        dgtime = sw.ElapsedMilliseconds;

                        sw.Restart();
                        matrix.QRDecomposeParallel(out DenseMatrix<double> _Q, out DenseMatrix<double> _R, false, QRDecompositionMethod.GIVENS_ROTATIONS);
                        sw.Stop();
                        _dgtime = sw.ElapsedMilliseconds;

                        DenseMatrix<double> QR = Q.MultiplyParallel(R), _QR = _Q.MultiplyParallel(_R);
                        if (!QR.ApproximatelyEquals(matrix))
                        {
                            Debug.WriteLine("Givens rotation error:\t" + QR.SubtractParallel(matrix).ElementwiseNorm(1, 1) / (n * n));
                        }
                        if (!_QR.ApproximatelyEquals(matrix))
                        {
                            Debug.WriteLine("Parallel Givens rotation error:\t" + _QR.SubtractParallel(matrix).ElementwiseNorm(1, 1) / (n * n));
                        }
                    }
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> matrix = DenseMatrix.Random<Complex>(n, n);

                    if (methods.Contains(QRDecompositionMethod.HOUSEHOLDER_TRANSFORM))
                    {
                        sw.Restart();
                        matrix.QRDecompose(out DenseMatrix<Complex> Q, out DenseMatrix<Complex> R, false, QRDecompositionMethod.HOUSEHOLDER_TRANSFORM);
                        sw.Stop();
                        ctime = sw.ElapsedMilliseconds;

                        sw.Restart();
                        matrix.QRDecomposeParallel(out DenseMatrix<Complex> _Q, out DenseMatrix<Complex> _R, false, QRDecompositionMethod.HOUSEHOLDER_TRANSFORM);
                        sw.Stop();
                        _ctime = sw.ElapsedMilliseconds;
                    }
                    if (methods.Contains(QRDecompositionMethod.GIVENS_ROTATIONS))
                    {
                        sw.Restart();
                        matrix.QRDecompose(out DenseMatrix<Complex> Qg, out DenseMatrix<Complex> Rg, false, QRDecompositionMethod.GIVENS_ROTATIONS);
                        sw.Stop();
                        cgtime = sw.ElapsedMilliseconds;

                        sw.Restart();
                        matrix.QRDecomposeParallel(out DenseMatrix<Complex> _Q, out DenseMatrix<Complex> _R, false, QRDecompositionMethod.GIVENS_ROTATIONS);
                        sw.Stop();
                        _cgtime = sw.ElapsedMilliseconds;
                    }
                }

                Debug.WriteLine($"{n}\tHH\t{dtime}\tG\t{dgtime}\tBH\t{bdtime}\t||HH\t{_dtime}\t||G\t{_dgtime}\t||BH\t{_bdtime}\t" +
                    $"{ctime}\t{cgtime}\t{_ctime}\t{_cgtime}");
            }
        }
        public static void OptimizeParameterBlockHouseholderTest()
        {
            int size = 8192;
            Stopwatch sw = new Stopwatch();
            for (int p = 256; p <= 512; p += 256)
            {
                DenseMatrix<double> matrix = DenseMatrix.Random<double>(size, size);
                BlockHouseholderQRDecompositionAlgorithm algo = new BlockHouseholderQRDecompositionAlgorithm(p);
                Thread.Sleep(500);
                sw.Restart();
                algo.QRDecompose(matrix, out DenseMatrix<double> Q, out DenseMatrix<double> R, false, false);
                sw.Stop();
                Debug.WriteLine(p + "\t" + sw.ElapsedMilliseconds);

            }
        }
        public static void LUDecompositionTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes =
            {
                128,
                256,
                512,
                1024,
                2048,
                4096
            };
            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                //typeof(decimal),
                typeof(Complex)
            };
            bool testSequential = true, testParallel = true;

            foreach (int n in sizes)
            {
                double ftime = 0, pftime = 0, dtime = 0, pdtime = 0, mtime = 0, pmtime = 0, ctime = 0, pctime = 0;
                if (types.Contains(typeof(float)))
                {
                    DenseMatrix<float> matrix = DenseMatrix.Random<float>(n, n);

                    if (testSequential)
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.LUDecompose(out DenseMatrix<float> L, out DenseMatrix<float> U);
                        sw.Stop();
                        ftime = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Float error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                    }
                    if (testParallel)
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.LUDecomposeParallel(out DenseMatrix<float> L, out DenseMatrix<float> U);
                        sw.Stop();
                        pftime = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Parallel float error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                    }
                }
                if (types.Contains(typeof(double)))
                {
                    DenseMatrix<double> matrix = DenseMatrix.Random<double>(n, n);

                    if (testSequential)
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.LUDecompose(out DenseMatrix<double> L, out DenseMatrix<double> U);
                        sw.Stop();
                        dtime = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Double error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                    }
                    if (testParallel)
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.LUDecomposeParallel(out DenseMatrix<double> L, out DenseMatrix<double> U);
                        sw.Stop();
                        pdtime = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Parallel double error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                    }
                }
                if (types.Contains(typeof(decimal)))
                {
                    DenseMatrix<decimal> matrix = DenseMatrix.Random<decimal>(n, n);

                    if (testSequential)
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.LUDecompose(out DenseMatrix<decimal> L, out DenseMatrix<decimal> U);
                        sw.Stop();
                        mtime = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Decimal error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                    }
                    if (testParallel)
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.LUDecomposeParallel(out DenseMatrix<decimal> L, out DenseMatrix<decimal> U);
                        sw.Stop();
                        pmtime = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Parallel decimal error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                    }
                }
                if (types.Contains(typeof(Complex)))
                {
                    DenseMatrix<Complex> matrix = DenseMatrix.Random<Complex>(n, n);

                    if (testSequential)
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.LUDecompose(out DenseMatrix<Complex> L, out DenseMatrix<Complex> U);
                        sw.Stop();
                        ctime = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Complex error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                    }
                    if (testParallel)
                    {
                        Thread.Sleep(500);
                        sw.Restart();
                        matrix.LUDecomposeParallel(out DenseMatrix<Complex> L, out DenseMatrix<Complex> U);
                        sw.Stop();
                        pctime = sw.ElapsedMilliseconds;
                        Debug.WriteLine("Parallel complex error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                    }
                }

                Debug.WriteLine(n + "\t" + ftime + "\t" + dtime + "\t" + mtime + "\t" + ctime + "\t" + pftime + "\t" + pdtime + "\t" + pmtime + "\t" + pctime);
            }
        }
        public static void LUPDecompositionTest()
        {
            Stopwatch sw = new Stopwatch();
            int[] sizes =
            {
                128,
                256,
                512,
                1024,
                2048,
                4096
            };
            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                //typeof(decimal),
                typeof(Complex)
            };

            foreach (int n in sizes)
            {
                double ftime = 0, dtime = 0, mtime = 0, ctime = 0;
                if (types.Contains(typeof(float)))
                {
                    Thread.Sleep(500);

                    DenseMatrix<float> matrix = DenseMatrix.Random<float>(n, n);

                    sw.Restart();
                    matrix.LUPDecompose(out DenseMatrix<float> L, out DenseMatrix<float> U, out int[] P);
                    sw.Stop();
                    ftime = sw.ElapsedMilliseconds;

                    matrix.PermuteRows(P);
                    Debug.WriteLine("Float error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                }
                if (types.Contains(typeof(double)))
                {
                    Thread.Sleep(500);

                    DenseMatrix<double> matrix = DenseMatrix.Random<double>(n, n);

                    sw.Restart();
                    matrix.LUPDecompose(out DenseMatrix<double> L, out DenseMatrix<double> U, out int[] P);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;

                    matrix.PermuteRows(P);
                    Debug.WriteLine("Double error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                }
                if (types.Contains(typeof(decimal)))
                {
                    Thread.Sleep(500);

                    DenseMatrix<decimal> matrix = DenseMatrix.Random<decimal>(n, n);

                    sw.Restart();
                    matrix.LUPDecompose(out DenseMatrix<decimal> L, out DenseMatrix<decimal> U, out int[] P);
                    sw.Stop();
                    mtime = sw.ElapsedMilliseconds;

                    matrix.PermuteRows(P);
                    Debug.WriteLine("Decimal error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                }
                if (types.Contains(typeof(Complex)))
                {
                    Thread.Sleep(500);

                    DenseMatrix<Complex> matrix = DenseMatrix.Random<Complex>(n, n);

                    sw.Restart();
                    matrix.LUPDecompose(out DenseMatrix<Complex> L, out DenseMatrix<Complex> U, out int[] P);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;

                    matrix.PermuteRows(P);
                    Debug.WriteLine("Complex error:\t" + matrix.SubtractParallel(L.MultiplyParallel(U)).ElementwiseNorm(1, 1) / (n * n));
                }

                Debug.WriteLine(n + "\t" + ftime + "\t" + dtime + "\t" + mtime + "\t" + ctime);
            }
        }
        public static void SparseQRDecompositionTest()
        {
            /// The purpose of this method is to test the crossover point where 
            /// Givens rotation becomes superior to Householder transformations 
            /// as the sparsity of the matrix increases. 

            int n = 1024, trials = 3;
            Stopwatch sw = new Stopwatch();
            for (double sparsity = 0.9;  sparsity < 1.0; sparsity += 0.01)
            {
                double hhTime = 0, grTime = 0;
                for (int i = 0; i < trials; ++i)
                {
                    DenseMatrix<double> matrix = DenseMatrix.RandomSparse<double>(n, n, sparsity);
                    Thread.Sleep(500);

                    sw.Restart();
                    matrix.QRDecompose(out DenseMatrix<double> Q, out DenseMatrix<double> R, false, QRDecompositionMethod.HOUSEHOLDER_TRANSFORM);
                    sw.Stop();
                    hhTime += sw.ElapsedMilliseconds;

                    sw.Restart();
                    matrix.QRDecompose(out DenseMatrix<double> Q1, out DenseMatrix<double> R1, false, QRDecompositionMethod.GIVENS_ROTATIONS);
                    sw.Stop();
                    grTime += sw.ElapsedMilliseconds;
                }

                Debug.WriteLine(sparsity + "\t" + (hhTime / trials) + "\t" + (grTime / trials));
            }
        }
        public static void RealLinearSolverTest()
        {
            int[] sizes = { 200, 400, 800, 1600, 3200 };
            foreach (int n in sizes)
            {
                double[,] A = RectangularMatrix.Random<double>(n, n);
                double[] b = RectangularVector.Random<double>(n);

                Stopwatch sw = new Stopwatch();
                sw.Start();

                Solution<double[]> solution = LinearSolver.Solve(A, b);

                sw.Stop();
                Debug.WriteLine($"{n}\t{sw.ElapsedMilliseconds}\tms");
            }
        }
        public static void ComplexLinearSolverTest()
        {
            int[] sizes = { 200, 400, 800, 1600, 3200 };
            foreach (int n in sizes)
            {
                Complex[,] A = RectangularMatrix.Random<Complex>(n, n);
                Complex[] b = RectangularVector.Random<Complex>(n);

                Stopwatch sw = new Stopwatch();
                sw.Start();

                Solution<Complex[]> solution = LinearSolver.Solve(A, b);

                sw.Stop();
                Debug.WriteLine($"{n}\t{sw.ElapsedMilliseconds}\tms");
            }
        }
        public static void BidiagonalFormTest()
        {
            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                typeof(decimal),
                typeof(Complex)
            };

            Stopwatch sw = new Stopwatch();
            for (int n = 200; n < 1400; n += 200)
            {
                double ftime = 0, dtime = 0, mtime = 0, ctime = 0,
                    ftimep = 0, dtimep = 0, mtimep = 0, ctimep = 0;

                // Sequential tests
                if (types.Contains(typeof(float)))
                {
                    float[,] A = RectangularMatrix.Random<float>(n, n);
                    sw.Restart();
                    A.ToBidiagonalForm(out float[,] U, out float[,] B, out float[,] V);
                    sw.Stop();
                    ftime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(double)))
                {
                    double[,] A = RectangularMatrix.Random<double>(n, n);
                    sw.Restart();
                    A.ToBidiagonalForm(out double[,] U, out double[,] B, out double[,] V);
                    sw.Stop();
                    dtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(decimal)))
                {
                    decimal[,] A = RectangularMatrix.Random<decimal>(n, n);
                    sw.Restart();
                    A.ToBidiagonalForm(out decimal[,] U, out decimal[,] B, out decimal[,] V);
                    sw.Stop();
                    mtime = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(Complex)))
                {
                    Complex[,] A = RectangularMatrix.Random<Complex>(n, n);
                    sw.Restart();
                    A.ToBidiagonalForm(out Complex[,] U, out Complex[,] B, out Complex[,] V);
                    sw.Stop();
                    ctime = sw.ElapsedMilliseconds;
                }

                // Parallel test
                if (types.Contains(typeof(float)))
                {
                    float[,] A = RectangularMatrix.Random<float>(n, n);
                    sw.Restart();
                    A.ToBidiagonalFormParallel(out float[,] U, out float[,] B, out float[,] V);
                    sw.Stop();
                    ftimep = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(double)))
                {
                    double[,] A = RectangularMatrix.Random<double>(n, n);
                    sw.Restart();
                    A.ToBidiagonalFormParallel(out double[,] U, out double[,] B, out double[,] V);
                    sw.Stop();
                    dtimep = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(decimal)))
                {
                    decimal[,] A = RectangularMatrix.Random<decimal>(n, n);
                    sw.Restart();
                    A.ToBidiagonalFormParallel(out decimal[,] U, out decimal[,] B, out decimal[,] V);
                    sw.Stop();
                    mtimep = sw.ElapsedMilliseconds;
                }
                if (types.Contains(typeof(Complex)))
                {
                    Complex[,] A = RectangularMatrix.Random<Complex>(n, n);
                    sw.Restart();
                    A.ToBidiagonalFormParallel(out Complex[,] U, out Complex[,] B, out Complex[,] V);
                    sw.Stop();
                    ctimep = sw.ElapsedMilliseconds;
                }

                Debug.WriteLine($"{n}\t{ftime}\t{dtime}\t{mtime}\t{ctime}\t{ftimep}\t{dtimep}\t{mtimep}\t{ctimep}");
            }
        }
        public static void TridiagonalFormTest()
        {
            Stopwatch sw = new Stopwatch();
            for (int n = 100; n < 1200; n += 200)
            {
                float[,] A = RectangularMatrix.Random<float>(n, n);
                sw.Restart();
                A.ToTridiagonalForm(out float[,] U0, out float[,] D0, out float[,] V0);
                sw.Stop();
                double floatTime = sw.ElapsedMilliseconds;

                double[,] B = RectangularMatrix.Random<double>(n, n);
                sw.Restart();
                B.ToTridiagonalForm(out double[,] U1, out double[,] D1, out double[,] V1);
                sw.Stop();
                double doubleTime = sw.ElapsedMilliseconds;

                decimal[,] C = RectangularMatrix.Random<decimal>(n, n);
                sw.Restart();
                C.ToTridiagonalForm(out decimal[,] U2, out decimal[,] D2, out decimal[,] V2);
                sw.Stop();
                double decimalTime = sw.ElapsedMilliseconds;

                Complex[,] D = RectangularMatrix.Random<Complex>(n, n);
                sw.Restart();
                D.ToTridiagonalForm(out Complex[,] U3, out Complex[,] D3, out Complex[,] V3);
                sw.Stop();
                double complexTime = sw.ElapsedMilliseconds;

                sw.Restart();
                A.ToTridiagonalFormParallel(out float[,] U0p, out float[,] D0p, out float[,] V0p);
                sw.Stop();
                double floatParallelTime = sw.ElapsedMilliseconds;

                sw.Restart();
                B.ToTridiagonalFormParallel(out double[,] U1p, out double[,] D1p, out double[,] V1p);
                sw.Stop();
                double doubleParallelTime = sw.ElapsedMilliseconds;

                sw.Restart();
                C.ToTridiagonalFormParallel(out decimal[,] U2p, out decimal[,] D2p, out decimal[,] V2p);
                sw.Stop();
                double decimalParallelTime = sw.ElapsedMilliseconds;

                sw.Restart();
                D.ToTridiagonalFormParallel(out Complex[,] U3p, out Complex[,] D3p, out Complex[,] V3p);
                sw.Stop();
                double complexParallelTime = sw.ElapsedMilliseconds;

                Debug.WriteLine(n + "\t" + floatTime + "\tms\t" + doubleTime + "\tms\t" + decimalTime + "\tms\t" + complexTime + "\tms\t" + 
                    floatParallelTime + "\tms\t" + doubleParallelTime + "\tms\t" + decimalParallelTime + "\tms\t" + complexParallelTime + "\tms");

                Debug.Assert(U0.ApproximatelyEquals(U0p));
                Debug.Assert(D0.ApproximatelyEquals(D0p));
                Debug.Assert(V0.ApproximatelyEquals(V0p));

                Debug.Assert(U1.ApproximatelyEquals(U1p));
                Debug.Assert(D1.ApproximatelyEquals(D1p));
                Debug.Assert(V1.ApproximatelyEquals(V1p));

                Debug.Assert(U2.ApproximatelyEquals(U2p));
                Debug.Assert(D2.ApproximatelyEquals(D2p));
                Debug.Assert(V2.ApproximatelyEquals(V2p));

                Debug.Assert(U3.ApproximatelyEquals(U3p));
                Debug.Assert(D3.ApproximatelyEquals(D3p));
                Debug.Assert(V3.ApproximatelyEquals(V3p));
            }
        }
        public static void RealCharacteristicPolynomialTest()
        {
            Stopwatch sw = new Stopwatch();
            for (int n = 5; n < 1000; n = (int)(n * 1.5))
            {
                double[,] A = RectangularMatrix.Random<double>(n, n);

                sw.Restart();
                Polynomial<double> cp = A.FaddeevLeVerrierAlgorithm();
                sw.Stop();

                
                Debug.WriteLine(n + "\t" + sw.ElapsedMilliseconds);
            }
        }
        public static void RealEigenvaluesTest()
        {
            Stopwatch sw = new Stopwatch();
            for (int n = 20; n <= 100; n = (int)(n * 1.5))
            {
                double[,] A = RectangularMatrix.Random<double>(n, n);

                sw.Restart();
                Complex[] eig = A.Eigenvalues();
                sw.Stop();
                double t1 = sw.ElapsedMilliseconds;

                Complex[,] C = A.ToComplex();
                sw.Restart();
                Complex[] eig2 = C.Eigenvalues();
                sw.Stop();
                double t2 = sw.ElapsedMilliseconds;

                Debug.WriteLine(n + "\t" + t1 + "\t" + t2);
            }
        }
        public static void HessenbergFormTest()
        {
            List<Type> types = new List<Type>()
            {
                typeof(double),
                typeof(float),
                typeof(decimal),
                typeof(Complex)
            };

            double qhdtime = 0.0, hdtime = 0.0, qhftime = 0, hftime = 0, qhmtime = 0, hmtime = 0, qhctime = 0, hctime = 0;
            Stopwatch sw = new Stopwatch();
            for (int n = 200; n < 2000; n += 200)
            {
                if (types.Contains(typeof(float)))
                {
                    float[,] A = RectangularMatrix.Random<float>(n, n);

                    sw.Restart();
                    A.ToHessenbergForm(out float[,] Q, out float[,] H);
                    sw.Stop();
                    qhftime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.ToHessenbergForm(out float[,] H1);
                    sw.Stop();
                    hftime = sw.ElapsedMilliseconds;

                    Debug.Assert(H1.ApproximatelyEquals(H));
                }
                if (types.Contains(typeof(double)))
                {
                    double[,] A = RectangularMatrix.Random<double>(n, n);

                    sw.Restart();
                    A.ToHessenbergForm(out double[,] Q, out double[,] H);
                    sw.Stop();
                    qhdtime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.ToHessenbergForm(out double[,] H1);
                    sw.Stop();
                    hdtime = sw.ElapsedMilliseconds;

                    Debug.Assert(Q.Transpose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<double>(n), 1e-6));
                    Debug.Assert(Q.Transpose().Multiply(H).Multiply(Q).ApproximatelyEquals(A, 1e-6));
                    Debug.Assert(H1.ApproximatelyEquals(H));
                }
                if (types.Contains(typeof(decimal)))
                {
                    decimal[,] A = RectangularMatrix.Random<decimal>(n, n);

                    sw.Restart();
                    A.ToHessenbergForm(out decimal[,] Q, out decimal[,] H);
                    sw.Stop();
                    qhmtime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.ToHessenbergForm(out decimal[,] H1);
                    sw.Stop();
                    hmtime = sw.ElapsedMilliseconds;

                    Debug.Assert(Q.Transpose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(n), 1e-6m));
                    Debug.Assert(Q.Transpose().Multiply(H).Multiply(Q).ApproximatelyEquals(A, 1e-6m));
                    Debug.Assert(H1.ApproximatelyEquals(H));
                }
                if (types.Contains(typeof(Complex)))
                {
                    Complex[,] A = RectangularMatrix.Random<Complex>(n, n);

                    sw.Restart();
                    A.ToHessenbergForm(out Complex[,] Q, out Complex[,] H);
                    sw.Stop();
                    qhctime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.ToHessenbergForm(out Complex[,] H1);
                    sw.Stop();
                    hctime = sw.ElapsedMilliseconds;

                    //H.Print();
                    //H1.Print();

                    Debug.Assert(Q.ConjugateTranspose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(n), 1e-6));
                    Debug.Assert(Q.ConjugateTranspose().Multiply(H).Multiply(Q).ApproximatelyEquals(A, 1e-6));
                    Debug.Assert(H1.ApproximatelyEquals(H));
                }

                Debug.WriteLine($"{n}\t{qhftime}\t{hftime}\t{qhdtime}\t{hdtime}\t{qhmtime}\t{hmtime}\t{qhctime}\t{hctime}");
            }
        }
        public static void HouseholderHessenbergFormTest()
        {
            // Testing the householder transform of a matrix in hessenberg form
            List<Type> types = new List<Type>()
            {
                typeof(double),
                typeof(float),
                typeof(decimal),
                typeof(Complex)
            };

            double qhdtime = 0.0, hdtime = 0.0, qhftime = 0, hftime = 0, qhmtime = 0, hmtime = 0, qhctime = 0, hctime = 0;
            Stopwatch sw = new Stopwatch();
            for (int n = 200; n < 2000; n += 200)
            {
                if (types.Contains(typeof(float)))
                {
                    float[,] A = RectangularMatrix.RandomHessenberg<float>(n);

                    sw.Restart();
                    A.ToHessenbergForm(out float[,] Q, out float[,] H);
                    sw.Stop();
                    qhftime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.ToHessenbergForm(out float[,] H1);
                    sw.Stop();
                    hftime = sw.ElapsedMilliseconds;

                    Debug.Assert(H1.ApproximatelyEquals(H));
                }
                if (types.Contains(typeof(double)))
                {
                    double[,] A = RectangularMatrix.RandomHessenberg<double>(n);

                    sw.Restart();
                    A.ToHessenbergForm(out double[,] Q, out double[,] H);
                    sw.Stop();
                    qhdtime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.ToHessenbergForm(out double[,] H1);
                    sw.Stop();
                    hdtime = sw.ElapsedMilliseconds;

                    Debug.Assert(Q.Transpose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<double>(n), 1e-6));
                    Debug.Assert(Q.Transpose().Multiply(H).Multiply(Q).ApproximatelyEquals(A, 1e-6));
                    Debug.Assert(H1.ApproximatelyEquals(H));
                }
                if (types.Contains(typeof(decimal)))
                {
                    decimal[,] A = RectangularMatrix.RandomHessenberg<decimal>(n);

                    sw.Restart();
                    A.ToHessenbergForm(out decimal[,] Q, out decimal[,] H);
                    sw.Stop();
                    qhmtime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.ToHessenbergForm(out decimal[,] H1);
                    sw.Stop();
                    hmtime = sw.ElapsedMilliseconds;

                    Debug.Assert(Q.Transpose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(n), 1e-6m));
                    Debug.Assert(Q.Transpose().Multiply(H).Multiply(Q).ApproximatelyEquals(A, 1e-6m));
                    Debug.Assert(H1.ApproximatelyEquals(H));
                }
                if (types.Contains(typeof(Complex)))
                {
                    Complex[,] A = RectangularMatrix.RandomHessenberg<Complex>(n);

                    sw.Restart();
                    A.ToHessenbergForm(out Complex[,] Q, out Complex[,] H);
                    sw.Stop();
                    qhctime = sw.ElapsedMilliseconds;

                    sw.Restart();
                    A.ToHessenbergForm(out Complex[,] H1);
                    sw.Stop();
                    hctime = sw.ElapsedMilliseconds;

                    Debug.Assert(Q.ConjugateTranspose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(n), 1e-6));
                    Debug.Assert(Q.ConjugateTranspose().Multiply(H).Multiply(Q).ApproximatelyEquals(A, 1e-6));
                    Debug.Assert(H1.ApproximatelyEquals(H));
                }

                Debug.WriteLine($"{n}\t{qhftime}\t{hftime}\t{qhdtime}\t{hdtime}\t{qhmtime}\t{hmtime}\t{qhctime}\t{hctime}");
            }
        }
    }
}
