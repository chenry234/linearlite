﻿using LinearLite.Structs.Tensors;
using LinearLite.Tensors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Benchmarks
{
    public class TensorBenchmarks
    {
        public static void AccessBenchmarks()
        {
            int dimension = 4;

            Random r = new Random();
            Stopwatch sw = new Stopwatch();
            int[] orders = { 2, 3, 4, 5, 6, 8, 9 };
            foreach (int order in orders)
            {
                int[] dimensions = new int[order];
                for (int i = 0; i < order; ++i)
                {
                    dimensions[i] = dimension;
                }

                int[] index = new int[order];
                for (int i = 0; i < order; ++i)
                {
                    index[i] = r.Next(dimension);
                }

                DenseTensor<double> tensor = new DenseTensor<double>(dimensions);
                sw.Restart();
                for (int i = 0; i < 10000000; ++i)
                {
                    tensor.Set(Math.PI, index);
                    double val = tensor.Get(index);
                }
                sw.Stop();
                double time1 = sw.ElapsedMilliseconds;

                SparseTensor<double> st = new SparseTensor<double>(dimensions);
                sw.Restart();
                for (int i = 0; i < 10000000; ++i)
                {
                    st.Set(Math.PI, index);
                    double val = st.Get(index);
                }
                sw.Stop();
                double time2 = sw.ElapsedMilliseconds;

                Debug.WriteLine(order + "\t" + time1 + "\tms\t" + time2 + "\tms");
            }
        }

        /// <summary>
        /// Remarkably, the Tensor2 implementation is so efficient that once the 
        /// tensors are constructed, the addition step is faster than 2D
        /// matrix addition.
        /// </summary>
        internal static void MatrixTensorBenchmarks()
        {

        }
    }
}
