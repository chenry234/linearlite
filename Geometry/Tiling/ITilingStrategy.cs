﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Geometry.Tiling
{
    public abstract class ITilingStrategy
    {
        public abstract SpatialPartition2D Partition(int x, int y, int xLength, int yLength);
        public abstract SpatialPartition3D Partition(int x, int y, int z, int xLength, int yLength, int zLength);
    }
}
