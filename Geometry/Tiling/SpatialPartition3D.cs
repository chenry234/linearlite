﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Geometry.Tiling
{
    public class SpatialPartition3D
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public int LengthX { get; set; }
        public int LengthY { get; set; }
        public int LengthZ { get; set; }

        public List<Tile3D> Tiles { get; set; }

        internal SpatialPartition3D(int x, int y, int z, int xlen, int ylen, int zlen, List<Tile3D> tiles)
        {
            X = x;
            Y = y;
            Z = z;

            LengthX = xlen;
            LengthY = ylen;
            LengthZ = zlen;

            Tiles = tiles;
        }

        public SpatialPartition3D Copy()
        {
            List<Tile3D> copy = new List<Tile3D>();
            foreach (Tile3D tile in Tiles)
            {
                copy.Add(tile.Copy());
            }
            return new SpatialPartition3D(X, Y, Z, LengthX, LengthY, LengthZ, copy);
        }
    }
}
