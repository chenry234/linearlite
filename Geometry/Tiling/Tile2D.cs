﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Geometry.Tiling
{
    public class Tile2D
    {
        internal bool IsRemainder { get; set; }

        internal int RowStart { get; set; }
        internal int ColumnStart { get; set; }

        internal int Rows { get; set; }
        internal int Columns { get; set; }

        internal int RowEnd { get { return RowStart + Rows; } }
        internal int ColumnEnd { get { return ColumnStart + Columns; } }

        internal Tile2D(int rowStart, int colStart, int rows, int cols, bool isRemainder)
        {
            RowStart = rowStart;
            ColumnStart = colStart;
            Rows = rows;
            Columns = cols;
            IsRemainder = isRemainder;
        }

        public void Shift(int rows, int columns)
        {
            RowStart += rows;
            ColumnStart += columns;
        }
        public void Transpose()
        {
            int temp = RowStart;
            RowStart = ColumnStart;
            ColumnStart = temp;

            temp = Rows;
            Rows = Columns;
            Columns = temp;
        }
        public Tile2D Copy()
        {
            return new Tile2D(RowStart, ColumnStart, Rows, Columns, IsRemainder);
        }
    }
}
