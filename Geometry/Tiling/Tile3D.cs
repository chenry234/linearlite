﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Geometry.Tiling
{
    public class Tile3D
    {
        public bool IsRemainder { get; set; }

        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public int LengthX { get; set; }
        public int LengthY { get; set; }
        public int LengthZ { get; set; }

        public int EndX { get { return X + LengthX; } }
        public int EndY { get { return Y + LengthY; } }
        public int EndZ { get { return Z + LengthZ; } }

        public Tile3D(int x, int y, int z, int xlen, int ylen, int zlen, bool isRemainder)
        {
            if (xlen < 0 || ylen < 0 || zlen < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            X = x;
            Y = y;
            Z = z;
            LengthX = xlen;
            LengthY = ylen;
            LengthZ = zlen;
            IsRemainder = isRemainder;
        }

        public void Shift(int x, int y, int z)
        {
            X += x;
            Y += y;
            Z += z;
        }

        public Tile3D Copy()
        {
            return new Tile3D(X, Y, Z, LengthX, LengthY, LengthZ, IsRemainder);
        }
        public void Print()
        {
            Debug.WriteLine($"Start: ({X}, {Y}, {Z}), dimensions: ({LengthX}, {LengthY}, {LengthZ}), remainder: {IsRemainder}");
        }
    }
}
