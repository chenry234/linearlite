﻿using LinearLite.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Global
{
    public static class Constants
    {
        public static readonly double SQRT_2 = Math.Sqrt(2.0);
        public static readonly float SQRT_2f = (float)Math.Sqrt(2.0);
        public static readonly decimal SQRT_2m = DecimalMath.Sqrt(2.0m);
    }
}
