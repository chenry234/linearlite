﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class Precision
    {
        public const float FLOAT_PRECISION = 1E-5f;
        public const double DOUBLE_PRECISION = 1E-8;
        public const decimal DECIMAL_PRECISION = 1E-16m;

    }
}
