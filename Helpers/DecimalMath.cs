﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Helpers
{
    public static class DecimalMath
    {
        private static int _max_iterations = 100;

        /// <summary>
        /// The pi constant in decimal precision
        /// </summary>
        public const decimal PI = 3.14159265358979323846264338327950288419716939937510m;

        /// <summary>
        /// Euler's constant in decimal precision
        /// </summary>
        public const decimal E = 2.7182818284590452353602874713526624977572470936999595749m;

        /// <summary>
        /// Calculate the decimal-precision square root of n, using the Babylonian method
        /// </summary>
        /// <returns></returns>
        public static decimal Sqrt(decimal n)
        {
            if (n <= 0m) return 0m;

            decimal x = (decimal)Math.Sqrt((double)n);
            for (int i = 0; i < _max_iterations; ++i)
            {
                decimal next = (n / x + x) / 2m;
                if (next == x)
                {
                    return next;
                }
                x = next;
            }
            return x;
        }

        public static decimal Pow(decimal bse, decimal pow)
        {
            // TODO: complete this function in decimal precision - currently borrows the Math.Pow method for doubles
            return (decimal)Math.Pow((double)bse, (double)pow);
        }

        /// <summary>
        /// TODO - complete this method in double precision
        /// </summary>
        /// <param name="pow"></param>
        /// <returns></returns>
        public static decimal Exp(decimal pow)
        {
            return (decimal)Math.Exp((double)pow);
        }
        public static decimal Log(decimal x)
        {
            return (decimal)Math.Log((double)x);
        }
        public static decimal Acos(decimal x)
        {
            return (decimal)Math.Acos((double)x);
        }
    }
}
