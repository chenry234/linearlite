﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Helpers
{
    internal static class DefaultGenerators
    {
        private static Random _random = new Random();

        public static double RandomDouble(int min, int max)
        {
            return _random.NextDouble() * (max - min) + min;
        }
        public static double RandomDouble()
        {
            return _random.NextDouble() * 2 - 1;
        }
        public static float RandomFloat(float min, float max)
        {
            return (float)_random.NextDouble() * (max - min) + min;
        }
        public static float RandomFloat()
        {
            return (float)_random.NextDouble() * 2 - 1;
        }
        public static int RandomInt(int min, int max)
        {
            return _random.Next(min, max);
        }
        public static int RandomInt()
        {
            return _random.Next(int.MinValue, int.MaxValue);
        }
        public static long RandomLong(long min, long max)
        {
            double range = max - min;
            return (long)Math.Round(_random.NextDouble() * range) + min;
        }
        public static long RandomLong()
        {
            return RandomLong(long.MinValue, long.MaxValue);
        }
        public static decimal RandomDecimal(decimal min, decimal max)
        {
            return (decimal)_random.NextDouble() * (max - min) + min;
        }
        public static decimal RandomDecimal()
        {
            return (decimal)_random.NextDouble() * 2 - 1;
        }
        public static Complex RandomComplex(double maxModulus)
        {
            return new Complex(_random.NextDouble() * maxModulus, _random.NextDouble() * 2 * Math.PI);
        }
        public static Complex RandomComplex()
        {
            return new Complex(_random.NextDouble(), _random.NextDouble() * 2 * Math.PI);
        }

        internal static Func<T> GetRandomGenerator<T>()
        {
            Type type = typeof(T);
            if (type == typeof(double))
            {
                return (dynamic)(new Func<double>(() => RandomDouble()));
            }
            if (type == typeof(float))
            {
                return (dynamic)(new Func<float>(() => RandomFloat()));
            }
            if (type == typeof(int))
            {
                return (dynamic)(new Func<int>(() => RandomInt()));
            }
            if (type == typeof(long))
            {
                return (dynamic)(new Func<long>(() => RandomLong()));
            }
            if (type == typeof(decimal))
            {
                return (dynamic)(new Func<decimal>(() => RandomDecimal()));
            }
            if (type == typeof(Complex))
            {
                return (dynamic)(new Func<Complex>(() => RandomComplex()));
            }
            throw new NotSupportedException();
        }
        internal static Func<string, T> GetParser<T>()
        {
            Type type = typeof(T);
            if (type == typeof(double))
            {
                return (dynamic)
                    (new Func<string, double>(s => {
                        if (double.TryParse(s, out double v)) return v;
                        return 0.0;
                    }));
            }
            if (type == typeof(float))
            {
                return (dynamic)
                    (new Func<string, float>(s => {
                        if (float.TryParse(s, out float v)) return v;
                        return 0.0f;
                    }));
            }
            if (type == typeof(int))
            {
                return (dynamic)
                    (new Func<string, int>(s => {
                        if (int.TryParse(s, out int v)) return v;
                        return 0;
                    }));
            }
            if (type == typeof(long))
            {
                return (dynamic)
                    (new Func<string, long>(s => {
                        if (long.TryParse(s, out long v)) return v;
                        return 0L;
                    }));
            }
            if (type == typeof(decimal))
            {
                return (dynamic)
                    (new Func<string, decimal>(s => {
                        if (decimal.TryParse(s, out decimal v)) return v;
                        return 0.0m;
                    }));
            }
            if (type == typeof(Complex))
            {
                return (dynamic)
                    (new Func<string, Complex>(s => {
                        if (Complex.TryParse(s, out Complex c)) return c;
                        return Complex.Zero;
                    }));
            }
            throw new NotSupportedException();
        }


        internal static T GetOne<T>() where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(float)) return (dynamic)1.0F;
            if (type == typeof(double)) return (dynamic)1.0;
            if (type == typeof(decimal)) return (dynamic)1.0M;
            if (type == typeof(int)) return (dynamic)1;
            if (type == typeof(long)) return (dynamic)1L;
            if (type == typeof(Complex)) return (dynamic)Complex.One;
            throw new NotSupportedException();
        }
        internal static T GetZero<T>() where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(float)) return (dynamic)0.0F;
            if (type == typeof(double)) return (dynamic)0.0;
            if (type == typeof(decimal)) return (dynamic)0.0M;
            if (type == typeof(int)) return (dynamic)0;
            if (type == typeof(long)) return (dynamic)0L;
            if (type == typeof(Complex)) return (dynamic)Complex.Zero;
            throw new NotSupportedException();
        }
    }
}
