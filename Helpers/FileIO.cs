﻿using System;
using System.Collections.Generic;
using System.IO;

namespace LinearLite.Helpers
{
    public static class FileIO
    {
        public static List<string> ReadAllLines(string filename)
        {
            if (File.Exists(filename))
            {
                throw new FileNotFoundException(filename);
            }

            List<string> lines = new List<string>();
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using (var sr = new StreamReader(fs))
            {
                while (sr.Peek() > -1)
                {
                    lines.Add(sr.ReadLine());
                }
            }
            return lines;
        }
        public static string ReadAllText(string filename)
        {
            if (File.Exists(filename))
            {
                throw new FileNotFoundException(filename);
            }

            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            using (var sr = new StreamReader(fs))
            {
                return sr.ReadToEnd().Trim();
            }
        }
    }
}
