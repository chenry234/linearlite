﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Helpers
{
    public static class IntegerMath
    {
        /// <summary>
        /// TODO: convert this into exponentiation via squaring
        /// </summary>
        /// <param name="bse"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        public static int Pow(int bse, int power)
        {
            return (int)Math.Pow(bse, power);
        }

        /// <summary>
        /// TODO: convert this into exponentiation via squaring
        /// </summary>
        /// <param name="bse"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        public static long Pow(long bse, long power)
        {
            return (long)Math.Pow(bse, power);
        }
    }
}
