﻿using LinearLite.BLAS;
using LinearLite.Structs.Matrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Band
{
    public static class BandMatrixDeterminantExtensions
    {
        private static T Determinant<T>(this BandMatrix<T> matrix, IBLAS<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.CheckIsSquare(matrix);

            int rows = matrix.Rows,
                last = matrix.Columns - 1, 
                lowerBandwidth = matrix.LowerBandwidth,
                i, j;

            T[][] lower = matrix.LowerBands;
            
            double factor = 1.0;

            // Iterate through the columns
            for (j = 0; j < last; ++j)
            {
                int start = j + 1, 
                    end = Math.Min(rows - j - 1, lowerBandwidth);

                // Row reduce
                T pivot = matrix.Diagonal[j];
                for (i = 0; i < end; ++i)
                {
                    // row i -= alpha * row j

                    T alpha = blas.Divide(matrix.LowerBands[i][j], pivot);

                }
            }
            throw new NotImplementedException();
        }
    }
}
