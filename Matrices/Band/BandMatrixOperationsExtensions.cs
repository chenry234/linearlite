﻿using LinearLite.BLAS;
using LinearLite.Structs;
using LinearLite.Structs.Matrix;
using System;

namespace LinearLite.Matrices.Band
{
    public static class BandMatrixOperationsExtensions
    {
        public static BandMatrix<int> Add(this BandMatrix<int> A, BandMatrix<int> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<long> Add(this BandMatrix<long> A, BandMatrix<long> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<float> Add(this BandMatrix<float> A, BandMatrix<float> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<double> Add(this BandMatrix<double> A, BandMatrix<double> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<decimal> Add(this BandMatrix<decimal> A, BandMatrix<decimal> B) => A.Add(B, (u, v) => u.Add(v));
        public static BandMatrix<T> Add<T>(this BandMatrix<T> A, BandMatrix<T> B) where T : AdditiveGroup<T>, new() => A.Add(B, (u, v) => u.Add(v));

        public static BandMatrix<int> Subtract(this BandMatrix<int> A, BandMatrix<int> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<long> Subtract(this BandMatrix<long> A, BandMatrix<long> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<float> Subtract(this BandMatrix<float> A, BandMatrix<float> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<double> Subtract(this BandMatrix<double> A, BandMatrix<double> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<decimal> Subtract(this BandMatrix<decimal> A, BandMatrix<decimal> B) => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());
        public static BandMatrix<T> Subtract<T>(this BandMatrix<T> A, BandMatrix<T> B) where T : AdditiveGroup<T>, new() => A.Subtract(B, (u, v) => u.Subtract(v), u => u.Negate());

        public static BandMatrix<int> Multiply(this BandMatrix<int> A, BandMatrix<int> B) => A.Multiply(B, new IntBLAS());
        public static BandMatrix<long> Multiply(this BandMatrix<long> A, BandMatrix<long> B) => A.Multiply(B, new LongBLAS());
        public static BandMatrix<float> Multiply(this BandMatrix<float> A, BandMatrix<float> B) => A.Multiply(B, new FloatBLAS());
        public static BandMatrix<double> Multiply(this BandMatrix<double> A, BandMatrix<double> B) => A.Multiply(B, new DoubleBLAS());
        public static BandMatrix<decimal> Multiply(this BandMatrix<decimal> A, BandMatrix<decimal> B) => A.Multiply(B, new DecimalBLAS());
        public static BandMatrix<T> Multiply<T>(this BandMatrix<T> A, BandMatrix<T> B) where T : Ring<T>, new() => A.Multiply(B, new RingBLAS<T>());

        public static int[] Multiply(this BandMatrix<int> A, int[] v) => A.Multiply(v, new IntBLAS());
        public static long[] Multiply(this BandMatrix<long> A, long[] v) => A.Multiply(v, new LongBLAS());
        public static float[] Multiply(this BandMatrix<float> A, float[] v) => A.Multiply(v, new FloatBLAS());
        public static double[] Multiply(this BandMatrix<double> A, double[] v) => A.Multiply(v, new DoubleBLAS());
        public static decimal[] Multiply(this BandMatrix<decimal> A, decimal[] v) => A.Multiply(v, new DecimalBLAS());
        public static T[] Multiply<T>(this BandMatrix<T> A, T[] v) where T : Ring<T>, new() => A.Multiply(v, new RingBLAS<T>());

        public static BandMatrix<int> Multiply(this BandMatrix<int> A, int scalar) => A.Multiply(scalar, new IntBLAS());
        public static BandMatrix<long> Multiply(this BandMatrix<long> A, long scalar) => A.Multiply(scalar, new LongBLAS());
        public static BandMatrix<float> Multiply(this BandMatrix<float> A, float scalar) => A.Multiply(scalar, new FloatBLAS());
        public static BandMatrix<double> Multiply(this BandMatrix<double> A, double scalar) => A.Multiply(scalar, new DoubleBLAS());
        public static BandMatrix<decimal> Multiply(this BandMatrix<decimal> A, decimal scalar) => A.Multiply(scalar, new DecimalBLAS());
        public static BandMatrix<T> Multiply<T>(this BandMatrix<T> A, T scalar) where T : Ring<T>, new() => A.Multiply(scalar, new RingBLAS<T>());

        public static BandMatrix<int> MultiplyParallel(this BandMatrix<int> A, BandMatrix<int> B) => A.MultiplyParallel(B, new IntBLAS());
        public static BandMatrix<long> MultiplyParallel(this BandMatrix<long> A, BandMatrix<long> B) => A.MultiplyParallel(B, new LongBLAS());
        public static BandMatrix<float> MultiplyParallel(this BandMatrix<float> A, BandMatrix<float> B) => A.MultiplyParallel(B, new FloatBLAS());
        public static BandMatrix<double> MultiplyParallel(this BandMatrix<double> A, BandMatrix<double> B) => A.MultiplyParallel(B, new DoubleBLAS());
        public static BandMatrix<decimal> MultiplyParallel(this BandMatrix<decimal> A, BandMatrix<decimal> B) => A.MultiplyParallel(B, new DecimalBLAS());
        public static BandMatrix<T> MultiplyParallel<T>(this BandMatrix<T> A, BandMatrix<T> B) where T : Ring<T>, new() => A.MultiplyParallel(B, new RingBLAS<T>());

        public static BandMatrix<T> ElementwiseOperate<F, T>(this BandMatrix<F> A, Func<F, T> Operation) where F : new() where T : new()
        {
            int upperBandWidth = A.UpperBandwidth, lowerBandwidth = A.LowerBandwidth, i;
            T[][] upper = new T[upperBandWidth][], lower = new T[lowerBandwidth][];
            for (i = 0; i < upperBandWidth; ++i) 
            {
                upper[i] = A.UpperBands[i].ElementwiseOperate(Operation);
            }
            for (i = 0; i < lowerBandwidth; ++i)
            {
                lower[i] = A.LowerBands[i].ElementwiseOperate(Operation);
            }
            T[] diagonal = A.Diagonal.ElementwiseOperate(Operation);
            return new BandMatrix<T>(A.Rows, A.Columns, upper, diagonal, lower);
        }

        public static int Trace(this BandMatrix<int> A) => A.Trace(new IntBLAS());
        public static long Trace(this BandMatrix<long> A) => A.Trace(new LongBLAS());
        public static float Trace(this BandMatrix<float> A) => A.Trace(new FloatBLAS());
        public static double Trace(this BandMatrix<double> A) => A.Trace(new DoubleBLAS());
        public static decimal Trace(this BandMatrix<decimal> A) => A.Trace(new DecimalBLAS());
        public static T Trace<T>(this BandMatrix<T> A) where T : Ring<T>, new() => A.Trace(new RingBLAS<T>());
    }
}
