﻿using LinearLite.Matrices;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Decompositions
{
    public static class BlockLDUDecomposition
    {
        /// <summary>
        /// <para>
        /// Calculate the block-LDU decomposition of matrix $A$.
        /// </para>
        /// <para>
        /// Factorises square matrix $A = LDU$ where $L$ is a lower triangular matrix, $D$ is a diagonal matrix, and $U$ is a upper triangular matrix.
        /// </para>
        /// </summary>
        /// <name>BlockLDUDecompose</name>
        /// <proto>void BlockLDUDecompose(this IMatrix<T> A, out IMatrix<T> L, out IMatrix<T> D, out IMatrix<T> U)</proto>
        /// <cat>la</cat>
        /// <param name="A">The matrix to be decomposed</param>
        /// <param name="L">The lower triangular matrix factor</param>
        /// <param name="U">The upper triangular matrix factor</param>
        public static void BlockLDUDecompose(this DenseMatrix<double> A, out DenseMatrix<double> L, out DenseMatrix<double> D, out DenseMatrix<double> U, int topLeftBlockDimension = -1)
        {
            int rows = A.Rows, columns = A.Columns;
            if (topLeftBlockDimension < 0)
            {
                topLeftBlockDimension = Math.Min(rows / 2, columns / 2);
            }
            int b_cols = columns - topLeftBlockDimension, c_rows = rows - topLeftBlockDimension;

            DenseMatrix<double> _A = A.Submatrix(0, 0, topLeftBlockDimension, topLeftBlockDimension);

            // Order operations so as to minimise memory consumption
            D = new DenseMatrix<double>(rows, columns);
            D.CopyFrom(_A);

            // Invert A and store in A - no need to reference A at this point.
            _A = _A.Invert();

            // E = A^-1B
            DenseMatrix<double> _E = _A.Multiply(A.Submatrix(0, topLeftBlockDimension, topLeftBlockDimension, b_cols));
            DenseMatrix<double> _C = A.Submatrix(topLeftBlockDimension, 0, c_rows, topLeftBlockDimension);
            DenseMatrix<double> _D = A.Submatrix(topLeftBlockDimension, topLeftBlockDimension, c_rows, b_cols);

            D.CopyFrom(_D.Subtract(_C.Multiply(_E)), 0, 0, topLeftBlockDimension, topLeftBlockDimension);

            // Help the GC (?)
            _D = null;

            // U only depends on E, so leave until D is disposed
            U = DenseMatrix.Identity<double>(columns);
            U.CopyFrom(_E, 0, 0, 0, topLeftBlockDimension);

            // Help the GC (?)
            _E = null;

            // L only depends on A and C, so leave until D, E are disposed 
            L = DenseMatrix.Identity<double>(rows);
            L.CopyFrom(_C.Multiply(_A), 0, 0, topLeftBlockDimension, 0);
        }
    }
}
