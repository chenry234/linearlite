﻿
using LinearLite.Matrices;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Decomposition
{
    public static class BlockLUDecomposition
    {
        /// <summary>
        /// <para>
        /// Calculate the block-LU decomposition of matrix $A$.
        /// </para>
        /// <para>
        /// Factorises square matrix $A = LU$ where $L$ is a lower triangular matrix and $U$ is a upper triangular matrix.
        /// </para>
        /// Also see:
        /// <ul>
        /// <li><a href="#LUDecompose"><txt>LUDecompose(this IMatrix&lt;T&gt; A, out IMatrix&lt;T&gt; L, out IMatrix&lt;T&gt; U)</txt></a></li>
        /// </ul>
        /// </summary>
        /// <name>BlockLUDecompose</name>
        /// <proto>void BlockLUDecompose(this IMatrix<T> A, out IMatrix<T> L, out IMatrix<T> U)</proto>
        /// <cat>la</cat>
        /// <param name="A">The matrix to be decomposed</param>
        /// <param name="L">The lower triangular matrix factor</param>
        /// <param name="U">The upper triangular matrix factor</param>
        public static void BlockLUDecompose(this DenseMatrix<double> M, out DenseMatrix<double> L, out DenseMatrix<double> U)
        {
            MatrixChecks.CheckNotNull(M);
            MatrixChecks.CheckIsSquare(M);

            int dimension = M.Rows;

            int topLeftDimension = dimension / 2,
                b_cols = dimension - topLeftDimension;

            // Q := D - CA^-1B where M = (A B C D)
            DenseMatrix<double> _B = M.Submatrix(0, topLeftDimension, topLeftDimension, b_cols);
            DenseMatrix<double> _C = M.Submatrix(topLeftDimension, 0, b_cols, topLeftDimension);
            DenseMatrix<double> _A = M.Submatrix(0, 0, topLeftDimension, topLeftDimension);
            DenseMatrix<double> _Q = M.Submatrix(topLeftDimension, topLeftDimension, b_cols, b_cols).Subtract(_C.Multiply(_A.Invert()).Multiply(_B));
            _Q.CholeskyDecompose(out DenseMatrix<double> sqrtQ);

            L = new DenseMatrix<double>(dimension, dimension);
            L.CopyFrom(sqrtQ, 0, 0, topLeftDimension, topLeftDimension);

            U = new DenseMatrix<double>(dimension, dimension);
            U.CopyFrom(sqrtQ.Transpose(), 0, 0, topLeftDimension, topLeftDimension);

            _A.CholeskyDecompose(out DenseMatrix<double> sqrtA);
            L.CopyFrom(sqrtA);
            U.CopyFrom(sqrtA.Transpose());

            // Invert sqrt A, store in sqrtA
            sqrtA = sqrtA.Invert();
            L.CopyFrom(_C.Multiply(sqrtA.Transpose()), 0, 0, topLeftDimension, 0);
            U.CopyFrom(sqrtA.Multiply(_B), 0, 0, 0, topLeftDimension);

        }
    }
}
