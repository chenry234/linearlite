﻿using LinearLite.BLAS;
using LinearLite.Matrices;
using LinearLite.Structs;
using LinearLite.Structs.Vectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Decompositions
{
    public static class CanonicalPolyadicDecomposition
    {
        /// <summary>
        /// <para>Calculate the canonical polyadic decomposition (tensor rank decomposition, PARAFAC, CANDECOMP) of a matrix $A\in\mathbb{F}^{m\times n}$, into two vectors $u\in\mathbb{F}^m, v\in\mathbb{F}^n$ whose 
        /// outer product best approximates $A$ in a elementwise least-squares sense.</para>
        /// <para>
        /// Implementation uses Regulatized Alternating Least Squares (RALS) algorithm, which finds the vectors $u, v$ as solutions to the optimization problem
        /// $$u, v = \argmin_{u', v'}\Big\{{||A - u' \otimes v'||_F + \lambda_u ||u'||_F + \lambda_v ||v'||_F}\Big\}$$
        /// </para>
        /// <!--inputs-->
        /// 
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// // Create a 10 x 10 rank-1 matrix 
        /// DenseMatrix&lt;double&gt; A = DenseMatrix.RandomLowRankMatrix&lt;double&gt;(10, 10, 1); 
        /// 
        /// // Perform the CPD with no regularization coefficients
        /// A.CanonicalPolyadicDecompose(A, out DenseVector&lt;double&gt; u, out DenseVector&lt;double&gt; v, 0.0, 0.0);
        /// 
        /// // Since A has rank 1, CPD is exact
        /// Console.WriteLine(u.OuterProduct(v).ApproximatelyEquals(A)); // true
        /// 
        /// </code></pre>
        /// 
        /// </summary>
        /// <name>CanonicalPolyadicDecompose</name>
        /// <proto>void CanonicalPolyadicDecompose(this IMatrix<T> A, out IVector<T> u, out IVector<T> v, double lambdaU = 0.0, double lambdaV = 0.0)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times n$ matrix $A$ to be decomposed.</param>
        /// <param name="u">
        /// <b>Out parameter</b><br/>
        /// The $m$-dimensional vector $u$.
        /// </param>
        /// <param name="v">
        /// <b>Out parameter</b><br/>
        /// The $n$-dimensional vector $v$.
        /// </param>
        /// <param name="lambdaU">
        /// <b>Optional parameter</b>, defaults to <txt>0.0</txt> <br/>
        /// The regularization term for vector $u$, $\lambda_u$.
        /// </param>
        /// <param name="lambdaV">
        /// <b>Optional parameter</b>, defaults to <txt>0.0</txt> <br/>
        /// The regularization term for vector $v$, $\lambda_v$.
        /// </param>
        public static void CanonicalPolyadicDecompose(this DenseMatrix<double> A, out DenseVector<double> u, out DenseVector<double> v, double lambdaU = 0.0, double lambdaV = 0.0)
        {
            MatrixChecks.CheckNotNull(A);

            int checkLossEvery = 5;

            /// Exit the ALS iterative loop when the absolute scaled norm of | A - uv | / (mn) < threshold
            double absoluteNormTerminationThreshold = 1e-20;
            /// Exit the ALS iterative loop when the absolute scaled norm of | A - uv | / mn changes by less
            /// than this amount in between two measures.
            double deltaNormTerminationThreshold = 1e-20;

            double[][] _A = A.Values;

            int m = A.Rows, n = A.Columns, i, j;

            // Initialise u and v vectors to sensible values 
            double[] _u = new double[m], _v = new double[n];
            for (i = 0; i < m; ++i)
            {
                double[] A_i = A[i];
                for (j = 0; j < n; ++j)
                {
                    double A_ij = A_i[j];
                    _u[i] += A_ij;
                    _v[j] += A_ij;
                }
                _u[i] /= n;
            }
            for (j = 0; j < n; ++j)
            {
                _v[j] /= m;
            }

            double lastNormMeasurement = double.MaxValue;

            // Iterate until convergence
            for (int t = 0; t < 100; ++t)
            {
                double sum_vSqrd = 0.0;
                for (j = 0; j < n; ++j)
                {
                    sum_vSqrd += _v[j] * _v[j];
                }
                sum_vSqrd += lambdaU;

                // Fix v, optimise u
                for (i = 0; i < m; ++i)
                {
                    double[] A_i = A[i];
                    double sum_Av = 0.0;
                    for (j = 0; j < n; ++j)
                    {
                        sum_Av += A_i[j] * _v[j];
                    }
                    _u[i] = sum_Av / sum_vSqrd;
                }

                double sum_uSqrd = 0.0;
                for (i = 0; i < m; ++i)
                {
                    sum_uSqrd += _u[i] * _u[i];
                }
                sum_uSqrd += lambdaV;

                // Fix u, optimise v
                for (j = 0; j < n; ++j)
                {
                    double sum_Au = 0.0;
                    for (i = 0; i < m; ++i)
                    {
                        sum_Au += A[i][j] * _u[i];
                    }
                    _v[j] = sum_Au / sum_uSqrd;
                }

                // As the computational complexity of such an operation is quite high relative to a single iteration
                // of ALS, we only check every few iterations 
                if (t % checkLossEvery == 0)
                {
                    double norm = CalculateL1Norm(_A, _u, _v);
                    if (norm < absoluteNormTerminationThreshold)
                    {
                        break;
                    }
                    if ((lastNormMeasurement - norm) < deltaNormTerminationThreshold)
                    {
                        break;
                    }
                    lastNormMeasurement = norm;
                }


                //Debug.WriteLine("iteration " + t + "\t" + A.Subtract(new DenseMatrix<double>(_u.OuterProduct(_v))).ElementwiseNorm(1, 1));
                //_u.OuterProduct(_v).Print();
            }

            u = new DenseVector<double>(_u);
            v = new DenseVector<double>(_v);
        }

        /// <summary>
        /// Calculates and returns | A - u^Tv | / (mn) where A is m x n. 
        /// Used to determine stopping criterion of ALS algorithm
        /// </summary>
        /// <param name="A"></param>
        /// <param name="u"></param>
        /// <param name="v"></param>
        private static double CalculateL1Norm(double[][] A, double[] u, double[] v)
        {
            double norm = 0.0;

            int m = u.Length, n = v.Length, i, j;
            for (i = 0; i < m; ++i)
            {
                double[] A_i = A[i];
                double u_i = u[i];
                for (j = 0; j < n; ++j)
                {
                    norm += Math.Abs(u_i * v[j] - A_i[j]);
                }
            }

            return norm / (m * n);
        }

    }
}
