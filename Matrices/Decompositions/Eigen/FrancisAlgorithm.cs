﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearLite.Global;
using LinearLite.Structs;

namespace LinearLite.Matrices.Decompositions
{
    internal class FrancisAlgorithm : IEigenvalueAlgorithm
    {
        internal override void CalculateEigenvalues(double[][] H, Complex[] eigenvalues)
        {
            int m = H.Length, i, j, k;

            // Special sizes - Francis' algorithm works for 3 x 3 and larger matrices
            if (m == 0)
            {
                return;
            }
            else if (m == 1)
            {
                eigenvalues[0] = H[0][0];
                return;
            }
            else if (m == 2)
            {
                calc_eigenvalues(H[0][0], H[0][1], H[1][0], H[1][1], out Complex z1, out Complex z2);
                eigenvalues[0] = z1;
                eigenvalues[1] = z2;
                return;
            }

            double[][] Q = MatrixInternalExtensions.JMatrix<double>(m, m);
            double[][] H_temp = MatrixInternalExtensions.JMatrix<double>(3, m);

            HessenbergExtensions.to_hessenberg_form(Q, H);

            double termination_threshold = 1e-60;
            int max_iterations_per_p_value = 10000;
            int p_iterations = 0;

            double[] x = new double[3], v = new double[3];
            double[][] P = MatrixInternalExtensions.JMatrix<double>(3, 3);  // stores the Householder transformation matrix
            double[][] G = MatrixInternalExtensions.JMatrix<double>(2, 2);  // stores the Givens rotation matrix

            int p = m - 1;
            while (p > 1)
            {
                int q = p - 1;

                double s = H[q][q] + H[p][p];
                double t = H[q][q] * H[p][p] - H[q][p] * H[p][q];

                // Compute the first 3 elements of M
                x[0] = H[0][0] * H[0][0] + H[0][1] * H[1][0] - s * H[0][0] + t;
                x[1] = H[1][0] * (H[0][0] + H[1][1] - s);
                x[2] = H[1][0] * H[2][1];

                for (k = 0; k <= p - 2; ++k)
                {
                    int r = Math.Max(0, k - 1);

                    Debug.WriteLine($"(p, k):\t({p},{k})");
                    if (x.Norm() < termination_threshold)
                    {
                        Debug.WriteLine("x: " + x.Norm());
                        x.Print();
                        Debug.WriteLine("v: " + v.Norm());
                        v.Print();
                        Debug.WriteLine("P");
                        P.ToRectangular().Print();
                        Debug.WriteLine("H");
                        H.ToRectangular().Print();
                        p--;
                        break;
                    }
                    
                    // Calculate 3 x 3 householder reflector matrix P, w.r.t. vector x
                    // P = I - 2 vv^T
                    PopulateHouseholderTransformationMatrix(x, v, P);

                    // H_temp[0 : 3][r : m] := P * H[k : k + 3][r : m]
                    for (i = 0; i < 3; ++i)
                    {
                        for (j = r; j < m; ++j)
                        {
                            H_temp[i][j] = P[i][0] * H[k][j] + P[i][1] * H[k + 1][j] + P[i][2] * H[k + 2][j];
                        }
                    }
                    // Copy H[k : k + 3][r : m] <- H_temp[0 : 3][r : m]
                    for (i = 0; i < 3; ++i)
                    {
                        for (j = r; j < m; ++j)
                        {
                            H[k + i][j] = H_temp[i][j];
                        }
                    }

                    r = Math.Min(k + 4, p) + 1;

                    // H_temp[0 : 3][0 : r] := H[0 : r][k : k + 3] * P 
                    // stored in transposed order 
                    for (i = 0; i < 3; ++i)
                    {
                        for (j = 0; j < r; ++j)
                        {
                            H_temp[i][j] = H[j][k] * P[0][i] + H[j][k + 1] * P[1][i] + H[j][k + 2] * P[2][i];
                        }
                    }
                    // Copy H[0 : r][k : k + 3] <- H_temp[0 : 3][0 : r]
                    for (i = 0; i < 3; ++i)
                    {
                        for (j = 0; j < r; ++j)
                        {
                            H[j][k + i] = H_temp[i][j];
                        }
                    }

                    x[0] = H[k + 1][k];
                    x[1] = H[k + 2][k];
                    if (k < p - 2)
                    {
                        x[2] = H[k + 3][k];
                    }
                    else
                    {
                        x[2] = 0;
                    }

                    Debug.WriteLine("H");
                    H.ToRectangular().Print();
                }

                // Givens rotation G such that G^T * [x y] = s * e1 
                PopulateGivensRotationMatrix(x, G);

                // G_temp[p - 1 : p + 1][p - 2 : m] := G^T * H[p - 1 : p + 1][p - 2 : m]
                double[][] G_temp = G.Multiply(H.Submatrix(p - 1, p - 2, 2, m - p + 2));
                // H[p - 1 : p + 1][p - 2 : m]
                for (i = 0; i < 2; ++i)
                {
                    for (j = p - 2; j < m; ++j)
                    {
                        H[q + i][j] = G_temp[i][j - p + 2];
                    }
                }

                // G_temp[0 : p + 1][p - 1: p + 1] := H[0 : p + 1][p - 1 : p + 1] * G
                G_temp = H.Submatrix(0, p - 1, p + 1, 2).Multiply(G.Transpose());
                // H[0 : p + 1][p - 1 : p + 1] := G_temp[0 : p + 1][p - 1 : p + 1]
                for (i = 0; i < 2; ++i)
                {
                    for (j = 0; j <= p; ++j)
                    {
                        H[j][p - 1 + i] = G_temp[j][i];
                    }
                }

                // Convergence checks
                if (Math.Abs(H[p][q]) < termination_threshold * (Math.Abs(H[q][q]) + Math.Abs(H[p][p])))
                {
                    H[p][q] = 0;
                    H.ToRectangular().Print();
                    p--;

                    Debug.WriteLine(p_iterations);
                    p_iterations = 0;
                }
                else if (Math.Abs(H[p - 1][q - 1]) < termination_threshold * (Math.Abs(H[q - 1][q - 1]) + Math.Abs(H[q][q])))
                {
                    H[p - 1][q - 1] = 0;
                    p -= 2;

                    Debug.WriteLine(p_iterations);
                    p_iterations = 0;
                }
                p_iterations++;
            }

            // Collect the eigenvalues from the real-schur matrix
            for (p = 1; p < m; ++p)
            {
                int q = p - 1;
                if (Math.Abs(H[p][q]) > Precision.DOUBLE_PRECISION)
                {
                    calc_eigenvalues(H[q][q], H[q][p], H[p][q], H[p][p], out Complex l1, out Complex l2);

                    eigenvalues[q] = l1;
                    eigenvalues[p] = l2;

                    p++;
                }
                else
                {
                    // Single eigenvalue case
                    eigenvalues[q] = H[q][q];
                    if (p == m - 1)
                    {
                        Debug.WriteLine("Adding last:  " + H[p][p]);
                        eigenvalues[p] = H[p][p];
                    }
                }
            }
        }

        /// <summary>
        /// P reflects point x -> e1
        /// Dimensions are P[3 x 3] and x, v length 3
        /// </summary>
        /// <param name="x"></param>
        /// <param name="v"></param>
        /// <param name="P"></param>
        private static void PopulateHouseholderTransformationMatrix(double[] x, double[] v, double[][] P)
        {
            int i;
            for (i = 0; i < 3; ++i)
            {
                v[i] = x[i];
            }
            v[0] += Math.Sign(v[0]) * v.Norm();

            double factor = Constants.SQRT_2 / v.Norm();
            for (i = 0; i < 3; ++i)
            {
                v[i] *= factor;
            }

            double v1 = v[0], v2 = v[1], v3 = v[2];

            double[] P1 = P[0];
            P1[0] = 1.0 - v1 * v1;
            P1[1] = -v1 * v2;
            P1[2] = -v1 * v3;

            double[] P2 = P[1];
            P2[0] = -v2 * v1;
            P2[1] = 1.0 - v2 * v2;
            P2[2] = -v2 * v3;

            double[] P3 = P[2];
            P3[0] = -v3 * v1;
            P3[1] = -v3 * v2;
            P3[2] = 1.0 - v3 * v3;
        }
        /// <summary>
        /// G = 2 x 2 Givens rotation matrix
        /// x is a length 3 vector, however the Givens rotation is only applied to the first 2 elements of x (x, y)
        /// </summary>
        /// <param name="x"></param>
        /// <param name="G"></param>
        private static void PopulateGivensRotationMatrix(double[] x, double[][] G)
        {
            double a = x[0], b = x[1];

            // Just return the identity - x[1] is already 0
            if (b == 0)
            {
                G[0][0] = 1.0;
                G[0][1] = 0.0;
                G[1][0] = 0.0;
                G[1][1] = 1.0;
            }

            // TODO: remove numerical underflow problem using Hypot
            double r = Math.Sqrt(a * a + b * b);
            double c = a / r, s = -b / r;
            G[0][0] = c;
            G[0][1] = -s;
            G[1][0] = s;
            G[1][1] = c;
        }


        internal override void CalculateEigenvalues(float[][] A, Complex[] eigenvalues)
        {
            throw new NotImplementedException();
        }

        internal override void CalculateEigenvalues(decimal[][] A, Complex[] eigenvalues)
        {
            throw new NotImplementedException();
        }

        internal override void CalculateEigenvalues(Complex[][] A, Complex[] eigenvalues)
        {
            throw new NotImplementedException();
        }
    }
}
