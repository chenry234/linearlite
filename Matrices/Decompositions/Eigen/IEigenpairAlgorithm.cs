﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Decompositions
{
    internal interface IEigenpairAlgorithm
    {
        List<EigenPair> CalculateEigenpairs(Complex[][] A);
    }
}
