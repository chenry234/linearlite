﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Decompositions
{
    internal abstract class IEigenvalueAlgorithm
    {
        internal Complex[] CalculateEigenvalues(double[,] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            Complex[] eigenvalues = new Complex[A.GetLength(0)];
            CalculateEigenvalues(A.ToJagged(), eigenvalues);
            return eigenvalues;
        }
        internal Complex[] CalculateEigenvalues(float[,] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            Complex[] eigenvalues = new Complex[A.GetLength(0)];
            CalculateEigenvalues(A.ToJagged(), eigenvalues);
            return eigenvalues;
        }
        internal Complex[] CalculateEigenvalues(decimal[,] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            Complex[] eigenvalues = new Complex[A.GetLength(0)];
            CalculateEigenvalues(A.ToJagged(), eigenvalues);
            return eigenvalues;
        }
        internal Complex[] CalculateEigenvalues(Complex[,] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            Complex[] eigenvalues = new Complex[A.GetLength(0)];
            CalculateEigenvalues(A.ToJagged(), eigenvalues);
            return eigenvalues;
        }

        #region Unsafe, efficient methods 

        internal abstract void CalculateEigenvalues(double[][] A, Complex[] eigenvalues);
        internal abstract void CalculateEigenvalues(float[][] A, Complex[] eigenvalues);
        internal abstract void CalculateEigenvalues(decimal[][] A, Complex[] eigenvalues);
        internal abstract void CalculateEigenvalues(Complex[][] A, Complex[] eigenvalues);

        #endregion

        #region Helper methods 

        /// <summary>
        /// Calculate the eigenvalues from a deflated jagged matrix _A
        /// </summary>
        /// <param name="_A"></param>
        /// <returns></returns>
        protected static List<Complex> calc_eigenvalues(double[][] _A)
        {
            int m = _A.Length;

            List<Complex> eigenvalues = new List<Complex>();
            for (int r = 1; r <= m; ++r)
            {
                // Since we are using QR decomp, we check the lower off-diagonal terms for non-zero
                if (r < m && !Util.ApproximatelyEquals(_A[r][r - 1], 0, 1e-5))
                {
                    // A block diagonal exists (r, c, 1, 1)
                    calc_eigenvalues(_A[r - 1][r - 1], _A[r - 1][r], _A[r][r - 1], _A[r][r], out Complex l1, out Complex l2);
                    eigenvalues.Add(l1);
                    eigenvalues.Add(l2);
                    r++;
                }
                else
                {
                    eigenvalues.Add(new Complex(_A[r - 1][r - 1]));
                }
            }
            return eigenvalues;
        }
        /// <summary>
        /// Calculate the L2 stopping criterion
        /// </summary>
        /// <param name="z"></param>
        /// <param name="w"></param>
        /// <returns></returns>
        protected static double calc_diff(List<Complex> a, List<Complex> b)
        {
            if (a.Count != b.Count) throw new InvalidOperationException();
            int len = a.Count, i;
            double L2 = 0.0;
            for (i = 0; i < len; ++i)
            {
                double re = a[i].Real - b[i].Real, im = a[i].Imaginary - b[i].Imaginary;
                L2 += re * re + im * im;
            }
            return L2;
        }

        /// <summary>
        /// This is not the same as Wilkinson's shift, since it is being applied to a non-symmetric submatrix
        /// Simply finds the analytic eigenvalue of the 2 x 2 submatrix (a b c d) that is closest to d
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        protected static Complex calc_wilkinson_shift(double a, double b, double c, double d)
        {
            calc_eigenvalues(a, b, c, d, out Complex cand1, out Complex cand2);
            if ((d - cand1).Modulus() > (d - cand2).Modulus()) return cand2;
            return cand1;
        }
        protected static Complex calc_wilkinson_shift(Complex a, Complex b, Complex c, Complex d)
        {
            calc_eigenvalues(a, b, c, d, out Complex cand1, out Complex cand2);
            if ((d - cand1).Modulus() > (d - cand2).Modulus()) return cand2;
            return cand1;
        }

        /// <summary>
        /// Calculate a conjugate eigenvalue pair from a real block matrix 
        /// |a b|
        /// |c d|
        /// </summary>
        /// <param name="a">A[1, 1]</param>
        /// <param name="b">A[1, 2]</param>
        /// <param name="c">A[2, 1]</param>
        /// <param name="d">A[2, 2]</param>
        /// <returns></returns>
        protected static void calc_eigenvalues(double a, double b, double c, double d, out Complex lambdaPlus, out Complex lambdaMinus)
        {
            double tr = a + d;
            double re = tr / 2;
            double im = Math.Sqrt(Math.Max(0, 4 * (a * d - b * c) - tr * tr)) / 2;
            lambdaPlus = new Complex(re, im);
            lambdaMinus = new Complex(re, -im);
        }
        /// <summary>
        /// Calculate a conjugate eigenvalue pair from a complex block matrix 
        /// |a b|
        /// |c d|
        /// </summary>
        /// <param name="a">A[1, 1]</param>
        /// <param name="b">A[1, 2]</param>
        /// <param name="c">A[2, 1]</param>
        /// <param name="d">A[2, 2]</param>
        /// <returns></returns>
        protected static void calc_eigenvalues(Complex a, Complex b, Complex c, Complex d, out Complex lambdaPlus, out Complex lambdaMinus)
        {
            Complex trace = a.Add(d);
            Complex det = a.Multiply(d).Subtract(b.Multiply(c));
            Complex determinant = Complex.Sqrt(trace.Multiply(trace).Subtract(det.Multiply(4)));
            lambdaPlus = trace.Add(determinant) / 2;
            lambdaMinus = trace.Subtract(determinant) / 2;
        }

        #endregion
    }
    
}
