﻿using LinearLite.Decomposition;
using LinearLite.Matrices.Decompositions.QR;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Decompositions
{
    internal class ShiftedQRHessenbergAlgorithm : IEigenvalueAlgorithm, IEigenpairAlgorithm
    {
        internal override void CalculateEigenvalues(double[][] A, Complex[] eigenvalues)
        {
            CalculateEigenvalues(A.Convert(a => new Complex(a, 0)), eigenvalues);
        }

        internal override void CalculateEigenvalues(float[][] A, Complex[] eigenvalues)
        {
            CalculateEigenvalues(A.Convert(a => new Complex(a, 0)), eigenvalues);
        }

        internal override void CalculateEigenvalues(decimal[][] A, Complex[] eigenvalues)
        {
            CalculateEigenvalues(A.Convert(a => new Complex((double)a, 0)), eigenvalues);
        }

        internal override void CalculateEigenvalues(Complex[][] H, Complex[] eigenvalues)
        {
            int max_iterations = 20;
            double termination_threshold = 1e-120;

            int m = H.Length, n = H[0].Length, i, j, k;
            Complex[][] Q = MatrixInternalExtensions.JMatrix<Complex>(m, m);

            HessenbergExtensions.to_hessenberg_form(Q, H);

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();

            for (i = n - 1; i >= 1; i--)
            {
                int prev = i - 1;

                Complex e1 = H[i][i], e2 = H[prev][prev];
                for (j = 0; j < max_iterations; ++j)
                {
                    // We shift the H matrix by mu * I where mu is the eigenvalue of the 2 x 2 submatrix 
                    // in the lower right corner of H, that is closest to H[i][i]. This is similar to 
                    // Rayleigh iteration that (under regularity conditions) guarantees quadratic convergence 
                    // of the off-diagonal term H[i][prev] (as opposed to linear convergence in the naive
                    // QR algorithm). 
                    Complex shift = calc_wilkinson_shift(H[prev][prev], H[prev][i], H[i][prev], H[i][i]);

                    // Apply spectral shift
                    for (k = 0; k <= i; ++k)
                    {
                        H[k][k].DecrementBy(shift);
                    }

                    // Perform QR decomposition on the (assumed) Hessenberg matrix H, generating unitary 
                    // matrix Q. As we are not storing the eigenvectors, Q is overwritten each time (resetQ = true)
                    qr.QRDecomposeHessenberg(Q, H, true);

                    // Under certain (rare) conditions, H experiences numerical instability. We perform a 
                    // simple check here to ensure that H is valid 
                    if (Complex.IsNaNOrInfinity(H[i][i]) || Complex.IsNaNOrInfinity(H[prev][prev]))
                    {
                        break;
                    }

                    // H_new := H * Q, we set H <- H_new once we verify that H_new is valid.
                    // Note - this step is O(n^3) (!!) which is why this implementation is O(n^4) 
                    // TODO: find a way to implicitly perform this step with lower complexity
                    Complex[][] H_new = H.Multiply(Q);

                    // Undo spectral shift
                    for (k = 0; k <= i; ++k)
                    {
                        H_new[k][k].IncrementBy(shift);
                    }

                    // We again check for numerical instability, since H_new := H * Q may introduce unexpected 
                    // results 
                    if (Complex.IsNaNOrInfinity(H_new[i][i]) || Complex.IsNaNOrInfinity(H_new[prev][prev]))
                    {
                        break;
                    }

                    // Set variables for the next iteration
                    e1 = H_new[i][i];
                    e2 = H_new[prev][prev];
                    H = H_new;

                    // Check termination conditions
                    if (H[i][prev].Modulus() < termination_threshold)
                    {
                        break;
                    }
                }

                if (i == 1)
                {
                    eigenvalues[1] = e1;
                    eigenvalues[0] = e2;
                }
                else
                {
                    eigenvalues[i] = e1;

                    // Deflate both matrices
                    H = H.Submatrix(0, 0, i, i);
                    Q = Q.Submatrix(0, 0, i, i);
                }
            }
        }
        public List<EigenPair> CalculateEigenpairs(Complex[][] H)
        {
            int max_iterations = 20;
            double termination_threshold = 1e-60;

            int m = H.Length, n = H[0].Length, i, j, k;
            Complex[][] Q = MatrixInternalExtensions.JMatrix<Complex>(m, m);

            HessenbergExtensions.to_hessenberg_form(Q, H);
            List<EigenPair> eigenPairs = new List<EigenPair>();

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            for (i = n - 1; i >= 1; i--)
            {
                int prev = i - 1;

                Complex e1 = H[i][i], e0 = H[prev][prev];
                for (j = 0; j < max_iterations; ++j)
                {
                    // We shift the H matrix by mu * I where mu is the eigenvalue of the 2 x 2 submatrix 
                    // in the lower right corner of H, that is closest to H[i][i]. This is similar to 
                    // Rayleigh iteration that (under regularity conditions) guarantees quadratic convergence 
                    // of the off-diagonal term H[i][prev] (as opposed to linear convergence in the naive
                    // QR algorithm). 
                    Complex shift = calc_wilkinson_shift(H[prev][prev], H[prev][i], H[i][prev], H[i][i]);

                    // Apply spectral shift
                    for (k = 0; k <= i; ++k)
                    {
                        H[k][k].DecrementBy(shift);
                    }

                    // Perform QR decomposition on the (assumed) Hessenberg matrix H, generating unitary 
                    // matrix Q. As we are not storing the eigenvectors, Q is overwritten each time (resetQ = true)
                    qr.QRDecomposeHessenberg(Q, H, false, false);

                    // Under certain (rare) conditions, H experiences numerical instability. We perform a 
                    // simple check here to ensure that H is valid 
                    if (Complex.IsNaNOrInfinity(H[i][i]) || Complex.IsNaNOrInfinity(H[prev][prev]))
                    {
                        break;
                    }

                    // H_new := H * Q, we set H <- H_new once we verify that H_new is valid.
                    // Note - this step is O(n^3) (!!) which is why this implementation is O(n^4) 
                    // TODO: find a way to implicitly perform this step with lower complexity
                    Complex[][] H_new = H.Multiply(Q);

                    // Undo spectral shift
                    for (k = 0; k <= i; ++k)
                    {
                        H_new[k][k].IncrementBy(shift);
                    }

                    // We again check for numerical instability, since H_new := H * Q may introduce unexpected 
                    // results 
                    if (Complex.IsNaNOrInfinity(H_new[i][i]) || Complex.IsNaNOrInfinity(H_new[prev][prev]))
                    {
                        break;
                    }

                    // Set variables for the next iteration
                    e1 = H_new[i][i];
                    e0 = H_new[prev][prev];
                    H = H_new;

                    // Check termination conditions
                    if (H[i][prev].Modulus() < termination_threshold)
                    {
                        break;
                    }
                }

                if (i == 1)
                {
                    eigenPairs.Add(new EigenPair(e1, H.Column(1)));
                    eigenPairs.Add(new EigenPair(e0, H.Column(0)));
                }
                else
                {
                    eigenPairs.Add(new EigenPair(e1, H.Column(1)));

                    // Deflate both matrices
                    H = H.Submatrix(0, 0, i, i);
                    Q = Q.Submatrix(0, 0, i, i);
                }
            }
            return eigenPairs;
        }

    }
}
