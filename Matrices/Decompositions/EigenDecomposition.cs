﻿using LinearLite.Global;
using LinearLite.Matrices;
using LinearLite.Matrices.Decompositions;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Decomposition
{
    public static class EigenDecomposition
    {

        /// <summary>
        /// Calculates the eigendecomposition of matrix A into a diagonal matrix of eigenvalues (D) and a eigenvector matrix (V)
        /// such that A = VDV^-1.
        /// Only diagonalisable matrices can be decomposed in such a manner. An ArgumentException will be thrown if matrix 
        /// is not diagonalisable. Uses QR iteration algorithm.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="eigenvalueMatrix"></param>
        /// <param name="eigenvectorMatrix"></param>
        public static void EigenDecompose(this double[,] A, out Complex[,] D, out Complex[,] V)
        {
            // Implementation previously used the Naive QR algorithm
            // but it was not performant enough to be used. 
            // TODO: find better eigenvector algorithm to implement this 
            // decomposition ... 

            throw new NotImplementedException();
        }

        public static Complex[] Eigenvalues(this double[,] A, bool sort = false, EigenvalueAlgorithm method = EigenvalueAlgorithm.FRANCIS_ALGORITHM)
        {
            IEigenvalueAlgorithm algorithm = GetAlgorithm(method);
            Complex[] eigenvalues = algorithm.CalculateEigenvalues(A);
            if (sort)
            {
                Sort(eigenvalues);
            }
            return eigenvalues;
        }
        public static Complex[] Eigenvalues(this float[,] A, bool sort = false, EigenvalueAlgorithm method = EigenvalueAlgorithm.FRANCIS_ALGORITHM)
        {
            IEigenvalueAlgorithm algorithm = GetAlgorithm(method);
            Complex[] eigenvalues = algorithm.CalculateEigenvalues(A);
            if (sort)
            {
                Sort(eigenvalues);
            }
            return eigenvalues;
        }
        public static Complex[] Eigenvalues(this decimal[,] A, bool sort = false, EigenvalueAlgorithm method = EigenvalueAlgorithm.FRANCIS_ALGORITHM)
        {
            IEigenvalueAlgorithm algorithm = GetAlgorithm(method);
            Complex[] eigenvalues = algorithm.CalculateEigenvalues(A);
            if (sort)
            {
                Sort(eigenvalues);
            }
            return eigenvalues;
        }
        public static Complex[] Eigenvalues(this Complex[,] A, bool sort = false, EigenvalueAlgorithm method = EigenvalueAlgorithm.FRANCIS_ALGORITHM)
        {
            IEigenvalueAlgorithm algorithm = GetAlgorithm(method);
            Complex[] eigenvalues = algorithm.CalculateEigenvalues(A);
            if (sort)
            {
                Sort(eigenvalues);
            }
            return eigenvalues;
        }

        /// <summary>
        /// Sort the eigenvalues of a matrix using a norm comparator
        /// </summary>
        /// <param name="eigenvalues"></param>
        private static void Sort(Complex[] eigenvalues)
        {
            // Precalculate and cache norms
            List<Tuple<Complex, double>> eigenvalueNormPairs = new List<Tuple<Complex, double>>();
            foreach (Complex e in eigenvalues)
            {
                eigenvalueNormPairs.Add(new Tuple<Complex, double>(e, e.Modulus()));
            }
            eigenvalueNormPairs.Sort((a, b) =>
            {
                if (double.IsNaN(a.Item2))
                {
                    if (double.IsNaN(b.Item2))
                    {
                        return 0;
                    }
                    return 1;
                }
                else if (double.IsNaN(b.Item2))
                {
                    return -1;
                }
                return -a.Item2.CompareTo(b.Item2);
            });

            // Copy back into the eigenvalues vector;
            int len = eigenvalues.Length, i;
            for (i = 0; i < len; ++i)
            {
                eigenvalues[i] = eigenvalueNormPairs[i].Item1;
            }
        }

        private static IEigenvalueAlgorithm GetAlgorithm(EigenvalueAlgorithm method)
        {
            switch (method)
            {
                case EigenvalueAlgorithm.DOUBLE_SHIFTED_QR_ALGORITHM: return new NaiveDoubleShiftAlgorithm();
                case EigenvalueAlgorithm.FRANCIS_ALGORITHM: return new FrancisAlgorithm();
                case EigenvalueAlgorithm.SHIFTED_QR_ALGORITHM: return new ShiftedQRHessenbergAlgorithm();
                case EigenvalueAlgorithm.NAIVE_QR_ALGORITHM: return new QRAlgorithm();

                default:
                    throw new NotImplementedException();
            }
        }

    }
}
