﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Decomposition
{
    /// <summary>
    /// Decomposes a square matrix A as A = S + N, where 
    /// 1) S is a diagonalisable matrix, and 
    /// 2) N is a nilpotent matrix, and 
    /// 3) S and N commute, i.e. SN = NS
    /// 
    /// Useful for computation of the matrix exponential in the general case. 
    /// </summary>
    public static class JordanChevalleyDecomposition
    {
        /// <summary>
        /// Decomposes a real square matrix A as A = S + N where S is a diagonalisable square matrix
        /// and N is a nilpotent matrix, such that S and N commute.
        /// </summary>
        /// <param name="A">The matrix to be decomposed. Must be square and real.</param>
        /// <param name="S"></param>
        /// <param name="N"></param>
        public static void JordanChevalleyDecompose(this double[,] A, out double[,] S, out double[,] N)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            throw new NotImplementedException();
        }
    }
}
