﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearLite.BLAS;
using LinearLite.Matrices.Strassen;
using LinearLite.Structs;

namespace LinearLite.Matrices.Decompositions.LU
{
    internal class CamareroBlockLUDecomposition : IDenseLUDecompositionAlgorithm
    {
        internal override void LUDecompose(float[][] A)
        {
            throw new NotImplementedException();
        }
        internal override void LUDecompose(double[][] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int p = 4, z = 0;
            int n = A.Length, i, j, k;

            double[][] RL_workspace = MatrixInternalExtensions.JMatrix<double>(n, p);
            double[][] RU_workspace = MatrixInternalExtensions.JMatrix<double>(p, n);
            double[][] prod_workspace = MatrixInternalExtensions.JMatrix<double>(n, n);

            double[][] L = MatrixInternalExtensions.JMatrix<double>(n, n);
            double[][] U = MatrixInternalExtensions.JMatrix<double>(n, n);

            var ins = new DoubleStrassenInstructionSet();
            var strassen = new IndefiniteSizeRectStrassenMultiplication<double>(ins, n - p, p, n - p, true, 2);
            for (j = 0; j < n; ++j)
            {
                if (j == z + p)
                {
                    Debug.WriteLine(j + " ==================================================");
                    // Copy RL = A[j : n][z : j - 1]
                    RL_workspace.CopyFrom(A, j, z, j, 0, n - j, p);
                    // Copy RU = A[z : c][j : n]
                    RU_workspace.CopyFrom(A, z, j, 0, j, p, n - j);

                    Debug.WriteLine("RL");
                    RL_workspace.ToRectangular().Print();
                    Debug.WriteLine("RU");
                    RU_workspace.ToRectangular().Print();

                    strassen.Multiply(
                        RL_workspace, RU_workspace, prod_workspace,
                        j, 0,
                        0, j,
                        j, j,
                        n - j, p, n - j, false);

                    Debug.WriteLine("Prod workspace");
                    prod_workspace.ToRectangular().Print();

                    // A[i, j] <- A[i, j] - S[i, j]
                    for (i = j; i < n; ++i)
                    {
                        double[] A_i = A[i], S_i = prod_workspace[i];
                        for (k = j; k < n; ++k)
                        {
                            A_i[k] -= S_i[k];
                        }
                    }

                    Debug.WriteLine("A");
                    A.ToRectangular().Print();

                    z = j;
                }

                for (i = j; i < n; ++i)
                {
                    L[i][j] = A[i][j];
                    for (k = z; k < j; ++k)
                    {
                        L[i][j] -= L[i][k] * U[k][j];
                    }
                }

                for (i = j; i < n; ++i)
                {
                    U[j][i] = A[j][i];
                    for (k = z; k < j; ++k)
                    {
                        U[j][i] -= L[j][k] * U[k][i];
                    }
                    U[j][i] /= L[j][j];
                }
            }
        }
        internal override void LUDecompose(decimal[][] A)
        {
            throw new NotImplementedException();
        }
        internal override void LUDecompose(Complex[][] A)
        {
            throw new NotImplementedException();
        }

        internal override void LUPDecompose(float[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
        internal override void LUPDecompose(double[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
        internal override void LUPDecompose(decimal[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
        internal override void LUPDecompose(Complex[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }

        internal override void LUDecomposeParallel(float[][] A)
        {
            throw new NotImplementedException();
        }
        internal override void LUDecomposeParallel(double[][] A)
        {
            throw new NotImplementedException();
        }
        internal override void LUDecomposeParallel(decimal[][] A)
        {
            throw new NotImplementedException();
        }
        internal override void LUDecomposeParallel(Complex[][] A)
        {
            throw new NotImplementedException();
        }

        internal override void LUPDecomposeParallel(float[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
        internal override void LUPDecomposeParallel(double[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
        internal override void LUPDecomposeParallel(decimal[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
        internal override void LUPDecomposeParallel(Complex[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
    }
}
