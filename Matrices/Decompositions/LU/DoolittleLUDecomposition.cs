﻿using LinearLite.BLAS;
using LinearLite.Structs;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Decompositions.LU
{
    internal class DoolittleLUDecomposition : IDenseLUDecompositionAlgorithm
    {
        internal void LUDecompose<T>(T[][] A, IBLAS<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int dim = A.Length, i, j, c;
            T[] column = new T[dim];
            for (i = 0; i < dim; i++)
            {
                // The upper triangular matrix U is stored in the upper triangular portion of A.
                T[] A_i = A[i];
                for (c = 0; c < i; ++c)
                {
                    blas.YSAX(A_i, A[c], A_i[c], i, dim);
                }

                for (j = 0; j < i; ++j)
                {
                    column[j] = A[j][i];
                }

                // The lower triangular matrix L is stored in the lower triangular portion of A.
                //T A_ii = A_i[i];
                T factor = blas.Divide(blas.One, A_i[i]);
                for (j = i + 1; j < dim; ++j)
                {
                    T[] A_j = A[j];
                    T sum = blas.DOT(A_j, column, 0, i);
                    A_j[i] = blas.Multiply(blas.Subtract(A_j[i], sum), factor);
                }
            }
        }
        internal override void LUDecompose(float[][] A) => LUDecompose(A, new FloatBLAS());
        internal override void LUDecompose(double[][] A) => LUDecompose(A, new DoubleBLAS());
        internal override void LUDecompose(decimal[][] A) => LUDecompose(A, new DecimalBLAS());
        internal override void LUDecompose(Complex[][] A) => LUDecompose(A, new ComplexBLAS());

        private void LUPDecompose<T>(T[][] A, int[] P, IBLAS<T> blas, Func<T, double> Norm) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            if (P.Length < A.Length)
            {
                throw new InvalidOperationException();
            }

            int dim = A.Length, i, j, r, c;
            for (i = 0; i < dim; ++i)
            {
                P[i] = i;
            }

            for (i = 0; i < dim; i++)
            {
                // Pivot ------------------------
                double max_norm = 0.0;
                int best_row = i;

                for (r = i; r < dim; r++)
                {
                    T[] A_r = A[r];
                    T diag = blas.Subtract(A_r[i], blas.AMULT(A_r, A, r, 0, i));

                    double norm = Norm(diag);
                    if (norm > max_norm)
                    {
                        max_norm = norm;
                        best_row = r;
                    }
                }

                if (i != best_row)
                {
                    // Swap rows in permutation and U matrix
                    int tmp_p = P[i];
                    P[i] = P[best_row];
                    P[best_row] = tmp_p;

                    T[] tmp = A[i];
                    A[i] = A[best_row];
                    A[best_row] = tmp;
                }

                // The upper triangular matrix U is stored in the upper triangular portion of A.
                T[] A_i = A[i];
                for (c = 0; c < i; ++c)
                {
                    blas.YSAX(A_i, A[c], A_i[c], i, dim);
                }

                // The lower triangular matrix L is stored in the lower triangular portion of A.
                //T A_ii = A_i[i];
                T factor = blas.Divide(blas.One, A_i[i]);
                for (j = i + 1; j < dim; ++j)
                {
                    T[] A_j = A[j];
                    T sum = blas.AMULT(A_j, A, i, 0, i);
                    A_j[i] = blas.Multiply(blas.Subtract(A_j[i], sum), factor);
                }
            }
        }
        internal override void LUPDecompose(float[][] A, int[] permutations) => LUPDecompose(A, permutations, new FloatBLAS(), x => Math.Abs(x));
        internal override void LUPDecompose(double[][] A, int[] permutations) => LUPDecompose(A, permutations, new DoubleBLAS(), x => Math.Abs(x));
        internal override void LUPDecompose(decimal[][] A, int[] permutations) => LUPDecompose(A, permutations, new DecimalBLAS(), x => (double)Math.Abs(x));
        internal override void LUPDecompose(Complex[][] A, int[] permutations) => LUPDecompose(A, permutations, new ComplexBLAS(), x => x.Modulus());

        // This naive parallelization attempt does not work, 
        // we need to access L, U in a different order
        private void LUDecomposeParallel<T>(T[][] A, IBLAS<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int dim = A.Length, i;
            T[] column = new T[dim];
            for (i = 0; i < dim; i++)
            {
                // The upper triangular matrix U is stored in the upper triangular portion of A.
                T[] A_i = A[i];
                if (i > 0)
                {
                    Parallel.ForEach(Partitioner.Create(0, i), range =>
                    {
                        int min = range.Item1, max = range.Item2, c;
                        for (c = min; c < max; ++c)
                        {
                            blas.YSAX(A_i, A[c], A_i[c], i, dim);
                        }
                    });
                }

                for (int j = 0; j < i; ++j)
                {
                    column[j] = A[j][i];
                }

                // The lower triangular matrix L is stored in the lower triangular portion of A.
                //T A_ii = A_i[i];
                if (i < dim - 1)
                {
                    T factor = blas.Divide(blas.One, A_i[i]);
                    for (int j = i + 1; j < dim; ++j)
                    {
                        T[] A_j = A[j];
                        T sum = blas.DOT(A_j, column, 0, i);
                        A_j[i] = blas.Multiply(blas.Subtract(A_j[i], sum), factor);
                    }
                }
            }
        }
        internal override void LUDecomposeParallel(float[][] A) => LUDecomposeParallel(A, new FloatBLAS());
        internal override void LUDecomposeParallel(double[][] A) => LUDecomposeParallel(A, new DoubleBLAS());
        internal override void LUDecomposeParallel(decimal[][] A) => LUDecomposeParallel(A, new DecimalBLAS());
        internal override void LUDecomposeParallel(Complex[][] A) => LUDecomposeParallel(A, new ComplexBLAS());

        internal override void LUPDecomposeParallel(float[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
        internal override void LUPDecomposeParallel(double[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
        internal override void LUPDecomposeParallel(decimal[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
        internal override void LUPDecomposeParallel(Complex[][] A, int[] permutations)
        {
            throw new NotImplementedException();
        }
    }
}
