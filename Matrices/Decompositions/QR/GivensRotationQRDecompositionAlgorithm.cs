﻿using LinearLite.BLAS;
using LinearLite.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Decompositions.QR
{
    internal class GivensRotationQRDecompositionAlgorithm : IDenseQRDecompositionAlgorithm
    {
        protected override void QR_decompose_inner(float[][] Q, float[][] R)
        {
            qr_decompose_givens_rotate(Q, R, x => Util.approx_zero(x, Precision.FLOAT_PRECISION * Precision.FLOAT_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }
        protected override void QR_decompose_inner(double[][] Q, double[][] R)
        {
            qr_decompose_givens_rotate(Q, R, x => Util.approx_zero(x, Precision.DOUBLE_PRECISION * Precision.DOUBLE_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }
        protected override void QR_decompose_inner(decimal[][] Q, decimal[][] R)
        {
            qr_decompose_givens_rotate(Q, R, x => Util.approx_zero(x, Precision.DECIMAL_PRECISION * Precision.DECIMAL_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }
        protected override void QR_decompose_inner(Complex[][] Q, Complex[][] R)
        {
            qr_decompose_givens_rotate(Q, R, x => Util.approx_zero(x, Precision.DOUBLE_PRECISION * Precision.DOUBLE_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }

        protected override void QR_decompose_inner_parallel(float[][] Q, float[][] R)
        {
            qr_decompose_givens_rotate_parallel(Q, R, x => Util.approx_zero(x, Precision.FLOAT_PRECISION * Precision.FLOAT_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }
        protected override void QR_decompose_inner_parallel(double[][] Q, double[][] R)
        {
            qr_decompose_givens_rotate_parallel(Q, R, x => Util.approx_zero(x, Precision.DOUBLE_PRECISION * Precision.DOUBLE_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }
        protected override void QR_decompose_inner_parallel(decimal[][] Q, decimal[][] R)
        {
            qr_decompose_givens_rotate_parallel(Q, R, x => Util.approx_zero(x, Precision.DECIMAL_PRECISION * Precision.DECIMAL_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }
        protected override void QR_decompose_inner_parallel(Complex[][] Q, Complex[][] R)
        {
            qr_decompose_givens_rotate_parallel(Q, R, x => Util.approx_zero(x, Precision.DOUBLE_PRECISION * Precision.DOUBLE_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }

        protected override void QL_decompose_inner(float[][] Q, float[][] L)
        {
            ql_decompose_givens_rotate(Q, L, x => Util.approx_zero(x, Precision.FLOAT_PRECISION * Precision.FLOAT_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }
        protected override void QL_decompose_inner(double[][] Q, double[][] L)
        {
            ql_decompose_givens_rotate(Q, L, x => Util.approx_zero(x, Precision.DOUBLE_PRECISION * Precision.DOUBLE_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }
        protected override void QL_decompose_inner(decimal[][] Q, decimal[][] L)
        {
            ql_decompose_givens_rotate(Q, L, x => Util.approx_zero(x, Precision.DECIMAL_PRECISION * Precision.DECIMAL_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }
        protected override void QL_decompose_inner(Complex[][] Q, Complex[][] L)
        {
            ql_decompose_givens_rotate(Q, L, x => Util.approx_zero(x, Precision.DOUBLE_PRECISION * Precision.DOUBLE_PRECISION), Util.Givens, Givens.RotateRows, Givens.RotateColumns);
        }

        protected override void QL_decompose_inner_parallel(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(double[][] Q, double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QL_decompose_inner_parallel(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_inner(float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_inner(Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_hessenberg_unsafe(float[][] Q, float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(double[][] Q, double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(decimal[][] Q, decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(Complex[][] Q, Complex[][] R)
        {
            throw new NotImplementedException();
        }

        protected override void QR_decompose_hessenberg_unsafe(float[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(double[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(decimal[][] R)
        {
            throw new NotImplementedException();
        }
        protected override void QR_decompose_hessenberg_unsafe(Complex[][] R)
        {
            throw new NotImplementedException();
        }


        private static void qr_decompose_givens_rotate<T>(T[][] Q, T[][] R,
            Func<T, bool> IsZero,
            Action<T, T, T[]> Givens,
            Action<T[][], int, int, T, T, int, int> GivensRotateRight,
            Action<T[][], int, int, T, T, int, int> GivensRotateLeft) where T : new()
        {
            int rows = R.Length, columns = R[0].Length, i, j;

            //int count = 0;
            T[] cs = new T[2];
            for (j = 0; j < columns; ++j)
            {
                for (i = j + 1; i < rows; ++i)
                {
                    // Take advantage of sparsity
                    if (IsZero(R[i][j]))
                    {
                        //count++;
                        continue;
                    }

                    Givens(R[j][j], R[i][j], cs);
                    GivensRotateRight(R, j, i, cs[0], cs[1], j, columns);
                    GivensRotateLeft(Q, j, i, cs[0], cs[1], 0, rows);
                }
            }
            //Debug.WriteLine(count);
        }

        private static void qr_decompose_givens_rotate_parallel<T>(T[][] Q, T[][] R,
          Func<T, bool> IsZero,
          Action<T, T, T[]> Givens,
          Action<T[][], int, int, T, T, int, int> GivensRotateRight,
          Action<T[][], int, int, T, T, int, int> GivensRotateLeft) where T : new()
        {
            int rows = R.Length, columns = R[0].Length, j;
            int parallelization_threshold = 8;

            for (j = 0; j < columns; ++j)
            {
                int blockSize = 2, step, tasks = rows - j;
                while (tasks / blockSize >= parallelization_threshold)
                {
                    step = blockSize / 2;

                    Parallel.ForEach(Partitioner.Create(j, rows, blockSize), range =>
                    {
                        int pivot = range.Item1, i = pivot + step;

                        // Range check
                        if (i >= range.Item2) return;

                        // Take advantage of sparsity
                        if (IsZero(R[i][j])) return;

                        T[] _cs = new T[2];
                        Givens(R[pivot][j], R[i][j], _cs);
                        GivensRotateRight(R, pivot, i, _cs[0], _cs[1], j, columns);
                        GivensRotateLeft(Q, pivot, i, _cs[0], _cs[1], 0, rows);
                    });

                    blockSize *= 2;
                }

                // residuals
                step = blockSize / 2;
                T[] cs = new T[2];
                for (int k = j + step; k < rows; k += step)
                {
                    // Take advantage of sparsity
                    if (IsZero(R[k][j])) continue;

                    Givens(R[j][j], R[k][j], cs);
                    GivensRotateRight(R, j, k, cs[0], cs[1], j, columns);
                    GivensRotateLeft(Q, j, k, cs[0], cs[1], 0, rows);
                }
            }
        }

        private static void ql_decompose_givens_rotate<T>(T[][] Q, T[][] L,
            Func<T, bool> IsZero,
            Action<T, T, T[]> Givens,
            Action<T[][], int, int, T, T, int, int> GivensRotateRow,
            Action<T[][], int, int, T, T, int, int> GivensRotateColumn)
        {
            int rows = L.Length, columns = L[0].Length, i, j, offset = rows - columns;

            T[] cs = new T[2];
            for (j = columns - 1; j >= 0; --j)
            {
                int end = offset + j;
                for (i = end - 1; i >= 0; --i)
                {
                    // Take advantage of sparsity
                    if (IsZero(L[i][j]))
                    {
                        continue;
                    }

                    T x1 = L[end][j], x2 = L[i][j];
                    Givens(x1, x2, cs);
                    GivensRotateRow(L, end, i, cs[0], cs[1], 0, j + 1);
                    GivensRotateColumn(Q, end, i, cs[0], cs[1], 0, rows);
                }
            }
        }

    }
}
