﻿using LinearLite.BLAS;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Decompositions.QR
{
    internal abstract class IDenseQRDecompositionAlgorithm
    {
        private FloatBLAS _fBLAS;
        private DoubleBLAS _dBLAS;
        private DecimalBLAS _mBLAS;
        private ComplexBLAS _cBLAS;

        protected IDenseQRDecompositionAlgorithm()
        {
            _fBLAS = new FloatBLAS();
            _dBLAS = new DoubleBLAS();
            _mBLAS = new DecimalBLAS();
            _cBLAS = new ComplexBLAS();
        }

        private void CheckMatrixDimensions(int A_rows, int A_cols, int Q_rows, int Q_cols, int R_rows, int R_cols)
        {
            if (A_rows < A_cols)
            {
                throw new InvalidOperationException();
            }
            if (Q_rows != Q_cols)
            {
                throw new InvalidOperationException();
            }
            if (A_rows != R_rows || A_cols != R_cols)
            {
                throw new InvalidOperationException();
            }
            if (Q_cols != R_rows)
            {
                throw new InvalidOperationException();
            }
        }
        private void CheckMatrixDimensions(int Q_rows, int Q_cols, int R_rows)
        {
            if (Q_rows != Q_cols)
            {
                throw new InvalidOperationException();
            }
            if (Q_cols != R_rows)
            {
                throw new InvalidOperationException();
            }
        }

        private void ResetToIdentity<T>(T[][] Q, T one, T zero)
        {
            int r, c, m = Q.Length;
            for (r = 0; r < m; ++r)
            {
                T[] Q_row = Q[r];
                for (c = 0; c < m; ++c)
                {
                    Q_row[c] = zero;
                }
                Q_row[r] = one;
            }
        }
        private void SetPositiveDiagonalsQR<T>(T[][] Q, T[][] R, IBLAS<T> blas)
        {
            int m = Q.Length, n = R[0].Length, c, r;
            for (c = 0; c < n; ++c)
            {
                if (blas.IsNegative(R[c][c]))
                {
                    for (r = 0; r < m; ++r)
                    {
                        Q[r][c] = blas.Negate(Q[r][c]);
                    }
                    T[] R_c = R[c];
                    for (r = c; r < n; ++r)
                    {
                        R_c[r] = blas.Negate(R_c[r]);
                    }
                }
            }
        }
        private void SetPositiveDiagonalsQL<T>(T[][] Q, T[][] L, IBLAS<T> blas)
        {
            int m = Q.Length, n = L[0].Length, c, r;
            for (c = 0; c < n; ++c)
            {
                if (blas.IsNegative(L[c][c]))
                {
                    for (r = 0; r < m; ++r)
                    {
                        Q[r][c] = blas.Negate(Q[r][c]);
                    }

                    int len = Math.Min(c + 1, n);
                    T[] L_c = L[c];
                    for (r = 0; r < len; ++r)
                    {
                        L_c[r] = blas.Negate(L_c[r]);
                    }
                }
            }
        }

        private void Decompose<T>(DenseMatrix<T> A, out DenseMatrix<T> Q, out DenseMatrix<T> R, Action<T[][], T[][]> SequentialDecompose, 
            Action<T[][], T[][]> ParallelDecompose, Action<T[][], T[][], IBLAS<T>> SetPositiveDiagonals, IBLAS<T> blas, 
            bool parallel = false, bool positiveDiagonals = true) where T : new()
        {
            Q = DenseMatrix.Identity<T>(A.Rows);

            if (parallel)
            {
                R = A.CopyParallel();
                ParallelDecompose(Q.Values, R.Values);
            }
            else
            {
                R = A.Copy();
                SequentialDecompose(Q.Values, R.Values);
            }

            if (positiveDiagonals)
            {
                SetPositiveDiagonals(Q.Values, R.Values, blas);
            }
        }
        internal void QRDecompose(DenseMatrix<float> A, out DenseMatrix<float> Q, out DenseMatrix<float> R, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out R, QR_decompose_inner, QR_decompose_inner_parallel, SetPositiveDiagonalsQR, _fBLAS, parallel, positiveDiagonals);
        internal void QRDecompose(DenseMatrix<double> A, out DenseMatrix<double> Q, out DenseMatrix<double> R, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out R, QR_decompose_inner, QR_decompose_inner_parallel, SetPositiveDiagonalsQR, _dBLAS, parallel, positiveDiagonals);
        internal void QRDecompose(DenseMatrix<decimal> A, out DenseMatrix<decimal> Q, out DenseMatrix<decimal> R, bool parallel = false, bool positiveDiagonals = false) =>
            Decompose(A, out Q, out R, QR_decompose_inner, QR_decompose_inner_parallel, SetPositiveDiagonalsQR, _mBLAS, parallel, positiveDiagonals);
        internal void QRDecompose(DenseMatrix<Complex> A, out DenseMatrix<Complex> Q, out DenseMatrix<Complex> R, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out R, QR_decompose_inner, QR_decompose_inner_parallel, SetPositiveDiagonalsQR, _cBLAS, parallel, positiveDiagonals);

        internal void QLDecompose(DenseMatrix<float> A, out DenseMatrix<float> Q, out DenseMatrix<float> L, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out L, QL_decompose_inner, QL_decompose_inner_parallel, SetPositiveDiagonalsQL, _fBLAS, parallel, positiveDiagonals);
        internal void QLDecompose(DenseMatrix<double> A, out DenseMatrix<double> Q, out DenseMatrix<double> L, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out L, QL_decompose_inner, QL_decompose_inner_parallel, SetPositiveDiagonalsQL, _dBLAS, parallel, positiveDiagonals);
        internal void QLDecompose(DenseMatrix<decimal> A, out DenseMatrix<decimal> Q, out DenseMatrix<decimal> L, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out L, QL_decompose_inner, QL_decompose_inner_parallel, SetPositiveDiagonalsQL, _mBLAS, parallel, positiveDiagonals);
        internal void QLDecompose(DenseMatrix<Complex> A, out DenseMatrix<Complex> Q, out DenseMatrix<Complex> L, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out L, QL_decompose_inner, QL_decompose_inner_parallel, SetPositiveDiagonalsQL, _cBLAS, parallel, positiveDiagonals);


        private void Decompose<T>(T[,] A, out T[,] Q, out T[,] M, Action<T[][], T[][]> SequentialDecompose, Action<T[][], T[][]> ParallelDecompose,
            Action<T[][], T[][], IBLAS<T>> SetPositiveDiagonals, IBLAS<T> blas, bool parallel = false, bool positiveDiagonals = false) where T : new()
        {
            T[][] _Q = DenseMatrix.Identity<T>(A.GetLength(0)).Values;

            if (parallel)
            {
                T[][] _L = A.ToJaggedParallel();
                ParallelDecompose(_Q, _L);
                if (positiveDiagonals)
                {
                    SetPositiveDiagonals(_Q, _L, blas);
                }
                M = _L.ToRectangularParallel();
                Q = _Q.ToRectangularParallel();
            }
            else
            {
                T[][] _L = A.ToJagged();
                SequentialDecompose(_Q, _L);
                if (positiveDiagonals)
                {
                    SetPositiveDiagonals(_Q, _L, blas);
                }
                M = _L.ToRectangular();
                Q = _Q.ToRectangular();
            }
        }
        internal void QRDecompose(float[,] A, out float[,] Q, out float[,] R, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out R, QR_decompose_inner, QR_decompose_inner_parallel, SetPositiveDiagonalsQR, _fBLAS, parallel, positiveDiagonals);
        internal void QRDecompose(double[,] A, out double[,] Q, out double[,] R, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out R, QR_decompose_inner, QR_decompose_inner_parallel, SetPositiveDiagonalsQR, _dBLAS, parallel, positiveDiagonals);
        internal void QRDecompose(decimal[,] A, out decimal[,] Q, out decimal[,] R, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out R, QR_decompose_inner, QR_decompose_inner_parallel, SetPositiveDiagonalsQR, _mBLAS, parallel, positiveDiagonals);
        internal void QRDecompose(Complex[,] A, out Complex[,] Q, out Complex[,] R, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out R, QR_decompose_inner, QR_decompose_inner_parallel, SetPositiveDiagonalsQR, _cBLAS, parallel, positiveDiagonals);

        internal void QLDecompose(float[,] A, out float[,] Q, out float[,] L, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out L, QL_decompose_inner, QL_decompose_inner_parallel, SetPositiveDiagonalsQL, _fBLAS, parallel, positiveDiagonals);
        internal void QLDecompose(double[,] A, out double[,] Q, out double[,] L, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out L, QL_decompose_inner, QL_decompose_inner_parallel, SetPositiveDiagonalsQL, _dBLAS, parallel, positiveDiagonals);
        internal void QLDecompose(decimal[,] A, out decimal[,] Q, out decimal[,] L, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out L, QL_decompose_inner, QL_decompose_inner_parallel, SetPositiveDiagonalsQL, _mBLAS, parallel, positiveDiagonals);
        internal void QLDecompose(Complex[,] A, out Complex[,] Q, out Complex[,] L, bool parallel = false, bool positiveDiagonals = false) => 
            Decompose(A, out Q, out L, QL_decompose_inner, QL_decompose_inner_parallel, SetPositiveDiagonalsQL, _cBLAS, parallel, positiveDiagonals);

        internal void QRDecompose(DenseMatrix<double> A, DenseMatrix<double> Q, DenseMatrix<double> R, bool resetQ = false, bool resetR = false, bool parallel = false)
        {
            if (resetR)
            {
                MatrixChecks.CheckNotNull(A, Q, R);
            }
            else
            {
                MatrixChecks.CheckNotNull(Q, R);
            }
            QRDecompose(A.Values, Q.Values, R.Values, resetQ, resetR, parallel);
        }
        internal void QRDecompose(double[][] A, double[][] Q, double[][] R, bool resetQ = false, bool resetR = false, bool parallel = false)
        {
            if (resetR)
            {
                MatrixChecks.CheckNotNull(A, Q, R);
                CheckMatrixDimensions(A.Length, A[0].Length, Q.Length, Q[0].Length, R.Length, R[0].Length);
            }
            else
            {
                MatrixChecks.CheckNotNull(Q, R);
                CheckMatrixDimensions(Q.Length, Q[0].Length, R.Length);
            }

            if (resetQ)
            {
                ResetToIdentity(Q, 1.0, 0.0);
            }

            if (resetR)
            {
                R.CopyFrom(A);
            }

            if (parallel)
            {
                QR_decompose_inner_parallel(Q, R);
            }
            else
            {
                QR_decompose_inner(Q, R);
            }
        }

        internal void QRDecomposeHessenberg(float[][] Q, float[][] R, bool resetQ = false, bool positiveDiagonals = false)
        {
            CheckMatrixDimensions(Q.Length, Q[0].Length, R.Length);
            if (resetQ)
            {
                ResetToIdentity(Q, 1.0f, 0.0f);
            }
            QR_decompose_hessenberg_unsafe(Q, R);
            if (positiveDiagonals)
            {
                SetPositiveDiagonalsQR(Q, R, _fBLAS);
            }
        }
        internal void QRDecomposeHessenberg(double[][] Q, double[][] R, bool resetQ = false, bool positiveDiagonals = false)
        {
            CheckMatrixDimensions(Q.Length, Q[0].Length, R.Length);
            if (resetQ)
            {
                ResetToIdentity(Q, 1.0, 0.0);
            }
            QR_decompose_hessenberg_unsafe(Q, R);
            if (positiveDiagonals)
            {
                SetPositiveDiagonalsQR(Q, R, _dBLAS);
            }
        }
        internal void QRDecomposeHessenberg(decimal[][] Q, decimal[][] R, bool resetQ = false, bool positiveDiagonals = false)
        {
            CheckMatrixDimensions(Q.Length, Q[0].Length, R.Length);
            if (resetQ)
            {
                ResetToIdentity(Q, 1.0m, 0.0m);
            }
            QR_decompose_hessenberg_unsafe(Q, R);
            if (positiveDiagonals)
            {
                SetPositiveDiagonalsQR(Q, R, _mBLAS);
            }
        }
        internal void QRDecomposeHessenberg(Complex[][] Q, Complex[][] R, bool resetQ = false, bool positiveDiagonals = false)
        {
            CheckMatrixDimensions(Q.Length, Q[0].Length, R.Length);
            if (resetQ)
            {
                ResetToIdentity(Q, Complex.One, Complex.Zero);
            }
            QR_decompose_hessenberg_unsafe(Q, R);
            if (positiveDiagonals)
            {
                SetPositiveDiagonalsQR(Q, R, _cBLAS);
            }
        }

        /// <summary>
        /// R is initialized to A, the matrix to be decomposed.
        /// </summary>
        /// <param name="Q"></param>
        /// <param name="R"></param>
        protected abstract void QR_decompose_inner(float[][] Q, float[][] R);
        protected abstract void QR_decompose_inner(double[][] Q, double[][] R);
        protected abstract void QR_decompose_inner(decimal[][] Q, decimal[][] R);
        protected abstract void QR_decompose_inner(Complex[][] Q, Complex[][] R);

        protected abstract void QR_decompose_inner_parallel(float[][] Q, float[][] R);
        protected abstract void QR_decompose_inner_parallel(double[][] Q, double[][] R);
        protected abstract void QR_decompose_inner_parallel(decimal[][] Q, decimal[][] R);
        protected abstract void QR_decompose_inner_parallel(Complex[][] Q, Complex[][] R);

        protected abstract void QL_decompose_inner(float[][] Q, float[][] L);
        protected abstract void QL_decompose_inner(double[][] Q, double[][] L);
        protected abstract void QL_decompose_inner(decimal[][] Q, decimal[][] L);
        protected abstract void QL_decompose_inner(Complex[][] Q, Complex[][] L);

        protected abstract void QL_decompose_inner_parallel(float[][] Q, float[][] L);
        protected abstract void QL_decompose_inner_parallel(double[][] Q, double[][] L);
        protected abstract void QL_decompose_inner_parallel(decimal[][] Q, decimal[][] L);
        protected abstract void QL_decompose_inner_parallel(Complex[][] Q, Complex[][] L);

        protected abstract void QR_decompose_inner(float[][] R);
        protected abstract void QR_decompose_inner(double[][] R);
        protected abstract void QR_decompose_inner(decimal[][] R);
        protected abstract void QR_decompose_inner(Complex[][] R);

        /// <summary>
        /// Perform QR decomposition on an upper Hessenberg matrix
        /// Note - this method does not check if the matrix is actually in Hessenberg form.
        /// All entries below the lower off-diagonal will be treated as though they are 
        /// identically 0, even if they are not in fact 0.
        /// </summary>
        protected abstract void QR_decompose_hessenberg_unsafe(float[][] Q, float[][] R);
        protected abstract void QR_decompose_hessenberg_unsafe(double[][] Q, double[][] R);
        protected abstract void QR_decompose_hessenberg_unsafe(decimal[][] Q, decimal[][] R);
        protected abstract void QR_decompose_hessenberg_unsafe(Complex[][] Q, Complex[][] R);

        protected abstract void QR_decompose_hessenberg_unsafe(float[][] R);
        protected abstract void QR_decompose_hessenberg_unsafe(double[][] R);
        protected abstract void QR_decompose_hessenberg_unsafe(decimal[][] R);
        protected abstract void QR_decompose_hessenberg_unsafe(Complex[][] R);
    }
}
