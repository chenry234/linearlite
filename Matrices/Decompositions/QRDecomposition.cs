﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using LinearLite.Matrices;
using LinearLite.Matrices.Decompositions;
using LinearLite.Matrices.Decompositions.QR;
using LinearLite.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace LinearLite.Decomposition
{
    /// <summary>
    /// This class provides useful methods for accessing QR, QL, LQ and RQ decomposition functionality provided 
    /// by other classes. It contains a unified interface for different decomposition methods and types. 
    /// </summary>
    public static class QRDecomposition
    {
        private static void PerformChecks<T>(this T[,] A)
        {
            if (A == null || A.GetLength(0) == 0 || A.GetLength(1) == 0)
            {
                throw new ArgumentNullException("Matrix is null or empty.");
            }
            if (A.GetLength(0) < A.GetLength(1))
            {
                throw new ArgumentOutOfRangeException($"The number of rows must be >= the number of columns. Received: {A.GetLength(0)} rows by {A.GetLength(1)} columns.");
            }
        }
        private static void PerformChecks<T>(DenseMatrix<T> A) where T : new()
        {
            if (A == null || A.Rows == 0 || A.Columns == 0)
            {
                throw new ArgumentNullException("Matrix is null or empty.");
            }
            if (A.Rows < A.Columns)
            {
                throw new ArgumentOutOfRangeException($"The number of rows must be >= the number of columns. Received: {A.Rows} rows by {A.Columns} columns.");
            }
        }

        #region Rectangular 

        #region Sequential dense-rectangular - public methods 

        /// <summary>
        /// <para>Calculates the QR decomposition of real or complex matrix $A\in\mathbb{F}^{m \times n}$ ($m \ge n$) into $A = QR$, where </para>
        /// <ul>
        /// <li>$Q\in\mathbb{F}^{m \times m}$ is a orthogonal (unitary) matrix,</li>
        /// <li>$R\in\mathbb{F}^{m \times n}$ is a upper triangular matrix</li>
        /// </ul>
        /// <para>
        /// There are currently four implementations:
        /// <ol>
        /// <li>Using Householder transformations (<txt>HOUSEHOLDER_TRANSFORM</txt>),</li>
        /// <li>Using the WY block-Householder method with Strassen-Winograd for the block-matrix multiplication step (<txt>BLOCK_HOUSEHOLDER_TRANSFORM</txt>),</li>
        /// <li>Using Givens rotations, (<txt>GIVENS_ROTATIONS</txt>) and</li>
        /// <li>Using Gram-Schmidt orthogonalization algorithm (<txt>GRAM_SCHMIDT</txt>)</li>
        /// </ol>
        /// The block-Householder method is selected by default and is recommended for moderate to large matrices ($n \ge 1000$). Because it uses 
        /// the Strassen-Winograd multiplication algorithm, its complexity is slightly lower than other methods at $O(n^{2.839})$.
        /// </para>
        /// <para>
        /// If <txt>positiveDiagonals</txt> is <txt>true</txt>, then the main diagonal entries of $R$ will be non-negative. Note that 
        /// $R$ is not necessarily unique unless if its diagonal elements are all positive. 
        /// </para>
        /// <para>Parallel method: <a href="#QRDecomposeParallel"><txt>QRDecomposeParallel</txt></a>.</para>
        /// 
        /// <!--inputs-->
        /// <para><b>Example:</b></para>
        /// <pre><code class="cs">
        /// // Create a random 15 x 10 real matrix
        /// DenseMatrix&lt;double&gt; matrix = DenseMatrix.Random&lt;double&gt;(15, 10); 
        /// 
        /// // Calculate the default QR decomposition (using Householder transformations)
        /// matrix.QRDecompose(out DenseMatrix&lt;double&gt; Q1, out DenseMatrix&lt;double&gt; R1);
        /// 
        /// // Calculate the QR decomposition with positive diagonals
        /// matrix.QRDecompose(out DenseMatrix&lt;double&gt; Q2, out DenseMatrix&lt;double&gt; R2, true);
        /// 
        /// // Calculate the QR decomposition with using Gram-Schmidt orthogonalization algorithm
        /// matrix.QRDecompose(out DenseMatrix&lt;double&gt; Q3, out DenseMatrix&lt;double&gt; R3, true, 
        /// TriangularDecompositionMethod.GRAM_SCHMIDT); 
        /// 
        /// Console.WriteLine(Q1.IsOrthogonal());        // true
        /// Console.WriteLine(R1.IsUpperTriangular());   // true
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>QRDecompose</name>
        /// <proto>void QRDecompose(this DenseMatrix<T> A, out DenseMatrix<T> Q, out DenseMatrix<T> R, bool positiveDiagonals, QRDecompositionMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A">$m \times n$ matrix $A$ to be decomposed, with $m \ge n$.</param>
        /// <param name="Q">
        /// <b>Out parameter</b><br/>
        /// $m \times m$ generated orthogonal/unitary matrix (the $Q$ matrix).
        /// </param>
        /// <param name="R">
        /// <b>Out parameter</b><br/>
        /// $m \times n$ generated upper-triangular matrix (the $R$ matrix).
        /// </param>
        /// <param name="positiveDiagonals">
        /// <b>Optional</b>, defaults to <txt>true</txt>. <br/>
        /// If <txt>true</txt>, then the diagonals of $R$ will be guaranteed to be non-negative. 
        /// </param>
        /// <param name="method">
        /// <b>Optional</b>, defaults to <txt>BLOCK_HOUSEHOLDER_TRANSFORM</txt>. <br/>
        /// The method used to decompose the matrix $A$. Options: <br/>
        /// <txt>BLOCK_HOUSEHOLDER_TRANSFORM</txt>: Block-Householder transformations and Strassen-Winograd (WY method)<br/>
        /// <txt>HOUSEHOLDER_TRANSFORM</txt>: Householder transformations<br/>
        /// <txt>GIVENS_ROTATIONS</txt>: Givens rotations<br/>
        /// <txt>GRAM_SCHMIDT</txt>: Gram-Schmidt orthogonalization algorithm
        /// </param>
        public static void QRDecompose(this float[,] A, out float[,] Q, out float[,] R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: false, positiveDiagonals);
        }
        public static void QRDecompose(this double[,] A, out double[,] Q, out double[,] R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: false, positiveDiagonals);
        }
        public static void QRDecompose(this decimal[,] A, out decimal[,] Q, out decimal[,] R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: false, positiveDiagonals);
        }
        public static void QRDecompose(this Complex[,] A, out Complex[,] Q, out Complex[,] R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: false, positiveDiagonals);
        }

        /// <summary>
        /// Calculates the QL decomposition of real matrix $A\in\mathbb{F}^{m \times n}$ ($m \ge n$) into $A = QL$, where </para>
        /// <ul>
        /// <li>$Q\in\mathbb{F}^{m \times m}$ is a orthogonal (unitary) matrix,</li>
        /// <li>$L\in\mathbb{F}^{m \times n}$ is a lower triangular matrix</li>
        /// </ul>
        /// Implementation is similar to <a href="#QRDecompose"><txt><b>QRDecompose</b></txt></a>.
        /// </summary>
        /// <name>QLDecompose</name>
        /// <proto>void QLDecompose(DenseMatrix<T> A, out DenseMatrix<T> Q, out DenseMatrix<T> L, bool positiveDiagonals, QRDecompositionMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A">Any matrix in R^(m x n) with m >= n</param>
        /// <param name="positiveDiagonals">If true, then the diagonals of L are guaranteed to be non-negative.</param>
        /// <param name="Q">An orthogonal matrix in R^(m x m)</param>
        /// <param name="R">A lower triangular matrix in R^(m x n)</param>
        /// <param name="method">The method used to decompose the matrix A</param>
        public static void QLDecompose(this float[,] A, out float[,] Q, out float[,] L, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QLDecompose(A, out Q, out L, parallel: false, positiveDiagonals);
        }
        public static void QLDecompose(this double[,] A, out double[,] Q, out double[,] L, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QLDecompose(A, out Q, out L, parallel: false, positiveDiagonals);
        }
        public static void QLDecompose(this decimal[,] A, out decimal[,] Q, out decimal[,] L, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QLDecompose(A, out Q, out L, parallel: false, positiveDiagonals);
        }
        public static void QLDecompose(this Complex[,] A, out Complex[,] Q, out Complex[,] L, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QLDecompose(A, out Q, out L, parallel: false, positiveDiagonals);
        }

        /// <summary>
        /// Calculates the RQ decomposition of real matrix $A\in\mathbb{F}^{m \times n}$ ($m \le n$) into $A = RQ$, where </para>
        /// <ul>
        /// <li>$R\in\mathbb{F}^{m \times n}$ is a upper triangular matrix</li>
        /// <li>$Q\in\mathbb{F}^{n \times n}$ is a orthogonal (unitary) matrix,</li>
        /// </ul>
        /// Implementation is similar to <a href="#QRDecompose"><txt><b>QRDecompose</b></txt></a>.
        /// </summary>
        /// <name>RQDecompose</name>
        /// <proto>void RQDecompose(DenseMatrix<T> A, out DenseMatrix<T> R, out DenseMatrix<T> Q, bool positiveDiagonals, QRDecompositionMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A">Any matrix in R^(m x n) with m <= n</param>
        /// <param name="positiveDiagonals">If true, then the diagonals of L are guaranteed to be non-negative.</param>
        /// <param name="Q">An orthogonal matrix in R^(n x n)</param>
        /// <param name="R">A lower triangular matrix in R^(m x n)</param>
        /// <param name="method">The method used to decompose the matrix A</param>
        public static void RQDecompose(this float[,] A, out float[,] R, out float[,] Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QLDecompose(A.Transpose(), out float[,] q, out float[,] l, positiveDiagonals, method);
            R = l.Transpose();
            Q = q.Transpose();
        }
        public static void RQDecompose(this double[,] A, out double[,] R, out double[,] Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QLDecompose(A.Transpose(), out double[,] q, out double[,] l, positiveDiagonals, method);
            R = l.Transpose();
            Q = q.Transpose();
        }
        public static void RQDecompose(this decimal[,] A, out decimal[,] R, out decimal[,] Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QLDecompose(A.Transpose(), out decimal[,] q, out decimal[,] l, positiveDiagonals, method);
            R = l.Transpose();
            Q = q.Transpose();
        }
        public static void RQDecompose(this Complex[,] A, out Complex[,] R, out Complex[,] Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QLDecompose(A.ConjugateTranspose(), out Complex[,] q, out Complex[,] l, positiveDiagonals, method);
            R = l.ConjugateTranspose();
            Q = q.ConjugateTranspose();
        }
        
        /// <summary>
        /// Calculates the LQ decomposition of real matrix $A\in\mathbb{F}^{m \times n}$ ($m \le n$) into $A = LQ$, where </para>
        /// <ul>
        /// <li>$L\in\mathbb{F}^{m \times n}$ is a lower triangular matrix</li>
        /// <li>$Q\in\mathbb{F}^{n \times n}$ is a orthogonal (unitary) matrix,</li>
        /// </ul>
        /// Implementation is similar to <a href="#QRDecompose"><txt><b>QRDecompose</b></txt></a>.
        /// </summary>
        /// <name>LQDecompose</name>
        /// <proto>void LQDecompose(DenseMatrix<T> A, out DenseMatrix<T> L, out DenseMatrix<T> Q, bool positiveDiagonals, QRDecompositionMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A">Any matrix in R^(m x n) with m <= n</param>
        /// <param name="positiveDiagonals">If true, then the diagonals of R are guaranteed to be non-negative.</param>
        /// <param name="Q">An orthogonal matrix in R^(n x n)</param>
        /// <param name="R">A upper triangular matrix in R^(m x n)</param>
        /// <param name="method">The method used to decompose the matrix A</param>
        public static void LQDecompose(this double[,] A, out double[,] L, out double[,] Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecompose(A.Transpose(), out double[,] q, out double[,] r, positiveDiagonals, method);
            L = r.Transpose();
            Q = q.Transpose();
        }
        public static void LQDecompose(this float[,] A, out float[,] L, out float[,] Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecompose(A.Transpose(), out float[,] q, out float[,] r, positiveDiagonals, method);
            L = r.Transpose();
            Q = q.Transpose();
        }
        public static void LQDecompose(this decimal[,] A, out decimal[,] L, out decimal[,] Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecompose(A.Transpose(), out decimal[,] q, out decimal[,] r, positiveDiagonals, method);
            L = r.Transpose();
            Q = q.Transpose();
        }
        public static void LQDecompose(this Complex[,] A, out Complex[,] L, out Complex[,] Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecompose(A.ConjugateTranspose(), out Complex[,] q, out Complex[,] r, positiveDiagonals, method);
            L = r.ConjugateTranspose();
            Q = q.ConjugateTranspose();
        }

        #endregion

        #region Parallel Dense-rectangular

        /// <summary>
        /// Calculates the QR decomposition in parallel, of a matrix A (m x n) into Q (m x m) x R (m x n), where m >= n, using Householder transformations.
        /// Q is an orthogonal matrix and R is an upper triangular matrix. 
        /// If positiveDiagonals = true, then the main diagonal entries of R will be non-negative. Note that 
        /// R is not necessarily unique unless if its diagonal elements are all positive. 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="Q"></param>
        /// <param name="R"></param>
        /// <param name="positiveDiagonals"></param>
        public static void QRDecomposeParallel(this double[,] A, out double[,] Q, out double[,] R, bool positiveDiagonals = false)
        {
            PerformChecks(A);

            int m = A.GetLength(0), n = A.GetLength(1), c, r;
            double[][] _Q = MatrixInternalExtensions.JIdentity(m),
                       _R = A.ToJaggedParallel();

            double[] v = new double[m],
                     w = new double[n];

            for (c = 0; c < n; ++c)
            {
                Householder.TransformParallel(_R, _Q, v, w, c, n, c, m, true);
            }

            // convert sign to ensure that diagonal elements are positive
            if (positiveDiagonals)
            {
                for (c = 0; c < n; ++c)
                {
                    if (_R[c][c] < 0)
                    {
                        for (r = 0; r < m; ++r)
                        {
                            _Q[r][c] = -_Q[r][c];
                        }
                        double[] R_c = _R[c];
                        for (r = c; r < n; ++r)
                        {
                            R_c[r] = -R_c[r];
                        }
                    }
                }
            }

            Q = _Q.ToRectangularParallel();
            R = _R.ToRectangularParallel();
        }

        #endregion

        #endregion

        #region DenseMatrix 

        #region Sequential dense DenseMatrix - public methods 

        private static IDenseQRDecompositionAlgorithm GetDefaultAlgorithm(QRDecompositionMethod method)
        {
            switch (method)
            {
                case QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM: return new BlockHouseholderQRDecompositionAlgorithm();
                case QRDecompositionMethod.HOUSEHOLDER_TRANSFORM: return new HouseholderQRDecompositionAlgorithm();
                case QRDecompositionMethod.GIVENS_ROTATIONS: return new GivensRotationQRDecompositionAlgorithm();
                case QRDecompositionMethod.GRAM_SCHMIDT: return new GramSchmidtQRDecompositionAlgorithm();
            }
            throw new NotImplementedException();
        }
        public static void QRDecompose(this DenseMatrix<float> A, out DenseMatrix<float> Q, out DenseMatrix<float> R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: false, positiveDiagonals);
        }
        public static void QRDecompose(this DenseMatrix<double> A, out DenseMatrix<double> Q, out DenseMatrix<double> R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: false, positiveDiagonals);
        }
        public static void QRDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> Q, out DenseMatrix<decimal> R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: false, positiveDiagonals);
        }
        public static void QRDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> Q, out DenseMatrix<Complex> R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: false, positiveDiagonals);
        }

        public static void QLDecompose(this DenseMatrix<float> A, out DenseMatrix<float> Q, out DenseMatrix<float> L, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QLDecompose(A, out Q, out L, parallel: false, positiveDiagonals);
        }
        public static void QLDecompose(this DenseMatrix<double> A, out DenseMatrix<double> Q, out DenseMatrix<double> L, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QLDecompose(A, out Q, out L, parallel: false, positiveDiagonals);
        }
        public static void QLDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> Q, out DenseMatrix<decimal> L, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QLDecompose(A, out Q, out L, parallel: false, positiveDiagonals);
        }
        public static void QLDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> Q, out DenseMatrix<Complex> L, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QLDecompose(A, out Q, out L, parallel: false, positiveDiagonals);
        }

        public static void LQDecompose(this DenseMatrix<float> A, out DenseMatrix<float> L, out DenseMatrix<float> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecompose(A.Transpose(), out Q, out DenseMatrix<float> r, positiveDiagonals, method);

            if (r.IsSquare)
            {
                r.Values.SelfTranspose();
                L = r;
            }
            else
            {
                L = r.Transpose();
            }
            
            Q.Values.SelfTranspose();
        }
        public static void LQDecompose(this DenseMatrix<double> A, out DenseMatrix<double> L, out DenseMatrix<double> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecompose(A.Transpose(), out Q, out DenseMatrix<double> r, positiveDiagonals, method);

            if (r.IsSquare)
            {
                r.Values.SelfTranspose();
                L = r;
            }
            else
            {
                L = r.Transpose();
            }

            Q.Values.SelfTranspose();
        }
        public static void LQDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> L, out DenseMatrix<decimal> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecompose(A.Transpose(), out Q, out DenseMatrix<decimal> r, positiveDiagonals, method);

            if (r.IsSquare)
            {
                r.Values.SelfTranspose();
                L = r;
            }
            else
            {
                L = r.Transpose();
            }

            Q.Values.SelfTranspose();
        }
        public static void LQDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> L, out DenseMatrix<Complex> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecompose(A.ConjugateTranspose(), out Q, out DenseMatrix<Complex> r, positiveDiagonals, method);

            if (r.IsSquare)
            {
                r.Values.SelfConjugateTranspose();
                L = r;
            }
            else
            {
                L = r.ConjugateTranspose();
            }

            Q.Values.SelfConjugateTranspose();
        }

        public static void RQDecompose(this DenseMatrix<float> A, out DenseMatrix<float> R, out DenseMatrix<float> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QLDecompose(A.Transpose(), out Q, out DenseMatrix<float> r, positiveDiagonals, method);
            R = r.Transpose();
            Q.Values.SelfTranspose();
        }
        public static void RQDecompose(this DenseMatrix<double> A, out DenseMatrix<double> R, out DenseMatrix<double> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QLDecompose(A.Transpose(), out Q, out DenseMatrix<double> r, positiveDiagonals, method);
            R = r.Transpose();
            Q.Values.SelfTranspose();
        }
        public static void RQDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> R, out DenseMatrix<decimal> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QLDecompose(A.Transpose(), out Q, out DenseMatrix<decimal> r, positiveDiagonals, method);
            R = r.Transpose();
            Q.Values.SelfTranspose();
        }
        public static void RQDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> R, out DenseMatrix<Complex> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QLDecompose(A.ConjugateTranspose(), out Q, out DenseMatrix<Complex> r, positiveDiagonals, method);
            R = r.ConjugateTranspose();
            Q.Values.SelfConjugateTranspose();
        }

        #endregion

        #region Parallel dense DenseMatrix - public methods 

        /// <summary>
        /// <para>The parallel version of <a href="#QRDecompose"><txt>QRDecompose</txt></a> for QR decomposition.</para>
        /// </summary>
        /// <name>QRDecomposeParallel</name>
        /// <proto>void QRDecomposeParallel(this DenseMatrix<T> A, out DenseMatrix<T> Q, out DenseMatrix<T> R, bool positiveDiagonals, QRDecompositionMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="Q"></param>
        /// <param name="R"></param>
        /// <param name="positiveDiagonals"></param>
        /// <param name="method"></param>
        public static void QRDecomposeParallel(this DenseMatrix<float> A, out DenseMatrix<float> Q, out DenseMatrix<float> R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: true, positiveDiagonals);
        }
        public static void QRDecomposeParallel(this DenseMatrix<double> A, out DenseMatrix<double> Q, out DenseMatrix<double> R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: true, positiveDiagonals);
        }
        public static void QRDecomposeParallel(this DenseMatrix<decimal> A, out DenseMatrix<decimal> Q, out DenseMatrix<decimal> R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: true, positiveDiagonals);
        }
        public static void QRDecomposeParallel(this DenseMatrix<Complex> A, out DenseMatrix<Complex> Q, out DenseMatrix<Complex> R, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            PerformChecks(A);
            GetDefaultAlgorithm(method).QRDecompose(A, out Q, out R, parallel: true, positiveDiagonals);
        }

        public static void LQDecomposeParallel(this DenseMatrix<float> A, out DenseMatrix<float> L, out DenseMatrix<float> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecomposeParallel(A.TransposeParallel(), out Q, out DenseMatrix<float> r, positiveDiagonals, method);

            if (r.IsSquare)
            {
                r.Values.SelfTransposeParallel();
                L = r;
            }
            else
            {
                L = r.TransposeParallel();
            }

            Q.Values.SelfTransposeParallel();
        }
        public static void LQDecomposeParallel(this DenseMatrix<double> A, out DenseMatrix<double> L, out DenseMatrix<double> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecomposeParallel(A.TransposeParallel(), out Q, out DenseMatrix<double> r, positiveDiagonals, method);

            if (r.IsSquare)
            {
                r.Values.SelfTransposeParallel();
                L = r;
            }
            else
            {
                L = r.TransposeParallel();
            }

            Q.Values.SelfTransposeParallel();
        }
        public static void LQDecomposeParallel(this DenseMatrix<decimal> A, out DenseMatrix<decimal> L, out DenseMatrix<decimal> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecomposeParallel(A.TransposeParallel(), out Q, out DenseMatrix<decimal> r, positiveDiagonals, method);

            if (r.IsSquare)
            {
                r.Values.SelfTransposeParallel();
                L = r;
            }
            else
            {
                L = r.TransposeParallel();
            }

            Q.Values.SelfTransposeParallel();
        }
        public static void LQDecomposeParallel(this DenseMatrix<Complex> A, out DenseMatrix<Complex> L, out DenseMatrix<Complex> Q, bool positiveDiagonals = false, QRDecompositionMethod method = QRDecompositionMethod.BLOCK_HOUSEHOLDER_TRANSFORM)
        {
            QRDecomposeParallel(A.ConjugateTransposeParallel(), out Q, out DenseMatrix<Complex> r, positiveDiagonals, method);

            if (r.IsSquare)
            {
                r.Values.SelfConjugateTransposeParallel();
                L = r;
            }
            else
            {
                L = r.ConjugateTransposeParallel();
            }

            Q.Values.SelfConjugateTransposeParallel();
        }

        #endregion

        #endregion
    }
    public enum QRDecompositionMethod
    {
        BLOCK_HOUSEHOLDER_TRANSFORM, HOUSEHOLDER_TRANSFORM, GRAM_SCHMIDT, GIVENS_ROTATIONS
    }
}
