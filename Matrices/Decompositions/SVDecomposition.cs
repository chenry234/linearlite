﻿using LinearLite.Matrices;
using LinearLite.Matrices.Decompositions.QR;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Decomposition
{
    public static class SVDecomposition
    {
        public static void SingularValueDecompose(this double[,] A, out double[,] U, out double[,] D, out double[,] V)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1);

            A.ToBidiagonalForm(out double[,] UPrime, out double[,] B, out double[,] VPrime, false, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            A.Print();

            U = RectangularMatrix.Identity<double>(m);
            D = new double[m, n];
            V = RectangularMatrix.Identity<double>(n);

            for (int i = 0; i < 2; ++i)
            {
                B.QRDecompose(out double[,] u, out double[,] R, true);
                R.LQDecompose(out double[,] d, out double[,] v, true);

                B = D;
                U = U.Multiply(u);
                D = d;
                V = v.Multiply(V);

                B.Print();
            }
        }

        /// <summary>
        /// Decompose a $m \times n$ matrix $A$ into $$A = UDV^*,$$ 
        /// where
        /// <ul>
        /// <li>$U$ is a $m \times m$ orthogonal/unitary matrix, the set of left singular vectors.</li>
        /// <li>$D$ is a $m \times n$ diagonal matrix with non-negative elements, the singular values of $A$.</li>
        /// <li>$V$ is a $n \times n$ orthogonal/unitary matrix, the set of right singular vectors.</li>
        /// </ul>
        /// and $V^*$ is the conjugate transpose of $V$.
        /// 
        /// <!--inputs-->
        /// 
        /// <para><b>Example</b></para>
        /// <pre><code class="cs">
        /// // Create a random 10 x 15 real matrix
        /// DenseMatrix&lt;double&gt; matrix = DenseMatrix.Random&lt;double&gt;(10, 15); 
        /// DenseMatrix&lt;double&gt; U, D, V;
        /// matrix.SingularValueDecompose(out U, out D, out V); // Perform a SVD
        /// 
        /// Console.WriteLine(U.IsOrthogonal()); // true
        /// Console.WriteLine(D.IsDiagonal());   // true
        /// Console.WriteLine(V.IsOrthogonal()); // true
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>SingularValueDecompose</name>
        /// <proto>void SingularValueDecompose(this IMatrix<T> A, out IMatrix<T> U, out IMatrix<T> D, out IMatrix<T> V)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times n$ matrix $A$ to be decomposed.</param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// The $m \times m$ orthogonal/unitary matrix $U$ of left singular vector matrix.
        /// </param>
        /// <param name="D">
        /// <b>Out parameter</b><br/>
        /// The $m \times n$ rectangular diagonal matrix $D$ of singular values.
        /// </param>
        /// <param name="V">
        /// <b>Out parameter</b><br/>
        /// The $n \times n$ orthogonal/unitary matrix $V$ of right singular vector matrix.
        /// </param>
        public static void SingularValueDecompose(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> D, out DenseMatrix<float> V)
        {
            MatrixChecks.CheckNotNull(A);

            A.ToBidiagonalForm(out U, out D, out V, false, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            float termination_norm = 1e-8f;
            U.Values.SelfTranspose();
            V.Values.SelfTranspose();

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            float[][] Dt = MatrixInternalExtensions.JMatrix<float>(D.Columns, D.Rows);
            for (int i = 0; i < 1000; ++i)
            {
                qr.QRDecomposeHessenberg(U.Values, D.Values, false, false);
                CopyUpperBidiagonalToTranspose(D.Values, Dt, 0.0f);

                qr.QRDecomposeHessenberg(V.Values, Dt, false, false);
                CopyUpperBidiagonalToTranspose(Dt, D.Values, 0.0f);

                float norm = OffDiagonalNorm(D);
                if (norm <= termination_norm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }
        }
        public static void SingularValueDecompose(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> D, out DenseMatrix<double> V)
        {
            MatrixChecks.CheckNotNull(A);

            A.ToBidiagonalForm(out U, out D, out V, false, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            double termination_norm = 1e-16;
            U.Values.SelfTranspose();
            V.Values.SelfTranspose();

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            double[][] Dt = MatrixInternalExtensions.JMatrix<double>(D.Columns, D.Rows);
            for (int i = 0; i < 1000; ++i)
            {
                qr.QRDecomposeHessenberg(U.Values, D.Values, false, false);
                CopyUpperBidiagonalToTranspose(D.Values, Dt, 0.0);

                qr.QRDecomposeHessenberg(V.Values, Dt, false, false);
                CopyUpperBidiagonalToTranspose(Dt, D.Values, 0.0);

                double norm = OffDiagonalNorm(D);
                if (norm <= termination_norm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }
        }
        public static void SingularValueDecompose(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> D, out DenseMatrix<decimal> V)
        {
            MatrixChecks.CheckNotNull(A);

            A.ToBidiagonalForm(out U, out D, out V, false, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            decimal termination_norm = 1e-60m;
            U.Values.SelfTranspose();
            V.Values.SelfTranspose();

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            decimal[][] Dt = MatrixInternalExtensions.JMatrix<decimal>(D.Columns, D.Rows);
            for (int i = 0; i < 1000; ++i)
            {
                qr.QRDecomposeHessenberg(U.Values, D.Values, false, false);
                CopyUpperBidiagonalToTranspose(D.Values, Dt, 0.0m);

                qr.QRDecomposeHessenberg(V.Values, Dt, false, false);
                CopyUpperBidiagonalToTranspose(Dt, D.Values, 0.0m);

                decimal norm = OffDiagonalNorm(D);
                if (norm <= termination_norm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }
        }
        public static void SingularValueDecompose(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> D, out DenseMatrix<Complex> V)
        {
            MatrixChecks.CheckNotNull(A);

            A.ToBidiagonalForm(out U, out D, out V, false, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            double termination_norm = 1e-16;
            U.Values.SelfConjugateTranspose();
            V.Values.SelfConjugateTranspose();

            IDenseQRDecompositionAlgorithm qr = new HouseholderQRDecompositionAlgorithm();
            Complex[][] Dt = MatrixInternalExtensions.JMatrix<Complex>(D.Columns, D.Rows);
            for (int i = 0; i < 1000; ++i)
            {
                qr.QRDecomposeHessenberg(U.Values, D.Values, false, false);
                CopyUpperBidiagonalToConjugateTranspose(D.Values, Dt);

                qr.QRDecomposeHessenberg(V.Values, Dt, false, false);
                CopyUpperBidiagonalToConjugateTranspose(Dt, D.Values);

                double norm = OffDiagonalNorm(D);
                if (norm <= termination_norm)
                {
                    Debug.WriteLine(i);
                    break;
                }
            }
        }

        private static void CopyUpperBidiagonalToTranspose<T>(T[][] D, T[][] Dt, T zero)
        {
            Dt[0][0] = D[0][0];

            int rows = D.Length, cols = D[0].Length, len = Math.Min(rows, cols), i;
            for (i = 1; i < len; ++i)
            {
                int _i = i - 1;
                T[] Dt_i = Dt[i];

                Dt[_i][i] = zero;
                Dt_i[i] = D[i][i];
                Dt_i[_i] = D[_i][i];
            }

            if (cols > rows)
            {
                Dt[len][len - 1] = D[len - 1][len];
            }
            if (rows > cols)
            {
                Dt[len - 1][len] = zero;
            }
        }
        private static void CopyUpperBidiagonalToConjugateTranspose(Complex[][] D, Complex[][] Dt)
        {
            Dt[0][0] = D[0][0].Conjugate();

            int rows = D.Length, cols = D[0].Length, len = Math.Min(rows, cols), i;
            for (i = 1; i < len; ++i)
            {
                int _i = i - 1;
                Complex[] Dt_i = Dt[i];

                Dt[_i][i] = Complex.Zero;
                Dt_i[i] = D[i][i].Conjugate();
                Dt_i[_i] = D[_i][i].Conjugate();
            }

            if (cols > rows)
            {
                Dt[len - 1][len] = Complex.Zero;
                Dt[len][len - 1] = D[len - 1][len].Conjugate();
            }
        }

        private static float OffDiagonalNorm(DenseMatrix<float> A)
        {
            int last = Math.Min(A.Columns, A.Rows - 1), i;
            float norm = 0.0f;
            for (i = 1; i <= last; ++i)
            {
                norm += Math.Abs(A[i][i - 1]);
            }
            return norm / last;
        }
        private static double OffDiagonalNorm(DenseMatrix<double> A)
        {
            int last = Math.Min(A.Columns, A.Rows - 1), i;
            double norm = 0.0;
            for (i = 1; i <= last; ++i)
            {
                norm += Math.Abs(A[i][i - 1]);
            }
            return norm / last;
        }
        private static decimal OffDiagonalNorm(DenseMatrix<decimal> A)
        {
            int last = Math.Min(A.Columns, A.Rows - 1), i;
            decimal norm = 0.0m;
            for (i = 1; i <= last; ++i)
            {
                norm += Math.Abs(A[i][i - 1]);
            }
            return norm / last;
        }
        private static double OffDiagonalNorm(DenseMatrix<Complex> A)
        {
            int last = Math.Min(A.Columns, A.Rows - 1), i;
            double norm = 0.0;
            for (i = 1; i <= last; ++i)
            {
                norm += A[i][i - 1].Modulus();
            }
            return norm / last;
        }
    }
}
