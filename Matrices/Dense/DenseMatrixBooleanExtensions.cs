﻿using LinearLite.BLAS;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    public static class DenseMatrixBooleanExtensions
    {
        /// <summary>
        /// <para>Returns <txt>true</txt> if a matrix $A$ is an identity matrix, and <txt>false</txt> otherwise.</para>
        /// <para>
        /// For matrices over limited-precision arithmetic, such as <txt>float</txt>, <txt>double</txt> and <txt>decimal</txt>, the method will return true 
        /// as long as each element of $A$ differs from the corresponding element from the identity matrix by at most <txt>tolerance</txt>.
        /// </para>
        /// </summary>
        /// <name>IsIdentity</name>
        /// <proto>bool IsIdentity(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool IsIdentity(this DenseMatrix<int> A) => IsIdentity(A, x => x == 0, x => x == 1);
        public static bool IsIdentity(this DenseMatrix<long> A) => IsIdentity(A, x => x == 0L, x => x == 1L);
        public static bool IsIdentity(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsIdentity(A, a => a.approx_zero(tolerance), a => a.approx_one(tolerance));
        public static bool IsIdentity(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsIdentity(A, a => a.approx_zero(tolerance), a => a.approx_one(tolerance));
        public static bool IsIdentity(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsIdentity(A, a => a.approx_zero(tolerance), a => a.approx_one(tolerance));
        public static bool IsIdentity<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : Ring<T>, new()
        {
            T e = new T();
            T zero = e.AdditiveIdentity, one = e.MultiplicativeIdentity;
            return IsIdentity(A, a => a.ApproximatelyEquals(zero, tolerance), a => a.ApproximatelyEquals(one, tolerance));
        }
        private static bool IsIdentity<T>(this DenseMatrix<T> A, Func<T, bool> IsZero, Func<T, bool> IsOne) where T : new()
        {
            if (A == null) return false;

            int m = A.Rows, n = A.Columns, i, j;
            if (m != n) return false;

            for (i = 0; i < m; ++i)
            {
                T[] A_i = A[i];
                for (j = 0; j < m; ++j)
                {
                    if (i == j)
                    {
                        if (!IsOne(A_i[i])) return false;
                    }
                    else
                    {
                        if (!IsZero(A_i[j])) return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns <txt>true</txt> if a matrix $A$ is a diagonal matrix, and <txt>false</txt> otherwise.</para>
        /// <para>
        /// For matrices over limited-precision arithmetic, such as <txt>float</txt>, <txt>double</txt> and <txt>decimal</txt>, the method will return true 
        /// as long as each off-diagonal element of $A$ has a magnitude of at most <txt>tolerance</txt>.
        /// </para>
        /// <para></para>
        /// </summary>
        /// <name>IsDiagonal</name>
        /// <proto>bool IsDiagonal(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool IsDiagonal(this DenseMatrix<int> A) => IsDiagonal(A, a => a == 0);
        public static bool IsDiagonal(this DenseMatrix<long> A) => IsDiagonal(A, a => a == 0L);
        public static bool IsDiagonal(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsDiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsDiagonal(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsDiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsDiagonal(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsDiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsDiagonal<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return IsDiagonal(A, a => a.ApproximatelyEquals(zero, tolerance));
        }
        private static bool IsDiagonal<T>(this DenseMatrix<T> A, Func<T, bool> ApproximatelyZero) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.Rows, n = A.Columns, i, j;
            for (i = 0; i < m; ++i)
            {
                T[] A_i = A[i];
                for (j = 0; j < n; ++j)
                {
                    if (i != j && !ApproximatelyZero(A_i[j])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns <txt>true</txt> if a matrix $A$ is a upper triangular matrix, and <txt>false</txt> otherwise.</para>
        /// <para>
        /// This method deems a matrix to be upper triangular if all elements $A_{ij}$ below the main diagonal satisfy:
        /// <ul>
        /// <li>$A_{ij} = 0$, for matrices over integer and long</li>
        /// <li>$|A_{ij}| < \epsilon$, for matrices over finite-precision arithmetic, for some tolerance level $\epsilon$</li>
        /// </ul>
        /// </para>
        /// <para></para>
        /// </summary>
        /// <name>IsUpperTriangular</name>
        /// <proto>bool IsUpperTriangular(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool IsUpperTriangular(this DenseMatrix<int> A) => IsUpperTriangular(A, e => e == 0);
        public static bool IsUpperTriangular(this DenseMatrix<long> A) => IsUpperTriangular(A, e => e == 0L);
        public static bool IsUpperTriangular(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsUpperTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsUpperTriangular(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsUpperTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsUpperTriangular(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsUpperTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsUpperTriangular<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return IsUpperTriangular(A, e => e.ApproximatelyEquals(zero, tolerance));
        }
        private static bool IsUpperTriangular<T>(this DenseMatrix<T> A, Func<T, bool> ApproximatelyZero) where T : new()
        {
            int m = A.Rows, n = A.Columns, i, j;
            for (i = 1; i < m; ++i)
            {
                int end = Math.Min(i, n);
                T[] A_i = A[i];
                for (j = 0; j < end; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns <txt>true</txt> if a matrix $A$ is a lower triangular matrix, and <txt>false</txt> otherwise.</para>
        /// <para>
        /// This method deems a matrix to be lower triangular if all elements $A_{ij}$ above the main diagonal satisfy:
        /// <ul>
        /// <li>$A_{ij} = 0$, for matrices over integer and long</li>
        /// <li>$|A_{ij}| < \epsilon$, for matrices over finite-precision arithmetic, for some tolerance level $\epsilon$</li>
        /// </ul>
        /// </para>
        /// <para></para>
        /// </summary>
        /// <name>IsLowerTriangular</name>
        /// <proto>bool IsLowerTriangular(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool IsLowerTriangular(this DenseMatrix<int> A) => IsLowerTriangular(A, e => e == 0);
        public static bool IsLowerTriangular(this DenseMatrix<long> A) => IsLowerTriangular(A, e => e == 0L);
        public static bool IsLowerTriangular(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsLowerTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsLowerTriangular(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsLowerTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsLowerTriangular(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsLowerTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsLowerTriangular<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return IsLowerTriangular(A, e => e.ApproximatelyEquals(zero, tolerance));
        }
        private static bool IsLowerTriangular<T>(this DenseMatrix<T> A, Func<T, bool> ApproximatelyZero) where T : new()
        {
            if (A == null) return false;

            int m = A.Rows, n = A.Columns, i, j;
            for (i = 0; i < m; ++i)
            {
                T[] A_i = A[i];
                for (j = i + 1; j < n; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns <txt>true</txt> if a matrix $A$ is a tridiagonal matrix, and <txt>false</txt> otherwise.</para>
        /// <para>
        /// This method deems a matrix to be tridiagonal if all elements $A_{ij}$ not on the middle 3 major diagonals satisfy:
        /// <ul>
        /// <li>$A_{ij} = 0$, for matrices over integer and long</li>
        /// <li>$|A_{ij}| < \epsilon$, for matrices over finite-precision arithmetic, for some tolerance level $\epsilon$</li>
        /// </ul>
        /// </para>
        /// <para></para>
        /// </summary>
        /// <name>IsTridiagonal</name>
        /// <proto>bool IsTridiagonal(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool IsTridiagonal(this DenseMatrix<int> A) => IsTridiagonal(A, a => a == 0);
        public static bool IsTridiagonal(this DenseMatrix<long> A) => IsTridiagonal(A, a => a == 0L);
        public static bool IsTridiagonal(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsTridiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsTridiagonal(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsTridiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsTridiagonal(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsTridiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsTridiagonal<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return IsTridiagonal(A, a => a.ApproximatelyEquals(zero, tolerance));
        }
        private static bool IsTridiagonal<T>(this DenseMatrix<T> A, Func<T, bool> ApproximatelyZero) where T : new()
        {
            if (A == null) return false;

            int m = A.Rows, n = A.Columns, i, j;
            for (i = 1; i < m; ++i)
            {
                T[] A_i = A[i];
                int end = Math.Min(i - 1, n);
                for (j = 0; j < end; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
                for (j = i + 2; j < n; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns <txt>true</txt> if a matrix $A$ is a upper-bidiagonal matrix, and <txt>false</txt> otherwise.</para>
        /// <para>
        /// This method classifies a matrix to be upper-bidiagonal if, $\forall 1\le{i, j}\le{n}$, $j \notin \{i, i + 1\}$,
        /// <ul>
        /// <li>$A_{ij} = 0$, for matrices over <txt>int</txt> and <txt>long</txt></li>
        /// <li>$|A_{ij}| < \epsilon$, for matrices over finite-precision arithmetic, for some tolerance level $\epsilon$</li>
        /// </ul>
        /// </para>
        /// <para></para>
        /// </summary>
        /// <name>IsUpperBidiagonal</name>
        /// <proto>bool IsUpperBidiagonal(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool IsUpperBidiagonal(this DenseMatrix<int> A) => IsUpperBidiagonal(A, a => a == 0);
        public static bool IsUpperBidiagonal(this DenseMatrix<long> A) => IsUpperBidiagonal(A, a => a == 0L);
        public static bool IsUpperBidiagonal(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsUpperBidiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsUpperBidiagonal(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsUpperBidiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsUpperBidiagonal(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsUpperBidiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsUpperBidiagonal<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return IsUpperBidiagonal(A, a => a.ApproximatelyEquals(zero, tolerance));
        }
        private static bool IsUpperBidiagonal<T>(this DenseMatrix<T> A, Func<T, bool> ApproximatelyZero) where T : new()
        {
            if (A == null) return false;

            int m = A.Rows, n = A.Columns, i, j;
            for (i = 1; i < m; ++i)
            {
                int end = Math.Min(i, n);
                T[] A_i = A[i];
                for (j = 0; j < end; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
                for (j = i + 2; j < n; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns <txt>true</txt> if a matrix $A$ is a lower-bidiagonal matrix, and <txt>false</txt> otherwise.</para>
        /// <para>
        /// This method classifies a $m \times n$ matrix to be lower-bidiagonal if, $\forall 1\le{i}\le{m}, 1\le{j}\le{n}$, $j \notin \{i - 1, i\}$,
        /// <ul>
        /// <li>$A_{ij} = 0$, for matrices over integer and long</li>
        /// <li>$|A_{ij}| < \epsilon$, for matrices over finite-precision arithmetic, for some tolerance level $\epsilon$</li>
        /// </ul>
        /// </para>
        /// <para></para>
        /// </summary>
        /// <name>IsLowerBidiagonal</name>
        /// <proto>bool IsLowerBidiagonal(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool IsLowerBidiagonal(this DenseMatrix<int> A) => IsLowerBidiagonal(A, a => a == 0);
        public static bool IsLowerBidiagonal(this DenseMatrix<long> A) => IsLowerBidiagonal(A, a => a == 0L);
        public static bool IsLowerBidiagonal(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsLowerBidiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsLowerBidiagonal(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsLowerBidiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsLowerBidiagonal(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsLowerBidiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsLowerBidiagonal<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return IsLowerBidiagonal(A, a => a.ApproximatelyEquals(zero, tolerance));
        }
        private static bool IsLowerBidiagonal<T>(this DenseMatrix<T> A, Func<T, bool> ApproximatelyZero) where T : new()
        {
            if (A == null) return false;

            int m = A.Rows, n = A.Columns, i, j;
            for (i = 1; i < m; ++i)
            {
                T[] A_i = A[i];
                int end = Math.Min(i - 1, n);
                for (j = 0; j < end; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
                for (j = i + 1; j < n; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns <txt>true</txt> if a matrix $A$ is a upper-hessenberg matrix, and <txt>false</txt> otherwise.</para>
        /// <para>
        /// This method classifies a $m \times n$ matrix to be upper-hessenberg if, $\forall 1\le{i}\le{m}$ and $1\le{j}<{\min\{i - 1, n\}}$,
        /// <ul>
        /// <li>$A_{ij} = 0$, for matrices over integer and long</li>
        /// <li>$|A_{ij}| < \epsilon$, for matrices over finite-precision arithmetic, for some tolerance level $\epsilon$</li>
        /// </ul>
        /// </para>
        /// <para></para>
        /// </summary>
        /// <name>IsUpperHessenberg</name>
        /// <proto>bool IsUpperHessenberg(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool IsUpperHessenberg(this DenseMatrix<int> A) => IsUpperHessenberg(A, a => a == 0);
        public static bool IsUpperHessenberg(this DenseMatrix<long> A) => IsUpperHessenberg(A, a => a == 0L);
        public static bool IsUpperHessenberg(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsUpperHessenberg(A, a => a.approx_zero(tolerance));
        public static bool IsUpperHessenberg(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsUpperHessenberg(A, a => a.approx_zero(tolerance));
        public static bool IsUpperHessenberg(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsUpperHessenberg(A, a => a.approx_zero(tolerance));
        public static bool IsUpperHessenberg<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return IsUpperHessenberg(A, a => a.ApproximatelyEquals(zero, tolerance));
        }
        private static bool IsUpperHessenberg<T>(this DenseMatrix<T> A, Func<T, bool> ApproximatelyZero) where T : new()
        {
            if (A == null) return false;

            int m = A.Rows, n = A.Columns, i, j;
            for (i = 1; i < m; ++i)
            {
                T[] A_i = A[i];
                int end = Math.Min(i - 1, n);
                for (j = 0; j < end; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns <txt>true</txt> if a matrix $A$ is a lower-hessenberg matrix, and <txt>false</txt> otherwise.</para>
        /// <para>
        /// This method classifies a $m \times n$ matrix to be lower-hessenberg if, $\forall 1\le{i}\le{m}$ and $i+1<{j}<{n}$,
        /// <ul>
        /// <li>$A_{ij} = 0$, for matrices over integer and long</li>
        /// <li>$|A_{ij}| < \epsilon$, for matrices over finite-precision arithmetic, for some tolerance level $\epsilon$</li>
        /// </ul>
        /// </para>
        /// <para></para>
        /// </summary>
        /// <name>IsLowerHessenberg</name>
        /// <proto>bool IsLowerHessenberg(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static bool IsLowerHessenberg(this DenseMatrix<int> A) => IsLowerHessenberg(A, a => a == 0);
        public static bool IsLowerHessenberg(this DenseMatrix<long> A) => IsLowerHessenberg(A, a => a == 0L);
        public static bool IsLowerHessenberg(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsLowerHessenberg(A, a => a.approx_zero(tolerance));
        public static bool IsLowerHessenberg(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsLowerHessenberg(A, a => a.approx_zero(tolerance));
        public static bool IsLowerHessenberg(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsLowerHessenberg(A, a => a.approx_zero(tolerance));
        public static bool IsLowerHessenberg<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return IsLowerHessenberg(A, a => a.ApproximatelyEquals(zero, tolerance));
        }
        private static bool IsLowerHessenberg<T>(this DenseMatrix<T> A, Func<T, bool> ApproximatelyZero) where T : new()
        {
            if (A == null) return false;

            int m = A.Rows, n = A.Columns, i, j;
            for (i = 1; i < m; ++i)
            {
                T[] A_i = A[i];
                for (j = i + 2; j < n; ++j)
                {
                    if (!ApproximatelyZero(A_i[j])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns whether a $n \times n$ matrix is symmetric up to a tolerance of $\epsilon$, i.e. for all $1\le{i}<{j}\le{n}$:</para>
        /// <ul>
        /// <li>$A_{ij} = A_{ji}$, for matrices over <txt>integer</txt> and <txt>long</txt>, or</li>
        /// <li>For matrices over finite-precision arithmetic,
        ///     <ul>
        ///         <li>$|A_{ij}| < \epsilon$, if $A_{ji} = 0$</li>
        ///         <li>$|A_{ij} / A_{ji} - 1| < \epsilon$, if $A_{ji} \not = 0$</li>
        ///     </ul>
        /// </li>
        /// </ul>
        /// </summary>
        /// <name>IsSymmetric</name>
        /// <proto>bool IsSymmetric(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsSymmetric(this DenseMatrix<int> A) => IsSymmetric(A, (a, b) => a == b);
        public static bool IsSymmetric(this DenseMatrix<long> A) => IsSymmetric(A, (a, b) => a == b);
        public static bool IsSymmetric(this DenseMatrix<float> A, float tolerance = Precision.FLOAT_PRECISION) => IsSymmetric(A, (a, b) => a.ApproximatelyEquals(b, tolerance));
        public static bool IsSymmetric(this DenseMatrix<double> A, double tolerance = Precision.DOUBLE_PRECISION) => IsSymmetric(A, (a, b) => a.ApproximatelyEquals(b, tolerance));
        public static bool IsSymmetric(this DenseMatrix<decimal> A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsSymmetric(A, (a, b) => a.ApproximatelyEquals(b, tolerance));
        public static bool IsSymmetric<T>(this DenseMatrix<T> A, double tolerance = Precision.DOUBLE_PRECISION) where T : Algebra<T>, new() => IsSymmetric(A, (a, b) => a.ApproximatelyEquals(b, tolerance));
        private static bool IsSymmetric<T>(this DenseMatrix<T> matrix, Func<T, T, bool> ApproximatelyEquals) where T : new()
        {
            if (matrix == null || !matrix.IsSquare) return false;

            int dim = matrix.Rows, i, j;
            T[][] A = matrix.Values;
            for (i = 0; i < dim; ++i)
            {
                T[] A_i = A[i];
                for (j = 0; j < i; ++j)
                {
                    if (!ApproximatelyEquals(A_i[j], A[j][i])) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether the two matrices $A$ and $B$ commute, i.e. $AB = BA$.
        /// </summary>
        /// <name>CommutesWith</name>
        /// <proto>bool CommutesWith(this IMatrix<T> A, IMatrix<T> B)</proto>
        /// <cat>la</cat>
        /// <param name="A">A real square matrix A</param>
        /// <param name="B">A real square matrix B with the same dimensions as A</param>
        /// <returns>True if A and B commute, i.e. AB = BA</returns>
        public static bool CommutesWith(this DenseMatrix<int> A, DenseMatrix<int> B) => CommutesWith(A, B, 0, (a, b) => a + b, (a, b) => a * b, (a, b) => a == b);
        public static bool CommutesWith(this DenseMatrix<long> A, DenseMatrix<long> B) => CommutesWith(A, B, 0L, (a, b) => a + b, (a, b) => a * b, (a, b) => a == b);
        public static bool CommutesWith(this DenseMatrix<float> A, DenseMatrix<float> B, float tolerance = Precision.FLOAT_PRECISION)
            => CommutesWith(A, B, 0.0F, (a, b) => a + b, (a, b) => a * b, (a, b) => a.ApproximatelyEquals(b, tolerance));
        public static bool CommutesWith(this DenseMatrix<double> A, DenseMatrix<double> B, double tolerance = Precision.DOUBLE_PRECISION) 
            => CommutesWith(A, B, 0.0, (a, b) => a + b, (a, b) => a * b, (a, b) => a.ApproximatelyEquals(b, tolerance));
        public static bool CommutesWith(this DenseMatrix<decimal> A, DenseMatrix<decimal> B, decimal tolerance = Precision.DECIMAL_PRECISION)
            => CommutesWith(A, B, 0.0M, (a, b) => a + b, (a, b) => a * b, (a, b) => a.ApproximatelyEquals(b, tolerance));
        public static bool CommutesWith<T>(this T[,] A, T[,] B, double tolerance = Precision.DOUBLE_PRECISION) where T : Ring<T>, new()
        {
            T zero = new T().AdditiveIdentity;
            return CommutesWith(A, B, zero, (a, b) => a.Add(b), (a, b) => a.Multiply(b), (a, b) => a.ApproximatelyEquals(b, tolerance));
        }
        private static bool CommutesWith<T>(this DenseMatrix<T> A, DenseMatrix<T> B, T zero, Func<T, T, T> Add, Func<T, T, T> Multiply, Func<T, T, bool> ApproximatelyEquals) where T : new()
        {
            if (A == null || B == null) return false;

            // Check compatible dimensions - a and b must be square and of the same dimensions
            int arow = A.Rows, acol = A.Columns, brow = B.Rows, bcol = B.Columns;
            if (arow != acol || brow != bcol || arow != brow) return false;

            int i, j, k;
            for (i = 0; i < arow; ++i)
            {
                T[] A_i = A[i], B_i = B[i];
                for (j = 0; j < acol; ++j)
                {
                    T ab = zero, ba = zero;
                    for (k = 0; k < bcol; ++k)
                    {
                        // Calculate (AB)[i, j] = sum(A[i, k] * B[k, j], 0 <= k < m)
                        ab = Add(ab, Multiply(A_i[k], B[k][j]));

                        // Calculate (BA)[i, j] = sum(B[i, k] * A[k, j], 0 <= k < m)
                        ba = Add(ba, Multiply(B_i[k], A[k][j]));
                    }
                    if (!ApproximatelyEquals(ab, ba)) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns whether complex matrix $A\in\mathbb{C}^{n \times n}$ is hermitian, subject to a tolerance level of 'tolerance'.</para>
        /// <para>I.e. $A_{ij} = \overline{A_{ji}}, \forall 1\le{i}<{j}\le{n}$</para>
        /// </summary>
        /// <name>IsHermitian</name>
        /// <proto>bool IsHermitian(this IMatrix<Complex> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsHermitian(this DenseMatrix<Complex> matrix, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (matrix == null || !matrix.IsSquare) return false;

            int rows = matrix.Rows, r, c;
            Complex[][] A = matrix.Values;
            for (r = 0; r < rows; ++r)
            {
                Complex[] A_r = A[r];
                for (c = 0; c < r; ++c)
                {
                    if (!A_r[c].IsConjugateOf(A[c][r], tolerance))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// <para>Returns whether a complex matrix $A\in\mathbb{C}^{n \times n}$ is skew-hermitian, up to a precision level $\epsilon$</para>
        /// <para>I.e. $\forall 1\le{i}<{j}\le{n}, A_{ij} \approx_\epsilon -\overline{A_{ji}}$, where</para>
        /// <para>$a\approx_\epsilon b$ if:</para>
        /// <ul>
        /// <li>$a = b$, if $a$ and $b$ are <txt>int</txt> or <txt>long</txt></li>
        /// <li>$|a| < 0$ if $b = 0$, $|a/b-1| < \epsilon$ if $b \not = 0$, for $a, b$ belonging to a finite-precision field.</li>
        /// </ul>
        /// </summary>
        /// <name>IsSkewHermitian</name>
        /// <proto>bool IsSkewHermitian(this IMatrix<Complex> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsSkewHermitian(this DenseMatrix<Complex> matrix, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (matrix == null || !matrix.IsSquare) return false;

            Complex[][] A = matrix.Values;
            int rows = matrix.Rows, r, c;

            for (r = 0; r < rows; ++r)
            {
                Complex[] A_r = A[r];
                for (c = 0; c < r; ++c)
                {
                    Complex A_cr = A[c][r];
                    if (!A_r[c].Real.ApproximatelyEquals(-A_cr.Real, tolerance)) return false;
                    if (!A_r[c].Imaginary.ApproximatelyEquals(A_cr.Imaginary, tolerance)) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>
        /// Determines whether a square matrix $A\in\mathbb{C}^{n\times n}$ is normal (up to precision level $\epsilon$), i.e. $A^*A = AA^*$ where $A^*$ is the 
        /// conjugate transpose of $A$.
        /// </para>
        /// <para>
        /// i.e. if $|\text{Re}(A^*A)_{ij} / \text{Re}(AA^*)_{ij} - 1| < \epsilon$ and $|\text{Im}(A^*A)_{ij} / \text{Im}(AA^*)_{ij} - 1| < \epsilon, \forall 1\le{i, j}\le{n}$.
        /// </para>
        /// <para>
        /// If a non-square or degenerate matrix is provided, the method will return null.
        /// </para>
        /// </summary>
        /// <name>IsNormal</name>
        /// <proto>bool IsNormal(this IMatrix<Complex> A)</proto>
        /// <cat>la</cat>
        /// <param name="a"></param>
        /// <param name="tolerance"></param>
        /// <returns>true if the matrix a is normal</returns>
        public static bool IsNormal(this DenseMatrix<Complex> matrix, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (matrix == null || !matrix.IsSquare)
            {
                return false;
            }

            int dim = matrix.Rows, i, j, k;
            DenseMatrix<Complex> conj = matrix.ConjugateTranspose();

            Complex[][] a = matrix.Values;
            for (i = 0; i < dim; i++)
            {
                Complex[] a_i = a[i], conj_i = conj[i];
                for (j = 0; j < dim; j++)
                {
                    Complex leftSum = Complex.Zero, rightSum = Complex.Zero;
                    for (k = 0; k < dim; k++)
                    {
                        leftSum.IncrementBy(conj_i[k].Multiply(a[k][j]));
                        rightSum.IncrementBy(a_i[k].Multiply(conj[k][j]));
                    }

                    if (!leftSum.ApproximatelyEquals(rightSum, tolerance))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// <para>
        /// Returns whether a square matrix $A\in\mathbb{F}^{n \times n}$ is positive definite, i.e. for all vectors $x\in\mathbb{F}^n$ of compatible dimension, $x^TAx > 0$. 
        /// This method uses the fact that a matrix is positive definite if its symmetric and all its pivots are positive.
        /// </para>
        /// </summary>
        /// <name>IsPositiveDefinite</name>
        /// <proto>bool IsPositiveDefinite(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="matrix"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsPositiveDefinite(this DenseMatrix<float> matrix, float tolerance = Precision.FLOAT_PRECISION) => IsPositiveDefinite(matrix, A => A.IsSymmetric(), A => A.ToRowEchelonForm().Values, x => x < -tolerance);
        public static bool IsPositiveDefinite(this DenseMatrix<double> matrix, double tolerance = Precision.DOUBLE_PRECISION) => IsPositiveDefinite(matrix, A => A.IsSymmetric(), A => A.ToRowEchelonForm().Values, x => x < -tolerance);
        public static bool IsPositiveDefinite(this DenseMatrix<decimal> matrix, decimal tolerance = Precision.DECIMAL_PRECISION) => IsPositiveDefinite(matrix, A => A.IsSymmetric(), A => A.ToRowEchelonForm().Values, x => x < -tolerance);
        public static bool IsPositiveDefinite(this DenseMatrix<Complex> matrix, double tolerance = Precision.DOUBLE_PRECISION) => IsPositiveDefinite(matrix, A => A.IsSymmetric(), A => A.ToRowEchelonForm().Values, x => x.Real < -tolerance);
        private static bool IsPositiveDefinite<T>(DenseMatrix<T> matrix, Func<DenseMatrix<T>, bool> IsSymmetric, Func<DenseMatrix<T>, T[][]> ToRowEchelonForm, Func<T, bool> IsNegative) where T : new()
        {
            if (!IsSymmetric(matrix)) return false;

            T[][] re = ToRowEchelonForm(matrix);

            int rows = matrix.Rows;
            for (int i = 0; i < rows; ++i)
            {
                if (IsNegative(re[i][i]))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// <para>
        /// Returns whether a square matrix $A\in\mathbb{F}^{n \times n}$ is orthogonal.
        /// </para>
        /// <para>Supported for matrices over the types <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt>, <txt>T : Ring&lt;T&gt;</txt></para>
        /// </summary>
        /// <name>IsOrthogonal</name>
        /// <proto>bool IsOrthogonal(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="matrix"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsOrthogonal(this DenseMatrix<float> matrix, float tolerance = Precision.FLOAT_PRECISION) => IsOrthogonal(matrix, new FloatBLAS(), x => x.approx_zero(tolerance), x => x.approx_one(tolerance));
        public static bool IsOrthogonal(this DenseMatrix<double> matrix, double tolerance = Precision.DOUBLE_PRECISION) => IsOrthogonal(matrix, new DoubleBLAS(), x => x.approx_zero(tolerance), x => x.approx_one(tolerance));
        public static bool IsOrthogonal(this DenseMatrix<decimal> matrix, decimal tolerance = Precision.DECIMAL_PRECISION) => IsOrthogonal(matrix, new DecimalBLAS(), x => x.approx_zero(tolerance), x => x.approx_one(tolerance));
        public static bool IsOrthogonal<T>(this DenseMatrix<T> matrix, double tolerance = Precision.DOUBLE_PRECISION) where T : Ring<T>, new()
        {
            T obj = new T();
            T zero = obj.AdditiveIdentity, one = obj.MultiplicativeIdentity;
            return IsOrthogonal(matrix, new RingBLAS<T>(), x => x.ApproximatelyEquals(zero, tolerance), x => x.ApproximatelyEquals(one, tolerance));
        }
        private static bool IsOrthogonal<T>(this DenseMatrix<T> matrix, IBLAS<T> blas, Func<T, bool> IsZero, Func<T, bool> IsOne) where T : new()
        {
            if (matrix == null || !matrix.IsSquare) return false;

            int rows = matrix.Rows, i, j;

            T[][] A = matrix.Values;
            for (i = 0; i < rows; ++i)
            {
                T[] A_i = A[i];
                for (j = 0; j < rows; ++j)
                {
                    T dot = blas.DOT(A_i, A[j], 0, rows);
                    if (i == j)
                    {
                        if (!IsOne(dot)) return false;
                    }
                    else
                    {
                        if (!IsZero(dot)) return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// <para>
        /// Returns whether a complex square matrix $A\in\mathbb{C}^{n \times n}$ is unitary.
        /// </para>
        /// <para>
        /// If $A_i$ is the $i$-th column of $A$, then the method returns <txt>true</txt> if $\forall 1\le{i, j}\le{n}$
        /// $$\begin{cases}
        /// |A_i^TA_j - 1| < \epsilon &\text{if } i = j \\
        /// |A_i^TA_j| < \epsilon &\text{if } i \not= j
        /// \end{cases}$$
        /// For some precision level $\epsilon > 0$.
        /// </para>
        /// </summary>
        /// <name>IsUnitary</name>
        /// <proto>bool IsUnitary(this IMatrix<Complex> A)</proto>
        /// <cat>la</cat>
        /// <param name="matrix"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsUnitary(this DenseMatrix<Complex> matrix, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (matrix == null || !matrix.IsSquare) return false;

            int rows = matrix.Rows, i, j, k;

            Complex[][] A = matrix.Values;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[i];
                for (j = 0; j < rows; ++j)
                {
                    Complex[] A_j = A[j];
                    double re = 0.0, im = 0.0;
                    for (k = 0; k < rows; ++k)
                    {
                        Complex z = A_i[k], w = A_j[k];

                        // += z * conj(w)
                        re += z.Real * w.Real + z.Imaginary * w.Imaginary;
                        im += z.Imaginary * w.Real - z.Real * w.Imaginary;
                    }

                    if (!im.approx_zero(tolerance))
                    {
                        return false;
                    }
                    if (i == j)
                    {
                        if (!re.approx_one(tolerance)) return false;
                    }
                    else
                    {
                        if (!im.approx_zero(tolerance)) return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// <para>
        /// For two matrices $A$ and $B$ over finite-precision arithmetic, returns <txt>true</txt> if $A$ and $B$ are approximately equal, up to precision $\epsilon$.
        /// </para>
        /// <para>
        /// The method returns <txt>true</txt> if, $\forall 1\le{i}\le{m}, 1\le{j}\le{n}$
        /// $$\begin{cases}
        /// |A_{ij}| < \epsilon &\text{if } B_{ij} = 0 \\
        /// |A_{ij} / B_{ij} - 1| < \epsilon &\text{if } B_{ij} \not= 0
        /// \end{cases}$$
        /// For some precision level $\epsilon > 0$.
        /// </para>
        /// <!--inputs-->
        /// <!--returns-->
        /// 
        /// <pre><code class="cs">
        /// DenseMatrix&lt;Complex&gt; A = DenseMatrix.Random&lt;Complex&gt;(10, 10);
        /// DenseMatrix&lt;Complex&gt; A_inv = A.Invert(); 
        /// 
        /// // true
        /// Console.WriteLine(A.Multiply(A_inv).ApproximatelyEquals(DenseMatrix.Identity&lt;Complex&gt;(10));
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>ApproximatelyEquals</name>
        /// <proto>bool ApproximatelyEquals(this IMatrix<T> A, IMatrix<T> B, T tolerance)</proto>
        /// <cat>la</cat>
        /// <param name="A">The first matrix to be compared, $A$.</param>
        /// <param name="B">The second matrix to be compared, $B$.</param>
        /// <param name="tolerance">
        /// <b>Optional</b>, defaults to: <br/>
        /// <txt>1e-6</txt> for matrices over <txt>float</txt>, <br/>
        /// <txt>1e-8</txt> for matrices over <txt>double</txt>, <br/>
        /// <txt>1e-12</txt> for matrices over <txt>decimal</txt> and <br/>
        /// <txt>1e-8</txt> for matrices over all types <txt>T</txt> implementing the <txt>Algebra&lt;T&gt;</txt> interface. <br/>
        /// The precision level $\epsilon$.
        /// </param>
        /// <returns>A <txt>bool</txt> representing whether <txt>A</txt> and <txt>B</txt> are equal within <txt>tolerance</txt>.</returns>
        public static bool ApproximatelyEquals(this DenseMatrix<float> A, DenseMatrix<float> B, float tolerance = Precision.FLOAT_PRECISION)
            => approximately_equals(A, B, (a, b) => a.ApproximatelyEquals(b, tolerance));
        public static bool ApproximatelyEquals(this DenseMatrix<double> A, DenseMatrix<double> B, double tolerance = Precision.DOUBLE_PRECISION)
            => approximately_equals(A, B, (a, b) => a.ApproximatelyEquals(b, tolerance));
        public static bool ApproximatelyEquals(this DenseMatrix<decimal> A, DenseMatrix<decimal> B, decimal tolerance = Precision.DECIMAL_PRECISION)
            => approximately_equals(A, B, (a, b) => a.ApproximatelyEquals(b, tolerance));
        public static bool ApproximatelyEquals<T>(this DenseMatrix<T> A, DenseMatrix<T> B, double tolerance = Precision.DOUBLE_PRECISION) where T : Algebra<T>, new()
            => approximately_equals(A, B, (a, b) => a.ApproximatelyEquals(b, tolerance));
        private static bool approximately_equals<T>(this DenseMatrix<T> A, DenseMatrix<T> B, Func<T, T, bool> ApproximatelyEqual) where T : new()
        {
            if (A == null || B == null)
            {
                return false;
            }
            if (A.Rows != B.Rows || A.Columns != B.Columns)
            {
                return false;
            }

            int m = A.Rows, n = A.Columns, i, j;
            for (i = 0; i < m; ++i)
            {
                T[] A_i = A[i], B_i = B[i];
                for (j = 0; j < n; ++j)
                {
                    if (!ApproximatelyEqual(A_i[j], B_i[j]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}
