﻿using LinearLite.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    /// <summary>
    /// Dense matrix extensions which require matrices to be over the complex field
    /// </summary>
    public static class DenseMatrixComplexExtensions
    {
        /// <summary>
        /// <para>Returns $A^H$, the conjugate transpose of $A\in\mathbb{C}^{m \times n}$, defined elementwise as 
        /// $$(A^H)_{ij} = \overline{A_{ji}},\> \forall 1\le{i}\le{m}, 1\le{j}\le{n}$$</para>
        /// <para>
        /// Parallel version: <a href="#ConjugateTransposeParallel"><txt><b>IMatrix<Complex> ConjugateTransposeParallel(this IMatrix<Complex> A)</b></txt></a>
        /// </para>
        /// </summary>
        /// <name>ConjugateTranspose</name>
        /// <proto>IMatrix<Complex> ConjugateTranspose(this IMatrix<Complex> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseMatrix<Complex> ConjugateTranspose(this DenseMatrix<Complex> A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.Rows, cols = A.Columns, r, c;

            DenseMatrix<Complex> matrix = new DenseMatrix<Complex>(cols, rows);
            Complex[][] transpose = matrix.Values;
            for (r = 0; r < rows; r++)
            {
                Complex[] A_r = A[r];
                for (c = 0; c < cols; c++)
                {
                    transpose[c][r] = A_r[c].Conjugate();
                }
            }
            return matrix;
        }
        /// <summary>
        /// Calculates A^H in parallel, the conjugate transpose of A, where (A^H)(i, j) = conjugate(A(j, i))
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseMatrix<Complex> ConjugateTransposeParallel(this DenseMatrix<Complex> A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.Rows, cols = A.Columns;

            Complex[][] transpose = new Complex[cols][], values = A.Values;
            Parallel.ForEach(Partitioner.Create(0, cols), range =>
            {
                int min = range.Item1, max = range.Item2, r, c;
                for (c = 0; c < cols; c++)
                {
                    Complex[] row = new Complex[rows];
                    for (r = 0; r < rows; r++)
                    {
                        row[r] = values[r][c].Conjugate();
                    }
                }
            });
            
            return new DenseMatrix<Complex>(transpose);
        }
    }
}
