﻿using LinearLite.BLAS;
using LinearLite.Decomposition;
using LinearLite.Matrices;
using LinearLite.Matrices.Strassen;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    /// <summary>
    /// Extension methods to invert matrices of type DenseMatrix<T> where T is either:
    /// - a primitive of type int, long, float, double or decimal, or
    /// - a custom algebra implementing the Field<T> interface
    /// 
    /// Note that even though Complex implements Field<Complex>, the implementation here 
    /// is more efficient and more numerically stable, so it is maintained separately 
    /// to the more general Field method. 
    /// </summary>
    public static class DenseMatrixInversionExtensions
    {
        #region Gaussian elimination inversion algorithms

        /// <summary>
        /// <para>
        /// Calculates and returns the inverse of a square matrix $A$. The original matrix is unchanged.
        /// </para>
        /// <para>If $A$ is not square, an <txt>InvalidOperationException</txt> will be thrown.</para>
        /// </summary>
        /// <name>Invert</name>
        /// <proto>IMatrix<T> Invert(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="x">The matrix to be inverted</param>
        /// <returns>The inverted matrix (the original matrix is unchanged)</returns>
        public static DenseMatrix<double> Invert(this DenseMatrix<double> A)
        {
            if (A == null) throw new ArgumentNullException();
            if (!A.IsSquare) throw new InvalidOperationException();

            int n = A.Rows, i_max, i, j, k;
            double order, max, alpha;

            // shape into correct order
            order = 0;
            for (i = 0; i < n; i++)
            {
                double[] A_i = A[i];
                for (j = 0; j < n; j++)
                {
                    order += Math.Log(Math.Abs(A_i[j]) + 0.01);
                }
            }
            order = Math.Exp(order / (n * n));

            // x_norm stores the values of x after order adjustment
            double[][] A_norm = MatrixInternalExtensions.JZero(n, n);
            for (i = 0; i < n; i++)
            {
                double[] A_norm_i = A_norm[i], A_i = A[i];
                for (j = 0; j < n; j++)
                {
                    A_norm_i[j] = A_i[j] / order;
                }
            }

            double[][] inv = MatrixInternalExtensions.JIdentity(n);
            for (k = 0; k < n - 1; k++)
            {
                // find i_max = argmax(x[i,k] for i = k, ..., n) - for numerical stability
                max = Math.Abs(A_norm[k + 1][k]);
                i_max = k + 1;
                for (i = k + 2; i < n; i++)
                {
                    double abs = Math.Abs(A_norm[i][k]);
                    if (abs > max)
                    {
                        i_max = i;
                        max = abs;
                    }
                }

                // swap row k with row i_max
                double[] temp = A_norm[i_max];
                A_norm[i_max] = A_norm[k];
                A_norm[k] = temp;

                temp = inv[i_max];
                inv[i_max] = inv[k];
                inv[k] = temp;

                // for each row below k, i.e. for i = k + 1 : n
                double[] A_norm_k = A_norm[k], inv_k = inv[k];
                for (i = k + 1; i < n; i++)
                {
                    double[] A_norm_i = A_norm[i], inv_i = inv[i];
                    alpha = A_norm_i[k] / A_norm_k[k];

                    for (j = k + 1; j < n; j++)
                    {
                        A_norm_i[j] -= A_norm_k[j] * alpha;
                    }
                    for (j = 0; j < n; j++)
                    {
                        inv_i[j] -= inv_k[j] * alpha;
                    }
                    A_norm_i[k] = 0;
                }
            }

            // going back upwards
            for (k = n - 1; k >= 0; k--)
            {
                double[] A_norm_k = A_norm[k], inv_k = inv[k];
                for (i = k - 1; i >= 0; i--)
                {
                    double[] A_norm_i = A_norm[i], inv_i = inv[i];
                    alpha = A_norm_i[k] / A_norm_k[k];
                    for (j = i + 1; j < n; j++)
                    {
                        A_norm_i[j] -= A_norm_k[j] * alpha;
                    }
                    for (j = 0; j < n; j++)
                    {
                        inv_i[j] -= inv_k[j] * alpha;
                    }
                }
            }

            // normalise - with order
            for (k = 0; k < n; k++)
            {
                alpha = A_norm[k][k] * order;

                double[] inv_k = inv[k];
                for (i = 0; i < n; i++)
                    inv_k[i] /= alpha;
            }

            return new DenseMatrix<double>(inv);
        }
        public static DenseMatrix<float> Invert(this DenseMatrix<float> A)
        {
            return Invert(A, new FloatBLAS(), x => 1.0f / x, x => Math.Abs(x),
                        (x, s) =>
                        {
                            if (s > 0) FloatBLAS.dSCAL(x, 1.0 / (1 << s), 0, x.Length);
                            else if (s < 0) FloatBLAS.dSCAL(x, 1 << Math.Abs(s), 0, x.Length);
                        },
                        (x, s) => 
                        {
                            if (s > 0) return (1 << s) * x;
                            if (s < 0) return 1.0F / (1 << Math.Abs(s)) * x;
                            return x;
                        });
        }
        public static DenseMatrix<decimal> Invert(this DenseMatrix<decimal> A)
        {
            DecimalBLAS blas = new DecimalBLAS();
            return Invert(A, blas, x => 1.0m / x,
                        x => (double)Math.Abs(x),
                        (x, s) =>
                        {
                            if (s > 0) blas.SCAL(x, 1.0M / (1 << s), 0, x.Length);
                            else if (s < 0) blas.SCAL(x, 1 << Math.Abs(s), 0, x.Length);  // Shift bits left
                        },
                        (x, s) => 
                        {
                            if (s > 0) return (1 << s) * x;
                            if (s < 0) return 1.0M / (1 << Math.Abs(s)) * x;
                            return x;
                        });
        }
        public static DenseMatrix<Complex> Invert(this DenseMatrix<Complex> A)
        {
            ComplexBLAS blas = new ComplexBLAS();
            return Invert(A, blas,
                x => x.MultiplicativeInverse(),
                x => x.Modulus(),
                (x, s) => // Shift bits left
                {
                    if (s > 0) ComplexBLAS.dSCAL(x, 1.0 / (1 << s), 0, x.Length);
                    else if (s < 0) ComplexBLAS.dSCAL(x, 1 << Math.Abs(s), 0, x.Length);
                },
                (x, s) => 
                {
                    if (s > 0) return x.Multiply(1 << s);
                    if (s < 0) return x.Multiply(1.0 / (1 << Math.Abs(s)));
                    return x;
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="zero"></param>
        /// <param name="one"></param>
        /// <param name="Inverse"></param>
        /// <param name="Multiply"></param>
        /// <param name="Norm"></param>
        /// <param name="sub_AXPY">subtraction version of the xAXPY BLAS 1 operation</param>
        /// <param name="SCAL">SCAL BLAS 1 operation</param>
        /// <returns></returns>
        private static DenseMatrix<T> Invert<T>(this DenseMatrix<T> A, IBLAS<T> blas, Func<T, T> Inverse, Func<T, double> Norm, Action<T[], int> ShiftBitsLeft, Func<T, int, T> ShiftBitRight) where T : new()
        {
            if (A == null) throw new ArgumentNullException();
            if (!A.IsSquare) throw new InvalidOperationException();

            int n = A.Rows, i_max, i, j, k;
            T alpha, zero = blas.Zero;

            // shape into correct order
            double order = 0;
            for (i = 0; i < n; ++i)
            {
                T[] A_i = A[i];
                for (j = 0; j < n; ++j)
                {
                    order += Math.Log(Norm(A_i[j]) + 0.01);
                }
            }

            int shift = (int)Math.Round((order / (n * n)) / Math.Log(2));

            // x_norm stores the values of x after order adjustment
            T[][] A_norm = new T[n][];
            for (i = 0; i < n; i++)
            {
                T[] A_norm_i = A[i].Copy();
                ShiftBitsLeft(A_norm_i, shift);
                A_norm[i] = A_norm_i;
            }

            T[][] inv = MatrixInternalExtensions.JIdentity(zero, blas.One, n);
            double max;
            for (k = 0; k < n - 1; k++)
            {
                // find i_max = argmax(x[i,k] for i = k, ..., n) - for numerical stability
                i_max = k + 1;
                max = Norm(A_norm[i_max][k]);
                for (i = k + 2; i < n; i++)
                {
                    double norm = Norm(A_norm[i][k]);
                    if (norm > max)
                    {
                        i_max = i;
                        max = norm;
                    }
                }

                // swap row k with row i_max
                T[] temp = A_norm[i_max];
                A_norm[i_max] = A_norm[k];
                A_norm[k] = temp;

                temp = inv[i_max];
                inv[i_max] = inv[k];
                inv[k] = temp;

                // for each row below k, i.e. for i = k + 1 : n
                T[] A_norm_k = A_norm[k], inv_k = inv[k];
                T factor = Inverse(A_norm_k[k]);
                for (i = k + 1; i < n; i++)
                {
                    T[] A_norm_i = A_norm[i];
                    alpha = blas.Multiply(A_norm_i[k], factor);

                    blas.YSAX(A_norm_i, A_norm_k, alpha, k + 1, n);
                    blas.YSAX(inv[i], inv_k, alpha, 0, n);
                    A_norm_i[k] = zero;
                }
            }

            // going back upwards
            for (k = n - 1; k >= 0; k--)
            {
                T[] A_norm_k = A_norm[k], inv_k = inv[k];
                T factor = Inverse(A_norm_k[k]);
                for (i = k - 1; i >= 0; i--)
                {
                    alpha = blas.Multiply(A_norm[i][k], factor);
                    blas.YSAX(A_norm[i], A_norm_k, alpha, i + 1, n);
                    blas.YSAX(inv[i], inv_k, alpha, 0, n);
                }
            }

            // normalise - with order
            for (k = 0; k < n; k++)
            {
                alpha = Inverse(ShiftBitRight(A_norm[k][k], shift));
                blas.SCAL(inv[k], alpha, 0, n);
            }

            return new DenseMatrix<T>(inv);
        }

        /// <summary>
        /// Calculates the inverse of a square matrix over any Field<T>
        /// This implementation uses Gaussian elimination and has complexity O(n^3)
        /// </summary>
        /// <typeparam name="T">Any field</typeparam>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseMatrix<T> Invert<T>(this DenseMatrix<T> A) where T : Field<T>, new()
        {
            if (A == null) throw new ArgumentNullException();
            if (!A.IsSquare) throw new InvalidOperationException();

            int n = A.Rows, i, j, k;
            T[][] cpy = MatrixInternalExtensions.JMatrix<T>(n, n);
            cpy.CopyFrom(A.Values);

            T e = cpy[0][0];
            T[][] inv = MatrixInternalExtensions.JIdentity(e, n);

            for (k = 0; k < n - 1; k++)
            {
                T[] cpy_k = cpy[k], inv_k = inv[k];

                // for each row below k, i.e. for i in [k + 1, n)
                T factor = cpy_k[k].MultiplicativeInverse();
                for (i = k + 1; i < n; i++)
                {
                    T[] cpy_i = cpy[i], inv_i = inv[i];
                    T alpha = cpy_i[k].Multiply(factor);

                    for (j = k + 1; j < n; j++)
                    {
                        cpy_i[j] = cpy_i[j].Subtract(cpy_k[j].Multiply(alpha));
                    }
                    for (j = 0; j < n; j++)
                    {
                        inv_i[j] = inv_i[j].Subtract(inv_k[j].Multiply(alpha));
                    }
                    cpy_i[k] = e.AdditiveIdentity;
                }
            }

            // going back upwards
            for (k = n - 1; k >= 0; k--)
            {
                T[] cpy_k = cpy[k], inv_k = inv[k];
                T factor = cpy_k[k].MultiplicativeInverse();
                for (i = k - 1; i >= 0; i--)
                {
                    T[] cpy_i = cpy[i], inv_i = inv[i];
                    T alpha = cpy_i[k].Multiply(factor);

                    for (j = i + 1; j < n; j++)
                    {
                        cpy_i[j] = cpy_i[j].Subtract(cpy_k[j].Multiply(alpha));
                    }
                    for (j = 0; j < n; j++)
                    {
                        inv_i[j] = inv_i[j].Subtract(inv_k[j].Multiply(alpha));
                    }
                }
            }

            // normalise - with order
            for (k = 0; k < n; k++)
            {
                T alpha = cpy[k][k];
                T[] inv_k = inv[k];
                for (i = 0; i < n; i++)
                {
                    inv_k[i] = inv_k[i].Divide(alpha);
                }
            }

            return new DenseMatrix<T>(inv);
        }


        #endregion

        #region Strassen inversion algorithm 

        /// <summary>
        /// Use Strassen's algorithm to invert a square matrix A, as per Strassen's original paper, 
        /// available at https://doi.org/10.1007/BF02165411.
        /// 
        /// Algorithm overview: 
        /// M_1 := inv(A_11)
        /// M_2 := A_21 * M_1
        /// M_3 := M_1 * A_12
        /// M_4 := A_21 * M_3
        /// M_5 := M_4 - A_22
        /// M_6 := inv(M_5)
        /// 
        /// C_12 := M_3 * M_6
        /// C_21 := M_6 * M_2
        /// M_7 := M_3 * C_21
        /// C_11 := M_2 - M_7
        /// C_22 := -M_6
        /// 
        /// Note that matrix is altered in the process,
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="matrix">The matrix to invert</param>
        /// <param name="inverse">The inverse of the original matrix.</param>
        /// <param name="preserveOriginalMatrix">
        ///     If true, the matrix 'matrix' will not be altered. The algorithm will be slightly more performant if set to 'false'
        /// </param>
        public static void InvertStrassen<T>(this DenseMatrix<T> matrix, DenseMatrix<T> inverse, bool preserveOriginalMatrix = true) where T : new()
        {
            MatrixChecks.CheckNotNull(matrix, inverse);
            MatrixChecks.CheckIsSquare(matrix);
            MatrixChecks.CheckIsSquare(inverse);
            if (matrix.Rows != inverse.Rows)
            {
                throw new InvalidOperationException("The dimensions of the matrix and its inverse does not match");
            }

            int n = matrix.Rows;

            IStrassenInstructionSet<T> instructions;
            if (typeof(T) == typeof(float))
            {
                instructions = (IStrassenInstructionSet<T>)new FloatStrassenInstructionSet();
            }
            else if (typeof(T) == typeof(double))
            {
                instructions = (IStrassenInstructionSet<T>)new DoubleStrassenInstructionSet();
            }
            else if (typeof(T) == typeof(decimal))
            {
                instructions = (IStrassenInstructionSet<T>)new DecimalStrassenInstructionSet();
            }
            else if (typeof(T) == typeof(Complex))
            {
                instructions = (IStrassenInstructionSet<T>)new ComplexStrassenInstructionSet();
            }
            else
            {
                throw new NotImplementedException();
            }

            // Strassen algorithm will take care of invalid type exceptions
            StrassenInversionAlgorithm<T> strassen = new StrassenInversionAlgorithm<T>(n, instructions);

            DenseMatrix<T> A;
            if (preserveOriginalMatrix)
            {
                A = matrix.Copy();
            }
            else
            {
                A = matrix;
            }

            strassen.Invert(A.Values, inverse.Values);
        }

        /// <summary>
        /// <para>
        /// Given a matrix, return its inverse calculated using Strassen's method, as per Strassen's original paper, 
        /// available at <a href="https://doi.org/10.1007/BF02165411">https://doi.org/10.1007/BF02165411</a>.
        /// <!--inputs-->
        /// </para>
        /// </summary>
        /// <name>InvertStrassen</name>
        /// <proto>IMatrix<T> InvertStrassen(this IMatrix<T> A, bool preserveOriginalMatrix = true)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="matrix">The matrix to be inverted.</param>
        /// <param name="preserveOriginalMatrix">
        /// <b>Optional</b>, defaults to <txt>true</txt> <br/>
        /// If <txt>true</txt>, the matrix <txt>matrix</txt> will not be altered. The method will be slightly more performant if set to <txt>false</txt>.
        /// </param>
        /// <returns></returns>
        public static DenseMatrix<T> InvertStrassen<T>(this DenseMatrix<T> matrix, bool preserveOriginalMatrix = true) where T : new()
        {
            MatrixChecks.CheckNotNull(matrix);

            DenseMatrix<T> inverse = new DenseMatrix<T>(matrix.Rows, matrix.Rows);
            InvertStrassen(matrix, inverse, preserveOriginalMatrix);
            return inverse;
        }
        #endregion

        #region Moore-Penrose Inverse

        /// <summary>
        /// <para>
        /// Calculate the <a href="https://en.wikipedia.org/wiki/Moore%E2%80%93Penrose_inverse">Moore-Penrose pseudo-inverse</a>, $A^+$, of a matrix 
        /// $A$ using the <txt>'geninv'</txt> function as documented in 'Fast Computation of Moore-Penrose Inverse Matrices', Pierre Courrieu (2005).
        /// </para>
        /// $A^+$ is a unique matrix that satisfies the following conditions:
        /// <ul>
        /// <li>$AA^+A=A$</li>
        /// <li>$A^+AA^+=A^+$</li>
        /// <li>$AA^+$ and $A^+A$ are Hermitian</li>
        /// </ul>
        /// </summary>
        /// <name>PseudoInvert</name>
        /// <proto>IMatrix<T> PseudoInvert(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="G"></param>
        /// <returns></returns>
        public static DenseMatrix<double> PseudoInvert(this DenseMatrix<double> G)
        {
            return PseudoInvert(G, (A, B) => A.Multiply(B), A => { A.CholeskyDecompose(out DenseMatrix<double> L); return L; }, A => A.Invert());
        }
        public static DenseMatrix<float> PseudoInvert(this DenseMatrix<float> G)
        {
            return PseudoInvert(G, (A, B) => A.Multiply(B), A => { A.CholeskyDecompose(out DenseMatrix<float> L); return L; }, A => A.Invert());
        }
        public static DenseMatrix<decimal> PseudoInvert(this DenseMatrix<decimal> G)
        {
            return PseudoInvert(G, (A, B) => A.Multiply(B), A => { A.CholeskyDecompose(out DenseMatrix<decimal> L); return L; }, A => A.Invert());
        }
        public static DenseMatrix<Complex> PseudoInvert(this DenseMatrix<Complex> G)
        {
            return PseudoInvert(G, (A, B) => A.Multiply(B), A => { A.CholeskyDecompose(out DenseMatrix<Complex> L); return L; }, A => A.Invert());
        }
        private static DenseMatrix<T> PseudoInvert<T>(this DenseMatrix<T> G, 
            Func<DenseMatrix<T>, DenseMatrix<T>, DenseMatrix<T>> Multiply,
            Func<DenseMatrix<T>, DenseMatrix<T>> CholeskyDecompose,
            Func<DenseMatrix<T>, DenseMatrix<T>> Invert) where T : new()
        {
            MatrixChecks.CheckNotNull(G);
            MatrixChecks.CheckIsSquare(G);

            DenseMatrix<T> Gt = G.Transpose(), A;

            bool transposed = G.Rows < G.Columns;
            if (transposed)
            {
                A = Multiply(G, Gt);
            }
            else
            {
                A = Multiply(Gt, G);
            }

            DenseMatrix<T> L = CholeskyDecompose(A);
            DenseMatrix<T> Lt = L.Transpose();
            DenseMatrix<T> M = Invert(Multiply(Lt, L));

            if (transposed)
            {
                return Multiply(Multiply(Multiply(Multiply(Gt, L), M), M), Lt);
            }
            else
            {
                return Multiply(Multiply(Multiply(Multiply(L, M), M), Lt), Gt);
            }
        }

        #endregion

        #region Left-inverse 

        /// <summary>
        /// Calculate and return the left-inverse of a matrix $A\in\mathbb{F}^{m \times n}$, where $m \ge n$
        /// </summary>
        /// <name>LeftInverse</name>
        /// <proto>IMatrix<T> LeftInverse(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseMatrix<double> LeftInverse(this DenseMatrix<double> A)
        {
            MatrixChecks.CheckNotNull(A);
            if (A.Rows < A.Columns)
            {
                throw new InvalidOperationException();
            }

            DenseMatrix<double> At = A.Transpose();
            return At.Multiply(A).Invert().Multiply(At);
        }

        /// <summary>
        /// Calculate and return the right-inverse of a matrix $A\in\mathbb{F}^{m \times n}$, where $m \le n$
        /// </summary>
        /// <name>RightInverse</name>
        /// <proto>IMatrix<T> RightInverse(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <returns></returns>
        public static DenseMatrix<double> RightInverse(this DenseMatrix<double> A)
        {
            MatrixChecks.CheckNotNull(A);
            if (A.Rows > A.Columns)
            {
                throw new InvalidOperationException();
            }

            DenseMatrix<double> At = A.Transpose();
            return At.Multiply(A.Multiply(At).Invert());
        }

        #endregion
    }
}
