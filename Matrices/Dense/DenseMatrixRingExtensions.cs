﻿using LinearLite.BLAS;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    public static class DenseMatrixRingExtensions
    {
        /// <summary>
        /// <para>Returns the trace (sum of diagonal elements) for a given square matrix $A$. </para>
        /// <para>Implemented for matrices over <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and all <txt>T : AdditiveGroup&lt;T&gt;</txt></para>
        /// <para>If $A$ is not square, <txt>InvalidOperationException</txt> will be thrown.</para>
        /// </summary>
        /// <name>Trace</name>
        /// <proto>T Trace(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="a">A square matrix.</param>
        /// <returns>The trace of the matrix.</returns>
        public static int Trace(this DenseMatrix<int> A) => Trace(A, new IntBLAS());
        public static long Trace(this DenseMatrix<long> A) => Trace(A, new LongBLAS());
        public static float Trace(this DenseMatrix<float> A) => Trace(A, new FloatBLAS());
        public static double Trace(this DenseMatrix<double> A) => Trace(A, new DoubleBLAS());
        public static decimal Trace(this DenseMatrix<decimal> A) => Trace(A, new DecimalBLAS());
        public static T Trace<T>(this DenseMatrix<T> A) where T : Ring<T>, new() => Trace(A, new RingBLAS<T>());
        internal static T Trace<T>(DenseMatrix<T> A, IBLAS<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int n = A.Rows, i;
            T tr = blas.Zero;
            for (i = 0; i < n; ++i) tr = blas.Add(tr, A[i][i]);
            return tr;
        }
    }
}
