﻿using LinearLite.BLAS;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    public static class DenseMatrixRowEchelonFormExtensions
    {
        private static DenseMatrix<T> to_row_echelon_form<T>(DenseMatrix<T> A, IBLAS<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.Rows, cols = A.Columns, r, c;
            DenseMatrix<T> re = A.Clone() as DenseMatrix<T>;

            int n = Math.Min(rows, cols);
            for (c = 0; c < n; ++c)
            {
                T[] re_c = re[c];
                for (r = c + 1; r < n; ++r)
                {
                    T s = blas.Divide(re[r][c], re_c[c]);
                    blas.YSAX(re[r], re_c, s, c, cols);
                }
            }

            // If there are any extra rows - remove them
            T zero = blas.Zero;
            for (r = n; r < rows; ++r)
            {
                T[] re_r = re[r];
                for (c = 0; c < cols; ++c)
                {
                    re_r[c] = zero;
                }
            }
            return re;
        }

        /// <summary>
        /// <para>
        /// Converts a matrix into its row echelon form, using Gaussian elimination. The original matrix is unchanged.
        /// </para>
        /// <para>
        /// Implemented for all primitive types and all types <txt>T</txt> implementing <txt>Field&lt;T&gt;</txt>.
        /// </para>
        /// </summary>
        /// <name>ToRowEchelonForm</name>
        /// <proto>IMatrix<T> ToRowEchelonForm(this IMatrix<T> A)</proto>
        /// <cat>la</cat>
        /// <param name="A">Any non-degenerate real matrix.</param>
        /// <returns>The matrix in row echelon form.</returns>
        public static DenseMatrix<Rational> ToRowEchelonForm(this DenseMatrix<int> A) => to_row_echelon_form(A.ToRational(), new FieldBLAS<Rational>());
        public static DenseMatrix<float> ToRowEchelonForm(this DenseMatrix<float> A) => to_row_echelon_form(A, new FloatBLAS());
        public static DenseMatrix<double> ToRowEchelonForm(this DenseMatrix<double> A) => to_row_echelon_form(A, new DoubleBLAS());
        public static DenseMatrix<decimal> ToRowEchelonForm(this DenseMatrix<decimal> A) => to_row_echelon_form(A, new DecimalBLAS());
        public static DenseMatrix<Complex> ToRowEchelonForm(this DenseMatrix<Complex> A) => to_row_echelon_form(A, new ComplexBLAS());
        public static DenseMatrix<T> ToRowEchelonForm<T>(this DenseMatrix<T> A) where T : Field<T>, new() => to_row_echelon_form(A, new FieldBLAS<T>());
    }
}
