﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Dense
{
    public static class DenseMatrixSimilarityExtensions
    {
        /// <summary>
        /// Returns the similarity transform of matrix $A$ with $P$, $PAP^{-1}$.
        /// </summary>
        /// <name>SimilarityTransform</name>
        /// <proto>IMatrix<T> SimilarityTransform(this IMatrix<T> A, IMatrix<T> P)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        public static DenseMatrix<double> SimilarityTransform(this DenseMatrix<double> A, DenseMatrix<double> P)
        {
            MatrixChecks.CheckNotNull(A, P);
            MatrixChecks.CheckIsSquare(A);
            MatrixChecks.CheckIsSquare(P);
            if (A.Rows != P.Rows)
            {
                throw new InvalidOperationException();
            }

            return P.Invert().Multiply(A).Multiply(P);
        }


        /// <summary>
        /// Returns whether matrices A and B are similar
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static bool IsSimilarTo(this DenseMatrix<double> A, DenseMatrix<double> B)
        {

            if (A == null || B == null || !A.IsSquare || !B.IsSquare || A.Rows != B.Rows)
            {
                return false;
            }
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns whether matrices A and B are equivalent 
        /// Equivalence is defined for rectangular matrices, unlike similarity which is 
        /// only defined for square matrices. 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static bool IsEquivalentTo(this DenseMatrix<double> A, DenseMatrix<double> B)
        {
            throw new NotImplementedException();
        }
    }
}
