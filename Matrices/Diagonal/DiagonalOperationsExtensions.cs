﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using LinearLite.Structs;
using LinearLite.Structs.Matrix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    public static class DiagonalOperationsExtensions
    {
        public static DiagonalMatrix<int> Add(this DiagonalMatrix<int> A, DiagonalMatrix<int> B) => A.Add(B, (a, b) => a + b);
        public static DiagonalMatrix<long> Add(this DiagonalMatrix<long> A, DiagonalMatrix<long> B) => A.Add(B, (a, b) => a + b);
        public static DiagonalMatrix<float> Add(this DiagonalMatrix<float> A, DiagonalMatrix<float> B) => A.Add(B, (a, b) => a + b);
        public static DiagonalMatrix<double> Add(this DiagonalMatrix<double> A, DiagonalMatrix<double> B) => A.Add(B, (a, b) => a + b);
        public static DiagonalMatrix<decimal> Add(this DiagonalMatrix<decimal> A, DiagonalMatrix<decimal> B) => A.Add(B, (a, b) => a + b);
        public static DiagonalMatrix<T> Add<T>(this DiagonalMatrix<T> A, DiagonalMatrix<T> B) where T : AdditiveGroup<T>, new() => A.Add(B, (a, b) => a.Add(b));


        public static DiagonalMatrix<int> Subtract(this DiagonalMatrix<int> A, DiagonalMatrix<int> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<long> Subtract(this DiagonalMatrix<long> A, DiagonalMatrix<long> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<float> Subtract(this DiagonalMatrix<float> A, DiagonalMatrix<float> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<double> Subtract(this DiagonalMatrix<double> A, DiagonalMatrix<double> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<decimal> Subtract(this DiagonalMatrix<decimal> A, DiagonalMatrix<decimal> B) => A.Subtract(B, (a, b) => a - b);
        public static DiagonalMatrix<T> Subtract<T>(this DiagonalMatrix<T> A, DiagonalMatrix<T> B) where T : AdditiveGroup<T>, new() => A.Subtract(B, (a, b) => a.Subtract(b));


        public static DiagonalMatrix<int> Multiply(this DiagonalMatrix<int> A, DiagonalMatrix<int> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<long> Multiply(this DiagonalMatrix<long> A, DiagonalMatrix<long> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<float> Multiply(this DiagonalMatrix<float> A, DiagonalMatrix<float> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<double> Multiply(this DiagonalMatrix<double> A, DiagonalMatrix<double> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<decimal> Multiply(this DiagonalMatrix<decimal> A, DiagonalMatrix<decimal> B) => A.Multiply(B, (a, b) => a * b);
        public static DiagonalMatrix<T> Multiply<T>(this DiagonalMatrix<T> A, DiagonalMatrix<T> B) where T : Ring<T>, new() => A.Multiply(B, (a, b) => a.Multiply(b));

        public static int[] Multiply(this DiagonalMatrix<int> A, int[] B) => A.Multiply(B, (a, b) => a * b);
        public static long[] Multiply(this DiagonalMatrix<long> A, long[] B) => A.Multiply(B, (a, b) => a * b);
        public static float[] Multiply(this DiagonalMatrix<float> A, float[] B) => A.Multiply(B, (a, b) => a * b);
        public static double[] Multiply(this DiagonalMatrix<double> A, double[] B) => A.Multiply(B, (a, b) => a * b);
        public static decimal[] Multiply(this DiagonalMatrix<decimal> A, decimal[] B) => A.Multiply(B, (a, b) => a * b);
        public static T[] Multiply<T>(this DiagonalMatrix<T> A, T[] B) where T : Ring<T>, new() => A.Multiply(B, (a, b) => a.Multiply(b));

        public static DiagonalMatrix<int> Multiply(this DiagonalMatrix<int> A, int s) => A.Multiply(s, new IntBLAS());
        public static DiagonalMatrix<long> Multiply(this DiagonalMatrix<long> A, long s) => A.Multiply(s, new LongBLAS());
        public static DiagonalMatrix<float> Multiply(this DiagonalMatrix<float> A, float s) => A.Multiply(s, new FloatBLAS());
        public static DiagonalMatrix<double> Multiply(this DiagonalMatrix<double> A, double s) => A.Multiply(s, new DoubleBLAS());
        public static DiagonalMatrix<decimal> Multiply(this DiagonalMatrix<decimal> A, decimal s) => A.Multiply(s, new DecimalBLAS());
        public static DiagonalMatrix<T> Multiply<T>(this DiagonalMatrix<T> A, T s) where T : Ring<T>, new() => A.Multiply(s, new RingBLAS<T>());


        public static DiagonalMatrix<int> Pow(this DiagonalMatrix<int> A, int power) => A.Pow(power, (a, b) => IntegerMath.Pow(a, b));
        public static DiagonalMatrix<long> Pow(this DiagonalMatrix<long> A, int power) => A.Pow(power, (a, b) => IntegerMath.Pow(a, b));
        public static DiagonalMatrix<float> Pow(this DiagonalMatrix<float> A, int power) => A.Pow(power, (a, b) => (float)Math.Pow(a, b));
        public static DiagonalMatrix<double> Pow(this DiagonalMatrix<double> A, int power) => A.Pow(power, (a, b) => Math.Pow(a, b));
        public static DiagonalMatrix<decimal> Pow(this DiagonalMatrix<decimal> A, int power) => A.Pow(power, (a, b) => DecimalMath.Pow(a, b));
        public static DiagonalMatrix<Complex> Pow(this DiagonalMatrix<Complex> A, int power) => A.Pow(power, (a, b) => Complex.Pow(a, b));

        public static DiagonalMatrix<double> Exp(this DiagonalMatrix<int> A) => A.Exp(1.0, x => Math.Exp(x));
        public static DiagonalMatrix<double> Exp(this DiagonalMatrix<long> A) => A.Exp(1.0, x => Math.Exp(x));
        public static DiagonalMatrix<float> Exp(this DiagonalMatrix<float> A) => A.Exp(1.0f, x => (float)Math.Exp(x));
        public static DiagonalMatrix<double> Exp(this DiagonalMatrix<double> A) => A.Exp(1.0, x => Math.Exp(x));
        public static DiagonalMatrix<decimal> Exp(this DiagonalMatrix<decimal> A) => A.Exp(1.0m, x => DecimalMath.Exp(x));
        public static DiagonalMatrix<Complex> Exp(this DiagonalMatrix<Complex> A) => A.Exp(Complex.One, x => Complex.Exp(x));
    }
}
