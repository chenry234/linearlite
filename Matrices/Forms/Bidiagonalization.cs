﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using LinearLite.Matrices.Forms;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    public static class Bidiagonalization
    {
        #region Public methods 

        /// <summary>
        /// Convert a matrix to its bidiagonal form.
        /// The method decomposes matrix $A\in\mathbb{F}^{m \times n}$ as
        /// $$A = U^*BV$$
        /// <ul>
        /// <li>$U\in\mathbb{F}^{m \times m}$, a orthogonal/unitary matrix</li>
        /// <li>
        /// $B\in\mathbb{F}^{m \times n}$, a bidiagonal matrix, with non zero items in the leading and one minor diagonal. 
        /// If <txt>upper</txt> is <txt>true</txt>, then the upper minor diagonal will be non-zero, otherwise the lower minor diagonal will be the non-zero minor diagonal.
        /// </li>
        /// <li>$V\in\mathbb{F}^{n \times n}$, a orthogonal/unitary matrix</li>
        /// </ul>
        /// 
        /// The method currently supports 2 bidiagonalization algorithms: 
        /// <ul>
        /// <li>Golub-Kahan-Lanczos bidiagonalization procedure, and</li>
        /// <li>Householder transformations</li>
        /// </ul>
        /// 
        /// <para>
        /// The householder transformation method is selected by default. For small matrices, 
        /// the Golub-Kahan-Lanczos bidiagonalization procedure may offer a slight performance advantage. 
        /// </para>
        /// <para>
        /// However, for moderately sized ($n > 50$) and larger matrices, GKL suffers from loss of 
        /// orthogonality in the $U$ and $V$ matrices due to numerical errors. Householder 
        /// transformations are more numerically stable in this domain. 
        /// </para>
        /// 
        /// <!--inputs-->
        /// 
        /// <para><b>Example</b></para>
        /// <pre><code class="cs">
        /// // Create a random 15 x 10 real matrix
        /// DenseMatrix&lt;Complex&gt; matrix = DenseMatrix.Random&lt;Complex&gt;(15, 10); 
        /// 
        /// // Perform the default upper bidiagonalization (using Householder transformations)
        /// matrix.ToBidiagonalForm(out DenseMatrix&lt;Complex&gt; U, out DenseMatrix&lt;Complex&gt; B, out DenseMatrix&lt;Complex&gt; V);
        /// 
        /// // Perform a lower bidiagonalization (using Golub-Kahan-Lanczos algorithm)
        /// matrix.ToBidiagonalForm(out DenseMatrix&lt;Complex&gt; U1, out DenseMatrix&lt;Complex&gt; B1, out DenseMatrix&lt;Complex&gt; V1, 
        /// false, BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS);
        /// 
        /// Console.WriteLine(U.IsOrthogonal());    // true
        /// Console.WriteLine(B.IsBidiagonal());    // true
        /// Console.WriteLine(V.IsOrthogonal());    // true
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>ToBidiagonalForm</name>
        /// <proto>void ToBidiagonalForm(this IMatrix<T> A, out IMatrix<T> U, out IMatrix<T> B, out IMatrix<T> V, bool upper, BidiagonalizationMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times n$ matrix matrix to be decomposed.</param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// The $m \times m$ left-orthogonal/unitary matrix $U$, with the property $UU^* = I$
        /// </param>
        /// <param name="B">
        /// <b>Out parameter</b><br/>
        /// The $m \times n$ bidiagonal matrix $B$. <br/>
        /// If <txt>upper</txt> is <txt>true</txt>, $B$ will be a upper-bidiagonal matrix, i.e. $B_{ii}$ and $B_{i,i+1}$ are the only non-zero entries.<br/>
        /// If <txt>upper</txt> is <txt>false</txt>, $B$ will be a lower-bidiagonal matrix, i.e. $B_{ii}$ and $B_{i,i-1}$ are the only non-zero entries.
        /// </param>
        /// <param name="V">
        /// <b>Out parameter</b><br/>
        /// The $n \times n$ right-orthogonal/unitary matrix $V$, with the property $VV^* = I$
        /// </param>
        /// <param name="upper">
        /// <b>Optional</b>, defaults to <txt>true</txt>.<br/>
        /// If <txt>true</txt>, the matrix will be decomposed into a upper bidiagonal matrix. If <txt>false</txt>, it will be 
        /// decomposed into a lower bidiagonal matrix (with non-zero entires in the lower minor diagonal and leading diagonal).
        /// </param>
        /// <param name="method">
        /// <b>Optional</b>, defaults to the Householder transformation method (<txt>HOUSEHOLDER_TRANSFORM</txt>).<br/>
        /// Enumerated type, representing the choice of bidiagonalisation algorithm.
        /// </param>
        public static void ToBidiagonalForm(this double[,] A, out double[,] U, out double[,] B, out double[,] V, bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out double[,] _B, out U, true, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalization(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        /// <summary>
        /// Convert a complex matrix into bidiagonal form
        /// The method creates 3 matrices from a matrix A (m x n)
        /// 1. U (m x m), an unitary matrix
        /// 2. B (m x n), a upper bidiagonal matrix, with non zero items in A(i, i) and A(i, i + 1) only
        /// 3. V (n x n), an unitary matrix
        /// such that A = U^TBV.
        /// 
        /// The method currently supports 2 bidiagonalization algorithms: 
        /// 1. Golub-Kahan-Lanczos bidiagonalization procedure, and
        /// 2. Householder transformations
        /// 
        /// The householder transformation method is selected by default. For small matrices, 
        /// the Golub-Kahan-Lanczos bidiagonalization procedure may offer some benefits such as 
        /// ease of interpretation and slight performance advantage. 
        /// 
        /// However, for moderately sized (n ~ 50) and larger matrices, GKL suffers from loss of 
        /// orthogonality in the U and V matrices due to numerical errors. Householder 
        /// transformations are more numerically stable in this domain. 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="U"></param>
        /// <param name="B"></param>
        /// <param name="V"></param>
        /// <param name="method"></param>
        public static void ToBidiagonalForm(this Complex[,] A, out Complex[,] U, out Complex[,] B, out Complex[,] V, bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ConjugateTranspose().ToBidiagonalForm(out V, out Complex[,] _B, out U, true, method);
                B = _B.ConjugateTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalization(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this float[,] A, out float[,] U, out float[,] B, out float[,] V, bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out float[,] _B, out U, true, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalization(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this decimal[,] A, out decimal[,] U, out decimal[,] B, out decimal[,] V, bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out decimal[,] _B, out U, true, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalization(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// <para>Parallel bidiagonalization of a matrix, with same the prototype as the sequential implementation, <a href="#ToBidiagonalForm"><txt>ToBidiagonalForm</txt></a>.</para>
        /// <!--inputs-->
        /// </summary>
        /// <name>ToBidiagonalFormParallel</name>
        /// <proto>void ToBidiagonalFormParallel(this IMatrix<T> A, out IMatrix<T> U, out IMatrix<T> B, out IMatrix<T> V, bool upper, BidiagonalizationMethod method)</proto>
        /// <cat>la</cat>
        /// <param name="A">The $m \times n$ matrix matrix to be decomposed.</param>
        /// <param name="U">
        /// <b>Out parameter</b><br/>
        /// The $m \times m$ left-orthogonal/unitary matrix $U$, with the property $UU^* = I$
        /// </param>
        /// <param name="B">
        /// <b>Out parameter</b><br/>
        /// The $m \times n$ bidiagonal matrix $B$. <br/>
        /// If <txt>upper</txt> is <txt>true</txt>, $B$ will be a upper-bidiagonal matrix, i.e. $B_{ii}$ and $B_{i,i+1}$ are the only non-zero entries.<br/>
        /// If <txt>upper</txt> is <txt>false</txt>, $B$ will be a lower-bidiagonal matrix, i.e. $B_{ii}$ and $B_{i,i-1}$ are the only non-zero entries.
        /// </param>
        /// <param name="V">
        /// <b>Out parameter</b><br/>
        /// The $n \times n$ right-orthogonal/unitary matrix $V$, with the property $VV^* = I$
        /// </param>
        /// <param name="upper">
        /// <b>Optional</b>, defaults to <txt>true</txt>.<br/>
        /// If <txt>true</txt>, the matrix will be decomposed into a upper bidiagonal matrix. If <txt>false</txt>, it will be 
        /// decomposed into a lower bidiagonal matrix (with non-zero entires in the lower minor diagonal and leading diagonal).
        /// </param>
        /// <param name="method">
        /// <b>Optional</b>, defaults to the Householder transformation method (<txt>HOUSEHOLDER_TRANSFORM</txt>).<br/>
        /// Enumerated type, representing the choice of bidiagonalisation algorithm.
        /// </param>
        public static void ToBidiagonalFormParallel(this double[,] A, out double[,] U, out double[,] B, out double[,] V, bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ParallelTranspose().ToBidiagonalFormParallel(out V, out double[,] _B, out U, true, method);
                B = _B.ParallelTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalizationParallel(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this float[,] A, out float[,] U, out float[,] B, out float[,] V, bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ParallelTranspose().ToBidiagonalFormParallel(out V, out float[,] _B, out U, true, method);
                B = _B.ParallelTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalizationParallel(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this decimal[,] A, out decimal[,] U, out decimal[,] B, out decimal[,] V, bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ParallelTranspose().ToBidiagonalFormParallel(out V, out decimal[,] _B, out U, true, method);
                B = _B.ParallelTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalizationParallel(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this Complex[,] A, out Complex[,] U, out Complex[,] B, out Complex[,] V, bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ParallelTranspose().ToBidiagonalFormParallel(out V, out Complex[,] _B, out U, true, method);
                B = _B.ParallelTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalizationParallel(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        

        public static void ToBidiagonalForm(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V, 
            bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out DenseMatrix<double> _B, out U, true, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalization(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V, 
            bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out DenseMatrix<float> _B, out U, true, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalization(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V,
            bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.Transpose().ToBidiagonalForm(out V, out DenseMatrix<decimal> _B, out U, true, method);
                B = _B.Transpose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalization(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalForm(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V,
            bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ConjugateTranspose().ToBidiagonalForm(out V, out DenseMatrix<Complex> _B, out U, true, method);
                B = _B.ConjugateTranspose();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalization(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static void ToBidiagonalFormParallel(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V, 
            bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.TransposeParallel().ToBidiagonalFormParallel(out V, out DenseMatrix<double> _B, out U, true, method);
                B = _B.TransposeParallel();
                return;
            }
            
            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalizationParallel(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V,
            bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.TransposeParallel().ToBidiagonalFormParallel(out V, out DenseMatrix<float> _B, out U, true, method);
                B = _B.TransposeParallel();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalizationParallel(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V,
            bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.TransposeParallel().ToBidiagonalFormParallel(out V, out DenseMatrix<decimal> _B, out U, true, method);
                B = _B.TransposeParallel();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalizationParallel(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
        public static void ToBidiagonalFormParallel(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V,
            bool upper = true, BidiagonalizationMethod method = BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
        {
            if (!upper)
            {
                A.ConjugateTransposeParallel().ToBidiagonalFormParallel(out V, out DenseMatrix<Complex> _B, out U, true, method);
                B = _B.ConjugateTransposeParallel();
                return;
            }

            if (method == BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM)
            {
                A.HouseholderBidiagonalizationParallel(out U, out B, out V);
            }
            else if (method == BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS)
            {
                GolubKahanLanczosAlgorithm.Decompose(A, out U, out B, out V);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        /// <summary>
        /// Reduce matrix A into bidiagonal form, also computing the orthogonal (unitary) matrices U and V.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="U"></param>
        /// <param name="V"></param>
        /// <param name="blas"></param>
        /// <param name="ConjugateTranspose"></param>
        private static void HouseholderBidiagonalize<T>(this T[][] A, out T[][] U, out T[][] V, T zero, T one, Action<T[][], T[][], T[], T[], int, int, int, int, bool> HouseholderTransform, Action<T[][]> ConjugateTranspose)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            // The householder transformation alternate between zero-ing a column and a row. 
            // This is the only way to guarantee that only 2 diagonal columns remain. 
            // otherwise, the effect of a Householder transformation applied to a row vector will undo
            // the previous Householder transformation applied to a column vector. 

            int m = A.Length, n = A[0].Length, rank = Math.Min(m, n), max = Math.Max(m, n), c;

            // Iterate through the columns, zero-ing each one from left to right, recording the householder 
            // transformation as we go
            U = MatrixInternalExtensions.JIdentity(zero, one, m);
            V = MatrixInternalExtensions.JIdentity(zero, one, n);

            T[] v = new T[max], w = new T[max];
            for (c = 0; c < rank; ++c)
            {
                HouseholderTransform(A, U, v, w, c, n, c, m, true);
                if (c < n - 1)
                {
                    HouseholderTransform(A, V, v, w, c, n, c + 1, m, false);
                }
            }
            ConjugateTranspose(U);
        }
        private static void HouseholderBidiagonalization<T>(this T[,] A, out T[,] U, out T[,] B, out T[,] V, IBLAS<T> blas, Action<T[][]> ConjugateTranspose, bool parallel)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            T[][] _B = parallel ? A.ToJaggedParallel() : A.ToJagged(), _U, _V;

            if (parallel)
            {
                HouseholderBidiagonalize(_B, out _U, out _V, blas.Zero, blas.One, blas.HouseholderTransformParallel, ConjugateTranspose);
            }
            else
            {
                HouseholderBidiagonalize(_B, out _U, out _V, blas.Zero, blas.One, blas.HouseholderTransform, ConjugateTranspose);
            }

            if (parallel)
            {
                U = _U.ToRectangularParallel();
                B = _B.ToRectangularParallel();
                V = _V.ToRectangularParallel();
            }
            else
            {
                U = _U.ToRectangular();
                B = _B.ToRectangular();
                V = _V.ToRectangular();
            }
        }
        private static void HouseholderBidiagonalization<T>(this DenseMatrix<T> A, out DenseMatrix<T> U, out DenseMatrix<T> B, out DenseMatrix<T> V, IBLAS<T> blas, Action<T[][]> ConjugateTranspose, bool parallel) where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            T[][] _B = parallel ? A.CopyParallel().Values : A.Copy().Values, _U, _V;

            if (parallel)
            {
                HouseholderBidiagonalize(_B, out _U, out _V, blas.Zero, blas.One, blas.HouseholderTransformParallel, ConjugateTranspose);
            }
            else
            {
                HouseholderBidiagonalize(_B, out _U, out _V, blas.Zero, blas.One, blas.HouseholderTransform, ConjugateTranspose);
            }

            U = new DenseMatrix<T>(_U);
            B = new DenseMatrix<T>(_B);
            V = new DenseMatrix<T>(_V);
        }

        #region Householder transformations algorithm for T[,]
        private static void HouseholderBidiagonalization(this float[,] A, out float[,] U, out float[,] B, out float[,] V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new FloatBLAS(), M => M.SelfTranspose(), false);
        }
        private static void HouseholderBidiagonalization(this double[,] A, out double[,] U, out double[,] B, out double[,] V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new DoubleBLAS(), M => M.SelfTranspose(), false);
        }
        private static void HouseholderBidiagonalization(this decimal[,] A, out decimal[,] U, out decimal[,] B, out decimal[,] V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new DecimalBLAS(), M => M.SelfTranspose(), false);
        }
        private static void HouseholderBidiagonalization(this Complex[,] A, out Complex[,] U, out Complex[,] B, out Complex[,] V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new ComplexBLAS(), M => M.SelfConjugateTranspose(), false);
        }
        #endregion

        #region Householder transformations algorithm for DenseMatrix<T>
        private static void HouseholderBidiagonalization(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new FloatBLAS(), M => M.SelfTranspose(), false);
        }
        private static void HouseholderBidiagonalization(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new DoubleBLAS(), M => M.SelfTranspose(), false);
        }
        private static void HouseholderBidiagonalization(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new DecimalBLAS(), M => M.SelfTranspose(), false);
        }
        private static void HouseholderBidiagonalization(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new ComplexBLAS(), M => M.SelfConjugateTranspose(), false);
        }
        #endregion 

        #region Parallel Householder transformation algorithms for T[,]
        private static void HouseholderBidiagonalizationParallel(this float[,] A, out float[,] U, out float[,] B, out float[,] V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new FloatBLAS(), M => M.SelfTransposeParallel(), true);
        }
        private static void HouseholderBidiagonalizationParallel(this double[,] A, out double[,] U, out double[,] B, out double[,] V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new DoubleBLAS(), M => M.SelfTransposeParallel(), true);
        }
        private static void HouseholderBidiagonalizationParallel(this decimal[,] A, out decimal[,] U, out decimal[,] B, out decimal[,] V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new DecimalBLAS(), M => M.SelfTransposeParallel(), true);
        }
        private static void HouseholderBidiagonalizationParallel(this Complex[,] A, out Complex[,] U, out Complex[,] B, out Complex[,] V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new ComplexBLAS(), M => M.SelfConjugateTransposeParallel(), true);
        }
        #endregion

        #region Parallel Householder transformation algorithm for DenseMatrix<T>
        private static void HouseholderBidiagonalizationParallel(this DenseMatrix<float> A, out DenseMatrix<float> U, out DenseMatrix<float> B, out DenseMatrix<float> V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new FloatBLAS(), M => M.SelfTransposeParallel(), true);
        }
        private static void HouseholderBidiagonalizationParallel(this DenseMatrix<double> A, out DenseMatrix<double> U, out DenseMatrix<double> B, out DenseMatrix<double> V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new DoubleBLAS(), M => M.SelfTransposeParallel(), true);
        }
        private static void HouseholderBidiagonalizationParallel(this DenseMatrix<decimal> A, out DenseMatrix<decimal> U, out DenseMatrix<decimal> B, out DenseMatrix<decimal> V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new DecimalBLAS(), M => M.SelfTransposeParallel(), true);
        }
        private static void HouseholderBidiagonalizationParallel(this DenseMatrix<Complex> A, out DenseMatrix<Complex> U, out DenseMatrix<Complex> B, out DenseMatrix<Complex> V)
        {
            HouseholderBidiagonalization(A, out U, out B, out V, new ComplexBLAS(), M => M.SelfConjugateTransposeParallel(), true);
        }
        #endregion
    }
    public enum BidiagonalizationMethod
    {
        GOLUB_KAHAN_LANCZOS,
        HOUSEHOLDER_TRANSFORM
    }
}
