﻿using LinearLite.BLAS;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    public static class HessenbergExtensions
    {
        #region Sequential Hessenberg decomposition methods for T[,]
        /// <summary>
        /// <para>
        /// Converts a real or complex square matrix $A\in\mathbb{F}^{n \times n}$ into its Hessenberg form, where all terms below (above)
        /// the lower minor (upper minor) diagonal are zeroes. 
        /// </para>
        /// 
        /// Specifically, $A$ is decomposes into $Q^*HQ$ where, 
        /// <ul>
        /// <li>$Q$ is a $n \times n$ is an orthogonal matrix</li>
        /// <li>$H$ is a $n \times n$ is a Hessenberg matrix</li>
        /// </ul>
        /// 
        /// <para>
        /// If <txt>upper</txt> is <txt>true</txt>, then the upper Hessenberg decomposition will be performed.
        /// Otherwise, the lower Hessenberg decomposition will be performed.
        /// </para>
        /// 
        /// <!--inputs-->
        /// </summary>
        /// <name>ToHessenbergForm</name>
        /// <proto>void ToHessenbergForm(this IMatrix<T> A, out IMatrix<T> Q, out IMatrix<T> H, bool upper)</proto>
        /// <cat>la</cat>
        /// <param name="A">The matrix to be converted into Hessenberg form.</param>
        /// <param name="Q">The $n \times n$ orthogonal matrix</param>
        /// <param name="H">The $n \times n$ Hessenberg matrix</param>
        /// <param name="upper">
        /// <b>Optional</b>, defaults to <txt>true</txt>.<br/>
        /// If <txt>true</txt>, $H$ will be upper Hessenberg, otherwise it will be lower Hessenberg.
        /// </param>
        public static void ToHessenbergForm(this double[,] A, out double[,] Q, out double[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            double[][] _H = upper ? A.ToJagged() : A.ToJaggedTranspose();
            double[][] _Q = MatrixInternalExtensions.JIdentity(m);

            to_hessenberg_form(_Q, _H, true);

            Q = _Q.ToRectangularTranspose();
            H = upper ? _H.ToRectangular() : _H.ToRectangularTranspose();
        }
        /// <summary>
        /// Please see documentation for ToHessenbergForm(this double[,] A, out double[,] Q, out double[,] H, bool upper = true)
        /// </summary>
        /// <param name="A"></param>
        /// <param name="Q"></param>
        /// <param name="H"></param>
        /// <param name="upper"></param>
        public static void ToHessenbergForm(this float[,] A, out float[,] Q, out float[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            float[][] _H = upper ? A.ToJagged() : A.ToJaggedTranspose();
            float[][] _Q = MatrixInternalExtensions.JIdentity(0.0f, 1.0f, m);

            to_hessenberg_form(_Q, _H, new FloatBLAS(), true);

            Q = _Q.ToRectangularTranspose();
            H = upper ? _H.ToRectangular() : _H.ToRectangularTranspose();
        }
        /// <summary>
        /// Please see documentation for ToHessenbergForm(this double[,] A, out double[,] Q, out double[,] H, bool upper = true)
        /// </summary>
        /// <param name="A"></param>
        /// <param name="Q"></param>
        /// <param name="H"></param>
        /// <param name="upper"></param>
        public static void ToHessenbergForm(this decimal[,] A, out decimal[,] Q, out decimal[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            decimal[][] _H = upper ? A.ToJagged() : A.ToJaggedTranspose();
            decimal[][] _Q = MatrixInternalExtensions.JIdentity(0.0m, 1.0m, m);

            to_hessenberg_form(_Q, _H, new DecimalBLAS(), true);

            Q = _Q.ToRectangularTranspose();
            H = upper ? _H.ToRectangular() : _H.ToRectangularTranspose();
        }
        /// <summary>
        /// Decompose a square matrix A in C^(m x m) into Q*HQ, where 
        /// - Q is a unitary m x m matrix, ie. Q*Q = I where Q* is the conjugate transpose of Q.
        /// - H is a m x m matrix in Hessenberg form, where every term H[i, j] = 0 if j < i - 1.
        /// 
        /// This implementation uses a modified Householder-transform based algorithm
        /// 
        /// If upper = true (default), then the upper Hessenberg decomposition will be performed.
        /// Otherwise, the lower Hessenberg decomposition will be performed.
        /// </summary>
        /// <param name="A">The square matrix (m x m) to be decomposed.</param>
        /// <param name="Q">m x m unitary matrix</param>
        /// <param name="H">m x m Hessenberg matrix</param>
        /// <param name="upper">(default true). If true, H will be upper Hessenberg, other it will be lower Hessenberg.</param>
        public static void ToHessenbergForm(this Complex[,] A, out Complex[,] Q, out Complex[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            Complex[][] _H = upper ? A.ToJagged() : A.ToJaggedConjugateTranspose();
            Complex[][] _Q = MatrixInternalExtensions.JIdentity(Complex.One, m);

            to_hessenberg_form(_Q, _H, true);

            Q = _Q.ToRectangularConjugateTranspose();
            H = upper ? _H.ToRectangular() : _H.ToRectangularConjugateTranspose();
        }

        public static void ToHessenbergForm(this double[,] A, out double[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            double[][] _H = upper ? A.ToJagged() : A.ToJaggedTranspose();

            to_hessenberg_form(_H);

            H = upper ? _H.ToRectangular() : _H.ToRectangularTranspose();
        }
        public static void ToHessenbergForm(this float[,] A, out float[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            float[][] _H = upper ? A.ToJagged() : A.ToJaggedTranspose();

            to_hessenberg_form(_H, new FloatBLAS(), Householder.Transform);

            H = upper ? _H.ToRectangular() : _H.ToRectangularTranspose();
        }
        public static void ToHessenbergForm(this decimal[,] A, out decimal[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            decimal[][] _H = upper ? A.ToJagged() : A.ToJaggedTranspose();

            to_hessenberg_form(_H, new DecimalBLAS(), Householder.Transform);

            H = upper ? _H.ToRectangular() : _H.ToRectangularTranspose();
        }
        public static void ToHessenbergForm(this Complex[,] A, out Complex[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            Complex[][] _H = upper ? A.ToJagged() : A.ToJaggedConjugateTranspose();

            to_hessenberg_form(_H);

            H = upper ? _H.ToRectangular() : _H.ToRectangularConjugateTranspose();
        }
        #endregion

        #region Sequential Hessenberg decomposition methods for DenseMatrix<T> 
        private static void to_hessenberg_form<T>(this DenseMatrix<T> A, out DenseMatrix<T> Q, out DenseMatrix<T> H, IBLAS<T> blas, bool upper) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.Rows;

            DenseMatrix<T> _H = upper ? A.Copy() : A.Transpose();
            DenseMatrix<T> _Q = DenseMatrix.Identity<T>(m);

            to_hessenberg_form(_Q.Values, _H.Values, blas, true);

            Q = _Q.Transpose();
            H = upper ? _H : _H.Transpose();
        }
        public static void ToHessenbergForm(this DenseMatrix<float> A, out DenseMatrix<float> Q, out DenseMatrix<float> H, bool upper = true)
        {
            to_hessenberg_form(A, out Q, out H, new FloatBLAS(), upper);
        }
        public static void ToHessenbergForm(this DenseMatrix<double> A, out DenseMatrix<double> Q, out DenseMatrix<double> H, bool upper = true)
        {
            to_hessenberg_form(A, out Q, out H, new DoubleBLAS(), upper);
        }
        public static void ToHessenbergForm(this DenseMatrix<decimal> A, out DenseMatrix<decimal> Q, out DenseMatrix<decimal> H, bool upper = true)
        {
            to_hessenberg_form(A, out Q, out H, new DecimalBLAS(), upper);
        }
        public static void ToHessenbergForm(this DenseMatrix<Complex> A, out DenseMatrix<Complex> Q, out DenseMatrix<Complex> H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.Rows;

            DenseMatrix<Complex> _H = upper ? A.Copy() : A.ConjugateTranspose();
            DenseMatrix<Complex> _Q = DenseMatrix.Identity<Complex>(m);

            to_hessenberg_form(_Q.Values, _H.Values, true);

            Q = _Q.ConjugateTranspose();
            H = upper ? _H : _H.ConjugateTranspose();
        }

        private static void to_hessenberg_form<T>(this DenseMatrix<T> A, out DenseMatrix<T> H, Action<T[][]> Hessenberg, bool upper) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            DenseMatrix<T> _H = upper ? A.Copy() : A.Transpose();
            Hessenberg(_H.Values);
            H = upper ? _H : _H.Transpose();
        }
        public static void ToHessenbergForm(this DenseMatrix<float> A, out DenseMatrix<float> H, bool upper = true)
        {
            to_hessenberg_form(A, out H, _H => to_hessenberg_form(_H, new FloatBLAS(), Householder.Transform), upper);
        }
        public static void ToHessenbergForm(this DenseMatrix<double> A, out DenseMatrix<double> H, bool upper = true)
        {
            to_hessenberg_form(A, out H, to_hessenberg_form, upper);
        }
        public static void ToHessenbergForm(this DenseMatrix<decimal> A, out DenseMatrix<decimal> H, bool upper = true)
        {
            to_hessenberg_form(A, out H, _H => to_hessenberg_form(_H, new DecimalBLAS(), Householder.Transform), upper);
        }
        public static void ToHessenbergForm(this DenseMatrix<Complex> A, out DenseMatrix<Complex> H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            DenseMatrix<Complex> _H = upper ? A.Copy() : A.ConjugateTranspose();
            to_hessenberg_form(_H.Values);
            H = upper ? _H : _H.ConjugateTranspose();
        }
        #endregion

        #region Parallel Hessenberg decomposition methods for T[,]
        public static void ToHessenbergFormParallel(this double[,] A, out double[,] Q, out double[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            double[][] _H = upper ? A.ToJaggedParallel() : A.ToJaggedTransposeParallel();
            double[][] _Q = MatrixInternalExtensions.JIdentity(m);

            to_hessenberg_form_parallel(_Q, _H, true);

            Q = _Q.ToRectangularTransposeParallel();
            H = upper ? _H.ToRectangularParallel() : _H.ToRectangularTransposeParallel();
        }
        public static void ToHessenbergFormParallel(this Complex[,] A, out Complex[,] Q, out Complex[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            Complex[][] _H = upper ? A.ToJaggedParallel() : A.ToJaggedConjugateTransposeParallel();
            Complex[][] _Q = MatrixInternalExtensions.JIdentity(Complex.One, m);

            to_hessenberg_form_parallel(_Q, _H, true);

            Q = _Q.ToRectangularConjugateTransposeParallel();
            H = upper ? _H.ToRectangularParallel() : _H.ToRectangularConjugateTransposeParallel();
        }
        public static void ToHessenbergFormParallel(this float[,] A, out float[,] Q, out float[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            float[][] _H = upper ? A.ToJaggedParallel() : A.ToJaggedTransposeParallel();
            float[][] _Q = MatrixInternalExtensions.JIdentity(0.0f, 1.0f, m);

            to_hessenberg_form_parallel(_Q, _H, new FloatBLAS(), true);

            Q = _Q.ToRectangularTransposeParallel();
            H = upper ? _H.ToRectangularParallel() : _H.ToRectangularTransposeParallel();
        }
        public static void ToHessenbergFormParallel(this decimal[,] A, out decimal[,] Q, out decimal[,] H, bool upper = true)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int m = A.GetLength(0);

            decimal[][] _H = upper ? A.ToJaggedParallel() : A.ToJaggedTransposeParallel();
            decimal[][] _Q = MatrixInternalExtensions.JIdentity(0.0m, 1.0m, m);

            to_hessenberg_form_parallel(_Q, _H, new DecimalBLAS(), true);

            Q = _Q.ToRectangularTransposeParallel();
            H = upper ? _H.ToRectangularParallel() : _H.ToRectangularTransposeParallel();
        }

        internal static void to_hessenberg_form_parallel(double[][] Q, double[][] H, bool Q_populated = false)
        {
            throw new NotImplementedException(); // TODO: implement method
        }
        internal static void to_hessenberg_form_parallel(Complex[][] Q, Complex[][] H, bool Q_populated = false)
        {
            throw new NotImplementedException(); // TODO: implement method
        }
        /// <summary>
        /// TODO: implement method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Q"></param>
        /// <param name="H"></param>
        /// <param name="zero"></param>
        /// <param name="one"></param>
        /// <param name="YSAX"></param>
        /// <param name="DOT"></param>
        /// <param name="HouseholderTransformColumnParallel"></param>
        /// <param name="Q_populated"></param>
        private static void to_hessenberg_form_parallel<T>(T[][] Q, T[][] H, IBLAS<T> blas, bool Q_populated = false)
        {
            throw new NotImplementedException(); // TODO: implement method
        }
        #endregion

        #region Internal (jagged) Hessenberg decomposition methods for T[][]

        /// <summary>
        /// H must be populated with the original matrix A
        /// </summary>
        /// <param name="Q"></param>
        /// <param name="H"></param>
        /// <param name="Q_populated"></param>
        internal static void to_hessenberg_form(double[][] Q, double[][] H, bool Q_populated = false)
        {
            if (Q == null || H == null)
            {
                throw new ArgumentNullException();
            }
            if (H.Length != Q.Length || H.Length != H[0].Length || Q.Length != Q[0].Length)
            {
                throw new InvalidOperationException();
            }

            int m = H.Length, c, i, j;

            // Populate Q if its not already populated with I
            if (!Q_populated)
            {
                for (i = 0; i < m; ++i)
                {
                    double[] Q_row = Q[i];
                    for (j = 0; j < m; ++j)
                    {
                        Q_row[j] = 0.0;
                    }
                    Q_row[i] = 1.0;
                }
            }

            double[] v = new double[m], w = new double[m], row;
            double w_i;
            for (c = 0; c < m; ++c)
            {
                int begin = c + 1;

                if (begin < m)
                {
                    Householder.Transform(H, Q, v, w, c, m, begin, m, true);

                    // Also postmultiply H by Q
                    for (i = 0; i < m; ++i)
                    {
                        row = H[i];
                        w_i = 0.0;
                        for (j = begin; j < m; ++j)
                        {
                            w_i += v[j] * row[j];
                        }
                        for (j = begin; j < m; ++j)
                        {
                            row[j] -= v[j] * w_i;
                        }
                    }
                }
            }
        }
        internal static void to_hessenberg_form(Complex[][] Q, Complex[][] H, bool Q_populated = false)
        {
            if (Q == null || H == null)
            {
                throw new ArgumentNullException();
            }
            if (H.Length != Q.Length || H.Length != H[0].Length || Q.Length != Q[0].Length)
            {
                throw new InvalidOperationException();
            }
            int m = H.Length, c, i, j;

            // Populate Q if its not already populated with I
            if (!Q_populated)
            {
                for (i = 0; i < m; ++i)
                {
                    Complex[] Q_row = Q[i];
                    for (j = 0; j < m; ++j)
                    {
                        Q_row[j].Real = 0.0;
                        Q_row[j].Imaginary = 0.0;
                    }
                    Q_row[i].Real = 1.0;
                    Q_row[i].Imaginary = 0.0;
                }
            }

            Complex[] v = new Complex[m], w = new Complex[m],
                //v_conj = new Complex[m], 
                D_row;

            ComplexBLAS blas = new ComplexBLAS();
            //Complex zero = Complex.Zero, w_i;
            
            for (c = 0; c < m; ++c)
            {
                int begin = c + 1;
                if (begin < m)
                {
                    Householder.Transform(H, Q, v, w, c, m, begin, m, true);

                    // Also postmultiply H by Q
                    for (i = 0; i < m; ++i)
                    {
                        D_row = H[i];
                        Complex w_i = blas.DOT(v, D_row, begin, m);
                        /*
                        w_i = zero;
                        for (j = begin; j < m; ++j)
                        {
                            w_i.Increment(v[j].Multiply(D_row[j]));
                        }*/

                        double w_i_re = w_i.Real, w_i_im = w_i.Imaginary;
                        for (j = begin; j < m; ++j)
                        {
                            Complex a = v[j];
                            D_row[j].Real -= (a.Real * w_i_re + a.Imaginary * w_i_im);
                            D_row[j].Imaginary -= (a.Real * w_i_im - a.Imaginary * w_i_re);
                        }
                        /*
                        for (j = begin; j < m; ++j)
                        {
                            D_row[j].Decrement(v_conj[j].Multiply(w_i));
                        }*/
                    }
                }
            }
        }
        internal static void to_hessenberg_form<T>(T[][] Q, T[][] H, IBLAS<T> blas, bool Q_populated = false)
        {
            if (Q == null || H == null)
            {
                throw new ArgumentNullException();
            }
            if (H.Length != Q.Length || H.Length != H[0].Length || Q.Length != Q[0].Length)
            {
                throw new InvalidOperationException();
            }

            int m = H.Length, c, i, j;

            // Populate Q if its not already populated with I
            if (!Q_populated)
            {
                T one = blas.One, zero = blas.Zero;
                for (i = 0; i < m; ++i)
                {
                    T[] Q_row = Q[i];
                    for (j = 0; j < m; ++j)
                    {
                        Q_row[j] = zero;
                    }
                    Q_row[i] = one;
                }
            }

            T[] v = new T[m], w = new T[m], row;
            T w_i;
            for (c = 0; c < m; ++c)
            {
                int begin = c + 1;

                if (begin < m)
                {
                    //HouseholderTransform(H, Q, v, w, c, m, begin, m, true, Sign, Norm, Add, Negate, SCAL, AMULT, AXPY, DOT);
                    blas.HouseholderTransform(H, Q, v, w, c, m, begin, m, true);

                    // Also postmultiply H by Q
                    for (i = 0; i < m; ++i)
                    {
                        row = H[i];
                        w_i = blas.DOT(v, row, begin, m);
                        blas.YSAX(row, v, w_i, begin, m);
                    }
                }
            }
        }

        internal static void to_hessenberg_form(double[][] H)
        {
            if (H == null)
            {
                throw new ArgumentNullException();
            }
            if (H.Length != H[0].Length)
            {
                throw new InvalidOperationException();
            }

            int m = H.Length, c, i, j;
            double[] v = new double[m], w = new double[m], row;
            double w_i;
            for (c = 0; c < m; ++c)
            {
                int begin = c + 1;

                if (begin < m)
                {
                    Householder.Transform(H, v, w, c, m, begin, m, true);

                    // Also postmultiply H by Q
                    for (i = 0; i < m; ++i)
                    {
                        row = H[i];
                        w_i = 0.0;
                        for (j = begin; j < m; ++j)
                        {
                            w_i += v[j] * row[j];
                        }
                        for (j = begin; j < m; ++j)
                        {
                            row[j] -= v[j] * w_i;
                        }
                    }
                }
            }
        }
        internal static void to_hessenberg_form(Complex[][] H)
        {
            if (H == null)
            {
                throw new ArgumentNullException();
            }
            if (H.Length != H[0].Length)
            {
                throw new InvalidOperationException();
            }

            int m = H.Length, last = m - 1, c, i, j;
            double w_i_re, w_i_im;

            Complex[] v = new Complex[m], w = new Complex[m], D_row;
            Complex a, b;
            for (c = 0; c < last; ++c)
            {
                int begin = c + 1;
                Householder.Transform(H, v, w, c, m, begin, m, true);

                // Also postmultiply H by Q
                for (i = 0; i < m; ++i)
                {
                    D_row = H[i];
                    w_i_re = 0.0;
                    w_i_im = 0.0;

                    for (j = begin; j < m; ++j)
                    {
                        a = v[j];
                        b = D_row[j];

                        w_i_re += (a.Real * b.Real - a.Imaginary * b.Imaginary);
                        w_i_im += (a.Imaginary * b.Real + a.Real * b.Imaginary);
                    }
                    for (j = begin; j < m; ++j)
                    {
                        a = v[j];
                        D_row[j].Real -= (a.Real * w_i_re + a.Imaginary * w_i_im);
                        D_row[j].Imaginary -= (-a.Imaginary * w_i_re + a.Real * w_i_im);
                    }
                }
            }
        }
        internal static void to_hessenberg_form<T>(T[][] H, IBLAS<T> blas, Action<T[][], T[], T[], int, int, int, int, bool> HouseholderTransform)
        {
            if (H == null)
            {
                throw new ArgumentNullException();
            }
            if (H.Length != H[0].Length)
            {
                throw new InvalidOperationException();
            }

            int m = H.Length, last = m - 1, c, i;
            T[] v = new T[m], w = new T[m], row;
            for (c = 0; c < last; ++c)
            {
                int begin = c + 1;
                HouseholderTransform(H, v, w, c, m, begin, m, true);

                // Also postmultiply H by Q
                for (i = 0; i < m; ++i)
                {
                    row = H[i];
                    T w_i = blas.DOT(v, row, begin, m);
                    blas.YSAX(row, v, w_i, begin, m);
                }
            }
        }

        #endregion
    }
}
