﻿using LinearLite.Structs;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    internal static class Givens
    {
        /// <summary>
        /// Rotate row i and row j from a matrix A, according to the Givens rotation matrix 
        ///  c s
        /// -s c
        /// </summary>
        /// <param name="A"></param>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <param name="c"></param>
        /// <param name="s"></param>
        /// <param name="col_start"></param>
        /// <param name="col_end"></param>
        internal static void RotateRows(double[][] A, int i, int j, double c, double s, int col_start, int col_end)
        {
            // only alter the rows i and j of matrix A
            double[] A_i = A[i], A_j = A[j];
            for (int k = col_start; k < col_end; ++k)
            {
                double A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c * A_ik + s * A_jk;
                A_j[k] = c * A_jk - s * A_ik;
            }
        }
        internal static void RotateRows(float[][] A, int i, int j, float c, float s, int col_start, int col_end)
        {
            // only alter the rows i and j of matrix A
            float[] A_i = A[i], A_j = A[j];
            for (int k = col_start; k < col_end; ++k)
            {
                float A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c * A_ik + s * A_jk;
                A_j[k] = c * A_jk - s * A_ik;
            }
        }
        internal static void RotateRows(decimal[][] A, int i, int j, decimal c, decimal s, int col_start, int col_end)
        {
            // only alter the rows i and j of matrix A
            decimal[] A_i = A[i], A_j = A[j];
            for (int k = col_start; k < col_end; ++k)
            {
                decimal A_ik = A_i[k], A_jk = A_j[k];
                A_i[k] = c * A_ik + s * A_jk;
                A_j[k] = c * A_jk - s * A_ik;
            }
        }
        internal static void RotateRows(Complex[][] A, int i, int j, Complex c, Complex s, int col_start, int col_end)
        {
            // only alter the rows i and j of matrix A
            Complex[] A_i = A[i], A_j = A[j];
            double c_re = c.Real, c_im = c.Imaginary, s_re = s.Real, s_im = s.Imaginary;

            for (int k = col_start; k < col_end; ++k)
            {
                Complex A_ik = A_i[k], A_jk = A_j[k];
                A_i[k].Set(
                    c_re * A_ik.Real - c_im * A_ik.Imaginary + s_re * A_jk.Real - s_im * A_jk.Imaginary,
                    c_im * A_ik.Real + c_re * A_ik.Imaginary + s_im * A_jk.Real + s_re * A_jk.Imaginary);
                A_j[k].Set(
                    c_re * A_jk.Real - c_im * A_jk.Imaginary - s_re * A_ik.Real - s_im * A_ik.Imaginary,
                    c_im * A_jk.Real + c_re * A_jk.Imaginary + s_im * A_ik.Real - s_re * A_ik.Imaginary);
            }
        }

        internal static void RotateRowsParallel(double[][] A, int i, int j, double c, double s, int col_start, int col_end)
        {
            // only alter the rows i and j of matrix A
            double[] A_i = A[i], A_j = A[j];
            Parallel.ForEach(Partitioner.Create(col_start, col_end), range =>
            {
                int min = range.Item1, max = range.Item2;
                for (int k = min; k < max; ++k)
                {
                    double A_ik = A_i[k], A_jk = A_j[k];
                    A_i[k] = c * A_ik + s * A_jk;
                    A_j[k] = c * A_jk - s * A_ik;
                }
            });
        }

        /// <summary>
        /// Rotate column i and column j from a matrix A, according to the Givens rotation matrix 
        ///  c s
        /// -s c
        /// </summary>
        /// <param name="A"></param>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <param name="c"></param>
        /// <param name="s"></param>
        /// <param name="row_start"></param>
        /// <param name="row_end"></param>
        internal static void RotateColumns(double[][] A, int i, int j, double c, double s, int row_start, int row_end)
        {
            // only alter column i and j of matrix A
            for (int k = row_start; k < row_end; ++k)
            {
                double[] A_k = A[k];
                double A_ki = A_k[i], A_kj = A_k[j];
                A_k[j] = c * A_kj - s * A_ki;
                A_k[i] = c * A_ki + s * A_kj;
            }
        }
        internal static void RotateColumns(float[][] A, int i, int j, float c, float s, int row_start, int row_end)
        {
            // only alter the rows i and j of matrix A
            for (int k = row_start; k < row_end; ++k)
            {
                float[] A_k = A[k];
                float A_ki = A_k[i], A_kj = A_k[j];
                A_k[j] = c * A_kj - s * A_ki;
                A_k[i] = c * A_ki + s * A_kj;
            }
        }
        internal static void RotateColumns(decimal[][] A, int i, int j, decimal c, decimal s, int row_start, int row_end)
        {
            // only alter the rows i and j of matrix A
            for (int k = row_start; k < row_end; ++k)
            {
                decimal[] A_k = A[k];
                decimal A_ki = A_k[i], A_kj = A_k[j];
                A_k[j] = c * A_kj - s * A_ki;
                A_k[i] = c * A_ki + s * A_kj;
            }
        }
        internal static void RotateColumns(Complex[][] A, int i, int j, Complex c, Complex s, int row_start, int row_end)
        {
            // only alter the rows i and j of matrix A
            //Debug.WriteLine(c.Real * c.Real + Math.Pow(s.Modulus(), 2));
            double c_re = c.Real, c_im = c.Imaginary, s_re = s.Real, s_im = s.Imaginary;
            Complex[] A_k;
            for (int k = row_start; k < row_end; ++k)
            {
                A_k = A[k];
                Complex A_ki = A_k[i], A_kj = A_k[j];
                A[k][i].Set(
                    c_re * A_ki.Real - c_im * A_ki.Imaginary + s_re * A_kj.Real + s_im * A_kj.Imaginary,
                    c_im * A_ki.Real + c_re * A_ki.Imaginary - s_im * A_kj.Real + s_re * A_kj.Imaginary);
                A[k][j].Set(
                    c_re * A_kj.Real - c_im * A_kj.Imaginary - s_re * A_ki.Real + s_im * A_ki.Imaginary,
                    c_im * A_kj.Real + c_re * A_kj.Imaginary - s_im * A_ki.Real - s_re * A_ki.Imaginary);
            }
        }
    
        internal static void RotateColumnsParallel(double[][] A, int i, int j, double c, double s, int row_start, int row_end)
        {
            // only alter column i and j of matrix A
            Parallel.ForEach(Partitioner.Create(row_start, row_end), range =>
            {
                int min = range.Item1, max = range.Item2, k;
                for (k = min; k < max; ++k)
                {
                    double[] A_k = A[k];
                    double A_ki = A_k[i], A_kj = A_k[j];
                    A_k[j] = c * A_kj - s * A_ki;
                    A_k[i] = c * A_ki + s * A_kj;
                }
            });
        }
        internal static void RotateRows(Dictionary<long, Dictionary<long, double>> matrixByRows, long i, long j, double c, double s, long col_start, long col_end)
        {
            Dictionary<long, double> 
                A_i = matrixByRows.ContainsKey(i) ? matrixByRows[i] : new Dictionary<long, double>(),
                A_j = matrixByRows.ContainsKey(j) ? matrixByRows[j] : new Dictionary<long, double>();

            HashSet<long> union = new HashSet<long>(A_i.Keys);
            union.UnionWith(A_j.Keys);

            foreach (long k in union)
            {
                if (k >= col_start && k < col_end)
                {
                    double A_ik = A_i.ContainsKey(k) ? A_i[k] : 0.0;
                    double A_jk = A_j.ContainsKey(k) ? A_j[k] : 0.0;

                    double temp = c * A_ik + s * A_jk;
                    A_j[k] = c * A_jk - s * A_ik;
                    A_i[k] = temp;
                }
            }

            matrixByRows[i] = A_i;
            matrixByRows[j] = A_j;
        }
    }
}
