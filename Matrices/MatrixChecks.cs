﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class MatrixChecks
    {
        public static void CheckDimensions(int m, int n)
        {
            if (m != n)
            {
                throw new ArgumentOutOfRangeException($"Vector dimensions don't match: {m} vs {n}");
            }
        }
        public static void CheckMatrixDimensionsEqual<T>(T[,] a, T[,] b)
        {
            if (a == null || b == null || a.GetLength(0) == 0 || b.GetLength(0) == 0)
            {
                throw new ArgumentNullException("Matrix is null or empty.");
            }
            if (a.GetLength(0) != b.GetLength(0))
            {
                throw new ArgumentOutOfRangeException($"The row count does not match. Received: {a.GetLength(0)} vs {b.GetLength(0)}");
            }
            if (a.GetLength(1) != b.GetLength(1))
            {
                throw new ArgumentOutOfRangeException($"The column count does not match. Received: {a.GetLength(1)} vs {b.GetLength(1)}");
            }
        }

        /// <summary>
        /// Jagged matrices may not have the same row dimension - check to make sure 'a'
        /// is a rectangular matrix
        /// </summary>
        /// <param name="a"></param>
        public static void CheckIsRectangular<T>(T[][] a)
        {
            if (a != null && a.Length > 0)
            {
                int len = a[0].Length, rows = a.Length, i;
                for (i = 1; i < rows; ++i)
                {
                    if (a[i].Length != len)
                    {
                        throw new ArgumentException("Argument is not a proper matrix.");
                    }
                }
            }
        }
        public static void CheckIsSquare<T>(T[,] a)
        {
            if (a.GetLength(0) != a.GetLength(1))
            {
                throw new InvalidOperationException($"Expected square matrix, received matrix with dimensions {a.GetLength(0)} x {a.GetLength(1)}.");
            }
        }
        public static void CheckIsSquare<T>(T[][] A)
        {
            if (A.Length != A[0].Length)
            {
                throw new InvalidOperationException($"Expected square matrix, received matrix with dimensions {A.Length} x {A[0].Length}.");
            }
        }

        internal static void CheckNotNull(params object[] As)
        {
            if (As == null)
            {
                throw new ArgumentNullException("Matrix is null or empty.");
            }
            foreach (object A in As)
            {
                if (A == null)
                {
                    throw new ArgumentNullException("Matrix is null or empty.");
                }
            }
        }
        internal static void CheckIsSquare<T>(IMatrix<T> A)
        {
            if (A.Rows != A.Columns)
            {
                throw new InvalidOperationException("Matrix is not square.");
            }
        }
        internal static void CheckMatrixDimensionsEqual<T>(IMatrix<T> A, IMatrix<T> B)
        {
            if (A == null || B == null)
            {
                throw new ArgumentNullException("Matrix is null or empty.");
            }
            if (A.Rows != B.Rows)
            {
                throw new ArgumentOutOfRangeException($"The row count does not match. Received: { A.Rows } vs { B.Rows }");
            }
            if (A.Columns != B.Columns)
            {
                throw new ArgumentOutOfRangeException($"The column count does not match. Received: { A.Columns } vs { B.Columns }");
            }
        }

        private static void CheckMatrixDimensionsForMultiplication(int A_rows, int A_cols, int B_rows, int B_cols, bool B_transposed = false)
        {
            if (B_transposed)
            {
                if (A_cols != B_cols)
                {
                    throw new InvalidOperationException($"Matrix dimensions not compatible for multiplication. Dimensions: A ({A_rows} x {A_cols}) and B ({B_cols} x {B_rows})");
                }
            }
            else
            {
                if (A_cols != B_rows)
                {
                    throw new InvalidOperationException($"Matrix dimensions not compatible for multiplication. Dimensions: A ({A_rows} x {A_cols}) and B ({B_rows} x {B_cols})");
                }
            }
        }
        private static void CheckMatrixDimensionsForMultiplication(int A_rows, int A_cols, int B_rows, int B_cols, int result_rows, int result_cols, bool B_transposed = false)
        {
            CheckMatrixDimensionsForMultiplication(A_rows, A_cols, B_rows, B_cols, B_transposed);

            if (A_rows != result_rows)
            {
                throw new InvalidOperationException(
                    $"# rows of 1st matrix ({A_rows}) does not equal # rows of result matrix ({result_rows})");
            }
            if (B_transposed)
            {
                if (B_rows != result_cols)
                {
                    throw new InvalidOperationException(
                        $"# columns of 2nd matrix ({B_rows}) does not equal # columns of the result matrix ({result_rows})");
                }
            }
            else
            {
                if (B_cols != result_cols)
                {
                    throw new InvalidOperationException(
                        $"# columns of 2nd matrix ({B_cols}) does not equal # columns of the result matrix ({result_rows})");
                }
            }
        }
        internal static void CheckMatrixDimensionsForMultiplication<T>(IMatrix<T> A, IMatrix<T> B, bool B_transposed = false)
        {
            CheckMatrixDimensionsForMultiplication(A.Rows, A.Columns, B.Rows, B.Columns, B_transposed);
        }
        internal static void CheckMatrixDimensionsForMultiplication<T>(IMatrix<T> A, IMatrix<T> B, IMatrix<T> result, bool B_transposed = false)
        {
            CheckMatrixDimensionsForMultiplication(A.Rows, A.Columns, B.Rows, B.Columns, result.Rows, result.Columns, B_transposed);
        }
        internal static void CheckMatrixDimensionsForMultiplication<T>(T[,] A, T[,] B, bool B_transposed = false)
        {
            CheckMatrixDimensionsForMultiplication(A.GetLength(0), A.GetLength(1), B.GetLength(0), B.GetLength(1), B_transposed);
        }
        internal static void CheckMatrixDimensionsForMultiplication<T>(T[,] A, T[,] B, T[,] result, bool B_transposed = false)
        {
            CheckMatrixDimensionsForMultiplication(A.GetLength(0), A.GetLength(1), B.GetLength(0), B.GetLength(1), result.GetLength(0), result.GetLength(1), B_transposed);
        }
        internal static void CheckMatrixDimensionsForMultiplication<T>(T[][] A, T[][] B, bool B_transposed = false)
        {
            CheckMatrixDimensionsForMultiplication(A.Length, A[0].Length, B.Length, B[0].Length, B_transposed);
        }
        internal static void CheckMatrixDimensionsForMultiplication<T>(T[][] A, T[][] B, T[][] result, bool B_transposed = false)
        {
            CheckMatrixDimensionsForMultiplication(A.Length, A[0].Length, B.Length, B[0].Length, result.Length, result[0].Length, B_transposed);
        }

    }
}
