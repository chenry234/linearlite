﻿using LinearLite.Structs;
using LinearLite.Structs.Vectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class MatrixConversionExtensions
    {
        public static DenseMatrix<T> Convert<F, T>(this DenseMatrix<F> matrix, Func<F, T> conversion) where T : new() where F : new()
        {
            return new DenseMatrix<T>(Convert(matrix.Values, conversion));
        }
        public static DenseVector<T> Convert<F, T>(this DenseVector<F> vector, Func<F, T> conversion) where T : new() where F : new()
        {
            return new DenseVector<T>(Convert(vector.Values, conversion));
        }
        public static T[,] Convert<F, T>(this F[,] matrix, Func<F, T> conversion)
        {
            if (matrix == null) return null;

            int rows = matrix.GetLength(0), cols = matrix.GetLength(1);
            T[,] dest = new T[rows, cols];
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    dest[i, j] = conversion(matrix[i, j]);
                }
            }
            return dest;
        }
        public static T[] Convert<F, T>(this F[] vector, Func<F, T> conversion)
        {
            if (vector == null) return null;

            T[] result = new T[vector.Length];
            for (int i = 0; i < vector.Length; ++i)
            {
                result[i] = conversion(vector[i]);
            }
            return result;
        }
        public static T[][] Convert<F, T>(this F[][] src, Func<F, T> conversion)
        {
            if (src == null) return null;

            int rows = src.Length;

            T[][] dest = new T[rows][];
            for (int i = 0; i < rows; i++)
            {
                F[] src_row = src[i];

                int cols = src_row.Length;
                T[] dest_row = new T[cols];
                for (int j = 0; j < cols; j++)
                {
                    dest_row[j] = conversion(src_row[j]);
                }
                dest[i] = dest_row;
            }
            return dest;
        }
        public static SparseMatrix<T> Convert<F, T>(this SparseMatrix<F> matrix, Func<F, T> conversion) where F : new() where T : new()
        {
            Dictionary<long, F> from = matrix.Values;
            Dictionary<long, T> to = new Dictionary<long, T>();
            foreach (KeyValuePair<long, F> pair in from)
            {
                to.Add(pair.Key, conversion(pair.Value));
            }
            return new SparseMatrix<T>(matrix.Rows, matrix.Columns, to);
        }

        public static Complex[,] ToComplex(this int[,] matrix) => Convert(matrix, x => new Complex(x));
        public static Complex[,] ToComplex(this long[,] matrix) => Convert(matrix, x => new Complex(x));
        public static Complex[,] ToComplex(this float[,] matrix) => Convert(matrix, x => new Complex(x));
        public static Complex[,] ToComplex(this double[,] matrix) => Convert(matrix, x => new Complex(x));
        public static Complex[,] ToComplex(this decimal[,] matrix) => Convert(matrix, x => new Complex((double)x));
        public static double[,] ToDouble(this int[,] matrix) => Convert(matrix, x => (double)x);
        public static double[,] ToDouble(this long[,] matrix) => Convert(matrix, x => (double)x);
        public static double[,] ToDouble(this float[,] matrix) => Convert(matrix, x => (double)x);
        public static Rational[,] ToRational(this int[,] matrix) => Convert(matrix, x => new Rational(x));

        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<int> matrix) => Convert(matrix, x => new Complex(x));
        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<long> matrix) => Convert(matrix, x => new Complex(x));
        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<float> matrix) => Convert(matrix, x => new Complex(x));
        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<double> matrix) => Convert(matrix, x => new Complex(x));
        public static DenseMatrix<Complex> ToComplex(this DenseMatrix<decimal> matrix) => Convert(matrix, x => new Complex((double)x));
        public static DenseMatrix<double> ToDouble(this DenseMatrix<int> matrix) => Convert(matrix, x => (double)x);
        public static DenseMatrix<double> ToDouble(this DenseMatrix<long> matrix) => Convert(matrix, x => (double)x);
        public static DenseMatrix<double> ToDouble(this DenseMatrix<float> matrix) => Convert(matrix, x => (double)x);
        public static DenseMatrix<Rational> ToRational(this DenseMatrix<int> matrix) => Convert(matrix, x => new Rational(x));

        public static DenseVector<Complex> ToComplex(this DenseVector<int> vector) => Convert(vector, x => new Complex(x));
        public static DenseVector<Complex> ToComplex(this DenseVector<long> vector) => Convert(vector, x => new Complex(x));
        public static DenseVector<Complex> ToComplex(this DenseVector<float> vector) => Convert(vector, x => new Complex(x));
        public static DenseVector<Complex> ToComplex(this DenseVector<double> vector) => Convert(vector, x => new Complex(x));
        public static DenseVector<Complex> ToComplex(this DenseVector<decimal> vector) => Convert(vector, x => new Complex((double)x));
        public static DenseVector<double> ToDouble(this DenseVector<int> vector) => Convert(vector, x => (double)x);
        public static DenseVector<double> ToDouble(this DenseVector<long> vector) => Convert(vector, x => (double)x);
        public static DenseVector<double> ToDouble(this DenseVector<float> vector) => Convert(vector, x => (double)x);
        public static DenseVector<Rational> ToRational(this DenseVector<int> vector) => Convert(vector, x => new Rational(x));

        public static SparseMatrix<Complex> ToComplex(this SparseMatrix<int> matrix) => Convert(matrix, x => new Complex(x));
        public static SparseMatrix<Complex> ToComplex(this SparseMatrix<long> matrix) => Convert(matrix, x => new Complex(x));
        public static SparseMatrix<Complex> ToComplex(this SparseMatrix<float> matrix) => Convert(matrix, x => new Complex(x));
        public static SparseMatrix<Complex> ToComplex(this SparseMatrix<double> matrix) => Convert(matrix, x => new Complex(x));
        public static SparseMatrix<Complex> ToComplex(this SparseMatrix<decimal> matrix) => Convert(matrix, x => new Complex((double)x));
        public static SparseMatrix<double> ToDouble(this SparseMatrix<int> matrix) => Convert(matrix, x => (double)x);
        public static SparseMatrix<double> ToDouble(this SparseMatrix<long> matrix) => Convert(matrix, x => (double)x);
        public static SparseMatrix<double> ToDouble(this SparseMatrix<float> matrix) => Convert(matrix, x => (double)x);
        public static SparseMatrix<Rational> ToRational(this SparseMatrix<int> matrix) => Convert(matrix, x => new Rational(x));


        public static DenseMatrix<T> ToDense<T>(this SparseMatrix<T> sparse) where T : new()
        {
            int rows = sparse.Rows, cols = sparse.Columns, i, j;
            Dictionary<long, T> _values = sparse.Values;

            DenseMatrix<T> matrix = new DenseMatrix<T>(rows, cols);
            T[][] values = matrix.Values;

            long index = 0;
            for (i = 0; i < rows; ++i)
            {
                T[] values_i = values[i];
                for (j = 0; j < cols; ++j)
                {
                    values_i[j] = _values.ContainsKey(index) ? _values[index] : new T();
                    index++;
                }
            }
            return matrix;
        }
        public static SparseMatrix<T> ToSparse<T>(this DenseMatrix<T> dense) where T : new()
        {
            int rows = dense.Rows, cols = dense.Columns, i, j;
            T[][] A = dense.Values;
            T def = new T();

            Dictionary<long, T> dict = new Dictionary<long, T>();
            long index = 0;
            for (i = 0; i < rows; ++i)
            {
                T[] A_i = A[i];
                for (j = 0; j < cols; ++j)
                {
                    T A_ij = A_i[j];
                    if (!A_ij.Equals(def))
                    {
                        dict[index] = A_ij;
                    }
                    index++;
                }
            }
            return new SparseMatrix<T>(rows, cols, dict);
        }


        public static double[] ToDouble(this int[] vector)
        {
            return Convert(vector, new Func<int, double>(d => d));
        }
        public static Rational[] ToFraction(this int[] vector)
        {
            return Convert(vector, new Func<int, Rational>(i => new Rational(i)));
        }
    }
}
