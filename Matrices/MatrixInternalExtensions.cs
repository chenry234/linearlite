﻿using LinearLite.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    internal static class MatrixInternalExtensions
    {
        /// <summary>
        /// The difference between this method and the JZero method is we don't populate this array
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        /// <returns></returns>
        internal static T[][] JMatrix<T>(int rows, int cols)
        {
            T[][] matrix = new T[rows][];
            for (int i = 0; i < rows; ++i) matrix[i] = new T[cols];
            return matrix;
        }
        internal static double[][] JZero(int rows, int cols)
        {
            double[][] matrix = new double[rows][];
            for (int r = 0; r < rows; ++r)
            {
                matrix[r] = new double[cols];
            }
            return matrix;
        }
        internal static Complex[][] JZeroComplex(int rows, int cols)
        {
            Complex[][] matrix = new Complex[rows][];
            int r, c;
            for (r = 0; r < rows; ++r)
            {
                Complex[] row = new Complex[cols];
                for (c = 0; c < cols; ++c)
                {
                    row[c] = Complex.Zero;
                }
                matrix[r] = row;
            }
            return matrix;
        }
        internal static T[][] JZero<T>(T obj, int rows, int cols) where T : AdditiveGroup<T>, new()
        {
            T[][] matrix = new T[rows][];
            int r, c;
            for (r = 0; r < rows; ++r)
            {
                T[] row = new T[cols];
                for (c = 0; c < cols; ++c)
                {
                    row[c] = obj.AdditiveIdentity;
                }
                matrix[r] = row;
            }
            return matrix;
        }

        internal static double[][] JIdentity(int n)
        {
            double[][] identity = JZero(n, n);
            for (int r = 0; r < n; ++r)
            {
                identity[r][r] = 1;
            }
            return identity;
        }
        internal static Complex[][] JIdentityComplex(int n)
        {
            Complex[][] identity = JZeroComplex(n, n);
            for (int r = 0; r < n; ++r)
            {
                identity[r][r] = Complex.One;
            }
            return identity;
        }
        internal static T[][] JIdentity<T>(T obj, int n) where T : Ring<T>, new()
        {
            T[][] identity = JZero(obj, n, n);
            for (int i = 0; i < n; ++i)
            {
                identity[i][i] = obj.MultiplicativeIdentity;
            }
            return identity;
        }
        internal static T[][] JIdentity<T>(T zero, T one, int n)
        {
            T[][] identity = new T[n][];
            
            for (int i = 0; i < n; ++i)
            {
                T[] row = new T[n];
                for (int j = 0; j < n; ++j)
                {
                    row[j] = zero;
                }
                row[i] = one;
                identity[i] = row;
            }
            return identity;
        }

        internal static T[,] ToRectangular<T>(this T[][] matrix)
        {
            T[,] rect = new T[matrix.Length, matrix[0].Length];
            int i, j;
            for (i = 0; i < matrix.Length; ++i)
            {
                T[] row = matrix[i];
                for (j = 0; j < row.Length; ++j)
                {
                    rect[i, j] = row[j];
                }
            }
            return rect;
        }
        internal static T[,] ToRectangularParallel<T>(this T[][] matrix)
        {
            T[,] rect = new T[matrix.Length, matrix[0].Length];
            
            Parallel.ForEach(Partitioner.Create(0, matrix.Length), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (i = min; i < max; ++i)
                {
                    T[] row = matrix[i];
                    for (j = 0; j < row.Length; ++j)
                    {
                        rect[i, j] = row[j];
                    }
                }
            });
            return rect;
        }
        /// <summary>
        /// Convert matrix into rectnagular matrix, while transposing it. Used a lot in matrix forms
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="matrix"></param>
        /// <returns></returns>
        internal static T[,] ToRectangularTranspose<T>(this T[][] matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            int m = matrix.Length, n = matrix[0].Length, i, j;
            T[,] rect = new T[n, m];
            T[] row;
            for (i = 0; i < m; ++i)
            {
                row = matrix[i];
                for (j = 0; j < m; ++j)
                {
                    rect[j, i] = row[j];
                }
            }
            return rect;
        }
        internal static T[,] ToRectangularTransposeParallel<T>(this T[][] matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            int m = matrix.Length, n = matrix[0].Length;
            T[,] rect = new T[n, m];

            Parallel.ForEach(Partitioner.Create(0, m), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                T[] row;

                for (i = min; i < max; ++i)
                {
                    row = matrix[i];
                    for (j = 0; j < m; ++j)
                    {
                        rect[j, i] = row[j];
                    }
                }
            });
            
            return rect;
        }
        internal static Complex[,] ToRectangularConjugateTranspose(this Complex[][] matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            int m = matrix.Length, n = matrix[0].Length, i, j;
            Complex[,] rect = new Complex[n, m];
            Complex[] row;
            for (i = 0; i < m; ++i)
            {
                row = matrix[i];
                for (j = 0; j < m; ++j)
                {
                    rect[j, i] = row[j].Conjugate();
                }
            }
            return rect;
        }
        internal static Complex[,] ToRectangularConjugateTransposeParallel(this Complex[][] matrix)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            int m = matrix.Length, n = matrix[0].Length;
            Complex[,] rect = new Complex[n, m];
            Parallel.ForEach(Partitioner.Create(0, m), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (i = min; i < max; ++i)
                {
                    Complex[] row = matrix[i];
                    for (j = 0; j < m; ++j)
                    {
                        rect[j, i] = row[j].Conjugate();
                    }
                }
            });
            return rect;
        }

        internal static T[][] ToJagged<T>(this T[,] matrix)
        {
            T[][] jagged = new T[matrix.GetLength(0)][];

            int cols = matrix.GetLength(1), r, c;
            for (r = 0; r < jagged.Length; ++r)
            {
                T[] row = new T[cols];
                for (c = 0; c < cols; ++c)
                {
                    row[c] = matrix[r, c];
                }
                jagged[r] = row;
            }
            return jagged;
        }
        internal static T[][] ToJaggedParallel<T>(this T[,] matrix)
        {
            T[][] jagged = new T[matrix.GetLength(0)][];

            int cols = matrix.GetLength(1);
            Parallel.For(0, jagged.Length, r =>
            {
                jagged[r] = new T[cols];
                for (int c = 0; c < cols; ++c)
                {
                    jagged[r][c] = matrix[r, c];
                }
            });
            return jagged;
        }
        internal static T[][] ToJaggedTranspose<T>(this T[,] matrix)
        {
            if (matrix == null) throw new ArgumentNullException();

            int rows = matrix.GetLength(1), cols = matrix.GetLength(0), r, c;

            T[][] jagged = new T[rows][];

            for (r = 0; r < rows; ++r)
            {
                T[] row = new T[cols];
                for (c = 0; c < cols; ++c)
                {
                    row[c] = matrix[c, r];
                }
                jagged[r] = row;
            }
            return jagged;
        }
        internal static T[][] ToJaggedTransposeParallel<T>(this T[,] matrix)
        {
            if (matrix == null) throw new ArgumentNullException();

            int rows = matrix.GetLength(1), cols = matrix.GetLength(0);

            T[][] jagged = new T[rows][];

            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, r, c;
                for (r = min; r < max; ++r)
                {
                    T[] row = new T[cols];
                    for (c = 0; c < cols; ++c)
                    {
                        row[c] = matrix[c, r];
                    }
                    jagged[r] = row;
                }
            });
            return jagged;
        }
        internal static Complex[][] ToJaggedConjugateTranspose(this Complex[,] matrix)
        {
            if (matrix == null) throw new ArgumentNullException();

            int rows = matrix.GetLength(1), cols = matrix.GetLength(0), r, c;

            Complex[][] jagged = new Complex[rows][];

            for (r = 0; r < rows; ++r)
            {
                Complex[] row = new Complex[cols];
                for (c = 0; c < cols; ++c)
                {
                    row[c] = matrix[c, r].Conjugate();
                }
                jagged[r] = row;
            }
            return jagged;
        }
        internal static Complex[][] ToJaggedConjugateTransposeParallel(this Complex[,] matrix)
        {
            if (matrix == null) throw new ArgumentNullException();

            int rows = matrix.GetLength(1), cols = matrix.GetLength(0);

            Complex[][] jagged = new Complex[rows][];
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, r, c;
                for (r = min; r < max; ++r)
                {
                    Complex[] row = new Complex[cols];
                    for (c = 0; c < cols; ++c)
                    {
                        row[c] = matrix[c, r].Conjugate();
                    }
                    jagged[r] = row;
                }
            });
            return jagged;
        }

        internal static T[][] Add<T>(this T[][] A, T[][] B) where T : AdditiveGroup<T>, new()
        {
            if (A == null || B == null)
            {
                throw new ArgumentNullException();
            }
            if (A.Length != B.Length || A[0].Length != B[0].Length)
            {
                throw new InvalidOperationException();
            }

            T[][] result = JZero(A[0][0], A.Length, A[0].Length);
            add_unsafe(A, B, result);
            return result;
        }
        internal static T[][] Subtract<T>(this T[][] A, T[][] B) where T: AdditiveGroup<T>, new()
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A.Length != B.Length || A[0].Length != B[0].Length) throw new InvalidOperationException();

            T[][] result = JZero(A[0][0], A.Length, A[0].Length);
            subtract_unsafe(A, B, result);
            return result;
        }

        /// <summary>
        /// TODO: cache multiplications to improve efficiency
        /// 
        /// Calculates the n-way sum-product of a group of n vectors.
        /// If the dimensions of the vectors are dim_1, dim_2, ..., dim_n,
        /// then the n-way sum is the sum of all elements of the tensor 
        /// T = v_1 (x) v_2 (x) ... (x) v_n
        /// where v_i is the i-th vector and (x) denotes tensor multiplication
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="vectors"></param>
        /// <returns></returns>
        internal static T NWaySumProduct<T>(T[][] vectors, T zero, T one, Func<T, T, T> Add, Func<T, T, T> Multiply)
        {
            if (vectors == null || vectors.Length == 0)
            {
                throw new ArgumentNullException();
            }

            T sum = zero;

            int n = vectors.Length;
            int[] lengths = new int[n];
            for (int i = 0; i < n; ++i)
            {
                lengths[i] = vectors[i].Length;
            }

            int[] index = new int[n];
            while (true)
            {
                // Evaluate the product at index
                T product = one;
                for (int i = 0; i < n; ++i)
                {
                    product = Multiply(product, vectors[i][index[i]]);
                }
                sum = Add(sum, product);

                // Increment
                index[0]++;

                // Regulate
                int j = 0;
                while (j < n && index[j] >= lengths[j])
                {
                    index[j] = 0;
                    j++;
                    if (j < n)
                    {
                        index[j]++;
                    }
                    else
                    {
                        return sum;
                    }
                }
            }
        }


        internal static void add_unsafe<T>(this T[][] A, T[][] B, T[][] result) where T : AdditiveGroup<T>, new()
        {
            int rows = A.Length, cols = A[0].Length, r, c;

            for (r = 0; r < rows; ++r)
            {
                T[] A_r = A[r], B_r = B[r], C_r = result[r];
                for (c = 0; c < cols; ++c)
                {
                    C_r[c] = A_r[c].Add(B_r[c]);
                }
            }
        }
        internal static void add_unsafe(this double[][] A, double[][] B, double[][] result)
        {
            int rows = A.Length, cols = A[0].Length, r, c;

            for (r = 0; r < rows; ++r)
            {
                double[] A_r = A[r], B_r = B[r], C_r = result[r];
                for (c = 0; c < cols; ++c)
                {
                    C_r[c] = A_r[c] + B_r[c];
                }
            }
        }

        internal static void subtract_unsafe<T>(this T[][] A, T[][] B, T[][] result) where T : AdditiveGroup<T>, new()
        {
            int rows = A.Length, cols = A[0].Length, r, c;

            for (r = 0; r < rows; ++r)
            {
                T[] A_r = A[r], B_r = B[r], C_r = result[r];
                for (c = 0; c < cols; ++c)
                {
                    C_r[c] = A_r[c].Subtract(B_r[c]);
                }
            }
        }
        internal static void subtract_unsafe(this double[][] A, double[][] B, double[][] result)
        {
            int rows = A.Length, cols = A[0].Length, r, c;

            for (r = 0; r < rows; ++r)
            {
                double[] A_r = A[r], B_r = B[r], C_r = result[r];
                for (c = 0; c < cols; ++c)
                {
                    C_r[c] = A_r[c] - B_r[c];
                }
            }
        }

        internal static void add_unsafe<T>(this T[][][] A, T[][][] B, T[][][] result) where T : AdditiveGroup<T>, new()
        {
            int len = A.Length, i;
            for (i = 0; i < len; ++i)
                add_unsafe(A[i], B[i], result[i]);
        }
        internal static void add_unsafe(this double[][][] A, double[][][] B, double[][][] result)
        {
            int len = A.Length, i;
            for (i = 0; i < len; ++i)
                add_unsafe(A[i], B[i], result[i]);
        }
        internal static void subtract_unsafe<T>(this T[][][] A, T[][][] B, T[][][] result) where T : AdditiveGroup<T>, new()
        {
            int len = A.Length, i;
            for (i = 0; i < len; ++i)
                subtract_unsafe(A[i], B[i], result[i]);
        }
        internal static void subtract_unsafe(this double[][][] A, double[][][] B, double[][][] result)
        {
            int len = A.Length, i;
            for (i = 0; i < len; ++i)
                subtract_unsafe(A[i], B[i], result[i]);
        }

        internal static void add_unsafe<T>(this T[][][][] A, T[][][][] B, T[][][][] result) where T : AdditiveGroup<T>, new()
        {
            int len = A.Length, i;
            for (i = 0; i < len; ++i)
                add_unsafe(A[i], B[i], result[i]);
        }
        internal static void add_unsafe(this double[][][][] A, double[][][][] B, double[][][][] result)
        {
            int len = A.Length, i;
            for (i = 0; i < len; ++i)
                add_unsafe(A[i], B[i], result[i]);
        }
        internal static void subtract_unsafe<T>(this T[][][][] A, T[][][][] B, T[][][][] result) where T : AdditiveGroup<T>, new()
        {
            int len = A.Length, i;
            for (i = 0; i < len; ++i)
                subtract_unsafe(A[i], B[i], result[i]);
        }
        internal static void subtract_unsafe(this double[][][][] A, double[][][][] B, double[][][][] result)
        {
            int len = A.Length, i;
            for (i = 0; i < len; ++i)
                subtract_unsafe(A[i], B[i], result[i]);
        }
    }
}
