﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    public static class MatrixMath
    {
        /// <summary>
        /// <para>For a matrix $A$, returns $A^k$ for integer $k \ge 0$.</para>
        /// 
        /// <para>If $k = 0$, then the identity matrix will be returned. In this case $A$ must be square.</para>
        /// 
        /// <para>
        /// Negative powers are not supported for integer matrices 
        /// since the inverse of a matrix over the integers is not (necessarily)
        /// another matrix over the integers.
        /// </para>
        /// 
        /// <para>
        /// This method uses recursive squaring, and has complexity $O(n^3 \log k)$. Diagonalisation 
        /// is not used in order to remain in the integer field. Another advantage 
        /// of using recursive squaring is it does not assume the diagonalizability
        /// of the matrix, and in particular it does not assume that the matrix is 
        /// square. 
        /// </para>
        /// 
        /// </summary>
        /// <name>Pow</name>
        /// <proto>IMatrix<T> Pow(this IMatrix<T> A, int power)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        public static DenseMatrix<int> Pow(DenseMatrix<int> A, int power) => RecursiveSquaringAlgorithm(A, power, (M, N) => M.Multiply(N));
        public static DenseMatrix<long> Pow(DenseMatrix<long> A, int power) => RecursiveSquaringAlgorithm(A, power, (M, N) => M.Multiply(N));

        /// <summary>
        /// Successive squaring matrix power algorithm
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="power"></param>
        /// <returns></returns>
        private static DenseMatrix<T> RecursiveSquaringAlgorithm<T>(DenseMatrix<T> A, int power, Func<DenseMatrix<T>, DenseMatrix<T>, DenseMatrix<T>> Multiply) where T : new()
        {
            if (power < 0)
            {
                throw new NotImplementedException();
            }
            if (power == 0)
            {
                MatrixChecks.CheckIsSquare(A);
                return DenseMatrix.Identity<T>(A.Rows);
            }
            if (power == 1)
            {
                return A.Copy();
            }

            // For powers > 1
            DenseMatrix<T> Apow = RecursiveSquaringAlgorithm(A, power / 2, Multiply);
            if (power % 2 == 0)
            {
                return Multiply(Apow, Apow);
            }
            else
            {
                return Multiply(A, Multiply(Apow, Apow));
            }
        }
    }
}
