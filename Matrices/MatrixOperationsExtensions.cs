﻿using LinearLite.BLAS;
using LinearLite.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    /// <summary>
    /// Internal methods for jagged matrices, covering elementary operations which are used by other 
    /// matrix types
    /// </summary>
    internal static class MatrixOperationsExtensions
    {
        internal static T[][] ElementwiseOperation<T>(this T[][] A, T[][] B, Func<T, T, T> Operation)
        {
            if (A == null || B == null) throw new ArgumentNullException();

            int m = A.Length, i;
            if (m != B.Length) throw new InvalidOperationException();

            T[][] result = new T[m][];
            for (i = 0; i < m; ++i)
            {
                result[i] = A[i].BinaryOperation(B[i], Operation);
            }
            return result;
        }
        
        #region Matrix-matrix multiplication

        internal static void multiply_unsafe(this double[][] A, double[][] B, double[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length, i, j, k;

            // Extract and store the columns of B
            double[][] b_cols = new double[cols][];
            for (j = 0; j < cols; ++j)
            {
                double[] col = new double[n];
                for (k = 0; k < n; ++k)
                {
                    col[k] = B[k][j];
                }
                b_cols[j] = col;
            }

            for (i = 0; i < rows; ++i)
            {
                double[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    double sum = 0.0;
                    double[] col = b_cols[j];
                    for (k = 0; k < n; ++k)
                    {
                        sum += A_i[k] * col[k];
                    }
                    result_i[j] = sum;
                }
            }
        }

        /// <summary>
        /// Experimental method for now - uses unsafe pointers to loop through the code 
        /// Next step - try blocking...
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="result"></param>
        internal unsafe static void multiply_unsafe_ptr(this double[][] A, double[][] B, double[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length, i, j, k;

            // Extract and store the columns of B
            double[][] b_cols = new double[cols][];
            for (j = 0; j < cols; ++j)
            {
                double[] col = new double[n];
                for (k = 0; k < n; ++k)
                {
                    col[k] = B[k][j];
                }
                b_cols[j] = col;
            }

            fixed (double* A_ptr = A[0], result_ptr = result[0], B_ptr = b_cols[0])
            {
                double* a_itr = A_ptr, 
                    b_itr = B_ptr, 
                    r_itr = result_ptr;

                for (i = 0; i < rows; ++i)
                {
                    for (j = 0; j < cols; ++j)
                    {
                        double sum = 0.0;
                        for (k = 0; k < n; ++k)
                        {
                            sum += *(a_itr + i * n + k) * *(b_itr + j * n + k);
                        }
                        *(r_itr + i * cols + k) = sum;
                    }
                }
            }
        }
        internal static void multiply_block_unsafe(this double[][] A, double[][] B, double[][] result, int blockSize = -1)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length, i, j, k, _i, _j, _k;
            if (blockSize < 0)
            {
                // want blockSize ~ sqrt(N)
                blockSize = 32;
            }

            // Extract and store the columns of B
            double[][] b_cols = new double[cols][];
            for (j = 0; j < cols; ++j)
            {
                double[] col = new double[n];
                for (k = 0; k < n; ++k)
                {
                    col[k] = B[k][j];
                }
                b_cols[j] = col;
            }

            for (i = 0; i < rows; i += blockSize)
            {
                int i_end = i + blockSize;
                for (j = 0; j < cols; j += blockSize)
                {
                    int j_end = j + blockSize;
                    for (k = 0; k < n; k += blockSize)
                    {
                        // Start traversing through block
                        for (_i = i; _i < i_end; ++_i)
                        {
                            double[] A_i = A[_i], result_i = result[_i];
                            for (_j = j; _j < j_end; ++_j)
                            {
                                double[] B_j = b_cols[_j];
                                double sum =
                                            A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++] +
                                             A_i[k] * B_j[k++];
                                result_i[_j] += sum;
                                k -= blockSize;
                            }
                        }
                    }
                }
            }
        }

        internal static void multiply_unsafe(this float[][] A, float[][] B, float[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length, i, j, k;

            // Extract and store the columns of B
            float[][] b_cols = new float[cols][];
            for (j = 0; j < cols; ++j)
            {
                float[] col = new float[n];
                for (k = 0; k < n; ++k)
                {
                    col[k] = B[k][j];
                }
                b_cols[j] = col;
            }

            for (i = 0; i < rows; ++i)
            {
                float[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    float sum = 0.0f;
                    float[] col = b_cols[j];
                    for (k = 0; k < n; ++k)
                    {
                        sum += A_i[k] * col[k];
                    }
                    result_i[j] = sum;
                }
            }
        }
        internal static void multiply_unsafe(this decimal[][] A, decimal[][] B, decimal[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length, i, j, k;

            // Extract and store the columns of B
            decimal[][] b_cols = new decimal[cols][];
            for (j = 0; j < cols; ++j)
            {
                decimal[] col = new decimal[n];
                for (k = 0; k < n; ++k)
                {
                    col[k] = B[k][j];
                }
                b_cols[j] = col;
            }

            for (i = 0; i < rows; ++i)
            {
                decimal[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    decimal sum = 0.0m;
                    decimal[] col = b_cols[j];
                    for (k = 0; k < n; ++k)
                    {
                        sum += A_i[k] * col[k];
                    }
                    result_i[j] = sum;
                }
            }
        }
        internal static void multiply_unsafe(this int[][] A, int[][] B, int[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length, i, j, k;

            // Extract and store the columns of B
            int[][] b_cols = new int[cols][];
            for (j = 0; j < cols; ++j)
            {
                int[] col = new int[n];
                for (k = 0; k < n; ++k)
                {
                    col[k] = B[k][j];
                }
                b_cols[j] = col;
            }

            for (i = 0; i < rows; ++i)
            {
                int[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    int sum = 0;
                    int[] col = b_cols[j];
                    for (k = 0; k < n; ++k)
                    {
                        sum += A_i[k] * col[k];
                    }
                    result_i[j] = sum;
                }
            }
        }
        internal static void multiply_unsafe(this long[][] A, long[][] B, long[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length, i, j, k;

            // Extract and store the columns of B
            long[][] b_cols = new long[cols][];
            for (j = 0; j < cols; ++j)
            {
                long[] col = new long[n];
                for (k = 0; k < n; ++k)
                {
                    col[k] = B[k][j];
                }
                b_cols[j] = col;
            }

            for (i = 0; i < rows; ++i)
            {
                long[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    long sum = 0L;
                    long[] col = b_cols[j];
                    for (k = 0; k < n; ++k)
                    {
                        sum += A_i[k] * col[k];
                    }
                    result_i[j] = sum;
                }
            }
        }
        internal static void multiply_unsafe(this Complex[][] A, Complex[][] B, Complex[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length, i, j, k;

            // Extract and store the columns of B
            Complex[][] b_cols = new Complex[cols][];
            for (j = 0; j < cols; ++j)
            {
                Complex[] col = new Complex[n];
                for (k = 0; k < n; ++k)
                {
                    col[k] = B[k][j];
                }
                b_cols[j] = col;
            }

            Complex a, b;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    double re = 0.0, im = 0.0;
                    Complex[] col = b_cols[j];
                    for (k = 0; k < n; ++k)
                    {
                        a = A_i[k];
                        b = col[k];

                        re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                        im += a.Imaginary * b.Real + a.Real * b.Imaginary;
                    }
                    result_i[j] = new Complex(re, im);
                }
            }
        }
        internal static void multiply_unsafe<T>(this T[][] A, T[][] B, T[][] result) where T : Ring<T>, new()
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length, i, j, k;

            // Extract and store the columns of B
            T[][] b_cols = new T[cols][];
            for (j = 0; j < cols; ++j)
            {
                T[] col = new T[n];
                for (k = 0; k < n; ++k)
                {
                    col[k] = B[k][j];
                }
                b_cols[j] = col;
            }

            for (i = 0; i < rows; ++i)
            {
                T[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    T sum = A[0][0].AdditiveIdentity;
                    T[] col = b_cols[j];
                    for (k = 0; k < n; ++k)
                    {
                        sum = sum.Add(A_i[k].Multiply(col[k]));
                    }
                    result_i[j] = sum;
                }
            }
        }

        internal static double[][] Multiply(this double[][] A, double[][] B)
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A[0].Length != B.Length) throw new InvalidOperationException();

            double[][] result = MatrixInternalExtensions.JMatrix<double>(A.Length, B[0].Length);
            multiply_unsafe(A, B, result);
            return result;
        }
        internal static Complex[][] Multiply(this Complex[][] A, Complex[][] B)
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A[0].Length != B.Length) throw new InvalidOperationException();

            Complex[][] result = MatrixInternalExtensions.JMatrix<Complex>(A.Length, B[0].Length);
            multiply_unsafe(A, B, result);
            return result;
        }

        #endregion

        #region Block-matrix multiplication

        /// <summary>
        /// Multiply a continuous block from A with a continuous block from B, storing the result in 
        /// a continuous block in 'result'. 
        /// There is a more efficient method that requires a workspace for the storage of B^T. 
        /// Without performing such a transposition, the unrolling of a 4-loop offers ~10% performance 
        /// advantage, so it is included here. Unfortunately small-loop unrolling does not offer any 
        /// advantage for the transposed variant. 
        /// </summary>
        /// <param name="A">The left matrix to be multiplied</param>
        /// <param name="B">The 2nd matrix to be multiplied</param>
        /// <param name="result"></param>
        /// <param name="A_start_row">The first row of matrix A to start multiplying</param>
        /// <param name="A_start_col">The first column of matrix A to start multiplying</param>
        /// <param name="B_start_row">The first row of matrix B to start multiplying</param>
        /// <param name="B_start_col">The first column of matrix B to start multiplying</param>
        /// <param name="C_start_row">The first row of the result matrix to store the results</param>
        /// <param name="C_start_col">The first column of the result matrix to store the results</param>
        /// <param name="rows">The number of rows in the resulting multiplied block</param>
        /// <param name="cols">The number of columns in the resulting multiplied block</param>
        /// <param name="N">The numebr of cols in A /or rows in B to multiply together</param>
        /// <param name="increment">If true, result will be incremented by AB. If false, then result will be set to AB</param>
        internal static void multiply_unsafe(this double[][] A, double[][] B, double[][] result, 
            int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, 
            int rows, int cols, int N, bool increment)
        {
            if (N % 4 == 0)
            {
                multiply_unsafe4(A, B, result, A_start_row, A_start_col, B_start_row, B_start_col, C_start_row, C_start_col, rows, cols, N, increment);
                return;
            }

            int A_last_row = A_start_row + rows,
                A_last_col = A_start_col + N,
                B_last_col = B_start_col + cols,
                CA_offset = C_start_row - A_start_row,
                BA_offset = B_start_row - A_start_col,
                CB_offset = C_start_col - B_start_col;

            int i, j, k;
            if (increment)
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    double[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_start_col; j < B_last_col; ++j)
                    {
                        double sum = 0.0;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] += sum;
                    }
                }
            }
            else
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    double[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_start_col; j < B_last_col; ++j)
                    {
                        double sum = 0.0;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] = sum;
                    }
                }
            }
        }
        internal static void multiply_unsafe4(this double[][] A, double[][] B, double[][] result,
            int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_start_row + rows,
                A_last_col = A_start_col + N,
                B_last_col = B_start_col + cols,
                CA_offset = C_start_row - A_start_row,
                BA_offset = B_start_row - A_start_col,
                CB_offset = C_start_col - B_start_col;

            int i, j, k;
            if (increment)
            {
                int BA_offset_1 = BA_offset + 1,
                    BA_offset_2 = BA_offset + 2,
                    BA_offset_3 = BA_offset + 3;

                for (i = A_start_row; i < A_last_row; ++i)
                {
                    double[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_start_col; j < B_last_col; ++j)
                    {
                        double sum = 0.0;
                        for (k = A_start_col; k < A_last_col; k += 4)
                        {
                            sum +=
                                A_i[k] * B[k + BA_offset][j] +
                                A_i[k + 1] * B[k + BA_offset_1][j] +
                                A_i[k + 2] * B[k + BA_offset_2][j] +
                                A_i[k + 3] * B[k + BA_offset_3][j];
                        }
                        result_i[j + CB_offset] += sum;
                    }
                }
            }
            else
            {
                int BA_offset_1 = BA_offset + 1,
                    BA_offset_2 = BA_offset + 2,
                    BA_offset_3 = BA_offset + 3;

                for (i = A_start_row; i < A_last_row; ++i)
                {
                    double[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_start_col; j < B_last_col; ++j)
                    {
                        double sum = 0.0;
                        for (k = A_start_col; k < A_last_col; k += 4)
                        {
                            sum +=
                                A_i[k] * B[k + BA_offset][j] +
                                A_i[k + 1] * B[k + BA_offset_1][j] +
                                A_i[k + 2] * B[k + BA_offset_2][j] +
                                A_i[k + 3] * B[k + BA_offset_3][j];
                        }
                        result_i[j + CB_offset] = sum;
                    }
                }
            }
        }
        internal static void multiply_unsafe(this double[][] A, double[][] B, double[][] Bt_workspace, double[][] result,
            int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col, 
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_start_row + rows,
                A_last_col = A_start_col + N,
                CA_offset = C_start_row - A_start_row;

            int i, j, k;

            // Transpose and shift
            for (i = 0; i < cols; ++i)
            {
                double[] Bt_i = Bt_workspace[i];
                int _i = i + B_start_col, 
                    _j = B_start_row;
                for (j = 0; j < N; ++j, ++_j)
                {
                    Bt_i[j] = B[_j][_i];
                }
            }

            if (increment)
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    double[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        double[] B_j = Bt_workspace[j];
                        double sum = 0.0;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] += sum;
                    }
                }
            }
            else
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    double[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        double[] B_j = Bt_workspace[j];
                        double sum = 0.0;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] = sum;
                    }
                }
            }
        }

        internal static void multiply_unsafe(this int[][] A, int[][] B, int[][] result,
            int A_offset_row, int A_offset_col, int B_offset_row, int B_offset_col, int C_offset_row, int C_offset_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_offset_row + rows,
                A_last_col = A_offset_col + N,
                B_last_col = B_offset_col + cols,
                CA_offset = C_offset_row - A_offset_row,
                BA_offset = B_offset_row - A_offset_col,
                CB_offset = C_offset_col - B_offset_col;

            int i, j, k;
            if (increment)
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    int[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        int sum = 0;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] += sum;
                    }
                }
            }
            else
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    int[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        int sum = 0;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] = sum;
                    }
                }
            }
        }
        internal static void multiply_unsafe(this int[][] A, int[][] B, int[][] Bt_workspace, int[][] result,
            int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_start_row + rows,
                A_last_col = A_start_col + N,
                CA_offset = C_start_row - A_start_row;

            int i, j, k;

            // Transpose and shift
            for (i = 0; i < cols; ++i)
            {
                int[] Bt_i = Bt_workspace[i];
                int _i = i + B_start_col,
                    _j = B_start_row;
                for (j = 0; j < N; ++j, ++_j)
                {
                    Bt_i[j] = B[_j][_i];
                }
            }

            if (increment)
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    int[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        int[] B_j = Bt_workspace[j];
                        int sum = 0;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] += sum;
                    }
                }
            }
            else
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    int[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        int[] B_j = Bt_workspace[j];
                        int sum = 0;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] = sum;
                    }
                }
            }
        }

        internal static void multiply_unsafe(this long[][] A, long[][] B, long[][] result,
            int A_offset_row, int A_offset_col, int B_offset_row, int B_offset_col, int C_offset_row, int C_offset_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_offset_row + rows,
                A_last_col = A_offset_col + N,
                B_last_col = B_offset_col + cols,
                CA_offset = C_offset_row - A_offset_row,
                BA_offset = B_offset_row - A_offset_col,
                CB_offset = C_offset_col - B_offset_col;

            int i, j, k;
            if (increment)
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    long[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        long sum = 0L;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] += sum;
                    }
                }
            }
            else
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    long[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        long sum = 0L;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] = sum;
                    }
                }
            }
        }
        internal static void multiply_unsafe(this long[][] A, long[][] B, long[][] Bt_workspace, long[][] result,
            int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_start_row + rows,
                A_last_col = A_start_col + N,
                CA_offset = C_start_row - A_start_row;

            int i, j, k;

            // Transpose and shift
            for (i = 0; i < cols; ++i)
            {
                long[] Bt_i = Bt_workspace[i];
                int _i = i + B_start_col,
                    _j = B_start_row;
                for (j = 0; j < N; ++j, ++_j)
                {
                    Bt_i[j] = B[_j][_i];
                }
            }

            if (increment)
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    long[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        long[] B_j = Bt_workspace[j];
                        long sum = 0L;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] += sum;
                    }
                }
            }
            else
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    long[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        long[] B_j = Bt_workspace[j];
                        long sum = 0L;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] = sum;
                    }
                }
            }
        }

        internal static void multiply_unsafe(this float[][] A, float[][] B, float[][] result,
            int A_offset_row, int A_offset_col, int B_offset_row, int B_offset_col, int C_offset_row, int C_offset_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_offset_row + rows,
                A_last_col = A_offset_col + N,
                B_last_col = B_offset_col + cols,
                CA_offset = C_offset_row - A_offset_row,
                BA_offset = B_offset_row - A_offset_col,
                CB_offset = C_offset_col - B_offset_col;

            int i, j, k;
            if (increment)
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    float[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        float sum = 0.0f;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] += sum;
                    }
                }
            }
            else
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    float[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        float sum = 0.0f;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] = sum;
                    }
                }
            }
        }
        internal static void multiply_unsafe(this float[][] A, float[][] B, float[][] Bt_workspace, float[][] result,
            int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_start_row + rows,
                A_last_col = A_start_col + N,
                CA_offset = C_start_row - A_start_row;

            int i, j, k;

            // Transpose and shift
            for (i = 0; i < cols; ++i)
            {
                float[] Bt_i = Bt_workspace[i];
                int _i = i + B_start_col,
                    _j = B_start_row;
                for (j = 0; j < N; ++j, ++_j)
                {
                    Bt_i[j] = B[_j][_i];
                }
            }

            if (increment)
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    float[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        float[] B_j = Bt_workspace[j];
                        float sum = 0.0f;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] += sum;
                    }
                }
            }
            else
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    float[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        float[] B_j = Bt_workspace[j];
                        float sum = 0.0f;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] = sum;
                    }
                }
            }
        }

        internal static void multiply_unsafe(this decimal[][] A, decimal[][] B, decimal[][] result,
            int A_offset_row, int A_offset_col, int B_offset_row, int B_offset_col, int C_offset_row, int C_offset_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_offset_row + rows,
                A_last_col = A_offset_col + N,
                B_last_col = B_offset_col + cols,
                CA_offset = C_offset_row - A_offset_row,
                BA_offset = B_offset_row - A_offset_col,
                CB_offset = C_offset_col - B_offset_col;

            int i, j, k;
            if (increment)
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    decimal[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        decimal sum = 0.0m;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] += sum;
                    }
                }
            }
            else
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    decimal[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        decimal sum = 0.0m;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B[k + BA_offset][j];
                        }
                        result_i[j + CB_offset] = sum;
                    }
                }
            }
        }
        internal static void multiply_unsafe(this decimal[][] A, decimal[][] B, decimal[][] Bt_workspace, decimal[][] result,
            int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_start_row + rows,
                A_last_col = A_start_col + N,
                CA_offset = C_start_row - A_start_row;

            int i, j, k;

            // Transpose and shift
            for (i = 0; i < cols; ++i)
            {
                decimal[] Bt_i = Bt_workspace[i];
                int _i = i + B_start_col,
                    _j = B_start_row;
                for (j = 0; j < N; ++j, ++_j)
                {
                    Bt_i[j] = B[_j][_i];
                }
            }

            if (increment)
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    decimal[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        decimal[] B_j = Bt_workspace[j];
                        decimal sum = 0.0m;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] += sum;
                    }
                }
            }
            else
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    decimal[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        decimal[] B_j = Bt_workspace[j];
                        decimal sum = 0.0m;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            sum += A_i[k] * B_j[k - A_start_col];
                        }
                        result_i[j + C_start_col] = sum;
                    }
                }
            }
        }

        internal static void multiply_unsafe(this Complex[][] A, Complex[][] B, Complex[][] result,
            int A_offset_row, int A_offset_col, int B_offset_row, int B_offset_col, int C_offset_row, int C_offset_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_offset_row + rows,
                A_last_col = A_offset_col + N,
                B_last_col = B_offset_col + cols,
                CA_offset = C_offset_row - A_offset_row,
                BA_offset = B_offset_row - A_offset_col,
                CB_offset = C_offset_col - B_offset_col;

            int i, j, k;
            if (increment)
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    Complex[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        double re = 0.0, im = 0.0;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            Complex a = A_i[k], b = B[k + BA_offset][j];
                            re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                            im += a.Real * b.Imaginary + a.Imaginary * b.Real;
                        }
                        result_i[j + CB_offset].IncrementBy(new Complex(re, im));
                    }
                }
            }
            else
            {
                for (i = A_offset_row; i < A_last_row; ++i)
                {
                    Complex[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = B_offset_col; j < B_last_col; ++j)
                    {
                        double re = 0.0, im = 0.0;
                        for (k = A_offset_col; k < A_last_col; ++k)
                        {
                            Complex a = A_i[k], b = B[k + BA_offset][j];
                            re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                            im += a.Real * b.Imaginary + a.Imaginary * b.Real;
                        }
                        result_i[j + CB_offset] = new Complex(re, im);
                    }
                }
            }
        }
        internal static void multiply_unsafe(this Complex[][] A, Complex[][] B, Complex[][] Bt_workspace, Complex[][] result,
            int A_start_row, int A_start_col, int B_start_row, int B_start_col, int C_start_row, int C_start_col,
            int rows, int cols, int N, bool increment)
        {
            int A_last_row = A_start_row + rows,
                A_last_col = A_start_col + N,
                CA_offset = C_start_row - A_start_row;

            int i, j, k;

            // Transpose and shift
            for (i = 0; i < cols; ++i)
            {
                Complex[] Bt_i = Bt_workspace[i];
                int _i = i + B_start_col,
                    _j = B_start_row;
                for (j = 0; j < N; ++j, ++_j)
                {
                    Bt_i[j] = B[_j][_i];
                }
            }

            if (increment)
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    Complex[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        Complex[] B_j = Bt_workspace[j];
                        double re = 0.0, im = 0.0;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            Complex a = A_i[k], b = B_j[k - A_start_col];
                            re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                            im += a.Real * b.Imaginary + a.Imaginary * b.Real;
                        }
                        result_i[j + C_start_col].IncrementBy(new Complex(re, im));
                    }
                }
            }
            else
            {
                for (i = A_start_row; i < A_last_row; ++i)
                {
                    Complex[] A_i = A[i], result_i = result[i + CA_offset];
                    for (j = 0; j < cols; ++j)
                    {
                        Complex[] B_j = Bt_workspace[j];
                        double re = 0.0, im = 0.0;
                        for (k = A_start_col; k < A_last_col; ++k)
                        {
                            Complex a = A_i[k], b = B_j[k - A_start_col];
                            re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                            im += a.Real * b.Imaginary + a.Imaginary * b.Real;
                        }
                        result_i[j + C_start_col] = new Complex(re, im);
                    }
                }
            }
        }

        internal static void multiply_parallel_unsafe(this double[][] A, double[][] B, double[][] result,
            int A_offset_row, int A_offset_col,
            int B_offset_row, int B_offset_col,
            int C_offset_row, int C_offset_col,
            int rows, int cols, int N, bool increment)
        {
            if (increment)
            {
                Parallel.ForEach(Partitioner.Create(0, rows), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j, k;
                    for (i = min; i < max; ++i)
                    {
                        double[] A_i = A[i + A_offset_row], result_i = result[i + C_offset_row];
                        for (j = 0; j < cols; ++j)
                        {
                            int B_col = B_offset_col + j;
                            double sum = 0.0;
                            for (k = 0; k < N; ++k)
                            {
                                sum += A_i[k + A_offset_col] * B[k + B_offset_row][B_col];
                            }
                            result_i[j + C_offset_col] += sum;
                        }
                    }
                });
            }
            else
            {
                Parallel.ForEach(Partitioner.Create(0, rows), range =>
                {
                    int min = range.Item1, max = range.Item2, i, j, k;
                    for (i = min; i < max; ++i)
                    {
                        double[] A_i = A[i + A_offset_row], result_i = result[i + C_offset_row];
                        for (j = 0; j < cols; ++j)
                        {
                            int B_col = B_offset_col + j;
                            double sum = 0.0;
                            for (k = 0; k < N; ++k)
                            {
                                sum += A_i[k + A_offset_col] * B[k + B_offset_row][B_col];
                            }
                            result_i[j + C_offset_col] = sum;
                        }
                    }
                });
            }
        }
        #endregion

        #region Matrix-vector multiplication

        internal static void multiply_unsafe(this double[][] A, double[] x, double[] result)
        {
            int rows = A.Length, cols = x.Length, i, j;
            for (i = 0; i < rows; ++i)
            {
                double[] A_i = A[i];
                double sum = 0.0;
                for (j = 0; j < cols; ++j)
                {
                    sum += A_i[j] * x[j];
                }
                result[i] = sum;
            }
        }

        #endregion

        #region Parallel multiplication 

        internal static void multiply_parallel_unsafe(this int[][] A, int[][] B, int[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length;

            // Extract and store the columns of B
            int[][] Bt = B.TransposeParallel();
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j, k;
                for (i = min; i < max; ++i)
                {
                    int[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < cols; ++j)
                    {
                        int sum = 0;
                        int[] col = Bt[j];
                        for (k = 0; k < n; ++k)
                        {
                            sum += A_i[k] * col[k];
                        }
                        result_i[j] = sum;
                    }
                }
            });
        }
        internal static void multiply_parallel_unsafe(this long[][] A, long[][] B, long[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length;

            // Extract and store the columns of B
            long[][] Bt = B.TransposeParallel();
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j, k;
                for (i = min; i < max; ++i)
                {
                    long[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < cols; ++j)
                    {
                        long sum = 0L;
                        long[] col = Bt[j];
                        for (k = 0; k < n; ++k)
                        {
                            sum += A_i[k] * col[k];
                        }
                        result_i[j] = sum;
                    }
                }
            });
        }
        internal static void multiply_parallel_unsafe(this float[][] A, float[][] B, float[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length;

            // Extract and store the columns of B
            float[][] Bt = B.TransposeParallel();
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j, k;
                for (i = min; i < max; ++i)
                {
                    float[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < cols; ++j)
                    {
                        float sum = 0.0f;
                        float[] col = Bt[j];
                        for (k = 0; k < n; ++k)
                        {
                            sum += A_i[k] * col[k];
                        }
                        result_i[j] = sum;
                    }
                }
            });
        }
        internal static void multiply_parallel_unsafe(this double[][] A, double[][] B, double[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length;

            // Extract and store the columns of B
            double[][] Bt = B.TransposeParallel();
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j, k;
                for (i = min; i < max; ++i)
                {
                    double[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < cols; ++j)
                    {
                        double sum = 0.0;
                        double[] col = Bt[j];
                        for (k = 0; k < n; ++k)
                        {
                            sum += A_i[k] * col[k];
                        }
                        result_i[j] = sum;
                    }
                }
            });
        }
        internal static void multiply_parallel_unsafe(this decimal[][] A, decimal[][] B, decimal[][] result)
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length;

            // Extract and store the columns of B
            decimal[][] Bt = B.TransposeParallel();
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j, k;
                for (i = min; i < max; ++i)
                {
                    decimal[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < cols; ++j)
                    {
                        decimal sum = 0.0m;
                        decimal[] col = Bt[j];
                        for (k = 0; k < n; ++k)
                        {
                            sum += A_i[k] * col[k];
                        }
                        result_i[j] = sum;
                    }
                }
            });
        }
        internal static void multiply_parallel_unsafe(this Complex[][] A, Complex[][] B, Complex[][] result)
        {
            int m = A.Length, n = B.Length, p = B[0].Length;
            multiply_parallel_unsafe(A, B, MatrixInternalExtensions.JMatrix<Complex>(p, n), result, m, n, p);
        }
        internal static void multiply_parallel_unsafe<T>(this T[][] A, T[][] B, T[][] result) where T : Ring<T>, new()
        {
            int rows = A.Length, cols = B[0].Length, n = A[0].Length;

            // Extract and store the columns of B
            T[][] Bt = B.TransposeParallel();
            T zero = new T().AdditiveIdentity;

            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j, k;
                for (i = min; i < max; ++i)
                {
                    T[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < cols; ++j)
                    {
                        T sum = zero;
                        T[] col = Bt[j];
                        for (k = 0; k < n; ++k)
                        {
                            sum = sum.Add(A_i[k].Multiply(col[k]));
                        }
                        result_i[j] = sum;
                    }
                }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="Bt_workspace">Arbitrary matrix of at least the dimensionality of B^T. Will be used as a workspace and overwritten with B^T.</param>
        /// <param name="result"></param>
        internal static void multiply_parallel_unsafe(this Complex[][] A, Complex[][] B, Complex[][] Bt_workspace, Complex[][] result, int rows, int n, int cols)
        {
            ComplexBLAS blas = new ComplexBLAS();

            // Extract and store the columns of B
            Parallel.ForEach(Partitioner.Create(0, cols), range =>
            {
                int min = range.Item1, max = range.Item2, j, k;
                for (j = min; j < max; ++j)
                {
                    Complex[] col = Bt_workspace[j];
                    for (k = 0; k < n; ++k)
                    {
                        col[k] = B[k][j];
                    }
                }
            });

            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (i = min; i < max; ++i)
                {
                    Complex[] A_i = A[i], result_i = result[i];
                    for (j = 0; j < cols; ++j)
                    {
                        result_i[j] = blas.DOT(A_i, Bt_workspace[j], 0, n);
                    }
                }
            });
        }

        #endregion

        #region Multiplication via overwrite 
        /// <summary>
        /// B <- AB
        /// Only works for square matrices
        /// Can be more computationally demanding since there are more cache misses because of suboptimal access patterns
        /// However this method is more memory efficient since it does not generate a new matrix. 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        internal static void multiply_overwrite_unsafe(this double[][] A, double[][] B, bool overwriteFirst)
        {
            int n = A.Length, i, j, k;

            if (overwriteFirst)
            {
                double[] row = new double[n];
                for (i = 0; i < n; ++i)
                {
                    double[] A_i = A[i];
                    for (j = 0; j < n; ++j)
                    {
                        double sum = 0.0;
                        for (k = 0; k < n; ++k)
                        {
                            sum += A_i[k] * B[k][j];
                        }
                        row[j] = sum;
                    }
                    for (j = 0; j < n; ++j)
                    {
                        A_i[j] = row[j];
                    }
                }
            }
            else
            {
                double[] column = new double[n], B_j = new double[n];
                for (j = 0; j < n; ++j)
                {
                    // Clear column
                    for (i = 0; i < n; ++i)
                    {
                        B_j[i] = B[i][j];
                    }

                    // Calculate product for one of the columns
                    for (i = 0; i < n; ++i)
                    {
                        double[] A_i = A[i];

                        double sum = 0.0;
                        for (k = 0; k < n; ++k)
                        {
                            sum += A_i[k] * B_j[k];
                        }
                        column[i] = sum;
                    }

                    // Set j-th column of B
                    for (i = 0; i < n; ++i)
                    {
                        B[i][j] = column[i];
                    }
                }
            }
        }
        internal static void multiply_overwrite_unsafe<T>(this T[][] A, T[][] B, bool overwriteFirst, IBLAS<T> blas)
        {
            int n = A.Length, i, j;
            if (overwriteFirst)
            {
                T[] row = new T[n];
                for (i = 0; i < n; ++i)
                {
                    T[] A_i = A[i];
                    for (j = 0; j < n; ++j)
                    {
                        row[j] = blas.AMULT(A_i, B, j, 0, n);
                    }
                    for (j = 0; j < n; ++j)
                    {
                        A_i[j] = row[j];
                    }
                }
            }
            else
            {
                T[] column = new T[n], B_j = new T[n];
                for (j = 0; j < n; ++j)
                {
                    for (i = 0; i < n; ++i)
                    {
                        B_j[i] = B[i][j];
                    }

                    // Calculate product for one of the columns
                    for (i = 0; i < n; ++i)
                    {
                        column[i] = blas.DOT(A[i], B_j, 0, n);
                    }

                    // Set j-th column of B
                    for (i = 0; i < n; ++i)
                    {
                        B[i][j] = column[i];
                    }
                }
            }
        }
        internal static void multiply_overwrite_unsafe(this int[][] A, int[][] B, bool overwriteFirst) => multiply_overwrite_unsafe(A, B, overwriteFirst, new IntBLAS());
        internal static void multiply_overwrite_unsafe(this long[][] A, long[][] B, bool overwriteFirst) => multiply_overwrite_unsafe(A, B, overwriteFirst, new LongBLAS());
        internal static void multiply_overwrite_unsafe(this float[][] A, float[][] B, bool overwriteFirst) => multiply_overwrite_unsafe(A, B, overwriteFirst, new FloatBLAS());
        internal static void multiply_overwrite_unsafe(this decimal[][] A, decimal[][] B, bool overwriteFirst) => multiply_overwrite_unsafe(A, B, overwriteFirst, new DecimalBLAS());
        internal static void multiply_overwrite_unsafe(this Complex[][] A, Complex[][] B, bool overwriteFirst) => multiply_overwrite_unsafe(A, B, overwriteFirst, new ComplexBLAS());
        internal static void multiply_overwrite_unsafe<T>(this T[][] A, T[][] B, bool overwriteFirst) where T : Ring<T>, new() => multiply_overwrite_unsafe(A, B, overwriteFirst, new RingBLAS<T>());

        internal static void multiply_overwrite_parallel_unsafe(this double[][] A, double[][] B, bool overwriteFirst)
        {
            int n = A.Length;
            
            if (overwriteFirst)
            {
                double[] row = new double[n];
                int i;
                for (i = 0; i < n; ++i)
                {
                    double[] A_i = A[i];

                    Parallel.ForEach(Partitioner.Create(0, n), range =>
                    {
                        int min = range.Item1, max = range.Item2, j, k;
                        for (j = min; j < max; ++j)
                        {
                            double sum = 0.0;
                            for (k = 0; k < n; ++k)
                            {
                                sum += A_i[k] * B[k][j];
                            }
                            row[j] = sum;
                        }
                    });
                    
                    for (int j = 0; j < n; ++j)
                    {
                        A_i[j] = row[j];
                    }
                }
            }
            else
            {
                double[] column = new double[n], B_j = new double[n];
                for (int j = 0; j < n; ++j)
                {
                    // Clear column
                    for (int i = 0; i < n; ++i)
                    {
                        B_j[i] = B[i][j];
                    }

                    // Calculate product for one of the columns
                    Parallel.ForEach(Partitioner.Create(0, n), range =>
                    {
                        int min = range.Item1, max = range.Item2, i, k;
                        for (i = min; i < max; ++i)
                        {
                            double[] A_i = A[i];
                            double sum = 0.0;
                            for (k = 0; k < n; ++k)
                            {
                                sum += A_i[k] * B_j[k];
                            }
                            column[i] = sum;
                        }
                    });


                    // Set j-th column of B
                    for (int i = 0; i < n; ++i)
                    {
                        B[i][j] = column[i];
                    }
                }
            }
            
        }
        internal static void multiply_overwrite_parallel_unsafe<T>(this T[][] A, T[][] B, bool overwriteFirst, IBLAS<T> blas)
        {
            int n = A.Length, j;

            T[] column = new T[n], B_j = new T[n];
            for (j = 0; j < n; ++j)
            {
                // Clear column
                for (int i = 0; i < n; ++i)
                {
                    B_j[i] = B[i][j];
                }

                // Calculate product for one of the columns
                Parallel.ForEach(Partitioner.Create(0, n), range =>
                {
                    int min = range.Item1, max = range.Item2, i;
                    for (i = min; i < max; ++i)
                    {
                        column[i] = blas.DOT(A[i], B_j, 0, n);
                    }
                });

                // Set j-th column of B
                for (int i = 0; i < n; ++i)
                {
                    B[i][j] = column[i];
                }
            }
        }
        internal static void multiply_overwrite_parallel_unsafe(this int[][] A, int[][] B, bool overwriteFirst) => multiply_overwrite_parallel_unsafe(A, B, overwriteFirst, new IntBLAS());
        internal static void multiply_overwrite_parallel_unsafe(this long[][] A, long[][] B, bool overwriteFirst) => multiply_overwrite_parallel_unsafe(A, B, overwriteFirst, new LongBLAS());
        internal static void multiply_overwrite_parallel_unsafe(this float[][] A, float[][] B, bool overwriteFirst) => multiply_overwrite_parallel_unsafe(A, B, overwriteFirst, new FloatBLAS());
        internal static void multiply_overwrite_parallel_unsafe(this decimal[][] A, decimal[][] B, bool overwriteFirst) => multiply_overwrite_parallel_unsafe(A, B, overwriteFirst, new DecimalBLAS());
        internal static void multiply_overwrite_parallel_unsafe(this Complex[][] A, Complex[][] B, bool overwriteFirst) => multiply_overwrite_parallel_unsafe(A, B, overwriteFirst, new ComplexBLAS());
        internal static void multiply_overwrite_parallel_unsafe<T>(this T[][] A, T[][] B, bool overwriteFirst) where T : Ring<T>, new() => multiply_overwrite_parallel_unsafe(A, B, overwriteFirst, new RingBLAS<T>());

        #endregion

        #region Increment
        internal static void increment_unsafe(this int[][] A, int[][] B)
        {
            int rows = A.Length, cols = A[0].Length, i, j;
            for (i = 0; i < rows; ++i)
            {
                int[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] += B_i[j];
                }
            }
        }
        internal static void increment_unsafe(this long[][] A, long[][] B)
        {
            int rows = A.Length, cols = A[0].Length, i, j;
            for (i = 0; i < rows; ++i)
            {
                long[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] += B_i[j];
                }
            }
        }
        internal static void increment_unsafe(this float[][] A, float[][] B)
        {
            int rows = A.Length, cols = A[0].Length, i, j;
            for (i = 0; i < rows; ++i)
            {
                float[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] += B_i[j];
                }
            }
        }
        internal static void increment_unsafe(this double[][] A, double[][] B)
        {
            int rows = A.Length, cols = A[0].Length, i, j;
            for (i = 0; i < rows; ++i)
            {
                double[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] += B_i[j];
                }
            }
        }
        internal static void increment_unsafe(this decimal[][] A, decimal[][] B)
        {
            int rows = A.Length, cols = A[0].Length, i, j;
            for (i = 0; i < rows; ++i)
            {
                decimal[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] += B_i[j];
                }
            }
        }
        internal static void increment_unsafe(this Complex[][] A, Complex[][] B)
        {
            int rows = A.Length, cols = A[0].Length, i, j;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j].IncrementBy(B_i[j]);
                }
            }
        }
        internal static void increment_unsafe<T>(this T[][] A, T[][] B) where T : AdditiveGroup<T>, new()
        {
            int rows = A.Length, cols = A[0].Length, i, j;
            for (i = 0; i < rows; ++i)
            {
                T[] A_i = A[i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[j] = A_i[j].Add(B_i[j]);
                }
            }
        }
        #endregion

        #region Inversion 

        internal static Complex[][] Invert(this Complex[][] A, Complex[][] inverse)
        {
            return invert_inner(A, inverse,
                new ComplexBLAS(),
                x => x.MultiplicativeInverse(),
                x => x.Modulus(),
                (x, s) => // Shift bits left
                {
                    if (s > 0) ComplexBLAS.dSCAL(x, 1.0 / (1 << s), 0, x.Length);
                    else if (s < 0) ComplexBLAS.dSCAL(x, 1 << Math.Abs(s), 0, x.Length);
                },
                (x, s) =>
                {
                    if (s > 0) return x.Multiply(1 << s);
                    if (s < 0) return x.Multiply(1.0 / (1 << Math.Abs(s)));
                    return x;
                });
        }
        internal static T[][] invert_inner<T>(T[][] A, T[][] inverse, IBLAS<T> blas, Func<T, T> Invert, 
            Func<T, double> Norm, Action<T[], int> ShiftBitsLeft, Func<T, int, T> ShiftBitRight)
        {
            if (A == null) throw new ArgumentNullException();
            if (A.Length != A[0].Length) throw new InvalidOperationException();

            int n = A.Length, i_max, i, j, k;
            T alpha, zero = blas.Zero, one = blas.One;

            // shape into correct order
            double order = 0;
            for (i = 0; i < n; ++i)
            {
                T[] A_i = A[i];
                for (j = 0; j < n; ++j)
                {
                    order += Math.Log(Norm(A_i[j]) + 0.01);
                }
            }

            int shift = (int)Math.Round((order / (n * n)) / Math.Log(2));

            // x_norm stores the values of x after order adjustment
            T[][] A_norm = new T[n][];
            for (i = 0; i < n; i++)
            {
                T[] A_norm_i = A[i].Copy();
                ShiftBitsLeft(A_norm_i, shift);
                A_norm[i] = A_norm_i;
            }

            // Set inv to be the identity matrix 
            
            for (i = 0; i < n; ++i)
            {
                T[] inv_i = inverse[i];
                for (j = 0; j < n; ++j)
                {
                    inv_i[j] = zero;
                }
                inv_i[i] = one;
            }

            double max;
            for (k = 0; k < n - 1; k++)
            {
                // find i_max = argmax(x[i,k] for i = k, ..., n) - for numerical stability
                i_max = k + 1;
                max = Norm(A_norm[i_max][k]);
                for (i = k + 2; i < n; i++)
                {
                    double norm = Norm(A_norm[i][k]);
                    if (norm > max)
                    {
                        i_max = i;
                        max = norm;
                    }
                }

                // swap row k with row i_max
                T[] temp = A_norm[i_max];
                A_norm[i_max] = A_norm[k];
                A_norm[k] = temp;

                temp = inverse[i_max];
                inverse[i_max] = inverse[k];
                inverse[k] = temp;

                // for each row below k, i.e. for i = k + 1 : n
                T[] A_norm_k = A_norm[k], inv_k = inverse[k];
                T factor = Invert(A_norm_k[k]);
                for (i = k + 1; i < n; i++)
                {
                    T[] A_norm_i = A_norm[i];
                    alpha = blas.Multiply(A_norm_i[k], factor);

                    blas.YSAX(A_norm_i, A_norm_k, alpha, k + 1, n);
                    blas.YSAX(inverse[i], inv_k, alpha, 0, n);
                    A_norm_i[k] = zero;
                }
            }

            // going back upwards
            for (k = n - 1; k >= 0; k--)
            {
                T[] A_norm_k = A_norm[k], inv_k = inverse[k];
                T factor = Invert(A_norm_k[k]);
                for (i = k - 1; i >= 0; i--)
                {
                    alpha = blas.Multiply(A_norm[i][k], factor);
                    blas.YSAX(A_norm[i], A_norm_k, alpha, i + 1, n);
                    blas.YSAX(inverse[i], inv_k, alpha, 0, n);
                }
            }

            // normalise - with order
            for (k = 0; k < n; k++)
            {
                alpha = Invert(ShiftBitRight(A_norm[k][k], shift));
                blas.SCAL(inverse[k], alpha, 0, n);
            }
            return inverse;
        }
        #endregion

        internal static void OuterProduct(this double[] u, double[] v, double[][] product)
        {
            int m = u.Length, n = v.Length, i, j;
            for (i = 0; i < m; ++i)
            {
                double[] product_i = product[i];
                double u_i = u[i];
                for (j = 0; j < n; ++j)
                {
                    product_i[j] = u_i * v[j];
                }
            }
        }

    }
}
