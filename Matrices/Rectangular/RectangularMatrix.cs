﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class RectangularMatrix
    {
        private static Random _random = new Random();
        private static Type[] _acceptedTypes = { typeof(int), typeof(long), typeof(double), typeof(float), typeof(decimal), typeof(Complex) }; // todo

        private static void CheckTypeSupport(Type t)
        {
            if (!_acceptedTypes.Contains(t))
            {
                throw new ArgumentException($"Unsupported data type: {t}.");
            }
        }

        /// <summary>
        /// Obtain the identity element, e, of a type system.
        /// </summary>
        /// <param name="t"></param>
        private static dynamic GetMultiplicativeIdentityOfType(Type t)
        {
            if (t == typeof(double) || t == typeof(int) || t == typeof(long) || t == typeof(float) || t == typeof(decimal))
            {
                return 1;
            }
            if (t == typeof(Complex)) return Complex.One;
            if (t == typeof(Rational)) return Rational.One;
            //if (t is Polynomial) return default(Polynomial).AdditiveIdentity;

            throw new NotImplementedException();
        }
        private static dynamic GetAdditiveIdentityOfType(Type t)
        {
            if (t == typeof(double) || t == typeof(int) || t == typeof(long) || t == typeof(float) || t == typeof(decimal))
            {
                return 0;
            }
            if (t == typeof(Complex)) return Complex.Zero;
            if (t == typeof(Rational)) return Rational.Zero;
            //if (t == typeof(Polynomial)) return Polynomial.Zero;

            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialises a (m x n) zero matrix
        /// </summary>
        /// <param name="m">The number of rows</param>
        /// <param name="n">The number of columns</param>
        /// <returns>A (m x n) zero matrix.</returns>
        public static T[,] Zero<T>(int m, int n)
        {
            CheckTypeSupport(typeof(T));
            if (m <= 0)
            {
                throw new ArgumentOutOfRangeException("The number of rows must be positive. Received: " + m);
            }
            if (n <= 0)
            {
                throw new ArgumentOutOfRangeException("The number of columns must be positive. Received: " + n);
            }

            // Faster this way - look to implement more generic checking later
            if (typeof(T) == typeof(double))
            {
                return new T[m, n];
            }

            T e = GetAdditiveIdentityOfType(typeof(T));

            var result = new T[m, n];
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    result[i, j] = e;
                }
            }
            return result;
        }

        /// <summary>
        /// Initialises a (n x n) zero square matrix
        /// </summary>
        /// <param name="n">The number of rows/columns</param>
        /// <returns>A (n x n) zero matrix.</returns>
        public static T[,] Zero<T>(int n)
        {
            return Zero<T>(n, n);
        }

        /// <summary>
        /// Initialises a (n x n) identity matrix (1's in the leading diagonal, 0's everywhere else)
        /// </summary>
        /// <param name="n">The number of rows/columns</param>
        /// <returns>The n x n identity matrix</returns>
        public static T[,] Identity<T>(int n)
        {
            CheckTypeSupport(typeof(T));

            T[,] identity = Zero<T>(n, n);

            T e = GetMultiplicativeIdentityOfType(typeof(T));
            for (int i = 0; i < n; ++i)
            {
                identity[i,i] = e;
            }
            return identity;
        }

        /// <summary>
        /// Creates a diagonal matrix out of values of the main diagonal.
        /// </summary>
        /// <param name="diagonal"></param>
        /// <returns>A matrix with 'diagonal' values along the main diagonal, and 0 elsewhere.</returns>
        public static double[,] Diag(params double[] diagonal)
        {
            if (diagonal == null || diagonal.Length == 0)
            {
                throw new ArgumentException("Required parameter 'diagonal' is null or empty.");
            }

            int n = diagonal.Length, i;

            double[,] matrix = new double[n, n];
            for (i = 0; i < n; i++)
            {
                matrix[i,i] = diagonal[i];
            }
            return matrix;
        }
        public static T[,] Diag<T>(params T[] diagonal) where T : Group<T>, new()
        {
            if (diagonal == null || diagonal.Length == 0)
            {
                throw new ArgumentNullException("Required parameter 'diagonal' is null or empty.");
            }

            T e = diagonal[0].Identity;

            int n = diagonal.Length, i, j;

            T[,] matrix = new T[n, n];
            for (i = 0; i < n; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    if (i == j)
                    {
                        matrix[i, i] = diagonal[i];
                    }
                    else
                    {
                        matrix[i, j] = e;
                    }
                }
            }
            return matrix;
        }


        /// <summary>
        /// Initialises a zero-vector in R^n
        /// </summary>
        /// <param name="n">The dimension of the vector</param>
        /// <returns>A n-dimension vector of 0's</returns>
        public static T[] Vector<T>(int n)
        {
            if (n <= 0)
            {
                throw new ArgumentOutOfRangeException($"Dimensionality of vector must be at least 1. Received: {n}.");
            }

            if (typeof(T) == typeof(double))
            {
                return new T[n];
            }

            T e = GetAdditiveIdentityOfType(typeof(T));
            T[] vect = new T[n];
            for (int i = 0; i < n; ++i)
            {
                vect[i] = e;
            }
            return vect;
        }

        /// <summary>
        /// Initialises a matrix with specified values, rows and columns.
        /// The number of values must be >= rows * columns. 
        /// 
        /// The first (rows * columns) entries will be used to construct the 
        /// matrix by filling the rows, one at a time. 
        /// 
        /// Any extra values will be ignored.
        /// </summary>
        /// <param name="rows">The number of rows in the matrix</param>
        /// <param name="cols"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static T[,] Create<T>(int rows, int cols, params T[] values)
        {
            if (values == null)
            {
                throw new ArgumentNullException();
            }
            if (rows * cols != values.Length)
            {
                throw new InvalidOperationException();
            }

            T[,] matrix = new T[rows, cols];

            int r, c, i = 0;
            for (r = 0; r < rows; r++)
            {
                for (c = 0; c < cols; c++)
                {
                    matrix[r, c] = values[i++];
                }
            }
            return matrix;
        }


        #region Random matrix creation methods 

        /// <summary>
        /// Create a random dense <txt>rows</txt> $\times$ <txt>columns</txt> matrix. See <a href="#Random"><txt><b>DenseMatrix.Random&lt;T&gt;</b></txt></a>.
        /// </summary>
        /// <name>Random_Rectangular</name>
        /// <proto>T[,] RectangularMatrix.Random<T>(int rows, int columns, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public static T[,] Random<T>(int rows, int columns, Func<T> random)
        {
            T[,] matrix = new T[rows, columns];
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                for (j = 0; j < columns; ++j)
                {
                    matrix[i, j] = random();
                }
            }
            return matrix;
        }
        /// <summary>
        /// Creates a (m x n) real matrix of type T[,] whose entries are random values.
        /// </summary>
        /// <param name="rows">The number of rows</param>
        /// <param name="columns">The number of columns</param>
        /// <returns>The random matrix.</returns>
        public static T[,] Random<T>(int rows, int columns)
        {
            if (typeof(T) == typeof(double))
            {
                return (dynamic)RandomDouble(rows, columns, -1.0, 1.0);
            }
            if (typeof(T) == typeof(Complex))
            {
                return (dynamic)RandomComplex(rows, columns, 1.0);
            }
            if (typeof(T) == typeof(int))
            {
                return (dynamic)RandomInt(rows, columns, int.MinValue, int.MaxValue);
            }
            if (typeof(T) == typeof(float))
            {
                return (dynamic)RandomFloat(rows, columns, -1.0f, 1.0f);
            }
            if (typeof(T) == typeof(decimal))
            {
                return (dynamic)RandomDecimal(rows, columns, -1.0m, 1.0m);
            }
            if (typeof(T) == typeof(long))
            {
                return (dynamic)RandomLong(rows, columns, long.MinValue, long.MaxValue);
            }
            throw new NotImplementedException();
        }
        internal static int[,] RandomInt(int m, int n, int min = int.MinValue, int max = int.MaxValue)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }
            double range = (double)max - min;
            return Random(m, n, () => (int)(_random.NextDouble() * range) + min);
        }
        /// <summary>
        /// Creates a (m x n) real matrix of type long[,] whose entries are random values in the interval [min, max)
        /// </summary>
        /// <param name="m">The number of rows</param>
        /// <param name="n">The number of columns</param>
        /// <param name="min">The minimum value of each entry in the matrix. Defaults to -9,223,372,036,854,775,808 (smallest value of a 64-bit integer)</param>
        /// <param name="max">The maximum value of each entry in the matrix. Defaults to 9,223,372,036,854,775,807 (largest value of a 64-bit integer)</param>
        /// <returns>The random matrix.</returns>
        internal static long[,] RandomLong(int m, int n, long min = long.MinValue, long max = long.MaxValue)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }
            double range = (double)max - min;
            return Random(m, n, () => (long)(_random.NextDouble() * range) + min);
        }
        /// <summary>
        /// Creates a (m x n) real matrix of type double[,] whose entries are random values in the interval [min, max)
        /// </summary>
        /// <param name="m">The number of rows</param>
        /// <param name="n">The number of columns</param>
        /// <param name="min">The minimum value of each entry in the matrix. Defaults to -0.5</param>
        /// <param name="max">The maximum value of each entry in the matrix. Defaults to 0.5</param>
        /// <returns>The random matrix.</returns>
        internal static double[,] RandomDouble(int m, int n, double min = -1.0, double max = 1.0)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }
            double range = max - min;
            return Random(m, n, () => _random.NextDouble() * range + min);
        }
        /// <summary>
        /// Creates a (m x n) real matrix of type float[,] whose entries are random values in the interval [min, max)
        /// </summary>
        /// <param name="m">The number of rows</param>
        /// <param name="n">The number of columns</param>
        /// <param name="min">The minimum value of each entry in the matrix. Defaults to -0.5</param>
        /// <param name="max">The maximum value of each entry in the matrix. Defaults to 0.5</param>
        /// <returns>The random matrix.</returns>
        internal static float[,] RandomFloat(int m, int n, float min = -1.0f, float max = 1.0f)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }

            float range = max - min;
            return Random(m, n, () => (float)_random.NextDouble() * range + min);
        }
        /// <summary>
        /// Creates a (m x n) real matrix of type decimal[,] whose entries are random values in the interval [min, max)
        /// </summary>
        /// <param name="m">The number of rows</param>
        /// <param name="n">The number of columns</param>
        /// <param name="min">The minimum value of each entry in the matrix. Defaults to -0.5</param>
        /// <param name="max">The maximum value of each entry in the matrix. Defaults to 0.5</param>
        /// <returns>The random matrix.</returns>
        internal static decimal[,] RandomDecimal(int m, int n, decimal min = -1.0m, decimal max = 1.0m)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }

            decimal range = max - min;
            return Random(m, n, () => (decimal)_random.NextDouble() * range + min);
        }
        /// <summary>
        /// Creates a (m x n) matrix whose entries are random complex values with modulus <= 'maxModulus'
        /// </summary>
        /// <param name="m"></param>
        /// <param name="n"></param>
        /// <param name="maxModulus"></param>
        /// <returns></returns>
        internal static Complex[,] RandomComplex(int m, int n, double maxModulus = 1.0)
        {
            Complex[,] matrix = new Complex[m, n];

            int i, j;
            for (i = 0; i < m; i++)
                for (j = 0; j < n; j++)
                    matrix[i, j] = ComplexMethodsExtensions.Random(_random, maxModulus);

            return matrix;
        }

        #endregion


        /// <summary>
        /// Returns a random positive-definite matrix A, with the property that (xT)Ax > 0 for any 
        /// real-valued vector x of compatible dimension. 
        /// 
        /// Generated by sampling a random square matrix (dim x dim) in [-0.5, 0.5)^(dim^2) then 
        /// multiplying it by its own transpose. 
        /// The generated matrix will be square.
        /// </summary>
        /// <param name="dim">The dimensions of the matrix (width/height).</param>
        /// <returns>A randomly generated positive-definite matrix.</returns>
        public static double[,] RandomPositiveDefinite(int dim)
        {
            double[,] random = RandomDouble(dim, dim);
            return random.Multiply(random.Transpose());
        }

        /// <summary>
        /// Returns a random negative-definite matrix A, with the property that (xT)Ax < 0 for any 
        /// real-valued vector x of compatible dimension.
        /// 
        /// Generated by sampling a random square matrix (dim x dim) in [-0.5, 0.5)^(dim^2) then 
        /// multiplying it by its own transpose, then multiplying by the scalar -1.
        /// The generated matrix will be square.
        /// </summary>
        /// <param name="dim">The dimensions of the matrix (width/height).</param>
        /// <returns>A randomly generated negative definite matrix.</returns>
        public static double[,] RandomNegativeDefinite(int dim)
        {
            return RandomPositiveDefinite(dim).Multiply(-1);
        }

        /// <summary>
        /// Generate a random real symmetric matrix with entries in the range [min, max).
        /// Generated matrix will be a square.
        /// </summary>
        /// <param name="dim">Dimension of the matrix</param>
        /// <param name="min">The minimum value of each entry in the matrix</param>
        /// <param name="max">The maximum value of each entry in the matrix</param>
        /// <returns>A random square symmetric matrix</returns>
        public static double[,] RandomSymmetric(int dim, double min = -0.5, double max = 0.5)
        {
            double[,] matrix = RandomDouble(dim, dim, min, max);
            int r, c;
            for (r = 0; r < dim; ++r)
            {
                for (c = 0; c < r; ++c)
                {
                    matrix[r, c] = matrix[c, r];
                }
            }
            return matrix;
        }

        /// <summary>
        /// Generate a random real skew-symmetric matrix with entries in the range [min, max).
        /// Skew-symmetric matrices have the property that A(i, j) = -A(i, j) for i != j.
        /// Generated matrix will be a square.
        /// </summary>
        /// <param name="dim">Dimension of the matrix</param>
        /// <param name="min">The minimum value of each entry in the matrix</param>
        /// <param name="max">The maximum value of each entry in the matrix</param>
        /// <returns>A random real-valued square skew-symmetric matrix</returns>
        public static double[,] RandomSkewSymmetric(int dim, double min = -0.5, double max = 0.5)
        {
            double[,] matrix = RandomDouble(dim, dim, min, max);
            int r, c;
            for (r = 0; r < dim; ++r)
            {
                for (c = 0; c < r; ++c)
                {
                    matrix[r, c] = -matrix[c, r];
                }
            }
            return matrix;
        }


        private static T[,] RandomHessenberg<T>(int dim, bool upper, Func<T> Random)
        {
            T[,] matrix = new T[dim, dim];
            int i, j;
            for (i = 0; i < dim; ++i)
            {
                if (upper)
                {
                    for (j = Math.Max(0, i - 1); j < dim; ++j)
                    {
                        matrix[i, j] = Random();
                    }
                }
                else
                {
                    int last = Math.Min(i + 1, dim);
                    for (j = 0; j < last; ++j)
                    {
                        matrix[i, j] = Random();
                    }
                }
            }
            return matrix;
        }
        public static T[,] RandomHessenberg<T>(int dim, bool upper = true)
        {
            if (typeof(T) == typeof(double))
            {
                return (dynamic)RandomHessenberg(dim, -0.5, 0.5, upper);
            }
            if (typeof(T) == typeof(float))
            {
                return (dynamic)RandomHessenberg(dim, -0.5f, 0.5f, upper);
            }
            if (typeof(T) == typeof(decimal))
            {
                return (dynamic)RandomHessenberg(dim, -0.5m, 0.5m, upper);
            }
            if (typeof(T) == typeof(Complex))
            {
                return (dynamic)RandomHessenberg(dim, 0.5, upper);
            }
            if (typeof(T) == typeof(int))
            {
                return (dynamic)RandomHessenberg(dim, int.MinValue, int.MaxValue, upper);
            }
            if (typeof(T) == typeof(long))
            {
                return (dynamic)RandomHessenberg(dim, long.MinValue, long.MaxValue, upper);
            }
            throw new NotImplementedException();
        }
        public static double[,] RandomHessenberg(int dim, double min = -0.5, double max = 0.5, bool upper = true)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }

            double range = max - min;
            return RandomHessenberg(dim, upper, () => _random.NextDouble() * range + min);
        }
        public static float[,] RandomHessenberg(int dim, float min = -0.5f, float max = 0.5f, bool upper = true)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }

            float range = max - min;
            return RandomHessenberg(dim, upper, () => (float)_random.NextDouble() * range + min);
        }
        public static decimal[,] RandomHessenberg(int dim, decimal min = -0.5m, decimal max = 0.5m, bool upper = true)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }

            decimal range = max - min;
            return RandomHessenberg(dim, upper, () => (decimal)_random.NextDouble() * range + min);
        }
        public static Complex[,] RandomHessenberg(int dim, double maxModulus = 1, bool upper = true)
        {
            return RandomHessenberg(dim, upper, () => ComplexMethodsExtensions.Random(_random, maxModulus));
        }
        public static int[,] RandomHessenberg(int dim, int min = int.MinValue, int max = int.MaxValue, bool upper = true)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }

            double range = (double)max - min;
            return RandomHessenberg(dim, upper, () => (int)(_random.NextDouble() * range) + min);
        }
        public static long[,] RandomHessenberg(int dim, long min = long.MinValue, long max = long.MaxValue, bool upper = true)
        {
            if (!(max > min))
            {
                throw new ArgumentException($"Maximum value of each entry must be greater than the minimum value of each entry. Provided interval is: [{min}, {max})");
            }

            double range = (double)max - min;
            return RandomHessenberg(dim, upper, () => (long)(_random.NextDouble() * range) + min);
        }
    }
}
