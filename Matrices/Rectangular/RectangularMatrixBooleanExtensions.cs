﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class RectangularMatrixBooleanExtensions
    {
        public static bool IsSquare<T>(this T[,] A)
        {
            return A.GetLength(0) == A.GetLength(1);
        }

        #region Is identity
        public static bool IsIdentity(this int[,] A) => IsIdentity(A, a => a == 0, a => a == 1);
        public static bool IsIdentity(this long[,] A) => IsIdentity(A, a => a == 0L, a => a == 1L);
        public static bool IsIdentity(this float[,] A, float tolerance = Precision.FLOAT_PRECISION) => IsIdentity(A, a => a.approx_zero(tolerance), a => a.approx_one(tolerance));
        public static bool IsIdentity(this double[,] A, double tolerance = Precision.DOUBLE_PRECISION) => IsIdentity(A, a => a.approx_zero(tolerance), a => a.approx_one(tolerance));
        public static bool IsIdentity(this decimal[,] A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsIdentity(A, a => a.approx_zero(tolerance), a => a.approx_one(tolerance));
        public static bool IsIdentity<T>(this T[,] A, double tolerance = Precision.DOUBLE_PRECISION) where T : Ring<T>, new()
        {
            T e = new T();
            T zero = e.AdditiveIdentity, one = e.MultiplicativeIdentity;
            return IsIdentity(A, a => a.ApproximatelyEquals(zero, tolerance), a => a.ApproximatelyEquals(one, tolerance));
        }
        private static bool IsIdentity<T>(this T[,] A, Func<T, bool> IsZero, Func<T, bool> IsOne)
        {
            if (A == null) return false;

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            if (m != n) return false;

            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < m; ++j)
                {
                    if (i == j)
                    {
                        if (!IsOne(A[i, i])) return false;
                    }
                    else
                    {
                        if (!IsZero(A[i, j])) return false;
                    }
                }
            }
            return true;
        }
        #endregion

        #region Is diagonal 
        public static bool IsDiagonal(this int[,] A) => IsDiagonal(A, a => a == 0);
        public static bool IsDiagonal(this long[,] A) => IsDiagonal(A, a => a == 0L);
        public static bool IsDiagonal(this float[,] A, float tolerance = Precision.FLOAT_PRECISION) => IsDiagonal(A, a => a.approx_zero(tolerance));
        /// <summary>
        /// Returns whetherr the matrix A is a diagonal matrix
        /// 
        /// Specifically, returns true if all off-diagonal elements of A have magnitude < 'tolerance'
        /// </summary>
        /// <param name="A"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsDiagonal(this double[,] A, double tolerance = Precision.DOUBLE_PRECISION) => IsDiagonal(A, a => a.approx_zero(tolerance));
        public static bool IsDiagonal(this decimal[,] A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsDiagonal(A, a => a.approx_zero(tolerance));
        /// <summary>
        /// Returns true if matrix A is diagonal, false otherwise
        /// 
        /// Specifically, returns true if all off-diagonal elements of A have a modulus < 'tolerance'
        /// </summary>
        /// <param name="A"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsDiagonal<T>(this T[,] A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = A[0, 0].AdditiveIdentity;
            return IsDiagonal(A, a => a.ApproximatelyEquals(zero, tolerance));
        }
        private static bool IsDiagonal<T>(this T[,] A, Func<T, bool> ApproximatelyZero)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    if (i != j && !ApproximatelyZero(A[i, j])) return false;
                }
            }
            return true;
        }
        #endregion

        /// <summary>
        /// Returns whether a matrix is positive definite, within tolerance of 'tolerance' (default = 0).
        /// The higher the tolerance, 
        /// - the lower the probability of false negatives (i.e. matrix is positive definite but method returns 'false' 
        /// due to numerical errors)
        /// - the higher the probability of false positives (i.e. matrix is not positive definite but method returns 'true'
        /// due to numerical errors).
        /// </summary>
        /// <param name="A">A non-degenerate, real-valued matrix</param>
        /// <param name="tolerance">A scalar (defaults to 0)</param>
        /// <returns>Whether the matrix is positive definite, subject to some tolerance level for numerical instability.</returns>
        public static bool IsPositiveDefinite(this double[,] A, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (!A.IsSymmetric()) return false;

            double[,] re = A.ToRowEchelonForm();

            int dim = A.GetLength(0);
            for (int i = 0; i < dim; ++i)
            {
                if (re[i, i] <= -tolerance)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsNegativeDefinite(this double[,] A, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (!A.IsSymmetric()) return false;

            double[,] re = A.ToRowEchelonForm();

            int dim = A.GetLength(0);
            for (int i = 0; i < dim; ++i)
            {
                if (re[i, i] >= -tolerance)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Returns whether a square matrix is symmetric within a tolerance of 'tolerance'
        /// The equality a(i, j) = a(j, i) holds if:
        /// - tolerance > |a(i, j)|                 if a(j, i) = 0
        /// - tolerance > |a(i, j) / a(j, i) - 1|   otherwise.
        /// 
        /// The default tolerance value is 1e-8
        /// </summary>
        /// <param name="A"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsSymmetric(this double[,] A, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (A == null || A.GetLength(0) != A.GetLength(1))
            {
                return false;
            }

            int dim = A.GetLength(0), i, j;
            for (i = 0; i < dim; ++i)
                for (j = 0; j < i; ++j)
                    if (!Util.ApproximatelyEquals(A[i, j], A[j, i], tolerance)) return false;

            return true;
        }
        public static bool IsSymmetric<T>(this T[,] A, double tolerance = Precision.DOUBLE_PRECISION) where T : Group<T>, new()
        {
            if (A == null || A.GetLength(0) != A.GetLength(1))
            {
                return false;
            }

            int dim = A.GetLength(0), i, j;
            for (i = 0; i < dim; ++i)
                for (j = 0; j < i; ++j)
                    if (!A[i, j].ApproximatelyEquals(A[j, i], tolerance)) return false;

            return true;
        }

        /// <summary>
        /// Determines whether complex matrix a is hermitian, subject to a tolerance level of 'tolerance'
        /// </summary>
        /// <param name="a"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsHermitian(this Complex[,] a, double tolerance = Precision.DOUBLE_PRECISION)
        {
            MatrixChecks.CheckNotNull(a);

            int rows = a.GetLength(0), cols = a.GetLength(1), r, c;

            if (rows != cols)
            {
                return false;
            }

            for (r = 0; r < rows; ++r)
            {
                for (c = 0; c < r; ++c)
                {
                    if (!a[r, c].IsConjugateOf(a[c, r], tolerance))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Determines whether a complex matrix is skew-hermitian, where A(i, j) = -conjugate(A(j, i))
        /// </summary>
        /// <param name="a"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsSkewHermitian(this Complex[,] a, double tolerance = Precision.DOUBLE_PRECISION)
        {
            MatrixChecks.CheckNotNull(a);

            int rows = a.GetLength(0), cols = a.GetLength(1), r, c;

            if (rows != cols)
            {
                return false;
            }

            for (r = 0; r < rows; ++r)
            {
                for (c = 0; c < r; ++c)
                {
                    if (!Util.ApproximatelyEquals(a[r, c].Real, -(a[c, r].Real), tolerance)) return false;
                    if (!Util.ApproximatelyEquals(a[r, c].Imaginary, a[c, r].Imaginary, tolerance)) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Determines whether a square matrix is normal (up to a tolerance level), i.e. A*A = AA* where A* is the 
        /// conjugate transpose of A.
        /// 
        /// The method has a default tolerance of 1e-8 (i.e. if |Re(A*A)(i, j) - Re(AA*)(i, j)| < 1e-8 and 
        /// |Im(A*A)(i, j) - Im(AA*)(i, j)| < 1e-8 then the entries are considered the same.
        /// 
        /// If a non-square or degenerate matrix is provided, the method will return null.
        /// 
        /// TODO: find a better verification method than computing the matrices
        /// </summary>
        /// <param name="a"></param>
        /// <param name="tolerance"></param>
        /// <returns>true if the matrix a is normal</returns>
        public static bool IsNormal(this Complex[,] a, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (a == null || a.GetLength(0) != a.GetLength(1))
            {
                return false;
            }

            int dim = a.GetLength(0), i, j, k;
            Complex[,] aConj = a.ConjugateTranspose();

            for (i = 0; i < dim; i++)
            {
                for (j = 0; j < dim; j++)
                {
                    Complex leftSum = Complex.Zero, rightSum = Complex.Zero;
                    for (k = 0; k < dim; k++)
                    {
                        leftSum += aConj[i, k] * a[k, j];
                        rightSum += a[i, k] * aConj[k, j];
                    }

                    if (!leftSum.ApproximatelyEquals(rightSum, tolerance))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Determines whether the two real matrices A and B commute.
        /// </summary>
        /// <param name="A">A real square matrix A</param>
        /// <param name="B">A real square matrix B with the same dimensions as A</param>
        /// <returns>True if A and B commute, i.e. AB = BA</returns>
        public static bool CommutesWith(this double[,] A, double[,] B)
        {
            if (A == null || B == null) return false;

            // Check compatible dimensions - a and b must be square and of the same dimensions
            int arow = A.GetLength(0), acol = A.GetLength(1), brow = B.GetLength(0), bcol = B.GetLength(1);
            if (arow != acol || brow != bcol || arow != brow) return false;

            int i, j, k;
            for (i = 0; i < arow; ++i)
            {
                for (j = 0; j < acol; ++j)
                {
                    double ab = 0.0, ba = 0.0;
                    for (k = 0; k < bcol; ++k)
                    {
                        // Calculate (AB)[i, j] = sum(A[i, k] * B[k, j], 0 <= k < m)
                        ab += A[i, k] * B[k, j];

                        // Calculate (BA)[i, j] = sum(B[i, k] * A[k, j], 0 <= k < m)
                        ba += B[i, k] * A[k, j];
                    }

                    if (!Util.ApproximatelyEquals(ab, ba)) return false;
                }
            }
            return true;
        }
        /// <summary>
        /// Determines whether the two complex matrices A and B commute.
        /// </summary>
        /// <param name="A">A square complex matrix A</param>
        /// <param name="B">A square complex matrix B with the same dimensions as A</param>
        /// <returns>True if A and B commute, i.e. AB = BA</returns>
        public static bool CommutesWith<T>(this T[,] A, T[,] B) where T : Ring<T>, new()
        {
            if (A == null || B == null) return false;

            // Check compatible dimensions - a and b must be square and of the same dimensions
            int arow = A.GetLength(0), acol = A.GetLength(1), brow = B.GetLength(0), bcol = B.GetLength(1);
            if (arow != acol || brow != bcol || arow != brow) return false;

            T e = A[0, 0];
            int i, j, k;
            for (i = 0; i < arow; ++i)
            {
                for (j = 0; j < acol; ++j)
                {
                    T ab = e.AdditiveIdentity, ba = e.AdditiveIdentity;
                    for (k = 0; k < bcol; ++k)
                    {
                        // Calculate (AB)[i, j] = sum(A[i, k] * B[k, j], 0 <= k < m)
                        ab = ab.Add(A[i, k].Multiply(B[k, j]));

                        // Calculate (BA)[i, j] = sum(B[i, k] * A[k, j], 0 <= k < m)
                        ba = ba.Add(B[i, k].Multiply(A[k, j]));
                    }

                    if (!ab.ApproximatelyEquals(ba, Precision.DOUBLE_PRECISION)) return false;
                }
            }
            return true;
        }

        private static bool IsUpperTriangular<T>(this T[,] A, Func<T, bool> ApproximatelyZero)
        {
            int m = A.GetLength(0), n = A.GetLength(1), i, j;

            if (m != n)
            {
                return false;
            }

            for (i = 1; i < m; ++i)
            {
                for (j = 0; j < i; ++j)
                {
                    if (!ApproximatelyZero(A[i, j])) return false;
                }
            }
            return true;
        }
        public static bool IsUpperTriangular(this int[,] A) => IsUpperTriangular(A, e => e == 0);
        public static bool IsUpperTriangular(this long[,] A) => IsUpperTriangular(A, e => e == 0L);
        public static bool IsUpperTriangular(this float[,] A, float tolerance = Precision.FLOAT_PRECISION) => IsUpperTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsUpperTriangular(this double[,] A, double tolerance = Precision.DOUBLE_PRECISION) => IsUpperTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsUpperTriangular(this decimal[,] A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsUpperTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsUpperTriangular<T>(this T[,] A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = A[0, 0].AdditiveIdentity;
            return IsUpperTriangular(A, e => e.ApproximatelyEquals(zero, tolerance));
        }

        private static bool IsLowerTriangular<T>(this T[,] A, Func<T, bool> ApproximatelyZero)
        {
            int m = A.GetLength(0), n = A.GetLength(1), i, j;

            if (m != n)
            {
                return false;
            }

            for (i = 0; i < m; ++i)
            {
                for (j = i + 1; j < m; ++j)
                {
                    if (!ApproximatelyZero(A[i, j])) return false;
                }
            }
            return true;
        }
        public static bool IsLowerTriangular(this int[,] A) => IsLowerTriangular(A, e => e == 0);
        public static bool IsLowerTriangular(this long[,] A) => IsLowerTriangular(A, e => e == 0L);
        public static bool IsLowerTriangular(this float[,] A, float tolerance = Precision.FLOAT_PRECISION) => IsLowerTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsLowerTriangular(this double[,] A, double tolerance = Precision.DOUBLE_PRECISION) => IsLowerTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsLowerTriangular(this decimal[,] A, decimal tolerance = Precision.DECIMAL_PRECISION) => IsLowerTriangular(A, e => e.approx_zero(tolerance));
        public static bool IsLowerTriangular<T>(this T[,] A, double tolerance = Precision.DOUBLE_PRECISION) where T : AdditiveGroup<T>, new()
        {
            T zero = A[0, 0].AdditiveIdentity;
            return IsLowerTriangular(A, e => e.ApproximatelyEquals(zero, tolerance));
        }


        /// <summary>
        /// Returns whether a matrix is approximately upper bidiagonal, up to precision 'tolerance'
        /// A matrix is upper-bidiagonal if the only non-zero elements lie on the main diagonal, or the upper
        /// main diagonal. 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool IsUpperBidiagonal(this double[,] A, double tolerance = Precision.DOUBLE_PRECISION)
        {
            int m = A.GetLength(0), n = A.GetLength(1), i, j;

            for (i = 1; i < m; ++i)
            {
                for (j = 0; j < i; ++j)
                {
                    if (!Util.ApproximatelyEquals(A[i, j], 0, tolerance)) return false;
                }
                for (j = i + 2; j < n; ++j)
                {
                    if (!Util.ApproximatelyEquals(A[i, j], 0, tolerance)) return false;
                }
            }
            return true;
        }

        private static bool ExactlyEquals<T>(this T[,] A, T[,] B, Func<T, T, bool> Equals)
        {
            if (A == null || B == null) return false;
            if (A.GetLength(0) != B.GetLength(0) || A.GetLength(1) != B.GetLength(1)) return false;

            int m = A.GetLength(0), n = B.GetLength(1), i, j;
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    if (!Equals(A[i, j], B[i, j])) return false;
                }
            }
            return true;
        }
        public static bool ExactlyEquals(this int[,] A, int[,] B) => ExactlyEquals(A, B, (a, b) => a == b);
        public static bool ExactlyEquals(this long[,] A, long[,] B) => ExactlyEquals(A, B, (a, b) => a == b);
        public static bool ExactlyEquals<T>(this T[,] A, T[,] B) where T : Algebra<T>, new() => ExactlyEquals(A, B, (a, b) => a.Equals(b));

        private static bool ApproximatelyEquals<T>(this T[,] A, T[,] B, T tolerance, Func<T, T, T, bool> ApproximatelyEqual)
        {
            if (A == null || B == null) return false;
            if (A.GetLength(0) != B.GetLength(0) || A.GetLength(1) != B.GetLength(1)) return false;

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    if (!ApproximatelyEqual(A[i, j], B[i, j], tolerance))
                    {
                        Debug.WriteLine(A[i, j] + " != " + B[i, j]);
                        return false;
                    }
                }
            }
            return true;
        }
        /// <summary>
        /// Returns whether 2 matrices are the same, subject to some tolerance level 'tolerance'
        /// </summary>
        /// <param name="matrix1"></param>
        /// <param name="matrix2"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool ApproximatelyEquals(this double[,] matrix1, double[,] matrix2, double tolerance = Precision.DOUBLE_PRECISION)
        {
            return ApproximatelyEquals(matrix1, matrix2, tolerance, (a, b, eps) => Util.ApproximatelyEquals(a, b, eps));
        }
        public static bool ApproximatelyEquals(this float[,] A, float[,] B, float tolerance = Precision.FLOAT_PRECISION)
        {
            return ApproximatelyEquals(A, B, tolerance, (a, b, eps) => Util.ApproximatelyEquals(a, b, eps));
        }
        public static bool ApproximatelyEquals(this decimal[,] A, decimal[,] B, decimal tolerance = Precision.DECIMAL_PRECISION)
        {
            return ApproximatelyEquals(A, B, tolerance, (a, b, eps) => Util.ApproximatelyEquals(a, b, eps));
        }
        public static bool ApproximatelyEquals<T>(this T[,] matrix1, T[,] matrix2, double tolerance = Precision.DOUBLE_PRECISION) where T : Algebra<T>, new()
        {
            /// this method must be implemented independently since it does not following the same equality structure as others.
            /// 

            if (matrix1 == null || matrix2 == null) return false;
            if (matrix1.GetLength(0) != matrix2.GetLength(0) || matrix1.GetLength(1) != matrix2.GetLength(1)) return false;

            int m = matrix1.GetLength(0), n = matrix2.GetLength(1), i, j;
            for (i = 0; i < m; ++i)
                for (j = 0; j < n; ++j)
                    if (!matrix1[i, j].ApproximatelyEquals(matrix2[i, j], tolerance))
                        return false;

            return true;
        }
    }
}
