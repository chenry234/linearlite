﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class RectangularMatrixCommonExtensions
    {
        public static T[] LeadingDiagonal<T>(this T[,] a)
        {
            MatrixChecks.CheckNotNull(a);

            int n = Math.Min(a.GetLength(0), a.GetLength(1)), i;

            T[] diagonal = new T[n];
            for (i = 0; i < n; i++)
            {
                diagonal[i] = a[i, i];
            }
            return diagonal;
        }

        /// <summary>
        /// Calculates the matrix transpose for a real matrix a, aT. The original matrix is unchanged.
        /// </summary>
        /// <param name="a"></param>
        /// <returns>The matrix transpose</returns>
        public static T[,] Transpose<T>(this T[,] a)
        {
            MatrixChecks.CheckNotNull(a);

            int rows = a.GetLength(0), cols = a.GetLength(1), r, c;
            T[,] transpose = new T[cols, rows];
            for (r = 0; r < rows; r++)
            {
                for (c = 0; c < cols; c++)
                {
                    transpose[c, r] = a[r, c];
                }
            }
            return transpose;
        }
        public static T[,] ParallelTranspose<T>(this T[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.GetLength(0), cols = A.GetLength(1);
            T[,] transpose = new T[cols, rows];

            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int r, c, min = range.Item1, max = range.Item2;
                for (r = min; r < max; r++)
                {
                    for (c = 0; c < cols; c++)
                    {
                        transpose[c, r] = A[r, c];
                    }
                }
            });

            return transpose;
        }


        /// <summary>
        /// Returns the column at index 'columnIndex' for a matrix a. Note that 'columnIndex' is 0-indexed.
        /// </summary>
        /// <param name="A">A non-degenerate matrix with at least ('columnIndex' + 1) columns.</param>
        /// <param name="columnIndex">A non-negative index of the column to get. 0-indexed.</param>
        /// <returns>The 'columnIndex'-th column ('columnIndex' is 0-indexed)</returns>
        public static T[] Column<T>(this T[,] A, int columnIndex)
        {
            MatrixChecks.CheckNotNull(A);

            if (A.GetLength(1) <= columnIndex)
            {
                throw new ArgumentOutOfRangeException($"Column index out of bounds. Matrix width = {A.GetLength(1)}, columnIndex = {columnIndex}.");
            }

            int n = A.GetLength(0);

            T[] col = new T[n];
            for (int i = 0; i < n; i++)
            {
                col[i] = A[i, columnIndex];
            }
            return col;
        }
        public static void Column<T>(this T[,] A, int columnIndex, T[] column)
        {
            MatrixChecks.CheckNotNull(A);

            if (A.GetLength(1) <= columnIndex)
            {
                throw new ArgumentOutOfRangeException($"Column index out of bounds. Matrix width = {A.GetLength(1)}, columnIndex = {columnIndex}.");
            }
            if (A.GetLength(0) != column.Length)
            {
                throw new ArgumentOutOfRangeException($"Matrix rows does not match column vector length. {A.GetLength(0)} vs {column.Length}");
            }

            int n = column.Length, i;
            for (i = 0; i < n; i++)
            {
                column[i] = A[i, columnIndex];
            }
        }

        /// <summary>
        /// Returns the row at index 'rowIndex'
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        public static T[] Row<T>(this T[,] A, int rowIndex)
        {
            MatrixChecks.CheckNotNull(A);

            T[] row = new T[A.GetLength(1)];
            Row(A, rowIndex, row);
            return row;
        }
        public static void Row<T>(this T[,] A, int rowIndex, T[] row)
        {
            MatrixChecks.CheckNotNull(A);
            if (row == null)
            {
                throw new ArgumentNullException();
            }
            if (rowIndex < 0 || rowIndex >= A.GetLength(0))
            {
                throw new ArgumentOutOfRangeException($"Row index out of bounds. Matrix height = {A.GetLength(0)}, rowIndex = {rowIndex}.");
            }
            if (A.GetLength(1) != row.Length)
            {
                throw new ArgumentOutOfRangeException();
            }

            int len = row.Length, i;
            for (i = 0; i < len; i++)
            {
                row[i] = A[rowIndex, i];
            }
        }


        internal static void swap_rows<T>(this T[,] A, int r1, int r2)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1), i;
            if (r1 < 0 || r2 < 0 || r1 >= m || r2 >= m) throw new ArgumentOutOfRangeException();

            for (i = 0; i < n; ++i)
            {
                T temp = A[r1, i];
                A[r1, i] = A[r2, i];
                A[r2, i] = temp;
            }
        }
        internal static void swap_columns<T>(this T[,] A, int c1, int c2)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1), i;
            if (c1 < 0 || c2 < 0 || c1 >= n || c2 >= n) throw new ArgumentOutOfRangeException();

            for (i = 0; i < m; ++i)
            {
                T temp = A[i, c1];
                A[i, c1] = A[i, c2];
                A[i, c2] = temp;
            }
        }
        /// <summary>
        /// Swaps two rows of a matrix. The original matrix is unchanged.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="row1"></param>
        /// <param name="row2"></param>
        /// <returns></returns>
        public static T[,] SwapRows<T>(this T[,] A, int row1, int row2)
        {
            T[,] copy = A.Copy();
            copy.swap_rows(row1, row2);
            return copy;
        }
        /// <summary>
        /// Swaps two columns of a matrix. The original matrix is unchanged.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="column1"></param>
        /// <param name="column2"></param>
        /// <returns></returns>
        public static T[,] SwapColumns<T>(this T[,] A, int column1, int column2)
        {
            T[,] copy = A.Copy();
            copy.swap_columns(column1, column2);
            return copy;
        }

        /// <summary>
        /// Returns a submatrix formed by taking from matrix a the rows [firstRow, firstRow + rows) and columns 
        /// [firstCol, firstCol + cols).
        /// Returns matrix will be of dimension (rows x cols)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a"></param>
        /// <param name="firstRow"></param>
        /// <param name="firstCol"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        /// <returns>Submatrix of size (rows, cols)</returns>
        public static T[,] Submatrix<T>(this T[,] a, int firstRow, int firstCol, int rows, int cols)
        {
            if (a == null)
            {
                throw new ArgumentNullException("Matrix is null or empty.");
            }
            if (a.GetLength(0) < firstRow + rows || a.GetLength(1) < firstCol + cols)
            {
                throw new ArgumentOutOfRangeException("Submatrix refers to a region outside bounds of the matrix.");
            }

            T[,] sub = new T[rows, cols];
            int r, c;
            for (r = 0; r < rows; r++)
            {
                for (c = 0; c < cols; c++)
                {
                    sub[r, c] = a[firstRow + r, firstCol + c];
                }
            }
            return sub;
        }


        /// <summary>
        /// Copy a (rows, cols) block from source to destination matrix, starting with (firstRow, firstCol) in the 
        /// upper left corner.
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="src"></param>
        /// <param name="srcFirstRow"></param>
        /// <param name="srcFirstCol"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        public static void CopyFrom<T>(this T[,] dest, T[,] src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol, int rows, int cols)
        {
            MatrixChecks.CheckNotNull(dest);
            MatrixChecks.CheckNotNull(src);

            if (srcFirstRow < 0 || srcFirstCol < 0 || rows < 0 || cols < 0 || destFirstRow < 0 || destFirstCol < 0)
            {
                throw new ArgumentOutOfRangeException("Index is less than 0.");
            }
            if (srcFirstRow + rows > src.GetLength(0) || srcFirstCol + cols > src.GetLength(1))
            {
                throw new ArgumentOutOfRangeException("Index refers to a location outside the source matrix.");
            }
            if (destFirstRow + rows > dest.GetLength(0) || destFirstCol + cols > dest.GetLength(1))
            {
                throw new ArgumentOutOfRangeException("Index refers to a location outside the destination matrix.");
            }

            int r, c;
            for (r = 0; r < rows; ++r)
            {
                int dest_row = destFirstRow + r, src_row = srcFirstRow + r;
                for (c = 0; c < cols; ++c)
                {
                    dest[dest_row, destFirstCol + c] = src[src_row, srcFirstCol + c];
                }
            }
        }
        public static void CopyFrom<T>(this T[,] dest, T[,] src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol)
        {
            CopyFrom(dest, src, srcFirstRow, srcFirstCol, destFirstRow, destFirstCol, src.GetLength(0), src.GetLength(1));
        }

        public static T[,] Copy<T>(this T[,] a)
        {
            return a.Clone() as T[,];
        }
    }
}
