﻿using LinearLite.Decomposition;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    /// <summary>
    /// Extension functions for calculating real and complex matrix exponentials
    /// </summary>
    public static class RectangularMatrixExponentialExtensions
    {
        /// <summary>
        /// Evaluate and returns Exp(A)
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static Complex[,] Exp(this double[,] A)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            A.EigenDecompose(out Complex[,] D, out Complex[,] V);

            int n = A.GetLength(0), i;
            for (i = 0; i < n; ++i)
            {
                D[i, i] = Complex.Exp(D[i, i]);
            }


            //return V.Multiply(D).Multiply(V.Invert());
            throw new NotImplementedException();
        }
    }
}
