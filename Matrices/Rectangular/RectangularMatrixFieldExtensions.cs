﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class RectangularMatrixFieldExtensions
    {

        /// <summary>
        /// Calculates and returns the determinant of a square matrix a. The original matrix is unchanged.
        /// </summary>
        /// <param name="a">A non-degenerate, real square matrix.</param>
        /// <returns>The determinant of the provided matrix.</returns>
        public static double Determinant(this double[,] a)
        {
            MatrixChecks.CheckNotNull(a);
            MatrixChecks.CheckIsSquare(a);

            double[,] copy = a.ToRowEchelonForm();

            int dim = a.GetLength(0), c;

            // Take product of the leading diagonal
            double det = 1;
            for (c = 0; c < dim; c++)
            {
                det *= copy[c, c];
            }
            return det;
        }
        public static T Determinant<T>(this T[,] a) where T : Field<T>, new()
        {
            MatrixChecks.CheckNotNull(a);
            MatrixChecks.CheckIsSquare(a);

            T[,] copy = a.ToRowEchelonForm();

            int dim = a.GetLength(0), c;

            // Take product of the leading diagonal
            T det = a[0, 0].AdditiveIdentity;
            for (c = 0; c < dim; c++)
            {
                det = det.Multiply(copy[c, c]);
            }
            return det;
        }



        /// <summary>
        /// Returns the rank of a matrix, up to 'tolerance'.
        /// Returns 0 if the matrix is null or degenerate
        /// </summary>
        /// <param name="eps"></param>
        /// <returns></returns>
        public static int Rank(this double[,] A, double tolerance = Precision.DOUBLE_PRECISION)
        {
            if (A == null || A.Length == 0)
            {
                return 0;
            }

            double[,] re = A.ToRowEchelonForm();

            int rows = re.GetLength(0), cols = re.GetLength(1), n = Math.Min(rows, cols), r, c, rank = 0;
            for (r = 0; r < n; ++r)
            {
                for (c = 0; c < cols; ++c)
                {
                    if (!Util.ApproximatelyEquals(A[r, c], 0, tolerance))
                    {
                        rank++;
                        break;
                    }
                }
            }
            return rank;
        }
        /// <summary>
        /// Returns the rank of a matrix, up to 'tolerance'.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static int Rank<T>(this T[,] A, double tolerance = Precision.DOUBLE_PRECISION) where T : Field<T>, new()
        {
            if (A == null || A.Length == 0)
            {
                return 0;
            }

            T zero = A[0, 0].AdditiveIdentity;
            T[,] re = A.ToRowEchelonForm();

            int rows = re.GetLength(0), cols = re.GetLength(1), n = Math.Min(rows, cols), r, c, rank = 0;
            for (r = 0; r < n; ++r)
            {
                for (c = 0; c < cols; ++c)
                {
                    if (!A[r, c].ApproximatelyEquals(zero, tolerance))
                    {
                        rank++;
                        break;
                    }
                }
            }
            return rank;
        }

    }
}
