﻿using LinearLite.BLAS;
using LinearLite.Global;
using LinearLite.Helpers;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class RectangularMatrixNormExtensions
    {
        /// <summary>
        /// TODO
        /// Calculates the vector-induced p-norm of matrix A
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static double Norm(this double[,] A, int p)
        {
            MatrixChecks.CheckNotNull(A);
            if (p < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (p == 1)
            {
                int m = A.GetLength(0), n = A.GetLength(1), i, j;
                double max = 0;
                for (j = 0; j < n; ++j)
                {
                    double sum = 0;
                    for (i = 0; i < m; ++i)
                    {
                        sum += Math.Abs(A[i, j]);
                    }
                    if (sum > max)
                    {
                        max = sum;
                    }
                }
                return max;
            }

            return CalculateVectorNormIterative(A, p);
        }

        private static double CalculateVectorNormIterative(double[,] A, int p)
        {
            int m = A.GetLength(0), n = A.GetLength(1), i, j, k;

            // Initialise 
            double[] x = new double[n];
            double val = Math.Pow(1.0 / n, 1.0 / p);
            for (i = 0; i < n; ++i)
            {
                x[i] = val;
            }

            double[] gradient = new double[n];
            for (i = 0; i < 100; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    double sum = 0.0;
                    for (k = 0; k < m; ++k)
                    {
                        sum += A[k, j] * x[j];
                    }

                    if (p % 2 == 0)
                    {
                        gradient[j] = Math.Pow(sum, p - 1) * A[k, j];
                    }
                    else
                    {
                        gradient[j] = Math.Sign(sum) * Math.Pow(sum, p - 1) * A[k, j];
                    }
                }

                for (j = 0; j < n; ++j)
                {
                    x[j] += gradient[j];
                }
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Calculates the L(p, q) norm of matrix A.
        /// The Frobenius Norm is a special case of this norm where p = q = 2
        /// </summary>
        /// <param name="A"></param>
        /// <param name="p"></param>
        /// <param name="q"></param>
        /// <returns></returns>
        public static double ElementwiseNorm(this double[,] A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleBLAS(), x => Math.Abs(x), (a, b) => Math.Pow(a, b));
        public static float ElementwiseNorm(this float[,] A, int p, int q) => ElementwiseNorm(A, p, q, new FloatBLAS(), x => Math.Abs(x), (a, b) => (float)Math.Pow(a, b));
        public static decimal ElementwiseNorm(this decimal[,] A, int p, int q) => ElementwiseNorm(A, p, q, new DecimalBLAS(), x => Math.Abs(x), (a, b) => DecimalMath.Pow(a, (decimal)b));
        public static double ElementwiseNorm(this Complex[,] A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleBLAS(), x => x.Modulus(), (a, b) => Math.Pow(a, b));
        private static F ElementwiseNorm<T, F>(this T[,] A, int p, int q, IBLAS<F> blas, Func<T, F> Norm, Func<F, double, F> Pow) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            if (p < 1) throw new ArgumentOutOfRangeException();
            if (q < 1) throw new ArgumentOutOfRangeException();

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            F norm = blas.Zero;

            if (p == 1 && q == 1)
            {
                for (i = 0; i < m; ++i)
                {
                    for (j = 0; j < n; ++j)
                    {
                        norm = blas.Add(norm, Norm(A[i, j]));
                    }
                }
                return norm;
            }
            else
            {
                double p_q = q / (double)p;
                for (j = 0; j < n; ++j)
                {
                    F sum = blas.Zero;
                    for (i = 0; i < m; ++i)
                    {
                        sum = blas.Add(sum, Pow(Norm(A[i, j]), p));
                    }
                    norm = blas.Add(norm, Pow(sum, p_q));
                }
                return Pow(norm, 1.0 / q);
            }
        }


        /// <summary>
        /// Calculates the max elementwise norm of matrix A
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static double ElementwiseMaxNorm(this double[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            double max = 0;
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    double abs = Math.Abs(A[i, j]);
                    if (abs > max)
                    {
                        max = abs;
                    }
                }
            }
            return max;
        }

        public static double ElementwiseL1Norm(this double[,] A) => ElementwiseL1Norm(A, 0.0, a => Math.Abs(a), (a, b) => a + b);
        public static float ElementwiseL1Norm(this float[,] A) => ElementwiseL1Norm(A, 0.0f, a => Math.Abs(a), (a, b) => a + b);
        public static decimal ElementwiseL1Norm(this decimal[,] A) => ElementwiseL1Norm(A, 0.0m, a => Math.Abs(a), (a, b) => a + b);
        private static T ElementwiseL1Norm<T>(this T[,] A, T zero, Func<T, T> Abs, Func<T, T, T> Add)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            T norm = zero;
            for (j = 0; j < n; ++j)
            {
                for (i = 0; i < m; ++i)
                {
                    norm = Add(norm, Abs(A[i, j]));
                }
            }
            return norm;
        }

        /// <summary>
        /// Calculates the Frobenius norm of matrix A, defined as sqrt of 
        /// sum of squares of elements of A.
        /// </summary>
        /// <param name="A"></param>
        /// <returns></returns>
        public static double FrobeniusNorm(this double[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            double norm = 0;
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    double d = A[i, j];
                    norm += d * d;
                }
            }
            return Math.Sqrt(norm);
        }
        public static float FrobeniusNorm(this float[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            float norm = 0.0f;
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    float d = A[i, j];
                    norm += d * d;
                }
            }
            return (float)Math.Sqrt(norm);
        }
        public static decimal FrobeniusNorm(this decimal[,] A)
        {
            MatrixChecks.CheckNotNull(A);

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            decimal norm = 0.0m;
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    decimal d = A[i, j];
                    norm += d * d;
                }
            }
            return DecimalMath.Sqrt(norm);
        }
    }
}
