﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using LinearLite.Matrices;
using LinearLite.Matrices.Strassen;
using LinearLite.Structs;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading.Tasks;

namespace LinearLite
{
    /// <summary>
    /// Implements matrix addition, subtraction, multiplication, transposition, inverse
    /// </summary>
    public static class RectangularMatrixOperationsExtensions
    {
        internal static T[,] ElementwiseOperation<T>(this T[,] A, T[,] B, Func<T, T, T> Operation)
        {
            if (A == null || B == null) throw new ArgumentNullException();
            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            if (m != B.GetLength(0) || n != B.GetLength(1)) throw new InvalidOperationException();

            T[,] result = new T[m, n];
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    result[i, j] = Operation(A[i, j], B[i, j]);
                }
            }
            return result;
        }

        #region Subtraction
        /// <summary>
        /// Subtract one matrix from another. The original matrices are unchanged.
        /// The dimensions of the two matrices must be the same, or ArgumentOutOfRangeException will be thrown.
        /// </summary>
        /// <param name="A">The matrix to subtract from.</param>
        /// <param name="B">The matrix to subtract.</param>
        /// <returns>The difference between the two matrices.</returns>
        public static byte[,] Subtract(this byte[,] A, byte[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            byte[,] diff = new byte[rows, cols];

            for (i = 0; i < rows; ++i)
                for (j = 0; j < cols; ++j)
                    diff[i, j] = (byte)(A[i, j] - B[i, j]);

            return diff;
        }
        public static short[,] Subtract(this short[,] A, short[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            short[,] diff = new short[rows, cols];

            for (i = 0; i < rows; ++i)
                for (j = 0; j < cols; ++j)
                    diff[i, j] = (short)(A[i, j] - B[i, j]);

            return diff;
        }
        public static int[,] Subtract(this int[,] A, int[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            int[,] diff = new int[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    diff[i, j] = A[i, j] - B[i, j];

            return diff;
        }
        public static long[,] Subtract(this long[,] A, long[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            long[,] diff = new long[rows, cols];

            for (i = 0; i < rows; ++i)
                for (j = 0; j < cols; ++j)
                    diff[i, j] = A[i, j] - B[i, j];

            return diff;
        }
        public static float[,] Subtract(this float[,] A, float[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            float[,] diff = new float[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    diff[i, j] = A[i, j] - B[i, j];

            return diff;
        }
        public static double[,] Subtract(this double[,] A, double[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            double[,] diff = new double[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    diff[i, j] = A[i, j] - B[i, j];

            return diff;
        }
        public static decimal[,] Subtract(this decimal[,] A, decimal[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            decimal[,] diff = new decimal[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    diff[i, j] = A[i, j] - B[i, j];

            return diff;
        }
        public static T[,] Subtract<T>(this T[,] A, T[,] B) where T : AdditiveGroup<T>, new()
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            T[,] diff = new T[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    diff[i, j] = A[i, j].Subtract(B[i, j]);

            return diff;
        }
        #endregion

        #region Parallel subtraction
        /// <summary>
        /// Calculates A - B in parallel across multiple threads
        /// The dimensions of A and B must match, otherwise InvalidOperationException will be thrown
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="nThreads">The number of threads to use.</param>
        /// <returns></returns>
        public static double[,] ParallelSubtract(this double[,] A, double[,] B)
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A.GetLength(0) != B.GetLength(0) || A.GetLength(1) != B.GetLength(1)) throw new InvalidOperationException();

            int rows = A.GetLength(0), cols = A.GetLength(1);

            double[,] result = new double[rows, cols];
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int start = range.Item1, end = range.Item2, i;
                for (i = start; i < end; ++i)
                {
                    for (int j = 0; j < cols; ++j)
                    {
                        result[i, j] = A[i, j] - B[i, j];
                    }
                }
            });
            return result;
        }
        public static T[,] ParallelSubtract<T>(this T[,] A, T[,] B) where T : AdditiveGroup<T>, new()
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A.GetLength(0) != B.GetLength(0) || A.GetLength(1) != B.GetLength(1)) throw new InvalidOperationException();

            int rows = A.GetLength(0), cols = A.GetLength(1);

            T[,] result = new T[rows, cols];
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int start = range.Item1, end = range.Item2, i;
                for (i = start; i < end; ++i)
                {
                    for (int j = 0; j < cols; ++j)
                    {
                        result[i, j] = A[i, j].Subtract(B[i, j]);
                    }
                }
            });
            return result;
        }
        #endregion

        #region Addition
        /// <summary>
        /// Add one matrix to another. The original matrices are unchanged.
        /// The dimensions of the two matrices must be the same, or ArgumentOutOfRangeException will be thrown.
        /// </summary>
        /// <param name="A">The first matrix to add.</param>
        /// <param name="B">The second matrix to add.</param>
        /// <returns>The sum of the two matrices.</returns>
        public static byte[,] Add(this byte[,] A, byte[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            byte[,] sum = new byte[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    sum[i, j] = (byte)(A[i, j] + B[i, j]);

            return sum;
        }
        public static short[,] Add(this short[,] A, short[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            short[,] sum = new short[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    sum[i, j] = (short)(A[i, j] + B[i, j]);

            return sum;
        }
        public static int[,] Add(this int[,] A, int[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            int[,] sum = new int[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    sum[i, j] = A[i, j] + B[i, j];

            return sum;
        }
        public static long[,] Add(this long[,] A, long[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            long[,] sum = new long[rows, cols];

            for (i = 0; i < rows; ++i)
                for (j = 0; j < cols; ++j)
                    sum[i, j] = A[i, j] + B[i, j];

            return sum;
        }
        public static float[,] Add(this float[,] A, float[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            float[,] sum = new float[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    sum[i, j] = A[i, j] + B[i, j];

            return sum;
        }
        public static double[,] Add(this double[,] A, double[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            double[,] sum = new double[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    sum[i, j] = A[i, j] + B[i, j];

            return sum;
        }
        public static decimal[,] Add(this decimal[,] A, decimal[,] B)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            decimal[,] sum = new decimal[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    sum[i, j] = A[i, j] + B[i, j];

            return sum;
        }
        public static T[,] Add<T>(this T[,] A, T[,] B) where T : AdditiveGroup<T>, new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            T[,] sum = new T[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    sum[i, j] = A[i, j].Add(B[i, j]);

            return sum;
        }
        internal static T[,] Add<T>(this T[,] A, T[,] B, Func<T, T, T> Add)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int rows = A.GetLength(0), cols = A.GetLength(1), i, j;
            T[,] sum = new T[rows, cols];

            for (i = 0; i < rows; i++)
                for (j = 0; j < cols; j++)
                    sum[i, j] = Add(A[i, j], B[i, j]);

            return sum;
        }
        #endregion

        #region Parallel addition
        /// <summary>
        /// Calculates A + B in parallel across multiple threads
        /// The dimensions of A and B must match, otherwise InvalidOperationException will be thrown
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="nThreads">The number of threads to use</param>
        /// <returns></returns>
        public static double[,] ParallelAdd(this double[,] A, double[,] B)
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A.GetLength(0) != B.GetLength(0) || A.GetLength(1) != B.GetLength(1)) throw new InvalidOperationException();

            int rows = A.GetLength(0), cols = A.GetLength(1);

            double[,] result = new double[rows, cols];
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int start = range.Item1, end = range.Item2, i;
                for (i = start; i < end; ++i)
                {
                    for (int j = 0; j < cols; ++j)
                    {
                        result[i, j] = A[i, j] + B[i, j];
                    }
                }
            });
            return result;
        }
        public static T[,] ParallelAdd<T>(this T[,] A, T[,] B) where T : AdditiveGroup<T>, new()
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A.GetLength(0) != B.GetLength(0) || A.GetLength(1) != B.GetLength(1)) throw new InvalidOperationException();

            int rows = A.GetLength(0), cols = A.GetLength(1);

            T[,] result = new T[rows, cols];
            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int start = range.Item1, end = range.Item2, i;
                for (i = start; i < end; ++i)
                {
                    for (int j = 0; j < cols; ++j)
                    {
                        result[i, j] = A[i, j].Add(B[i, j]);
                    }
                }
            });
            return result;
        }
        #endregion

        #region Naive matrix multiplication
        public static int[,] Multiply(this int[,] A, int[,] B)
        {
            MatrixChecks.CheckNotNull(A, B);
            int[,] result = new int[A.GetLength(0), B.GetLength(1)];
            Multiply(A, B, result);
            return result;
        }
        public static long[,] Multiply(this long[,] A, long[,] B)
        {
            MatrixChecks.CheckNotNull(A, B);
            long[,] result = new long[A.GetLength(0), B.GetLength(1)];
            Multiply(A, B, result);
            return result;
        }
        public static float[,] Multiply(this float[,] A, float[,] B)
        {
            MatrixChecks.CheckNotNull(A, B);
            float[,] result = new float[A.GetLength(0), B.GetLength(1)];
            Multiply(A, B, result);
            return result;
        }
        /// <summary>
        /// Calculates the product AB (m x p), where A (m x n) and B (n x p) are matrices over the reals.
        /// As matrix multiplication is not commutative, the order of multiplication is important. This method 
        /// post-multiplies A by B.
        /// The original matrices are unchanged by this method.
        /// </summary>
        /// <param name="A">The first matrix to multiply.</param>
        /// <param name="B">The second matrix to multiply.</param>
        /// <returns>The product of the two matrices.</returns>
        public static double[,] Multiply(this double[,] A, double[,] B)
        {
            MatrixChecks.CheckNotNull(A, B);
            double[,] result = new double[A.GetLength(0), B.GetLength(1)];
            Multiply(A, B, result);
            return result;
        }
        public static decimal[,] Multiply(this decimal[,] A, decimal[,] B)
        {
            MatrixChecks.CheckNotNull(A, B);
            decimal[,] result = new decimal[A.GetLength(0), B.GetLength(1)];
            Multiply(A, B, result);
            return result;
        }
        /// <summary>
        /// Calculates and returns the product AB, where A (m x n) and B (n x p) are matrices over the complex field.
        /// This method has strictly identical behaviour to the equivalent method for Rings (since Complex implements
        /// the Ring<T> interface), however is about 6-7 times faster for problem sizes > <50, 50, 50>.
        /// The original matrices A and B are unchanged by this method.
        /// </summary>
        /// <param name="A">Complex matrix with dimensions m x n</param>
        /// <param name="B">Complex matrix with dimensions n x p</param>
        /// <returns>The matrix product AB, with dimensions m x p</returns>
        public static Complex[,] Multiply(this Complex[,] A, Complex[,] B)
        {
            MatrixChecks.CheckNotNull(A, B);
            Complex[,] result = new Complex[A.GetLength(0), B.GetLength(1)];
            Multiply(A, B, result);
            return result;
        }
        /// <summary>
        /// Calculates the product AB (m x p), where A (m x n) and B (n x p) are matrices over any ring.
        /// 
        /// This is the most general method for matrix multiplication, however is the least performant. 
        /// Please see the other Multiply() overloads for more specialised matrix multiplication if 
        /// performance is a concern.
        /// 
        /// The original matrices A and B are unchanged by this method.
        /// </summary>
        /// <typeparam name="T">Any struct or class implementing the Ring<T> interface</typeparam>
        /// <param name="A">Matrix with dimensions m x n</param>
        /// <param name="B">Matrix with dimensions n x p</param>
        /// <returns>The matrix product AB, with dimensions m x p</returns>
        public static T[,] Multiply<T>(this T[,] A, T[,] B) where T : Ring<T>, new()
        {
            MatrixChecks.CheckNotNull(A, B);

            T[,] result = new T[A.GetLength(0), B.GetLength(1)];
            Multiply(A, B, result);
            return result;
        }
        /// <summary>
        /// Returns true if we should fall back to naive multiplication method (for small matrices)
        /// Number of operations is O(mnp) so we use this as a guide for the 'size' of the multiplication
        /// 
        /// Returns true if mnp < 50^3, which based on rough experiments is the crossover point where the 
        /// 'naive' method falls behind the jagged matrix access method.
        /// </summary>
        /// <param name="m"></param>
        /// <param name="n"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        private static bool UseNaiveMultiplicationMethod(int m, int n, int p)
        {
            return ((double)m) * n * p < 125000;
        }

        private static void Multiply<T>(this T[,] A, T[,] B, T[,] result, Func<T[,], T[,], int, int, int, T> MatrixProduct, Func<T[], T[], int, T> DotProduct)
        {
            if (A == null || B == null || result == null)
            {
                throw new ArgumentNullException();
            }
            if (A.GetLength(1) != B.GetLength(0) || A.GetLength(0) != result.GetLength(0) || B.GetLength(1) != result.GetLength(1))
            {
                throw new InvalidOperationException();
            }

            int arow = A.GetLength(0), acol = B.GetLength(0), bcol = B.GetLength(1), i, j;

            // For sufficiently small matrices, operate directly on the [,] structure
            if (UseNaiveMultiplicationMethod(arow, acol, bcol))
            {
                for (i = 0; i < arow; i++)
                {
                    for (j = 0; j < bcol; j++)
                    {
                        result[i, j] = MatrixProduct(A, B, i, j, acol);
                    }
                }
                return;
            }

            // For larger matrices, it is more efficient to spend O(n^2) time converting B
            // into jagged matrices then calculating the result in that form
            // Access is more efficient if we transpose B first
            T[][] _Bt = new T[bcol][];
            for (i = 0; i < bcol; ++i)
            {
                T[] col = new T[acol];
                for (j = 0; j < acol; ++j)
                {
                    col[j] = B[j, i];
                }
                _Bt[i] = col;
            }

            T[] A_i = new T[acol];
            for (i = 0; i < arow; i++)
            {
                for (j = 0; j < acol; ++j)
                {
                    A_i[j] = A[i, j];
                }

                for (j = 0; j < bcol; ++j)
                {
                    result[i, j] = DotProduct(A_i, _Bt[j], acol);
                }
            }
        }
        public static void Multiply(this int[,] A, int[,] B, int[,] result)
        {
            Multiply(A, B, result,
               (M, N, i, j, len) =>
               {
                   int sum = 0;
                   for (int k = 0; k < len; ++k) sum += M[i, k] * N[k, j];
                   return sum;
               },
               (v, u, len) =>
               {
                   int sum = 0;
                   for (int k = 0; k < len; ++k) sum += v[k] * u[k];
                   return sum;
               });
        }
        public static void Multiply(this long[,] A, long[,] B, long[,] result)
        {
            Multiply(A, B, result,
               (M, N, i, j, len) =>
               {
                   long sum = 0L;
                   for (int k = 0; k < len; ++k) sum += M[i, k] * N[k, j];
                   return sum;
               },
               (v, u, len) =>
               {
                   long sum = 0L;
                   for (int k = 0; k < len; ++k) sum += v[k] * u[k];
                   return sum;
               });
        }
        public static void Multiply(this float[,] A, float[,] B, float[,] result)
        {
            Multiply(A, B, result,
               (M, N, i, j, len) =>
               {
                   float sum = 0.0f;
                   for (int k = 0; k < len; ++k) sum += M[i, k] * N[k, j];
                   return sum;
               },
               (v, u, len) =>
               {
                   float sum = 0.0f;
                   for (int k = 0; k < len; ++k) sum += v[k] * u[k];
                   return sum;
               });
        }
        /// <summary>
        /// Calculates the matrix product AB, storing the result in 'result'.
        /// The original matrices A and B are unchanged by this method.
        /// </summary>
        /// <param name="A">Matrix with dimensions m x n</param>
        /// <param name="B">Matrix with dimensions n x p</param>
        /// <param name="result">Matrix with dimensions m x p, which will contain the results of the product AB</param>
        public static void Multiply(this double[,] A, double[,] B, double[,] result)
        {
            Multiply(A, B, result,
                (M, N, i, j, len) =>
                {
                    double sum = 0.0;
                    for (int k = 0; k < len; ++k) sum += M[i, k] * N[k, j];
                    return sum;
                },
                (v, u, len) =>
                {
                    double sum = 0.0;
                    for (int k = 0; k < len; ++k) sum += v[k] * u[k];
                    return sum;
                });
        }
        public static void Multiply(this decimal[,] A, decimal[,] B, decimal[,] result)
        {
            Multiply(A, B, result,
               (M, N, i, j, len) =>
               {
                   decimal sum = 0.0m;
                   for (int k = 0; k < len; ++k) sum += M[i, k] * N[k, j];
                   return sum;
               },
               (v, u, len) =>
               {
                   decimal sum = 0.0m;
                   for (int k = 0; k < len; ++k) sum += v[k] * u[k];
                   return sum;
               });
        }
        /// <summary>
        /// Optimised method for multiplying complex matrices. This method is about 6x faster than using Multiply<Ring<T>>
        /// for medium sized problems <m, n, p> ~ <100, 100, 100>.
        /// The original matrices A and B are unchanged by this method.
        /// </summary>
        /// <param name="A">Matrix with dimensions m x n</param>
        /// <param name="B">Matrix with dimensions n x p</param>
        /// <param name="result">Matrix with dimensions m x p, which will contain the resullts of the product AB</param>
        public static void Multiply(this Complex[,] A, Complex[,] B, Complex[,] result)
        {
            Multiply(A, B, result,
               (M, N, i, j, len) =>
               {
                   double sum_re = 0.0, sum_im = 0.0;
                   for (int k = 0; k < len; k++)
                   {
                       Complex a = M[i, k], b = N[k, j];
                       sum_re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                       sum_im += a.Imaginary * b.Real + a.Real * b.Imaginary;
                   }
                   return new Complex(sum_re, sum_im);
               },
               (v, u, len) =>
               {
                   double sum_re = 0.0, sum_im = 0.0;
                   for (int k = 0; k < len; ++k)
                   {
                       Complex a = v[k], b = u[k];
                       sum_re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                       sum_im += a.Imaginary * b.Real + a.Real * b.Imaginary;
                   }
                   return new Complex(sum_re, sum_im);
               });
        }
        /// <summary>
        /// Calculates the matrix product AB, storing the result in 'result'.
        /// The original matrices A and B are unchanged by this method.
        /// </summary>
        /// <typeparam name="T">Any struct or class implementing the Ring<T> interface</typeparam>
        /// <param name="A">Matrix with dimensions m x n</param>
        /// <param name="B">Matrix with dimensions n x p</param>
        /// <param name="result">Matrix with dimensions m x p, which will contain the results of the product AB</param>
        public static void Multiply<T>(this T[,] A, T[,] B, T[,] result) where T : Ring<T>, new()
        {
            MatrixChecks.CheckNotNull(A, B, result);

            int m = A.GetLength(0), n = A.GetLength(1), p = B.GetLength(1), i, j, k;
            if (n != B.GetLength(0))
            {
                throw new InvalidOperationException($"Invalid matrix dimensions: [{A.GetLength(0)}x{A.GetLength(1)}] vs [{B.GetLength(0)}x{B.GetLength(1)}]");
            }

            // Unlike primitive multiplication, there does not appear to be any benefit in
            // defaulting to the naive implementation for small(er) matrices. 

            // For larger matrices, it is more efficient to spend O(n^2) time converting B
            // into jagged matrices then calculating the result in that form
            // Access is more efficient if we transpose B first

            T zero = A[0, 0].AdditiveIdentity;
            T[][] _Bt = new T[p][];
            for (i = 0; i < p; ++i)
            {
                T[] col = new T[n];
                for (j = 0; j < n; ++j)
                {
                    col[j] = B[j, i];
                }
                _Bt[i] = col;
            }

            T[] A_i = new T[n];
            for (i = 0; i < m; i++)
            {
                for (j = 0; j < n; ++j)
                {
                    A_i[j] = A[i, j];
                }

                for (j = 0; j < p; ++j)
                {
                    T sum = zero;
                    T[] Bt_j = _Bt[j];
                    for (k = 0; k < n; k++)
                    {
                        sum = sum.Add(A_i[k].Multiply(Bt_j[k]));
                    }
                    result[i, j] = sum;
                }
            }
        }

        #endregion

        #region Parallel multiplication 
        /// <summary>
        /// Calculates AB in parallel across multiple threads
        /// The number of columns of A must match the number of rows of B, otherwise InvalidOperationException will be thrown
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="nThreads">The number of threads to use</param>
        /// <returns></returns>
        public static double[,] ParallelMultiply(this double[,] A, double[,] B)
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A.GetLength(1) != B.GetLength(0)) throw new InvalidOperationException();

            int arow = A.GetLength(0), acol = A.GetLength(1), bcol = B.GetLength(1);
            double[][] C = MatrixInternalExtensions.JZero(arow, bcol);

            // Access is more efficient if we transpose B first
            double[][] _Bt = new double[bcol][];
            for (int i = 0; i < acol; ++i)
            {
                double[] col = new double[acol];
                for (int j = 0; j < acol; ++j)
                {
                    col[j] = B[j, i];
                }
                _Bt[i] = col;
            }

            Parallel.ForEach(Partitioner.Create(0, arow), range =>
            {
                int start = range.Item1, end = range.Item2, i;
                for (i = start; i < end; ++i)
                {
                    double[] A_i = new double[acol], C_i = C[i];

                    for (int r = 0; r < acol; ++r)
                    {
                        A_i[r] = A[i, r];
                    }

                    for (int j = 0; j < bcol; j++)
                    {
                        double[] Bt_j = _Bt[j];
                        double sum = 0.0;
                        for (int k = 0; k < acol; k++)
                        {
                            sum += A_i[k] * Bt_j[k];
                        }
                        C_i[j] = sum;
                    }
                }
            });

            return C.ToRectangular();
        }
        public static T[,] ParallelMultiply<T>(this T[,] A, T[,] B) where T : Ring<T>, new()
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A.GetLength(1) != B.GetLength(0)) throw new InvalidOperationException();

            int arow = A.GetLength(0), acol = A.GetLength(1), bcol = B.GetLength(1);
            T zero = A[0, 0].AdditiveIdentity;
            T[][] C = MatrixInternalExtensions.JZero(zero, arow, bcol);

            // Access is more efficient if we transpose B first
            T[][] _Bt = new T[bcol][];
            for (int i = 0; i < acol; ++i)
            {
                T[] col = new T[acol];
                for (int j = 0; j < acol; ++j)
                {
                    col[j] = B[j, i];
                }
                _Bt[i] = col;
            }

            Parallel.ForEach(Partitioner.Create(0, arow), range =>
            {
                int start = range.Item1, end = range.Item2, i;
                for (i = start; i < end; ++i)
                {
                    T[] A_i = new T[acol], C_i = C[i];

                    for (int r = 0; r < acol; ++r)
                    {
                        A_i[r] = A[i, r];
                    }

                    for (int j = 0; j < bcol; j++)
                    {
                        T[] Bt_j = _Bt[j];
                        T sum = zero;
                        for (int k = 0; k < acol; k++)
                        {
                            sum = sum.Add(A_i[k].Multiply(Bt_j[k]));
                        }
                        C_i[j] = sum;
                    }
                }
            });

            return C.ToRectangular();
        }
        #endregion

        #region Strassen matrix multiplication
        private static void multiply_strassen<T>(this T[,] A, T[,] B, T[,] result, IStrassenInstructionSet<T> instructions) where T : new()
        {
            MatrixChecks.CheckMatrixDimensionsForMultiplication(A, B, result);
            var strassen = new IndefiniteSizeRectStrassenMultiplication<T>(instructions, A.GetLength(0), A.GetLength(1), B.GetLength(1), true);
            strassen.Multiply(A, B, result);
        }
        /// <summary>
        /// Calculate the matrix product AB using the Strassen method, storing the result in 'result'.
        /// The strassen method starts to outperform naive matrix multiplication for problem sizes of 
        /// <m, n, p> ~ <1000, 1000, 1000>. It has a complexity of O(n^2.807) versus the naive matrix 
        /// multiplication complexity of O(n^3). 
        /// 
        /// A parallel version of this algorithm is also available. 
        /// The original matrices A and B are unchanged by this method.
        /// </summary>
        /// <typeparam name="T">Any struct or class implementing the Ring<T> interface</typeparam>
        /// <param name="A">Matrix with dimensions m x n</param>
        /// <param name="B">Matrix with dimensions n x p</param>
        /// <param name="result">Matrix with dimensions m x p, which will contain the results of the product AB</param>
        public static void MultiplyStrassen<T>(this T[,] A, T[,] B, T[,] result) where T : Ring<T>, new() => throw new NotImplementedException();
        public static void MultiplyStrassen(this int[,] A, int[,] B, int[,] result) => multiply_strassen(A, B, result, new Int32StrassenInstructionSet());
        public static void MultiplyStrassen(this long[,] A, long[,] B, long[,] result) => multiply_strassen(A, B, result, new Int64StrassenInstructionSet());
        public static void MultiplyStrassen(this float[,] A, float[,] B, float[,] result) => multiply_strassen(A, B, result, new FloatStrassenInstructionSet());
        public static void MultiplyStrassen(this double[,] A, double[,] B, double[,] result) => multiply_strassen(A, B, result, new DoubleStrassenInstructionSet());
        public static void MultiplyStrassen(this decimal[,] A, decimal[,] B, decimal[,] result) => multiply_strassen(A, B, result, new DecimalStrassenInstructionSet());
        public static void MultiplyStrassen(this Complex[,] A, Complex[,] B, Complex[,] result) => multiply_strassen(A, B, result, new ComplexStrassenInstructionSet());
        #endregion

        #region Matrix vector multiplication
        private static T[] Multiply<T>(this T[,] A, T[] x, Func<T[,], int, T[], int, T> DotProduct)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(x);

            int arow = A.GetLength(0), acol = A.GetLength(1);
            MatrixChecks.CheckDimensions(acol, x.GetLength(0));

            T[] result = new T[arow];
            for (int i = 0; i < arow; i++)
            {
                result[i] = DotProduct(A, i, x, acol);
            }
            return result;
        }
        public static int[] Multiply(this int[,] A, int[] x)
        {
            return Multiply(A, x, (_A, i, _x, len) =>
            {
                int sum = 0;
                for (int j = 0; j < len; ++j) sum += A[i, j] * x[j];
                return sum;
            });
        }
        public static long[] Multiply(this long[,] A, long[] x)
        {
            return Multiply(A, x, (_A, i, _x, len) =>
            {
                long sum = 0L;
                for (int j = 0; j < len; ++j) sum += A[i, j] * x[j];
                return sum;
            });
        }
        /// <summary>
        /// Multiplies a matrix by a vector, to form the product Ax. The original matrix and vector will 
        /// remain unchanged.
        /// The number of columns of A must match the dimensionality of x, or a ArgumentException will be thrown.
        /// </summary>
        /// <param name="A">A 2D real matrix</param>
        /// <param name="x">A vector</param>
        /// <returns>A vector representing the product Ax</returns>
        public static double[] Multiply(this double[,] A, double[] x)
        {
            return Multiply(A, x, (_A, i, _x, len) =>
            {
                double sum = 0.0;
                for (int j = 0; j < len; ++j) sum += A[i, j] * x[j];
                return sum;
            });
        }
        public static float[] Multiply(this float[,] A, float[] x)
        {
            return Multiply(A, x, (_A, i, _x, len) =>
            {
                float sum = 0.0f;
                for (int j = 0; j < len; ++j) sum += A[i, j] * x[j];
                return sum;
            });
        }
        public static decimal[] Multiply(this decimal[,] A, decimal[] x)
        {
            return Multiply(A, x, (_A, i, _x, len) =>
            {
                decimal sum = 0.0m;
                for (int j = 0; j < len; ++j) sum += A[i, j] * x[j];
                return sum;
            });
        }
        public static Complex[] Multiply(this Complex[,] A, Complex[] x)
        {
            return Multiply(A, x, (_A, i, _x, len) =>
            {
                double re = 0.0, im = 0.0;
                for (int j = 0; j < len; ++j)
                {
                    Complex a = A[i, j], b = x[j];
                    re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                    im += a.Imaginary * b.Real + a.Real * b.Imaginary;
                }
                return new Complex(re, im);
            });
        }
        public static T[] Multiply<T>(this T[,] A, T[] x) where T : Ring<T>, new()
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(x);

            int arow = A.GetLength(0), acol = A.GetLength(1);

            MatrixChecks.CheckDimensions(acol, x.GetLength(0));

            T[] result = new T[arow];
            T zero = x[0].AdditiveIdentity;

            int i, j;
            for (i = 0; i < arow; i++)
            {
                T sum = zero;
                for (j = 0; j < acol; j++)
                {
                    sum = sum.Add(A[i, j].Multiply(x[j]));
                }
                result[i] = sum;
            }
            return result;
        }
        #endregion

        #region Matrix scalar multiplication
        private static T[,] Multiply<T>(this T[,] A, T s, Func<T, T, T> Multiply)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.GetLength(0), cols = A.GetLength(1), r, c;
            T[,] result = new T[rows, cols];
            for (r = 0; r < rows; ++r)
            {
                for (c = 0; c < cols; ++c)
                {
                    result[r, c] = Multiply(A[r, c], s);
                }
            }
            return result;
        }
        /// <summary>
        /// Multiplies matrices with by a scalar. The original matrix is unchanged.
        /// </summary>
        /// <param name="A">Matrix with dimensions m x n</param>
        /// <param name="s">Any scalar</param>
        /// <returns>The product of the matrix by the scalar.</returns>
        public static int[,] Multiply(this int[,] A, int s) => Multiply(A, s, (x, y) => x * y);
        public static long[,] Multiply(this long[,] A, long s) => Multiply(A, s, (x, y) => x * y);
        public static float[,] Multiply(this float[,] A, float s) => Multiply(A, s, (x, y) => x * y);
        public static double[,] Multiply(this double[,] A, double s) => Multiply(A, s, (x, y) => x * y);
        public static decimal[,] Multiply(this decimal[,] A, decimal s) => Multiply(A, s, (x, y) => x * y);
        public static T[,] Multiply<T>(this T[,] A, T s) where T : Ring<T>, new() => Multiply(A, s, (x, y) => x.Multiply(y));
        #endregion

        #region Kronecker Product
        private static T[,] KroneckerProduct<T>(this T[,] A, T[,] B, Func<T, T, T> Multiply)
        {
            if (A == null || B == null)
            {
                throw new ArgumentNullException();
            }

            int m = A.GetLength(0), n = A.GetLength(1), p = B.GetLength(0), q = B.GetLength(1), i, j, k, l, row = 0;

            T[,] result = new T[m * p, n * q];
            for (i = 0; i < m; ++i)
            {
                for (k = 0; k < p; ++k)
                {
                    int col = 0;
                    for (j = 0; j < n; ++j)
                    {
                        T A_ij = A[i, j];
                        for (l = 0; l < q; ++l)
                        {
                            result[row, col++] = Multiply(A_ij, B[k, l]);
                        }
                    }
                    row++;
                }
            }
            return result;
        }
        public static int[,] KroneckerProduct(this int[,] A, int[,] B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static long[,] KroneckerProduct(this long[,] A, long[,] B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static float[,] KroneckerProduct(this float[,] A, float[,] B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static double[,] KroneckerProduct(this double[,] A, double[,] B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static decimal[,] KroneckerProduct(this decimal[,] A, decimal[,] B) => KroneckerProduct(A, B, (a, b) => a * b);
        public static T[,] KroneckerProduct<T>(this T[,] A, T[,] B) where T : Ring<T>, new() => KroneckerProduct(A, B, (T a, T b) => a.Multiply(b));
        #endregion

        #region Khatri-Rao Product
        internal static T[,] KhatriRaoProduct<T>(T[,] A, T[,] B, Func<T, T, T> Multiply)
        {
            if (A == null || B == null) throw new ArgumentNullException();
            if (A.GetLength(1) != B.GetLength(1)) throw new InvalidOperationException();

            int m = A.GetLength(0), n = A.GetLength(1), p = B.GetLength(0), i, j, k, row = 0;
            T[,] result = new T[m * p, n];

            for (j = 0; j < n; ++j)
            {
                row = 0;
                for (i = 0; i < m; ++i)
                {
                    T A_ij = A[i, j];
                    for (k = 0; k < p; ++k)
                    {
                        result[row++, j] = Multiply(A_ij, B[k, j]);
                    }
                }
            }
            return result;
        }
        public static int[,] KhatriRaoProduct(this int[,] A, int[,] B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static long[,] KhatriRaoProduct(this long[,] A, long[,] B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static float[,] KhatriRaoProduct(this float[,] A, float[,] B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static double[,] KhatriRaoProduct(this double[,] A, double[,] B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static decimal[,] KhatriRaoProduct(this decimal[,] A, decimal[,] B) => KhatriRaoProduct(A, B, (a, b) => a * b);
        public static T[,] KhatriRaoProduct<T>(this T[,] A, T[,] B) where T : Ring<T>, new() => KhatriRaoProduct(A, B, (a, b) => a.Multiply(b));
        #endregion

        #region Hadamard Product
        internal static T[,] HadamardProduct<T>(this T[,] A, T[,] B, Func<T, T, T> Multiply)
        {
            if (A == null || B == null)
            {
                throw new ArgumentNullException();
            }
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            T[,] result = new T[m, n];
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    result[i, j] = Multiply(A[i, j], B[i, j]);
                }
            }
            return result;
        }
        public static int[,] HadamardProduct(this int[,] A, int[,] B) => HadamardProduct(A, B, (a, b) => a * b);
        public static long[,] HadamardProduct(this long[,] A, long[,] B) => HadamardProduct(A, B, (a, b) => a * b);
        public static float[,] HadamardProduct(this float[,] A, float[,] B) => HadamardProduct(A, B, (a, b) => a * b);
        public static double[,] HadamardProduct(this double[,] A, double[,] B) => HadamardProduct(A, B, (a, b) => a * b);
        public static decimal[,] HadamardProduct(this decimal[,] A, decimal[,] B) => HadamardProduct(A, B, (a, b) => a * b);
        public static T[,] HadamardProduct<T>(this T[,] A, T[,] B) where T : Ring<T>, new() => HadamardProduct(A, B, (a, b) => a.Multiply(b));

        #endregion

        #region Hadamard Power
        internal static T[,] HadamardPower<T>(this T[,] A, T exponent, Func<T, T, T> Exp)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            int m = A.GetLength(0), n = A.GetLength(1), i, j;
            T[,] result = new T[m, n];
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < m; ++j)
                {
                    result[i, j] = Exp(A[i, j], exponent);
                }
            }
            return result;
        }
        public static float[,] HadamardPower(this float[,] A, float power) => HadamardPower(A, power, (a, e) => (float)Math.Pow(a, e));
        public static double[,] HadamardPower(this double[,] A, double power) => HadamardPower(A, power, (a, e) => Math.Pow(a, e));
        public static decimal[,] HadamardPower(this decimal[,] A, decimal power) => HadamardPower(A, power, (a, e) => DecimalMath.Pow(a, e));
        public static Complex[,] HadamardPower(this Complex[,] A, double power) => HadamardPower(A, power, (a, e) => Complex.Pow(a, e.Real));
        #endregion

        #region Direct Sum
        /// <summary>
        /// Calculates the direct sum of A (+) B, where A and B are matrices of arbitrary size
        /// Returns a matrix with dimensions equal to the sum of the dimensions of A and B.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static T[,] DirectSum<T>(this T[,] A, T[,] B)
        {
            MatrixChecks.CheckNotNull(A, B);

            int m1 = A.GetLength(0), n1 = A.GetLength(1), m2 = B.GetLength(0), n2 = B.GetLength(1), i, j;
            T[,] sum = new T[m1 + m2, n1 + n2];
            for (i = 0; i < m1; ++i)
            {
                for (j = 0; j < n1; ++j)
                {
                    sum[i, j] = A[i, j];
                }
            }
            for (i = 0; i < m2; ++i)
            {
                int row = i + m1;
                for (j = 0; j < n2; ++j)
                {
                    sum[row, j + n1] = B[i, j]; 
                }
            }
            return sum;
        }
        #endregion

        #region Kronecker Sum
        /// <summary>
        /// Calculates the Kronecker sum of A (+) B, defined as (A x I(m)) + (I(n) x B) where x 
        /// denotes the Kronecker product, and I(k) is the identity matrix of size k x k.
        /// 
        /// A and B are square matrices of size n x n and m x m respectively. 
        /// 
        /// The matrix returned will be of size mn.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        private static T[,] KroneckerSum<T>(this T[,] A, T[,] B, Func<T, T, T> Add)
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.CheckIsSquare(A);
            MatrixChecks.CheckIsSquare(B);

            int n = A.GetLength(0), m = B.GetLength(0), d = m * n, i, j, k;
            T[,] sum = new T[d, d];
            for (i = 0; i < n; ++i)
            {
                int im = i * m;
                for (j = 0; j < n; ++j)
                {
                    int jm = j * m;
                    for (k = 0; k < n; ++k)
                    {
                        sum[im + k, jm + k] = A[i, j];
                    }
                }
            }

            for (k = 0; k < n; ++k)
            {
                int x = k * n;
                for (i = 0; i < m; ++i)
                {
                    for (j = 0; j < m; ++j)
                    {
                        int a = x + i, b = x + j;
                        sum[a, b] = Add(sum[a, b], B[i, j]);
                    }
                }
            }

            return sum;
        }
        public static int[,] KroneckerSum(this int[,] A, int[,] B) => KroneckerSum(A, B, (a, b) => a + b);
        public static long[,] KroneckerSum(this long[,] A, long[,] B) => KroneckerSum(A, B, (a, b) => a + b);
        public static float[,] KroneckerSum(this float[,] A, float[,] B) => KroneckerSum(A, B, (a, b) => a + b);
        public static double[,] KroneckerSum(this double[,] A, double[,] B) => KroneckerSum(A, B, (a, b) => a + b);
        public static decimal[,] KroneckerSum(this decimal[,] A, decimal[,] B) => KroneckerSum(A, B, (a, b) => a + b);
        public static T[,] KroneckerSum<T>(this T[,] A, T[,] B) where T : AdditiveGroup<T>, new() => KroneckerSum(A, B, (a, b) => a.Add(b));
        #endregion

        #region Givens rotations
        /// <summary>
        /// Returns the Given's rotation matrix for row/column i and j of matrix A
        /// 
        /// If A is of dimension (m x n), the returned Givens rotation matrix (G) will 
        /// be (m x m).
        /// 
        /// Premultiplying matrix A by G can be thought of as a counterclockwise 
        /// rotation of A's column vectors, in the i-j plane, of atan(s/c) radians. 
        /// 
        /// Note that if repeated application Givens rotation is required, it is more 
        /// efficient to use the A.GivensRotate(int i, int j, double s, double c)
        /// method directly on matrix A. 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <param name="c"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static double[,] GivensRotationMatrix(this double[,] A, int i, int j, double c, double s)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.GetLength(0), cols = A.GetLength(1);
            if (i < 0 || i >= rows || i >= cols)
            {
                throw new ArgumentOutOfRangeException($"The value of parameter {i} lies outside the bounds of matrix [{rows} x {cols}].");
            }
            if (j < 0 || j >= rows || j >= cols)
            {
                throw new ArgumentOutOfRangeException($"The value of parameter {j} lies outside the bounds of matrix [{rows} x {cols}].");
            }

            double[,] G = RectangularMatrix.Identity<double>(rows);
            G[i, i] = c;
            G[i, j] = -s;
            G[j, i] = s;
            G[j, j] = c;

            return G;
        }
        /// <summary>
        /// Returns the result of applying a Givens rotation to matrix A, in the row/columns i and j
        /// </summary>
        /// <param name="A"></param>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <param name="c"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static double[,] GivensRotate(this double[,] A, int i, int j, double c, double s)
        {
            MatrixChecks.CheckNotNull(A);

            int rows = A.GetLength(0), cols = A.GetLength(1);
            if (i < 0 || i >= rows || i >= cols)
            {
                throw new ArgumentOutOfRangeException($"The value of parameter {i} lies outside the bounds of matrix [{rows} x {cols}].");
            }
            if (j < 0 || j >= rows || j >= cols)
            {
                throw new ArgumentOutOfRangeException($"The value of parameter {j} lies outside the bounds of matrix [{rows} x {cols}].");
            }

            double[,] GA = A.Copy();
            GA[i, i] = c * A[i, i] - s * A[j, i];
            GA[i, j] = c * A[i, j] - s * A[j, j];
            GA[j, i] = s * A[i, i] + c * A[j, i];
            GA[j, j] = s * A[i, j] + c * A[j, j];

            return GA;
        }
        #endregion
    }
}
