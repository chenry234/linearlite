﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class RectangularMatrixRingExtensions
    {
        /// <summary>
        /// Calculates the matrix trace (sum of diagonal elements) for a given square matrix a. 
        /// The original matrix is unchanged.
        /// If the number of rows does not match the number of columns, ArgumentOutOfRangeException 
        /// will be thrown.
        /// </summary>
        /// <param name="a">A square matrix.</param>
        /// <returns>The trace of the matrix.</returns>
        internal static T Trace<T>(T[,] A, T zero, Func<T, T, T> Add)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckIsSquare(A);

            int n = A.GetLength(0), i;
            T tr = zero;
            for (i = 0; i < n; ++i) tr = Add(tr, A[i, i]);
            return tr;
        }
        public static int Trace(this int[,] A) => Trace(A, 0, (a, b) => a + b);
        public static long Trace(this long[,] A) => Trace(A, 0L, (a, b) => a + b);
        public static float Trace(this float[,] A) => Trace(A, 0.0f, (a, b) => a + b);
        public static double Trace(this double[,] A) => Trace(A, 0.0, (a, b) => a + b);
        public static decimal Trace(this decimal[,] A) => Trace(A, 0.0m, (a, b) => a + b);
        public static T Trace<T>(this T[,] A) where T : Ring<T>, new() => Trace(A, A[0, 0].AdditiveIdentity, (a, b) => a.Add(b));

    }
}
