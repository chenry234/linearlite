﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class SparseMatrixNormExtensions
    {
        public static double ElementwiseNorm(this SparseMatrix<int> A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleBLAS(), x => Math.Abs(x), (a, b) => Math.Pow(a, b));
        public static double ElementwiseNorm(this SparseMatrix<long> A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleBLAS(), x => Math.Abs(x), (a, b) => Math.Pow(a, b));
        public static float ElementwiseNorm(this SparseMatrix<float> A, int p, int q) => ElementwiseNorm(A, p, q, new FloatBLAS(), x => Math.Abs(x), (a, b) => (float)Math.Pow(a, b));
        public static double ElementwiseNorm(this SparseMatrix<double> A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleBLAS(), x => Math.Abs(x), (a, b) => Math.Pow(a, b));
        public static decimal ElementwiseNorm(this SparseMatrix<decimal> A, int p, int q) => ElementwiseNorm(A, p, q, new DecimalBLAS(), x => Math.Abs(x), (a, b) => DecimalMath.Pow(a, (decimal)b));
        public static double ElementwiseNorm(this SparseMatrix<Complex> A, int p, int q) => ElementwiseNorm(A, p, q, new DoubleBLAS(), x => x.Modulus(), (a, b) => Math.Pow(a, b));
        private static F ElementwiseNorm<T, F>(this SparseMatrix<T> A, int p, int q, IBLAS<F> blas, Func<T, F> Norm, Func<F, double, F> Pow) where T : new()
        {
            MatrixChecks.CheckNotNull(A);
            if (p < 1) throw new ArgumentOutOfRangeException();
            if (q < 1) throw new ArgumentOutOfRangeException();

            int columns = A.Columns;
            F norm = blas.Zero;

            // L(1, 1) norm special case
            if (p == 1 && q == 1)
            {
                foreach (T value in A.Values.Values)
                {
                    norm = blas.Add(norm, Norm(value));
                }
                return norm;
            }

            double p_q = q / (double)p;

            Dictionary<long, F> columnSums = new Dictionary<long, F>();
            foreach (KeyValuePair<long, T> pair in A.Values)
            {
                long column = pair.Key % columns;
                if (columnSums.ContainsKey(column))
                {
                    columnSums[column] = blas.Add(columnSums[column], Pow(Norm(pair.Value), p));
                }
            }

            foreach (F value in columnSums.Values)
            {
                norm = blas.Add(norm, Pow(value, p_q));
            }
            return Pow(norm, 1.0 / q);
        }

        public static int InducedInfinityNorm(this SparseMatrix<int> A) => InducedInfinityNorm(A, new IntBLAS(), x => Math.Abs(x));
        public static long InducedInfinityNorm(this SparseMatrix<long> A) => InducedInfinityNorm(A, new LongBLAS(), x => Math.Abs(x));
        public static float InducedInfinityNorm(this SparseMatrix<float> A) => InducedInfinityNorm(A, new FloatBLAS(), x => Math.Abs(x));
        public static double InducedInfinityNorm(this SparseMatrix<double> A) => InducedInfinityNorm(A, new DoubleBLAS(), x => Math.Abs(x));
        public static decimal InducedInfinityNorm(this SparseMatrix<decimal> A) => InducedInfinityNorm(A, new DecimalBLAS(), x => Math.Abs(x));
        public static double InducedInfinityNorm(this SparseMatrix<Complex> A) => InducedInfinityNorm(A, new DoubleBLAS(), x => x.Modulus());
        private static F InducedInfinityNorm<T, F>(this SparseMatrix<T> A, IBLAS<F> blas, Func<T, F> Norm) where F : IComparable<F> where T : new()  
        {
            int n = A.Columns;
            F max = blas.Zero;

            // Group by row 
            Dictionary<long, F> rowSums = new Dictionary<long, F>();
            foreach (KeyValuePair<long, T> pair in A.Values)
            {
                long row = pair.Key / n;
                if (rowSums.ContainsKey(row))
                {
                    rowSums[row] = blas.Add(rowSums[row], Norm(pair.Value));
                }
                else
                {
                    rowSums[row] = Norm(pair.Value);
                }
            }

            foreach (F value in rowSums.Values)
            {
                if (value.CompareTo(max) > 0)
                {
                    max = value;
                }
            }
            return max;
        }
    }
}
