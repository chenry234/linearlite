﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using LinearLite.Structs;
using LinearLite.Structs.Vectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class SparseMatrixOperationsExtensions
    {
        public static SparseMatrix<int> Add(this SparseMatrix<int> A, SparseMatrix<int> B) => A.Add(B, (a, b) => a + b);
        public static SparseMatrix<long> Add(this SparseMatrix<long> A, SparseMatrix<long> B) => A.Add(B, (a, b) => a + b);
        public static SparseMatrix<float> Add(this SparseMatrix<float> A, SparseMatrix<float> B) => A.Add(B, (a, b) => a + b);
        public static SparseMatrix<double> Add(this SparseMatrix<double> A, SparseMatrix<double> B) => A.Add(B, (a, b) => a + b);
        public static SparseMatrix<decimal> Add(this SparseMatrix<decimal> A, SparseMatrix<decimal> B) => A.Add(B, (a, b) => a + b);
        public static SparseMatrix<T> Add<T>(this SparseMatrix<T> A, SparseMatrix<T> B) where T : AdditiveGroup<T>, new() => A.Add(B, (a, b) => a.Add(b));

        public static SparseMatrix<int> Subtract(this SparseMatrix<int> A, SparseMatrix<int> B) => A.Subtract(B, new IntBLAS());
        public static SparseMatrix<long> Subtract(this SparseMatrix<long> A, SparseMatrix<long> B) => A.Subtract(B, new LongBLAS());
        public static SparseMatrix<float> Subtract(this SparseMatrix<float> A, SparseMatrix<float> B) => A.Subtract(B, new FloatBLAS());
        public static SparseMatrix<double> Subtract(this SparseMatrix<double> A, SparseMatrix<double> B) => A.Subtract(B, new DoubleBLAS());
        public static SparseMatrix<decimal> Subtract(this SparseMatrix<decimal> A, SparseMatrix<decimal> B) => A.Subtract(B, new DecimalBLAS());
        public static SparseMatrix<T> Subtract<T>(this SparseMatrix<T> A, SparseMatrix<T> B) where T : AdditiveGroup<T>, new() => A.Subtract(B, new AdditiveGroupBLAS<T>());

        public static SparseMatrix<int> Multiply(this SparseMatrix<int> A, SparseMatrix<int> B) => A.Multiply(B, new IntBLAS());
        public static SparseMatrix<long> Multiply(this SparseMatrix<long> A, SparseMatrix<long> B) => A.Multiply(B, new LongBLAS());
        public static SparseMatrix<float> Multiply(this SparseMatrix<float> A, SparseMatrix<float> B) => A.Multiply(B, new FloatBLAS());
        public static SparseMatrix<double> Multiply(this SparseMatrix<double> A, SparseMatrix<double> B) => A.Multiply(B, new DoubleBLAS());
        public static SparseMatrix<decimal> Multiply(this SparseMatrix<decimal> A, SparseMatrix<decimal> B) => A.Multiply(B, new DecimalBLAS());
        public static SparseMatrix<T> Multiply<T>(this SparseMatrix<T> A, SparseMatrix<T> B) where T : Ring<T>, new() => A.Multiply(B, new RingBLAS<T>());

        public static SparseVector<int> Multiply(this SparseMatrix<int> A, SparseVector<int> x) => A.Multiply(x, new IntBLAS());
        public static SparseVector<long> Multiply(this SparseMatrix<long> A, SparseVector<long> B) => A.Multiply(B, new LongBLAS());
        public static SparseVector<float> Multiply(this SparseMatrix<float> A, SparseVector<float> B) => A.Multiply(B, new FloatBLAS());
        public static SparseVector<double> Multiply(this SparseMatrix<double> A, SparseVector<double> B) => A.Multiply(B, new DoubleBLAS());
        public static SparseVector<decimal> Multiply(this SparseMatrix<decimal> A, SparseVector<decimal> B) => A.Multiply(B, new DecimalBLAS());
        public static SparseVector<T> Multiply<T>(this SparseMatrix<T> A, SparseVector<T> B) where T : Ring<T>, new() => A.Multiply(B, new RingBLAS<T>());

        public static SparseVector<int> Multiply(this SparseMatrix<int> A, int[] x) => A.Multiply(x, new IntBLAS());
        public static SparseVector<long> Multiply(this SparseMatrix<long> A, long[] x) => A.Multiply(x, new LongBLAS());
        public static SparseVector<float> Multiply(this SparseMatrix<float> A, float[] x) => A.Multiply(x, new FloatBLAS());
        public static SparseVector<double> Multiply(this SparseMatrix<double> A, double[] x) => A.Multiply(x, new DoubleBLAS());
        public static SparseVector<decimal> Multiply(this SparseMatrix<decimal> A, decimal[] x) => A.Multiply(x, new DecimalBLAS());
        public static SparseVector<T> Multiply<T>(this SparseMatrix<T> A, T[] x) where T : Ring<T>, new() => A.Multiply(x, new RingBLAS<T>());

        public static SparseMatrix<int> Multiply(this SparseMatrix<int> A, int s) => A.Multiply(s, (a, b) => a * b);
        public static SparseMatrix<long> Multiply(this SparseMatrix<long> A, long s) => A.Multiply(s, (a, b) => a * b);
        public static SparseMatrix<float> Multiply(this SparseMatrix<float> A, float s) => A.Multiply(s, (a, b) => a * b);
        public static SparseMatrix<double> Multiply(this SparseMatrix<double> A, double s) => A.Multiply(s, (a, b) => a * b);
        public static SparseMatrix<decimal> Multiply(this SparseMatrix<decimal> A, decimal s) => A.Multiply(s, (a, b) => a * b);
        public static SparseMatrix<T> Multiply<T>(this SparseMatrix<T> A, T s) where T : Ring<T>, new() => A.Multiply(s, (a, b) => a.Multiply(b));

        private static SparseMatrix<T> HadamardProduct<T>(this SparseMatrix<T> A, SparseMatrix<T> B, IBLAS<T> blas) where T : new()
        {
            MatrixChecks.CheckNotNull(A, B);
            MatrixChecks.CheckMatrixDimensionsEqual(A, B);

            Dictionary<long, T> product = new Dictionary<long, T>(Math.Max(A.Values.Count, B.Values.Count));
            Dictionary<long, T> _B = B.Values;
            foreach (KeyValuePair<long, T> pair in A.Values)
            {
                if (_B.ContainsKey(pair.Key))
                {
                    product[pair.Key] = blas.Multiply(pair.Value, _B[pair.Key]);
                }
            }
            return new SparseMatrix<T>(A.Rows, A.Columns, product);
        }
        public static SparseMatrix<int> HadamardProduct(this SparseMatrix<int> A, SparseMatrix<int> B) => HadamardProduct(A, B, new IntBLAS());
        public static SparseMatrix<long> HadamardProduct(this SparseMatrix<long> A, SparseMatrix<long> B) => HadamardProduct(A, B, new LongBLAS());
        public static SparseMatrix<float> HadamardProduct(this SparseMatrix<float> A, SparseMatrix<float> B) => HadamardProduct(A, B, new FloatBLAS());
        public static SparseMatrix<double> HadamardProduct(this SparseMatrix<double> A, SparseMatrix<double> B) => HadamardProduct(A, B, new DoubleBLAS());
        public static SparseMatrix<decimal> HadamardProduct(this SparseMatrix<decimal> A, SparseMatrix<decimal> B) => HadamardProduct(A, B, new DecimalBLAS());
        public static SparseMatrix<T> HadamardProduct<T>(this SparseMatrix<T> A, SparseMatrix<T> B) where T : Ring<T>, new() => HadamardProduct(A, B, new RingBLAS<T>());

        public static SparseMatrix<int> HadamardPower(this SparseMatrix<int> A, int power) => ElementwiseOperate(A, x => IntegerMath.Pow(x, power));
        public static SparseMatrix<long> HadamardPower(this SparseMatrix<long> A, long power) => ElementwiseOperate(A, x => IntegerMath.Pow(x, power));
        public static SparseMatrix<float> HadamardPower(this SparseMatrix<float> A, float power) => ElementwiseOperate(A, x => (float)Math.Pow(x, power));
        public static SparseMatrix<double> HadamardPower(this SparseMatrix<double> A, double power) => ElementwiseOperate(A, x => Math.Pow(x, power));
        public static SparseMatrix<decimal> HadamardPower(this SparseMatrix<decimal> A, decimal power) => ElementwiseOperate(A, x => DecimalMath.Pow(x, power));

        public static SparseMatrix<int> KroneckerProduct(this SparseMatrix<int> A, SparseMatrix<int> B) => A.KroneckerProduct(B, new IntBLAS());
        public static SparseMatrix<long> KroneckerProduct(this SparseMatrix<long> A, SparseMatrix<long> B) => A.KroneckerProduct(B, new LongBLAS());
        public static SparseMatrix<float> KroneckerProduct(this SparseMatrix<float> A, SparseMatrix<float> B) => A.KroneckerProduct(B, new FloatBLAS());
        public static SparseMatrix<double> KroneckerProduct(this SparseMatrix<double> A, SparseMatrix<double> B) => A.KroneckerProduct(B, new DoubleBLAS());
        public static SparseMatrix<decimal> KroneckerProduct(this SparseMatrix<decimal> A, SparseMatrix<decimal> B) => A.KroneckerProduct(B, new DecimalBLAS());
        public static SparseMatrix<T> KroneckerProduct<T>(this SparseMatrix<T> A, SparseMatrix<T> B) where T : Ring<T>, new() => A.KroneckerProduct(B, new RingBLAS<T>());

        public static SparseMatrix<T> ElementwiseOperate<F, T>(this SparseMatrix<F> A, Func<F, T> Operation) where F : new() where T : new()
        {
            MatrixChecks.CheckNotNull(A);

            Dictionary<long, T> result = new Dictionary<long, T>(A.Values.Count);
            Dictionary<long, F> values = A.Values;

            foreach (KeyValuePair<long, F> pair in values)
            {
                result[pair.Key] = Operation(pair.Value);
            }
            return new SparseMatrix<T>(A.Rows, A.Columns, result);
        }

        public static SparseMatrix<Complex> ConjugateTranspose(this SparseMatrix<Complex> A)
        {
            SparseMatrix<Complex> transpose = A.Transpose();

            Dictionary<long, Complex> values = transpose.Values;
            List<long> keys = values.Keys.ToList();
            foreach (long key in keys)
            {
                // Convert into conjugate
                values[key] = values[key].Conjugate();
            }
            return transpose;
        }

        public static int Trace(this SparseMatrix<int> A) => A.Trace(new IntBLAS());
        public static long Trace(this SparseMatrix<long> A) => A.Trace(new LongBLAS());
        public static float Trace(this SparseMatrix<float> A) => A.Trace(new FloatBLAS());
        public static double Trace(this SparseMatrix<double> A) => A.Trace(new DoubleBLAS());
        public static decimal Trace(this SparseMatrix<decimal> A) => A.Trace(new DecimalBLAS());
        public static T Trace<T>(this SparseMatrix<T> A) where T : AdditiveGroup<T>, new() => A.Trace(new AdditiveGroupBLAS<T>());
    }
}
