﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Strassen
{
    internal class ComplexStrassenInstructionSet : IStrassenInstructionSet<Complex>
    {
        public void add(Complex[][] A, Complex[][] B, Complex[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int r, c;
            Complex[] result_r, A_r, B_r;

            if (a_row_start == 0 && a_col_start == 0)
            {
                for (r = 0; r < rows; ++r)
                {
                    result_r = result[r];
                    A_r = A[r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        Complex a = A_r[c], b = B_r[b_col_start + c];
                        result_r[c].Real = a.Real + b.Real;
                        result_r[c].Imaginary = a.Imaginary + b.Imaginary;
                    }
                }
            }
            else
            {
                for (r = 0; r < rows; ++r)
                {
                    result_r = result[r];
                    A_r = A[a_row_start + r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        Complex a = A_r[a_col_start + c], b = B_r[b_col_start + c];
                        result_r[c].Real = a.Real + b.Real;
                        result_r[c].Imaginary = a.Imaginary + b.Imaginary;
                    }
                }
            }
        }

        public void add_parallel(Complex[][] A, Complex[][] B, Complex[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        public void append_bottom_row(Complex[][] A, Complex[][] B, Complex[][] result, int a, int b, int c)
        {
            int submatrix_rows = (a / 2) * 2,
                submatrix_cols = (c / 2) * 2;

            int k, j;
            Complex[] A_last = A[submatrix_rows];
            Complex[] result_row = result[submatrix_rows];
            for (j = 0; j < submatrix_cols; ++j)
            {
                double sum_re = 0.0, sum_im = 0.0;
                for (k = 0; k < b; ++k)
                {
                    Complex x = A_last[k], y = B[k][j];
                    sum_re += x.Real * y.Real - x.Imaginary * y.Imaginary;
                    sum_im += x.Imaginary * y.Real + x.Real * y.Imaginary;
                }
                result_row[j] = new Complex(sum_re, sum_im);
            }
        }
        public void append_bottom_row(Complex[][] A, Complex[][] B, Complex[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            int submatrix_rows = (m / 2) * 2,
                   A_col_end = A_col_start + n,
                   B_col_end = B_col_start + (p / 2) * 2,
                   B_A_offset = B_row_start - A_col_start,
                   result_B_col_offset = result_col_start - B_col_start;

            int k, j;
            Complex[] A_last = A[A_row_start + submatrix_rows],
                result_row = result[result_row_start + submatrix_rows];

            for (j = B_col_start; j < B_col_end; ++j)
            {
                double sum_re = 0.0, sum_im = 0.0;
                for (k = A_col_start; k < A_col_end; ++k)
                {
                    Complex x = A_last[k], y = B[k + B_A_offset][j];
                    sum_re += x.Real * y.Real - x.Imaginary * y.Imaginary;
                    sum_im += x.Imaginary * y.Real + x.Real * y.Imaginary;
                }
                result_row[j + result_B_col_offset] = new Complex(sum_re, sum_im);
            }
        }

        public void append_right_column(Complex[][] A, Complex[][] B, Complex[][] result, int a, int b, int c)
        {
            int last_col = c - 1, i, k;
            Complex w, z;
            for (i = 0; i < a; ++i)
            {
                Complex[] A_i = A[i];
                double re = 0.0, im = 0.0;
                for (k = 0; k < b; ++k)
                {
                    z = A_i[k];
                    w = B[k][last_col];
                    re += z.Real * w.Real - z.Imaginary * w.Imaginary;
                    im += z.Imaginary * w.Real + z.Real * w.Imaginary;
                }
                result[i][last_col] = new Complex(re, im);
            }
        }
        public void append_right_column(Complex[][] A, Complex[][] B, Complex[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            int B_col = B_col_start + p - 1,
                   result_col = result_col_start + p - 1,
                   A_col_end = A_col_start + n,
                   B_offset = B_row_start - A_col_start,
                   i, k;

            Complex w, z;
            for (i = 0; i < m; ++i)
            {
                Complex[] A_i = A[i + A_row_start];
                double re = 0.0, im = 0.0;
                for (k = A_col_start; k < A_col_end; ++k)
                {
                    z = A_i[k];
                    w = B[k + B_offset][B_col];
                    re += z.Real * w.Real - z.Imaginary * w.Imaginary;
                    im += z.Imaginary * w.Real + z.Real * w.Imaginary;
                }
                result[i + result_row_start][result_col] = new Complex(re, im);
            }
        }

        public void append_top_left_submatrix(Complex[][] A, Complex[][] B, Complex[][] result, int a, int b, int c)
        {
            int submatrix_rows = (a / 2) * 2, submatrix_cols = (c / 2) * 2, submatrix_terms = b - 1;

            int i, j;
            Complex[] B_last = B[submatrix_terms];
            for (i = 0; i < submatrix_rows; ++i)
            {
                Complex[] result_i = result[i];
                Complex s = A[i][submatrix_terms];

                for (j = 0; j < submatrix_cols; ++j)
                {
                    result_i[j].IncrementBy(s.Multiply(B_last[j]));
                }
            }
        }
        public void append_top_left_submatrix(Complex[][] A, Complex[][] B, Complex[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            int submatrix_rows = (m / 2) * 2,
               submatrix_cols = (p / 2) * 2,
               B_row = B_row_start + n - 1,
               A_col = A_col_start + n - 1;

            int i, j;
            Complex[] B_last = B[B_row];
            for (i = 0; i < submatrix_rows; ++i)
            {
                Complex[] result_i = result[i + result_row_start];
                Complex s = A[i + A_row_start][A_col];

                for (j = 0; j < submatrix_cols; ++j)
                {
                    result_i[j + result_col_start].IncrementBy(s.Multiply(B_last[j + B_col_start]));
                }
            }
        }

        public void clear(Complex[][] result, int row_start, int col_start, int rows, int cols)
        {
            int row_end = row_start + rows, col_end = col_start + cols, i, j;
            for (i = row_start; i < row_end; ++i)
            {
                Complex[] row = result[i];
                for (j = col_start; j < col_end; ++j)
                {
                    row[j].Real = 0.0;
                    row[j].Imaginary = 0.0;
                }
            }
        }

        public void decrement(Complex[][] A, Complex[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[a_row_start + i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    int k = a_col_start + j;
                    A_i[k].DecrementBy(B_i[j]);
                }
            }
        }

        public void decrement_parallel(Complex[][] A, Complex[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        public void increment(Complex[][] A, Complex[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[a_row_start + i], B_i = B[i];
                for (j = 0; j < cols; ++j)
                {
                    A_i[a_col_start + j].IncrementBy(B_i[j]);
                }
            }
        }

        public void increment_parallel(Complex[][] A, Complex[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        public void invert_naive(Complex[][] A, Complex[][] result, int n)
        {
            A.Invert(result);
        }

        public void multiply_naive(Complex[][] A, Complex[][] B, Complex[][] Bt_ws, Complex[][] result, int rows, int cols, int n, bool increment)
        {
            int i, j, k;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    double re = 0.0, im = 0.0;
                    for (k = 0; k < n; ++k)
                    {
                        Complex a = A_i[k], b = B[k][j];
                        re += a.Real * b.Real - a.Imaginary * b.Imaginary;
                        im += a.Imaginary * b.Real + a.Real * b.Imaginary;
                    }

                    if (increment)
                    {
                        result_i[j].IncrementBy(new Complex(re, im));
                    }
                    else
                    {
                        result_i[j] = new Complex(re, im);
                    }
                }
            }
        }
        public void multiply_naive(Complex[][] A, Complex[][] B, Complex[][] Bt_ws, Complex[][] result, 
            int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int rows, int cols, int k, bool increment)
        {
            A.multiply_unsafe(B, Bt_ws, result, A_row_start, A_col_start, B_row_start, B_col_start, result_row_start, result_col_start, rows, cols, k, increment);
        }
        public void multiply_naive(Complex[][] A, Complex[][] B, Complex[][] result,
            int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int rows, int cols, int k, bool increment)
        {
            A.multiply_unsafe(B, result, A_row_start, A_col_start, B_row_start, B_col_start, result_row_start, result_col_start, rows, cols, k, increment);
        }

        public void multiply_naive_parallel(Complex[][] A, Complex[][] B, Complex[][] Bt_workspace, Complex[][] result, int rows, int cols, int k)
        {
            A.multiply_parallel_unsafe(B, Bt_workspace, result, rows, k, cols);
        }

        public void multiply_naive_transposed(Complex[][] A, Complex[][] Bt, Complex[][] result, int rows, int cols, int k, bool increment)
        {
            throw new NotImplementedException();
        }

        public void multiply_naive_transposed(Complex[][] A, Complex[][] Bt, Complex[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int rows, int cols, int k, bool increment)
        {
            throw new NotImplementedException();
        }

        public void negate(Complex[][] A, Complex[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                Complex[] A_i = A[a_row_start + i], result_i = result[result_row_start + i];
                for (j = 0; j < cols; ++j)
                {
                    result_i[result_col_start + j] = -A_i[a_col_start + j];
                }
            }
        }

        public void subtract(Complex[][] A, Complex[][] B, Complex[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int r, c;
            Complex[] result_r, A_r, B_r;
            for (r = 0; r < rows; ++r)
            {
                result_r = result[r];
                A_r = A[a_row_start + r];
                B_r = B[b_row_start + r];
                for (c = 0; c < cols; ++c)
                {
                    Complex a = A_r[a_col_start + c], b = B_r[b_col_start + c];
                    result_r[c].Real = a.Real - b.Real;
                    result_r[c].Imaginary = a.Imaginary - b.Imaginary;
                }
            }
        }

        public void subtract_parallel(Complex[][] A, Complex[][] B, Complex[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }
    }
}
