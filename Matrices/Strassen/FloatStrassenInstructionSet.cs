﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Strassen
{
    internal class FloatStrassenInstructionSet : IStrassenInstructionSet<float>
    {
        public void add(float[][] A, float[][] B, float[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int i, j;

            if (a_row_start == 0 && a_col_start == 0)
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[i], B_i = B[b_row_start + i], result_i = result[i];
                    for (j = 0, k = b_col_start; j < cols; ++j, ++k)
                    {
                        result_i[j] = A_i[j] + B_i[k];
                    }
                }
            }
            else
            {
                int k, l;
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[a_row_start + i], B_i = B[b_row_start + i], result_i = result[i];
                    for (j = 0, k = a_col_start, l = b_col_start; j < cols; ++j, ++k, ++l)
                    {
                        result_i[j] = A_i[k] + B_i[l];
                    }
                }
            }
        }

        public void add_parallel(float[][] A, float[][] B, float[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        public void append_bottom_row(float[][] A, float[][] B, float[][] result, int a, int b, int c)
        {
            int submatrix_rows = (a / 2) * 2,
                submatrix_cols = (c / 2) * 2;

            int k, j;
            float[] A_last = A[submatrix_rows];
            float[] result_row = result[submatrix_rows];
            for (j = 0; j < submatrix_cols; ++j)
            {
                float sum = 0.0f;
                for (k = 0; k < b; ++k)
                {
                    sum += A_last[k] * B[k][j];
                }
                result_row[j] = sum;
            }
        }

        public void append_bottom_row(float[][] A, float[][] B, float[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            int submatrix_rows = (m / 2) * 2,
                A_col_end = A_col_start + n,
                B_col_end = B_col_start + (p / 2) * 2,
                B_A_offset = B_row_start - A_col_start,
                result_B_col_offset = result_col_start - B_col_start;

            int k, j;
            float[] A_last = A[A_row_start + submatrix_rows], 
                result_row = result[result_row_start + submatrix_rows];

            for (j = B_col_start; j < B_col_end; ++j)
            {
                float sum = 0.0f;
                for (k = A_col_start; k < A_col_end; ++k)
                {
                    sum += A_last[k] * B[k + B_A_offset][j];
                }
                result_row[j + result_B_col_offset] = sum;
            }
        }

        public void append_right_column(float[][] A, float[][] B, float[][] result, int a, int b, int c)
        {
            int last_col = c - 1, i, k;
            for (i = 0; i < a; ++i)
            {
                float[] A_i = A[i];
                float sum = 0.0f;
                for (k = 0; k < b; ++k)
                {
                    sum += A_i[k] * B[k][last_col];
                }
                result[i][last_col] = sum;
            }
        }

        public void append_right_column(float[][] A, float[][] B, float[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            int B_col = B_col_start + p - 1,
                   result_col = result_col_start + p - 1,
                   A_col_end = A_col_start + n,
                   B_offset = B_row_start - A_col_start,
                   i, k;

            for (i = 0; i < m; ++i)
            {
                float[] A_i = A[i + A_row_start];
                float sum = 0.0f;
                for (k = A_col_start; k < A_col_end; ++k)
                {
                    sum += A_i[k] * B[k + B_offset][B_col];
                }
                result[i + result_row_start][result_col] = sum;
            }
        }

        public void append_top_left_submatrix(float[][] A, float[][] B, float[][] result, int a, int b, int c)
        {
            int submatrix_rows = (a / 2) * 2, submatrix_cols = (c / 2) * 2, submatrix_terms = b - 1;

            int i, j;
            float[] B_last = B[submatrix_terms];
            for (i = 0; i < submatrix_rows; ++i)
            {
                float[] result_i = result[i];
                float s = A[i][submatrix_terms];

                for (j = 0; j < submatrix_cols; ++j)
                {
                    result_i[j] += s * B_last[j];
                }
            }
        }

        public void append_top_left_submatrix(float[][] A, float[][] B, float[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            int submatrix_rows = (m / 2) * 2,
                   submatrix_cols = (p / 2) * 2,
                   B_row = B_row_start + n - 1,
                   A_col = A_col_start + n - 1;

            int i, j;
            float[] B_last = B[B_row];
            for (i = 0; i < submatrix_rows; ++i)
            {
                float[] result_i = result[i + result_row_start];
                float s = A[i + A_row_start][A_col];

                for (j = 0; j < submatrix_cols; ++j)
                {
                    result_i[j + result_col_start] += s * B_last[j + B_col_start];
                }
            }
        }

        public void clear(float[][] result, int row_start, int col_start, int rows, int cols)
        {
            int row_end = row_start + rows, col_end = col_start + cols, i, j;
            for (i = row_start; i < row_end; ++i)
            {
                float[] row = result[i];
                for (j = col_start; j < col_end; ++j)
                {
                    row[j] = 0.0f;
                }
            }
        }
        public void decrement(float[][] A, float[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] -= B_i[j];
                    }
                }
            }
            else
            {
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[a_col_start + j] -= B_i[j];
                    }
                }
            }
        }

        public void decrement_parallel(float[][] A, float[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        public void increment(float[][] A, float[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;

            if (a_row_start == 0 && a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] += B_i[j];
                    }
                }
            }
            else if (a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] += B_i[j];
                    }
                }
            }
            else
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    float[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0, k = a_col_start; j < cols; ++j, ++k)
                    {
                        A_i[k] += B_i[j];
                    }
                }
            }
        }

        public void increment_parallel(float[][] A, float[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        public void invert_naive(float[][] A, float[][] result, int n)
        {
            int i_max, i, j, k;
            double order;
            float max, swap, alpha;

            // shape into correct order
            order = 0;
            for (i = 0; i < n; ++i)
            {
                float[] A_i = A[i];
                for (j = 0; j < n; ++j)
                {
                    order += Math.Log(Math.Abs(A_i[j]) + 0.01);
                }
            }


            order = Math.Exp(order / (n * n));

            // x_norm stores the values of x after order adjustment
            float[][] A_norm = MatrixInternalExtensions.JMatrix<float>(n, n);
            for (i = 0; i < n; i++)
            {
                float[] A_norm_i = A_norm[i], A_i = A[i];
                for (j = 0; j < n; j++)
                {
                    A_norm_i[j] = (float)(A_i[j] / order);
                }
            }

            // Set result to identity
            for (i = 0; i < n; ++i)
            {
                float[] result_i = result[i];
                for (j = 0; j < n; ++j)
                {
                    result_i[j] = 0.0f;
                }
                result_i[i] = 1.0f;
            }

            // inv
            for (k = 0; k < n - 1; k++)
            {
                // find i_max = argmax(x[i,k] for i = k, ..., n) - for numerical stability
                max = Math.Abs(A_norm[k + 1][k]);
                i_max = k + 1;
                for (i = k + 2; i < n; i++)
                {
                    float abs = Math.Abs(A_norm[i][k]);
                    if (abs > max)
                    {
                        i_max = i;
                        max = abs;
                    }
                }

                // swap row k with row i_max
                float[] A_norm_i_max = A_norm[i_max],
                        A_norm_k = A_norm[k],
                        inv_i_max = result[i_max],
                        inv_k = result[k];
                for (i = 0; i < n; i++)
                {
                    swap = A_norm_i_max[i];
                    A_norm_i_max[i] = A_norm_k[i];
                    A_norm_k[i] = swap;

                    swap = inv_i_max[i];
                    inv_i_max[i] = inv_k[i];
                    inv_k[i] = swap;
                }

                // for each row below k, i.e. for i = k + 1 : n
                for (i = k + 1; i < n; i++)
                {
                    float[] A_norm_i = A_norm[i], inv_i = result[i];
                    alpha = A_norm_i[k] / A_norm_k[k];

                    for (j = k + 1; j < n; j++)
                    {
                        A_norm_i[j] -= A_norm_k[j] * alpha;
                    }
                    for (j = 0; j < n; j++)
                    {
                        inv_i[j] -= inv_k[j] * alpha;
                    }
                    A_norm_i[k] = 0.0f;
                }
            }

            // going back upwards
            for (k = n - 1; k >= 0; --k)
            {
                float[] A_norm_k = A_norm[k], inv_k = result[k];
                for (i = k - 1; i >= 0; --i)
                {
                    float[] A_norm_i = A_norm[i], inv_i = result[i];
                    alpha = A_norm_i[k] / A_norm_k[k];
                    for (j = i + 1; j < n; ++j)
                    {
                        A_norm_i[j] -= A_norm_k[j] * alpha;
                    }
                    for (j = 0; j < n; ++j)
                    {
                        inv_i[j] -= inv_k[j] * alpha;
                    }
                }
            }

            // normalise - with order
            for (k = 0; k < n; ++k)
            {
                alpha = (float)(1.0 / (A_norm[k][k] * order));

                float[] inv_k = result[k];
                for (i = 0; i < n; ++i)
                {
                    inv_k[i] *= alpha;
                }
            }
        }

        public void multiply_naive(float[][] A, float[][] B, float[][] Bt_ws, float[][] result, int rows, int cols, int n, bool increment)
        {
            int i, j, k;
            for (i = 0; i < rows; ++i)
            {
                float[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    float sum = 0.0f;
                    for (k = 0; k < n; ++k)
                    {
                        sum += A_i[k] * B[k][j];
                    }

                    if (increment)
                    {
                        result_i[j] += sum;
                    }
                    else
                    {
                        result_i[j] = sum;
                    }
                }
            }
        }
        public void multiply_naive(float[][] A, float[][] B, float[][] Bt_ws, float[][] result, 
            int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int rows, int cols, int k, bool increment)
        {
            A.multiply_unsafe(B, Bt_ws, result, A_row_start, A_col_start, B_row_start, B_col_start, result_row_start, result_col_start, rows, cols, k, increment);
        }
        public void multiply_naive(float[][] A, float[][] B, float[][] result,
            int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int rows, int cols, int k, bool increment)
        {
            A.multiply_unsafe(B, result, A_row_start, A_col_start, B_row_start, B_col_start, result_row_start, result_col_start, rows, cols, k, increment);
        }
        public void multiply_naive_parallel(float[][] A, float[][] B, float[][] Bt_workspace, float[][] result, int rows, int cols, int k)
        {
            throw new NotImplementedException();
        }

        public void multiply_naive_transposed(float[][] A, float[][] Bt, float[][] result, int rows, int cols, int k, bool increment)
        {
            throw new NotImplementedException();
        }

        public void multiply_naive_transposed(float[][] A, float[][] Bt, float[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int rows, int cols, int k, bool increment)
        {
            throw new NotImplementedException();
        }

        public void negate(float[][] A, float[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                float[] A_i = A[a_row_start + i], result_i = result[result_row_start + i];
                for (j = 0; j < cols; ++j)
                {
                    result_i[result_col_start + j] = -A_i[a_col_start + j];
                }
            }
        }
        public void subtract(float[][] A, float[][] B, float[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int r, c;

            float[] result_r, A_r, B_r;

            if (a_row_start == 0 && a_col_start == 0)
            {
                if (b_row_start == 0 && b_col_start == 0)
                {
                    for (r = 0; r < rows; ++r)
                    {
                        result_r = result[r];
                        A_r = A[r];
                        B_r = B[r];
                        for (c = 0; c < cols; ++c)
                        {
                            result_r[c] = A_r[c] - B_r[c];
                        }
                    }
                }
                else
                {
                    for (r = 0; r < rows; ++r)
                    {
                        result_r = result[r];
                        A_r = A[r];
                        B_r = B[b_row_start + r];
                        for (c = 0; c < cols; ++c)
                        {
                            result_r[c] = A_r[c] - B_r[b_col_start + c];
                        }
                    }
                }
            }
            else
            {
                for (r = 0; r < rows; ++r)
                {
                    result_r = result[r];
                    A_r = A[a_row_start + r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        result_r[c] = A_r[a_col_start + c] - B_r[b_col_start + c];
                    }
                }
            }
        }

        public void subtract_parallel(float[][] A, float[][] B, float[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }
    }
}
