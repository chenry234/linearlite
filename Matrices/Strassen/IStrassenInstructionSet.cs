﻿namespace LinearLite.Matrices.Strassen
{
    internal interface IStrassenInstructionSet<T>
    {
        void add(T[][] A, T[][] B, T[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols);
        void subtract(T[][] A, T[][] B, T[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols);

        /// <summary>
        /// Perform a <rows, k, cols> multiplication
        /// </summary>
        void multiply_naive(T[][] A, T[][] B, T[][] Bt_workspace, T[][] result, int rows, int cols, int k, bool increment);

        /// <summary>
        /// Perform a <rows, k, cols> multiplication, where 
        /// The (row x k) block is taken from A starting at (A_row_start, A_col_start),
        /// The (k x cols) block is taken from B starting at (B_row_start, B_col_start)
        /// The (row x cols) result is stored in 'result' starting at (result_row_start, result_col_start)
        /// If increment is true, the result matrix will be incremented by the multiplication, otherwise the result matrix 
        /// wiil be set to the multiplication.
        /// 
        /// Note: now that we have a transposed multiplication method, this is no longer the preferred method to use
        /// </summary>
        void multiply_naive(T[][] A, T[][] B, T[][] Bt_workspace, T[][] result,
            int A_row_start, int A_col_start,
            int B_row_start, int B_col_start,
            int result_row_start, int result_col_start, int rows, int cols, int k, bool increment);
        void multiply_naive(T[][] A, T[][] B, T[][] result,
            int A_row_start, int A_col_start,
            int B_row_start, int B_col_start,
            int result_row_start, int result_col_start, int rows, int cols, int k, bool increment);

        /// <summary>
        /// Perform a <rows, k, cols> multiplication A * B, where instead of B, B^T is supplied.
        /// This reduces cache misses when accessing B (since all columns are now contigous)..
        /// </summary>
        void multiply_naive_transposed(T[][] A, T[][] Bt, T[][] result, int rows, int cols, int k, bool increment);
        void multiply_naive_transposed(T[][] A, T[][] Bt, T[][] result,
            int A_row_start, int A_col_start,
            int B_row_start, int B_col_start,
            int result_row_start, int result_col_start, int rows, int cols, int k, bool increment);

        void increment(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols);
        void decrement(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols);
        void clear(T[][] result, int row_start, int col_start, int rows, int cols);

        /// <summary>
        /// Complete the bottom row of a <2k + 1, n, p> multiplication, where the (2k + 1)-th row
        /// is not complete because of Strassen. 
        /// Here, a = 2k + 1, b = n, c = p.
        /// </summary>
        void append_bottom_row(T[][] A, T[][] B, T[][] result, int a, int b, int c);
        /// <summary>
        /// Complete the bottom row of a <2k + 1, n, p> multiplication, where the (2k + 1)-th row
        /// is not complete because of Strassen. 
        /// We assume that we're multiplying:
        /// - a (m x p) block from A, starting with (A_row_start, A_col_start), and 
        /// - a (n x p) block from B, starting with (B_row_start, B_col_start), storing the result in:
        /// - a (m x p) block in 'result', starting with (result_row_start, result_col_start). 
        /// </summary>
        void append_bottom_row(T[][] A, T[][] B, T[][] result, 
            int A_row_start, int A_col_start, 
            int B_row_start, int B_col_start, 
            int result_row_start, int result_col_start, 
            int m, int n, int p);

        /// <summary>
        /// Complete the bottom row of a <a, b, c> multiplication, where both or either of a, b is 
        /// odd and incomplete due to Strassen's multiplication.
        /// </summary>
        void append_top_left_submatrix(T[][] A, T[][] B, T[][] result, int a, int b, int c);
        void append_top_left_submatrix(T[][] A, T[][] B, T[][] result,
            int A_row_start, int A_col_start,
            int B_row_start, int B_col_start,
            int result_row_start, int result_col_start,
            int m, int n, int p);

        void append_right_column(T[][] A, T[][] B, T[][] result, int a, int b, int c);
        void append_right_column(T[][] A, T[][] B, T[][] result,
            int A_row_start, int A_col_start,
            int B_row_start, int B_col_start,
            int result_row_start, int result_col_start, 
            int m, int n, int p);


        /// <summary>
        /// Sets the region in matrix 'result' bounded by the submatrix 
        /// [result_row_start, result_row_start + rows) x [result_col_start, result_col_start + cols)
        /// 
        /// with the negation of the region in matrix 'A', bounded by the submatrix
        /// [a_row_start, a_row_start + rows) x [a_col_start, a_col_start + cols)
        /// 
        /// </summary>
        /// <param name="A">The matrix to negate and copy from (src)</param>
        /// <param name="result">The matrix to negate and copy to (dest)</param>
        /// <param name="a_row_start">The top boundary of the submatrix of A</param>
        /// <param name="a_col_start">The left boundary of the submatrix of A</param>
        /// <param name="result_row_start">The top boundary of the submatrix of 'result'</param>
        /// <param name="result_col_start">The left boundary of the submatrix of 'result'</param>
        /// <param name="rows">The number of rows to negate + copy</param>
        /// <param name="cols">The number of cols to negate + copy</param>
        void negate(T[][] A, T[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols);

        /// <summary>
        /// Invert the matrix A (n x n) using a naive algorithm (e.g. Gaussian elimination)
        /// </summary>
        /// <param name="A">Real matrix of dimensions (n x n)</param>
        /// <param name="result">Real matrix of dimensions (n x n), which will be set to inverse of A</param>
        /// <param name="n">The dimensionality of A</param>
        void invert_naive(T[][] A, T[][] result, int n);


        #region Parallel operations 

        void add_parallel(T[][] A, T[][] B, T[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols);
        void subtract_parallel(T[][] A, T[][] B, T[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols);
        void increment_parallel(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols);
        void decrement_parallel(T[][] A, T[][] B, int a_row_start, int a_col_start, int rows, int cols);
        void multiply_naive_parallel(T[][] A, T[][] B, T[][] Bt_workspace, T[][] result, int rows, int cols, int k);

        #endregion
    }
}
