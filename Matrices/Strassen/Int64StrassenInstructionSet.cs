﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Strassen
{
    internal class Int64StrassenInstructionSet : IStrassenInstructionSet<long>
    {
        public void add(long[][] A, long[][] B, long[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int i, j;

            if (a_row_start == 0 && a_col_start == 0)
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    long[] A_i = A[i], B_i = B[b_row_start + i], result_i = result[i];
                    for (j = 0, k = b_col_start; j < cols; ++j, ++k)
                    {
                        result_i[j] = A_i[j] + B_i[k];
                    }
                }
            }
            else
            {
                int k, l;
                for (i = 0; i < rows; ++i)
                {
                    long[] A_i = A[a_row_start + i], B_i = B[b_row_start + i], result_i = result[i];
                    for (j = 0, k = a_col_start, l = b_col_start; j < cols; ++j, ++k, ++l)
                    {
                        result_i[j] = A_i[k] + B_i[l];
                    }
                }
            }
        }

        public void add_parallel(long[][] A, long[][] B, long[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        public void append_bottom_row(long[][] A, long[][] B, long[][] result, int a, int b, int c)
        {
            int submatrix_rows = (a / 2) * 2, submatrix_cols = (c / 2) * 2, submatrix_terms = b - 1;

            int i, j;
            long[] B_last = B[submatrix_terms];
            for (i = 0; i < submatrix_rows; ++i)
            {
                long[] result_i = result[i];
                long s = A[i][submatrix_terms];

                for (j = 0; j < submatrix_cols; ++j)
                {
                    result_i[j] += s * B_last[j];
                }
            }
        }

        public void append_bottom_row(long[][] A, long[][] B, long[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            throw new NotImplementedException();
        }

        public void append_right_column(long[][] A, long[][] B, long[][] result, int a, int b, int c)
        {
            int last_col = c - 1, i, k;
            for (i = 0; i < a; ++i)
            {
                long[] A_i = A[i];
                long sum = 0L;
                for (k = 0; k < b; ++k)
                {
                    sum += A_i[k] * B[k][last_col];
                }
                result[i][last_col] = sum;
            }
        }

        public void append_right_column(long[][] A, long[][] B, long[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            throw new NotImplementedException();
        }

        public void append_top_left_submatrix(long[][] A, long[][] B, long[][] result, int a, int b, int c)
        {
            int submatrix_rows = (a / 2) * 2, submatrix_cols = (c / 2) * 2, submatrix_terms = b - 1;

            int i, j;
            long[] B_last = B[submatrix_terms];
            for (i = 0; i < submatrix_rows; ++i)
            {
                long[] result_i = result[i];
                long s = A[i][submatrix_terms];

                for (j = 0; j < submatrix_cols; ++j)
                {
                    result_i[j] += s * B_last[j];
                }
            }
        }

        public void append_top_left_submatrix(long[][] A, long[][] B, long[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int m, int n, int p)
        {
            throw new NotImplementedException();
        }

        public void clear(long[][] result, int row_start, int col_start, int rows, int cols)
        {
            int row_end = row_start + rows, col_end = col_start + cols, i, j;
            for (i = row_start; i < row_end; ++i)
            {
                long[] row = result[i];
                for (j = col_start; j < col_end; ++j)
                {
                    row[j] = 0L;
                }
            }
        }

        public void decrement(long[][] A, long[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    long[] A_i = A[i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] -= B_i[j];
                    }
                }
            }
            else
            {
                for (i = 0; i < rows; ++i)
                {
                    long[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[a_col_start + j] -= B_i[j];
                    }
                }
            }
        }

        public void decrement_parallel(long[][] A, long[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        public void increment(long[][] A, long[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            int i, j;
            if (a_row_start == 0 && a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    long[] A_i = A[i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] += B_i[j];
                    }
                }
            }
            else if (a_col_start == 0)
            {
                for (i = 0; i < rows; ++i)
                {
                    long[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0; j < cols; ++j)
                    {
                        A_i[j] += B_i[j];
                    }
                }
            }
            else
            {
                int k;
                for (i = 0; i < rows; ++i)
                {
                    long[] A_i = A[a_row_start + i], B_i = B[i];
                    for (j = 0, k = a_col_start; j < cols; ++j, ++k)
                    {
                        A_i[k] += B_i[j];
                    }
                }
            }
        }

        public void increment_parallel(long[][] A, long[][] B, int a_row_start, int a_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }

        public void invert_naive(long[][] A, long[][] result, int n)
        {
            throw new NotImplementedException();
        }

        public void multiply_naive(long[][] A, long[][] B, long[][] Bt_ws, long[][] result, int rows, int cols, int n, bool increment)
        {
            int i, j, k;
            for (i = 0; i < rows; ++i)
            {
                long[] A_i = A[i], result_i = result[i];
                for (j = 0; j < cols; ++j)
                {
                    long sum = 0L;
                    for (k = 0; k < n; ++k)
                    {
                        sum += A_i[k] * B[k][j];
                    }

                    if (increment)
                    {
                        result_i[j] += sum;
                    }
                    else
                    {
                        result_i[j] = sum;
                    }
                }
            }
        }
        public void multiply_naive(long[][] A, long[][] B, long[][] Bt_ws, long[][] result, 
            int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int rows, int cols, int k, bool increment)
        {
            A.multiply_unsafe(B, Bt_ws, result, A_row_start, A_col_start, B_row_start, B_col_start, result_row_start, result_col_start, rows, cols, k, increment);
        }
        public void multiply_naive(long[][] A, long[][] B, long[][] result,
            int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int rows, int cols, int k, bool increment)
        {
            A.multiply_unsafe(B, result, A_row_start, A_col_start, B_row_start, B_col_start, result_row_start, result_col_start, rows, cols, k, increment);
        }
        public void multiply_naive_parallel(long[][] A, long[][] B, long[][] Bt_workspace, long[][] result, int rows, int cols, int k)
        {
            throw new NotImplementedException();
        }

        public void multiply_naive_transposed(long[][] A, long[][] Bt, long[][] result, int rows, int cols, int k, bool increment)
        {
            throw new NotImplementedException();
        }

        public void multiply_naive_transposed(long[][] A, long[][] Bt, long[][] result, int A_row_start, int A_col_start, int B_row_start, int B_col_start, int result_row_start, int result_col_start, int rows, int cols, int k, bool increment)
        {
            throw new NotImplementedException();
        }

        public void negate(long[][] A, long[][] result, int a_row_start, int a_col_start, int result_row_start, int result_col_start, int rows, int cols)
        {
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                long[] A_i = A[a_row_start + i], result_i = result[result_row_start + i];
                for (j = 0; j < cols; ++j)
                {
                    result_i[result_col_start + j] = -A_i[a_col_start + j];
                }
            }
        }
        public void subtract(long[][] A, long[][] B, long[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            int r, c;

            long[] result_r, A_r, B_r;

            if (a_row_start == 0 && a_col_start == 0)
            {
                if (b_row_start == 0 && b_col_start == 0)
                {
                    for (r = 0; r < rows; ++r)
                    {
                        result_r = result[r];
                        A_r = A[r];
                        B_r = B[r];
                        for (c = 0; c < cols; ++c)
                        {
                            result_r[c] = A_r[c] - B_r[c];
                        }
                    }
                }
                else
                {
                    for (r = 0; r < rows; ++r)
                    {
                        result_r = result[r];
                        A_r = A[r];
                        B_r = B[b_row_start + r];
                        for (c = 0; c < cols; ++c)
                        {
                            result_r[c] = A_r[c] - B_r[b_col_start + c];
                        }
                    }
                }
            }
            else
            {
                for (r = 0; r < rows; ++r)
                {
                    result_r = result[r];
                    A_r = A[a_row_start + r];
                    B_r = B[b_row_start + r];
                    for (c = 0; c < cols; ++c)
                    {
                        result_r[c] = A_r[a_col_start + c] - B_r[b_col_start + c];
                    }
                }
            }
        }

        public void subtract_parallel(long[][] A, long[][] B, long[][] result, int a_row_start, int a_col_start, int b_row_start, int b_col_start, int rows, int cols)
        {
            throw new NotImplementedException();
        }
    }
}
