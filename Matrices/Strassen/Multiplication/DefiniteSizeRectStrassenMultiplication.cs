﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Strassen
{
    internal class DefiniteSizeRectStrassenMultiplication<T> : IMultiplicationAlgorithm<T> where T : new()
    {
        private readonly bool _useWinogradVariant, _BTransposed, _parallel;
        private readonly int _naiveMultiplicationThreshold;
        private readonly Dictionary<int, T[][]> _tempAs, _tempBs, _Ms, _tempResults;
        private readonly Dictionary<int, T[][]> _A_3s, _B_3s, _tempMs;

        private IStrassenInstructionSet<T> _instruction;
        private T[][] _naive_mult_workspace;

        /// <summary>
        /// Set up a Strassen's matrix multiplication problem.
        /// </summary>
        /// <param name="instructions">The instruction set to use - availble for most primitive types and Field<T></param>
        /// <param name="m">The max. number of rows of the 1st matrix to be multiplied.</param>
        /// <param name="n">The max number of columns of the 1st matrix to be multiplied.</param>
        /// <param name="p">The max number of columns of the 2nd matrix to be multiplied.</param>
        /// <param name="useWinogradVariant">If true, uses the Winograd variant of Strassen's algorithm (15 addition/subtractions instead of 18)</param>
        /// <param name="naiveMultThreshold">The matrix dimensions under which we revert back to regular matrix multiplication.</param>
        /// <param name="BTransposed">If true, the matrix B^T will be provided in place of B (improves caching)</param>
        /// <param name="parallel">If true, the multiplication will execute in parallel.</param>
        internal DefiniteSizeRectStrassenMultiplication(
            IStrassenInstructionSet<T> instructions, 
            int m, int n, int p, 
            bool useWinogradVariant = false, 
            int naiveMultThreshold = 32, 
            bool BTransposed = false,
            bool parallel = false) : base(BTransposed) // no support for transposed yet
        {
            _instruction = instructions;
            _useWinogradVariant = useWinogradVariant;
            _naiveMultiplicationThreshold = naiveMultThreshold;
            _BTransposed = BTransposed;
            _parallel = parallel;

            if (naiveMultThreshold > 0)
            {
                _naive_mult_workspace = MatrixInternalExtensions.JMatrix<T>(naiveMultThreshold, naiveMultThreshold);
            }

            // Pre-calculate the blocks
            _tempAs = new Dictionary<int, T[][]>();
            _tempBs = new Dictionary<int, T[][]>();
            _Ms = new Dictionary<int, T[][]>();
            _tempResults = new Dictionary<int, T[][]>();

            if (_useWinogradVariant)
            {
                _A_3s = new Dictionary<int, T[][]>();
                _B_3s = new Dictionary<int, T[][]>();
                _tempMs = new Dictionary<int, T[][]>();
            }

            while (!use_naive_algorithm(m, n, p))
            {
                _tempResults[m] = MatrixInternalExtensions.JMatrix<T>(m, p);

                m /= 2;
                n /= 2;
                p /= 2;

                _tempAs[m] = MatrixInternalExtensions.JMatrix<T>(m, n);
                _tempBs[n] = MatrixInternalExtensions.JMatrix<T>(n, p);
                _Ms[m] = MatrixInternalExtensions.JMatrix<T>(m, p);

                if (_useWinogradVariant)
                {
                    _A_3s[m] = MatrixInternalExtensions.JMatrix<T>(m, n);
                    _B_3s[m] = MatrixInternalExtensions.JMatrix<T>(n, p);
                    _tempMs[m] = MatrixInternalExtensions.JMatrix<T>(m, p);
                }
            }
        }
        private bool use_naive_algorithm(int m, int n, int p)
        {
            return m < _naiveMultiplicationThreshold || n < _naiveMultiplicationThreshold || p < _naiveMultiplicationThreshold;
        }

        internal override void Multiply(T[][] A, T[][] B, T[][] result, int m, int n, int p)
        {
            if (_useWinogradVariant)
            {
                if (_BTransposed)
                {
                    if (_parallel)
                    {
                        mult_winograd_parallel(A, B, result, m, n, p);
                    }
                    else
                    {
                        mult_winograd(A, B, result, m, n, p);
                    }
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
            else
            {
                if (_BTransposed)
                {
                    mult_strassen(A, B, result, m, n, p);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }
        internal override void Multiply(T[][] A, T[][] B, T[][] result, int A_row_offset, int A_col_offset, int B_row_offset, int B_col_offset, int C_row_offset, int C_col_offset, int m, int n, int p, bool increment)
        {
            throw new NotImplementedException();
        }

        private void mult_strassen(T[][] A, T[][] B, T[][] result, int a, int b, int c)
        {
            int half_a = a / 2, half_b = b / 2, half_c = c / 2;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(a, b, c))
            {
                _instruction.multiply_naive(A, B, _naive_mult_workspace, result, a, c, b, false);
                return;
            }

            T[][] tempA = _tempAs[half_a];
            T[][] tempB = _tempBs[half_b];
            T[][] M = _Ms[half_a];
            T[][] tempResult = _tempResults[a];

            _instruction.clear(tempResult, 0, 0, a, c);

            // M_1 = (A_11 + A_22)(B_11 + B_22)
            _instruction.add(A, A, tempA, 0, 0, half_a, half_b, half_a, half_b);
            _instruction.add(B, B, tempB, 0, 0, half_b, half_c, half_b, half_c);
            mult_strassen(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.increment(tempResult, M, 0, 0, half_a, half_c);
            _instruction.increment(tempResult, M, half_a, half_c, half_a, half_c);

            // M_2 = (A_21 + A_22)B_11
            _instruction.add(A, A, tempA, half_a, 0, half_a, half_b, half_a, half_b);
            Copy(tempB, B, 0, 0, half_b, half_c);
            mult_strassen(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.increment(tempResult, M, half_a, 0, half_a, half_c);
            _instruction.decrement(tempResult, M, half_a, half_c, half_a, half_c);

            // M_3 = A_11(B_12 - B_22) 
            Copy(tempA, A, 0, 0, half_a, half_b);
            _instruction.subtract(B, B, tempB, 0, half_c, half_b, half_c, half_b, half_c);
            mult_strassen(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.increment(tempResult, M, 0, half_c, half_a, half_c);
            _instruction.increment(tempResult, M, half_a, half_c, half_a, half_c);

            // M_4 = A_22(B_21 - B_11)
            Copy(tempA, A, half_a, half_b, half_a, half_b);
            _instruction.subtract(B, B, tempB, half_b, 0, 0, 0, half_b, half_c);
            mult_strassen(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.increment(tempResult, M, 0, 0, half_a, half_c);
            _instruction.increment(tempResult, M, half_a, 0, half_a, half_c);

            // M_5 = (A_11 + A_12)B_22
            _instruction.add(A, A, tempA, 0, 0, 0, half_b, half_a, half_b);
            Copy(tempB, B, half_b, half_c, half_b, half_c);
            mult_strassen(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.decrement(tempResult, M, 0, 0, half_a, half_c);
            _instruction.increment(tempResult, M, 0, half_c, half_a, half_c);

            // M_6 = (A_21 - A_11)(B_11 + B_12)
            _instruction.subtract(A, A, tempA, half_a, 0, 0, 0, half_a, half_b);
            _instruction.add(B, B, tempB, 0, 0, 0, half_c, half_b, half_c);
            mult_strassen(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.increment(tempResult, M, half_a, half_c, half_a, half_c);

            // M_7 = (A_12 - A_22)(B_21 + B_22) 
            _instruction.subtract(A, A, tempA, 0, half_b, half_a, half_b, half_a, half_b);
            _instruction.add(B, B, tempB, half_b, 0, half_b, half_c, half_b, half_c);
            mult_strassen(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.increment(tempResult, M, 0, 0, half_a, half_c);

            Copy(result, tempResult, 0, 0, a, c);

            // If a is odd, append the bottom row
            if (a % 2 == 1)
            {
                _instruction.append_bottom_row(A, B, result, a, b, c);
            }
            // If b is odd, append to the top-left submatrix
            if (b % 2 == 1)
            {
                _instruction.append_top_left_submatrix(A, B, result, a, b, c);
            }
            // If c is odd, append the rightmost column
            if (c % 2 == 1)
            {
                _instruction.append_right_column(A, B, result, a, b, c);
            }
        }

        /// <summary>
        /// Postmultiply A (a x b) by B (b x c), storing the result in 'result' (a x c).
        /// 
        /// This method uses the Winograd variant of Strassen's algorithm, which uses 15 additions
        /// instead of 18 (at the cost of higher memory consumption).
        /// 
        /// This implementation runs approximately 5% faster than the naive Strassen algorithm
        /// implementation, and consumes approximately twice the memory of the native 
        /// Strassen algorithm.
        /// 
        /// Algorithm: 
        /// With A_11, A_12, A_21, A_22, B_11, B_12, B_21, B_22 defined as the same submatrices as per
        /// Strassen, we define:
        /// 
        /// M_1 := A_11 * B_11
        /// M_2 := A_12 * B_21
        /// M_3 := (-A_11 + A_21 + A_22) * (B_11 - B_12 + B_22)
        /// M_4 := (A_11 - A_21) * (-B_12 + B_22)
        /// M_5 := (A_21 + A_22) * (-B_11 + B_12)
        /// M_6 := (A_11 + A_12 - A_21 - A_22) * B_22
        /// M_7 := A_22 * (-B_11 + B_12 + B_21 - B_22)
        /// 
        /// The result of the multiplication is calculated as follows:
        /// 
        /// result_11 := M_1 + M_2
        /// result_12 := M_1 + M_3 + M_5 + M_6
        /// result_21 := M_1 + M_3 + M_4 + M_7
        /// result_22 := M_1 + M_3 + M_4 + M_5
        /// 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="result"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        private void mult_winograd(T[][] A, T[][] B, T[][] result, int a, int b, int c)
        {
            int half_a = a / 2, half_b = b / 2, half_c = c / 2;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(a, b, c))
            {
                _instruction.multiply_naive(A, B, _naive_mult_workspace, result, a, c, b, false);
                return;
            }

            T[][] tempA = _tempAs[half_a];
            T[][] tempB = _tempBs[half_b];
            T[][] M = _Ms[half_a];
            T[][] tempResult = _tempResults[a];

            //_instruction.clear(tempResult, a, c);

            T[][] A_3 = _A_3s[half_a];
            T[][] B_3 = _B_3s[half_b];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[half_a];

            // M_1 := A_11 * B_11
            Copy(tempA, A, 0, 0, half_a, half_b);
            Copy(tempB, B, 0, 0, half_b, half_c);
            mult_winograd(tempA, tempB, M_temp, half_a, half_b, half_c);
            Copy(tempResult, M_temp, 0, 0, half_a, half_c); // C_11 = M_1

            // M_2 := A_12 * B_21, C11 := M_1 + M_2
            Copy(tempA, A, 0, half_b, half_a, half_b);
            Copy(tempB, B, half_b, 0, half_b, half_c);
            mult_winograd(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.increment(tempResult, M, 0, 0, half_a, half_c); // C_11 = M_1 + M_2, complete

            // A_3 := A_21 + A_22
            _instruction.add(A, A, A_3, half_a, 0, half_a, half_b, half_a, half_b);
            // B_3 := B_12 - B_11
            _instruction.subtract(B, B, B_3, 0, half_c, 0, 0, half_a, half_b);

            // M_5 := A_3 * B_3
            mult_winograd(A_3, B_3, M, half_a, half_b, half_c);
            Copy(tempResult, M, 0, half_b, 0, 0, half_a, half_b); // C_12 = M_5
            Copy(tempResult, M, half_a, half_b, 0, 0, half_a, half_b); // C_22 = M_5

            // B_3 := B_22 - B_3 (B_3 := B_11 - B_12 + B_22)
            _instruction.subtract(B, B_3, B_3, half_b, half_c, 0, 0, half_b, half_c);
            // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
            _instruction.decrement(A_3, A, 0, 0, half_a, half_b);

            // M_3 := A_3 * B_3
            mult_winograd(A_3, B_3, M, half_a, half_b, half_c);
            _instruction.increment(M_temp, M, 0, 0, half_a, half_c);
            _instruction.increment(tempResult, M_temp, 0, half_c, half_a, half_c); // C_12 = M_5 + (M_1 + M_3)

            // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
            _instruction.subtract(A, A_3, A_3, 0, half_b, 0, 0, half_a, half_b);
            // M_6 := A_3 * B_22
            Copy(tempB, B, half_b, half_c, half_b, half_c);
            mult_winograd(A_3, tempB, M, half_a, half_b, half_c);
            _instruction.increment(tempResult, M, 0, half_c, half_a, half_c); // C_12 = (M_1 + M_3) + M_5 + M_6, complete

            // B_3 := B_21 - B_3 (B_3 := -B_11 + B_12 + B_21 - B_22)
            _instruction.subtract(B, B_3, B_3, half_b, 0, 0, 0, half_b, half_c);
            // M_7 := A_22 * B_3;
            Copy(tempA, A, half_a, half_b, half_a, half_b);
            mult_winograd(tempA, B_3, M, half_a, half_b, half_c);
            Copy(tempResult, M, half_a, 0, 0, 0, half_a, half_c);

            // A_3 := A_11 - A_21
            _instruction.subtract(A, A, A_3, 0, 0, half_a, 0, half_a, half_b);
            // B_3 := B_22 - B_12
            _instruction.subtract(B, B, B_3, half_b, half_c, 0, half_c, half_b, half_c);
            // M_4 := A_3 * B_3
            mult_winograd(A_3, B_3, M, half_a, half_b, half_c);
            _instruction.increment(M_temp, M, 0, 0, half_a, half_c);
            _instruction.increment(tempResult, M_temp, half_a, 0, half_a, half_c); // C_21 = M_7 + (M_1 + M_3 + M_4), complete
            _instruction.increment(tempResult, M_temp, half_a, half_c, half_a, half_c); // C_22 = M_5 + (M_1 + M_3 + M_4), complete

            Copy(result, tempResult, 0, 0, a, c);
        }

        /// <summary>
        /// Second parallel implementation that parallelizes within each instruction
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="result"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        private void mult_winograd_parallel(T[][] A, T[][] B, T[][] result, int a, int b, int c)
        {
            int half_a = a / 2, half_b = b / 2, half_c = c / 2;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(a, b, c))
            {
                _instruction.multiply_naive_parallel(A, B, _naive_mult_workspace, result, a, c, b);
                return;
            }

            T[][] tempA = _tempAs[half_a];
            T[][] tempB = _tempBs[half_b];
            T[][] M = _Ms[half_a];
            T[][] tempResult = _tempResults[a];

            //_instruction.clear(tempResult, a, c);

            T[][] A_3 = _A_3s[half_a];
            T[][] B_3 = _B_3s[half_b];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[half_a];

            // M_1 := A_11 * B_11
            CopyParallel(tempA, A, 0, 0, 0, 0, half_a, half_b);
            CopyParallel(tempB, B, 0, 0, 0, 0, half_b, half_c);
            mult_winograd_parallel(tempA, tempB, M_temp, half_a, half_b, half_c);
            CopyParallel(tempResult, M_temp, 0, 0, 0, 0, half_a, half_c); // C_11 = M_1

            // M_2 := A_12 * B_21, C11 := M_1 + M_2
            CopyParallel(tempA, A, 0, 0, 0, half_b, half_a, half_b);
            CopyParallel(tempB, B, 0, 0, half_b, 0, half_b, half_c);
            mult_winograd_parallel(tempA, tempB, M, half_a, half_b, half_c);
            _instruction.increment_parallel(tempResult, M, 0, 0, half_a, half_c); // C_11 = M_1 + M_2, complete

            // A_3 := A_21 + A_22
            _instruction.add_parallel(A, A, A_3, half_a, 0, half_a, half_b, half_a, half_b);
            // B_3 := B_12 - B_11
            _instruction.subtract_parallel(B, B, B_3, 0, half_c, 0, 0, half_a, half_b);

            // M_5 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            CopyParallel(tempResult, M, 0, half_b, 0, 0, half_a, half_b); // C_12 = M_5
            CopyParallel(tempResult, M, half_a, half_b, 0, 0, half_a, half_b); // C_22 = M_5

            // B_3 := B_22 - B_3 (B_3 := B_11 - B_12 + B_22)
            _instruction.subtract_parallel(B, B_3, B_3, half_b, half_c, 0, 0, half_b, half_c);
            // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
            _instruction.decrement_parallel(A_3, A, 0, 0, half_a, half_b);

            // M_3 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            _instruction.increment_parallel(M_temp, M, 0, 0, half_a, half_c);
            _instruction.increment_parallel(tempResult, M_temp, 0, half_c, half_a, half_c); // C_12 = M_5 + (M_1 + M_3)

            // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
            _instruction.subtract_parallel(A, A_3, A_3, 0, half_b, 0, 0, half_a, half_b);
            // M_6 := A_3 * B_22
            CopyParallel(tempB, B, 0, 0, half_b, half_c, half_b, half_c);
            mult_winograd_parallel(A_3, tempB, M, half_a, half_b, half_c);
            _instruction.increment_parallel(tempResult, M, 0, half_c, half_a, half_c); // C_12 = (M_1 + M_3) + M_5 + M_6, complete

            // B_3 := B_21 - B_3 (B_3 := -B_11 + B_12 + B_21 - B_22)
            _instruction.subtract_parallel(B, B_3, B_3, half_b, 0, 0, 0, half_b, half_c);
            // M_7 := A_22 * B_3;
            CopyParallel(tempA, A, 0, 0, half_a, half_b, half_a, half_b);
            mult_winograd_parallel(tempA, B_3, M, half_a, half_b, half_c);
            CopyParallel(tempResult, M, half_a, 0, 0, 0, half_a, half_c);

            // A_3 := A_11 - A_21
            _instruction.subtract_parallel(A, A, A_3, 0, 0, half_a, 0, half_a, half_b);
            // B_3 := B_22 - B_12
            _instruction.subtract_parallel(B, B, B_3, half_b, half_c, 0, half_c, half_b, half_c);
            // M_4 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            _instruction.increment_parallel(M_temp, M, 0, 0, half_a, half_c);
            _instruction.increment_parallel(tempResult, M_temp, half_a, 0, half_a, half_c); // C_21 = M_7 + (M_1 + M_3 + M_4), complete
            _instruction.increment_parallel(tempResult, M_temp, half_a, half_c, half_a, half_c); // C_22 = M_5 + (M_1 + M_3 + M_4), complete

            CopyParallel(result, tempResult, 0, 0, 0, 0, a, c);
        }

        /// <summary>
        /// The original parallel implementation of Strassen's.. not used for now because it does not match the 
        /// other implementation's performance
        /// </summary>
        private void mult_winograd_parallel_ss(T[][] A, T[][] B, T[][] result, int a, int b, int c)
        {
            int half_a = a / 2, half_b = b / 2, half_c = c / 2;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(a, b, c))
            {
                _instruction.multiply_naive_parallel(A, B, _naive_mult_workspace, result, a, c, b);
                return;
            }

            T[][] tempA = _tempAs[half_a];
            T[][] tempB = _tempBs[half_b];
            T[][] M = _Ms[half_a];
            T[][] tempResult = _tempResults[a];

            //_instruction.clear(tempResult, a, c);

            T[][] A_3 = _A_3s[half_a];
            T[][] B_3 = _B_3s[half_b];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[half_a];

            // M_1 := A_11 * B_11
            Parallel.Invoke(
                () => Copy(tempA, A, 0, 0, half_a, half_b),
                () => Copy(tempB, B, 0, 0, half_b, half_c)
                );

            mult_winograd_parallel(tempA, tempB, M_temp, half_a, half_b, half_c);
            Parallel.Invoke(
                () => Copy(tempResult, M_temp, 0, 0, half_a, half_c), // C_11 = M_1
                () => Copy(tempA, A, 0, half_b, half_a, half_b),
                () => Copy(tempB, B, half_b, 0, half_b, half_c)
                );

            // M_2 := A_12 * B_21, C11 := M_1 + M_2
            mult_winograd_parallel(tempA, tempB, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => _instruction.increment(tempResult, M, 0, 0, half_a, half_c), // C_11 = M_1 + M_2, complete
                () => _instruction.add(A, A, A_3, half_a, 0, half_a, half_b, half_a, half_b), // A_3 := A_21 + A_22
                () => _instruction.subtract(B, B, B_3, 0, half_c, 0, 0, half_a, half_b) // B_3 := B_12 - B_11
                );

            // M_5 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => Copy(tempResult, M, 0, half_b, 0, 0, half_a, half_b), // C_12 = M_5
                () => Copy(tempResult, M, half_a, half_b, 0, 0, half_a, half_b), // C_22 = M_5
                () => _instruction.subtract(B, B_3, B_3, half_b, half_c, 0, 0, half_b, half_c), // B_3 := B_22 - B_3 (B_3 := B_11 - B_12 + B_22)
                () => _instruction.decrement(A_3, A, 0, 0, half_a, half_b) // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
                );

            // M_3 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => _instruction.increment(M_temp, M, 0, 0, half_a, half_c),
                () => _instruction.increment(tempResult, M_temp, 0, half_c, half_a, half_c), // C_12 = M_5 + (M_1 + M_3)
                () => _instruction.subtract(A, A_3, A_3, 0, half_b, 0, 0, half_a, half_b), // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
                () => Copy(tempB, B, half_b, half_c, half_b, half_c) // M_6 := A_3 * B_22
                );

            mult_winograd_parallel(A_3, tempB, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => _instruction.increment(tempResult, M, 0, half_c, half_a, half_c), // C_12 = (M_1 + M_3) + M_5 + M_6, complete
                () => _instruction.subtract(B, B_3, B_3, half_b, 0, 0, 0, half_b, half_c), // B_3 := B_21 - B_3 (B_3 := -B_11 + B_12 + B_21 - B_22)
                () => Copy(tempA, A, half_a, half_b, half_a, half_b) // M_7 := A_22 * B_3;
                );

            mult_winograd_parallel(tempA, B_3, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => Copy(tempResult, M, half_a, 0, 0, 0, half_a, half_c),
                () => _instruction.subtract(A, A, A_3, 0, 0, half_a, 0, half_a, half_b), // A_3 := A_11 - A_21
                () => _instruction.subtract(B, B, B_3, half_b, half_c, 0, half_c, half_b, half_c) // B_3 := B_22 - B_12
                );

            // M_4 := A_3 * B_3
            mult_winograd_parallel(A_3, B_3, M, half_a, half_b, half_c);
            Parallel.Invoke(
                () => _instruction.increment(M_temp, M, 0, 0, half_a, half_c),
                () => _instruction.increment(tempResult, M_temp, half_a, 0, half_a, half_c), // C_21 = M_7 + (M_1 + M_3 + M_4), complete
                () => _instruction.increment(tempResult, M_temp, half_a, half_c, half_a, half_c), // C_22 = M_5 + (M_1 + M_3 + M_4), complete
                () => Copy(result, tempResult, 0, 0, a, c)
                );
        }

    }
}
