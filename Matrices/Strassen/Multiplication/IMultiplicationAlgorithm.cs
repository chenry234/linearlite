﻿using LinearLite.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices
{
    public abstract class IMultiplicationAlgorithm<T> where T : new()
    {
        internal bool BTransposed { get; set; }

        protected IMultiplicationAlgorithm(bool BTransposed)
        {
            this.BTransposed = BTransposed;
        }

        public DenseMatrix<T> Multiply(DenseMatrix<T> A, DenseMatrix<T> B)
        {
            MatrixChecks.CheckNotNull(A, B);
            DenseMatrix<T> result;
            if (BTransposed)
            {
                result = new DenseMatrix<T>(A.Rows, B.Rows);
            }
            else
            {
                result = new DenseMatrix<T>(A.Rows, B.Columns);
            }
            Multiply(A, B, result);
            return result;
        }
        public T[,] Multiply(T[,] A, T[,] B)
        {
            MatrixChecks.CheckNotNull(A, B);
            T[,] result;
            if (BTransposed)
            {
                result = new T[A.GetLength(0), B.GetLength(0)];
            }
            else
            {
                result = new T[A.GetLength(0), B.GetLength(1)];
            }
            Multiply(A, B, result);
            return result;
        }
        public T[][] Multiply(T[][] A, T[][] B)
        {
            MatrixChecks.CheckNotNull(A, B);
            T[][] result;
            if (BTransposed) 
            {
                result = MatrixInternalExtensions.JMatrix<T>(A.Length, B.Length);
            }
            else
            {
                result = MatrixInternalExtensions.JMatrix<T>(A.Length, B[0].Length);
            }
            Multiply(A, B, result);
            return result;
        }

        public void Multiply(DenseMatrix<T> A, DenseMatrix<T> B, DenseMatrix<T> result)
        {
            MatrixChecks.CheckNotNull(A, B, result);
            MatrixChecks.CheckMatrixDimensionsForMultiplication(A, B, result, BTransposed);

            Multiply(A.Values, B.Values, result.Values, A.Rows, A.Columns, result.Columns);
        }
        public void Multiply(T[,] A, T[,] B, T[,] result)
        {
            MatrixChecks.CheckNotNull(A, B, result);
            MatrixChecks.CheckMatrixDimensionsForMultiplication(A, B, result, BTransposed);

            T[][] _A = A.ToJagged();
            T[][] _B = B.ToJagged();
            T[][] _result = result.ToJagged();

            int m = A.GetLength(0), n = A.GetLength(1), p = result.GetLength(1), i, j;
            Multiply(_A, _B, _result, m, n, p);

            for (i = 0; i < m; ++i)
            {
                T[] _result_i = _result[i];
                for (j = 0; j < p; ++j)
                {
                    result[i, j] = _result_i[j];
                }
            }
        }
        public void Multiply(T[][] A, T[][] B, T[][] result)
        {
            MatrixChecks.CheckNotNull(A, B, result);
            MatrixChecks.CheckMatrixDimensionsForMultiplication(A, B, result, BTransposed);
            Multiply(A, B, result, A.Length, A[0].Length, result[0].Length);
        }

        internal abstract void Multiply(T[][] A, T[][] B, T[][] result, int m, int n, int p);

        /// <summary>
        /// <rows, N, cols> block-matrix multiplication problem.
        /// </summary>
        /// <param name="A">The 1st matrix to be multiplied.</param>
        /// <param name="B">The 2nd matrix to be multiplied.</param>
        /// <param name="result"></param>
        /// <param name="A_row_offset">The first row of matrix A to start multiplying</param>
        /// <param name="A_col_offset">The first col of matrix A to start multiplying</param>
        /// <param name="B_row_offset">The first row of matrix B to start multiplying</param>
        /// <param name="B_col_offset">The first col of matrix B to start multiplying</param>
        /// <param name="C_row_offset">The first row of the result matrix to store the results</param>
        /// <param name="C_col_offset">The first col of the result matrix to store the results</param>
        /// <param name="m">The number of rows in the resulting multiplied block</param>
        /// <param name="n">The number of cols in the resulting multiplied block</param>
        /// <param name="p">The numebr of cols in A /or rows in B to multiply together</param>
        /// <param name"preserve_A">If true, A must be preserved (small performance + memory penalty)</param>
        /// <param name"preserve_B">If true, B must be preserved (small performance + memory penalty)</param>
        /// <param name"preserve_result">If true, result must be unchanged except for the specified block</param>
        internal abstract void Multiply(T[][] A, T[][] B, T[][] result,
            int A_row_offset, int A_col_offset,
            int B_row_offset, int B_col_offset,
            int C_row_offset, int C_col_offset,
            int m, int n, int p, bool increment);

        // Not used
        private void MultiplyUsingCanonicalForm(T[][] A, T[][] B, T[][] result,
            int A_row_offset, int A_col_offset,
            int B_row_offset, int B_col_offset,
            int C_row_offset, int C_col_offset,
            int rows, int cols, int N,
            bool preserve_A, bool preserve_B, bool preserve_result)
        {
            MatrixChecks.CheckNotNull(A, B, result);

            T[][] A_workspace;
            if (preserve_A)
            {
                A_workspace = MatrixInternalExtensions.JMatrix<T>(rows, N);
                Copy(A_workspace, A, A_row_offset, A_col_offset, rows, N);
            }
            else
            {
                A_workspace = A;
                Shift(A, A_row_offset, A_col_offset, rows, N, -A_row_offset, -A_col_offset);
            }

            T[][] B_workspace;
            if (preserve_B)
            {
                B_workspace = MatrixInternalExtensions.JMatrix<T>(N, cols);
                Copy(B_workspace, B, B_row_offset, B_col_offset, N, cols);
            }
            else
            {
                B_workspace = B;
                Shift(B, B_row_offset, B_col_offset, N, cols, -B_row_offset, -B_col_offset);
            }

            if (preserve_result)
            {
                T[][] result_workspace = MatrixInternalExtensions.JMatrix<T>(rows, cols);
                Multiply(A_workspace, B_workspace, result_workspace, rows, N, cols);
                Copy(result, result_workspace, C_row_offset, C_col_offset, 0, 0, rows, cols);
            }
            else
            {
                Multiply(A_workspace, B_workspace, result, rows, N, cols);
                Shift(result, 0, 0, rows, cols, C_row_offset, C_col_offset);
            }
        }

        /// <summary>
        /// Shift a submatrix block of size 'rows' x 'columns', 
        /// starting at 'row_start' and 'col_start',
        /// 'row_shift' rows down and 'col_shift' columns right
        /// 
        /// Currently unused
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="rowShift"></param>
        /// <param name="colShift"></param>
        private void Shift(T[][] matrix, int row_start, int col_start, int rows, int columns, int row_shift, int col_shift)
        {
            if (row_shift == 0)
            {
                // row_shift = col_shift = 0, do nothing
                if (col_shift == 0)
                {
                    return;
                }

                int row_end = row_start + rows, col_end = col_start + columns, i, j;
                if (col_shift < 0)
                {
                    // row_shift = 0, col_shift < 0
                    for (i = row_start; i < row_end; ++i)
                    {
                        T[] row = matrix[i];
                        for (j = col_start; j < col_end; ++j)
                        {
                            row[j + col_shift] = row[j];
                        }
                    }
                }
                else
                {
                    // row_shift = 0, col_shift > 0
                    int start = col_end - 1;
                    for (i = row_start; i < row_end; ++i)
                    {
                        T[] row = matrix[i];
                        for (j = start; j >= col_start; --j)
                        {
                            row[j + col_shift] = row[j];
                        }
                    }
                }
            }

            // row_shift != 0, col_shift = 0
            else if (col_shift == 0)
            {
                int row_end = row_start + rows, col_end = col_start + columns, i, j;
                if (row_shift < 0)
                {
                    for (i = row_start; i < row_end; ++i)
                    {
                        T[] row_src = matrix[i], row_dest = matrix[i + row_shift];
                        for (j = col_start; j < col_end; ++j)
                        {
                            row_dest[j] = row_src[j];
                        }
                    }
                }
                else
                {
                    // row_shift > 0, col_shift = 0
                    int begin = row_end - 1;
                    for (i = begin; i >= row_end; --i)
                    {
                        T[] row_src = matrix[i], row_dest = matrix[i + row_shift];
                        for (j = col_start; j < col_end; ++j)
                        {
                            row_dest[j] = row_src[j];
                        }
                    }
                }
            }

            // row_shift != 0 and col_shift != 0
            else
            {
                int row_end = row_start + rows, col_end = col_start + columns, i, j;
                if (row_shift < 0)
                {
                    int last = col_end - 1;
                    for (i = row_start; i < row_end; ++i)
                    {
                        T[] row_src = matrix[i], row_dest = matrix[i + row_shift];
                        if (col_shift < 0)
                        {
                            for (j = col_start; j < col_end; ++j)
                            {
                                row_dest[j + col_shift] = row_src[j];
                            }
                        }
                        else
                        {
                            for (j = last; j >= col_start; --j)
                            {
                                row_dest[j + col_shift] = row_src[j];
                            }
                        }
                    }
                }
                else
                {
                    // row_shift > 0, col_shift = 0
                    int last = row_end - 1;
                    for (i = last; i >= row_end; --i)
                    {
                        T[] row_src = matrix[i], row_dest = matrix[i + row_shift];
                        if (col_shift < 0)
                        {
                            for (j = col_start; j < col_end; ++j)
                            {
                                row_dest[j + col_shift] = row_src[j];
                            }
                        }
                        else
                        {
                            for (j = last; j >= col_start; --j)
                            {
                                row_dest[j + col_shift] = row_src[j];
                            }
                        }
                    }
                }
            }
        }

        protected void Copy(T[][] dest, T[][] src, int srcFirstRow, int srcFirstCol, int rows, int cols)
        {
            int r, c;
            if (srcFirstRow == 0 && srcFirstCol == 0)
            {
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[r], s = src[r];
                    for (c = 0; c < cols; ++c)
                    {
                        d[c] = s[c];
                    }
                }
            }
            else
            {
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[r], s = src[srcFirstRow + r];
                    for (c = 0; c < cols; ++c)
                    {
                        d[c] = s[srcFirstCol + c];
                    }
                }
            }
        }
        protected void Copy(T[][] dest, T[][] src, int destFirstRow, int destFirstCol, int srcFirstRow, int srcFirstCol, int rows, int cols)
        {
            int r, c, k;
            if (srcFirstRow == 0 && srcFirstCol == 0)
            {
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[destFirstRow + r], s = src[r];
                    for (c = 0, k = destFirstCol; c < cols; ++c, ++k)
                    {
                        d[k] = s[c];
                    }
                }
            }
            else
            {
                int end = cols + srcFirstCol;
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[destFirstRow + r], s = src[srcFirstRow + r];
                    for (c = srcFirstCol, k = destFirstCol; c < end; ++c, ++k)
                    {
                        d[k] = s[c];
                    }
                }
            }
        }
        protected void CopyParallel(T[][] dest, T[][] src, int destFirstRow, int destFirstCol, int srcFirstRow, int srcFirstCol, int rows, int cols)
        {
            if (destFirstRow == 0 && destFirstCol == 0)
            {
                if (srcFirstRow == 0 && srcFirstCol == 0)
                {
                    Parallel.ForEach(Partitioner.Create(0, rows), range =>
                    {
                        int min = range.Item1, max = range.Item2, r, c;
                        for (r = min; r < max; ++r)
                        {
                            T[] d = dest[r], s = src[r];
                            for (c = 0; c < cols; ++c)
                            {
                                d[c] = s[c];
                            }
                        }
                    });
                }
                else
                {
                    Parallel.ForEach(Partitioner.Create(0, rows), range =>
                    {
                        int min = range.Item1, max = range.Item2, r, c;
                        for (r = min; r < max; ++r)
                        {
                            T[] d = dest[r], s = src[srcFirstRow + r];
                            for (c = 0; c < cols; ++c)
                            {
                                d[c] = s[srcFirstCol + c];
                            }
                        }
                    });
                }
            }
            else
            {
                if (srcFirstRow == 0 && srcFirstCol == 0)
                {
                    Parallel.ForEach(Partitioner.Create(0, rows), range =>
                    {
                        int min = range.Item1, max = range.Item2, r, c, k;
                        for (r = min; r < max; ++r)
                        {
                            T[] d = dest[destFirstRow + r], s = src[r];
                            for (c = 0, k = destFirstCol; c < cols; ++c, ++k)
                            {
                                d[k] = s[c];
                            }
                        }
                    });
                }
                else
                {
                    int end = cols + srcFirstCol;
                    Parallel.ForEach(Partitioner.Create(0, rows), range =>
                    {
                        int min = range.Item1, max = range.Item2, r, c, k;
                        for (r = min; r < max; ++r)
                        {
                            T[] d = dest[destFirstRow + r], s = src[srcFirstRow + r];
                            for (c = srcFirstCol, k = destFirstCol; c < end; ++c, ++k)
                            {
                                d[k] = s[c];
                            }
                        }
                    });
                }
            }
        }
    }
}
