﻿using LinearLite.Geometry.Tiling;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Strassen
{
    /// <summary>
    /// The naive (exact size) implementation of Strassen's algorithm for rectangular matrices suffers from 
    /// 'level skipping' when the sizes of the initialized problem do not exactly match those of the matrices to be multiplied. 
    /// This implementation circumvents that problem by using tile multiplication via smaller square Strassen's algorithm.
    /// </summary>
    internal class IndefiniteSizeRectStrassenMultiplication<T> : IMultiplicationAlgorithm<T> where T : new()
    {
        private int _naiveMultiplicationThreshold;
        private IStrassenInstructionSet<T> _instruction;
        private SquareStrassenMultiplication<T> _squareMultiplier;
        private List<int> _registerSizesInc;
        private ITilingStrategy _tilingStrategy;

        /// <summary>
        /// Set up a Strassen's matrix multiplication problem.
        /// </summary>
        /// <param name="instructions">The instruction set to use - availble for most primitive types and Field<T></param>
        /// <param name="m">The max. number of rows of the 1st matrix to be multiplied.</param>
        /// <param name="n">The max number of columns of the 1st matrix to be multiplied.</param>
        /// <param name="p">The max number of columns of the 2nd matrix to be multiplied.</param>
        /// <param name="useWinogradVariant">If true, uses the Winograd variant of Strassen's algorithm (15 addition/subtractions instead of 18)</param>
        /// <param name="naiveMultThreshold">The matrix dimensions under which we revert back to regular matrix multiplication.</param>
        internal IndefiniteSizeRectStrassenMultiplication(
            IStrassenInstructionSet<T> instructions, 
            int m, int n, int p, 
            bool useWinogradVariant = false, 
            int naiveMultThreshold = 32, 
            bool Btransposed = false)
            : base(Btransposed)
        {
            _instruction = instructions;

            // Round down to the nearest power of 2 (for efficiency)
            int power2 = 1 << (int)(Math.Log(Util.Min(m, n, p)) / Math.Log(2));
            _squareMultiplier = new SquareStrassenMultiplication<T>(instructions, power2, useWinogradVariant, naiveMultThreshold, Btransposed);

            _registerSizesInc = _squareMultiplier.GetRegisterSizesInIncreasingOrder();
            _naiveMultiplicationThreshold = naiveMultThreshold;
            _tilingStrategy = new GreedyTilingStrategy(_registerSizesInc);
        }
        private bool use_naive_algorithm(int m, int n, int p)
        {
            return m < _naiveMultiplicationThreshold || n < _naiveMultiplicationThreshold || p < _naiveMultiplicationThreshold;
        }

        internal override void Multiply(T[][] A, T[][] B, T[][] result, int m, int n, int p)
        {
            Multiply(A, B, result, 0, 0, 0, 0, 0, 0, m, n, p, false);
        }
        internal override void Multiply(T[][] A, T[][] B, T[][] result, 
            int A_row_start, int A_col_start, 
            int B_row_start, int B_col_start,
            int C_row_start, int C_col_start,
            int m, int n, int p, bool increment)
        {
            if (use_naive_algorithm(m, n, p))
            {
                if (_squareMultiplier.BTransposed)
                {
                    _instruction.multiply_naive_transposed(
                        A, B, result,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        C_row_start, C_col_start,
                        m, p, n, increment);
                }
                else
                {
                    _instruction.multiply_naive(
                        A, B, result,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        C_row_start, C_col_start,
                        m, p, n, increment);
                }
                return;
            }

            SpatialPartition3D tiling = _tilingStrategy.Partition(0, 0, 0, m, n, p);

            // Because we are incrementing, need a once-off clear of the result space.
            if (!increment)
            {
                _instruction.clear(result, C_row_start, C_col_start, m, p);
            }

            foreach (Tile3D t in tiling.Tiles)
            {
                if (t.IsRemainder)
                {
                    if (_squareMultiplier.BTransposed)
                    {
                        _instruction.multiply_naive_transposed(
                            A, B, result,
                            A_row_start + t.X, A_col_start + t.Y,
                            B_row_start + t.Y, B_col_start + t.Z,
                            C_row_start + t.X, C_col_start + t.Z,
                            t.LengthX, t.LengthZ, t.LengthY, true);
                    }
                    else
                    {
                        _instruction.multiply_naive(
                            A, B, result,
                            A_row_start + t.X, A_col_start + t.Y,
                            B_row_start + t.Y, B_col_start + t.Z,
                            C_row_start + t.X, C_col_start + t.Z,
                            t.LengthX, t.LengthZ, t.LengthY, true);
                    }
                }
                else
                {
                    _squareMultiplier.Multiply(
                        A, B, result,
                        A_row_start + t.X, A_col_start + t.Y,
                        B_row_start + t.Y, B_col_start + t.Z,
                        C_row_start + t.X, C_col_start + t.Z, 
                        t.LengthX, t.LengthX, t.LengthX, true);
                }
            }
        }
    }
}
