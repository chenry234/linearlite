﻿using LinearLite.Structs;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Strassen
{
    internal class SquareStrassenMultiplication<T> : IMultiplicationAlgorithm<T> where T: new()
    {
        private readonly bool _transposeB;
        private readonly bool _useWinogradVariant;
        private readonly int _naiveMultiplicationThreshold;
        private readonly Dictionary<int, T[][]> _tempAs, _tempBs, _Ms, _tempResults;
        private readonly Dictionary<int, T[][]> _A_3s, _tempMs;
        private readonly T[][] _naive_mult_workspace;

        private readonly IStrassenInstructionSet<T> _instruction;

        /// <summary>
        /// Set up a Strassen's matrix multiplication problem.
        /// </summary>
        /// <param name="instructions">The instruction set to use - availble for most primitive types and Field<T></param>
        /// <param name="size">The max. number of rows of the 1st matrix to be multiplied.</param>
        /// <param name="n">The max number of columns of the 1st matrix to be multiplied.</param>
        /// <param name="p">The max number of columns of the 2nd matrix to be multiplied.</param>
        /// <param name="useWinogradVariant">If true, uses the Winograd variant of Strassen's algorithm (15 addition/subtractions instead of 18)</param>
        /// <param name="naiveMultThreshold">The matrix dimensions under which we revert back to regular matrix multiplication.</param>
        /// <param name="BTransposed">
        /// If true, then the matrix B (the 2nd matrix to be multiplied) is provided in transposed form. This speeds up the naive 
        /// matrix multiplication step.
        /// </param>
        internal SquareStrassenMultiplication(
            IStrassenInstructionSet<T> instructions, 
            int size, 
            bool useWinogradVariant = false, 
            int naiveMultThreshold = 32, 
            bool BTransposed = false)
            : base(BTransposed)
        {
            _instruction = instructions;
            _transposeB = BTransposed;
            _useWinogradVariant = useWinogradVariant;
            _naiveMultiplicationThreshold = naiveMultThreshold;

            if (naiveMultThreshold > 0)
            {
                _naive_mult_workspace = MatrixInternalExtensions.JMatrix<T>(naiveMultThreshold, naiveMultThreshold);
            }

            // Pre-calculate the blocks
            _tempAs = new Dictionary<int, T[][]>();
            _tempBs = new Dictionary<int, T[][]>();
            _Ms = new Dictionary<int, T[][]>();
            _tempResults = new Dictionary<int, T[][]>();

            if (_useWinogradVariant)
            {
                _A_3s = new Dictionary<int, T[][]>();
                _tempMs = new Dictionary<int, T[][]>();
            }

            while (!use_naive_algorithm(size))
            {
                _tempResults[size] = MatrixInternalExtensions.JMatrix<T>(size, size);

                size /= 2;

                _tempAs[size] = MatrixInternalExtensions.JMatrix<T>(size, size);
                _tempBs[size] = MatrixInternalExtensions.JMatrix<T>(size, size);
                _Ms[size] = MatrixInternalExtensions.JMatrix<T>(size, size);

                if (_useWinogradVariant)
                {
                    _A_3s[size] = MatrixInternalExtensions.JMatrix<T>(size, size);
                    _tempMs[size] = MatrixInternalExtensions.JMatrix<T>(size, size);
                }
            }
        }

        private bool use_naive_algorithm(int m)
        {
            return m < _naiveMultiplicationThreshold;
        }
        internal List<int> GetRegisterSizesInIncreasingOrder()
        {
            return _tempResults.Keys.OrderBy(i => i).ToList();
        }

        internal override void Multiply(T[][] A, T[][] B, T[][] result, int m, int n, int p)
        {
            Multiply(A, B, result, 0, 0, 0, 0, 0, 0, m, n, p, false);
        }
        internal override void Multiply(T[][] A, T[][] B, T[][] result, 
            int A_row_start, int A_col_start, 
            int B_row_start, int B_col_start, 
            int result_row_start, int result_col_start, 
            int m, int n, int p, bool increment)
        {
            if (m != n || n != p)
            {
                throw new InvalidOperationException();
            }

            if (_useWinogradVariant)
            {
                if (_transposeB)
                {
                    mult_winograd_transposed(A, B, result, A_row_start, A_col_start, B_row_start, B_col_start, result_row_start, result_col_start, m, increment);
                }
                else
                {
                    mult_winograd(A, B, result, A_row_start, A_col_start, B_row_start, B_col_start, result_row_start, result_col_start, m, increment);
                }
            }
            else
            {
                if (_transposeB)
                {
                    throw new NotImplementedException();
                }
                else
                {
                    mult_strassen(A, B, result, A_row_start, A_col_start, B_row_start, B_col_start, result_row_start, result_col_start, m, increment);
                }
            }
        }

        private void mult_strassen(T[][] A, T[][] B, T[][] result, 
            int A_row_start, int A_col_start, 
            int B_row_start, int B_col_start, 
            int result_row_start, int result_col_start, int size, bool increment)
        {
            int hsize = size / 2,
                A_row_half = A_row_start + hsize,
                A_col_half = A_col_start + hsize,
                B_row_half = B_row_start + hsize,
                B_col_half = B_col_start + hsize;

            bool is_simple = 
                A_row_start == 0 && A_col_start == 0 &&
                B_row_start == 0 && B_col_start == 0 &&
                result_row_start == 0 && result_col_start == 0;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(size))
            {
                if (is_simple)
                {
                    _instruction.multiply_naive(A, B, _naive_mult_workspace, result, size, size, size, increment);
                }
                else
                {
                    _instruction.multiply_naive(A, B, _naive_mult_workspace, result,
                        A_row_start, A_col_start, 
                        B_row_start, B_col_start, 
                        result_row_start, result_col_start, 
                        size, size, size, increment);
                }
                return;
            }

            T[][] tempA = _tempAs[hsize];
            T[][] tempB = _tempBs[hsize];
            T[][] M = _Ms[hsize];
            T[][] tempResult = _tempResults[size];

            _instruction.clear(tempResult, 0, 0, size, size);

            // M_1 = (A_11 + A_22)(B_11 + B_22)
            _instruction.add(A, A, tempA, A_row_start, A_col_start, A_row_half, A_col_half, hsize, hsize);
            _instruction.add(B, B, tempB, B_row_start, B_col_start, B_row_half, B_col_half, hsize, hsize);
            mult_strassen(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, 0, 0, hsize, hsize);
            _instruction.increment(tempResult, M, hsize, hsize, hsize, hsize);

            // M_2 = (A_21 + A_22)B_11
            _instruction.add(A, A, tempA, A_row_half, A_col_start, A_row_half, A_col_half, hsize, hsize);
            Copy(tempB, B, B_row_start, B_col_start, hsize, hsize);
            mult_strassen(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, hsize, 0, hsize, hsize);
            _instruction.decrement(tempResult, M, hsize, hsize, hsize, hsize);

            // M_3 = A_11(B_12 - B_22) 
            Copy(tempA, A, A_row_start, A_col_start, hsize, hsize);
            _instruction.subtract(B, B, tempB, B_row_start, B_col_half, B_row_half, B_col_half, hsize, hsize);
            mult_strassen(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, 0, hsize, hsize, hsize);
            _instruction.increment(tempResult, M, hsize, hsize, hsize, hsize);

            // M_4 = A_22(B_21 - B_11)
            Copy(tempA, A, A_row_half, A_col_half, hsize, hsize);
            _instruction.subtract(B, B, tempB, B_row_half, B_col_start, B_row_start, B_col_start, hsize, hsize);
            mult_strassen(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, 0, 0, hsize, hsize);
            _instruction.increment(tempResult, M, hsize, 0, hsize, hsize);

            // M_5 = (A_11 + A_12)B_22
            _instruction.add(A, A, tempA, A_row_start, A_col_start, A_row_start, A_col_half, hsize, hsize);
            Copy(tempB, B, B_row_half, B_col_half, hsize, hsize);
            mult_strassen(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.decrement(tempResult, M, 0, 0, hsize, hsize);
            _instruction.increment(tempResult, M, 0, hsize, hsize, hsize);

            // M_6 = (A_21 - A_11)(B_11 + B_12)
            _instruction.subtract(A, A, tempA, A_row_half, A_col_start, A_row_start, A_col_start, hsize, hsize);
            _instruction.add(B, B, tempB, B_row_start, B_col_start, B_row_start, B_col_half, hsize, hsize);
            mult_strassen(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, hsize, hsize, hsize, hsize);

            // M_7 = (A_12 - A_22)(B_21 + B_22) 
            _instruction.subtract(A, A, tempA, A_row_start, A_col_half, A_row_half, A_col_half, hsize, hsize);
            _instruction.add(B, B, tempB, B_row_half, B_col_start, B_row_half, B_col_half, hsize, hsize);
            mult_strassen(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, 0, 0, hsize, hsize);

            // If size is odd, append the bottom row
            
            if (size % 2 == 1)
            {
                if (is_simple)
                {
                    _instruction.append_bottom_row(A, B, tempResult, size, size, size);
                    _instruction.append_top_left_submatrix(A, B, tempResult, size, size, size);
                    _instruction.append_right_column(A, B, tempResult, size, size, size);
                }
                else
                {
                    _instruction.append_bottom_row(A, B, tempResult, 
                        A_row_start, A_col_start, 
                        B_row_start, B_col_start, 
                        result_row_start, result_col_start, 
                        size, size, size);

                    _instruction.append_top_left_submatrix(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        result_row_start, result_col_start, 
                        size, size, size);
                        
                    _instruction.append_right_column(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        result_row_start, result_col_start, 
                        size, size, size);
                }
            }

            if (increment)
            {
                _instruction.increment(result, tempResult, result_row_start, result_col_start, size, size);
            }
            else
            {
                Copy(result, tempResult, result_row_start, result_col_start, 0, 0, size, size);
            }
        }

        /// <summary>
        /// Postmultiply A (a x b) by B (b x c), storing the result in 'result' (a x c).
        /// 
        /// This method uses the Winograd variant of Strassen's algorithm, which uses 15 additions
        /// instead of 18 (at the cost of higher memory consumption).
        /// 
        /// This implementation runs approximately 5% faster than the naive Strassen algorithm
        /// implementation, and consumes approximately twice the memory of the native 
        /// Strassen algorithm.
        /// 
        /// Algorithm: 
        /// With A_11, A_12, A_21, A_22, B_11, B_12, B_21, B_22 defined as the same submatrices as per
        /// Strassen, we define:
        /// 
        /// M_1 := A_11 * B_11
        /// M_2 := A_12 * B_21
        /// M_3 := (-A_11 + A_21 + A_22) * (B_11 - B_12 + B_22)
        /// M_4 := (A_11 - A_21) * (-B_12 + B_22)
        /// M_5 := (A_21 + A_22) * (-B_11 + B_12)
        /// M_6 := (A_11 + A_12 - A_21 - A_22) * B_22
        /// M_7 := A_22 * (-B_11 + B_12 + B_21 - B_22)
        /// 
        /// The result of the multiplication is calculated as follows:
        /// 
        /// result_11 := M_1 + M_2
        /// result_12 := M_1 + M_3 + M_5 + M_6
        /// result_21 := M_1 + M_3 + M_4 + M_7
        /// result_22 := M_1 + M_3 + M_4 + M_5
        /// 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <param name="result"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        private void mult_winograd(T[][] A, T[][] B, T[][] result,
            int A_row_start, int A_col_start,
            int B_row_start, int B_col_start,
            int result_row_start, int result_col_start, int size, bool increment)
        {
            int hsize = size / 2, 
                A_row_half = A_row_start + hsize,
                A_col_half = A_col_start + hsize,
                B_row_half = B_row_start + hsize,
                B_col_half = B_col_start + hsize;

            bool is_simple =
                A_row_start == 0 && A_col_start == 0 &&
                B_row_start == 0 && B_col_start == 0 &&
                result_row_start == 0 && result_col_start == 0;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(size))
            {
                if (is_simple)
                {
                    _instruction.multiply_naive(A, B, _naive_mult_workspace, result, size, size, size, increment);
                }
                else
                {
                    _instruction.multiply_naive(A, B, _naive_mult_workspace, result,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        result_row_start, result_col_start,
                        size, size, size, increment);
                }
                return;
            }

            T[][] tempA = _tempAs[hsize];
            T[][] M = _Ms[hsize];
            T[][] tempResult = _tempResults[size];
            T[][] A_3 = _A_3s[hsize];
            T[][] B_3 = _tempBs[hsize];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[hsize];

            // M_1 := A_11 * B_11
            mult_winograd(A, B, M_temp, A_row_start, A_col_start, B_row_start, B_col_start, 0, 0, hsize, false);
            Copy(tempResult, M_temp, 0, 0, hsize, hsize); // C_11 = M_1

            // M_2 := A_12 * B_21, C11 := M_1 + M_2
            mult_winograd(A, B, M, A_row_start, A_col_half, B_row_half, B_col_start, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, 0, 0, hsize, hsize); // C_11 = M_1 + M_2, complete

            // A_3 := A_21 + A_22
            _instruction.add(A, A, A_3, A_row_half, A_col_start, A_row_half, A_col_half, hsize, hsize);
            // B_3 := B_12 - B_11
            _instruction.subtract(B, B, B_3, B_row_start, B_col_half, B_row_start, B_col_start, hsize, hsize);

            // M_5 := A_3 * B_3
            mult_winograd(A_3, B_3, M, 0, 0, 0, 0, 0, 0, hsize, false);
            Copy(tempResult, M, 0,      hsize, 0, 0, hsize, hsize); // C_12 = M_5
            Copy(tempResult, M, hsize,  hsize, 0, 0, hsize, hsize); // C_22 = M_5

            // B_3 := B_22 - B_3 (B_3 := B_11 - B_12 + B_22)
            _instruction.subtract(B, B_3, B_3, B_row_half, B_col_half, 0, 0, hsize, hsize);
            // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
            Copy(tempA, A, A_row_start, A_col_start, hsize, hsize);
            _instruction.decrement(A_3, tempA, 0, 0, hsize, hsize);

            // M_3 := A_3 * B_3
            mult_winograd(A_3, B_3, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(M_temp, M, 0, 0, hsize, hsize);
            _instruction.increment(tempResult, M_temp, 0, hsize, hsize, hsize); // C_12 = M_5 + (M_1 + M_3)

            // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
            _instruction.subtract(A, A_3, A_3, A_row_start, A_col_half, 0, 0, hsize, hsize);
            // M_6 := A_3 * B_22
            mult_winograd(A_3, B, M, 0, 0, B_row_half, B_col_half, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, 0, hsize, hsize, hsize); // C_12 = (M_1 + M_3) + M_5 + M_6, complete

            // B_3 := B_21 - B_3 (B_3 := -B_11 + B_12 + B_21 - B_22)
            _instruction.subtract(B, B_3, B_3, B_row_half, B_col_start, 0, 0, hsize, hsize);
            // M_7 := A_22 * B_3;
            //Copy(tempA, A, A_row_half, A_col_half, hsize, hsize);
            //mult_winograd(tempA, B_3, M, 0, 0, 0, 0, 0, 0, hsize);
            mult_winograd(A, B_3, M, A_row_half, A_col_half, 0, 0, 0, 0, hsize, false);
            Copy(tempResult, M, hsize, 0, 0, 0, hsize, hsize);

            // A_3 := A_11 - A_21
            _instruction.subtract(A, A, A_3, A_row_start, A_col_start, A_row_half, A_col_start, hsize, hsize);
            // B_3 := B_22 - B_12
            _instruction.subtract(B, B, B_3, B_row_half, B_col_half, B_row_start, B_col_half, hsize, hsize);
            // M_4 := A_3 * B_3
            mult_winograd(A_3, B_3, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(M_temp, M, 0, 0, hsize, hsize);
            _instruction.increment(tempResult, M_temp, hsize, 0, hsize, hsize); // C_21 = M_7 + (M_1 + M_3 + M_4), complete
            _instruction.increment(tempResult, M_temp, hsize, hsize, hsize, hsize); // C_22 = M_5 + (M_1 + M_3 + M_4), complete

            // If size is odd, append the bottom row
            if (size % 2 == 1)
            {
                if (is_simple)
                {
                    _instruction.append_bottom_row(A, B, tempResult, size, size, size);
                    _instruction.append_top_left_submatrix(A, B, tempResult, size, size, size);
                    _instruction.append_right_column(A, B, tempResult, size, size, size);
                }
                else
                {
                    _instruction.append_bottom_row(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        0, 0,
                        size, size, size);

                    _instruction.append_top_left_submatrix(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        0, 0,
                        size, size, size);

                    _instruction.append_right_column(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        0, 0,
                        size, size, size);
                }
            }

            if (increment)
            {
                _instruction.increment(result, tempResult, result_row_start, result_col_start, size, size);
            }
            else
            {
                Copy(result, tempResult, result_row_start, result_col_start, 0, 0, size, size);
            }
        }

        /// <summary>
        /// Perform the Winograd variant of Strassen's multiplication on A and B, where B is given in transposed format 
        /// for accelerating naive multiplication
        /// 
        /// Note that B_row_start and B_col_start apply to B, not Bt. 
        /// </summary>
        private void mult_winograd_transposed(T[][] A, T[][] Bt, T[][] result,
            int A_row_start, int A_col_start,
            int B_row_start, int B_col_start,
            int result_row_start, int result_col_start, int size, bool increment)
        {
            int hsize = size / 2,
                A_row_half = A_row_start + hsize,
                A_col_half = A_col_start + hsize,
                B_row_half = B_row_start + hsize,
                B_col_half = B_col_start + hsize;

            bool is_simple =
                A_row_start == 0 && A_col_start == 0 &&
                B_row_start == 0 && B_col_start == 0 &&
                result_row_start == 0 && result_col_start == 0;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(size))
            {
                if (is_simple)
                {
                    _instruction.multiply_naive_transposed(A, Bt, result, size, size, size, increment);
                }
                else
                {
                    _instruction.multiply_naive_transposed(A, Bt, result,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        result_row_start, result_col_start,
                        size, size, size, increment);
                }
                return;
            }

            T[][] tempA = _tempAs[hsize];
            T[][] M = _Ms[hsize];
            T[][] tempResult = _tempResults[size];
            T[][] A_3 = _A_3s[hsize];
            T[][] B_3 = _tempBs[hsize];

            // Stores M_1, M_1 + M_3, M_1 + M_3 + M_4
            T[][] M_temp = _tempMs[hsize];

            // M_1 := A_11 * (B_11)^T = A_11 * (B^T)_11
            mult_winograd_transposed(A, Bt, M_temp, A_row_start, A_col_start, B_row_start, B_col_start, 0, 0, hsize, false);
            Copy(tempResult, M_temp, 0, 0, hsize, hsize); // C_11 = M_1

            // M_2 := A_12 * (B_21)^T = A_12 * (B^T)_12, C11 := M_1 + M_2
            mult_winograd_transposed(A, Bt, M, A_row_start, A_col_half, B_row_half, B_col_start, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, 0, 0, hsize, hsize); // C_11 = M_1 + M_2, complete

            // A_3 := A_21 + A_22
            _instruction.add(A, A, A_3, A_row_half, A_col_start, A_row_half, A_col_half, hsize, hsize);
            // B_3 := (B_12 - B_11)^T = (B_12)^T - (B_11)^T = (B^T)_21 - (B^T)_11
            _instruction.subtract(Bt, Bt, B_3, B_col_half, B_row_start, B_col_start, B_row_start, hsize, hsize);

            // M_5 := A_3 * B_3
            mult_winograd_transposed(A_3, B_3, M, 0, 0, 0, 0, 0, 0, hsize, false);
            Copy(tempResult, M, 0, hsize, 0, 0, hsize, hsize); // C_12 = M_5
            Copy(tempResult, M, hsize, hsize, 0, 0, hsize, hsize); // C_22 = M_5

            // B_3 := (B_22)^T - B_3 = (B^T)_22 - B_3 (i.e. B_3 := (B^T)_11 - (B^T)_12 + (B^T)_22)
            _instruction.subtract(Bt, B_3, B_3, B_col_half, B_row_half, 0, 0, hsize, hsize);
            // A_3 -= A_11 (A_3 := -A_11 + A_21 + A_22)
            Copy(tempA, A, A_row_start, A_col_start, hsize, hsize);
            _instruction.decrement(A_3, tempA, 0, 0, hsize, hsize);

            // M_3 := A_3 * B_3
            mult_winograd_transposed(A_3, B_3, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(M_temp, M, 0, 0, hsize, hsize);
            _instruction.increment(tempResult, M_temp, 0, hsize, hsize, hsize); // C_12 = M_5 + (M_1 + M_3)

            // A_3 := A_12 - A_3 (A_3 := A_11 + A_12 - A_21 - A_22)
            _instruction.subtract(A, A_3, A_3, A_row_start, A_col_half, 0, 0, hsize, hsize);
            // M_6 := A_3 * (B_22)^T = A_3 * (B^T)_22
            mult_winograd_transposed(A_3, Bt, M, 0, 0, B_row_half, B_col_half, 0, 0, hsize, false);
            _instruction.increment(tempResult, M, 0, hsize, hsize, hsize); // C_12 = (M_1 + M_3) + M_5 + M_6, complete

            // B_3 := (B_21)^T - B_3 = (B^T)_12 - B_3 (i.e. B_3 := -B_11 + B_12 + B_21 - B_22)
            _instruction.subtract(Bt, B_3, B_3, B_col_start, B_row_half, 0, 0, hsize, hsize);
            // M_7 := A_22 * B_3;
            //Copy(tempA, A, A_row_half, A_col_half, hsize, hsize);
            //mult_winograd(tempA, B_3, M, 0, 0, 0, 0, 0, 0, hsize);
            mult_winograd_transposed(A, B_3, M, A_row_half, A_col_half, 0, 0, 0, 0, hsize, false);
            Copy(tempResult, M, hsize, 0, 0, 0, hsize, hsize);

            // A_3 := A_11 - A_21
            _instruction.subtract(A, A, A_3, A_row_start, A_col_start, A_row_half, A_col_start, hsize, hsize);
            // B_3 := (B_22 - B_12)^T = (B^T)_22 - (B^T)_21
            _instruction.subtract(Bt, Bt, B_3, B_col_half, B_row_half, B_col_half, B_row_start, hsize, hsize);
            // M_4 := A_3 * B_3
            mult_winograd_transposed(A_3, B_3, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment(M_temp, M, 0, 0, hsize, hsize);
            _instruction.increment(tempResult, M_temp, hsize, 0, hsize, hsize); // C_21 = M_7 + (M_1 + M_3 + M_4), complete
            _instruction.increment(tempResult, M_temp, hsize, hsize, hsize, hsize); // C_22 = M_5 + (M_1 + M_3 + M_4), complete

            // If size is odd, append the bottom row
            if (size % 2 == 1)
            {
                throw new NotImplementedException();
                /*
                if (is_simple)
                {
                    _instruction.append_bottom_row(A, B, tempResult, size, size, size);
                    _instruction.append_top_left_submatrix(A, B, tempResult, size, size, size);
                    _instruction.append_right_column(A, B, tempResult, size, size, size);
                }
                else
                {
                    _instruction.append_bottom_row(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        0, 0,
                        size, size, size);

                    _instruction.append_top_left_submatrix(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        0, 0,
                        size, size, size);

                    _instruction.append_right_column(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        0, 0,
                        size, size, size);
                }*/
            }

            if (increment)
            {
                _instruction.increment(result, tempResult, result_row_start, result_col_start, size, size);
            }
            else
            {
                Copy(result, tempResult, result_row_start, result_col_start, 0, 0, size, size);
            }
        }

        private void mult_strassen_parallel(T[][] A, T[][] B, T[][] result,
            int A_row_start, int A_col_start,
            int B_row_start, int B_col_start,
            int result_row_start, int result_col_start, int size, bool increment)
        {
            int hsize = size / 2,
                A_row_half = A_row_start + hsize,
                A_col_half = A_col_start + hsize,
                B_row_half = B_row_start + hsize,
                B_col_half = B_col_start + hsize;

            bool is_simple =
                A_row_start == 0 && A_col_start == 0 &&
                B_row_start == 0 && B_col_start == 0 &&
                result_row_start == 0 && result_col_start == 0;

            // base case - use simple matrix multiplication
            if (use_naive_algorithm(size))
            {
                if (is_simple)
                {
                    _instruction.multiply_naive(A, B, _naive_mult_workspace, result, size, size, size, increment);
                }
                else
                {
                    _instruction.multiply_naive(A, B, _naive_mult_workspace, result,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        result_row_start, result_col_start,
                        size, size, size, increment);
                }
                return;
            }

            T[][] tempA = _tempAs[hsize];
            T[][] tempB = _tempBs[hsize];
            T[][] M = _Ms[hsize];
            T[][] tempResult = _tempResults[size];

            _instruction.clear(tempResult, 0, 0, size, size);

            // M_1 = (A_11 + A_22)(B_11 + B_22)
            _instruction.add_parallel(A, A, tempA, A_row_start, A_col_start, A_row_half, A_col_half, hsize, hsize);
            _instruction.add_parallel(B, B, tempB, B_row_start, B_col_start, B_row_half, B_col_half, hsize, hsize);
            mult_strassen_parallel(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment_parallel(tempResult, M, 0, 0, hsize, hsize);
            _instruction.increment_parallel(tempResult, M, hsize, hsize, hsize, hsize);

            // M_2 = (A_21 + A_22)B_11
            _instruction.add_parallel(A, A, tempA, A_row_half, A_col_start, A_row_half, A_col_half, hsize, hsize);
            CopyParallel(tempB, B, 0, 0, B_row_start, B_col_start, hsize, hsize);
            mult_strassen_parallel(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment_parallel(tempResult, M, hsize, 0, hsize, hsize);
            _instruction.decrement_parallel(tempResult, M, hsize, hsize, hsize, hsize);

            // M_3 = A_11(B_12 - B_22) 
            CopyParallel(tempA, A, 0, 0, A_row_start, A_col_start, hsize, hsize);
            _instruction.subtract_parallel(B, B, tempB, B_row_start, B_col_half, B_row_half, B_col_half, hsize, hsize);
            mult_strassen_parallel(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment_parallel(tempResult, M, 0, hsize, hsize, hsize);
            _instruction.increment_parallel(tempResult, M, hsize, hsize, hsize, hsize);

            // M_4 = A_22(B_21 - B_11)
            CopyParallel(tempA, A, 0, 0, A_row_half, A_col_half, hsize, hsize);
            _instruction.subtract_parallel(B, B, tempB, B_row_half, B_col_start, B_row_start, B_col_start, hsize, hsize);
            mult_strassen_parallel(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment_parallel(tempResult, M, 0, 0, hsize, hsize);
            _instruction.increment_parallel(tempResult, M, hsize, 0, hsize, hsize);

            // M_5 = (A_11 + A_12)B_22
            _instruction.add_parallel(A, A, tempA, A_row_start, A_col_start, A_row_start, A_col_half, hsize, hsize);
            CopyParallel(tempB, B, 0, 0, B_row_half, B_col_half, hsize, hsize);
            mult_strassen_parallel(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.decrement_parallel(tempResult, M, 0, 0, hsize, hsize);
            _instruction.increment_parallel(tempResult, M, 0, hsize, hsize, hsize);

            // M_6 = (A_21 - A_11)(B_11 + B_12)
            _instruction.subtract_parallel(A, A, tempA, A_row_half, A_col_start, A_row_start, A_col_start, hsize, hsize);
            _instruction.add_parallel(B, B, tempB, B_row_start, B_col_start, B_row_start, B_col_half, hsize, hsize);
            mult_strassen_parallel(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment_parallel(tempResult, M, hsize, hsize, hsize, hsize);

            // M_7 = (A_12 - A_22)(B_21 + B_22) 
            _instruction.subtract_parallel(A, A, tempA, A_row_start, A_col_half, A_row_half, A_col_half, hsize, hsize);
            _instruction.add_parallel(B, B, tempB, B_row_half, B_col_start, B_row_half, B_col_half, hsize, hsize);
            mult_strassen_parallel(tempA, tempB, M, 0, 0, 0, 0, 0, 0, hsize, false);
            _instruction.increment_parallel(tempResult, M, 0, 0, hsize, hsize);

            // If size is odd, append the bottom row

            if (size % 2 == 1)
            {
                if (is_simple)
                {
                    _instruction.append_bottom_row(A, B, tempResult, size, size, size);
                    _instruction.append_top_left_submatrix(A, B, tempResult, size, size, size);
                    _instruction.append_right_column(A, B, tempResult, size, size, size);
                }
                else
                {
                    _instruction.append_bottom_row(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        result_row_start, result_col_start,
                        size, size, size);

                    _instruction.append_top_left_submatrix(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        result_row_start, result_col_start,
                        size, size, size);

                    _instruction.append_right_column(A, B, tempResult,
                        A_row_start, A_col_start,
                        B_row_start, B_col_start,
                        result_row_start, result_col_start,
                        size, size, size);
                }
            }

            if (increment)
            {
                _instruction.increment_parallel(result, tempResult, result_row_start, result_col_start, size, size);
            }
            else
            {
                CopyParallel(result, tempResult, result_row_start, result_col_start, 0, 0, size, size);
            }
        }
    }
}
