﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Matrices.Strassen
{
    internal class StrassenInversionAlgorithm<T> where T : new()
    {
        private const int THRESHOLD = 32;

        private SquareStrassenMultiplication<T> _strassenMultiply;
        private IStrassenInstructionSet<T> _instruction;

        private Dictionary<int, T[][]> _temps, _M_1s, _M_2s, _M_3s, _M_6s;

        internal StrassenInversionAlgorithm(int n, IStrassenInstructionSet<T> instructions)
        {
            _strassenMultiply = new SquareStrassenMultiplication<T>(instructions, n, true);
            _instruction = instructions;

            _temps = new Dictionary<int, T[][]>();
            _M_1s = new Dictionary<int, T[][]>();
            _M_2s = new Dictionary<int, T[][]>();
            _M_3s = new Dictionary<int, T[][]>();
            _M_6s = new Dictionary<int, T[][]>();


            while (!use_naive_algorithm(n))
            {
                n /= 2;
                _temps[n] = MatrixInternalExtensions.JMatrix<T>(n, n);
                _M_1s[n] = MatrixInternalExtensions.JMatrix<T>(n, n);
                _M_2s[n] = MatrixInternalExtensions.JMatrix<T>(n, n);
                _M_3s[n] = MatrixInternalExtensions.JMatrix<T>(n, n);
                _M_6s[n] = MatrixInternalExtensions.JMatrix<T>(n, n);
            }
        }
        private bool use_naive_algorithm(int n)
        {
            return n < THRESHOLD;
        }

        /// <summary>
        /// Use Strassen's algorithm to invert a square matrix A, as per Strassen's original paper, 
        /// available at https://doi.org/10.1007/BF02165411.
        /// 
        /// Algorithm overview: 
        /// M_1 := inv(A_11)
        /// M_2 := A_21 * M_1
        /// M_3 := M_1 * A_12
        /// M_4 := A_21 * M_3
        /// M_5 := M_4 - A_22
        /// M_6 := inv(M_5)
        /// 
        /// C_12 := M_3 * M_6
        /// C_21 := M_6 * M_2
        /// M_7 := M_3 * C_21
        /// C_11 := M_2 - M_7
        /// C_22 := -M_6
        /// 
        /// To save space, only M_2, M_3, M_6 are kept, the other matrices are discarded at least step
        /// </summary>
        /// <param name="A"></param>
        /// <param name="result"></param>
        internal void Invert(T[][] A, T[][] result)
        {
            if (A == null || result == null)
            {
                throw new ArgumentNullException();
            }
            if (A.Length != A[0].Length || result.Length != result[0].Length || A.Length != result.Length)
            {
                throw new InvalidOperationException();
            }

            invert_unsafe(A, result, A.Length);
        }
        internal void Invert(DenseMatrix<T> A, DenseMatrix<T> result) => Invert(A.Values, result.Values);

        private void invert_unsafe(T[][] A, T[][] result, int n)
        {
            // Revert to naive inversion algorithm for small matrices
            if (use_naive_algorithm(n))
            {
                _instruction.invert_naive(A, result, n);
                return;
            }

            int half_n = n / 2;

            T[][] temp = _temps[half_n];
            T[][] M_1 = _M_1s[half_n];
            T[][] M_2 = _M_2s[half_n];
            T[][] M_3 = _M_3s[half_n];
            T[][] M_6 = _M_6s[half_n];

            // M_1 := inv(A_11)
            copy(temp, A, half_n, half_n);
            invert_unsafe(temp, M_1, half_n);

            // M_2 := A_21 * M_1
            copy(temp, A, half_n, 0, half_n, half_n);
            _strassenMultiply.Multiply(temp, M_1, M_2);

            // M_3 := M_1 * A_12
            copy(temp, A, 0, half_n, half_n, half_n);
            _strassenMultiply.Multiply(M_1, temp, M_3);

            // M_4 := A_21 * M_3 (stored in M_6)
            copy(temp, A, half_n, 0, half_n, half_n);
            _strassenMultiply.Multiply(temp, M_3, M_6);

            // M_5 := M_4 - A_22 (stored in M_6)
            copy(temp, A, half_n, half_n, half_n, half_n);
            _instruction.decrement(M_6, temp, 0, 0, half_n, half_n);

            // M_6 := inv(M_5), store in M_6 (overwrite and discard M_4, M_5) 
            copy(temp, M_6, half_n, half_n);
            invert_unsafe(temp, M_6, half_n);

            // C_12 := M_3 * M_6
            _strassenMultiply.Multiply(M_3, M_6, temp);
            copy(result, temp, 0, half_n, 0, 0, half_n, half_n);

            // C_21 := M_6 * M_2
            _strassenMultiply.Multiply(M_6, M_2, temp);
            copy(result, temp, half_n, 0, 0, 0, half_n, half_n);

            // M_7 := M_3 * C_21 (store in M_2, which is no longer needed)
            _strassenMultiply.Multiply(M_3, temp, M_2);

            // C_11 := M_1 - M_7 (which is stored in M_2)
            _instruction.subtract(M_1, M_2, result, 0, 0, 0, 0, half_n, half_n);

            // C_22 := -M_6
            _instruction.negate(M_6, result, 0, 0, half_n, half_n, half_n, half_n);
        }
        private static void copy(T[][] dest, T[][] src, int rows, int cols)
        {
            int r, c;
            for (r = 0; r < rows; ++r)
            {
                T[] d = dest[r], s = src[r];
                for (c = 0; c < cols; ++c)
                {
                    d[c] = s[c];
                }
            }
        }
        private static void copy(T[][] dest, T[][] src, int srcFirstRow, int srcFirstCol, int rows, int cols)
        {
            int r, c;
            for (r = 0; r < rows; ++r)
            {
                T[] d = dest[r], s = src[srcFirstRow + r];
                for (c = 0; c < cols; ++c)
                {
                    d[c] = s[srcFirstCol + c];
                }
            }
        }
        private static void copy(T[][] dest, T[][] src, int destFirstRow, int destFirstCol, int srcFirstRow, int srcFirstCol, int rows, int cols)
        {
            int r, c, k;
            if (srcFirstRow == 0 && srcFirstCol == 0)
            {
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[destFirstRow + r], s = src[r];
                    for (c = 0, k = destFirstCol; c < cols; ++c, ++k)
                    {
                        d[k] = s[c];
                    }
                }
            }
            else if (destFirstRow == 0 && destFirstCol == 0)
            {
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[r], s = src[srcFirstRow + r];
                    for (c = 0; c < cols; ++c)
                    {
                        d[c] = s[srcFirstCol + c];
                    }
                }
            }
            else
            {
                int end = cols + srcFirstCol;
                for (r = 0; r < rows; ++r)
                {
                    T[] d = dest[destFirstRow + r], s = src[srcFirstRow + r];
                    for (c = srcFirstCol, k = destFirstCol; c < end; ++c, ++k)
                    {
                        d[k] = s[c];
                    }
                }
            }
        }
    }
}
