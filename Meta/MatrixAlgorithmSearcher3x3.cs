﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Meta
{
    /// <summary>
    /// An algorithm that performs brute force search for an efficient algorithm for multiplication of two 3 x 3 matrices
    /// in less than 23 multiplication operations. 
    /// 
    /// </summary>
    public class MatrixAlgorithmSearcher3x3
    {
        private int _maxMultiplications;
        private int[] _gammaCoefficients, _lambdaCoefficients, _muCoefficients;
        private Random _random;

        private int[][] _gamma, _lambda, _mu;

        public MatrixAlgorithmSearcher3x3(int maxMultiplications, int[] gammaCoeffs, int[] lambdaCoeffs, int[] muCoeffs)
        {
            _maxMultiplications = maxMultiplications;
            _gammaCoefficients = gammaCoeffs;
            _lambdaCoefficients = lambdaCoeffs;
            _muCoefficients = muCoeffs;
            _random = new Random();

            _gamma = MatrixInternalExtensions.JMatrix<int>(9, maxMultiplications);
            _lambda = MatrixInternalExtensions.JMatrix<int>(9, maxMultiplications);
            _mu = MatrixInternalExtensions.JMatrix<int>(9, maxMultiplications);
        }

        // Defualt constructor
        public MatrixAlgorithmSearcher3x3() : this(22, new int[] { -1, 0, 1 }, new int[] { -1, 0, 1 }, new int[] { -1, 0, 1 })
        {

        }

        public void Search() 
        { 
        
        }

        /// <summary>
        /// Determines whether the coefficient set is a valid solution 
        /// The dimnesions of gamma, lambda, mu are the same: 9 x r where
        /// r is the number of multiplications we are targetting.
        /// </summary>
        /// <param name="gamma"></param>
        /// <param name="lambda"></param>
        /// <param name="mu"></param>
        /// <returns></returns>
        public bool IsValidSolution(int[][] gamma, int[][] lambda, int[][] mu)
        {
            int r, i, j, k;

            // First loop: fixes the C[k] = a[a_start] * b[b_start] + a[a_start + 1] * b[b_start + 1] + a[a_start + 2] * b[b_start + 2]
            for (k = 0; k < 9; ++k)
            {
                int a_start = 3 * (k / 3), a_end = a_start + 3, b_start = 3 * (k % 3), b_end = b_start + 3;
                
                // Lambda loop
                for (i = 0; i < 9; ++i)
                {
                    bool a_in_range = i >= a_start && i < a_end;

                    // mu loop
                    for (j = 0; j < 9; ++j)
                    {
                        bool b_in_range = j >= b_start && j < b_end;

                        // Set the target of the sum
                        int target = (a_in_range && b_in_range) ? 1 : 0;

                        // rank loop 
                        int sum = 0;
                        for (r = 0; r < _maxMultiplications; ++r)
                        {
                            sum += gamma[r][k] * lambda[r][i] * mu[r][i];
                        }

                        if (sum != target)
                        {

                            Debug.WriteLine("Conflict: " + k + "\t" + i + "\t" + j + "\t" + sum + "\t" + target);

                            // rank loop 
                            for (r = 0; r < _maxMultiplications; ++r)
                            {
                                Debug.WriteLine(r + "\t" + gamma[r][k] + " * " + lambda[r][i] + " * " + mu[r][i]);
                            }

                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public static void TestLadermanAlgorithm()
        {
            int[][] gamma_coeffs =
            {
                new int[] { 6, 14, 19 },
                new int[] { 1, 4, 5, 6, 12, 14, 15 },
                new int[] { 6, 7, 9, 10, 14, 16, 18 },
                new int[] { 2, 3, 4, 6, 14, 16, 17 },
                new int[] { 2, 4, 5, 6, 20 },
                new int[] { 14, 16, 17, 18, 21 },
                new int[] { 6, 7, 8, 11, 12, 13, 14 },
                new int[] { 12, 13, 14, 15, 22 },
                new int[] { 6, 7, 8, 9, 23 }
            };

            int[][] gamma = MatrixInternalExtensions.JMatrix<int>(9, 23);
            for (int i = 0; i < 9; ++i)
            {
                foreach (int index in gamma_coeffs[i])
                {
                    gamma[i][index - 1] = 1;
                }
            }
            gamma = gamma.Transpose();

            int[,] alpha = new int[23, 9]
            {
                { 1, 1, 1, -1, -1, 0, 0, -1, -1 },
                { 1, 0, 0, -1, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                {-1, 0, 0, 1, 1, 0, 0, 0, 0 },
                { 0, 0, 0, 1, 1, 0, 0, 0, 0 },
                { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                {-1, 0, 0, 0, 0, 0, 1, 1, 0 },
                {-1, 0, 0, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 1, 1, 0 },
                { 1, 1, 1, 0, -1, -1, -1, -1, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                { 0, 0, -1, 0, 0, 0, 0, 1, 1  },
                { 0, 0, 1, 0, 0, 0, 0, 0, -1 },
                { 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 1, 1 },
                { 0, 0, -1, 0, 1, 1, 0, 0, 0 },
                { 0, 0, 1, 0, 0, -1, 0, 0, 0 },
                { 0, 0, 0, 0, 1, 1, 0, 0, 0 },
                { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                { 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 1 }
            };

            int[,] beta = new int[23, 9]
            {
                { 0, 0, 0, 0, 1, 0, 0, 0, 0 },
                { 0, -1, 0, 0, 1, 0, 0, 0, 0 },
                { -1, 1, 0, 1, -1, -1, -1, 0, 1 },
                { 1, -1, 0, 0, 1, 0, 0, 0, 0 },
                { -1, 1, 0, 0, 0, 0, 0, 0, 0 },
                { 1, 0, 0, 0, 0, 0, 0, 0, 0 },
                { 1, 0, -1, 0, 0, 1, 0, 0, 0 },
                { 0, 0, 1, 0, 0, -1, 0, 0, 0 },
                { -1, 0, 1, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 1, 0, 0, 0 },
                { -1, 0, 1, 1, -1, -1, -1, 1, 0 },
                { 0, 0, 0, 0, 1, 0, 1, -1, 0 },
                { 0, 0, 0, 0, 1, 0, 0, -1, 0 },
                { 0, 0, 0, 0, 0, 0, 1, 0, 0 },
                { 0, 0, 0, 0, 0, 0, -1, 1, 0 },
                { 0, 0, 0, 0, 0, 1, 1, 0, -1 },
                { 0, 0, 0, 0, 0, 1, 0, 0, -1 },
                { 0, 0, 0, 0, 0, 0, -1, 0, 1  },
                { 0, 0, 0, 1, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 1, 0 },
                { 0, 0, 1, 0, 0, 0, 0, 0, 0 },
                { 0, 1, 0, 0, 0, 0, 0, 0, 0 },
                { 0, 0, 0, 0, 0, 0, 0, 0, 1 }
            };

            int[][] lambda = alpha.ToJagged();
            // Need a change of coordinates here before it works...
            int[][] mu = new int[23][];
            for (int i = 0; i < 23; ++i)
            {
                int[] row = new int[9];
                int[] src = beta.Row(i);
                row[0] = src[0];
                row[1] = src[3];
                row[2] = src[6];
                row[3] = src[1];
                row[4] = src[4];
                row[5] = src[7];
                row[6] = src[2];
                row[7] = src[5];
                row[8] = src[8];
                mu[i] = row;
            }

            // Print out the summands for debugging purposes 
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < 23; ++i)
            {
                s.Append($"M{i + 1}\t=\t(");
                for (int j = 0; j < 9; ++j)
                {
                    if (alpha[i, j] > 0)
                    {
                        s.Append($" + a{j / 3 + 1}{j % 3 + 1}");
                    }
                    else if (alpha[i, j] < 0)
                    {
                        s.Append($" - a{j / 3 + 1}{j % 3 + 1}");
                    }
                }
                s.Append(")(");
                for (int j = 0; j < 9; ++j)
                {
                    if (mu[i][j] > 0)
                    {
                        s.Append($" + b{j % 3 + 1}{j / 3 + 1}");
                    }
                    else if (mu[i][j] < 0)
                    {
                        s.Append($" - b{j % 3 + 1}{j / 3 + 1}");
                    }
                }
                s.AppendLine(")");
            }
            Debug.WriteLine(s.ToString());

            MatrixAlgorithmSearcher3x3 searcher = new MatrixAlgorithmSearcher3x3(23, null, null, null);
            bool isValid = searcher.IsValidSolution(gamma, lambda, mu);

            Debug.WriteLine("Is valid? " + isValid);
        }
    }
}
