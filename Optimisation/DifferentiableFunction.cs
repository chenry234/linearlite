﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Optimisation
{
    public class DifferentiableFunction<T> : IObjectiveFunction<T[]>
    {
        /// <summary>
        /// The dimensionality of the space in which we are optimising.
        /// </summary>
        public int Dimension { get; private set; }
        public Func<T[], double> Function { get; set; }
        public Func<T[], T[]> Gradient { get; set; }

        public DifferentiableFunction(int dimension, Func<T[], double> calculateFunction, Func<T[], T[]> calculateGradient)
        {
            Dimension = dimension;
            Function = calculateFunction;
            Gradient = calculateGradient;
        }

        public double Evaluate(T[] point)
        {
            return Function(point);
        }
    }
}
