﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Optimisation
{
    public static class Extensions
    {
        /// <summary>
        /// Calculate the argmin of a real differentiable function, using a gradient method
        /// </summary>
        /// <param name="function"></param>
        /// <returns>Argmin of a function</returns>
        public static double[] Argmin(this DifferentiableFunction<double> function)
        {
            GradientOptimiserReal optimiser = new GradientOptimiserReal();
            optimiser.Type = OptimisationType.Minimise;
            return optimiser.Optimise(function);
        }

        /// <summary>
        /// Calculate the argmax of a real differentiable function, using a gradient method
        /// </summary>
        /// <param name="function">The function to be maximised</param>
        /// <returns>Argmax of a function</returns>
        public static double[] Argmax(this DifferentiableFunction<double> function)
        {
            GradientOptimiserReal optimiser = new GradientOptimiserReal();
            optimiser.Type = OptimisationType.Maximise;
            return optimiser.Optimise(function);
        }
    }
}
