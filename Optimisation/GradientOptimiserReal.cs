﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Optimisation
{
    /// <summary>
    /// Iterative gradient-based optimiser for functions f : R^n -> R
    /// </summary>
    public class GradientOptimiserReal : IOptimiser<DifferentiableFunction<double>, double[]>
    {
        public int MaxIterations = 1000;

        /// <summary>
        /// If not NaN, then if the gradient's 2-norm goes below this value, the optimisation exits. 
        /// </summary>
        public double GradientTerminationThreshold = double.NaN;
        public double LearningRate = 0.01;
        public bool Verbose = false;
        public OptimisationType Type = OptimisationType.Minimise;

        private double[] _point;
        private double[] _gradient;

        public void Reset()
        {
            _point = null;
            _gradient = null;
        }

        public double[] Optimise(DifferentiableFunction<double> function)
        {
            Reset();

            int dim = function.Dimension;

            _point = RectangularVector.Random<double>(dim);
            for (int i = 0; i < MaxIterations; i++)
            {
                _gradient = function.Gradient(_point);

                if (Type == OptimisationType.Minimise)
                {
                    for (int d = 0; d < dim; d++)
                    {
                        _point[d] -= LearningRate * _gradient[d];
                    }
                }
                else
                {
                    for (int d = 0; d < dim; d++)
                    {
                        _point[d] += LearningRate * _gradient[d];
                    }
                }

                if (!double.IsNaN(GradientTerminationThreshold))
                {
                    if (_gradient.Norm(2) < GradientTerminationThreshold) return _point;
                }

                if (Verbose)
                {
                    _gradient.Print();
                    //Console.WriteLine($"Iteration {i}: f(x) = {function.Function(_point)}.");
                }
            }
            return _point;
        }

    }
}
