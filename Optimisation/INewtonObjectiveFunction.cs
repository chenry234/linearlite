﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Optimisation
{
    /// <summary>
    /// An objective function defined over the space S, that has a well-defined
    /// 1st and 2nd derivative and hence suitable for Newton's optimisation method.
    /// </summary>
    /// <typeparam name="S"></typeparam>
    public abstract class INewtonObjectiveFunction<S> : IObjectiveFunction<S[]> where S : new()
    {
        public int Dimension { get; private set; }

        /// <summary>
        /// Create a Newton objective function over S^dimension
        /// </summary>
        /// <param name="dimension"></param>
        protected INewtonObjectiveFunction(int dimension)
        {
            Dimension = dimension;
        }

        /// <summary>
        /// Calculate the gradient at point 'point', output to 'gradient'
        /// </summary>
        /// <param name="point"></param>
        /// <param name="gradient"></param>
        public abstract void CalculateGradient(S[] point, ref S[] gradient);

        /// <summary>
        /// Calculate the hessian matrix at point 'point', output to 'hessian'
        /// </summary>
        /// <param name="point"></param>
        /// <param name="hessian"></param>
        public abstract void CalculateHessian(S[] point, ref DenseMatrix<S> hessian);

        public abstract double Evaluate(S[] point);
    }
}
