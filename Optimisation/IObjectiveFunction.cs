﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Optimisation
{
    /// <summary>
    /// An objective function defined over the domain S
    /// </summary>
    /// <typeparam name="S"></typeparam>
    public interface IObjectiveFunction<S>
    {
        /// <summary>
        /// Evaluates the objective function at location 'point'
        /// </summary>
        /// <param name="point"></param>
        /// <returns>The objective function evaluated at 'point'</returns>
        double Evaluate(S point);
    }
}
