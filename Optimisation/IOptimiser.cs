﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Optimisation
{
    /// <summary>
    /// Optimiser interface for maximising/minimising a function F subject in the space S
    /// </summary>
    /// <typeparam name="F">The objective function, which is defined in space S</typeparam>
    /// <typeparam name="S">The function's space</typeparam>
    public interface IOptimiser<F, S> where F : IObjectiveFunction<S>
    {
        void Reset();

        S Optimise(F objFunc);
    }
}
