﻿using LinearLite.Structs;
using System;

namespace LinearLite.Optimisation
{
    /// <summary>
    /// Newton optimisation class for objective functions f: C^n -> R
    /// </summary>
    public class NewtonOptimiserComplex : IOptimiser<INewtonObjectiveFunction<Complex>, Complex[]>
    {
        private Complex[,] _hessian;
        private Complex[,] _gradient;
        private Complex[] _point;

        public void Reset()
        {
            throw new NotImplementedException();
        }
        public Complex[] Optimise(INewtonObjectiveFunction<Complex> objFunc)
        {
            throw new NotImplementedException();
        }

    }
}
