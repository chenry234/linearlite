﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Optimisation
{
    /// <summary>
    /// Newton optimisation class for objective functions f: R^n -> R
    /// </summary>
    public abstract class NewtonOptimiserReal : IOptimiser<INewtonObjectiveFunction<double>, double[]>
    {
        #region Default values 

        private double _gamma = 1;
        private int _maxIterations = 1000;

        #endregion

        private DenseMatrix<double> _hessian;
        private double[] _gradient;
        private double[] _point;

        /// <summary>
        /// The step size 
        /// </summary>
        public double Gamma { get { return _gamma; } set { _gamma = value; } }
        /// <summary>
        /// The maximum number of iterations
        /// </summary>
        public int MaxIterations { get { return _maxIterations;  } set { _maxIterations = value; } }

        public void Reset()
        {
            _point = null;
            _gradient = null;
            _hessian = null;
        }

        public double[] Optimise(INewtonObjectiveFunction<double> objFunc)
        {
            return Optimise(objFunc, new double[objFunc.Dimension]);
        }

        public double[] Optimise(INewtonObjectiveFunction<double> function, double[] seed)
        {
            _point = seed.Copy();
            _gradient = new double[_point.Length];
            _hessian = new DenseMatrix<double>(_point.Length, _point.Length);

            for (int i = 0; i < _maxIterations; i++)
            {
                Iteration(function);
            }

            return _point;
        }

        private void Iteration(INewtonObjectiveFunction<double> function)
        {
            function.CalculateGradient(_point, ref _gradient);
            function.CalculateHessian(_point, ref _hessian);

            //double[] step = _hessian.Invert().Multiply(_gradient);
            double[] step = _hessian.Invert().Values.ToRectangular().Multiply(_gradient);

            for (int i = 0; i < step.Length; i++)
            {
                _point[i] -= _gamma * step[i];
            }
        }
    }
}
