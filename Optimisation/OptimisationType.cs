﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Optimisation
{
    public enum OptimisationType
    {
        Minimise, Maximise
    }
}
