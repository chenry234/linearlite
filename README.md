
# Linear.NET

A high performance linear algebra and scientific computing library, written in native C#. 
Features:
- Common and advanced vector, matrix and tensor operations over different algebras
- Matrix decompositions
- Numerical linear and non-linear equation solvers
- Linear and non-linear programming
- Symbolic algebra and exact equation solvers (coming soon)
- Numerical ODE and PDE solvers (coming soon)
- Machine learning library (coming soon)

Full documentation and examples are available at: http://lineardotnet.com/  
The library is currently under development; a stable release will be available soon.

## Usage guide

### Objects
Linear.NET methods operate on generic types that represent algebraic objects. 
The following objects are supported:  

| **Algebraic structure** | **Equivalent object or interface** | **Examples of predefined implementations** |
| :----- | :---------- | :---------------------------------------- |
| Group  | `Group<T>`  | `AdditiveGroup<T>`, `CyclicGroup`         |
| Ring   | `Ring<T>`   | `DivisionRing<T>`, `Polynomial<T>`, `Quaternion` |
| Field  | `Field<T>`  | `Complex`, `Rational`, `RationalFunction` |
| Vector | `IVector<T>` or `T[]`       | `DenseVector<T>`, `SparseVector<T>`         |
| Matrix | `IMatrix<T>` or `T[,]`      | `DenseMatrix<T>`, `SparseMatrix<T>`, `BandMatrix<T>`, `TriangularMatrix<T>` |
| Tensor | `ITensor<T>` | `DenseTensor<T>`, `SparseTensor<T>`       |

In the above table, the generic type `T` can be:
- a primitive of type `int`, `long`, `float`, `double` or `decimal`, or
- any struct implementing an algebraic interface (e.g. the predefined `Complex` struct which implements the `Field<T>` interface).

### Object initialisation
Utility methods are provided to quickly initialize common types of objects.
```cs
// Create a 5 x 10 dense zero matrix over the reals
DenseMatrix<double> zero = DenseMatrix.Zero<double>(5, 10);

// Create a 5 x 5 dense identity quaternionic matrix
DenseMatrix<Quaternion> identity = DenseMatrix.Identity<Quaternion>(5);

// Create a 3 x 3 rectangular diagonal matrix 
double[,] diagonal = RectangularMatrix.Diag<double>(2, 5, -3);

// Create a random 6 x 4 sparse matrix, with up to 10 entries
SparseMatrix<double> random = SparseMatrix.RandomDense<double>(6, 4, 10);

// Create a random 10 x 10 positive-definite matrix
DenseMatrix<double> psd = DenseMatrix.RandomPositiveDefinite<double>(10);

// Create a random vector in [-1,1]^10
double[] vect = RectangularVector.Random<double>(10, -1, 1);

// Create a zero tensor in C^5 x C^6 x C^3
Tensor<Complex> tensor = new Tensor<Complex>(5, 6, 3);
```
### Common operations
Linear.NET supports most common tensor, matrix and vector operations via extension methods.  
For a full list of supported methods, please see the documentation here.  

**Support table for common matrix operations:**  
Note:  all complexity estimates are arithmetic complexities, and apply to a matrix of size $`n \times n`$.

| **Operation** | `DenseMatrix<T>` and `T[,]` | `SparseMatrix<T>` with $`k`$ non-zero entries|
|:---|:---|:---|
| $`+`$, $`-`$ | **Matrix addition/subtraction** parallelizable implementation for all primitive types, and all `T` implementing the `AdditiveGroup<T>` interface.<br>**Other sum types:** Kronecker sums and direct sums are also supported. | Supported via $`O(k)`$ naive method for all primitive types, and all `T` implementing the `AdditiveGroup<T>` interface.<br>**Other sum types:** direct sums are also supported. |
| $`\times`$ | **Matrix-matrix multiplication** implemented via $`O(n^3)`$ naive method, and $`O(n^{2.807})`$ Strassen-Winograd's algorithm. Both implementations support parallelization.<br>**Other product types:** matrix-vector product, matrix-scalar product, Kronecker product, Khatri-Rao product, Hadamard product, Frobenius inner product and outer-products are also supported.<br> All primitive types, and all types `T` implementing the `Ring<T>` interface are supported. | **Matrix-matrix multiplication** implemented via $`O(\min\{k^2,n^3\})`$ naive algorithm.<br>**Other supported products:** sparse matrix-vector product, sparse matrix-scalar product, Kronecker product.<br>Supported for all primitive types, and all types `T` implementing the `Ring<T>` interface. |
| Inversion | **Matrix inversion** implemented via $`O(n^3)`$ Gaussian elimination, and $`O(n^{2.807})`$ Strassen's algorithm.<br>**Moore-Penrose pseudo-inversion** implemented using Courrieu's algorithm.<br>**Left/right inverse** also supported for rectangular matrices.<br>Supported for all primitive types, and all types `T` implementing the `Field<T>` interface. | Not supported yet. |
| Rank | Implemented via $`O(n^3)`$ Gaussian elimination.<br>Supported for all primitive types, and all types `T` implementing the `Field<T>` interface.| Not supported yet. |
| Determinant | Implemented via $`O(n^3)`$ Gaussian elimination.<br>Supported for all primitive types, and all types `T` implementing the `Field<T>` interface. | Supported for all sparse matrices over the primitive types, and all types `T` implementing `Field<T>`. |
| Trace | Supported for all primitive types, and all types `T` implementing the `AdditiveGroup<T>` interface. | $`O(\min\{k,n\})`$ implementation, supported for all primitive types, and all types `T` implementing `AdditiveGroup<T>`. |
| Elementary row operations, Transposition, Submatrix, Vectorization | Supported for all types. | Supported for all types. |
| Norm | **$`L_{p, q}`$ elementwise norm** supported for all integers $`p, q > 0`$.<br>**Induced $`p`$-norm** supported for any integer $`p > 0`$ and $`p = \infty`$. | $`O(k)`$ implementation of **$`L_{p, q}`$ elementwise norm**, supported for all integers $`p, q > 0`$. |
| Eigenvalues, eigenvectors and eigendecomposition<br>$`A=Q\Lambda Q^{-1}`$ | Implemented via Francis' double-shift algorithm.<br>Alternative implementations: QR algorithm, shifted QR algorithm | Not supported yet. |
| Characteristic polynomial | Implemented via Faddeev–LeVerrier's algorithm.<br>Supported for all primitive types, and all types `T` implementing the `Field<T>` interface.| Not supported yet. |
| Matrix exponentiation, matrix powers<br>$`\exp(A)`$, $`A^k`$ | Implemented via spectral decomposition | Not supported yet. |
| Tridiagonalization<br> $`A=U^*TV`$| Implemented via Householder transformations. | Not supported yet. |
| Bidiagonalization<br> $`A=U^*BV`$| Implemented via Householder transformations. Alternative implementation using the Golub-Kahan-Lanczos algorithm. | Not supported yet. |
| Hessenberg form decomposition<br> $`A=QHQ^*`$| Supported for matrices over `float`, `double`, `decimal` and `Complex`. | Not supported yet. |
| QR, QL, LQ and RQ decomposition| Supported for matrices over `float`, `double`, `decimal` and `Complex`. Alternative implementation using the Gram-Schmidt algorithm. | Not supported yet. |
| Cholesky decomposition<br>$`A=LL^*`$| Implemented via the Cholesky–Banachiewicz algorithm for matrices over `float`, `double`, `decimal` and `Complex`. | Not supported yet. |
| LU and block-LU decomposition<br>$`A=LU`$ | Implemented via the Doolittle algorithm for matrices over `float`, `double`, `decimal` and `Complex`.  Block variant uses Cholesky decompositions. | Not supported yet. |
| Block LDU decomposition<br>$`A=LDU`$ | Implemented via naive method. | Not supported yet.|
| Singular value decomposition<br>$`A=UDV^*`$ | Implemented via Bidiagonalized-QR algorithm. | Not supported yet. |
| Rank decomposition<br>$`A=XY`$ | Implemented via row echelon reduction. | Not supported yet. |
| Canonical polyadic decomposition<br>$`A=v\otimes u`$ | Implemented via regularized alternating-least-squares (RALS) algorithm. | Not supported yet. |

**Example**: common matrix operations
```cs
// Initialise the matrices
// 1  3  4        3  4 -2
// 6  2  3  and  -1  0 -10
// 8  6  9       -5  0 -5
double[,] A = Matrix.Create(3, 3, new double[] { 1, 3, 4, 6, 2, 3, 8, 6, 9 });
double[,] B = new double[3, 3] {{ 3, 4, -2 }, { -1, 0, -10 }, {-5, 0, -5}};

// All operations leave both A and B unchanged, allowing them to be re-used later.
double[,] sum = A.Add(B);              // matrix addition (A + B)
double[,] prod = A.Multiply(B);        // matrix multiplication (AB)
double[,] inv = A.Invert();            // matrix inversion
double[,] tran = A.Transpose();        // matrix transposition
double trace = A.Trace();              // matrix trace
double det = A.Determinant();          // matrix determinant
int rank = A.Rank();                   // matrix rank
double norm = A.Norm(2);               // matrix induced 2-norm 
Complex[] eigen = A.Eigenvalues();     // matrix eigenvalues
Complex[] exp = A.Exp();               // matrix exponential
double[,] re = A.ToRowEchelonForm();   // matrix to row-echelon form
double[,] bi = A.ToBidiagonalForm();   // matrix to bi-diagonal form

// Parallel methods for large matrices
double[,] D = A.ParallelSubtract(B);
double[,] P = A.ParallelMultiplyStrassen(B);

```
**Example**: matrix decompositions
```cs
double[,] A = Matrix.Create(3, 3, new double[] { 1, 3, 4, 6, 2, 3, 8, 6, 9 });
Complex[,] C = Matrix.RandomComplex(3, 3);

// QR decomposition: A = QR
// matrix A (m x n) -> unitary matrix Q (m x m) and upper-triangular matrix R (m x n)
A.QRDecompose(out double[,] Q, out double[,] R);

// LU decomposition: A = LU
// square matrix A (m x m) -> lower-triangular matrix L (m x m) and
// upper-triangular matrix R (m x m)
C.LUDecompose(out Complex[,] L, out Complex[,] U);

// Cholesky decomposition: A = L(L^T)
// positive definite A (m x m) -> lower-triangular matrix L (m x m) and its transpose
A.CholeskyDecompose(out double[,] L);

// Eigendecomposition: A = VD(V^-1)
// Diagonalisable square matrix A -> Eigenvector matrix V and diagonal matrix D
A.EigenDecompose(out Complex[,] D, out Complex[,] V);

```

**Support table for common tensor operations**  

| **Operation** | `DenseTensor<T>` | `SparseTensor<T>` |
|:---|:---|:---|
| $`+,-`$ | **Elementwise addition and subtraction** supported for all primitives and all `T` implementing `AdditiveGroup<T>` <br>**Direct sums** $`\mathcal{A}\oplus\mathcal{B}`$ supported for all types. | **Elementwise addition and subtraction** supported for all primitives and all `T` implementing `AdditiveGroup<T>` <br>**Direct sums** supported for all types. |
| $`\times`$ | **Elementwise multiplication** supported for all primitives and all `T` implementing `Ring<T>`<br>**Tensor product** $`\mathcal{A}\otimes\mathcal{B}`$ supported for all primitives and all `T` implementing `Ring<T>` | **Elementwise multiplication** supported for all primitives and all `T` implementing `Ring<T>`<br>**Tensor product** $`\mathcal{A}\otimes\mathcal{B}`$ supported for all primitives and all `T` implementing `Ring<T>` |
| Vectorization | Supported for all types. | Supported for all types. |
| $`n`$-mode flattening/unfolding $`\mathcal{A}_{(n)}`$ | Supported for all types.| Not supported yet. |
| $`n`$-mode product $`\mathcal{A}\times_n U`$ | Tensor-matrix and tensor-vector products are supported, for all primitives and all `T` implementing `Ring<T>` | Not supported yet. |
| Rank-one tensor approximation<br>$`\mathcal{A}=\underset{1\le i\le n}{\bigotimes}v_i, v_i\in\Reals^{d_i}`$ | Supported for tensors over `double` | Not supported yet. |
| Higher Order Singular Value Decomposition (HOSVD)<br>$`\mathcal{A}=\mathcal{S}\times_1 U^{(1)}...\times_n U^{(n)}`$ | Supported for tensors over `float`, `double`, `decimal` and `Complex`| Not supported yet. |


**Example**: vector space operations
```cs
double[] v = { 4, 5, 6 }, u = { -1, -4, 9 };
double[] vSum = v.Add(u);              // vector addition (v + u)
double dotProd = v.Dot(u);             // vector dot product (v . u)
double[] crossProd = v.Cross(u);       // vector cross product (v x u)
double[] Av = A.Multiply(v);           // matrix-vector multiplication (Av)
double L2 = v.Norm(2);                 // vector 2-norm
double angle = v.AngleTo(u);           // angle between u and v

Complex[] w = { new Complex(1, 2), Complex.FromPolar(5, Math.PI / 3);
Complex[] z = { Complex.One, new Complex(-5, 0) };
Complex[] prod = z.Multiply(3);        // vector scalar multiplication
Complex[] proj = w.ProjectOnto(z);     // vector projection
```

### Numerical optimisation
LinearLite supports numerical optimisation through the following algorithms:
- Gradient-descent/ascent optimisation
- Newton's optimisation method

**Example**: Gradient-based minimisation of $`f(x,y)=2x^2 + 9y^2 - 6xy + 10x - 30y + 30`$.
```cs
var function = new DifferentiableFunction<double>(
    2,
    point => {    // f(x, y) = 2x^2 + 9y^2 - 6xy + 10x - 30y + 30
        double x = point[0], y = point[1];
        return 2 * x * x + 9 * y * y - 6 * x * y + 10 * x - 30 * y + 30;
    },
    point => {    // dF(x, y) = (4x - 6y + 10, 18y - 6x - 30)
        double x = point[0], y = point[1];
        return new double[] { 4 * x - 6 * y + 10, 18 * y - 6 * x - 30 };
    });
    
double[] argmin = function.Argmin();              // [0.00000, 1.66666]
double minimum = function.Function(solution);     // 5.000

```
### Equation solvers
LinearLite offers the following linear and non-linear solvers:
- Multivariate linear solver, for equations of the form $`Ax=b`$
- Polynomial root solver, for polynomial equations of the form $`p(x)=0`$
- General non-linear solver, for general non-linear systems of the form $`\{f_i(x)=0,1 \le i \le n\}`$

**Example**: Solving the linear system
```math
\begin{cases}
x+3y-2x=5\\
3x+5y+6z=7\\
2x+4y+3z=8
\end{cases}
```
```cs
double[,] A = { { 1, 3, -2 }, { 3, 5, 6 }, { 2, 4, 3} };
double[] b = { 5, 7, 8 };

Solution<double[]> solution = LinearSolver.Solve(A, b);
if (solution.Success) {
    double[] x = solution.Result;       // [-15, 8, 2]
}
```
**Example**: Solving the equation $`p(x)=x^5+x^4-15x^2+21x^2-16x+20=0`$ using a polynomial solver.
```cs
// Define the polynomial p(x) = x^5 + x^4 - 15x^2 + 21x^2 - 16x + 20
Polynomial p = new Polynomial(1, 1, -15, 21, -16, 20);
Complex[] roots = p.Roots();            // [2, 2, -5, i, -i]
double[] realRoots = p.RealRoots();     // [2, 2, -5]
```
**Example**: Using the Newton method to partially solve the non-linear system 
```math
\begin{cases}
e^x \cos(x+y) + 1 = 0\\ 
xy^3 + \frac{1}{x} = 0
\end{cases}
```
```cs
NewtonSolver solver = new NewtonSolver(
    new DifferentiableFunction<double>(2,
        x => { return Math.Exp(x[0]) * Math.Cos(x[0] + x[1]) + 1; },
        x => { return new double[] {
                  Math.Exp(x[0]) * (-Math.Sin(x[0] + x[1]) + Math.Cos(x[0] + x[1])),
                  -Math.Exp(x[0]) * Math.Sin(x[0] + x[1])
               };
        }),
    new DifferentiableFunction<double>(2, 
        x => { return x[0] * Math.Pow(x[1], 3) + 1 / x[0]; },
        x => { return new double[] { Math.Pow(x[1], 3) - 1 / (x[0] * x[0]), 3 * x[0] * x[1] * x[1] };
    }));
    
// Run the algorithm starting at (x, y) = (1, 1)
Solution<double[]> solution = solver.Solve(new double[] { 1, 1 });  
if (solution.Success) {
    double[] answer = solution.Result;   // [2.25687, -0.58120]
}
```

### Benchmarks
**Matrix benchmarks:**   
CPU matrix benchmarks for matrices of size $`n \times n`$, run on 3.60 GHz 8-core i9-9900K, 16mb cache (max. 16 threads).  


| **Operation** |  Data type | $`n=256`$ | $`n=1024`$ | $`n=4196`$ |
|:---|:---:|:---:|:---:|:---:|
| Matrix addition | `double[,]`<br>`Complex[,]` | 1 ms<br>3 ms | 8 ms<br>52 ms | 138 ms<br>687 ms |
| Matrix multiplication | `double[,]`<br>`Complex[,]` | 0.03 s<br>0.07 s | 2.22 s<br>4.60 s | 140 s<br>290 s |
| Matrix multiplication - Strassen-Winograd | `double[,]`<br>`Complex[,]` | 0.04 s<br>0.07 s | 1.86 s<br>3.58 s | 92.6 s<br>181 s | 
| Parallel matrix multiplication | `DenseMatrix<double>`<br>`DenseMatrix<Complex>` | < 0.01 s<br>0.01 s | 0.31 s<br> 0.65 s | 19.0 s<br>39.3 s |
| Matrix inversion - Strassen | `double[,]`<br>`Complex[,]` | 0.07 s<br>0.13 s | 3.73 s<br>8.33 s | 241 s<br>520 s |
| QR decomposition | `DenseMatrix<double>`<br>`DenseMatrix<Complex>` | 0.07 s<br>0.13 s | 4.68 s<br>9.82 s | 387 s<br>650 s |
| Parallel QR decomposition | `DenseMatrix<double>`<br>`DenseMatrix<Complex>` | 0.04 s<br>0.07 s | 0.91 s<br>1.64 s | 77.1 s<br>140 s |
