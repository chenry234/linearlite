﻿using LinearLite.Structs;
using LinearLite.Structs.Rings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Solvers
{
    internal abstract class IPolynomialRootSolver
    {
        public abstract Complex[] Solve(ComplexPolynomial polynomial);
    }
}
