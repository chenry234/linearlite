﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Solvers
{
    public class LinearSolver
    {
        /// <summary>
        /// TODO: debug
        /// Solves the linear system Ax = b, where
        /// A is (m x n), the coefficient matrix
        /// b is (m x 1) vector
        /// </summary>
        /// <param name="A"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Solution<double[]> Solve(double[,] A, double[] b)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(b);

            int m = A.GetLength(0), n = A.GetLength(1);
            if (m != b.Length)
            {
                throw new InvalidOperationException("Dimensions don't match.");
            }
            if (m < n)
            {
                return new Solution<double[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            double[][] _A = A.ToJagged();
            double[] _b = b.Copy();

            // Gaussian elimination
            for (int c = 0; c < n; ++c)
            {
                // pivot on the row r with largest magnitude on element _A[r][c]
                double max = _A[c][c];
                int swap = c;
                for (int r = c + 1; r < m; ++r)
                {
                    double abs = Math.Abs(_A[r][c]);
                    if (abs > max)
                    {
                        max = abs;
                        swap = r;
                    }
                }

                // swap the row
                double[] tempRow = _A[swap];
                _A[swap] = _A[c];
                _A[c] = tempRow;

                // swap the b
                double tempB = _b[swap];
                _b[swap] = _b[c];
                _b[c] = tempB;

                // perform elimination on row c
                double[] A_c = _A[c];
                double b_c = _b[c], A_cc = A_c[c];
                for (int r = c + 1; r < m; ++r)
                {
                    double[] A_r = _A[r];
                    double s = A_r[c] / A_cc;
                    for (int i = c; i < n; i++)
                    {
                        A_r[i] -= s * A_c[i];
                    }
                    _b[r] -= s * b_c;
                }
            }

            if (m > n && !Util.ApproximatelyEquals(_A[m - 1][n - 1], 0))
            {
                return new Solution<double[]>(ExitCode.InconsistentEquations, "Equations are inconsistent. No solution exists.");
            }
            if (m == n && Util.ApproximatelyEquals(_A[n - 1][n - 1], 0))
            {
                return new Solution<double[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solve using backsubstitution
            double[] x = new double[n];
            for (int i = n - 1; i >= 0; --i)
            {
                double[] A_i = _A[i];
                double sum = _b[i];
                for (int j = n - 1; j > i; j--)
                {
                    sum -= x[j] * A_i[j];
                }
                x[i] = sum / A_i[i];
            }
            return new Solution<double[]>(ExitCode.Success, x);
        }
        public static Solution<double[]> Solve(int[,] A, int[] b)
        {
            return Solve(A.ToDouble(), b.ToDouble());
        }

        /// <summary>
        /// Solves the integer linear equation system, returning solution in exact form as fractions, 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Solution<Rational[]> SolveExact(int[,] A, int[] b)
        {
            MatrixChecks.CheckNotNull(A);
            MatrixChecks.CheckNotNull(b);

            int m = A.GetLength(0), n = A.GetLength(1);
            if (m != b.Length)
            {
                throw new InvalidOperationException("Dimensions don't match.");
            }
            if (m < n)
            {
                return new Solution<Rational[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            Rational[,] AFrac = A.ToRational();
            Rational[] bFrac = b.ToFraction();

            // Gaussian elimination
            int r, c, i, j;
            for (c = 0; c < n; ++c)
            {
                for (r = c + 1; r < m; ++r)
                {
                    Rational s = (AFrac[r, c] / AFrac[c, c]).Simplify();
                    for (i = c; i < n; i++)
                    {
                        AFrac[r, i] -= s * AFrac[c, i];
                    }
                    bFrac[r] -= s * bFrac[c];
                }
            }

            if (m > n && AFrac[m - 1, n - 1].Equals(Rational.Zero))
            {
                return new Solution<Rational[]>(ExitCode.InconsistentEquations, "Equations are inconsistent. No solution exists.");
            }
            if (m == n && AFrac[n - 1, n - 1].Equals(Rational.Zero))
            {
                return new Solution<Rational[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solve using backsubstitution
            Rational[] x = new Rational[n];
            for (i = n - 1; i >= 0; --i)
            {
                Rational sum = bFrac[i];
                for (j = n - 1; j > i; j--)
                {
                    sum -= x[j] * AFrac[i, j];
                }
                x[i] = (sum / AFrac[i, i]).Simplify();
            }
            return new Solution<Rational[]>(ExitCode.Success, x);
        }
        public static Solution<T[]> Solve<T>(T[,] Amatrix, T[] bVect, double tolerance = Precision.DOUBLE_PRECISION) where T : Field<T>, new()
        {
            MatrixChecks.CheckNotNull(Amatrix);
            MatrixChecks.CheckNotNull(bVect);

            int m = Amatrix.GetLength(0), n = Amatrix.GetLength(1);
            if (m != bVect.Length)
            {
                throw new InvalidOperationException("Dimensions don't match.");
            }
            if (m < n)
            {
                return new Solution<T[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solving the linear system requires O(n^3) time, it's worthwhile to spend O(n^2) 
            // time converting T[,] into the more efficient T[][] matrix
            T[][] A = Amatrix.ToJagged();
            T[] b = bVect.Copy();
            T zero = A[0][0].AdditiveIdentity;

            // The method as it stands suffers from numerical instability
            // Gaussian elimination
            for (int c = 0; c < n; ++c)
            {
                T[] A_c = A[c];
                T b_c = b[c], A_cc_inv = A_c[c].MultiplicativeInverse();
                for (int r = c + 1; r < m; ++r)
                {
                    T[] A_r = A[r];
                    T s = A_r[c].Multiply(A_cc_inv);
                    for (int i = c; i < n; i++)
                    {
                        A_r[i] = A_r[i].Subtract(s.Multiply(A_c[i]));
                    }
                    b[r] = b[r].Subtract(s.Multiply(b_c));
                }
            }

            if (m > n && !A[m - 1][n - 1].ApproximatelyEquals(zero, tolerance))
            {
                return new Solution<T[]>(ExitCode.InconsistentEquations, "Equations are inconsistent. No solution exists.");
            }
            if (m == n && A[n - 1][n - 1].ApproximatelyEquals(zero, tolerance))
            {
                return new Solution<T[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solve using backsubstitution
            T[] x = new T[n];
            for (int i = n - 1; i >= 0; --i)
            {
                T[] A_i = A[i];
                T sum = b[i];
                for (int j = n - 1; j > i; j--)
                {
                    sum = sum.Subtract(x[j].Multiply(A_i[j]));
                }
                x[i] = sum.Divide(A_i[i]);
            }
            return new Solution<T[]>(ExitCode.Success, x);
        }
        /// <summary>
        /// The complex method differs from that of a general Field because C 
        /// is a normed vector space. 
        /// We use the norm to pivot to ensure greater numerical stability
        /// </summary>
        /// <param name="Amatrix"></param>
        /// <param name="bVect"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static Solution<Complex[]> Solve(Complex[,] Amatrix, Complex[] bVect, double tolerance = Precision.DOUBLE_PRECISION)
        {
            MatrixChecks.CheckNotNull(Amatrix);
            MatrixChecks.CheckNotNull(bVect);

            int m = Amatrix.GetLength(0), n = Amatrix.GetLength(1);
            if (m != bVect.Length)
            {
                throw new InvalidOperationException("Dimensions don't match.");
            }
            if (m < n)
            {
                return new Solution<Complex[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solving the linear system requires O(n^3) time, it's worthwhile to spend O(n^2) 
            // time converting T[,] into the more efficient T[][] matrix
            Complex[][] A = Amatrix.ToJagged();
            Complex[] b = bVect.Copy();

            // The method as it stands suffers from numerical instability
            // Gaussian elimination
            for (int c = 0; c < n; ++c)
            {
                // pivot on the row r with largest magnitude on element _A[r][c]
                double max = A[c][c].Modulus();
                int swap = c;
                for (int r = c + 1; r < m; ++r)
                {
                    double abs = A[r][c].Modulus();
                    if (abs > max)
                    {
                        max = abs;
                        swap = r;
                    }
                }

                // swap the row
                Complex[] tempRow = A[swap];
                A[swap] = A[c];
                A[c] = tempRow;

                // swap the b
                Complex tempB = b[swap];
                b[swap] = b[c];
                b[c] = tempB;

                // perform elimination on row c
                Complex[] A_c = A[c];
                Complex b_c = b[c], A_cc_inv = A_c[c].MultiplicativeInverse();
                for (int r = c + 1; r < m; ++r)
                {
                    Complex[] A_r = A[r];
                    Complex s = A_r[c].Multiply(A_cc_inv);
                    for (int i = c; i < n; i++)
                    {
                        A_r[i] = A_r[i].Subtract(s.Multiply(A_c[i]));
                    }
                    b[r] = b[r].Subtract(s.Multiply(b_c));
                }
            }

            if (m > n && !A[m - 1][n - 1].ApproximatelyEquals(Complex.Zero, tolerance))
            {
                return new Solution<Complex[]>(ExitCode.InconsistentEquations, "Equations are inconsistent. No solution exists.");
            }
            if (m == n && A[n - 1][n - 1].ApproximatelyEquals(Complex.Zero, tolerance))
            {
                return new Solution<Complex[]>(ExitCode.InfiniteSolutions, "Not enough equations to solve this system. Infinite solutions exist.");
            }

            // Solve using backsubstitution
            Complex[] x = new Complex[n];
            for (int i = n - 1; i >= 0; --i)
            {
                Complex[] A_i = A[i];
                Complex sum = b[i];
                for (int j = n - 1; j > i; j--)
                {
                    sum = sum.Subtract(x[j].Multiply(A_i[j]));
                }
                x[i] = sum.Divide(A_i[i]);
            }
            return new Solution<Complex[]>(ExitCode.Success, x);
        }
    }
}
