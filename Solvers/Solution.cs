﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Solvers
{
    public class Solution<T>
    {
        public bool Success { get; private set; }
        public ExitCode ExitCode { get; private set; }
        public string Error { get; private set; }
        public T Result { get; private set; }

        public Solution(ExitCode exitCode, T result)
        {
            ExitCode = exitCode;
            Success = true;
            Error = null;
            Result = result;
        }
        public Solution(ExitCode exitCode, string error)
        {
            ExitCode = exitCode;
            Success = false;
            Error = error;
            Result = default(T);
        }
    }
    public enum ExitCode
    {
        Success,
        InconsistentEquations,
        InfiniteSolutions,
        EarlyTermination
    }
}
