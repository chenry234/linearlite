﻿using LinearLite.Structs.Spaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    public struct Complex : Field<Complex>
    {
        public double Real;
        public double Imaginary;

        public static readonly Complex Zero = new Complex(0);
        public static readonly Complex One = new Complex(1);
        public static readonly Complex I = new Complex(0, 1);
        public static readonly Complex NaN = new Complex(double.NaN, double.NaN);


        /// <summary>
        /// Constructor for the complex number x + yi
        /// </summary>
        /// <param name="x">The real part of the complex number.</param>
        /// <param name="y">(Optional) The imaginary part of the complex number.</param>
        public Complex(double x, double y = 0)
        {
            Real = x;
            Imaginary = y;
        }

        /// <summary>
        /// Construct a complex number, z, using modulus |z| and argument arg(z) in radians
        /// </summary>
        /// <param name="modulus">The modulus of the complex number</param>
        /// <param name="argument">The argument of the complex number</param>
        /// <returns></returns>
        public static Complex FromPolar(double modulus, double argument)
        {
            if (modulus < 0)
            {
                throw new ArgumentOutOfRangeException("Modulus of a complex number must be positive");
            }
            return new Complex(modulus * Math.Cos(argument), modulus * Math.Sin(argument));
        }

        /// <summary>
        /// Evaluate e^z where z is complex
        /// </summary>
        /// <param name="power"></param>
        /// <returns>e^z for complex z</returns>
        public static Complex Exp(Complex power)
        {
            return FromPolar(Math.Exp(power.Real), power.Imaginary);
        }

        /// <summary>
        /// Evaluate z^s for some real value s and complex z
        /// </summary>
        /// <param name="bse">A complex number z</param>
        /// <param name="power">A scalar representing the power to raise z to.</param>
        /// <returns>Returns z^s</returns>
        public static Complex Pow(Complex bse, double power)
        {
            return FromPolar(Math.Pow(bse.Modulus(), power), power * bse.Argument());
        }

        /// <summary>
        /// Returns the natural logarithm of a complex number x, ln(x)
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static Complex Log(Complex x)
        {
            double mod = x.Modulus();
            double arg = x.Argument();

            if (mod == 0)
            {
                return new Complex(double.NaN, double.NaN);
            }

            return new Complex(Math.Log(mod), arg);
        }

        public static bool TryParse(string s, out Complex c)
        {
            if (string.IsNullOrEmpty(s))
            {
                c = Zero;
                return false;
            }
            string[] parts = s.Split('+');
            if (parts.Length != 2)
            {
                c = Zero;
                return false;
            }

            string im = parts[1];
            if (!im.EndsWith("i"))
            {
                c = Zero;
                return false;
            }
            im = im.Substring(0, im.Length - 1);

            double _re, _im;
            if (double.TryParse(parts[0].Trim(), out _re) && double.TryParse(im.Trim(), out _im))
            {
                c = new Complex(_re, _im);
                return true;
            }

            c = Zero;
            return false;
        }

        public static Complex Sqrt(Complex c)
        {
            return Pow(c, 0.5);
        }
        public static bool IsNaN(Complex c)
        {
            return double.IsNaN(c.Real) || double.IsNaN(c.Imaginary);
        }
        public static bool IsNaNOrInfinity(Complex c)
        {
            return double.IsNaN(c.Real) || double.IsNaN(c.Imaginary) || double.IsInfinity(c.Real) || double.IsInfinity(c.Imaginary);
        }

        public static Complex operator +(Complex c1, Complex c2)
        {
            return c1.Add(c2);
        }
        public static Complex operator +(Complex c, double d)
        {
            return new Complex(c.Real + d, c.Imaginary);
        }
        public static Complex operator +(Complex c, float f) => c + (double)f;
        public static Complex operator +(Complex c, int i) => c + (double)i;
        public static Complex operator +(Complex c, decimal d) => c + (double)d;
        public static Complex operator +(double d, Complex c) => c + d;
        public static Complex operator +(float f, Complex c) => c + (double)f;
        public static Complex operator +(int i, Complex c) => c + (double)i;
        public static Complex operator +(decimal d, Complex c) => c + (double)d;

        public static Complex operator -(Complex c1, Complex c2)
        {
            return c1.Subtract(c2);
        }
        public static Complex operator -(Complex c, double d)
        {
            return new Complex(c.Real - d, c.Imaginary);
        }
        public static Complex operator -(Complex c, int i) => c - (double)i;
        public static Complex operator -(Complex c, float f) => c - (double)f;
        public static Complex operator -(Complex c, decimal d) => c - (double)d;
        public static Complex operator -(double d, Complex c)
        {
            return new Complex(d - c.Real, -c.Imaginary);
        }
        public static Complex operator -(int i, Complex c) => (double)i - c;
        public static Complex operator -(float f, Complex c) => (double)f - c;
        public static Complex operator -(decimal d, Complex c) => (double)d - c;

        public static Complex operator *(Complex c1, Complex c2)
        {
            return c1.Multiply(c2);
        }
        public static Complex operator *(Complex c, double d)
        {
            return new Complex(c.Real * d, c.Imaginary * d);
        }
        public static Complex operator *(Complex c, int i) => c * (double)i;
        public static Complex operator *(Complex c, float f) => c * (double)f;
        public static Complex operator *(Complex c, decimal d) => c * (double)d;
        public static Complex operator *(double d, Complex c) => c * d;
        public static Complex operator *(int i, Complex c) => c * (double)i;
        public static Complex operator *(float f, Complex c) => c * (double)f;
        public static Complex operator *(decimal d, Complex c) => c * (double)d;

        public static Complex operator -(Complex c)
        {
            return c.AdditiveInverse();
        }
        public static Complex operator /(Complex c1, Complex c2)
        {
            return c1.Divide(c2);
        }
        public static Complex operator /(Complex c, double d) => c * (1 / d);
        public static Complex operator /(Complex c, int i) => c / (double)i;
        public static Complex operator /(Complex c, float f) => c / (double)f;
        public static Complex operator /(Complex c, decimal d) => c / (double)d;
        public static Complex operator /(double d, Complex c) => new Complex(d) / c;
        public static Complex operator /(int i, Complex c) => (double)i / c;
        public static Complex operator /(float f, Complex c) => (double)f / c;
        public static Complex operator /(decimal d, Complex c) => (double)d / c;

        public static implicit operator Complex(double d)
        {
            return new Complex(d);
        }
        public static implicit operator Complex(int i)
        {
            return new Complex(i);
        }
        public static implicit operator Complex(float f)
        {
            return new Complex(f);
        }
        public static implicit operator Complex(decimal d)
        {
            return new Complex((double)d);
        }

        public override string ToString()
        {
            return ToString("n4");
        }
        public string ToString(string format)
        {
            if (Imaginary < 0)
            {
                return Real.ToString(format) + " - " + Math.Abs(Imaginary).ToString(format) + "i";
            }
            return Real.ToString(format) + " + " + Imaginary.ToString(format) + "i";
        }

        public Complex AdditiveIdentity => Zero;
        public Complex MultiplicativeIdentity => One;
        public Complex Add(Complex e)
        {
            return new Complex(Real + e.Real, Imaginary + e.Imaginary);
        }
        public Complex Multiply(Complex e)
        {
            return new Complex(
                Real * e.Real - Imaginary * e.Imaginary,
                Real * e.Imaginary + Imaginary * e.Real);
        }
        public Complex Multiply(double s)
        {
            // Method provided for performance reasons only
            return new Complex(Real * s, Imaginary * s);
        }
        public Complex Subtract(Complex e)
        {
            return new Complex(Real - e.Real, Imaginary - e.Imaginary);
        }
        public Complex AdditiveInverse()
        {
            return new Complex(-Real, -Imaginary);
        }
        public Complex MultiplicativeInverse()
        {
            double divisor = Real * Real + Imaginary * Imaginary;
            if (divisor == 0)
            {
                return new Complex(double.NaN, double.NaN);
            }
            return new Complex(Real / divisor, -Imaginary / divisor);
        }
        public bool Equals(Complex e)
        {
            return Real == e.Real && Imaginary == e.Imaginary;
        }
        public bool ApproximatelyEquals(Complex e, double eps)
        {
            return Util.ApproximatelyEquals(Real, e.Real, eps) && Util.ApproximatelyEquals(Imaginary, e.Imaginary, eps);
        }

        #region internal modifiers
        internal void IncrementBy(Complex e)
        {
            Real += e.Real;
            Imaginary += e.Imaginary;
        }
        internal void DecrementBy(Complex e)
        {
            Real -= e.Real;
            Imaginary -= e.Imaginary;
        }
        internal void MultiplyEquals(double s)
        {
            Real *= s;
            Imaginary *= s;
        }
        internal void MultiplyEquals(Complex e)
        {
            double re = Real;
            Real = re * e.Real - Imaginary * e.Imaginary;
            Imaginary = re * e.Imaginary + Imaginary * e.Real;
        }
        internal void Negate()
        {
            Real = -Real;
            Imaginary = -Imaginary;
        }
        internal void Set(double real, double imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }
        #endregion internal modifiers
    }
    public static class ComplexMethodsExtensions
    {
        /// <summary>
        /// Returns the modulus of the complex number, |z|
        /// </summary>
        public static double Modulus(this Complex c)
        {
            return Util.Hypot(c.Real, c.Imaginary);
        }

        /// <summary>
        /// Returns the argument of the complex number, in radians, in the range [-pi/2, pi/2]
        /// </summary>
        public static double Argument(this Complex c)
        {
            if (c.Real == 0)
            {
                if (c.Imaginary == 0) return double.NaN;
                if (c.Imaginary > 0) return Math.PI / 2;
                return -Math.PI / 2;
            }
            return Math.Atan2(c.Imaginary, c.Real);
        }

        /// <summary>
        /// Returns the complex conjugate of some complex number c.
        /// I.e. if c = a + bi, then a - bi is returned.
        /// </summary>
        /// <param name="c">A complex number</param>
        /// <returns>The complex conjugate of c</returns>
        public static Complex Conjugate(this Complex c)
        {
            return new Complex(c.Real, -c.Imaginary);
        }

        /// <summary>
        /// Returns whether a complex number is the conjugate of another, up to 'tolerance'
        /// </summary>
        /// <param name="c"></param>
        /// <param name="other"></param>
        /// <param name="tolerance">Defaults to 1e-8</param>
        /// <returns>true if the two complex numbers are complex conjugates of each other.</returns>
        public static bool IsConjugateOf(this Complex c, Complex other, double tolerance = 1e-8)
        {
            return Util.ApproximatelyEquals(c.Real, other.Real, tolerance) && 
                Util.ApproximatelyEquals(c.Imaginary, -other.Imaginary, tolerance); 
        }

        /// <summary>
        /// Determines whether the complex number is real within 'tolerance', i.e. |Im(z)| < tolerance
        /// </summary>
        /// <param name="tolerance">The tolerance, defaults to 1e-8</param>
        /// <returns>true if the complex number is real</returns>
        public static bool IsReal(this Complex c, double tolerance = 1e-8)
        {
            return Util.ApproximatelyEquals(c.Imaginary, 0, tolerance);
        }

        /// <summary>
        /// Determines whether the complex number is purely imaginary within 'tolerance', i.e. |Re(z)| < tolerance
        /// </summary>
        /// <param name="tolerance">The tolerance, defaults to 1e-8</param>
        /// <returns>true if the complex number is purely imaginary</returns>
        public static bool IsImaginary(this Complex c, double tolerance = 1e-8)
        {
            return Util.ApproximatelyEquals(c.Real, 0, tolerance);
        }

        internal static Complex Random(Random _random, double maxModulus)
        {
            return Complex.FromPolar(_random.NextDouble() * maxModulus, _random.NextDouble() * Math.PI * 2);
        }
    }
}
