﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    /// <summary>
    /// Implements a Field structure
    /// 
    /// The following methods are inherited from Group<T>:
    /// T AdditiveIdentity { get; }
    /// T Add(T e);
    /// T AdditiveInverse();
    /// 
    /// The following methods are inherited from Ring<T>:
    /// T MultiplicativeIdentity { get; }
    /// T Multiply(T e);
    /// 
    /// The following methods are inherited from DivisionRing<T>
    /// T MultiplicativeInverse();
    /// 
    /// As with DivisionRing<T>, Field<T> implements an 
    /// multiplicative inverse method (and subsequently a 
    /// 'division' method) which is not present in Ring<T>
    /// 
    /// Unlike DivisionRing<T>, Field<T> multiplication must be 
    /// commutative. This is never enforced in the interface 
    /// level, and we leave it up to the user to ensure that 
    /// their Field<T> multiplication implementations do in fact 
    /// commute. Failure to do so may result in unexpected results 
    /// when using Field<T> methods.
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface Field<T> : DivisionRing<T>, CommutativeRing<T> where T : new()
    {

    }

}
