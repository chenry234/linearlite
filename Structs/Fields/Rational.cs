﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    public struct Rational : Field<Rational>, IComparable<Rational>
    {
        public readonly int Numerator;
        public readonly int Denominator;

        public static Rational One = new Rational(1, 1);
        public static Rational Zero = new Rational(0, 1);

        public Rational MultiplicativeIdentity => One;
        public Rational AdditiveIdentity => Zero;

        public Rational(int integer)
        {
            Numerator = integer;
            Denominator = 1;
        }
        public Rational(int num, int den)
        {
            if (den < 0)
            {
                Numerator = -num;
                Denominator = -den;
            }
            else
            {
                Numerator = num;
                Denominator = den;
            }
        }

        public Rational Simplify()
        {
            int gcd = GreatestCommonDivisor(Numerator, Denominator);
            return new Rational(Numerator / gcd, Denominator / gcd);
        }
        private int GreatestCommonDivisor(int a, int b)
        {
            if (a == 0) return b;
            if (b == 0) return a;

            if (a > b)
            {
                return GreatestCommonDivisor(a % b, b);
            }
            return GreatestCommonDivisor(a, b % a);
        }

        public Rational Add(Rational e)
        {
            return new Rational(Numerator * e.Denominator + e.Numerator * Denominator, Denominator * e.Denominator);
        }
        public Rational AdditiveInverse()
        {
            return new Rational(-Numerator, Denominator);
        }
        public Rational Multiply(Rational e)
        {
            return new Rational(Numerator * e.Numerator, Denominator * e.Denominator);
        }
        public Rational MultiplicativeInverse()
        {
            return new Rational(Denominator, Numerator);
        }

        public bool Equals(Rational e)
        {
            return Numerator * e.Denominator == e.Numerator * Denominator;
        }
        public bool ApproximatelyEquals(Rational e, double eps)
        {
            return Equals(e); // fractions are exact forms
        }

        public int CompareTo(Rational e)
        {
            return Numerator * e.Denominator - e.Numerator * Denominator;
        }

        public override string ToString()
        {
            return $"{Numerator}/{Denominator}";
        }

        public double ToDouble()
        {
            return Numerator / (double)Denominator;
        }

        #region Operator overrides 
        public static Rational operator +(Rational f, Rational g)
        {
            return f.Add(g);
        }
        public static Rational operator -(Rational f, Rational g)
        {
            return f.Subtract(g);
        }
        public static Rational operator *(Rational f, Rational g)
        {
            return f.Multiply(g);
        }
        public static Rational operator /(Rational f, Rational g)
        {
            return f.Divide(g);
        }
        public static Rational operator -(Rational f)
        {
            return f.AdditiveInverse();
        }
        public static implicit operator Rational(int i)
        {
            return new Rational(i);
        }
        #endregion
    }
}
