﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    /// <summary>
    /// Implements the algebraic structure of an abelian group, which is 
    /// a group for which the * operation is commutative, i.e. 
    /// a * b = b * a for any a, b in set T.
    /// 
    /// We leave the enforcement of commutativity to the implementation of 
    /// Operate(T e) method. Please note that if Operate(T e) is not 
    /// commutative, then unexpected results may be produced.
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface AbelianGroup<T> : Group<T> where T : new()
    {

    }
}
