﻿namespace LinearLite.Structs
{
    /// <summary>
    /// Imterface for a group G = (S, +) where S is a set of elements of type T
    /// This group does not extend the Group<T> interface since the method has 
    /// different names 
    /// 
    /// The interface: inherits:
    /// - Add(T e) from AdditiveSemigroup<T>
    /// - T AdditiveIdentity { get; } from AdditiveMonoid<T>
    /// </summary>
    public interface AdditiveGroup<T> : AdditiveMonoid<T> where T : new()
    {
        /// <summary>
        /// Calculates the inversion operation of (this) 
        /// Guaranteed to exist since every element of a group 
        /// has an inverse
        /// </summary>
        /// <returns></returns>
        T AdditiveInverse();
    }
    public static class AdditiveGroupExtensions
    {
        public static T Subtract<T>(this T e, T f) where T : AdditiveGroup<T>, new()
        {
            return e.Add(f.AdditiveInverse());
        }
    }
}
