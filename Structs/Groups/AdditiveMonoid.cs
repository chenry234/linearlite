﻿using LinearLite.Structs;

namespace LinearLite.Structs
{
    /// <summary>
    /// A monoid structure whose binary operation is addition
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface AdditiveMonoid<T> : AdditiveSemigroup<T> where T : new()
    {
        /// <summary>
        /// The identity element e, for operation +, which satisfies 
        /// e + g = g + e = g for any element g in set T.
        /// </summary>
        T AdditiveIdentity { get; }
    }
}
