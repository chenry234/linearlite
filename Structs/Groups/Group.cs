﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    /// <summary>
    /// Implements a group, which is a pair (S, *) for some set S and 
    /// binary operation * : S * S -> S, for which a inverse exists for 
    /// every element in S, and an identity element exists. 
    /// 
    /// A group is hence also:
    /// - A monoid with an inverse
    /// - A semigroup with an identity element and an inverse
    /// 
    /// The following methods are inherited from Algebra<T>:
    /// - Equals(T e);
    /// - ApproximatelyEquals(T e);
    /// 
    /// The following methods are inherited from Semigroup<T>
    /// - Operate(T e);
    /// 
    /// The following methods are inherited from Monoid<T>
    /// - T Identity { get; }
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface Group<T> : Monoid<T> where T : new()
    {
        /// <summary>
        /// Calculates the inversion operation of (this) 
        /// </summary>
        /// <returns></returns>
        T OperatorInverse();
    }
}
