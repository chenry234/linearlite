﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    /// <summary>
    /// Implements a monoid algebraic structure over the set T, and operation * : T x T -> T
    /// (T is closed under *), plus an identity element in T, e, such that e * g = g * e = g
    /// for every element g in T.
    /// 
    /// The operation is not necessarily commutative. 
    /// A monoid is a semigroup with an identity element
    /// 
    /// The following methods are inherited from Semigroup:
    /// T Operate(T e);
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface Monoid<T> : Semigroup<T> where T : new()
    {
        /// <summary>
        /// The identity element e, for operation *, which satisfies 
        /// e * g = g * e = g for any element g in set T.
        /// </summary>
        T Identity { get; }
    }
}
