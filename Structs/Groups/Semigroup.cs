﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    public interface Semigroup<T>  : Algebra<T> where T : new()
    {
        /// <summary>
        /// The binary operation * : T x T -> T
        /// Calculate and returns (this) * g for some element g in the set T.
        /// 
        /// Note that in semigroups, the binary operation is associative.
        /// </summary>
        /// <param name="g">Some element from the set T</param>
        /// <returns>The result of the binary operation</returns>
        T Operate(T g);
    }
}
