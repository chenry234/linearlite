﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Matrix
{
    /// <summary>
    /// A memory-efficient band-matrix where the only non-zero elements are along a diagonal band
    /// 
    /// This structure is suitable for large matrices for which the non-zero elements all lie 
    /// along a thin band near the main diagonal.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BandMatrix<T> : IMatrix<T> where T : new()
    {
        private readonly T _default;

        /// <summary>
        /// _upperBandwidth refers to the number of non-zero diagonals above the leading diagonal,
        /// _lowerBandwidth refers to the number of non-zero diagonals below the leading diagonal.
        /// </summary>
        private int _upperBandwidth, _lowerBandwidth;

        /// <summary>
        /// Bands are arranged diagonally,
        /// With the 0-index closest to the main diagonal
        /// </summary>
        private T[][] _upperBands, _lowerBands;
        private T[] _diagonal;

        public override T[] this[int index]
        {
            get
            {
                if (index < 0 || index >= Rows) throw new ArgumentOutOfRangeException();

                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public override T this[int rowIndex, int columnIndex]
        {
            get
            {
                if (rowIndex < 0 || rowIndex >= Rows || columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                int diff = rowIndex - columnIndex;
                if (diff < -_upperBandwidth || diff > _lowerBandwidth)
                {
                    return _default;
                }

                if (diff > 0)
                {
                    return _lowerBands[diff - 1][columnIndex];
                }
                if (diff < 0)
                {
                    return _upperBands[-1 - diff][rowIndex];
                }

                return _diagonal[rowIndex];
            }
            set
            {
                if (rowIndex < 0 || rowIndex >= Rows || columnIndex < 0 || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                int diff = rowIndex - columnIndex;
                if (diff < -_upperBandwidth || diff > _lowerBandwidth)
                {
                    return;
                }

                if (diff > 0)
                {
                    _lowerBands[diff - 1][columnIndex] = value;
                }
                else if (diff < 0)
                {
                    _upperBands[diff - 1][rowIndex] = value;
                }
                else
                {
                    _diagonal[rowIndex] = value;
                }
            }
        }
        internal T[] Diagonal { get { return _diagonal; } }
        internal T[][] LowerBands { get { return _lowerBands; } }
        internal T[][] UpperBands { get { return _upperBands; } }

        public int UpperBandwidth { get { return _upperBandwidth; } }
        public int LowerBandwidth { get { return _lowerBandwidth; } }
        public bool IsDiagonal { get { return _lowerBandwidth == 0 && _upperBandwidth == 0; } }
        public bool IsUpperBidiagonal { get { return _lowerBandwidth == 0 && _upperBandwidth == 1; } }
        public bool IsLowerBidiagonal { get { return _lowerBandwidth == 1 && _upperBandwidth == 0; } }
        public bool IsBidiagonal { get { return IsUpperBidiagonal || IsLowerBidiagonal; } }
        public bool IsTridiagonal { get { return _lowerBandwidth == 1 && _upperBandwidth == 1; } }
        public bool IsPentadiagonal { get { return _lowerBandwidth == 2 && _upperBandwidth == 2; } }

        /// <summary>
        /// Constructor for a band matrix with a specified number of bands above and below the leading diagonal.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="bandsAboveLeadingDiagonal"></param>
        /// <param name="bandsBelowLeadingDiagonal"></param>
        public BandMatrix(int rows, int columns, int bandsAboveLeadingDiagonal, int bandsBelowLeadingDiagonal) : base(rows, columns)
        {
            _default = new T();
            _upperBandwidth = bandsAboveLeadingDiagonal;
            _lowerBandwidth = bandsBelowLeadingDiagonal;

            int leadingDiagonalDim = Math.Min(rows, columns);
            
            _upperBands = new T[_upperBandwidth][];
            for (int b = 0; b < _upperBandwidth; ++b)
            {
                _upperBands[b] = new T[Math.Min(leadingDiagonalDim, columns - b - 1)];
            }
            _diagonal = new T[leadingDiagonalDim];

            _lowerBands = new T[_lowerBandwidth][];
            for (int b = 0; b < _lowerBandwidth; ++b)
            {
                _lowerBands[b] = new T[Math.Min(leadingDiagonalDim, rows - b - 1)];
            }
        }
        internal BandMatrix(int rows, int columns, T[][] upperBands, T[] diagonal, T[][] lowerBands) : base(rows, columns)
        {
            _upperBands = upperBands;
            _diagonal = diagonal;
            _lowerBands = lowerBands;
            _upperBandwidth = upperBands.Length;
            _lowerBandwidth = lowerBands.Length;
        }
        
        private T[][] AddBands(T[][] A, T[][] B, Func<T[], T[], T[]> Add)
        {
            int min = Math.Min(A.Length, B.Length), max = Math.Max(A.Length, B.Length);

            T[][] sum = new T[max][];
            for (int i = 0; i < min; ++i)
            {
                sum[i] = Add(A[i], B[i]);
            }

            if (A.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    sum[i] = A[i].Copy();
                }
            }

            else if (B.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    sum[i] = B[i].Copy();
                }
            }
            return sum;
        }
        private T[][] SubtractBands(T[][] A, T[][] B, Func<T[], T[], T[]> Subtract, Func<T[], T[]> Negate)
        {
            int min = Math.Min(A.Length, B.Length), max = Math.Max(A.Length, B.Length);

            T[][] diff = new T[max][];
            for (int i = 0; i < min; ++i)
            {
                diff[i] = Subtract(A[i], B[i]);
            }

            if (A.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    diff[i] = A[i].Copy();
                }
            }
            else if (B.Length > min)
            {
                for (int i = min; i < max; ++i)
                {
                    diff[i] = Negate(B[i]);
                }
            }
            return diff;
        }

        internal BandMatrix<T> Add(BandMatrix<T> matrix, Func<T[], T[], T[]> Add)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.CheckMatrixDimensionsEqual(this, matrix);

            T[][] upperBandSum = AddBands(_upperBands, matrix._upperBands, Add);
            T[] diagonal = Add(_diagonal, matrix._diagonal);
            T[][] lowerBandSum = AddBands(_lowerBands, matrix._lowerBands, Add);
            return new BandMatrix<T>(Rows, Columns, upperBandSum, diagonal, lowerBandSum);
        }
        internal BandMatrix<T> Subtract(BandMatrix<T> matrix, Func<T[], T[], T[]> Subtract, Func<T[], T[]> Negate)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.CheckMatrixDimensionsEqual(this, matrix);

            T[][] upperBandSum = SubtractBands(_upperBands, matrix._upperBands, Subtract, Negate);
            T[] diagonal = Subtract(_diagonal, matrix._diagonal);
            T[][] lowerBandSum = SubtractBands(_lowerBands, matrix._lowerBands, Subtract, Negate);
            return new BandMatrix<T>(Rows, Columns, upperBandSum, diagonal, lowerBandSum);
        }
        internal BandMatrix<T> Multiply(BandMatrix<T> matrix, IBLAS<T> blas)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.CheckMatrixDimensionsForMultiplication(this, matrix);

            int upper = _upperBandwidth + matrix._upperBandwidth;
            int lower = _lowerBandwidth + matrix._lowerBandwidth;

            BandMatrix<T> product = new BandMatrix<T>(Rows, matrix.Columns, upper, lower);

            T[][] prod_upper = product._upperBands,
                prod_lower = product._lowerBands,
                A_upper = _upperBands,
                A_lower = _lowerBands,
                B_upper = matrix._upperBands, 
                B_lower = matrix._lowerBands;

            T[] prod_diag = product._diagonal,
                A_diag = _diagonal,
                B_diag = matrix._diagonal,
                A_row = new T[Columns],
                B_col = new T[Columns];

            int row_end = Math.Min(Rows, Math.Min(product.Rows, product.Columns) + lower), i, j, k;
            for (i = 0; i < row_end; ++i)
            {
                int i_ = i + 1,
                    _i = i - 1,
                    col_start = Math.Max(0, i - lower), 
                    col_end = Math.Min(matrix.Columns, i_ + upper), 
                    start_lower_bound = Math.Max(0, i - _lowerBandwidth), 
                    end_upper_bound = Math.Min(Rows, i_ + _upperBandwidth);

                for (j = col_start; j < col_end; ++j)
                {
                    int j_ = j + 1, 
                        _j = j - 1, 
                        k_start = Math.Max(start_lower_bound, j - matrix._upperBandwidth), 
                        k_end = Math.Min(end_upper_bound, j_ + matrix._lowerBandwidth);

                    for (k = k_start; k < k_end; ++k)
                    {
                        if (k > i)
                        {
                            A_row[k] = A_upper[k - i_][i];
                        }
                        else if (k < i)
                        {
                            A_row[k] = A_lower[_i - k][k];
                        }
                        else
                        {
                            A_row[k] = A_diag[i];
                        }

                        if (k > j)
                        {
                            B_col[k] = B_lower[k - j_][j];
                        }
                        else if (k < j)
                        {
                            B_col[k] = B_upper[_j - k][k];
                        }
                        else
                        {
                            B_col[k] = B_diag[j];
                        }
                    }

                    T sum = blas.DOT(A_row, B_col, k_start, k_end);
                    if (j > i)
                    {
                        prod_upper[j - i_][i] = sum;
                    }
                    else if (j < i)
                    {
                        prod_lower[_i - j][j] = sum;
                    }
                    else
                    {
                        prod_diag[i] = sum;
                    }
                }
            }
            return product;
        }
        internal T[] Multiply(T[] x, IBLAS<T> blas)
        {
            int rows = Rows, cols = Columns, i, j;

            T[][] upper = _upperBands,
                lower = _lowerBands;
            T[] diagonal = _diagonal,
                workspace = new T[cols],
                product = new T[rows];

            for (i = 0; i < rows; ++i)
            {
                int i_ = i + 1, 
                    _i = i - 1, 
                    start = Math.Max(0, i - _lowerBandwidth), 
                    end = Math.Min(cols, i_ + _upperBandwidth);
                for (j = start; j < end; ++j)
                {
                    if (j > i)
                    {
                        workspace[j] = upper[j - i_][i];
                    }
                    else if (j < i)
                    {
                        workspace[j] = lower[_i - j][j];
                    }
                    else
                    {
                        workspace[i] = diagonal[i];
                    }
                }
                product[i] = blas.DOT(workspace, x, start, end);
            }
            return product;
        }
        internal BandMatrix<T> Multiply(T s, IBLAS<T> blas)
        {
            T[][] upper = new T[_upperBandwidth][];
            for (int i = 0; i < _upperBandwidth; ++i)
            {
                T[] x = _upperBands[i].Copy();
                blas.SCAL(x, s, 0, x.Length);
                upper[i] = x;
            }

            T[][] lower = new T[_lowerBandwidth][];
            for (int i = 0; i < _lowerBandwidth; ++i)
            {
                T[] x = _lowerBands[i].Copy();
                blas.SCAL(x, s, 0, x.Length);
                lower[i] = x;
            }

            T[] diag = _diagonal.Copy();
            blas.SCAL(diag, s, 0, diag.Length);

            return new BandMatrix<T>(Rows, Columns, upper, diag, lower);
        }
        internal BandMatrix<T> MultiplyParallel(BandMatrix<T> matrix, IBLAS<T> blas)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.CheckMatrixDimensionsForMultiplication(this, matrix);

            int upper = _upperBandwidth + matrix._upperBandwidth;
            int lower = _lowerBandwidth + matrix._lowerBandwidth;

            BandMatrix<T> product = new BandMatrix<T>(Rows, matrix.Columns, upper, lower);

            T[][] prod_upper = product._upperBands,
                prod_lower = product._lowerBands,
                A_upper = _upperBands,
                A_lower = _lowerBands,
                B_upper = matrix._upperBands,
                B_lower = matrix._lowerBands;

            T[] prod_diag = product._diagonal,
                A_diag = _diagonal,
                B_diag = matrix._diagonal,
                A_row = new T[Columns],
                B_col = new T[Columns];

            int row_end = Math.Min(Rows, Math.Min(product.Rows, product.Columns) + lower);
            Parallel.ForEach(Partitioner.Create(0, row_end), range =>
            {
                int min = range.Item1, max = range.Item2, i, j, k;
                for (i = min; i < max; ++i)
                {
                    int i_ = i + 1,
                        _i = i - 1,
                        col_start = Math.Max(0, i - lower),
                        col_end = Math.Min(matrix.Columns, i_ + upper),
                        start_lower_bound = Math.Max(0, i - _lowerBandwidth),
                        end_upper_bound = Math.Min(Rows, i_ + _upperBandwidth);

                    for (j = col_start; j < col_end; ++j)
                    {
                        int j_ = j + 1,
                            _j = j - 1,
                            k_start = Math.Max(start_lower_bound, j - matrix._upperBandwidth),
                            k_end = Math.Min(end_upper_bound, j_ + matrix._lowerBandwidth);

                        for (k = k_start; k < k_end; ++k)
                        {
                            if (k > i)
                            {
                                A_row[k] = A_upper[k - i_][i];
                            }
                            else if (k < i)
                            {
                                A_row[k] = A_lower[_i - k][k];
                            }
                            else
                            {
                                A_row[k] = A_diag[i];
                            }

                            if (k > j)
                            {
                                B_col[k] = B_lower[k - j_][j];
                            }
                            else if (k < j)
                            {
                                B_col[k] = B_upper[_j - k][k];
                            }
                            else
                            {
                                B_col[k] = B_diag[j];
                            }
                        }

                        T sum = blas.DOT(A_row, B_col, k_start, k_end);
                        if (j > i)
                        {
                            prod_upper[j - i_][i] = sum;
                        }
                        else if (j < i)
                        {
                            prod_lower[_i - j][j] = sum;
                        }
                        else
                        {
                            prod_diag[i] = sum;
                        }
                    }
                }
            });
            
            return product;
        }
        internal T Trace(IBLAS<T> blas)
        {
            int len = _diagonal.Length, i;
            T sum = blas.Zero;
            for (i = 0; i < len; ++i)
            {
                sum = blas.Add(sum, _diagonal[i]);
            }
            return sum;
        }
        
        public T[] Row(int rowIndex)
        {
            T[] row = new T[Columns];
            Row(rowIndex, row, true);
            return row;
        }
        public void Row(int rowIndex, T[] row, bool clear = false)
        {
            if (Columns != row.Length)
            {
                throw new InvalidOperationException();
            }
            if (rowIndex < 0 || rowIndex >= Rows)
            {
                throw new ArgumentOutOfRangeException();
            }

            row[rowIndex] = _diagonal[rowIndex];

            int i, j;
            for (i = 0, j = rowIndex - 1; i < _lowerBandwidth; ++i, --j)
            {
                if (j < 0)
                {
                    break;
                }
                row[j] = _lowerBands[i][j];
            }
            if (clear)
            {
                for (; j >= 0; --j)
                {
                    row[j] = _default;
                }
            }

            for (i = 0, j = rowIndex + 1; i < _upperBandwidth; ++i, ++j) 
            {
                if (j >= Columns)
                {
                    break;
                }
                row[j] = _upperBands[i][rowIndex];
            }
            if (clear)
            {
                for (; j < Columns; ++j)
                {
                    row[j] = _default;
                }
            }
        }

        internal bool ApproximatelyEquals(BandMatrix<T> matrix, Func<T, T, bool> Equals)
        {
            if (Rows != matrix.Rows || 
                Columns != matrix.Columns ||
                _upperBandwidth != matrix._upperBandwidth || 
                _lowerBandwidth != matrix._lowerBandwidth)
            {
                return false;
            }

            if (!ApproximatelyEquals(_upperBands, matrix._upperBands, Equals))
            {
                return false;
            }
            if (!ApproximatelyEquals(_lowerBands, matrix._lowerBands, Equals))
            {
                return false;
            }
            if (!ApproximatelyEquals(_diagonal, matrix._diagonal, Equals))
            {
                return false;
            }
            return true;
        }
        private bool ApproximatelyEquals(T[][] u, T[][] v, Func<T, T, bool> Equals)
        {
            if (u.Length != v.Length)
            {
                return false;
            }

            int len = u.Length, i;
            for (i = 0; i < len; ++i) 
            {
                if (!ApproximatelyEquals(u[i], v[i], Equals))
                {
                    return false;
                }
            }
            return true;
        }
        private bool ApproximatelyEquals(T[] u, T[] v, Func<T, T, bool> Equals)
        {
            int len = u.Length;
            if (v.Length != len)
            {
                return false;
            }

            for (int i = 0; i < len; ++i)
            {
                if (!Equals(u[i], v[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public BandMatrix<T> Transpose()
        {
            T[][] upperBands = new T[_lowerBandwidth][];
            for (int i = 0; i < _lowerBandwidth; ++i)
            {
                upperBands[i] = _lowerBands[i].Copy();
            }

            T[] diagonal = _diagonal.Copy();

            T[][] lowerBands = new T[_upperBandwidth][];
            for (int i = 0; i < _upperBandwidth; ++i)
            {
                lowerBands[i] = _upperBands[i].Copy();
            }
            return new BandMatrix<T>(Columns, Rows, upperBands, diagonal, lowerBands);
        }
        public T[] LeadingDiagonal()
        {
            return _diagonal.Copy();
        }
        public DenseMatrix<T> ToDense()
        {
            return new DenseMatrix<T>(this);
        }

        public override object Clone()
        {
            return Copy();
        }
        public BandMatrix<T> Copy()
        {
            return new BandMatrix<T>(Rows, Columns, Copy(_upperBands), _diagonal.Copy(), Copy(_lowerBands));
        }
        private T[][] Copy(T[][] bands)
        {
            T[][] copy = new T[bands.Length][];
            for (int i = 0; i < bands.Length; ++i)
            {
                copy[i] = bands[i].Copy();
            }
            return copy;
        }
    }
    public static class BandMatrix
    {
        public static BandMatrix<T> Identity<T>(int dim) where T : new()
        {
            T one = DefaultGenerators.GetOne<T>();

            BandMatrix<T> matrix = new BandMatrix<T>(dim, dim, 0, 0);
            T[] diagonal = matrix.Diagonal;
            for (int i = 0; i < dim; ++i)
            {
                diagonal[i] = one;
            }
            return matrix;
        }
        public static BandMatrix<T> Diag<T>(T[] diagonal) where T : new()
        {
            int dim = diagonal.Length;

            BandMatrix<T> matrix = new BandMatrix<T>(dim, dim, 0, 0);
            T[] diag = matrix.Diagonal;
            for (int i = 0; i < dim; ++i)
            {
                diag[i] = diagonal[i];
            }
            return matrix;
        }

        /// <summary>
        /// <para>
        /// Create <txt>rows</txt> $\times$ <txt>columns</txt> band matrix of type <txt>T</txt>, with <txt>upperBandwidth</txt> upper bands
        /// and <txt>lowerBandwidth</txt> lower bands, whose entries are random values.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>Random_Band</name>
        /// <proto>BandMatrix<T> BandMatrix<T>.Random(int rows, int columns, int upperBandwidth, int lowerBandwidth, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="upperBandwidth">The upper-bandwidth, i.e. the number of diagonals above the main diagonal.</param>
        /// <param name="lowerBandwidth">The lower-bandwidth, i.e. the number of diagonals below the main diagonal.</param>
        /// <param name="random">
        /// <b>Optional</b>, the random <txt>T</txt> generator.<br/>
        /// If unspecified, a default generator will be used. See <a href="#Random"><txt><b>Random</b></txt></a> for details on default random generators.
        /// </param>
        /// <returns></returns>
        public static BandMatrix<T> Random<T>(int rows, int columns, int upperBandwidth, int lowerBandwidth, Func<T> random = null) where T : new()
        {
            if (rows < 0 || columns < 0 || upperBandwidth < 0 || lowerBandwidth < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (upperBandwidth >= columns - 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (lowerBandwidth >= rows - 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            //Console.WriteLine("Dude what is going on");
            BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, upperBandwidth, lowerBandwidth);
            for (int i = 0; i < upperBandwidth; ++i)
            {
                FillRandom(matrix.UpperBands[i], random);
            }
            FillRandom(matrix.Diagonal, random);
            for (int i = 0; i < lowerBandwidth; ++i)
            {
                FillRandom(matrix.LowerBands[i], random);
            }

            return matrix;
        }
        private static void FillRandom<T>(T[] band, Func<T> Random)
        {
            for (int i = 0; i < band.Length; ++i)
            {
                band[i] = Random();
            }
        }

        public static BandMatrix<T> RandomDiagonal<T>(int rows, int columns, Func<T> Random = null) where T : new()
        {
            if (rows < 0 || columns < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, 0, 0);
            FillRandom(matrix.Diagonal, Random);
            return matrix;
        }
        public static BandMatrix<T> RandomBidiagonal<T>(int rows, int columns, bool upper = true, Func<T> Random = null) where T : new()
        {
            if (rows < 0 || columns < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (upper)
            {
                BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, 1, 0);
                FillRandom(matrix.Diagonal, Random);
                FillRandom(matrix.UpperBands[0], Random);
                return matrix;
            }
            else
            {
                BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, 0, 1);
                FillRandom(matrix.Diagonal, Random);
                FillRandom(matrix.LowerBands[0], Random);
                return matrix;
            }
        }
        public static BandMatrix<T> RandomTridiagonal<T>(int rows, int columns, Func<T> Random = null) where T : new()
        {
            if (rows < 0 || columns < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            BandMatrix<T> matrix = new BandMatrix<T>(rows, columns, 1, 1);
            FillRandom(matrix.UpperBands[0], Random);
            FillRandom(matrix.Diagonal, Random);
            FillRandom(matrix.LowerBands[0], Random);

            return matrix;
        }
    }
}
