﻿using LinearLite.Helpers;
using LinearLite.Matrices;
using LinearLite.Structs;
using LinearLite.Structs.Matrix;
using LinearLite.Structs.Vectors;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    public static class DenseMatrix
    {
        /// <summary>
        /// Create a matrix of dimensions <txt>rows</txt> x <txt>columns</txt> where each entry is <txt>value</txt>.
        /// <!--inputs-->
        /// <!--returns-->
        /// 
        /// <h4>Examples</h4>
        /// <pre><code class="cs">
        /// // Create a dense 10 x 10 matrix of ones
        /// DenseMatrix&lt;double&gt; ones = DenseMatrix.Repeat&lt;double&gt;(10, 10, 0.0); 
        /// 
        /// // Create a sparse 5 by 5 matrix with each entry equal to -3 + 2i
        /// SparseMatrix&lt;Complex&gt; matrix = SparseMatrix.Repeat&lt;Complex&gt;(5, 5, new Complex(-3, 2));
        /// 
        /// // Create a rectangular 15 by 10 matrix with entry equal to -2
        /// float[,] A = RectangularMatrix.Repeat&lt;float&gt;(15, 10, -2.0f);
        /// 
        /// </code></pre>
        /// 
        /// </summary>
        /// <name>Repeat</name>
        /// <proto>IMatrix<T> IMatrix<T>.Repeat(int rows, int columns, T value)</proto>
        /// <cat>la</cat>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="value">The value of each entry in the created matrix.</param>
        /// <returns>A matrix of dimensions <txt>rows</txt> $\times$ <txt>columns</txt> with all entries equal to <txt>value</txt>.</returns>
        public static DenseMatrix<T> Repeat<T>(int rows, int columns, T value) where T : new()
        {
            T[][] values = MatrixInternalExtensions.JMatrix<T>(rows, columns);
            T[] row;

            int i, j;
            for (i = 0; i < rows; ++i)
            {
                row = values[i];
                for (j = 0; j < columns; ++j)
                {
                    row[j] = value;
                }
            }
            return new DenseMatrix<T>(values);
        }

        /// <summary>
        /// <para>
        /// Create a <txt>rows</txt> $\times$ <txt>columns</txt> matrix where all entries are the additive identity of some 
        /// additive group.
        /// </para>
        /// <para>
        /// Supported matrix types: <txt>DenseMatrix&lt;T&gt;</txt>, <txt>SparseMatrix&lt;T&gt;</txt>, <txt>DiagonalMatrix&lt;T&gt;</txt>, 
        /// <txt>BandMatrix&lt;T&gt;</txt>, <txt>TriangularMatrix&lt;T&gt;</txt> and <txt>T[,]</txt>.
        /// </para>
        /// <para>
        /// Supported data types: <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and all types 
        /// <txt>T</txt> implementing <txt>AdditiveGroup&lt;T&gt;</txt>.
        /// </para>
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>ZeroesMatrix</name>
        /// <proto>IMatrix<T> IMatrix<T>.Zeroes(int rows, int columns)</proto>
        /// <cat>la</cat>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <returns>A <txt>rows</txt> $\times$ <txt>columns</txt> matrix of zeroes.</returns>
        public static DenseMatrix<T> Zeroes<T>(int rows, int columns) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                return (dynamic)new DenseMatrix<int>(rows, columns);
            }
            if (type == typeof(long))
            {
                return (dynamic)new DenseMatrix<long>(rows, columns);
            }
            if (type == typeof(float))
            {
                return (dynamic)new DenseMatrix<float>(rows, columns);
            }
            if (type == typeof(double))
            {
                return (dynamic)new DenseMatrix<double>(rows, columns);
            }
            if (type == typeof(decimal))
            {
                return (dynamic)new DenseMatrix<decimal>(rows, columns);
            }
            if (typeof(AdditiveGroup<T>).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
            {
                T zero = ((AdditiveGroup<T>)new T()).AdditiveIdentity;
                return Repeat(rows, columns, zero);
            }

            throw new NotSupportedException();
        }
        public static DenseMatrix<T> Ones<T>(int rows, int columns) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                return (dynamic)new DenseMatrix<int>(rows, columns);
            }
            if (type == typeof(long))
            {
                return (dynamic)new DenseMatrix<long>(rows, columns);
            }
            if (type == typeof(float))
            {
                return (dynamic)new DenseMatrix<float>(rows, columns);
            }
            if (type == typeof(double))
            {
                return (dynamic)new DenseMatrix<double>(rows, columns);
            }
            if (type == typeof(decimal))
            {
                return (dynamic)new DenseMatrix<decimal>(rows, columns);
            }
            if (typeof(Ring<T>).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
            {
                T one = ((Ring<T>)new T()).MultiplicativeIdentity;
                return Repeat(rows, columns, one);
            }

            throw new NotSupportedException();
        }

        #region Identity matrix creation methods 

        /// <summary>
        /// Create a <txt>dim</txt> $\times$ <txt>dim</txt> identity matrix.
        /// 
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// // Create a dense 10 x 10 real identity matrix
        /// DenseMatrix&lt;double&gt; matrix1 = DenseMatrix.Identity&lt;double&gt;(10); 
        /// 
        /// // Create a sparse 10 x 10 complex identity matrix
        /// SparseMatrix&lt;Complex&gt; matrix2 = SparseMatrix.Identity&lt;Complex&gt;(10); 
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>Identity</name>
        /// <proto>IMatrix<T> IMatrix<T>.Identity(int dim)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim"></param>
        /// <returns></returns>
        public static DenseMatrix<T> Identity<T>(int dim) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                return (dynamic)Identity<int>(dim, 1);
            }
            if (type == typeof(long))
            {
                return (dynamic)Identity<long>(dim, 1L);
            }
            if (type == typeof(float))
            {
                return (dynamic)Identity<float>(dim, 1.0F);
            }
            if (type == typeof(double))
            {
                return (dynamic)Identity<double>(dim, 1.0);
            }
            if (type == typeof(decimal))
            {
                return (dynamic)Identity<decimal>(dim, 1.0M);
            }
            if (typeof(Ring<T>).GetTypeInfo().IsAssignableFrom(type.GetTypeInfo()))
            {
                Ring<T> def = (dynamic)new T();
                return (dynamic)Identity(dim, def.MultiplicativeIdentity, def.AdditiveIdentity);
            }
            throw new NotSupportedException("Matrices over the type " + type.ToString() + " are not supported yet.");
        }
        /// <summary>
        /// Create an identity matrix, given the additive and multiplicative identity 
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="one"></param>
        /// <param name="zero"></param>
        /// <returns></returns>
        private static DenseMatrix<T> Identity<T>(int dim, T one, T zero) where T : new()
        {
            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(dim, dim);
            T[] row;

            int i, j;
            for (i = 0; i < dim; ++i)
            {
                row = matrix[i];
                for (j = 0; j < dim; ++j)
                {
                    row[j] = zero;
                }
                row[i] = one;
            }
            return new DenseMatrix<T>(matrix);
        }
        /// <summary>
        /// Create an identity matrix, given the multiplicative identity. Used by matrix types 
        /// whose default value is the additive identity (e.g. all primitive types), so we dont 
        /// waste time looping though the entire matrix setting entries to 0.
        /// </summary>
        /// <param name="dim"></param>
        /// <param name="one"></param>
        /// <returns></returns>
        private static DenseMatrix<T> Identity<T>(int dim, T one) where T : new()
        {
            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(dim, dim);
            for (int i = 0; i < dim; ++i)
            {
                matrix[i][i] = one;
            }
            return new DenseMatrix<T>(matrix);
        }

        #endregion

        #region Diagonal matrix creation methods

        /// <summary>
        /// Create a $n \times n$ diagonal matrix from a length-$n$ vector of the diagonal elements.
        /// <!--inputs-->
        /// </summary>
        /// <name>Diag</name>
        /// <proto>IMatrix<T> IMatrix<T>.Diag(params T[] diagonal)</proto>
        /// <cat>la</cat>
        /// <param name="diagonal">The main diagonal terms of the matrix.</param>
        /// <returns></returns>
        public static DenseMatrix<T> Diag<T>(params T[] diagonal) where T : AdditiveGroup<T>, new()
        {
            if (diagonal == null)
            {
                throw new ArgumentNullException();
            }
            int dim = diagonal.Length, i, j;
            if (dim == 0)
            {
                throw new ArgumentOutOfRangeException("Cannot create an empty matrix.");
            }

            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(dim, dim);
            T[] row;

            T zero = new T().AdditiveIdentity;
            for (i = 0; i < dim; ++i)
            {
                row = matrix[i];
                for (j = 0; j < dim; ++j)
                {
                    row[j] = zero;
                }
                row[i] = diagonal[i];
            }
            return new DenseMatrix<T>(matrix);
        }
        private static DenseMatrix<T> diag_inner<T>(params T[] diagonal) where T : new()
        {
            if (diagonal == null) throw new ArgumentNullException();

            int dim = diagonal.Length;
            if (dim == 0) throw new ArgumentOutOfRangeException("Cannot create an empty matrix.");

            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(dim, dim);
            for (int i = 0; i < dim; ++i)
            {
                matrix[i][i] = diagonal[i];
            }
            return new DenseMatrix<T>(matrix);
        }
        public static DenseMatrix<int> Diag(params int[] diagonal) => diag_inner(diagonal);
        public static DenseMatrix<long> Diag(params long[] diagonal) => diag_inner(diagonal);
        public static DenseMatrix<float> Diag(params float[] diagonal) => diag_inner(diagonal);
        public static DenseMatrix<double> Diag(params double[] diagonal) => diag_inner(diagonal);
        public static DenseMatrix<decimal> Diag(params decimal[] diagonal) => diag_inner(diagonal);

        #endregion

        #region Random matrix creation methods

        public static DenseMatrix<T> Random<T>(int rows, int columns) where T : new()
        {
            return Random(rows, columns, DefaultGenerators.GetRandomGenerator<T>());
        }

        /// <summary>
        /// <para>
        /// Creates a dense <txt>rows</txt> $\times$ <txt>columns</txt> matrix of type <txt>T</txt> whose entries are random values.
        /// </para>
        /// <para>
        /// The method accepts a random element generator, or uses a default generator if one not specified. Default random generators 
        /// are defined for <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and <txt>Complex</txt>.
        /// <ul>
        /// <li>For matrices over <txt>float</txt>, <txt>double</txt> and <txt>decimal</txt>, the random elements will be uniformly sampled between $[-1, 1]$</li>
        /// <li>For matrices over <txt>int</txt>, the elements will be uniformly sampled from $[-2^{32}, 2^{32} - 1]$.</li>
        /// <li>For matrices over <txt>long</txt>, the elements will be uniformly sampled from $[-2^{64}, 2^{64} - 1]$.</li>
        /// <li>For matrices over <txt>Complex</txt>, the elements will be uniformly sampled from the unit disk $\{ z: |z| \le 1 \}$</li>
        /// </ul>
        /// </para>
        /// <para>For custom algebras over $G$, the method accepts a input <txt>func</txt> $f: () \to G$</para>
        /// 
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>Random</name>
        /// <proto>DenseMatrix<T> DenseMatrix.Random<T>(int rows, int columns, Func<T> random)
        /// </proto>
        /// <cat>la</cat>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used.
        /// </param>
        /// <returns>A <txt>rows</txt> $\times$ <txt>columns</txt> random matrix.</returns>
        public static DenseMatrix<T> Random<T>(int rows, int columns, Func<T> random = null) where T : new()
        {
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            T[][] matrix = MatrixInternalExtensions.JMatrix<T>(rows, columns);
            T[] row;
            int i, j;
            for (i = 0; i < rows; ++i)
            {
                row = matrix[i];
                for (j = 0; j < columns; ++j)
                {
                    row[j] = random();
                }
            }
            return matrix;
        }

        /// <summary>
        /// <para>Creates a random <txt>dim</txt> $\times$ <txt>dim</txt> positive-definite matrix.</para>
        /// <para>The method generates a random matrix then multiplies it by its own transpose.</para>
        /// </summary>
        /// <name>RandomPositiveDefinite</name>
        /// <proto>IMatrix<T> IMatrix<T>.RandomPositiveDefinite(int dim, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="dim"></param>
        /// <returns></returns>
        public static DenseMatrix<T> RandomPositiveDefinite<T>(int dim) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                return (dynamic)RandomPositiveDefinite<int>(dim, A => A.Transpose(), (A, B) => A.Multiply(B));
            }
            if (type == typeof(long))
            {
                return (dynamic)RandomPositiveDefinite<long>(dim, A => A.Transpose(), (A, B) => A.Multiply(B));
            }
            if (type == typeof(float))
            {
                return (dynamic)RandomPositiveDefinite<float>(dim, A => A.Transpose(), (A, B) => A.Multiply(B));
            }
            if (type == typeof(double))
            {
                return (dynamic)RandomPositiveDefinite<double>(dim, A => A.Transpose(), (A, B) => A.Multiply(B));
            }
            if (type == typeof(decimal))
            {
                return (dynamic)RandomPositiveDefinite<decimal>(dim, A => A.Transpose(), (A, B) => A.Multiply(B));
            }
            if (type == typeof(Complex))
            {
                return (dynamic)RandomPositiveDefinite<Complex>(dim, A => A.ConjugateTranspose(), (A, B) => A.Multiply(B));
            }
            throw new NotImplementedException();
        }
        private static DenseMatrix<T> RandomPositiveDefinite<T>(int rows, Func<DenseMatrix<T>, DenseMatrix<T>> Transpose, Func<DenseMatrix<T>, DenseMatrix<T>, DenseMatrix<T>> Multiply) where T : new()
        {
            DenseMatrix<T> matrix = Random<T>(rows, rows);
            DenseMatrix<T> transpose = Transpose(matrix);
            return Multiply(matrix, transpose);
        }

        /// <summary>
        /// <para>Creates a random <txt>dim</txt> $\times$ <txt>dim</txt> Hessenberg matrix.</para>
        /// <!--inputs-->
        /// </summary>
        /// <name>RandomHessenberg</name>
        /// <proto>IMatrix<T> IMatrix<T>.RandomHessenberg(int dim, bool upper, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <param name="dim">The dimension of the created matrix.</param>
        /// <param name="upper">
        /// <b>Optional.</b><br/>
        /// Specifies whether the returned matrix will be upper or lower Hessenberg.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns></returns>
        public static DenseMatrix<T> RandomHessenberg<T>(int dim, bool upper = true, Func<T> random = null) where T : new()
        {
            DenseMatrix<T> zero = Zeroes<T>(dim, dim);
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            for (int i = 0; i < dim; ++i)
            {
                T[] row = zero[i];

                int start = upper ? Math.Max(0, i - 1) : 0;
                int end = upper ? dim : Math.Min(dim, i + 2);
                for (int j = start; j < end; ++j)
                {
                    row[j] = random();
                }
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> diagonal matrix.
        /// All off-diagonal entries of the created matrix will be equal to the additive identity of the group <txt>T</txt>.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>RandomDiagonal</name>
        /// <proto>IMatrix<T> IMatrix<T>.RandomDiagonal(int rows, int columns, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns></returns>
        public static DenseMatrix<T> RandomDiagonal<T>(int rows, int columns, Func<T> random = null) where T : new()
        {

            DenseMatrix<T> zero = Zeroes<T>(rows, columns);
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int end = Math.Min(rows, columns), i;
            for (i = 0; i < end; ++i)
            {
                zero[i][i] = random();
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> bidiagonal matrix.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>RandomBidiagonal</name>
        /// <proto>IMatrix<T> IMatrix<T>.RandomBidiagonal(int rows, int columns, bool upper, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="upper">
        /// <b>Optional.</b><br/>
        /// Specifies whether the created matrix will be an upper bidiagonal matrix.
        /// </param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns></returns>
        public static DenseMatrix<T> RandomBidiagonal<T>(int rows, int columns, bool upper = true, Func<T> random = null) where T : new()
        {
            DenseMatrix<T> zero = Zeroes<T>(rows, columns);
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            zero[0][0] = random();
            if (upper)
            {
                int end = Math.Min(rows, columns);
                for (int i = 1; i < end; ++i)
                {
                    zero[i][i] = random();
                    zero[i - 1][i] = random();
                }

                if (columns > rows)
                {
                    zero[end][end + 1] = random();
                }
            }
            else
            {
                int end = Math.Min(rows, columns);
                for (int i = 1; i < end; ++i)
                {
                    zero[i][i] = random();
                    zero[i][i - 1] = random();
                }
                if (rows > columns)
                {
                    zero[end + 1][end] = random();
                }
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> tridiagonal matrix.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>RandomTridiagonal</name>
        /// <proto>IMatrix<T> IMatrix<T>.RandomTridiagonal(int rows, int columns, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns></returns>
        public static DenseMatrix<T> RandomTridiagonal<T>(int rows, int columns, Func<T> random = null) where T : new()
        {
            DenseMatrix<T> zero = Zeroes<T>(rows, columns);

            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            zero[0][0] = random();
            int end = Math.Min(rows, columns);
            for (int i = 0; i < end; ++i)
            {
                zero[i][i - 1] = random();
                zero[i][i] = random();
                zero[i - 1][i] = random();
            }

            if (rows > columns)
            {
                zero[end + 1][end] = random();
            }
            else if (rows < columns)
            {
                zero[end][end + 1] = random();
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> triangular matrix.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>RandomTriangular</name>
        /// <proto>IMatrix<T> IMatrix<T>.RandomTriangular(int rows, int columns, bool upper, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="upper">
        /// <b>Optional.</b><br/>
        /// Specifies whether the created matrix will be an upper triangular matrix.
        /// </param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns></returns>
        public static DenseMatrix<T> RandomTriangular<T>(int rows, int columns, bool upper = true, Func<T> random = null) where T : new()
        {
            DenseMatrix<T> zero = Zeroes<T>(rows, rows);
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            for (int i = 0; i < rows; ++i)
            {
                T[] row = zero[i];

                int start = upper ? i : 0;
                int end = upper ? columns : Math.Min(columns, i + 1);
                for (int j = start; j < end; ++j)
                {
                    row[j] = random();
                }
            }
            return zero;
        }

        /// <summary>
        /// <para>
        /// Creates a random <txt>rows</txt> $\times$ <txt>columns</txt> matrix of rank $r$.
        /// </para>
        /// <!--inputs-->
        /// </summary>
        /// <name>RandomLowRankMatrix</name>
        /// <proto>IMatrix<T> IMatrix<T>.RandomLowRankMatrix(int rows, int columns, int rank, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="rank">The rank of the created matrix, $r$.</param>
        /// <param name="random">
        /// <b>Optional.</b><br/>
        /// A random generator of type <txt>T</txt>. If not specified, a default generator will be used. Please see <a href="#Random"><txt>random</txt></a> for 
        /// documentation on default random generators. 
        /// </param>
        /// <returns></returns>
        public static DenseMatrix<T> RandomLowRankMatrix<T>(int rows, int columns, int rank) where T : new()
        {
            Type type = typeof(T);
            if (type == typeof(int))
            {
                return (dynamic)RandomLowRankMatrix<int>(rows, columns, rank, (A, B) => A.Multiply(B));
            }
            if (type == typeof(long))
            {
                return (dynamic)RandomLowRankMatrix<long>(rows, columns, rank, (A, B) => A.Multiply(B));
            }
            if (type == typeof(float))
            {
                return (dynamic)RandomLowRankMatrix<float>(rows, columns, rank, (A, B) => A.Multiply(B));
            }
            if (type == typeof(double))
            {
                return (dynamic)RandomLowRankMatrix<double>(rows, columns, rank, (A, B) => A.Multiply(B));
            }
            if (type == typeof(decimal))
            {
                return (dynamic)RandomLowRankMatrix<decimal>(rows, columns, rank, (A, B) => A.Multiply(B));
            }
            if (type == typeof(Complex))
            {
                return (dynamic)RandomLowRankMatrix<Complex>(rows, columns, rank, (A, B) => A.Multiply(B));
            }
            throw new NotImplementedException();
        }
        private static DenseMatrix<T> RandomLowRankMatrix<T>(int rows, int columns, int rank, Func<DenseMatrix<T>, DenseMatrix<T>, DenseMatrix<T>> Multiply) where T : new()
        {
            if (rows < 0 || columns < 0 || rank < 0 || rank > Math.Min(rows, columns))
            {
                throw new ArgumentOutOfRangeException();
            }
            return Multiply(Random<T>(rows, rank), Random<T>(rank, columns));
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="sparsity">The proportion of elements that are zero, between 0 and 1.</param>
        /// <param name="random"></param>
        /// <returns></returns>
        public static DenseMatrix<T> RandomSparse<T>(int rows, int columns, double sparsity, Func<T> random = null) where T : new()
        {
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            DenseMatrix<T> matrix = Random(rows, columns, random);

            int totalElements = rows * columns, zeroElements = (int)(sparsity * totalElements);
            //Debug.WriteLine("Generating " + rows + " x " + columns + " matrix with " + zeroElements + " 0's");
            int[] elements = new int[totalElements];
            for (int i = 0; i < totalElements; ++i)
            {
                elements[i] = i;
            }

            Random r = new Random(0);
            elements.Shuffle(r);

            T zero = new T();
            for (int i = 0; i < zeroElements; ++i)
            {
                int row = elements[i] / columns, column = elements[i] % columns;
                matrix[row, column] = zero;
            }
            return matrix;
        }
        #endregion

        /// <summary>
        /// Load a dense matrix from a file.
        /// <!--inputs-->
        /// </summary>
        /// <name>Load</name>
        /// <proto>DenseMatrix<T> DenseMatrix.Load<T>(string filename, string elementDelim, string rowDelim, Func<string, T> Parse)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="filename">The path to the file.</param>
        /// <param name="elementDelim">
        /// <b>Optional</b>, defaults to <txt>","</txt><br/>
        /// The string separating each matrix element within the same row.</param>
        /// <param name="rowDelim">
        /// <b>Optional</b>, defaults to <txt>"\n"</txt><br/>
        /// The string separating each row of the matrix.</param>
        /// <param name="Parse">
        /// <b>Optional</b>, defaults to the default generator if <txt>T</txt> is <txt>int</txt>, <txt>long</txt>, 
        /// <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and <txt>Complex</txt>.<br/>
        /// A func <txt>string</txt> $\to$ <txt>T</txt> that parses a string.
        /// </param>
        /// <returns></returns>
        public static DenseMatrix<T> Load<T>(string filename, string elementDelim = ",", string rowDelim = "\n", Func<string, T> Parse = null) where T : new()
        {
            if (Parse == null)
            {
                Parse = DefaultGenerators.GetParser<T>();
            }

            string[] lines = FileIO.ReadAllText(filename).Split(new string[] { rowDelim }, StringSplitOptions.RemoveEmptyEntries);
            string[] elDelims = { elementDelim };

            int rows = lines.Length, columns = 0, i, j;
            string[][] r = new string[rows][];

            for (i = 0; i < rows; ++i)
            {
                r[i] = lines[i].Split(elDelims, StringSplitOptions.None);
                if (columns < r[i].Length)
                {
                    columns = r[i].Length;
                }
            }

            T[][] matrix = new T[rows][];
            for (i = 0; i < rows; ++i)
            {
                T[] row = new T[columns];
                string[] r_i = r[i];
                int len = r[i].Length;
                for (j = 0; j < len; ++j)
                {
                    row[j] = Parse(r_i[j]);
                }
            }
            return new DenseMatrix<T>(rows, columns, matrix);
        }
    }
    public class DenseMatrix<T> : IMatrix<T> where T : new()
    {
        private T[][] _values;

        public T[][] Values { get { return _values; } }

        public override T this[int row, int col] { get => _values[row][col]; set => _values[row][col] = value; }
        public override T[] this[int index] { get => _values[index]; set => _values[index] = value; }

        #region Constructors

        /// <summary>
        /// Initialize a dense matrix of dimensions [rows x columns]
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        public DenseMatrix(int rows, int columns) : base(rows, columns)
        {
            _values = MatrixInternalExtensions.JMatrix<T>(rows, columns);
        }
        /// <summary>
        /// Create a matrix given its values, and the number of rows.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="values"></param>
        public DenseMatrix(int rows, T[] values) : base(rows, values.Length / rows)
        {
            if (values == null)
            {
                throw new ArgumentNullException();
            }
            if (rows < 0 || rows > values.Length)
            {
                throw new ArgumentOutOfRangeException();
            }

            _values = new T[rows][];
            int i, j, k = 0;
            for (i = 0; i < rows; ++i)
            {
                T[] row = new T[Columns];
                for (j = 0; j < Columns; ++j)
                {
                    row[j] = values[k++];
                }
                _values[i] = row;
            }
        }
        public DenseMatrix(T[][] values) : base(values.Length, values[0].Length)
        {
            // Check all rows are of the same length

            if (values == null)
            {
                throw new ArgumentNullException();
            }
            if (values.Length == 0 || values[0].Length == 0)
            {
                throw new ArgumentOutOfRangeException("Cannot create an empty matrix");
            }

            int cols = values[0].Length, rows = values.Length, i;
            for (i = 1; i < rows; ++i)
            {
                if (values[i].Length != cols)
                {
                    throw new ArgumentOutOfRangeException("Row lengths do not match.");
                }
            }

            _values = values;
        }
        public DenseMatrix(T[,] values) : base(values.GetLength(0), values.GetLength(1))
        {
            if (values == null) throw new ArgumentNullException();
            if (values.Length == 0) throw new ArgumentOutOfRangeException("Cannot create an empty matrix.");

            _values = MatrixInternalExtensions.ToJagged(values);
        }
        public DenseMatrix(DenseMatrix<T> matrix) : base(matrix.Rows, matrix.Columns)
        {
            MatrixChecks.CheckNotNull(matrix);

            int m = matrix.Rows, i;
            _values = new T[m][];
            for (i = 0; i < m; ++i)
            {
                _values[i] = matrix[i].Copy();
            }
        }
        public DenseMatrix(BandMatrix<T> matrix) : base(matrix.Rows, matrix.Columns)
        {
            _values = MatrixInternalExtensions.JMatrix<T>(matrix.Rows, matrix.Columns);

            int len, i, j;
            for (i = 0; i < matrix.UpperBandwidth; ++i)
            {
                T[] band = matrix.UpperBands[i];
                len = band.Length;
                int offset = i + 1;
                for (j = 0; j < len; ++j)
                {
                    _values[j][j + offset] = band[j];
                }
            }

            len = matrix.Diagonal.Length;
            for (i = 0; i < len; ++i)
            {
                _values[i][i] = matrix.Diagonal[i];
            }

            for (i = 0; i < matrix.LowerBandwidth; ++i)
            {
                T[] band = matrix.LowerBands[i];
                len = band.Length;
                int offset = i + 1;
                for (j = 0; j < len; ++j)
                {
                    _values[j + offset][j] = band[j];
                }
            }
        }
        public DenseMatrix(SparseMatrix<T> matrix) : base(matrix.Rows, matrix.Columns)
        {
            MatrixChecks.CheckNotNull(matrix);
            _values = matrix.ToDense().Values;
        }
        internal DenseMatrix(int rows, int columns, T[][] values) : base(rows, columns)
        {
            _values = values;
        }

        #endregion

        #region Public methods 

        /// <summary>
        /// Return the leading diagonal (main diagonal) of matrix as an array.
        /// </summary>
        /// <name>LeadingDiagonal</name>
        /// <proto>T[] IMatrix<T>.LeadingDiagonal()</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <returns></returns>
        public T[] LeadingDiagonal()
        {
            int n = Math.Min(Rows, Columns), i;
            T[] diagonal = new T[n];
            for (i = 0; i < n; i++)
            {
                diagonal[i] = _values[i][i];
            }
            return diagonal;
        }

        /// <summary>
        /// <para>Copy the row of index <txt>rowIndex</txt> from matrix $A$ into the array <txt>row</txt>.</para>
        /// <para>Also see: <a href="#Row"><txt>IMatrix&lt;T&gt;.Row(int rowIndex)</txt></a></para>
        /// <!--inputs-->
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// DenseMatrix&lt;int&gt; matrix = new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        /// int[] row = new int[3];
        /// // Copy the 3rd row of 'matrix' into 'row'
        /// matrix.Row(2, row);
        /// row.Print();  // [7, 8, 9]
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>RowSet</name>
        /// <proto>void IMatrix<T>.Row(int rowIndex, T[] row)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="rowIndex">
        /// The index of the row to copy. <br/>
        /// Throws <txt>ArgumentOutOfRangeException</txt> if <txt>rowIndex</txt> is not a valid row index.
        /// </param>
        /// <param name="row">
        /// The array to copy onto. <br/>
        /// Throws <txt>ArgumentOutOfRangeException</txt> if its length does not equal the number of columns of the matrix.
        /// </param>
        public void Row(int rowIndex, T[] row)
        {
            if (rowIndex < 0 || rowIndex >= Rows || row.Length != Columns) throw new ArgumentOutOfRangeException();

            int len = row.Length, i;
            T[] A_row = _values[rowIndex];
            for (i = 0; i < len; ++i)
            {
                row[i] = A_row[i];
            }
        }
        /// <summary>
        /// <para>Returns the row of index <txt>rowIndex</txt> from a matrix.</para>
        /// <para>Also see: <a href="#RowSet"><txt>IMatrix&lt;T&gt;.Row(int rowIndex, T[] row)</txt></a></para>
        /// <!--inputs-->
        /// </summary>
        /// <name>Row</name>
        /// <proto>T[] IMatrix<T>.Row(int rowIndex)</proto>
        /// <cat>la</cat>
        /// <param name="rowIndex">
        /// The index of the row to return.<br/>
        /// Throws <txt>ArgumentOutOfRangeException</txt> if <txt>rowIndex</txt> is not a valid row index.
        /// </param>
        /// <returns></returns>
        public T[] Row(int rowIndex)
        {
            if (rowIndex < 0 || rowIndex >= Rows) throw new ArgumentOutOfRangeException();
            return _values[rowIndex].Copy();
        }

        /// <summary>
        /// Extract column from a dense matrix A, storing it in 'column'
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="columnIndex"></param>
        /// <param name="column"></param>
        public void Column(int columnIndex, T[] column)
        {
            _values.Column(columnIndex, column);
        }
        public T[] Column(int columnIndex)
        {
            T[] column = new T[Rows];
            Column(columnIndex, column);
            return column;
        }

        /// <summary>
        /// Calculates and returns the transpose of a matrix over type <txt>T</txt>. The original matrix is unchanged.
        /// <!--returns-->
        /// </summary>
        /// <name>Transpose</name>
        /// <proto>IMatrix<T> IMatrix<T>.Transpose()</proto>
        /// <cat>la</cat>
        /// <returns>The transpose of the matrix.</returns>
        public DenseMatrix<T> Transpose()
        {
            return new DenseMatrix<T>(_values.Transpose());
        }
        public DenseMatrix<T> TransposeParallel()
        {
            return new DenseMatrix<T>(_values.TransposeParallel());
        }

        /// <summary>
        /// Swap the rows <txt>row1</txt> and <txt>row2</txt> of a matrix. Row indices start at 0.
        /// <h4>Example</h4>
        /// <pre><code class="cs">
        /// DenseMatrix&lt;int&gt; permutation = DenseMatrix&lt;int&gt;.Identity(3).SwapRows(1, 2);
        /// permutation.Print();
        /// // 1 0 0
        /// // 0 0 1
        /// // 0 1 0
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>SwapRows</name>
        /// <proto>IMatrix<T> IMatrix<T>.SwapRows(int row1, int row2)</proto>
        /// <cat>la</cat>
        /// <param name="row1"></param>
        /// <param name="row2"></param>
        public void SwapRows(int row1, int row2)
        {
            if (row1 < 0 || row1 >= Rows) throw new ArgumentOutOfRangeException();
            if (row2 < 0 || row2 >= Rows) throw new ArgumentOutOfRangeException();

            T[] temp = _values[row1];
            _values[row1] = _values[row2];
            _values[row2] = temp;
        }

        /// <summary>
        /// Swap the columns <txt>column1</txt> and <txt>column2</txt> of a matrix. Column indices start at 0.
        /// </summary>
        /// <name>SwapColumns</name>
        /// <proto>IMatrix<T> IMatrix<T>.SwapColumns(int column1, int column2)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="A"></param>
        /// <param name="column1"></param>
        /// <param name="column2"></param>
        /// <returns></returns>
        public void SwapColumns(int column1, int column2)
        {
            if (column1 < 0 || column1 >= Columns) throw new ArgumentOutOfRangeException();
            if (column2 < 0 || column2 >= Columns) throw new ArgumentOutOfRangeException();

            int rows = Rows, i;
            for (i = 0; i < rows; ++i)
            {
                T[] row = _values[i];
                T temp = row[column1];
                row[column1] = row[column2];
                row[column2] = temp;
            }
        }

        /// <summary>
        /// Extract the submatrix of dimension <txt>rows</txt> $\times$ <txt>columns</txt>, starting from 
        /// the coordinates (<txt>firstRow</txt>, <txt>firstCol</txt>), returning a new matrix.
        /// 
        /// <!--inputs-->
        /// 
        /// <para><b>Example</b></para>
        /// <pre><code class="cs">
        /// // 1 2 3
        /// // 4 5 6
        /// // 7 8 9 
        /// DenseMatrix&lt;double&gt; matrix = new double[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        /// DenseMatrix&lt;double&gt; submatrix = matrix.Submatrix(1, 1, 2, 2);
        /// 
        /// // 5 6
        /// // 8 9
        /// submatrix.Print();
        /// 
        /// </code></pre>
        /// </summary>
        /// <name>Submatrix</name>
        /// <proto>IMatrix<T> IMatrix<T>.Submatrix(int firstRow, int firstCol, int rows, int columns)</proto>
        /// <cat>la</cat>
        /// <param name="firstRow">The first row of the matrix to extract to the submatrix.</param>
        /// <param name="firstCol">The first column of the matrix to extract to the submatrix.</param>
        /// <param name="rows">The number of rows of the submatrix.</param>
        /// <param name="columns">The number of columns of the submatrix.</param>
        /// <returns></returns>
        public DenseMatrix<T> Submatrix(int firstRow, int firstCol, int rows, int columns)
        {
            if (firstRow < 0 || firstCol < 0) throw new ArgumentOutOfRangeException();
            if (firstRow + rows > Rows || firstCol + columns > Columns) throw new ArgumentOutOfRangeException();

            return new DenseMatrix<T>(_values.Submatrix(firstRow, firstCol, rows, columns));
        }

        /// <summary>
        /// Permute the rows of a matrix $A$ from a permutation vector $v$, such that 
        /// $(PA)_i = A_{v_i}$ for $1\le{i}\le{n}$ where $P$ represents the row permutation matrix,
        /// and $A_j$ represents the $j$-th row of matrix $A$.
        /// 
        /// <!--inputs-->
        /// </summary>
        /// <name>PermuteRows</name>
        /// <proto>void IMatrix<T>.PermuteRows(int[] permutation)</proto>
        /// <cat>la</cat>
        /// <param name="permutation">
        /// An <txt>int32</txt> array representing the permutation vector $v$, of dimension at least the number of rows of the matrix.<br/>
        /// The array must contain each of the integers $0, 1, ..., n - 1$ in its first $n$ entries, where $n$ is the number of rows of the matrix.
        /// </param>
        /// <returns></returns>
        public void PermuteRows(int[] permutation)
        {
            // TODO: check that this is a permutation?
            T[][] P = new T[Rows][];
            for (int i = 0; i < Rows; ++i)
            {
                P[i] = _values[permutation[i]];
            }
            _values = P;
        }

        /// <summary>
        /// Permute the columns of a matrix $A$ from a permutation vector $v$, such that 
        /// $(AP)_{:i} = A_{:v_i}$ for $1\le{i}\le{n}$ where $P$ represents the column permutation matrix,
        /// and $A_{:j}$ represents the $j$-th column of matrix $A$.
        /// 
        /// <!--inputs-->
        /// </summary>
        /// <name>PermuteColumns</name>
        /// <proto>void IMatrix<T>.PermuteColumns(int[] permutation)</proto>
        /// <cat>la</cat>
        /// <param name="permutation">
        /// An <txt>int32</txt> array representing the permutation vector $v$, of dimension at least the number of columns of the matrix.<br/>
        /// The array must contain each of the integers $0, 1, ..., n - 1$ in its first $n$ entries, where $n$ is the number of columns of the matrix.
        /// </param>
        /// <returns></returns>
        public void PermuteColumns(int[] permutation)
        {
            // TODO: check that this is a permutation?
            T[][] P = MatrixInternalExtensions.JMatrix<T>(Rows, Columns);
            for (int j = 0; j < Columns; ++j) 
            {
                for (int i = 0; i < Rows; ++i)
                {
                    P[i][j] = _values[i][permutation[j]];
                }
            }
            _values = P;
        }

        /// <summary>
        /// Returns the dense matrix with row 'row' returned
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public DenseMatrix<T> RemoveRow(int row)
        {
            if (row < 0 || row >= Rows)
            {
                throw new ArgumentOutOfRangeException();
            }

            T[][] newValues = new T[Rows - 1][];
            for (int i = 0, j = 0; i < Rows; ++i)
            {
                if (i != row) 
                {
                    newValues[j++] = _values[i].Copy();
                }
            }
            return new DenseMatrix<T>(newValues);
        }
        /// <summary>
        /// Returns a new dense matrix with every row in 'rows' deleted.
        /// </summary>
        /// <param name="rows"></param>
        /// <returns></returns>
        public DenseMatrix<T> RemoveRows(IEnumerable<int> rows)
        {
            HashSet<int> uniqueRows = new HashSet<int>();
            foreach (int row in rows)
            {
                if (row < 0 || row > Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }
                uniqueRows.Add(row);
            }

            int n = uniqueRows.Count;
            T[][] newValues = new T[Rows - n][];
            for (int i = 0, j = 0; i < Rows; ++i)
            {
                if (!uniqueRows.Contains(n))
                {
                    newValues[j++] = _values[i].Copy();
                }
            }
            return new DenseMatrix<T>(newValues);
        }

        /// <summary>
        /// Returns a new dense matrix with the column 'column' deleted.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public DenseMatrix<T> RemoveColumn(int column)
        {
            if (column < 0 || column >= Columns)
            {
                throw new ArgumentOutOfRangeException();
            }

            int len = Columns - 1, i, j, k;
            T[][] newValues = new T[Rows][];
            for (i = 0; i < Rows; ++i)
            {
                T[] row = new T[len], values_i = _values[i];
                for (j = 0, k = 0; j < Columns; ++j)
                {
                    if (j != column)
                    {
                        row[k++] = values_i[j];
                    }
                }
                newValues[i] = row;
            }
            return new DenseMatrix<T>(newValues);
        }
        /// <summary>
        /// Returns a new dense matrix with all columns in 'columns' deleted.
        /// </summary>
        /// <param name="columns"></param>
        /// <returns></returns>
        public DenseMatrix<T> RemoveColumns(IEnumerable<int> columns)
        {
            HashSet<int> uniqueColumns = new HashSet<int>();
            foreach (int column in columns)
            {
                if (column < 0 || column >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }
                uniqueColumns.Add(column);
            }

            int len = Columns - 1, i, j, k;
            T[][] newValues = new T[Rows][];
            for (i = 0; i < Rows; ++i)
            {
                T[] row = new T[len], values_i = _values[i];
                for (j = 0, k = 0; j < Columns; ++j)
                {
                    if (!uniqueColumns.Contains(j))
                    {
                        row[k++] = values_i[j];
                    }
                }
                newValues[i] = row;
            }
            return new DenseMatrix<T>(newValues);
        }

        /// <summary>
        /// Copy the entries from a dense matrix src, onto this matrix, starting at position (srcFirstRow, srcFirstCol)
        /// and extending for (rows, cols). The entries will be copied onto location starting with (destFirstRow, destFirstCol).
        /// </summary>
        /// <param name="src"></param>
        /// <param name="srcFirstRow"></param>
        /// <param name="srcFirstCol"></param>
        /// <param name="destFirstRow"></param>
        /// <param name="destFirstCol"></param>
        /// <param name="rows"></param>
        /// <param name="cols"></param>
        public void CopyFrom(DenseMatrix<T> src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol, int rows, int cols)
        {
            _values.CopyFrom(src._values, srcFirstRow, srcFirstCol, destFirstRow, destFirstCol, rows, cols);
        }
        public void CopyFrom(DenseMatrix<T> src, int srcFirstRow, int srcFirstCol, int destFirstRow, int destFirstCol)
        {
            _values.CopyFrom(src._values, srcFirstRow, srcFirstCol, destFirstRow, destFirstCol);
        }
        public void CopyFrom(DenseMatrix<T> src)
        {
            _values.CopyFrom(src._values);
        }

        /// <summary>
        /// Deep clone 
        /// </summary>
        /// <returns></returns>
        public override object Clone()
        {
            return Copy();
        }

        /// <summary>
        /// Returns a deep copy of a matrix.
        /// </summary>
        /// <name>CopyMatrix</name>
        /// <proto>IMatrix<T> IMatrix<T>.Copy()</proto>
        /// <cat>la</cat>
        /// <returns></returns>
        public DenseMatrix<T> Copy()
        {
            int rows = Rows, cols = Columns, i, j;
            T[][] copy = new T[rows][];
            for (i = 0; i < rows; ++i)
            {
                T[] row = new T[cols], value_i = _values[i];
                for (j = 0; j < cols; ++j)
                {
                    row[j] = value_i[j];
                }
                copy[i] = row;
            }
            return new DenseMatrix<T>(copy);
        }
        public DenseMatrix<T> CopyParallel()
        {
            int rows = Rows, cols = Columns;
            T[][] copy = new T[rows][];

            Parallel.ForEach(Partitioner.Create(0, rows), range =>
            {
                int min = range.Item1, max = range.Item2, i, j;
                for (i = min; i < max; ++i)
                {
                    T[] row = new T[cols], value_i = _values[i];
                    for (j = 0; j < cols; ++j)
                    {
                        row[j] = value_i[j];
                    }
                    copy[i] = row;
                }
            });
            return new DenseMatrix<T>(copy);
        }

        /// <summary>
        /// Calculates and returns the transpose of a matrix over type <txt>T</txt>. The original matrix is unchanged.
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>Vectorize</name>
        /// <proto>IMatrix<T> IMatrix<T>.Vectorize(bool rowMajor)</proto>
        /// <cat>la</cat>
        /// <param name="rowMajor"><b>Optional, </b> defaults to <txt>false</txt>.<br/> 
        /// Specifies whether the vectorization should traverse in a row-major or column-major manner.</param>
        /// <returns>
        /// The vectorized form of the matrix, as a <txt>IVector</txt>.<br/>
        /// For a <txt>DenseMatrix</txt>, a <txt>DenseVector</txt> will be returned.<br/>
        /// For a <txt>SparseMatrix</txt>, a <txt>SparseVector</txt> will be returned.<br/>
        /// For a rectangular matrix <txt>T[,]</txt>, an array <txt>T[]</txt> will be returned. 
        /// </returns>
        public DenseVector<T> Vectorize(bool rowMajor = false)
        {
            int rows = Rows, cols = Columns, len = rows * cols, i, j, k;
            T[] vect = new T[len];

            if (rowMajor)
            {
                k = 0;
                for (i = 0; i < rows; ++i)
                {
                    T[] row = _values[i];
                    for (j = 0; j < cols; ++j, ++k)
                    {
                        vect[k] = row[j];
                    }
                }
            }
            else
            {
                for (i = 0; i < rows; ++i)
                {
                    T[] row = _values[i];
                    for (j = 0, k = i; j < cols; ++j, k += rows)
                    {
                        vect[k] = row[j];
                    }
                }
            }
            
            return new DenseVector<T>(vect);
        }
        #endregion


        #region Casts

        public static implicit operator DenseMatrix<T>(T[][] matrix) => new DenseMatrix<T>(matrix);
        public static implicit operator DenseMatrix<T>(T[,] matrix) => new DenseMatrix<T>(matrix);
        public static explicit operator DenseMatrix<T>(SparseMatrix<T> matrix) => matrix.ToDense();
        public static explicit operator DenseMatrix<T>(BandMatrix<T> matrix) => new DenseMatrix<T>(matrix);

        #endregion
    }
}
