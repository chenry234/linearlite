﻿using LinearLite.BLAS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Matrix
{
    public class DiagonalMatrix<T> : IMatrix<T> where T : new()
    {
        private T[] _diagonalTerms;

        public T[] DiagonalTerms { get { return _diagonalTerms; } }

        public override T[] this[int index]
        {
            get
            {
                if (index < 0 || index >= Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }

                T[] row = new T[Columns];
                if (index < Math.Min(Rows, Columns))
                {
                    row[index] = _diagonalTerms[index];
                }
                return row;
            }
            set
            {
                if (index < 0 || index >= Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }
                throw new NotImplementedException();
            }
        }
        public override T this[int rowIndex, int columnIndex]
        {
            get
            {
                if (rowIndex < 0 || columnIndex < 0 || rowIndex >= Rows || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (rowIndex != columnIndex)
                {
                    return default(T);
                }
                return _diagonalTerms[rowIndex];
            }
            set
            {
                if (rowIndex < 0 || columnIndex < 0 || rowIndex >= Rows || columnIndex >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (rowIndex != columnIndex)
                {
                    throw new InvalidOperationException();
                }
                _diagonalTerms[rowIndex] = value;
            }
        }

        public DiagonalMatrix(int rows, int columns) : base(rows, columns)
        {
            if (rows < 0 || columns < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            _diagonalTerms = new T[Math.Min(rows, columns)];
        }
        public DiagonalMatrix(T[] diagonalTerms) : base(diagonalTerms.Length, diagonalTerms.Length)
        {
            if (diagonalTerms != null)
            {
                throw new ArgumentNullException();
            }

            _diagonalTerms = diagonalTerms;
        }
        /// <summary>
        /// Construct a diagonal matrix given the dimensions of a matrix, and its diagonal elements.
        /// The length of the diagonal vector must be at most both the number of rows and columns.
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="diagonalTerms"></param>
        public DiagonalMatrix(int rows, int columns, T[] diagonalTerms) : base(rows, columns)
        {
            if (rows < 0 || columns < 0 || diagonalTerms.Length > rows || diagonalTerms.Length > columns)
            {
                throw new ArgumentOutOfRangeException();
            }
            _diagonalTerms = diagonalTerms;
        }

        /// <summary>
        /// Calculates and returns the diagonal matrix equal to (this) + B
        /// </summary>
        /// <param name="D"></param>
        /// <returns></returns>
        internal DiagonalMatrix<T> Add(DiagonalMatrix<T> D, Func<T, T, T> Add)
        {
            MatrixChecks.CheckMatrixDimensionsEqual(this, D);

            T[] diag1 = _diagonalTerms;
            T[] diag2 = D._diagonalTerms;

            int len1 = diag1.Length, len2 = diag2.Length, i;

            // Add, while remaining agnostic to differing lengths
            int max = Math.Max(len1, len2);
            T[] sum = new T[max];
            for (i = 0; i < len1; ++i)
            {
                sum[i] = diag1[i];
            }
            for (i = len1; i < len2; ++i)
            {
                sum[i] = new T();
            }
            for (i = 0; i < len2; ++i)
            {
                sum[i] = Add(sum[i], diag2[i]);
            }
            return new DiagonalMatrix<T>(sum);
        }

        /// <summary>
        /// Calculates and returns the diagonal matrix equal to (this) - B
        /// </summary>
        /// <param name="D"></param>
        /// <returns></returns>
        internal DiagonalMatrix<T> Subtract(DiagonalMatrix<T> D, Func<T, T, T> Subtract)
        {
            MatrixChecks.CheckMatrixDimensionsEqual(this, D);

            T[] diag1 = _diagonalTerms;
            T[] diag2 = D._diagonalTerms;

            int len1 = diag1.Length, len2 = diag2.Length, i;

            // Add, while remaining agnostic to differing lengths
            int max = Math.Max(len1, len2);
            T[] sum = new T[max];
            for (i = 0; i < len1; ++i)
            {
                sum[i] = diag1[i];
            }
            for (i = len1; i < len2; ++i)
            {
                sum[i] = new T();
            }
            for (i = 0; i < len2; ++i)
            {
                sum[i] = Subtract(sum[i], diag2[i]);
            }
            return new DiagonalMatrix<T>(sum);
        }

        /// <summary>
        /// Calculates and returns the product of two diagonal matrices 
        /// </summary>
        /// <param name="D"></param>
        /// <param name="Multiply"></param>
        /// <returns></returns>
        internal DiagonalMatrix<T> Multiply(DiagonalMatrix<T> D, Func<T, T, T> Multiply)
        {
            MatrixChecks.CheckMatrixDimensionsForMultiplication(this, D);

            T[] diag = _diagonalTerms;
            T[] B_diag = D._diagonalTerms;

            int min = Math.Min(diag.Length, B_diag.Length), i;
            T[] prod = new T[min];
            for (i = 0; i < min; ++i)
            {
                prod[i] = Multiply(diag[i], B_diag[i]);
            }
            return new DiagonalMatrix<T>(Rows, D.Columns, prod);
        }

        /// <summary>
        /// Multiply a matrix by a vector
        /// </summary>
        /// <param name="v"></param>
        /// <param name="Multiply"></param>
        /// <returns></returns>
        internal T[] Multiply(T[] v, Func<T, T, T> Multiply)
        {
            if (v == null)
            {
                throw new ArgumentNullException();
            }
            if (Columns != v.Length)
            {
                throw new InvalidOperationException();
            }

            T[] product = new T[Rows];
            int min = Math.Min(Rows, Columns), i;
            for (i = 0; i < min; ++i)
            {
                product[i] = Multiply(_diagonalTerms[i], v[i]);
            }
            return product;
        }

        /// <summary>
        /// Calculate and return the product of (this) and a scalar of type T
        /// </summary>
        /// <param name="s"></param>
        /// <param name="Multiply"></param>
        /// <returns></returns>
        internal DiagonalMatrix<T> Multiply(T s, IBLAS<T> blas)
        {
            DiagonalMatrix<T> result = Copy();
            T[] vec = result._diagonalTerms;
            blas.SCAL(vec, s, 0, vec.Length);
            return result;
        }

        /// <summary>
        /// Calculates and returns a matrix raised to a integer power
        /// </summary>
        /// <param name="power"></param>
        /// <param name="Power"></param>
        /// <returns></returns>
        internal DiagonalMatrix<T> Pow(int power, Func<T, int, T> Power)
        {
            int len = _diagonalTerms.Length, i;
            T[] diag = new T[len];
            for (i = 0; i < len; ++i)
            {
                diag[i] = Power(_diagonalTerms[i], power);
            }
            return new DiagonalMatrix<T>(diag);
        }

        /// <summary>
        /// Calculates and returns exp(A) where A is diagonal 
        /// </summary>
        /// <param name="Exp"></param>
        /// <returns></returns>
        internal DiagonalMatrix<F> Exp<F>(F one, Func<T, F> Exp) where F : new()
        {
            if (Rows != Columns)
            {
                throw new InvalidOperationException();
            }

            int len = _diagonalTerms.Length, i;
            F[] diag = new F[Rows];
            for (i = 0; i < len; ++i)
            {
                diag[i] = Exp(_diagonalTerms[i]);
            }
            for (i = len; i < Rows; ++i) 
            {
                diag[i] = one;
            }
            return new DiagonalMatrix<F>(diag);
        }

        public override object Clone()
        {
            return Copy();
        }
        public DiagonalMatrix<T> Copy()
        {
            return new DiagonalMatrix<T>(Rows, Columns, _diagonalTerms.Copy());
        }
    }
}
