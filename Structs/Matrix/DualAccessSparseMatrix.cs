﻿using System;
using System.Collections.Generic;

namespace LinearLite.Structs.Matrix
{
    /// <summary>
    /// Sparse matrix that can be accessed in two different formats
    /// </summary>
    internal class DualAccessSparseMatrix<T> where T : new()
    {
        internal long NumRows { get; set; }
        internal long NumColumns { get; set; }

        private T _default;

        internal T this[int rowIndex, int colIndex]
        {
            get
            {
                if (!_byRowThenColumn.ContainsKey(rowIndex))
                {
                    return _default;
                }

                Dictionary<long, T> row = _byRowThenColumn[rowIndex];
                if (!row.ContainsKey(colIndex))
                {
                    return _default;
                }
                return row[colIndex];
            }
        }
        private Dictionary<long, Dictionary<long, T>> _byRowThenColumn, _byColumnThenRow;
        internal Dictionary<long, Dictionary<long, T>> Rows { get { return _byRowThenColumn; } set { _byRowThenColumn = value; } }
        internal Dictionary<long, Dictionary<long, T>> Columns { get { return _byColumnThenRow; } set { _byColumnThenRow = value; } }

        internal DualAccessSparseMatrix(SparseMatrix<T> matrix)
        {
            _byRowThenColumn = new Dictionary<long, Dictionary<long, T>>();
            _byColumnThenRow = new Dictionary<long, Dictionary<long, T>>();
            _default = new T();

            NumRows = matrix.Rows;
            NumColumns = matrix.Columns;

            Dictionary<long, T> values = matrix.Values;
            foreach (KeyValuePair<long, T> pair in values)
            {
                long key = pair.Key, row = key / NumColumns, col = key % NumColumns;

                if (_byRowThenColumn.ContainsKey(row))
                {
                    _byRowThenColumn[row][col] = pair.Value;
                }
                else
                {
                    Dictionary<long, T> _r = new Dictionary<long, T>();
                    _r[col] = pair.Value;
                    _byRowThenColumn[row] = _r;
                }

                if (_byColumnThenRow.ContainsKey(col))
                {
                    _byColumnThenRow[col][row] = pair.Value;
                }
                else
                {
                    Dictionary<long, T> _c = new Dictionary<long, T>();
                    _c[row] = pair.Value;
                    _byColumnThenRow[col] = _c;
                }
            }
        }

        internal Dictionary<long, T> Row(long rowIndex)
        {
            if (_byRowThenColumn.ContainsKey(rowIndex))
            {
                return _byRowThenColumn[rowIndex];
            }
            return new Dictionary<long, T>();
        }
        internal Dictionary<long, T> Column(long colIndex)
        {
            if (_byColumnThenRow.ContainsKey(colIndex))
            {
                return _byColumnThenRow[colIndex];
            }
            return new Dictionary<long, T>();
        }
    }
}
