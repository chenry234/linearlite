﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    public abstract class IMatrix<T> : ICloneable
    {
        public int Rows { get; protected set; }
        public int Columns { get; protected set; }
        public bool IsSquare { get { return Rows == Columns; } }

        protected IMatrix(int rows, int columns)
        {
            if (rows < 0 || columns < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            Rows = rows;
            Columns = columns;
        }
        protected IMatrix()
        {
            Rows = 0;
            Columns = 0;
        }

        public abstract T[] this[int index] { get; set; }
        public abstract T this[int index1, int index2] { get; set; }

        public void Print(Func<T, string> ToString)
        {
            StringBuilder sb = new StringBuilder();

            int i, j;
            for (i = 0; i < Rows; ++i)
            {
                for (j = 0; j < Columns; ++j)
                {
                    sb.Append(ToString(this[i, j]) + "\t");
                }
                sb.AppendLine();
            }
            Debug.WriteLine(sb.ToString());
        }
        public abstract object Clone();
    }
}
