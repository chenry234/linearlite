﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using LinearLite.Structs.Matrix;
using LinearLite.Structs.Vectors;
using LinearLite.Vectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    public class SparseMatrix<T> : IMatrix<T> where T : new()
    {
        private readonly Dictionary<long, T> _values;
        internal Dictionary<long, T> Values { get { return _values; } }

        public override T[] this[int index]
        {
            get
            {
                if (index < 0 || index >= Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }

                int cols = Columns;
                T[] row = new T[cols];
                foreach (long key in _values.Keys)
                {
                    int rowIndex = (int)(key / cols);
                    if (rowIndex == index)
                    {
                        int colIndex = (int)(key % cols);
                        row[colIndex] = _values[key];
                    }
                }
                return row;
            }
            set 
            {
                if (index < 0 || index >= Rows)
                {
                    throw new ArgumentOutOfRangeException();
                }

                // Only add values that are not = new()
                T def = new T();
                T[] row = value;

                int cols = Columns, i;
                if (row.Length != cols)
                {
                    throw new ArgumentOutOfRangeException();
                }

                long keybase = (long)index * cols;
                for (i = 0; i < cols; ++i)
                {
                    if (!row[i].Equals(def))
                    {
                        long key = keybase + i;
                        _values[key] = row[i];
                    }
                }
            }
        }
        public override T this[int row, int col]
        {
            get
            {
                if (row < 0 || col < 0 || row >= Rows || col >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                long key = get_key(row, col);
                return _values.ContainsKey(key) ? _values[key] : default(T);
            }
            set
            {
                if (row < 0 || col < 0 || row >= Rows || col >= Columns)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _values[get_key(row, col)] = value;
            }
        }

        public SparseMatrix(int rows, int columns) : base(rows, columns)
        {
            _values = new Dictionary<long, T>();
        }
        public SparseMatrix(DenseMatrix<T> matrix) : base(matrix.Rows, matrix.Columns)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            long key = 0;
            int i, j;
            _values = new Dictionary<long, T>();
            for (i = 0; i < matrix.Rows; ++i)
            {
                T[] row = matrix[i];
                for (j = 0; j < matrix.Columns; ++j)
                {
                    _values[key] = row[j];
                    key++;
                }
            }
        }
        public SparseMatrix(DiagonalMatrix<T> matrix) : base(matrix.Rows, matrix.Columns)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            T[] diagonal = matrix.DiagonalTerms;
            _values = new Dictionary<long, T>();

            for (int i = 0; i < diagonal.Length; ++i)
            {
                long key = (long)i * matrix.Columns + i;
                _values[key] = diagonal[i];
            }
        }
        public SparseMatrix(TriangularMatrix<T> matrix) : base(matrix.Rows, matrix.Columns)
        {
            if (matrix == null)
            {
                throw new ArgumentNullException();
            }

            _values = new Dictionary<long, T>();
            int rows = matrix.Rows, cols = matrix.Columns, i, j;

            if (matrix.IsUpper)
            {
                for (i = 0; i < rows; ++i)
                {
                    T[] row = matrix[i];

                    int len = row.Length;
                    long key = (long)(i + 1) * cols - len;
                    for (j = 0; j < len; ++j)
                    {
                        _values[key + j] = row[j];
                    }
                }
            }
            else
            {
                for (i = 0; i < rows; ++i)
                {
                    T[] row = matrix[i];

                    int len = row.Length;
                    long key = (long)i * cols;
                    for (j = 0; j < len; ++j)
                    {
                        _values[key + j] = row[j];
                    }
                }
            }
        }
        internal SparseMatrix(int rows, int columns, Dictionary<long, T> values) : base(rows, columns)
        {
            _values = values;
        }

        private long get_key(int rows, int columns) => (long)rows * Columns + columns;

        internal void Increment(SparseMatrix<T> matrix, Func<T, T, T> Add)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.CheckMatrixDimensionsEqual(this, matrix);

            Dictionary<long, T> _vs = matrix._values;
            foreach (KeyValuePair<long, T> pair in _vs)
            {
                long key = pair.Key;
                if (_values.ContainsKey(key))
                {
                    _values[key] = Add(_values[key], pair.Value);
                }
                else
                {
                    _values[key] = pair.Value;
                }
            }
        }
        internal void Decrement(SparseMatrix<T> matrix, IBLAS<T> blas)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.CheckMatrixDimensionsEqual(this, matrix);

            Dictionary<long, T> _vs = matrix._values;
            foreach (KeyValuePair<long, T> pair in _vs)
            {
                long key = pair.Key;
                if (_values.ContainsKey(key))
                {
                    _values[key] = blas.Subtract(_values[key], pair.Value);
                }
                else
                {
                    _values[key] = blas.Negate(pair.Value);
                }
            }
        }

        internal Dictionary<long, Dictionary<long, T>> GroupByRows()
        {
            long m = Columns;
            int row_dict_capacity = Math.Min(_values.Count, (int)m);

            // To row dictionary form
            var values = new Dictionary<long, Dictionary<long, T>>(row_dict_capacity);
            foreach (KeyValuePair<long, T> pair in _values)
            {
                long row = pair.Key / m, column = pair.Key % m;
                if (values.ContainsKey(row))
                {
                    values[row][column] = pair.Value;
                }
                else
                {
                    var rowDict = new Dictionary<long, T>(row_dict_capacity);
                    rowDict[column] = pair.Value;
                    values[row] = rowDict;
                }
            }
            return values;
        }
        internal Dictionary<long, Dictionary<long, T>> GroupByColumns()
        {
            // To row dictionary form
            var values = new Dictionary<long, Dictionary<long, T>>();
            long m = Columns;

            foreach (KeyValuePair<long, T> pair in _values)
            {
                long row = pair.Key / m, column = pair.Key % m;
                if (values.ContainsKey(column))
                {
                    values[column][row] = pair.Value;
                }
                else
                {
                    var columnDict = new Dictionary<long, T>();
                    columnDict[row] = pair.Value;
                    values[column] = columnDict;
                }
            }
            return values;
        }

        internal SparseMatrix<T> Add(SparseMatrix<T> matrix, Func<T, T, T> Add)
        {
            SparseMatrix<T> result = Clone() as SparseMatrix<T>;
            result.Increment(matrix, Add);
            return result;
        }
        internal SparseMatrix<T> Subtract(SparseMatrix<T> matrix, IBLAS<T> blas)
        {
            SparseMatrix<T> result = Clone() as SparseMatrix<T>;
            result.Decrement(matrix, blas);
            return result;
        }
        internal SparseMatrix<T> Multiply(SparseMatrix<T> matrix, IBLAS<T> blas)
        {
            MatrixChecks.CheckNotNull(matrix);
            MatrixChecks.CheckMatrixDimensionsForMultiplication(this, matrix);

            Dictionary<long, T> B_values = matrix._values;
            int A_columns = Columns, B_columns = matrix.Columns, ops = _values.Count * B_values.Count;

            Dictionary<long, T> product = new Dictionary<long, T>(ops);
            if (ops < Rows * Columns * matrix.Columns)
            {
                foreach (KeyValuePair<long, T> a in _values)
                {
                    int row1 = (int)(a.Key / A_columns);
                    int column1 = (int)(a.Key % A_columns);

                    foreach (KeyValuePair<long, T> b in B_values)
                    {
                        int row2 = (int)(b.Key / B_columns);
                        if (column1 == row2)
                        {
                            int column2 = (int)(b.Key % B_columns);

                            long key = (long)row1 * B_columns + column2;
                            T prod = blas.Multiply(a.Value, b.Value);
                            if (product.ContainsKey(key))
                            {
                                product[key] = blas.Add(product[key], prod);
                            }
                            else
                            {
                                product[key] = prod;
                            }
                        }
                    }
                }
            }
            else
            {
                Dictionary<long, Dictionary<long, T>> A_values_by_row = GroupByRows(), B_values_by_column = matrix.GroupByColumns();
                foreach (KeyValuePair<long, Dictionary<long, T>> A_row in A_values_by_row)
                {
                    long index = A_row.Key * B_columns;
                    Dictionary<long, T> row = A_row.Value;
                    foreach (KeyValuePair<long, Dictionary<long, T>> B_col in B_values_by_column)
                    {
                        product[index + B_col.Key] = blas.DOT(row, B_col.Value, 0, A_columns);
                    }
                }
            }

            return new SparseMatrix<T>(Rows, B_columns, product);
        }
        internal SparseVector<T> Multiply(SparseVector<T> vector, IBLAS<T> blas)
        {
            VectorChecks.CheckNotNull(vector);

            Dictionary<long, T> vect_values = vector.Values;
            Dictionary<long, T> result = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> pair in _values)
            {
                long key = pair.Key, row = key / Columns, col = key % Columns;
                if (vect_values.ContainsKey(col))
                {
                    if (result.ContainsKey(row))
                    {
                        result[row] = blas.Add(result[row], blas.Multiply(pair.Value, vect_values[col]));
                    }
                    else
                    {
                        result[row] = blas.Multiply(pair.Value, vect_values[col]);
                    }
                }
            }
            return new SparseVector<T>(Rows, result);
        }
        internal SparseVector<T> Multiply(T[] vector, IBLAS<T> blas)
        {
            VectorChecks.CheckNotNull(vector);

            Dictionary<long, T> result = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> pair in _values)
            {
                long key = pair.Key, row = key / Columns, col = key % Columns;
                if (result.ContainsKey(row))
                {
                    result[row] = blas.Add(result[row], blas.Multiply(pair.Value, vector[col]));
                }
                else
                {
                    result[row] = blas.Multiply(pair.Value, vector[col]);
                }
            }
            return new SparseVector<T>(Rows, result);
        }
        internal SparseMatrix<T> Multiply(T scalar, Func<T, T, T> Multiply)
        {
            Dictionary<long, T> result = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> pair in _values)
            {
                result[pair.Key] = Multiply(scalar, pair.Value);
            }
            return new SparseMatrix<T>(Rows, Columns, result);
        }
        
        public SparseMatrix<T> Transpose()
        {
            SparseMatrix<T> transpose = new SparseMatrix<T>(Columns, Rows);
            foreach (KeyValuePair<long, T> pair in _values)
            {
                int row = (int)(pair.Key / Columns), col = (int)(pair.Key % Columns);
                transpose[col, row] = pair.Value;
            }
            return transpose;
        }
        internal T Trace(IBLAS<T> blas)
        {
            T trace = blas.Zero;

            int minorDimension = Math.Min(Rows, Columns);
            if (_values.Count > minorDimension)
            {
                for (int i = 0; i < minorDimension; ++i)
                {
                    long key = get_key(i, i);
                    if (_values.ContainsKey(key))
                    {
                        trace = blas.Add(trace, _values[key]);
                    }
                }
            }
            else
            {
                foreach (KeyValuePair<long, T> pair in _values)
                {
                    long rows = pair.Key / Columns, cols = pair.Key % Columns;
                    if (rows == cols)
                    {
                        trace = blas.Add(trace, pair.Value);
                    }
                }
            }
            return trace;
        }

        public override object Clone()
        {
            return Copy();
        }
        public SparseMatrix<T> Copy()
        {
            Dictionary<long, T> values = new Dictionary<long, T>();
            foreach (var kvp in _values)
            {
                values[kvp.Key] = kvp.Value;
            }
            return new SparseMatrix<T>(Rows, Columns, values);
        }

        public SparseMatrix<T> DirectSum(SparseMatrix<T> matrix) 
        {
            long columns = Columns + matrix.Columns;
            Dictionary<long, T> result = new Dictionary<long, T>(_values.Count + matrix.Values.Count);

            foreach (KeyValuePair<long, T> pair in _values)
            {
                long row = pair.Key / Columns, col = pair.Key % Columns, key = row * columns + col;
                result[key] = pair.Value;
            }
            foreach (KeyValuePair<long, T> pair in matrix.Values)
            {
                long row = Rows + pair.Key / Columns, col = Columns + pair.Key % Columns, key = row * columns + col;
                result[key] = pair.Value;
            }
            return new SparseMatrix<T>(Rows + matrix.Rows, Columns + matrix.Columns, result);
        }

        internal SparseMatrix<T> KroneckerProduct(SparseMatrix<T> matrix, IBLAS<T> blas)
        {
            MatrixChecks.CheckNotNull(matrix);

            int A_rows = Rows, A_cols = Columns, 
                B_rows = matrix.Rows, B_cols = matrix.Columns, 
                rows = A_rows * B_rows, cols = A_cols * B_cols;

            Dictionary<long, T> result = new Dictionary<long, T>(), B_values = matrix.Values;
            foreach (KeyValuePair<long, T> p1 in _values)
            {
                long A_row = p1.Key / A_cols, A_col = p1.Key % A_cols;
                T A_ij = p1.Value;
                foreach (KeyValuePair<long, T> p2 in B_values)
                {
                    long B_row = p2.Key / B_cols, B_col = p2.Key % B_cols;
                    long row = A_row * B_rows + B_row, col = A_col * B_cols + B_col;
                    result[row * cols + col] = blas.Multiply(A_ij, p2.Value);
                }
            }
            return new SparseMatrix<T>(rows, cols, result);
        }

        #region Casts
        public static explicit operator SparseMatrix<T>(DenseMatrix<T> matrix) => new SparseMatrix<T>(matrix);
        public static explicit operator SparseMatrix<T>(DiagonalMatrix<T> matrix) => new SparseMatrix<T>(matrix);
        public static explicit operator SparseMatrix<T>(TriangularMatrix<T> matrix) => new SparseMatrix<T>(matrix);
        #endregion
    }
    public static class SparseMatrix
    {
        private static readonly Random _random = new Random();

        /// <summary>
        /// <para>Create a random sparse matrix of dimensions <txt>rows</txt> $\times$ <txt>columns</txt> with up to <txt>maxNonZeroEntries</txt> non-zero entries.</para>
        /// <!--inputs-->
        /// </summary>
        /// <name>Random_Sparse</name>
        /// <proto>SparseMatrix<T> SparseMatrix<T>.Random(int rows, int columns, int maxNonZeroEntries, Func<T> random)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="rows">The number of rows in the created matrix.</param>
        /// <param name="columns">The number of columns in the created matrix.</param>
        /// <param name="maxNonZeroEntries">The maximum number of non-zero entries in the matrix.</param>
        /// <param name="random">
        /// <b>Optional</b>, the random <txt>T</txt> generator.<br/>
        /// If unspecified, a default generator will be used. See <a href="#Random"><txt><b>random</b></txt></a> for details on default random generators.
        /// </param>
        /// <returns></returns>
        public static SparseMatrix<T> Random<T>(int rows, int columns, int maxNonZeroEntries, Func<T> random = null) where T : new()
        {
            if (random == null)
            {
                random = DefaultGenerators.GetRandomGenerator<T>();
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                values[GetKey(columns, _random.Next(rows), _random.Next(columns))] = random();
            }
            return new SparseMatrix<T>(rows, columns, values);
        }
        public static SparseMatrix<T> RandomTriangular<T>(int rows, int columns, int maxNonZeroEntries, bool upper = true, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int row = _random.Next(rows), column;
                if (upper)
                {
                    if (row >= columns) continue;
                    column = RandomInt(row, columns);
                }
                else
                {
                    column = _random.Next(Math.Min(rows, row + 1));
                }
                values[GetKey(columns, row, column)] = Random();
            }
            return new SparseMatrix<T>(rows, columns, values);
        }
        public static SparseMatrix<T> RandomDiagonal<T>(int rows, int columns, int maxNonZeroEntries, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            int end = Math.Min(rows, columns);
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int row = _random.Next(end);
                values[GetKey(columns, row, row)] = Random();
            }
            return new SparseMatrix<T>(rows, columns, values);
        }
        public static SparseMatrix<T> RandomBidiagonal<T>(int rows, int columns, int maxNonZeroEntries, bool upper = true, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int len = Math.Min(rows, columns);

            // Calculate the number of unique locations in which to sample
            int locations;
            if (rows < columns && !upper) locations = 2 * len - 1;
            else if (rows > columns && upper) locations = 2 * len - 1;
            else locations = 2 * len;

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int location = _random.Next(locations);
                if (upper)
                {
                    int row = location / 2;
                    int col = row + location % 2;
                    values[GetKey(columns, row, col)] = Random();
                }
                else
                {
                    int col = location / 2;
                    int row = col + location % 2;
                    values[GetKey(columns, row, col)] = Random();
                }
            }
            return new SparseMatrix<T>(rows, columns, values);
        }
        public static SparseMatrix<T> RandomTridiagonal<T>(int rows, int columns, int maxNonZeroEntries, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            int len = Math.Min(rows, columns);
            int locations;
            if (rows != columns)
            {
                locations = 3 * len - 1;
            }
            else
            {
                locations = 3 * len - 2;
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int location = _random.Next(locations) + 1;

                int col = location / 3;
                int row = col - 1 + location % 3;

                // The final location might be pivoted
                if (location == locations && rows < columns)
                {
                    row--;
                    col++;
                }

                values[GetKey(columns, row, col)] = Random();
            }

            return new SparseMatrix<T>(rows, columns, values);
        }
        public static SparseMatrix<T> RandomHessenberg<T>(int dimension, int maxNonZeroEntries, bool upper = true, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            Dictionary<long, T> values = new Dictionary<long, T>();
            for (int i = 0; i < maxNonZeroEntries; ++i)
            {
                int row = _random.Next(dimension), col = _random.Next(dimension);
                if (upper && row <= col + 1)
                {
                    values[GetKey(dimension, row, col)] = Random();
                }
                else if (!upper && col <= row + 1)
                {
                    values[GetKey(dimension, row, col)] = Random();
                }
            }
            return new SparseMatrix<T>(dimension, dimension, values);
        }
        public static SparseMatrix<T> Diag<T>(T[] diagonal) where T : new()
        {
            int dim = diagonal.Length, i;
            SparseMatrix<T> matrix = new SparseMatrix<T>(dim, dim);
            for (i = 0; i < dim; ++i)
            {
                matrix[i, i] = diagonal[i];
            }
            return matrix;
        }
        public static SparseMatrix<T> Identity<T>(int dimension) where T : new()
        {
            T one = DefaultGenerators.GetOne<T>();

            SparseMatrix<T> matrix = new SparseMatrix<T>(dimension, dimension);
            for (int i = 0; i < dimension; ++i)
            {
                matrix[i, i] = one;
            }
            return matrix;
        }

        private static long GetKey(int columns, int i, int j)
        {
            return (long)columns * i + j;
        }
        private static int RandomInt(int min, int max)
        {
            return _random.Next(max - min) + min;
        }
    }
}
