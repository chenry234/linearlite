﻿using LinearLite.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Matrix
{
    public class TriangularMatrix<T> : IMatrix<T> where T : new()
    {
        private T _defaultValue = new T();

        private bool _isUpper;

        /// <summary>
        /// Values are stored by rows, irrespective of whether the triangular matrix is upper or lower triangular
        /// </summary>
        private T[][] _values;

        public bool IsUpper { get { return _isUpper; } }
        public override T[] this[int index]
        {
            get
            {
                T[] row = new T[Columns];
                if (_isUpper)
                {
                    for (int i = 0; i < index; ++i)
                    {
                        row[i] = _defaultValue;
                    }

                    T[] values_index = _values[index];
                    for (int i = index; i < Columns; ++i)
                    {
                        row[i] = values_index[i - index];
                    }
                }
                else
                {
                    T[] values_index = _values[index];
                    for (int i = 0; i <= index; ++i)
                    {
                        row[i] = values_index[i];
                    }
                    for (int i = index + 1; i < Columns; ++i)
                    {
                        row[i] = _defaultValue;
                    }
                }
                return row;
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        public override T this[int rowIndex, int columnIndex] 
        {
            get
            {
                if (_isUpper)
                {
                    if (rowIndex > columnIndex)
                    {
                        return _defaultValue;
                    }

                    return _values[rowIndex][columnIndex - rowIndex];
                }
                else
                {
                    if (rowIndex < columnIndex)
                    {
                        return _defaultValue;
                    }

                    return _values[rowIndex][columnIndex];
                }
            }
            set
            {
                if (_isUpper)
                {
                    if (rowIndex > columnIndex && !value.Equals(_defaultValue))
                    {
                        throw new InvalidOperationException();
                    }
                    _values[rowIndex][columnIndex - rowIndex] = value;
                }
                else
                {
                    if (rowIndex < columnIndex && !value.Equals(_defaultValue))
                    {
                        throw new InvalidOperationException();
                    }
                    _values[rowIndex][columnIndex] = value;
                }
            }
        }

        /// <summary>
        /// Construct a triangular matrix of dimensions (rows x columns)
        /// - If 'isUpper' is true, an upper triangular matrix is created
        /// - If 'isUpper' is false, a lower triangular matrix is created. 
        /// </summary>
        /// <param name="rows"></param>
        /// <param name="columns"></param>
        /// <param name="isUpper"></param>
        public TriangularMatrix(int rows, int columns, bool isUpper) : base(rows, columns)
        {
            if (rows < 0 || columns < 0) throw new ArgumentOutOfRangeException();

            _isUpper = isUpper;

            int min = Math.Min(rows, columns);
            _values = new T[min][];
            if (isUpper)
            {
                for (int i = 0; i < min; ++i)
                {
                    _values[i] = new T[columns - i];
                }
            }
            else
            {
                for (int i = 0; i < min; ++i)
                {
                    _values[i] = new T[i + 1];
                }
            }
            
        }
        /// <summary>
        /// Create a triangular matrix using a jagged array of upper/lower triangular array of values.
        /// The structure of 'values' must be either:
        /// - An array of arrays of increasing length, starting with length 1 up to length m (for a lower triangular matrix)
        /// - An array of arrays of decreasing length, starting with length m up to length 1 (for a upper triangular matrix)
        /// 
        /// If the above, m refers to the number of rows in the matrix.
        /// 
        /// </summary>
        /// <param name="values"></param>
        public TriangularMatrix(T[][] values)
        {
            if (values == null) throw new ArgumentNullException();

            Rows = values.Length;
            if (values[0].Length == 1)
            {
                _isUpper = false;

                for (int i = 1; i <= Rows; ++i)
                {
                    if (values[i].Length != i)
                    {
                        throw new InvalidOperationException();
                    }
                }

                Columns = values[Rows - 1].Length;
                _values = values;
            }
            else
            {
                _isUpper = true;

                Columns = values[0].Length;
                for (int i = 0; i < Rows; ++i)
                {
                    if (values[i].Length != Rows - i)
                    {
                        throw new InvalidOperationException();
                    }
                }
                _values = values;
            }
        }
        internal TriangularMatrix(int rows, int columns, bool isUpper, T[][] values) : base(rows, columns)
        {
            _isUpper = isUpper;
            _values = values;
        }

        public override object Clone()
        {
            return Copy();
        }
        public TriangularMatrix<T> Copy()
        {
            throw new NotImplementedException();
            T[][] transpose = new T[Columns][];
            for (int i = 1; i <= Columns; ++i)
            {
                int min = Math.Min(Rows, i);
                T[] row = new T[min];
                for (int j = 0; j < min; ++j)
                {
                    row[j] = _values[j][i];
                }
                transpose[i] = row;
            }
            return new TriangularMatrix<T>(Columns, Rows, !_isUpper, transpose);
        }

    }
    public static class TriangularMatrix
    {
        public static TriangularMatrix<T> Random<T>(int rows, int columns, bool upper = true, Func<T> Random = null) where T : new()
        {
            if (Random == null)
            {
                Random = DefaultGenerators.GetRandomGenerator<T>();
            }

            T[][] values = new T[rows][];
            for (int i = 0; i < rows; ++i)
            {
                int start = upper ? i : 0;
                int end = upper ? columns : Math.Min(i + 1, columns);
                T[] row = new T[end];
                for (int j = 0; j < end; ++j)
                {
                    row[j] = Random();
                }
                values[i] = row;
            }

            return new TriangularMatrix<T>(rows, columns, upper, values);
        }
    }
}
