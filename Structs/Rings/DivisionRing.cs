﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    /// <summary>
    /// A ring over the set of type T that is also equipped with 
    /// a multiplicative inverse. Division rings differ from Fields 
    /// (see interface Field<T>) in that multiplication is not necessarily 
    /// commutative. 
    /// 
    /// Examples of implementations: Quaternion
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface DivisionRing<T> : Ring<T> where T : new()
    {
        /// <summary>
        /// Calculates and returns the multiplicative inverse of (this).
        /// (this) is unchanged, a new struct is created.
        /// </summary>
        /// <returns></returns>
        T MultiplicativeInverse();
    }
    public static class DivisionRingExtensions
    {
        public static T Divide<T>(this T f, T g) where T : DivisionRing<T>, new()
        {
            return f.Multiply(g.MultiplicativeInverse());
        }
    }
}
