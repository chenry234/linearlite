﻿using LinearLite.Solvers;
using LinearLite.Structs.Rings;
using LinearLite.Structs.Rings.Polynomial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    public class Polynomial<T> : Ring<Polynomial<T>> where T : new()
    {
        public int Degree { get { return _polynomial.Degree; } }
        public T[] Coefficients { get { return _polynomial.Coefficients; } }

        private IPolynomial<T> _polynomial;

        private Polynomial(IPolynomial<T> polynomial)
        {
            _polynomial = polynomial;
        }

        /// <summary>
        /// Initialise a polynomial over a field T, using an array of its coefficients. 
        /// </summary>
        /// <param name="coefficients">
        /// For the array [a_n, a_(n-1), ..., a_0] over field x, creates the polynomial 
        /// a_n*x^n + a_(n-1)*x^(n-1) + ... + a_0 
        /// </param>
        public Polynomial(params T[] coefficients)
        {
            // Hacks...
            Type t = typeof(T);
            if (t == typeof(double))
            {
                _polynomial = (dynamic)new DoublePolynomial(coefficients as double[]);
            }
            else if (t == typeof(int))
            {
                _polynomial = (dynamic)new IntegerPolynomial(coefficients as int[]);
            }
            else if (t == typeof(Complex))
            {
                _polynomial = (dynamic)new ComplexPolynomial(coefficients as Complex[]);
            }
            else if (t == typeof(float))
            {
                _polynomial = (dynamic)new FloatPolynomial(coefficients as float[]);
            }
            else if (t == typeof(decimal))
            {
                // DecimalPolynomial
            }
        }
        public Polynomial() : this(new T[0])
        {
            
        }

        /// <summary>
        /// Evaluates the polynomial at point x
        /// </summary>
        /// <param name="x">A point inside field T</param>
        /// <returns>P(x), the polynomial evaluated at point x</returns>
        public T Evaluate(T x)
        {
            return _polynomial.Evaluate(x);
        }

        /// <summary>
        /// Evaluate the derivative of polynomial at a point x.
        /// This method is more efficient that finding a polynomial's derivative 
        /// then evaluating it at x.
        /// </summary>
        /// <param name="x">A point inside field T</param>
        /// <returns>P'(x), the polynomial's derivative evaluated at point x</returns>
        public T EvaluateDerivative(T x)
        {
            return _polynomial.Evaluate(x);
        }

        /// <summary>
        /// Returns the derivative polynomial, P'(x)
        /// </summary>
        /// <returns></returns>
        public virtual Polynomial<T> Derivative()
        {
            return new Polynomial<T>(_polynomial.Derivative());
        }

        /// <summary>
        /// Calculates and returns the real and complex roots of the polynomial
        /// </summary>
        /// <returns></returns>
        public T[] Roots()
        {
            return _polynomial.Roots();
        }

        public static Polynomial<T> operator +(Polynomial<T> p, Polynomial<T> q)
        {
            return p.Add(q);
        }
        public static Polynomial<T> operator -(Polynomial<T> p, Polynomial<T> q)
        {
            return p.Subtract(q);
        }
        public static Polynomial<T> operator -(Polynomial<T> p)
        {
            return p.AdditiveInverse();
        }
        public static Polynomial<T> operator *(Polynomial<T> p, Polynomial<T> q)
        {
            return p.Multiply(q);
        }

        public override string ToString()
        {
            return _polynomial.ToString();
        }

        public Polynomial<T> AdditiveIdentity => new Polynomial<T>(_polynomial.AdditiveIdentity);
        public Polynomial<T> MultiplicativeIdentity => new Polynomial<T>(_polynomial.MultiplicativeIdentity);
        public Polynomial<T> Add(Polynomial<T> q)
        {
            return new Polynomial<T>(_polynomial.Add(q._polynomial));
        }
        public virtual Polynomial<T> Multiply(Polynomial<T> q)
        {
            return new Polynomial<T>(_polynomial.Multiply(q._polynomial));
        }
        public virtual Polynomial<T> AdditiveInverse()
        {
            return new Polynomial<T>(_polynomial.AdditiveInverse());
        }
        public virtual bool Equals(Polynomial<T> e)
        {
            return _polynomial.Equals(e._polynomial);
        }
        public virtual bool ApproximatelyEquals(Polynomial<T> e, double eps)
        {
            return _polynomial.ApproximatelyEquals(e._polynomial, eps);
        }

    }
}
