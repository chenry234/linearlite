﻿using LinearLite.Solvers;
using LinearLite.Structs.Rings.Polynomial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Rings
{
    internal class ComplexPolynomial : IPolynomial<Complex>
    {
        public override IPolynomial<Complex> MultiplicativeIdentity => new ComplexPolynomial(Complex.One);
        public override IPolynomial<Complex> AdditiveIdentity => new ComplexPolynomial(Complex.Zero);

        private ComplexPolynomial(int degree, Complex[] coeffs)
        {
            _degree = degree;
            _coefficients = coeffs;
        }
        internal ComplexPolynomial(params double[] coefficients) : this(coefficients.ToComplex()) { }

        /// <summary>
        /// Convert this into a static argument sanitation
        /// </summary>
        /// <param name="coefficients"></param>
        internal ComplexPolynomial(params Complex[] coeffs)
        {
            // Find the first non-zero coefficient
            int start = 0;
            while (start < coeffs.Length && coeffs[start].ApproximatelyEquals(Complex.Zero, Precision.DOUBLE_PRECISION)) start++;

            if (start == coeffs.Length)
            {
                _degree = int.MinValue;
                _coefficients = new Complex[1] { Complex.Zero };  // 0 constant term
            }
            else
            {
                _degree = coeffs.Length - 1;
                _coefficients = new Complex[coeffs.Length - start];
                for (int d = 0; d < Coefficients.Length; ++d)
                {
                    _coefficients[d] = coeffs[d + start];
                }
            }
        }

        internal override Complex Evaluate(Complex x)
        {
            Complex value = _coefficients[0];
            int len = _coefficients.Length, d;
            for (d = 1; d < len; ++d)
            {
                // value <- value * x
                value.MultiplyEquals(x);
                value.IncrementBy(_coefficients[d]);
            }
            return value;
        }
        internal override Complex EvaluateDerivative(Complex x)
        {
            Complex cPower = Complex.One, derivative = Complex.Zero;
            for (int d = _coefficients.Length - 2; d >= 0; d--)
            {
                int n = _coefficients.Length - d - 1;
                derivative.IncrementBy(cPower.Multiply(_coefficients[d]).Multiply(n));
                cPower = cPower.Multiply(x);
            }
            return derivative;
        }
        internal override IPolynomial<Complex> Derivative()
        {
            // p(x) = 0, p(x) = c => p'(x) = 0
            if (_degree <= 0)
            {
                return new ComplexPolynomial(_degree, new Complex[] { Complex.Zero });
            }

            Complex[] derivative = new Complex[_coefficients.Length - 1];
            for (int i = 0; i < derivative.Length; ++i)
            {
                int degree = derivative.Length - i;
                derivative[i] = _coefficients[i].Multiply(degree);
            }
            return new ComplexPolynomial(_degree - 1, derivative);
        }
        internal override Complex[] Roots()
        {
            AberthMethodSolver solver = new AberthMethodSolver();
            return solver.Solve(this);
        }

        internal override IPolynomial<Complex> Of(IPolynomial<Complex> q)
        {
            throw new NotImplementedException();
        }
        public override IPolynomial<Complex> AdditiveInverse()
        {
            Complex[] coeffs = Coefficients.Copy();
            for (int i = 0; i < coeffs.Length; ++i)
            {
                coeffs[i] = coeffs[i].AdditiveInverse();
            }
            return new ComplexPolynomial(_degree, coeffs);
        }
        public override IPolynomial<Complex> Add(IPolynomial<Complex> q)
        {
            IPolynomial<Complex> min, max;
            if (Degree > q.Degree)
            {
                min = q;
                max = this;
            }
            else
            {
                min = this;
                max = q;
            }

            Complex[] coeffs = max.Coefficients.Copy();
            int offset = coeffs.Length - min.Coefficients.Length;
            for (int i = 0; i < min.Coefficients.Length; ++i)
            {
                coeffs[i + offset].IncrementBy(min.Coefficients[i]);
            }
            return new ComplexPolynomial(coeffs);
        }
        public override IPolynomial<Complex> Multiply(IPolynomial<Complex> q)
        {
            Complex[] a = Coefficients, b = q.Coefficients;

            int d = a.Length + b.Length - 1, i, j;
            Complex[] coeffs = RectangularMatrix.Vector<Complex>(d);
            for (i = 0; i < a.Length; ++i)
                for (j = 0; j < b.Length; ++j)
                {
                    //int deg = d - i - j;
                    coeffs[coeffs.Length - (d - i - j)].IncrementBy(a[i].Multiply(b[j]));
                }

            return new ComplexPolynomial(coeffs);
        }
        public override bool Equals(IPolynomial<Complex> e)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!_coefficients[i].Equals(e.Coefficients[i])) return false;
            }
            return true;
        }
        public override bool ApproximatelyEquals(IPolynomial<Complex> e, double eps)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!_coefficients[i].ApproximatelyEquals(e.Coefficients[i], eps)) return false;
            }
            return true;
        }
        public override string ToString()
        {
            if (Coefficients == null) return "null";
            if (Coefficients.Length == 0) return "0";

            StringBuilder s = new StringBuilder();
            for (int i = 0; i < Coefficients.Length; ++i)
            {
                int power = Degree - i;

                string coeff = Coefficients[i].IsReal() ? Coefficients[i].Real.ToString("n4") : Coefficients[i].ToString();
                if (i > 0)
                {
                    s.Append(" + ");
                }

                string x = "";
                if (power == 1) x = "x";
                else if (power > 1) x = "x^" + power;

                s.Append(coeff + x);
            }
            return s.ToString();
        }
        protected override string ToString(Complex coefficient)
        {
            return coefficient.IsReal() ? coefficient.Real.ToString("n4") : coefficient.ToString();
        }

    }
}
