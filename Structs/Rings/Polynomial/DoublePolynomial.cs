﻿using LinearLite.Structs.Rings.Polynomial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Rings
{
    internal class DoublePolynomial : IPolynomial<double>
    {
        public override IPolynomial<double> MultiplicativeIdentity => new DoublePolynomial(0, 1.0);

        public override IPolynomial<double> AdditiveIdentity => new DoublePolynomial(int.MinValue, 0.0);

        private DoublePolynomial(int degree, params double[] coefficients)
        {
            _degree = degree;
            _coefficients = coefficients;
        }
        internal DoublePolynomial(double[] coeffs)
        {
            int start = 0;
            while (start < coeffs.Length && Util.ApproximatelyEquals(coeffs[start], 0)) start++;

            if (start == coeffs.Length) // 0 constant polynomial
            {
                _degree = int.MinValue;
                _coefficients = new double[] { 0.0 };
            }
            else
            {
                _coefficients = new double[coeffs.Length - start];
                _degree = coeffs.Length - 1;
                for (int d = 0; d < Coefficients.Length; ++d)
                {
                    _coefficients[d] = coeffs[d + start];
                }
            }
        }

        internal override IPolynomial<double> Derivative()
        {
            // p(x) = 0, p(x) = c => p'(x) = 0
            if (_degree <= 0)
            {
                return new DoublePolynomial(_degree, new double[] { 0.0 });
            }

            double[] derivative = new double[_coefficients.Length - 1];
            for (int i = 0; i < derivative.Length; ++i)
            {
                int degree = derivative.Length - i;
                derivative[i] = _coefficients[i] * degree;
            }
            return new DoublePolynomial(_degree - 1, derivative);
        }
        internal override double Evaluate(double x)
        {
            double value = _coefficients[0];
            int len = _coefficients.Length, d;
            for (d = 1; d < len; ++d)
            {
                value = value * x + _coefficients[d];
            }
            return value;
        }
        internal override double EvaluateDerivative(double x)
        {
            double xPower = 1, derivative = 0;
            for (int d = _coefficients.Length - 2; d >= 0; d--)
            {
                int n = _coefficients.Length - d - 1;
                derivative += xPower * _coefficients[d] * n;
                xPower *= x;
            }
            return derivative;
        }
        internal override double[] Roots()
        {
            throw new NotImplementedException();
        }


        internal override IPolynomial<double> Of(IPolynomial<double> q)
        {
            throw new NotImplementedException();
        }
        public override IPolynomial<double> AdditiveInverse()
        {
            double[] coeffs = _coefficients.Copy();
            for (int i = 0; i < coeffs.Length; ++i)
            {
                coeffs[i] = -coeffs[i];
            }
            return new DoublePolynomial(_degree, coeffs);
        }
        public override IPolynomial<double> Add(IPolynomial<double> q)
        {
            IPolynomial<double> min, max;
            if (Degree > q.Degree)
            {
                min = q;
                max = this;
            }
            else
            {
                min = this;
                max = q;
            }

            double[] coeffs = max.Coefficients.Copy();
            int offset = coeffs.Length - min.Coefficients.Length;
            for (int i = 0; i < min.Coefficients.Length; ++i)
            {
                coeffs[i + offset] += min.Coefficients[i];
            }
            return new DoublePolynomial(coeffs);
        }
        public override IPolynomial<double> Multiply(IPolynomial<double> q)
        {
            double[] a = Coefficients, b = q.Coefficients;
            double[] coeffs = RectangularMatrix.Vector<double>(a.Length + b.Length);

            int d = a.Length + b.Length - 1, i, j;
            for (i = 0; i < a.Length; ++i)
                for (j = 0; j < b.Length; ++j)
                {
                    //int deg = d - i - j;
                    coeffs[coeffs.Length - (d - i - j)] += a[i] * b[j];
                }

            return new DoublePolynomial(coeffs);
        }
        public override bool Equals(IPolynomial<double> e)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (_coefficients[i] != e.Coefficients[i]) return false;
            }
            return true;
        }
        public override bool ApproximatelyEquals(IPolynomial<double> e, double eps)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!Util.ApproximatelyEquals(_coefficients[i], e.Coefficients[i], eps)) return false;
            }
            return true;
        }
        protected override string ToString(double coefficient)
        {
            return coefficient.ToString("n4");
        }

    }
}
