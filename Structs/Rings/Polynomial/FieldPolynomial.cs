﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Rings.Polynomial
{
    internal class FieldPolynomial<T> : IPolynomial<T> where T : Field<T>, new()
    {
        private readonly T _zero = default(T).AdditiveIdentity;
        private readonly T _one = default(T).MultiplicativeIdentity;

        public override IPolynomial<T> MultiplicativeIdentity => new FieldPolynomial<T>(_one);
        public override IPolynomial<T> AdditiveIdentity => new FieldPolynomial<T>(_zero);

        internal FieldPolynomial(int degree, T[] coeffs)
        {
            _degree = degree;
            _coefficients = coeffs;
        }
        internal FieldPolynomial(params T[] coeffs)
        {
            int start = 0;
            while (start < coeffs.Length && coeffs[start].ApproximatelyEquals(_zero, Precision.DOUBLE_PRECISION)) start++;

            if (start == coeffs.Length) // 0 constant polynomial
            {
                _degree = int.MinValue;
                _coefficients = new T[] { _zero };
            }
            else
            {
                _coefficients = new T[coeffs.Length - start];
                _degree = coeffs.Length - 1;
                for (int d = 0; d < _coefficients.Length; ++d)
                {
                    _coefficients[d] = coeffs[d + start];
                }
            }
        }

        /// <summary>
        /// In a specific field F, an integer n is defined as n = 1 + 1 + ... + 1, and we use this 
        /// to perform the multiplication of degree and an element in F.
        /// </summary>
        /// <returns></returns>
        internal override IPolynomial<T> Derivative()
        {
            // p(x) = 0, p(x) = c => p'(x) = 0
            if (_degree <= 0)
            {
                return new FieldPolynomial<T>(_degree, new T[] { _zero });
            }

            T[] derivative = new T[_coefficients.Length - 1];
            T degree = _zero;
            for (int i = derivative.Length - 1; i >= 0; --i)
            {
                degree = degree.Add(_one);
                derivative[i] = _coefficients[i].Multiply(degree);
            }
            return new FieldPolynomial<T>(_degree - 1, derivative); 
        }
        internal override T Evaluate(T x)
        {
            T value = _coefficients[0];
            int len = _coefficients.Length, d;
            for (d = 1; d < len; ++d)
            {
                value = value.Multiply(x).Add(_coefficients[d]);
            }
            return value;
        }
        internal override T EvaluateDerivative(T x)
        {
            T xPower = _one, derivative = _zero, degree = _zero;
            for (int d = _coefficients.Length - 2; d >= 0; d--)
            {
                degree = degree.Add(_one);
                derivative = derivative.Add(xPower.Multiply(_coefficients[d]).Multiply(degree));
                xPower = xPower.Multiply(x);
            }
            return derivative;
        }
        internal override T[] Roots()
        {
            throw new NotImplementedException();
        }

        internal override IPolynomial<T> Of(IPolynomial<T> q)
        {
            throw new NotImplementedException();
        }
        public override IPolynomial<T> AdditiveInverse()
        {
            T[] coeffs = _coefficients.Copy();
            for (int i = 0; i < coeffs.Length; ++i)
            {
                coeffs[i] = coeffs[i].AdditiveInverse();
            }
            return new FieldPolynomial<T>(_degree, coeffs);
        }
        public override IPolynomial<T> Add(IPolynomial<T> q)
        {
            IPolynomial<T> min, max;
            if (Degree > q.Degree)
            {
                min = q;
                max = this;
            }
            else
            {
                min = this;
                max = q;
            }

            T[] coeffs = max.Coefficients.Copy();
            int offset = coeffs.Length - min.Coefficients.Length;
            for (int i = 0; i < min.Coefficients.Length; ++i)
            {
                int k = i + offset;
                coeffs[k] = coeffs[k].Add(min.Coefficients[i]);
            }
            return new FieldPolynomial<T>(coeffs);
        }
        public override IPolynomial<T> Multiply(IPolynomial<T> q)
        {
            T[] a = _coefficients, b = q.Coefficients;

            int d = a.Length + b.Length - 1, i, j;

            T[] coeffs = new T[d];
            for (i = 0; i < d; ++i)
            {
                coeffs[i] = _zero;
            }

            for (i = 0; i < a.Length; ++i)
            {
                for (j = 0; j < b.Length; ++j)
                {
                    int k = coeffs.Length - (d - i - j);
                    coeffs[k] = coeffs[k].Add(a[i].Multiply(b[j]));
                }
            }
            return new FieldPolynomial<T>(coeffs);
        }
        public override bool Equals(IPolynomial<T> e)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (_coefficients[i].Equals(e.Coefficients[i])) return false;
            }
            return true;
        }
        public override bool ApproximatelyEquals(IPolynomial<T> e, double eps)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!_coefficients[i].ApproximatelyEquals(e.Coefficients[i], eps)) return false;
            }
            return true;
        }
        protected override string ToString(T coefficient)
        {
            return coefficient.ToString();
        }

    }
}
