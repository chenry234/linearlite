﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Rings.Polynomial
{
    internal class FloatPolynomial : IPolynomial<float>
    {

        public override IPolynomial<float> MultiplicativeIdentity => new FloatPolynomial(1.0f);

        public override IPolynomial<float> AdditiveIdentity => new FloatPolynomial(0.0f);

        private FloatPolynomial(int degree, float[] coeffs)
        {
            _degree = degree;
            _coefficients = coeffs;
        }
        internal FloatPolynomial(params float[] coeffs)
        {
            int start = 0;
            while (start < coeffs.Length && Util.ApproximatelyEquals(coeffs[start], 0)) start++;

            if (start == coeffs.Length) // 0 constant polynomial
            {
                _degree = int.MinValue;
                _coefficients = new float[] { 0.0f };
            }
            else
            {
                _coefficients = new float[coeffs.Length - start];
                _degree = coeffs.Length - 1;
                for (int d = 0; d < Coefficients.Length; ++d)
                {
                    _coefficients[d] = coeffs[d + start];
                }
            }
        }

        internal override IPolynomial<float> Derivative()
        {
            // p(x) = 0, p(x) = c => p'(x) = 0
            if (_degree <= 0)
            {
                return new FloatPolynomial(_degree, new float[] { 0.0f });
            }

            float[] derivative = new float[_coefficients.Length - 1];
            for (int i = 0; i < derivative.Length; ++i)
            {
                int degree = derivative.Length - i;
                derivative[i] = _coefficients[i] * degree;
            }
            return new FloatPolynomial(_degree - 1, derivative);
        }
        internal override float Evaluate(float x)
        {
            float value = _coefficients[0];
            int len = _coefficients.Length, d;
            for (d = 1; d < len; ++d)
            {
                value = value * x + _coefficients[d];
            }
            return value;
        }
        internal override float EvaluateDerivative(float x)
        {
            float xPower = 1, derivative = 0;
            for (int d = _coefficients.Length - 2; d >= 0; d--)
            {
                int n = _coefficients.Length - d - 1;
                derivative += xPower * _coefficients[d] * n;
                xPower *= x;
            }
            return derivative;
        }
        internal override float[] Roots()
        {
            throw new NotImplementedException();
        }


        internal override IPolynomial<float> Of(IPolynomial<float> q)
        {
            throw new NotImplementedException();
        }
        public override IPolynomial<float> AdditiveInverse()
        {
            float[] coeffs = _coefficients.Copy();
            for (int i = 0; i < coeffs.Length; ++i)
            {
                coeffs[i] = -coeffs[i];
            }
            return new FloatPolynomial(_degree, coeffs);
        }
        public override IPolynomial<float> Add(IPolynomial<float> q)
        {
            IPolynomial<float> min, max;
            if (Degree > q.Degree)
            {
                min = q;
                max = this;
            }
            else
            {
                min = this;
                max = q;
            }

            float[] coeffs = max.Coefficients.Copy();
            int offset = coeffs.Length - min.Coefficients.Length;
            for (int i = 0; i < min.Coefficients.Length; ++i)
            {
                coeffs[i + offset] += min.Coefficients[i];
            }
            return new FloatPolynomial(coeffs);
        }
        public override IPolynomial<float> Multiply(IPolynomial<float> q)
        {
            float[] a = Coefficients, b = q.Coefficients;
            float[] coeffs = new float[a.Length + b.Length - 1];

            int d = a.Length + b.Length - 1, i, j;
            for (i = 0; i < a.Length; ++i)
                for (j = 0; j < b.Length; ++j)
                {
                    //int deg = d - i - j;
                    coeffs[coeffs.Length - (d - i - j)] += a[i] * b[j];
                }

            return new FloatPolynomial(coeffs);
        }
        public override bool Equals(IPolynomial<float> e)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (_coefficients[i] != e.Coefficients[i]) return false;
            }
            return true;
        }
        public override bool ApproximatelyEquals(IPolynomial<float> e, double eps)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!Util.ApproximatelyEquals(_coefficients[i], e.Coefficients[i], eps)) return false;
            }
            return true;
        }
        protected override string ToString(float coefficient)
        {
            return coefficient.ToString("n4");
        }

    }
}
