﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Rings.Polynomial
{
    internal abstract class IPolynomial<T>
    {
        protected int _degree;
        protected T[] _coefficients;

        internal int Degree { get { return _degree; } }
        internal T[] Coefficients { get { return _coefficients; } }

        public abstract IPolynomial<T> MultiplicativeIdentity { get; }
        public abstract IPolynomial<T> AdditiveIdentity { get; }

        protected IPolynomial()
        {

        }

        /// <summary>
        /// Evaluates the polynomial at point x, P(x): T -> T 
        /// </summary>
        /// <param name="x"></param>
        /// <returns>p(x)</returns>
        internal abstract T Evaluate(T x);

        /// <summary>
        /// Evaluate the derivative of polynomial at a point c
        /// This method is more efficient that finding a polynomial's derivative 
        /// then evaluating it at a point x.
        /// </summary>
        /// <param name="x">The point where we evaluate the polynomial's derivative</param>
        /// <returns>P'(x) where P(x) is the polynomial</returns>
        internal abstract T EvaluateDerivative(T x);

        /// <summary>
        /// Returns the derivative polynomial 
        /// </summary>
        /// <returns></returns>
        internal abstract IPolynomial<T> Derivative();

        /// <summary>
        /// Calculates and returns the real and complex roots of the polynomial
        /// </summary>
        /// <returns></returns>
        internal abstract T[] Roots();

        /// <summary>
        /// Evaluates (this) o q, the composition operator
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        internal abstract IPolynomial<T> Of(IPolynomial<T> q);

        public abstract IPolynomial<T> AdditiveInverse();
        public abstract IPolynomial<T> Add(IPolynomial<T> q);
        public abstract IPolynomial<T> Multiply(IPolynomial<T> q);
        public abstract bool Equals(IPolynomial<T> e);
        public abstract bool ApproximatelyEquals(IPolynomial<T> e, double eps);

        protected abstract string ToString(T coefficient);
        public override string ToString()
        {
            if (Coefficients == null) return "null";
            if (Coefficients.Length == 0) return "0";

            StringBuilder s = new StringBuilder();
            for (int i = 0; i < Coefficients.Length; ++i)
            {
                int power = Degree - i;

                string coeff = ToString(Coefficients[i]);
                if (i > 0)
                {
                    s.Append(" + ");
                }

                string x = "";
                if (power == 1) x = "x";
                else if (power > 1) x = "x^" + power;

                s.Append(coeff + x);
            }
            return s.ToString();
        }
    }
}
