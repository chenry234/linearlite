﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Rings.Polynomial
{
    internal class IntegerPolynomial : IPolynomial<int>
    {
        public override IPolynomial<int> MultiplicativeIdentity => new IntegerPolynomial(int.MinValue, new int[] { 1 });
        public override IPolynomial<int> AdditiveIdentity => new IntegerPolynomial(int.MinValue, new int[] { 0 });

        private IntegerPolynomial(int degree, int[] coeffs)
        {
            _degree = degree;
            _coefficients = coeffs;
        }
        internal IntegerPolynomial(params int[] coeffs)
        {
            int start = 0;
            while (start < coeffs.Length && coeffs[start] == 0) start++;

            if (start == coeffs.Length) // 0 constant polynomial
            {
                _degree = int.MinValue;
                _coefficients = new int[] { 0 };
            }
            else
            {
                _coefficients = new int[coeffs.Length - start];
                _degree = coeffs.Length - 1;
                for (int d = 0; d < Coefficients.Length; ++d)
                {
                    _coefficients[d] = coeffs[d + start];
                }
            }
        }


        internal override IPolynomial<int> Derivative()
        {
            // p(x) = 0, p(x) = c => p'(x) = 0
            if (_degree <= 0)
            {
                return new IntegerPolynomial(_degree, new int[] { 0 });
            }

            int[] derivative = new int[_coefficients.Length - 1];
            for (int i = 0; i < derivative.Length; ++i)
            {
                int degree = derivative.Length - i;
                derivative[i] = _coefficients[i] * degree;
            }
            return new IntegerPolynomial(_degree - 1, derivative);
        }

        /// <summary>
        /// This implementation uses Horner's evaluation method
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        internal override int Evaluate(int x)
        {
            int value = _coefficients[0], len = _coefficients.Length, d;
            for (d = 1; d < len; ++d)
            {
                value = value * x + _coefficients[d];
            }
            return value;
        }
        internal override int EvaluateDerivative(int x)
        {
            int xPower = 1, derivative = 0;
            for (int d = _coefficients.Length - 2; d >= 0; d--)
            {
                int n = _coefficients.Length - d - 1;
                derivative += xPower * _coefficients[d] * n;
                xPower *= x;
            }
            return derivative;
        }
        internal override int[] Roots()
        {
            throw new NotImplementedException();
        }


        internal override IPolynomial<int> Of(IPolynomial<int> q)
        {
            throw new NotImplementedException();
        }
        public override IPolynomial<int> AdditiveInverse()
        {
            int[] coeffs = _coefficients.Copy();
            for (int i = 0; i < coeffs.Length; ++i)
            {
                coeffs[i] = -coeffs[i];
            }
            return new IntegerPolynomial(_degree, coeffs);
        }
        public override IPolynomial<int> Add(IPolynomial<int> q)
        {
            IPolynomial<int> min, max;
            if (Degree > q.Degree)
            {
                min = q;
                max = this;
            }
            else
            {
                min = this;
                max = q;
            }

            int[] coeffs = max.Coefficients.Copy();
            int offset = coeffs.Length - min.Coefficients.Length;
            for (int i = 0; i < min.Coefficients.Length; ++i)
            {
                coeffs[i + offset] += min.Coefficients[i];
            }
            return new IntegerPolynomial(coeffs);
        }
        public override IPolynomial<int> Multiply(IPolynomial<int> q)
        {
            int[] a = Coefficients, b = q.Coefficients;
            int[] coeffs = new int[a.Length + b.Length - 1];

            int d = a.Length + b.Length - 1, i, j;
            for (i = 0; i < a.Length; ++i)
                for (j = 0; j < b.Length; ++j)
                {
                    //int deg = d - i - j;
                    coeffs[coeffs.Length - (d - i - j)] += a[i] * b[j];
                }

            return new IntegerPolynomial(coeffs);
        }
        public override bool Equals(IPolynomial<int> e)
        {
            if (this == null || e == null) return false;
            if (_degree != e.Degree) return false;

            int len = _coefficients.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (_coefficients[i] != e.Coefficients[i]) return false;
            }
            return true;
        }
        public override bool ApproximatelyEquals(IPolynomial<int> e, double eps)
        {
            return Equals(e); // integers are precise
        }
        protected override string ToString(int coefficient)
        {
            return coefficient.ToString();
        }

    }
}
