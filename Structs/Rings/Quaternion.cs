﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    /// <summary>
    /// Quaternions are division algebras not fields due to non-commutativity, 
    /// however just for implementation purposes we extend the Field methods 
    /// </summary>
    public struct Quaternion : DivisionRing<Quaternion>
    {
        public double Real { get; set; }
        public double I { get; set; }
        public double J { get; set; }
        public double K { get; set; }

        public static Quaternion Zero = new Quaternion(0, 0, 0, 0);
        public static Quaternion One = new Quaternion(1, 0, 0, 0);

        public Quaternion AdditiveIdentity => Zero;

        public Quaternion MultiplicativeIdentity => One;

        public Quaternion(double real, double i, double j, double k)
        {
            Real = real;
            I = i;
            J = j;
            K = k;
        }
        public Quaternion(double real)
        {
            Real = real;
            I = 0;
            J = 0;
            K = 0;
        }
        public Quaternion(Complex c)
        {
            Real = c.Real;
            I = c.Imaginary;
            J = 0;
            K = 0;
        }

        public double Norm()
        {
            if (double.IsNaN(Real) || double.IsNaN(I) || double.IsNaN(J) || double.IsNaN(K)) return double.NaN;
            return Math.Sqrt(Math.Max(0, Real * Real + I * I + J * J + K * K));
        }
        public double DistanceTo(Quaternion q)
        {
            return (this - q).Norm();
        }

        public Quaternion Conjugate()
        {
            return new Quaternion(Real, -I, -J, -K);
        }
        public Quaternion Unit()
        {
            double norm = Norm();
            return new Quaternion(Real / norm, I / norm, J / norm, K / norm);
        }

        public Quaternion AdditiveInverse()
        {
            return new Quaternion(-Real, -I, -J, -K);
        }
        public Quaternion MultiplicativeInverse()
        {
            double divisor = Real * Real + I * I + J * J + K * K;
            return new Quaternion(Real / divisor, -I / divisor, -J / divisor, -K / divisor);
        }
        public Quaternion Add(Quaternion e)
        {
            return new Quaternion(Real + e.Real, I + e.I, J + e.J, K + e.K);
        }
        public Quaternion Multiply(Quaternion e)
        {
            return new Quaternion(
                Real * e.Real - I * e.I - J * e.J - K * e.K,
                Real * e.I + I * e.Real + J * e.K - K * e.J,
                Real * e.J + J * e.Real - I * e.K + K * e.I,
                Real * e.K + K * e.Real + I * e.J - J * e.I
                );
        }
        public bool Equals(Quaternion e)
        {
            return (Real == e.Real) && (I == e.I) && (J == e.J) && (K == e.K);
        }
        public bool ApproximatelyEquals(Quaternion e, double eps)
        {
            return Util.ApproximatelyEquals(Real, e.Real, eps) &&
                Util.ApproximatelyEquals(I, e.I, eps) &&
                Util.ApproximatelyEquals(J, e.J, eps) &&
                Util.ApproximatelyEquals(K, e.K, eps);
        }

        public static Quaternion operator -(Quaternion q)
        {
            return new Quaternion(-q.Real, -q.I, -q.J, -q.K);
        }
        public static Quaternion operator +(Quaternion p, Quaternion q)
        {
            return p.Add(q);
        }
        public static Quaternion operator -(Quaternion p, Quaternion q)
        {
            return p.Subtract(q);
        }
        public static Quaternion operator *(Quaternion p, Quaternion q)
        {
            return p.Multiply(q);
        }
        public static Quaternion operator *(double s, Quaternion p)
        {
            return new Quaternion(s * p.Real, s * p.I, s * p.J, s * p.K);
        }
        public static Quaternion operator *(Quaternion p, double s) => s * p;
        public static Quaternion operator /(Quaternion p, Quaternion q)
        {
            return p.Divide(q);
        }

        public static implicit operator Quaternion(double d)
        {
            return new Quaternion(d);
        }
        public static implicit operator Quaternion(Complex c)
        {
            return new Quaternion(c);
        }

        public override string ToString()
        {
            return $"{Real.ToString("n4")} + {I.ToString("n4")}i + {J.ToString("n4")}j + {K.ToString("n4")}k";
        }

    }
}
