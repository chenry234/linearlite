﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    /// <summary>
    /// Implements a Ring structure.
    /// 
    /// The following methods are inherited from Group<T>
    /// 
    /// T AdditiveIdentity { get; }
    /// T Add(T e);
    /// T AdditiveInverse();
    /// 
    /// The following methods are inherited from SemiRing<T>
    /// 
    /// T AdditiveIdentity { get; }
    /// T Add(T e);
    /// T MultiplicativeIdentity { get; }
    /// T Multiply(T e);
    /// 
    /// Note that Ring<T> has the additional requirement over Group<T>
    /// in that the Add(e) method must be commutative, i.e. e.Add(f) = f.Add(e)
    /// for any two elements e, f in Ring<T>. 
    /// Non-commutativity will lead to unexpected results when computing
    /// with Rings.
    /// 
    /// Ring<T> is also an extension over SemiRing<T> since it has an 
    /// additive inverse 
    /// 
    /// </summary>
    public interface Ring<T> : AdditiveGroup<T>, SemiRing<T> where T : new()
    {

    }
}
