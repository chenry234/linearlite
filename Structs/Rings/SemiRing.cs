﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs
{
    /// <summary>
    /// Represents the algebraic structure of a semiring over the set T, which differs from 
    /// a ring in that elements do not necessarily have an additive inverse
    /// 
    /// This interface inherits: 
    /// - T Add(T e);                   from AdditiveSemigroup<T>
    /// - T AdditiveIdentity { get; }   from AdditiveMonoid<T>
    /// 
    /// The Ring<T> interface extends this interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface SemiRing<T> : AdditiveMonoid<T> where T : new()
    {
        /// <summary>
        /// The binary multiplicative operator, returns (this) * b without altering (this) or b
        /// Note that multiplication does not need to commute for Semirings.
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        T Multiply(T b);

        /// <summary>
        /// The element e (of type T) in Ring<T> such that 
        /// e * a = a * e = e for any element a in Ring<T>, 
        /// where * is the multiplication operator. 
        /// </summary>
        T MultiplicativeIdentity { get; }
    }
}
