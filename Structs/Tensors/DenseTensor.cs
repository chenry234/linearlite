﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using LinearLite.Structs.Vectors;
using LinearLite.Tensors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Tensors
{
    public class DenseTensor<T> : ITensor<T> where T : new()
    {
        private int _order;
        public override int Order => _order;

        private DenseTensor<T>[] _subtensors;
        private T[] _values;

        internal T[] Values { get { return _values; } }
        public DenseTensor<T> this[int index]
        {
            get
            {
                if (_order == 0)
                {
                    throw new InvalidOperationException($"Cannot access the index '{ index }' of 0-dimensional tensor.");
                }
                if (_order == 1)
                {
                    if (index < 0 || index >= _values.Length)
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    return new DenseTensor<T>(_values);
                }

                if (index < 0 || index >= _subtensors.Length)
                {
                    throw new ArgumentOutOfRangeException();
                }
                return _subtensors[index];
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException();
                }
                if (_order == 0)
                {
                    throw new InvalidOperationException($"Cannot access the index '{ index }' of 0-dimensional tensor.");
                }
                if (_order == 1)
                {
                    if (value._order == 0)
                    {
                        throw new InvalidOperationException($"Expected a 0-order tensor, received a {value._order}-order tensor.");
                    }
                    if (index < 0 || index >= _values.Length)
                    {
                        throw new ArgumentOutOfRangeException();
                    }
                    _values = value._values;
                }

                if (index < 0 || index >= _subtensors.Length)
                {
                    throw new ArgumentOutOfRangeException();
                }
                if (value._order != _order - 1)
                {
                    throw new InvalidOperationException($"Expected a {_order - 1}-order tensor, received a {value._order}-order tensor.");
                }
                if (!_subtensors[index].DimensionsEquals(value))
                {
                    throw new InvalidOperationException($"Dimensions don't match: [{ string.Join(", ", _subtensors[index].GetDimensions().Select(d => d.ToString())) }] vs [{ string.Join(", ", value.GetDimensions()) }]");
                }
                _subtensors[index] = value;
            }
        }

        #region Constructors

        public DenseTensor(params int[] dimensions)
        {
            if (dimensions == null)
            {
                throw new ArgumentNullException();
            }
            _order = dimensions.Length;

            if (dimensions.Length == 0)
            {
                _subtensors = null;
                _values = new T[1];
            }
            else if (dimensions.Length == 1)
            {
                _subtensors = null;
                _values = new T[dimensions[0]];
            }
            else
            {
                _subtensors = new DenseTensor<T>[dimensions[0]];
                _values = null;

                int[] subdimensions = new int[dimensions.Length - 1];
                for (int d = 1; d < dimensions.Length; ++d)
                {
                    subdimensions[d - 1] = dimensions[d];
                }
                for (int d = 0; d < _subtensors.Length; ++d)
                {
                    _subtensors[d] = new DenseTensor<T>(subdimensions);
                }
            }
        }
        
        public DenseTensor(T value)
        {
            _order = 0;
            _subtensors = null;
            _values = new T[1] { value };
        }
        public DenseTensor(T[] vector)
        {
            if (vector == null) throw new ArgumentNullException();

            _order = 1;
            _subtensors = null;
            _values = vector;
        }
        public DenseTensor(DenseVector<T> vector) : this(vector.Values) { }

        public DenseTensor(T[,] matrix) : this(matrix.ToJagged()) { }
        public DenseTensor(T[][] matrix)
        {
            if (matrix == null) throw new ArgumentNullException();

            _order = 2;
            _subtensors = new DenseTensor<T>[matrix.Length];
            for (int d = 0; d < matrix.Length; ++d)
            {
                _subtensors[d] = new DenseTensor<T>(matrix[d]);
            }
            _values = null;
        }
        public DenseTensor(DenseMatrix<T> matrix) : this(matrix.Values) { }

        public DenseTensor(T[,,] tensor) : this(DenseTensorOperationsExtensions.ToJagged(tensor)) { }
        public DenseTensor(T[][][] tensor)
        {
            _order = 3;
            _subtensors = new DenseTensor<T>[tensor.Length];
            for (int d = 0; d < tensor.Length; ++d)
            {
                _subtensors[d] = new DenseTensor<T>(tensor[d]);
            }
            _values = null;
        }

        public DenseTensor(T[,,,] tensor) : this(DenseTensorOperationsExtensions.ToJagged(tensor)) { }
        public DenseTensor(T[][][][] tensor)
        {
            _order = 4;
            _subtensors = new DenseTensor<T>[tensor.Length];
            for (int d = 0; d < tensor.Length; ++d)
            {
                _subtensors[d] = new DenseTensor<T>(tensor[d]);
            }
            _values = null;
        }

        internal DenseTensor(DenseTensor<T>[] subtensors)
        {
            _order = subtensors[0].Order + 1;
            _subtensors = subtensors;
            _values = null;
        }

        #endregion Constructors


        #region Private methods

        internal T get_unsafe(int[] index)
        {
            if (_order == 0)
            {
                return _values[0];
            }

            // Argument range check required for _order >= 1
            int ind = index[index.Length - _order];

            if (_order == 1)
            {
                if (ind < 0 || ind >= _values.Length)
                {
                    throw new ArgumentOutOfRangeException();
                }
                return _values[ind];
            }

            // otherwise, recurse from the subtensor
            if (ind < 0 || ind >= _subtensors.Length)
            {
                throw new ArgumentOutOfRangeException();
            }
            return _subtensors[ind].get_unsafe(index);
        }
        private void set_unsafe(int[] index, T value)
        {
            if (_order == 0)
            {
                _values[0] = value;
                return;
            }

            int ind = index[index.Length - _order];
            if (_order == 1)
            {
                if (ind < 0 || ind >= _values.Length)
                {
                    throw new ArgumentOutOfRangeException();
                }
                _values[ind] = value;
                return;
            }

            if (ind < 0 || ind >= _subtensors.Length)
            {
                throw new ArgumentOutOfRangeException();
            }
            _subtensors[ind].set_unsafe(index, value);
        }
        private void transform_unsafe(int[] index, Func<T, T> transformation)
        {
            if (_order == 0)
            {
                _values[0] = transformation(_values[0]);
                return;
            }

            // Argument range check required for _order >= 1
            int ind = index[index.Length - _order];

            if (_order == 1)
            {
                if (ind < 0 || ind >= _values.Length)
                {
                    throw new ArgumentOutOfRangeException();
                }
                _values[ind] = transformation(_values[ind]);
                return;
            }

            // otherwise, recurse from the subtensor
            if (ind < 0 || ind >= _subtensors.Length)
            {
                throw new ArgumentOutOfRangeException();
            }
            _subtensors[ind].transform_unsafe(index, transformation);
        }
        private void get_dimensions(List<int> dimensions)
        {
            if (_order == 0)
            {
                return;
            }

            if (_order == 1)
            {
                dimensions.Add(_values.Length);
                return;
            }

            dimensions.Add(_subtensors.Length);
            _subtensors[0].get_dimensions(dimensions);
        }

        /// <summary>
        /// Perform binary operation without checking for dimensionality, since this method is 
        /// called recursively so it must be kept light
        /// </summary>
        /// <param name="S"></param>
        /// <param name="Operation"></param>
        /// <returns></returns>
        private DenseTensor<T> elementwise_operate_unsafe(DenseTensor<T> S, Func<T, T, T> Operation)
        {
            if (_order == 0)
            {
                return new DenseTensor<T>(Operation(_values[0], S._values[0]));
            }
            if (_order == 1)
            {
                T[] result = new T[_values.Length];
                for (int d = 0; d < _values.Length; ++d)
                {
                    result[d] = Operation(_values[d], S._values[d]);
                }
                return new DenseTensor<T>(result);
            }

            DenseTensor<T>[] res = new DenseTensor<T>[_subtensors.Length];
            for (int d = 0; d < res.Length; ++d)
            {
                res[d] = _subtensors[d].elementwise_operate_unsafe(S._subtensors[d], Operation);
            }
            return new DenseTensor<T>(res);
        }
        /// <summary>
        /// Overwrite the corresponding fields of (this) with the elements of S.
        /// </summary>
        /// <param name="S"></param>
        private void overwrite_unsafe(DenseTensor<T> S)
        {
            if (_order == 0)
            {
                _values[0] = S._values[0];
                return;
            }
            if (_order == 1)
            {
                for (int i = 0; i < S._values.Length; ++i)
                {
                    _values[i] = S._values[i];
                }
                return;
            }

            for (int i = 0; i < S._subtensors.Length; ++i)
            {
                _subtensors[i].overwrite_unsafe(S._subtensors[i]);
            }
        }

        /// <summary>
        /// Extract a column given all the indices fixed except for the index at dimension 'index'
        /// No checks are done to make sure that column is actually of the correct dimensionality
        /// </summary>
        /// <param name="dimensionIndex"></param>
        /// <param name="fixedIndices"></param>
        /// <param name="column"></param>
        private void get_fibre_unsafe(int dimensionIndex, int[] fixedIndices, T[] column)
        {
            int len = column.Length, i;
            for (i = 0; i < len; ++i)
            {
                fixedIndices[dimensionIndex] = i;
                column[i] = get_unsafe(fixedIndices);
            }
        }
        private void set_fibre_unsafe(int dimensionIndex, int[] fixedIndices, T[] column)
        {
            int len = column.Length, i;
            for (i = 0; i < len; ++i)
            {
                fixedIndices[dimensionIndex] = i;
                set_unsafe(fixedIndices, column[i]);
            }
        }

        /// <summary>
        /// For any dense tensor, shift each dimension by the corresponding entry in indexes
        /// useful in performing dense direct sums
        /// </summary>
        /// <param name="indexes"></param>
        /// <returns></returns>
        private DenseTensor<T> shift_unsafe(int currIndex, int[] indexes)
        {
            // Shifting is not defined for 0-dimensional tensors (scalars)
            if (_order == 0)
            {
                return new DenseTensor<T>(_values[0]);
            }

            int shift = indexes[currIndex];
            if (_order == 1)
            {
                T[] shifted_values = new T[_values.Length + shift];
                for (int i = 0; i < _values.Length; ++i)
                {
                    shifted_values[shift + i] = _values[i];
                }
                return new DenseTensor<T>(shifted_values);
            }

            // Higher order tensors - shift the dense matrices themselves?
            DenseTensor<T>[] shifted_subtensors = new DenseTensor<T>[shift + _subtensors.Length];

            // Need to first initialise the subtensors occupying "empty space"
            List<int> subDims = _subtensors[0].GetDimensions();
            int[] shiftedSubDims = new int[subDims.Count];
            for (int i = 0; i < subDims.Count; ++i)
            {
                shiftedSubDims[i] = subDims[i] + indexes[currIndex + i + 1];
            }
            for (int i = 0; i < shift; ++i)
            {
                shifted_subtensors[i] = new DenseTensor<T>(shiftedSubDims);
            }

            // Shift recursively
            for (int i = 0; i < _subtensors.Length; ++i)
            {
                shifted_subtensors[i + shift] = _subtensors[i].shift_unsafe(currIndex + 1, indexes);
            }
            return new DenseTensor<T>(shifted_subtensors);
        }

        /// <summary>
        /// Store all the tensor elements inside the 'values' dictionary, where all elements are 
        /// indexed sequentially, starting with T[0][0]...[0] = 0 and traversing depth-first.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="startingIndex"></param>
        private void to_indexed_unsafe(Dictionary<long, T> values, ref long startingIndex)
        {
            if (_order == 0)
            {
                values[startingIndex++] = _values[0];
            }
            else if (_order == 1)
            {
                long index = startingIndex;
                int len = _values.Length, i;
                for (i = 0; i < len; ++i)
                {
                    values[index++] = _values[i];
                }
                startingIndex = index;
            }
            else
            {
                foreach (DenseTensor<T> subtensor in _subtensors)
                {
                    subtensor.to_indexed_unsafe(values, ref startingIndex);
                }
            }
        }
        /// <summary>
        /// Traverse through the tensor depth-first, checking if all values match the values inside 
        /// 'values' dictionary.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="startingIndex"></param>
        private bool equals_unsafe(Dictionary<long, T> values, Func<T, T, bool> Equals, ref long startingIndex)
        {
            if (_order == 0)
            {
                // Don't need to re-base around index
                return values.ContainsKey(0L) && Equals(values[0L], _values[0]);
            }
            else if (_order == 1)
            {
                int len = _values.Length, j;
                long i;
                for (i = startingIndex, j = 0; i < len; ++i, ++j)
                {
                    if (!values.ContainsKey(i) || !Equals(values[i], _values[j])) return false;
                }
                startingIndex = i;
                return true;
            }
            else
            {
                foreach (DenseTensor<T> subtensor in _subtensors)
                {
                    if (!subtensor.equals_unsafe(values, Equals, ref startingIndex))
                    {
                        return false;
                    }
                }
                return true;
            }

        }

        #endregion Private methods

        internal bool Equals(DenseTensor<T> tensor, Func<T, T, bool> Equals)
        {
            if (tensor == null || !DimensionsEquals(tensor)) return false;

            if (_order == 0)
            {
                return Equals(_values[0], tensor._values[0]);
            }
            if (_order == 1)
            {
                int len = _values.Length, i;
                for (i = 0; i < len; ++i)
                {
                    if (!Equals(_values[i], tensor._values[i])) return false;
                }
                return true;
            }
            else
            {
                // Higher order tensors
                int len = _subtensors.Length, i;
                for (i = 0; i < len; ++i)
                {
                    if (!_subtensors[i].Equals(tensor._subtensors[i], Equals)) return false;
                }
                return true;
            }
        }
        internal bool Equals(SparseTensor<T> tensor, Func<T, T, bool> Equals)
        {
            long startingIndex = 0;
            return equals_unsafe(tensor.Values, Equals, ref startingIndex);
        }

        /// <summary>
        /// <para>
        /// Given two order-$n$ tensors $\mathcal{S}, \mathcal{T} \in \mathbb{F}^{d_1 \times d_2 \times ... \times d_n }$, 
        /// and a binary operation $\circ : \mathbb{F} \times \mathbb{F} \to \mathbb{F}$, returns the tensor $\mathcal{S} \circ \mathcal{T}$ generated by 
        /// applying the operation elementwise to each corresponding pair of elements in $\mathcal{S}, \mathcal{T}$, i.e. 
        /// $$(\mathcal{S} \circ \mathcal{T})_{i_1 i_2 ... i_n} := \mathcal{S}_{i_1 i_2 ... i_n} \circ \mathcal{T}_{i_1 i_2 ... i_n},$$
        /// for all $1\le{i_j}\le{d_j}$ where $1\le{j}\le{n}$.
        /// </para>
        /// </summary>
        /// <name>ElementwiseOperateTensor</name>
        /// <proto>ITensor<F> ElementwiseOperate(this ITensor<F> S, ITensor<F> T, Func<F, F, F> Operation)</proto>
        /// <cat>la</cat>
        /// <param name="S">Some tensor with the same dimensions as this tensor</param>
        /// <returns></returns>
        public DenseTensor<T> ElementwiseOperate(DenseTensor<T> S, Func<T, T, T> Operation)
        {
            if (S == null)
            {
                throw new ArgumentNullException("Tensor is null");
            }
            if (_order != S._order)
            {
                throw new InvalidOperationException("Dimensions of tensors don't match");
            }

            List<int> d1 = GetDimensions(), d2 = S.GetDimensions();
            for (int i = 0; i < d1.Count; ++i)
            {
                if (d1[i] != d2[i])
                {
                    throw new InvalidOperationException("Dimensions of tensors don't match");
                }
            }
            return elementwise_operate_unsafe(S, Operation);
        }
        internal DenseTensor<T> TensorProduct(DenseTensor<T> S, Func<T, T, T> Product)
        {
            if (_order == 0)
            {
                return new DenseTensor<T>(Product(_values[0], S._values[0]));
            }

            if (_order == 1)
            {
                int i, j, k = 0, m = _values.Length, n = S._values.Length;
                T[] prod = new T[m * n];
                for (i = 0; i < m; ++i)
                {
                    T v_i = _values[i];
                    for (j = 0; j < n; ++j)
                    {
                        prod[k++] = Product(v_i, S._values[j]);
                    }
                }
                return new DenseTensor<T>(prod);
            }
            else
            {
                int m = _subtensors.Length, n = S._subtensors.Length, i, j, k = 0;
                DenseTensor<T>[] subtensorProd = new DenseTensor<T>[m * n];
                for (i = 0; i < m; ++i)
                {
                    for (j = 0; j < n; ++j)
                    {
                        subtensorProd[k++] = _subtensors[i].TensorProduct(S._subtensors[j], Product);
                    }
                }
                return new DenseTensor<T>(subtensorProd);
            }
        }
        internal DenseTensor<T> NModeProduct(int n, DenseMatrix<T> matrix, Func<DenseMatrix<T>, T[], T[]> Product)
        {
            if (n < 0 || n >= _order)
            {
                throw new ArgumentOutOfRangeException();
            }
            MatrixChecks.CheckNotNull(matrix);

            List<int> dimensions = GetDimensions();
            if (dimensions[n] != matrix.Columns)
            {
                throw new InvalidOperationException();
            }

            // Naive implementation - unoptimised 
            int[] index = new int[_order];
            int[] newDimensions = dimensions.ToArray();
            newDimensions[n] = matrix.Rows;

            DenseTensor<T> product = new DenseTensor<T>(newDimensions);
            T[] column = new T[dimensions[n]];

            int last = _order - 1, start = (n == last) ? last - 1 : last;
            while (true)
            {
                get_fibre_unsafe(n, index, column);
                T[] columnProd = Product(matrix, column);
                product.set_fibre_unsafe(n, index, columnProd);

                index[start]++;

                int k = start;
                while (index[k] == dimensions[k])
                {
                    index[k] = 0;
                    k -= (k == n + 1) ? 2 : 1;
                    if (k < 0) return product;
                    index[k]++;
                }
            }
        }

        /// <summary>
        /// Calculate and return the product between each fibre along dimension n of the tensor, with a vector 
        /// The result will be a tensor of order 1 less than the original tensor
        /// </summary>
        /// <param name="n"></param>
        /// <param name="Dot"></param>
        /// <returns></returns>
        internal DenseTensor<T> NModeProduct(int n, DenseVector<T> vector, Func<T[], T[], T> Dot)
        {
            if (n < 0 || n >= _order)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (vector == null)
            {
                throw new ArgumentNullException();
            }

            // Special case if order is 2
            if (_order == 2)
            {
                int rows = _subtensors.Length, columns = _subtensors[0]._values.Length;
                if (n == 0)
                {
                    if (rows != vector.Dimension)
                    {
                        throw new InvalidOperationException();
                    }

                    T[] prod = new T[columns], col = new T[rows];
                    for (int i = 0; i < columns; ++i)
                    {
                        for (int j = 0; j < rows; ++j)
                        {
                            col[j] = _subtensors[j]._values[i];
                        }
                        prod[i] = Dot(col, vector.Values);
                    }
                    return new DenseTensor<T>(prod);
                }
                else if (n == 1)
                {
                    if (columns != vector.Dimension)
                    {
                        throw new InvalidOperationException();
                    }

                    T[] prod = new T[rows];
                    for (int i = 0; i < rows; ++i)
                    {
                        prod[i] = Dot(_subtensors[i]._values, vector.Values);
                    }
                    return new DenseTensor<T>(prod);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }

            List<int> dimensions = GetDimensions();
            if (dimensions[n] != vector.Dimension)
            {
                throw new InvalidOperationException();
            }

            // Naive implementation - unoptimised 
            int[] index = new int[_order], newIndex = new int[_order - 1], newDimensions = new int[_order - 1];
            for (int i = 0, j = 0; i < _order; ++i)
            {
                if (i == n) continue;
                newDimensions[j++] = dimensions[i];
            }

            DenseTensor<T> product = new DenseTensor<T>(newDimensions);
            T[] column = new T[dimensions[n]], values = vector.Values;

            int last = _order - 1, start = (n == last) ? last - 1 : last;
            while (true)
            {
                get_fibre_unsafe(n, index, column);
                T dot = Dot(values, column);

                // Calculate product index
                for (int i = 0, j = 0; i < _order; ++i)
                {
                    if (i == n) continue;
                    newIndex[j++] = index[i];
                }
                product.set_unsafe(newIndex, dot);

                // Increment then regularise index
                index[start]++;

                int k = start;
                while (index[k] >= dimensions[k])
                {
                    index[k] = 0;
                    k -= (k == n + 1) ? 2 : 1;
                    if (k < 0) return product;
                    index[k]++;
                }
            }
        }

        /// <summary>
        /// Calcualtes and returns the outer product between (this) x vector
        /// The resultant tensor will be of order equal to sum of the orders of the tensors.
        /// 
        /// This contrasts with the TensorProduct method which returns a tensor with the 
        /// same order as the constituent tensors.
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        internal DenseTensor<T> OuterProduct(DenseVector<T> vector, IBLAS<T> blas)
        {
            if (_order == 0)
            {
                T[] vect = vector.Values.Copy();
                blas.SCAL(vect, _values[0], 0, vector.Dimension);
                return new DenseTensor<T>(vect);
            }

            else if (_order == 1)
            {
                T[] v = vector.Values;
                int m = _values.Length, n = vector.Dimension, i;

                DenseTensor<T>[] subtensors = new DenseTensor<T>[m];
                for (i = 0; i < m; ++i)
                {
                    T[] cpy = v.Copy();
                    blas.SCAL(cpy, _values[i], 0, n);
                    subtensors[i] = new DenseTensor<T>(cpy);
                }
                return new DenseTensor<T>(subtensors);
            }

            else
            {
                int m = _subtensors.Length, i;
                DenseTensor<T>[] subtensors = new DenseTensor<T>[m];
                for (i = 0; i < m; ++i)
                {
                    subtensors[i] = _subtensors[i].OuterProduct(vector, blas);
                }
                return new DenseTensor<T>(subtensors);
            }
        }

        /// <summary>
        /// Calculate the sum of all elements of the tensor, across all dimensions, while fixing column n
        /// This method will return a dense vector of dimension dim[n]
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        internal DenseVector<T> SumOverAllExcept(int n, T zero, Func<T, T, T> Add)
        {
            if (_order < 2)
            {
                throw new InvalidOperationException();
            }
            if (n < 0 || n >= _order)
            {
                throw new ArgumentOutOfRangeException();
            }
            
            List<int> dimensions = GetDimensions();

            int dim = dimensions[n];
            T[] sums = new T[dim];
            for (int i = 0; i < dim; ++i)
            {
                sums[i] = zero;
            }

            DenseTensorIterator<T> iterator = new DenseTensorIterator<T>(this);
            while (iterator.HasNext)
            {
                int[] index = iterator.Index;
                int index_n = index[n];
                T e = iterator.Next();
                sums[index_n] = Add(sums[index_n], e);
            }
            return new DenseVector<T>(sums);
        }
        internal DenseMatrix<T> SumOverAllExcept(int m, int n, T zero, Func<T, T, T> Add)
        {
            if (_order < 3)
            {
                throw new InvalidOperationException();
            }
            if (n < 0 || n >= _order || m < 0 || m >= _order)
            {
                throw new ArgumentOutOfRangeException();
            }
            
            List<int> dimensions = GetDimensions();

            int rows = dimensions[m], columns = dimensions[n];
            DenseMatrix<T> sums = DenseMatrix.Zeroes<T>(rows, columns);
            T[][] values = sums.Values;

            DenseTensorIterator<T> iterator = new DenseTensorIterator<T>(this);
            while (iterator.HasNext)
            {
                int[] index = iterator.Index;
                int index_n = index[n], index_m = index[m];
                T e = iterator.Next();
                values[index_m][index_n] = Add(values[index_m][index_n], e);
            }
            return sums;
        }

        /// <summary>
        /// Sum over the dimension n, leaving a Order - 1 tensor 
        /// </summary>
        /// <param name="n"></param>
        /// <param name="Add"></param>
        /// <returns></returns>
        internal DenseTensor<T> SumOver(int n, Func<T, T, T> Add)
        {
            if (_order < 1)
            {
                throw new InvalidOperationException();
            }
            if (n < 0 || n >= _order)
            {
                throw new ArgumentOutOfRangeException();
            }

            List<int> dimensions = GetDimensions();

            int[] newDimensions = new int[_order - 1], newIndex = new int[_order - 1];
            for (int i = 0, j = 0; i < _order; ++i, ++j)
            {
                if (i == n) i++;
                if (i < _order)
                {
                    newDimensions[j] = dimensions[i];
                }
            }
            DenseTensor<T> tensor = new DenseTensor<T>(newDimensions);

            DenseTensorIterator<T> iterator = new DenseTensorIterator<T>(this);
            while (iterator.HasNext)
            {
                int[] index = iterator.Index;

                // Copy over to the new index
                for (int i = 0, j = 0; i < _order; ++i, ++j)
                {
                    if (i == n) i++;
                    if (i < _order)
                    {
                        newIndex[j] = index[i];
                    }
                }

                // Set as the new value
                T e = iterator.Next();
                tensor.transform_unsafe(newIndex, x => Add(x, e));
            }
            return tensor;
        }

        internal DenseTensor<T> Multiply(T scalar, IBLAS<T> blas)
        {
            if (_order == 0)
            {
                return new DenseTensor<T>(blas.Multiply(_values[0], scalar));
            }
            if (_order == 1)
            {
                T[] row = _values.Copy();
                blas.SCAL(row, scalar, 0, row.Length);
                return new DenseTensor<T>(row);
            }

            // Otherwise
            DenseTensor<T>[] product = new DenseTensor<T>[_subtensors.Length];
            for (int i = 0; i < product.Length; ++i)
            {
                product[i] = _subtensors[i].Multiply(scalar, blas);
            }
            return new DenseTensor<T>(product);
        }
        #region Public methods 

        public override T Get(params int[] index)
        {
            if (index == null)
            {
                throw new ArgumentNullException();
            }
            if (index.Length < _order)
            {
                throw new ArgumentOutOfRangeException();
            }
            return get_unsafe(index);
        }
        public override void Set(T value, params int[] index)
        {
            if (index == null)
            {
                throw new ArgumentNullException();
            }
            if (index.Length < _order)
            {
                throw new ArgumentOutOfRangeException();
            }

            set_unsafe(index, value);
        }
        public override List<int> GetDimensions()
        {
            List<int> dimensions = new List<int>();
            get_dimensions(dimensions);
            return dimensions;
        }

        /// <summary>
        /// Returns true if this subtensor's dimensions equals that of the other subtensor
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool DimensionsEquals(DenseTensor<T> other)
        {
            if (other == null || _order != other.Order) return false;
            if (_order == 0) return true;
            if (_order == 1)
            {
                return _values.Length == other._values.Length;
            }

            // Higher order 
            int dim = _subtensors.Length, i;
            if (dim != other._subtensors.Length) return false;
            for (i = 0; i < dim; ++i)
            {
                if (!_subtensors[i].DimensionsEquals(other._subtensors[i])) return false;
            }
            return true;
        }
        public bool DimensionsEquals(SparseTensor<T> other)
        {
            if (other == null)
            {
                return false;
            }
            return other.DimensionsEquals(this);
        }

        public SparseTensor<T> ToSparse()
        {
            Dictionary<long, T> values = new Dictionary<long, T>();
            long index = 0;
            to_indexed_unsafe(values, ref index);

            List<int> list = GetDimensions();
            int[] dimensions = new int[list.Count];
            for (int i = 0; i < dimensions.Length; ++i)
            {
                dimensions[i] = list[i];
            }
            return new SparseTensor<T>(dimensions, values);
        }
        /// <summary>
        /// Calculates and returns the direct sum of (this) and another tensor S, of the same order.
        /// The dimension of the sum will be equal to the sum of the dimensions of (this) and S.
        /// The original tensors are unchanged; a new dense tensor is created by this method.
        /// </summary>
        /// <param name="S">A tensor of the same order as (this).</param>
        /// <returns>The direct sum of (this) and tensor S.</returns>
        public DenseTensor<T> DirectSum(DenseTensor<T> S)
        {
            if (S == null)
            {
                throw new ArgumentNullException("Tensor is null");
            }

            if (_order != S._order)
            {
                throw new InvalidOperationException("Order of tensors do not match.");
            }

            if (_order == 0)
            {
                throw new ArgumentOutOfRangeException("Direct sums do not currently support 0-th order tensors.");
            }

            if (_order == 1)
            {
                T[] sum = new T[_values.Length + S._values.Length];
                for (int i = 0; i < _values.Length; ++i)
                {
                    sum[i] = _values[i];
                }
                for (int i = 0; i < S._values.Length; ++i)
                {
                    sum[i + _values.Length] = S._values[i];
                }
                return new DenseTensor<T>(sum);
            }

            DenseTensor<T> direct = S.shift_unsafe(0, GetDimensions().ToArray());
            direct.overwrite_unsafe(this);
            return direct;
        }

        /// <summary>
        /// Calculate the $n$-mode unfolding/matricization of a tensor $\mathcal{T}\in\mathbb{F}^{d_1 \times ... \times d_m}$, 
        /// returning a matrix of dimension $d_n$ by $\prod_{i\not=n}{d_i}$.
        /// </summary>
        /// <name>Flatten</name>
        /// <proto>IMatrix<T> ITensor<T>.Flatten(int n)</proto>
        /// <cat>la</cat>
        /// <param name="n"></param>
        /// <returns></returns>
        public DenseMatrix<T> Flatten(int n)
        {
            if (n < 0 || n >= Order)
            {
                throw new ArgumentOutOfRangeException();
            }

            List<int> dims = GetDimensions();
            int rows = dims[n], columns = 1, i, j, k;
            for (i = 0; i < dims.Count; ++i)
            {
                if (i != n)
                {
                    columns *= dims[i];
                }
            }

            DenseMatrix<T> matrix = new DenseMatrix<T>(rows, columns);
            int[] index = new int[Order];
            for (i = 0; i < rows; ++i)
            {
                index[n] = i;

                T[] row = matrix[i];
                for (j = 0; j < columns; ++j)
                {
                    row[j] = Get(index);

                    k = n == 0 ? 1 : 0;
                    index[k]++;
                    while (index[k] == dims[k])
                    {
                        index[k] = 0;
                        k += (k == n - 1) ? 2 : 1;
                        if (k >= _order) break;
                        index[k]++;
                    }
                }
            }
            return matrix;
        }

        /// <summary>
        /// <para>Convert a tensor to a vector, prioritising the first dimension.</para>
        /// <para>Returns a vector of type <txt>DenseVector&lt;T&gt;</txt> if the tensor is of type <txt>DenseTensor&lt;T&gt;</txt>, and 
        /// <txt>SparseVector&lt;T&gt;</txt> if the tensor is of type <txt>SparseTensor&lt;T&gt;</txt>.</para>
        /// </summary>
        /// <name>VectorizeTensor</name>
        /// <proto>IVector<T> ITensor<T>.Vectorize()</proto>
        /// <cat>la</cat>
        /// <returns></returns>
        public DenseVector<T> Vectorize()
        {
            int len = 1;
            List<int> dimensions = GetDimensions();
            foreach (int dim in dimensions)
            {
                len *= dim;
            }

            T[] vector = new T[len];
            int[] index = new int[_order];
            for (int i = 0; i < len; ++i)
            {
                int j = 0;
                while (index[j] == dimensions[j])
                {
                    index[j] = 0;
                    j++;
                    index[j]++;
                }

                vector[i] = get_unsafe(index);

                index[0]++;
            }
            return new DenseVector<T>(vector);
        }


        #endregion

        #region Explicit casting operators 

        public static explicit operator DenseTensor<T>(T value) => new DenseTensor<T>(value);

        public static explicit operator DenseTensor<T>(T[] vector) => new DenseTensor<T>(vector);
        public static explicit operator DenseTensor<T>(DenseVector<T> vector) => new DenseTensor<T>(vector);

        public static explicit operator DenseTensor<T>(T[][] matrix) => new DenseTensor<T>(matrix);
        public static explicit operator DenseTensor<T>(T[,] matrix) => new DenseTensor<T>(matrix);
        public static explicit operator DenseTensor<T>(DenseMatrix<T> matrix) => new DenseTensor<T>(matrix);

        public static explicit operator DenseTensor<T>(T[][][] tensor) => new DenseTensor<T>(tensor);
        public static explicit operator DenseTensor<T>(T[,,] tensor) => new DenseTensor<T>(tensor);

        public static explicit operator DenseTensor<T>(T[][][][] tensor) => new DenseTensor<T>(tensor);
        public static explicit operator DenseTensor<T>(T[,,,] tensor) => new DenseTensor<T>(tensor);

        #endregion
    }
    public static class DenseTensor
    {
        internal static DenseTensor<T> Random<T>(Func<T> Sample, params int[] dimensions) where T : new()
        {
            if (dimensions == null) throw new ArgumentNullException();

            if (dimensions.Length == 0)
            {
                return new DenseTensor<T>(Sample());
            }
            else if (dimensions.Length == 1)
            {
                return new DenseTensor<T>(RectangularVector.Random(dimensions[0], Sample));
            }

            int[] subDimensions = new int[dimensions.Length - 1];
            for (int d = 1; d < dimensions.Length; ++d)
            {
                subDimensions[d - 1] = dimensions[d];
            }

            DenseTensor<T>[] subtensors = new DenseTensor<T>[dimensions[0]];
            for (int d = 0; d < subtensors.Length; ++d)
            {
                subtensors[d] = Random(Sample, subDimensions);
            }
            return new DenseTensor<T>(subtensors);
        }
        public static DenseTensor<T> Random<T>(params int[] dimensions) where T : new()
        {
            return Random(DefaultGenerators.GetRandomGenerator<T>(), dimensions);
        }

        /// <summary>
        /// Creates a tensor of dimensionality <txt>dimensions</txt> where each element is equal to <txt>value</txt>.
        /// </summary>
        /// <name>RepeatTensor</name>
        /// <proto>ITensor<T> ITensor<T>.Repeat<T>(T value, params int[] dimensions)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="dimensions"></param>
        /// <returns></returns>
        public static DenseTensor<T> Repeat<T>(T value, params int[] dimensions) where T : new()
        {
            if (dimensions == null)
            {
                throw new ArgumentNullException();
            }
            if (dimensions.Length == 0)
            {
                return new DenseTensor<T>(value);
            }
            else if (dimensions.Length == 1)
            {
                return new DenseTensor<T>(RectangularVector.Repeat(value, dimensions[0]));
            }

            int[] subDimensions = new int[dimensions.Length - 1];
            for (int d = 1; d < dimensions.Length; ++d)
            {
                subDimensions[d - 1] = dimensions[d];
            }

            DenseTensor<T>[] subtensors = new DenseTensor<T>[dimensions[0]];
            for (int d = 0; d < subtensors.Length; ++d)
            {
                subtensors[d] = Repeat(value, subDimensions);
            }
            return new DenseTensor<T>(subtensors);
        }
        public static DenseTensor<T> Ones<T>(params int[] dimensions) where T : new()
        {
            return Repeat(DefaultGenerators.GetOne<T>(), dimensions);
        }

        /// <summary>
        /// Create a tensor of dimension <txt>dimensions</txt>, where each element is zero.
        /// <para>Supported for <txt>DenseTensor&lt;T&gt;</txt> and <txt>SparseTensor&lt;T&gt;</txt>.</para>
        /// <para>Supported for data types <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and all types 
        /// <txt>T</txt> implementing <txt>AdditiveGroup&lt;T&gt;</txt>.</para>
        /// </summary>
        /// <name>ZeroesTensor</name>
        /// <proto>ITensor<T> ITensor<T>.Zeroes<T>(params int[] dimensions)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="dimensions"></param>
        /// <returns></returns>
        public static DenseTensor<T> Zeroes<T>(params int[] dimensions) where T : new()
        {
            return Repeat(DefaultGenerators.GetZero<T>(), dimensions);
        }
    }
}
