﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Tensors
{
    /// <summary>
    /// The implementation of tensors are as follows:
    /// 
    /// - For tensors with order > 4, sparse tensors are used
    /// - For tensors with order <= 4 (including 0), dense jagged matrices are used
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ITensor<T>
    {
        /// <summary>
        /// The number of dimenisons of the tensor (note this may differ from the rank)
        /// </summary>
        public abstract int Order { get; }

        /// <summary>
        /// Gets the value of the tensor at the specified index. 
        /// The index array length must be >= the tensor's order
        /// If the array length > the tensor's order, the first 
        /// 'order' indices are used.
        /// </summary>
        /// <param name="index"></param>
        /// <returns>The value of the tensor at the index.</returns>
        public abstract T Get(params int[] index);


        /// <summary>
        /// Sets the value of the tensor at the specified index.
        /// The index array must be >= the tensor's order
        /// If the array length > the tensor's order, the first 'order'
        /// indices are used.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="index">The index within the tensor to set the value</param>
        public abstract void Set(T value, params int[] index);

        /// <summary>
        /// Returns a list of dimensions of the tensor
        /// The number of elements of this list will be equal to the order of the tensor.
        /// </summary>
        /// <returns>A list of the dimensionality of the tensor</returns>
        public abstract List<int> GetDimensions();

        public void Print(Func<T, string> Format)
        {
            if (Order == 0)
            {
                Debug.WriteLine(Format(Get()));
            }
            else if (Order == 1)
            {
                int dimension = GetDimensions()[0];
                var s = new StringBuilder();
                for (int i = 0; i < dimension; ++i)
                {
                    s.Append(Format(Get(i)) + "\t");
                }
                Debug.WriteLine(s.ToString());
            }
            else if (Order == 2)
            {
                StringBuilder s = new StringBuilder();

                List<int> dimensions = GetDimensions();
                int rows = dimensions[0], columns = dimensions[1], i, j;
                for (i = 0; i < rows; ++i)
                {
                    for (j = 0; j < columns; ++j)
                    {
                        s.Append(Format(Get(i, j)) + "\t");
                    }
                    s.AppendLine();
                }
                Debug.WriteLine(s.ToString());
            }
            else if (Order == 3)
            {
                StringBuilder s = new StringBuilder();
                List<int> dimensions = GetDimensions();

                int m = dimensions[0], rows = dimensions[1], columns = dimensions[2], i, j, k;
                for (i = 0; i < m; ++i)
                {
                    for (j = 0; j < rows; ++j)
                    {
                        for (k = 0; k < columns; ++k)
                        {
                            s.Append(Format(Get(i, j, k)) + "\t");
                        }
                        s.AppendLine();
                    }
                    s.AppendLine();
                }
                Debug.WriteLine(s.ToString());
            }
            else
            {
                Debug.WriteLine($"Dense tensor [{string.Join(", ", GetDimensions())}]");
            }
        }
        public void PrintDimensions()
        {
            Debug.WriteLine($"[{string.Join(", ", GetDimensions())}]");
        }

    }
}
