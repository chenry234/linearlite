﻿using LinearLite.Helpers;
using LinearLite.Structs.Vectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Tensors
{
    public class SparseTensor<T> : ITensor<T> where T : new()
    {
        private readonly T _default;
        private readonly int _order;
        private readonly List<int> _dimensions;
        private readonly Dictionary<long, T> _values;

        internal Dictionary<long, T> Values { get { return _values; } }
        public override int Order => _order;

        #region Constructors 

        /// <summary>
        /// Constructor for a sparse tensor by copying another tensor
        /// </summary>
        /// <param name="tensor"></param>
        public SparseTensor(SparseTensor<T> tensor)
        {
            if (tensor == null)
            {
                throw new ArgumentNullException();
            }

            _default = new T();
            _order = tensor.Order;

            // Copy over the dimensions list
            _dimensions = new List<int>();
            foreach (int dim in tensor._dimensions)
            {
                _dimensions.Add(dim);
            }

            // Copy over the values dictionary
            _values = InitializeDictionary(_dimensions);
            foreach (KeyValuePair<long, T> pair in _values)
            {
                _values[pair.Key] = pair.Value;
            }
        }
        public SparseTensor(params int[] dimensions)
        {
            if (dimensions == null)
            {
                throw new ArgumentNullException();
            }

            _dimensions = new List<int>();
            _dimensions.AddRange(dimensions);

            _order = dimensions.Length;
            _default = new T();

            _values = InitializeDictionary(dimensions);
        }
        internal SparseTensor(int[] dimensions, Dictionary<long, T> values) : this(dimensions)
        {
            _values = values;
        }

        private Dictionary<long, T> InitializeDictionary(IEnumerable<int> dimensions)
        {
            long size = TryCalculateSize(dimensions);
            
            if (size > int.MaxValue)
            {
                // Give up, accept the crush in performance
                return new Dictionary<long, T>();
            }
            else
            {
                // Maximise performance by setting a key size
                return new Dictionary<long, T>((int)size);
            }
        }
        private long TryCalculateSize(IEnumerable<int> dimensions)
        {
            long size = 1L;
            foreach (int dim in dimensions)
            {
                size = checked(size * dim);
            }
            return size;
        }

        /// <summary>
        /// Create an order 0 tensor from a scalar value of type T
        /// </summary>
        /// <param name="value"></param>
        public SparseTensor(T value) : this()
        {
            _values[0] = value;
        }

        /// <summary>
        /// Create a order 1 tensor from a dense vector of values
        /// </summary>
        /// <param name="values"></param>
        public SparseTensor(T[] values) : this(values.Length)
        {
            int len = values.Length, i;
            for (i = 0; i < len; ++i)
            {
                _values[i] = values[i];
            }
        }
        public SparseTensor(DenseVector<T> vector) : this(vector.Values) { }

        /// <summary>
        /// Create a order 2 sparse tensor from a dense 2D rectangular array of values
        /// </summary>
        /// <param name="values"></param>
        public SparseTensor(T[,] values) : this(values.GetLength(0), values.GetLength(1))
        {
            int rows = values.GetLength(0), cols = values.GetLength(1), i, j, index = 0;
            for (i = 0; i < rows; ++i)
            {
                for (j = 0; j < cols; ++j)
                {
                    _values[index++] = values[i, j];
                }
            }
        }
        /// <summary>
        /// Create an order 2 sparse tensor from a dense 2D jagged array of values
        /// </summary>
        /// <param name="values"></param>
        public SparseTensor(T[][] values) : this(values, GetBoundingDimensions(values)) { }
        public SparseTensor(T[][] values, int[] dimensions) : this(dimensions)
        {
            if (values == null)
            {
                throw new ArgumentNullException("Matrix is null or empty.");
            }
            if (dimensions.Length != 2)
            {
                throw new ArgumentException("Order must be 2.");
            }

            int rows = values.Length, i, j, index = 0, outerIndex = 0;
            for (i = 0; i < rows; ++i)
            {
                T[] row = values[i];
                int cols = row.Length;
                for (j = 0; j < cols; ++j)
                {
                    _values[index++] = row[j];
                }

                outerIndex += dimensions[0];
                index = outerIndex;
            }
        }
        public SparseTensor(DenseMatrix<T> matrix) : this(matrix.Values) { }

        /// <summary>
        /// Create an order 3 tensor from a 3D rectangular array
        /// </summary>
        /// <param name="values"></param>
        public SparseTensor(T[,,] values) : this(values.GetLength(0), values.GetLength(1), values.GetLength(2))
        {
            if (values == null)
            {
                throw new ArgumentNullException("Tensor is null or empty.");
            }

            int m = values.GetLength(0), n = values.GetLength(1), p = values.GetLength(2), i, j, k, index = 0;
            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    for (k = 0; k < p; ++k)
                    {
                        _values[index++] = values[i, j, k];
                    }
                }
            }
        }
        public SparseTensor(T[][][] values) : this(values, GetBoundingDimensions(values)) { }
        public SparseTensor(T[][][] S, int[] dimensions)
        {
            if (S == null)
            {
                throw new ArgumentNullException("Tensor is null or empty.");
            }

            int m = S.Length, i, j, k, index = 0, boundingIndex = 0, d2 = dimensions[1], d3 = dimensions[2];
            for (i = 0; i < m; ++i)
            {
                T[][] S_i = S[i];
                int n = S_i.Length;
                for (j = 0; j < n; ++j)
                {
                    T[] S_ij = S_i[j];
                    int p = S_ij.Length;
                    for (k = 0; k < p; ++k)
                    {
                        _values[index++] = S_ij[k];
                    }

                    boundingIndex += d3;
                    index = boundingIndex;
                }
                boundingIndex += d2;
                index = boundingIndex;
            }
        }

        /// <summary>
        /// Create a rank 4 tensor from a 4D rectangular array
        /// </summary>
        /// <param name="S"></param>
        public SparseTensor(T[,,,] S) : this(S.GetLength(0), S.GetLength(1), S.GetLength(2), S.GetLength(3))
        {
            if (S == null) throw new ArgumentNullException("Tensor is null or empty.");

            int m = S.GetLength(0), n = S.GetLength(1), p = S.GetLength(2), q = S.GetLength(3), i, j, k, l, index = 0;

            for (i = 0; i < m; ++i)
            {
                for (j = 0; j < n; ++j)
                {
                    for (k = 0; k < p; ++k)
                    {
                        for (l = 0; l < q; ++l)
                        {
                            _values[index++] = S[i, j, k, l];
                        }
                    }
                }
            }
        }
        public SparseTensor(T[][][][] S) : this(S, GetBoundingDimensions(S)) { }
        private SparseTensor(T[][][][] S, int[] dimensions) : this(dimensions)
        {
            if (S == null) throw new ArgumentNullException("Tensor is null or empty.");

            int m = S.Length, i, j, k, l, index = 0, boundingIndex = 0, d2 = dimensions[1], d3 = dimensions[2], d4 = dimensions[3];
            for (i = 0; i < m; ++i)
            {
                T[][][] S_i = S[i];
                int n = S_i.Length;
                for (j = 0; j < n; ++j)
                {
                    T[][] S_ij = S_i[j];
                    int p = S_ij.Length;
                    for (k = 0; k < p; ++k)
                    {
                        T[] S_ijk = S_ij[k];
                        int q = S_ijk.Length;
                        for (l = 0; l < q; ++l)
                        {
                            _values[index++] = S_ijk[l];
                        }
                        boundingIndex += q;
                        index = boundingIndex;
                    }
                    boundingIndex += p;
                    index = boundingIndex;
                }
                boundingIndex += n;
                index = boundingIndex;
            }
        }

        public SparseTensor(DenseTensor<T> tensor) : this(tensor.ToSparse()) { }

        #endregion

        #region Private methods

        /// <summary>
        /// Get the maximum (bounding) dimensionality along a particular dimension.
        /// Similar to GetLength(int i)
        /// </summary>
        /// <param name="A"></param>
        /// <param name="dimension"></param>
        /// <returns></returns>
        private static int[] GetBoundingDimensions(T[][] A)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            int max = A[0].Length, i;
            for (i = 0; i < max; ++i)
            {
                int len = A[i].Length;
                if (len > max) max = len;
            }
            return new int[] { A.Length, max };
        }
        private static int[] GetBoundingDimensions(T[][][] A)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            int max1 = 0, max2 = 0, m = A.Length, i, j;
            for (i = 0; i < m; ++i)
            {
                T[][] A_i = A[i];
                int n = A_i.Length;
                if (n > max1) max1 = n;

                for (j = 0; j < n; ++j)
                {
                    if (A_i[j].Length > max2) max2 = A_i[j].Length;
                }
            }
            return new int[] { A.Length, max1, max2 };
        }
        private static int[] GetBoundingDimensions(T[][][][] A)
        {
            if (A == null)
            {
                throw new ArgumentNullException();
            }

            int max1 = 0, max2 = 0, max3 = 0, i, j, k, m = A.Length;
            for (i = 0; i < m; ++i)
            {
                T[][][] A_i = A[i];
                int n = A_i.Length;
                if (n > max1) max1 = n;

                for (j = 0; j < n; ++j)
                {
                    T[][] A_ij = A_i[j];
                    int p = A_ij.Length;
                    if (p > max2) max2 = p;

                    for (k = 0; k < p; ++k)
                    {
                        if (A_ij[k].Length > max3)
                        {
                            max3 = A_ij[k].Length;
                        }
                    }
                }
            }
            return new int[] { A.Length, max1, max2, max3 };
        }

        private void check_index_valid(int[] index)
        {
            if (index == null)
            {
                throw new ArgumentNullException();
            }

            int len = index.Length;
            if (len < _order)
            {
                throw new ArgumentOutOfRangeException();
            }

            for (int i = len - _order; i < len; ++i)
            {
                int ix = index[i];
                if (ix < 0 || ix >= _dimensions[i])
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Get the key using the index array
        /// </summary>
        /// <param name="index"></param>
        /// <param name="big_endian">
        /// If true, the [0] index will have largest weight. If false, the last 
        /// index will have largest weight.
        /// </param>
        /// <returns></returns>
        private long get_key(int[] index, bool big_endian = true)
        {
            if (_order == 0)
            {
                return 0L;
            }

            int len = index.Length;
            long ix = 0L;

            if (big_endian)
            {
                for (int i = len - _order, j = 0; i < len; ++i, ++j)
                {
                    ix *= _dimensions[j];
                    ix += index[i];
                }
            }
            else
            {
                int last = _order - len;
                for (int i = len - 1, j = _order - 1; i >= last; --i, --j)
                {
                    ix *= _dimensions[j];
                    ix += index[i];
                }
            }

            return ix;
        }
        /// <summary>
        /// Given a valid key, fills in the index array
        /// The index array must be at least the order of the tensor.
        /// If the index array is longer than the tensor's order, then the 
        /// indices will be filled starting at the last element of the array.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="index"></param>
        internal void get_index(long key, int[] index, bool big_endian = true)
        {
            if (index.Length < _order)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (big_endian)
            {
                int i, j;
                for (i = index.Length - 1, j = _order - 1; j >= 0; --i, --j)
                {
                    int dim = _dimensions[j];
                    long ix = key % dim;
                    index[i] = (int)ix;
                    key = (key - ix) / dim;
                }
            }
            else
            {
                int len = index.Length;
                for (int i = len - _order - 1, j = 0; i < len; ++i, ++j)
                {
                    int dim = _dimensions[j];
                    long ix = key % dim;
                    index[i] = (int)ix;
                    key = (key - ix) / dim;
                }
            }
        }

        #endregion

        #region Internal methods 

        internal bool Equals(SparseTensor<T> tensor, Func<T, T, bool> Equals)
        {
            if (!DimensionsEquals(tensor))
            {
                return false;
            }

            Dictionary<long, T> vals = tensor.Values;

            // Dual looping is less efficient but conserves memory
            foreach (KeyValuePair<long, T> pair in _values)
            {
                long k = pair.Key;
                if (!vals.ContainsKey(k) || !Equals(pair.Value, vals[k])) return false;
            }

            // No need to check for equality, as it should already be handled 
            // in the previous case.
            foreach (long key in vals.Keys)
            {
                if (!_values.ContainsKey(key)) return false;
            }
            return true;
        }
        internal bool Equals(DenseTensor<T> tensor, Func<T, T, bool> Equals)
        {
            if (tensor == null) return false;
            return tensor.Equals(this, Equals);
        }

        /// <summary>
        /// Calculates and returns the tensor product of (this) (x) tensor.
        /// The order of the tensor product is equal to the order of the tensors.
        /// The dimension of the tensor product is equal to the product of the dimensions 
        /// of the original tensors.
        /// 
        /// The orders of the two tensors being multiplied must be equal.
        /// </summary>
        /// <param name="tensor"></param>
        /// <returns></returns>
        internal SparseTensor<T> TensorProduct(SparseTensor<T> tensor, Func<T, T, T> Multiply)
        {
            if (tensor == null)
            {
                throw new ArgumentNullException();
            }

            if (tensor._order != _order)
            {
                throw new InvalidOperationException();
            }

            int[] dimensions = new int[_order];
            for (int i = 0; i < _order; ++i)
            {
                dimensions[i] = checked(_dimensions[i] * tensor._dimensions[i]);
            }
            SparseTensor<T> product = new SparseTensor<T>(dimensions);
            Dictionary<long, T> dict = product.Values;

            //Debug.WriteLine("Tensor 1 dimensions: " + string.Join(",", _dimensions));
            //Debug.WriteLine("Tensor 2 dimensions: " + string.Join(",", tensor._dimensions));

            int[] index1 = new int[_order], index2 = new int[_order], indexProd = new int[_order];
            foreach (KeyValuePair<long, T> p1 in _values)
            {
                /*
                if (p1.Key == 6L)
                {
                    Debug.WriteLine("This is from the 1st tensor");
                }*/
                get_index(p1.Key, index1, true);
                T x = p1.Value;

                foreach (KeyValuePair<long, T> p2 in tensor._values)
                {
                    /*
                    if (p2.Key == 6L)
                    {
                        Debug.WriteLine("This is from the 2nd tensor");
                    }*/
                    tensor.get_index(p2.Key, index2, true);

                    for (int i = 0; i < _order; ++i)
                    {
                        indexProd[i] = index2[i] + tensor._dimensions[i] * index1[i];
                    }

                    //Debug.WriteLine(p1.Key + "\t" + p2.Key + "\t" + string.Join(",", index1) + "\t" + string.Join(",", index2) + "\t=\t" + string.Join(",", indexProd) + "\t" + x + "\t" + p2.Value + "\t=\t" + Multiply(x, p2.Value));
                    dict[product.get_key(indexProd)] = Multiply(x, p2.Value);
                }
            }
            return product;
        }
        internal SparseTensor<T> DirectSum(SparseTensor<T> tensor)
        {
            if (tensor == null)
            {
                throw new ArgumentNullException();
            }
            if (tensor._order != _order)
            {
                throw new InvalidOperationException();
            }

            int[] dimensions = new int[_order];
            for (int i = 0; i < _order; ++i)
            {
                dimensions[i] = checked(_dimensions[i] + tensor._dimensions[i]);
            }

            SparseTensor<T> sum = new SparseTensor<T>(dimensions);

            int[] index = new int[_order];
            foreach (KeyValuePair<long, T> pair in _values)
            {
                get_index(pair.Key, index, true);
                sum.Set(pair.Value, index);
            }
            foreach (KeyValuePair<long, T> pair in tensor._values)
            {
                get_index(pair.Key, index, true);
                for (int i = 0; i < _order; ++i) index[i] += _dimensions[i];
                sum.Set(pair.Value, index);
            }
            return sum;
        }

        #endregion

        #region Public methods 

        public bool DimensionsEquals(SparseTensor<T> tensor)
        {
            if (tensor == null || tensor._order != _order)
            {
                return false;
            }

            List<int> dimensions = tensor._dimensions;
            for (int i = 0; i < _order; ++i)
            {
                if (_dimensions[i] != dimensions[i])
                {
                    return false;
                }
            }
            return true;
        }
        public bool DimensionsEquals(DenseTensor<T> tensor)
        {
            if (tensor == null || tensor.Order != _order)
            {
                return false;
            }

            List<int> dimensions = tensor.GetDimensions();
            for (int i = 0; i < _order; ++i)
            {
                if (_dimensions[i] != dimensions[i])
                {
                    return false;
                }
            }
            return true;
        }
        public SparseVector<T> Vectorize()
        {
            // TODO: need to re-order... which one is correct??

            long len = 1L;
            foreach (int dim in _dimensions)
            {
                len *= dim;
            }

            Dictionary<long, T> dict = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> pair in _values)
            {
                int[] index = new int[_order];
                get_index(pair.Key, index, true);

                long key = get_key(index, false);
                dict[key] = pair.Value;
            }
            return new SparseVector<T>(len, dict);
        }

        #endregion

        #region Implicit casts

        public static explicit operator SparseTensor<T>(T scalar) => new SparseTensor<T>(scalar);
        public static explicit operator SparseTensor<T>(T[] vector) => new SparseTensor<T>(vector);
        public static explicit operator SparseTensor<T>(T[][] matrix) => new SparseTensor<T>(matrix);
        public static explicit operator SparseTensor<T>(T[,] matrix) => new SparseTensor<T>(matrix);
        public static explicit operator SparseTensor<T>(T[][][] tensor) => new SparseTensor<T>(tensor);
        public static explicit operator SparseTensor<T>(T[,,] tensor) => new SparseTensor<T>(tensor);
        public static explicit operator SparseTensor<T>(T[][][][] tensor) => new SparseTensor<T>(tensor);
        public static explicit operator SparseTensor<T>(T[,,,] tensor) => new SparseTensor<T>(tensor);

        public static explicit operator SparseTensor<T>(DenseVector<T> tensor) => new SparseTensor<T>(tensor);
        public static explicit operator SparseTensor<T>(DenseMatrix<T> matrix) => new SparseTensor<T>(matrix);
        public static explicit operator SparseTensor<T>(DenseTensor<T> tensor) => new SparseTensor<T>(tensor);

        #endregion

        #region Interface implementations

        public override List<int> GetDimensions()
        {
            return _dimensions;
        }
        public override T Get(params int[] index)
        {
            check_index_valid(index);

            long key = get_key(index, true);
            if (_values.ContainsKey(key))
            {
                return _values[key];
            }
            return _default;
        }
        public override void Set(T value, params int[] index)
        {
            check_index_valid(index);
            _values[get_key(index, true)] = value;
        }

        #endregion
    }
    public static class SparseTensor
    {
        public static SparseTensor<T> Random<T>(params int[] dimensions) where T : new()
        {
            // Generate sampler
            Func<T> sample = DefaultGenerators.GetRandomGenerator<T>();

            // Generate and return a Sparse tensor
            SparseTensor<T> tensor = new SparseTensor<T>(dimensions);

            // Calculate number of samples to take
            long totalSize = 1;
            foreach (int dim in dimensions)
            {
                // overflow check
                long size = totalSize * dim;
                if (size < totalSize)
                {
                    totalSize = long.MaxValue;
                    break;
                }
                totalSize = size;
            }

            // Fill sparsely up to 1e6 entries
            Random r = new Random();
            int entries = (int)Math.Min(totalSize, 1e6);
            int[] index = new int[dimensions.Length];
            for (int i = 0; i < entries; ++i)
            {
                // Sample a point inside the tensor
                for (int j = 0; j < dimensions.Length; ++j)
                {
                    index[j] = r.Next(dimensions[j]);
                }
                tensor.Set(sample(), index);
            }
            return tensor;
        }
    }
}
