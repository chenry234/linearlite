﻿using LinearLite.BLAS;
using LinearLite.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Vectors
{
    public class DenseVector<T> : IVector<T>
    {
        private T[] _values;

        /// <summary>
        /// Let the array handle out of range exceptions
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public override T this[int index]
        {
            get
            {
                return _values[index];
            }
            set 
            {
                _values[index] = value;
            } 
        }

        public T[] Values { get { return _values; } }

        public DenseVector(int length) : base(length)
        {
            _values = new T[length];
        }
        public DenseVector(T[] values) : base(values.Length)
        {
            if (values == null)
            {
                throw new ArgumentNullException();
            }

            _values = values;
        }

        /// <summary>
        /// Returns a deep copy of a vector.
        /// <para>
        /// </para>
        /// </summary>
        /// <name>CopyVector</name>
        /// <proto>IVector<T> IVector<T>.Copy()</proto>
        /// <cat>la</cat>
        /// <returns></returns>
        public DenseVector<T> Copy()
        {
            return new DenseVector<T>(_values.Copy());
        }

    }
    public static class DenseVector
    {
        /// <summary>
        /// Create a <txt>dim</txt>-dimensional vector where each entry is equal to <txt>value</txt>.
        /// <!--inputs-->
        /// <!--returns-->
        /// 
        /// <h4>Examples</h4>
        /// <pre><code class="cs">
        /// // Create a 10-dimensional vector
        /// DenseVector&lt;double&gt; ones = DenseVector.Repeat&lt;double&gt;(10, 1.0); 
        /// 
        /// // Create a sparse 5-dimensional vector with each entry equal to -3 + 2i
        /// SparseVector&lt;Complex&gt; vector = SparseVector.Repeat&lt;Complex&gt;(5, new Complex(-3, 2));
        /// 
        /// int[] intVector = RectangularVector.Repeat&lt;T&gt;(10, 1); 
        /// </code></pre>
        /// </summary>
        /// <name>RepeatVector</name>
        /// <proto>IVector<T> IVector<T>.Repeat(int dim, T value)</proto>
        /// <cat>la</cat>
        /// <param name="dim">The dimensionality of the created vector.</param>
        /// <param name="value">The value of each entry in the created vector.</param>
        /// <returns></returns>
        public static DenseVector<T> Repeat<T>(int dim, T value) where T : new()
        {
            if (dim < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            T[] vector = new T[dim];
            for (int i = 0; i < dim; ++i)
            {
                vector[i] = value;
            }
            return new DenseVector<T>(vector);
        }

        /// <summary>
        /// Create a <txt>dim</txt>-dimensional vector of zeroes.
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>ZeroesVector</name>
        /// <proto>IVector<T> IVector<T>.Zeroes(int dim)</proto>
        /// <cat>la</cat>
        /// <param name="dim">The dimensionality of the created vector.</param>
        /// <returns>A <txt>dim</txt>-dimensional vector of zeroes.</returns>
        public static DenseVector<T> Zeroes<T>(int dim) where T : new() => Repeat(dim, DefaultGenerators.GetZero<T>());
        public static DenseVector<T> Ones<T>(int dim) where T : new() => Repeat(dim, DefaultGenerators.GetOne<T>());

        public static DenseVector<T> Random<T>(int dim) => Random(dim, DefaultGenerators.GetRandomGenerator<T>());
        public static DenseVector<T> Random<T>(int dim, Func<T> random) 
        {
            T[] vect = new T[dim];
            for (int i = 0; i < dim; ++i)
            {
                vect[i] = random();
            }
            return new DenseVector<T>(vect);
        }
    }
}
