﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Vectors
{
    public abstract class IVector<T>
    {
        public int Dimension { get; private set; }
        public long LongDimension { get; private set; }
        
        public abstract T this[int index] { get; set; }

        protected IVector(int dimension)
        {
            Dimension = dimension;
            LongDimension = (long)dimension;
        }
        protected IVector(long dimension)
        {
            Dimension = (int)dimension;
            LongDimension = dimension;
        }

        internal void Print(Func<T, string> ToString)
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < Dimension; ++i)
            {
                s.Append(ToString(this[i]) + "\t");
            }
            Debug.WriteLine(s.ToString());
        }
    }
}
