﻿using LinearLite.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Structs.Vectors
{
    public class SparseVector<T> : IVector<T>
    {
        private Dictionary<long, T> _values;

        internal Dictionary<long, T> Values { get { return _values; } }

        public override T this[int index]
        {
            get
            {
                return _values.ContainsKey(index) ? _values[index] : default(T);
            }
            set
            {
                _values[index] = value;
            }
        }

        public SparseVector(long dim) : base(dim)
        {
            _values = new Dictionary<long, T>();
        }
        public SparseVector(T[] vector) : base(vector.Length)
        {
            if (vector == null)
            {
                throw new ArgumentNullException();
            }

            _values = new Dictionary<long, T>();

            long len = vector.Length;
            T def = default(T);
            for (long i = 0; i < len; ++i)
            {
                T value = vector[i];
                if (!value.Equals(def))
                {
                    _values[i] = value;
                }
            }
        }
        public SparseVector(long dim, Dictionary<long, T> values) : base(dim)
        {
            _values = values;
        }

        public SparseVector<T> Copy()
        {
            Dictionary<long, T> cpy = new Dictionary<long, T>();
            foreach (KeyValuePair<long, T> pair in _values)
            {
                cpy[pair.Key] = pair.Value;
            }
            return new SparseVector<T>(Dimension, cpy);
        }
    }
    public static class SparseVector
    {
        public static SparseVector<T> Random<T>(int dimension, int maxNonZeroElements)
        {
            Func<T> random = DefaultGenerators.GetRandomGenerator<T>();

            SparseVector<T> vector = new SparseVector<T>(dimension);
            Dictionary<long, T> values = new Dictionary<long, T>();
            Random rand = new Random();

            for (int i = 0; i < maxNonZeroElements; ++i)
            {
                int r = rand.Next(dimension);
                vector[r] = random();
            }
            return new SparseVector<T>(dimension, values);
        }
    }
}
