﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LinearLite.Symbolic
{
    public class ExpressionParser
    {
        private char[] _supportedInstructionSet = { '+', '-', '*', '/', '^', '!', '(', '{', '[', ')', '}', ']' };
        private char[] _openBrackets = { '(', '[', '{' };

        internal Expression Parse(string s)
        {
            s = s.Trim();

            ValidateString(s);
            CheckBrackets(s);

            var tags = new Dictionary<string, string>();
            int count = 0;

            ReplaceCommonFunctions(s, ref count, tags);
            InsertMultiplication(ref s);

            tags["root"] = s;

            foreach (var p in tags)
            {
                Debug.WriteLine(p.Key + "\t" + p.Value);
            }

            return null;
        }


        /// <summary>
        /// Check to make sure the string conforms to our supported instruction sets
        /// </summary>
        /// <param name="s"></param>
        private void ValidateString(string s)
        {
            foreach (char c in s)
            {
                if (char.IsWhiteSpace(c)) continue;

                // numeric digits are allowed
                if ('0' <= c && c <= '9') continue;

                // decimal points and placeholders are allowed
                if (c == '.' || c == ',') continue;

                // variables are allowed
                if ('a' <= c && c <= 'z') continue;
                if ('A' <= c && c <= 'Z') continue;

                // instructions are allowed
                if (_supportedInstructionSet.Contains(c)) continue;

                throw new ArgumentException();
            }
        }

        /// <summary>
        /// Check brackets
        /// </summary>
        /// <param name="s"></param>
        private void CheckBrackets(string s)
        {
            List<char> currentOpenBrackets = new List<char>();
            foreach (char c in s)
            {
                if (IsOpenBracket(c))
                {
                    currentOpenBrackets.Add(c);
                }
                else if (IsCloseBracket(c))
                {
                    char last = currentOpenBrackets[currentOpenBrackets.Count - 1];
                    if (last != GetComplementingBracket(c))
                    {
                        throw new ArgumentException("Mismatched brackets");
                    }
                    currentOpenBrackets.RemoveAt(currentOpenBrackets.Count - 1);
                }
            }

            if (currentOpenBrackets.Count > 0)
            {
                throw new ArgumentException("Mismatched brackets");
            }
        }
        
        /// <summary>
        /// Replace common functions, exp(x), log(x)
        /// </summary>
        /// <param name="s"></param>
        private void ReplaceCommonFunctions(string s, ref int tagCount, Dictionary<string, string> tags)
        {

        }

        /// <summary>
        /// Insert multiplication operators in between 
        /// 1) numbers and brackets
        /// 2) characters and brackets
        /// </summary>
        /// <param name="s"></param>
        private void InsertMultiplication(ref string s)
        {
            // alphanumeric then opening bracket
            Regex regex = new Regex("[0-9a-zA-Z]\\s*[([{]");
            while (true)
            {
                Match match = regex.Match(s);
                if (!match.Success)
                {
                    break;
                }

                string debug = s;
                s = s.Substring(0, match.Index + 1) + " * " + s.Substring(match.Index + match.Length - 1);
                Debug.WriteLine(debug + " -> " + s);
            }

            // closing bracket then alphanumeric
            regex = new Regex("[)\\]}]\\s*[0-9a-zA-Z]");
            while (true)
            {
                Match match = regex.Match(s);
                if (!match.Success)
                {
                    break;
                }

                string debug = s;
                s = s.Substring(0, match.Index + 1) + " * " + s.Substring(match.Index + match.Length - 1);
                Debug.WriteLine(debug + " -> " + s);
            }

            // Closing bracket and opening bracket
            regex = new Regex("[)\\]}]\\s*[([{]");
            while (true)
            {
                Match match = regex.Match(s);
                if (!match.Success)
                {
                    break;
                }

                string debug = s;
                s = s.Substring(0, match.Index + 1) + " * " + s.Substring(match.Index + match.Length - 1);
                Debug.WriteLine(debug + " -> " + s);
            }
        }

        /// <summary>
        /// Replace sub expressions with ?#? where # is some number. Then store the true expressions in 
        /// the tags dictionary
        /// </summary>
        /// <param name="s"></param>
        /// <param name="tags"></param>
        private void RecursiveReplaceWithTag(ref string expr, ref int tagCount, Dictionary<string, string> tags)
        {
            // Brackets
            while (expr.IndexOfAny(_openBrackets) >= 0)
            {
                Tuple<string, string> value = ReplaceOutermostBracketWithTag(ref expr, ref tagCount);
                if (value != null)
                {
                    string childExpr = value.Item2;
                    RecursiveReplaceWithTag(ref childExpr, ref tagCount, tags);
                    tags[value.Item1] = childExpr;
                }
            }
        }
        /// <summary>
        /// 1 + (2 + 3(1 + 5)) + 2 -> 1 + ?tag? + 2, returning <?tag?, 2 + 3(1 + 5)>
        /// </summary>
        /// <param name="expr"></param>
        /// <param name="tagCount"></param>
        /// <returns></returns>
        private Tuple<string, string> ReplaceOutermostBracketWithTag(ref string expr, ref int tagCount)
        {
            char open_bracket = ' ';
            int open_index = -1;

            for (int i = 0; i < expr.Length; ++i)
            {
                char c = expr[i];
                if (IsOpenBracket(c))
                {
                    open_bracket = c;
                    open_index = i;
                    break;
                }
            }

            if (open_index < 0)
            {
                return null;
            }
            
            List<char> brackets = new List<char>() { open_bracket };
            for (int i = open_index + 1; i < expr.Length; ++i)
            {
                char c = expr[i];
                if (IsOpenBracket(c))
                {
                    brackets.Add(c);
                }
                else if (IsCloseBracket(c))
                {
                    char comp = GetComplementingBracket(c);
                    if (comp != brackets.Last())
                    {
                        throw new InvalidOperationException();
                    }

                    brackets.RemoveAt(brackets.Count - 1);
                    if (brackets.Count == 0)
                    {
                        string tag = Tag(tagCount++);

                        Tuple<string, string> result = new Tuple<string, string>(tag, expr.Substring(open_index + 1, i - open_index - 1));
                        expr = expr.Substring(0, open_index) + tag + expr.Substring(i + 1);
                        return result;
                    }
                }
            }
            return null;
        }
        private bool IsOpenBracket(char c) => c == '(' || c == '[' || c == '{';
        private bool IsCloseBracket(char c) => c == ')' || c == ']' || c == '}';
        private bool IsDigit(char c) => '0' <= c && c <= '9';
        private string Tag(int count) => "<" + count + ">";

        private char GetComplementingBracket(char c)
        {
            switch (c)
            {
                case '(': return ')';
                case '{': return '}';
                case '[': return ']';
                case ')': return '(';
                case '}': return '{';
                case ']': return '[';
            }
            throw new NotSupportedException();
        }
    }
}
