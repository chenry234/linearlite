﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Symbolic
{
    public class ExpressionParser2
    {
        public Expression Parse(string s)
        {
            // Find the top level function 

            s = s.Trim();

            List<Tuple<int, int>> topLevelBrackets = new List<Tuple<int, int>>();

            int startIndex = 0;
            while (true)
            {
                s.OutermostBracketSet(startIndex, out int openBracketIndex, out int closeBracketIndex);
                startIndex = closeBracketIndex;

                if (openBracketIndex < 0 || closeBracketIndex < 0)
                {
                    break;
                }
                topLevelBrackets.Add(new Tuple<int, int>(openBracketIndex, closeBracketIndex));
            }
            
            foreach (Tuple<int, int> t in topLevelBrackets)
            {
                Debug.WriteLine(t.Item1 + "\t" + t.Item2 + "\t" + s.Substring(t.Item1, t.Item2 - t.Item1));
            }

            return null;
        }
    }
}
