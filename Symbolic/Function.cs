﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Symbolic
{
    public class Function
    {
        private bool _sign;
        private List<Function> _children;

        public Function Parse(string s)
        {
            s = s.Trim();

            // Split by brackets first, highest on the BIMDAS hierarchy, 
            // for all supported bracket types
            int open_index = s.IndexOf("(", "[", "{");
            if (open_index >= 0)
            {
                char bracket_type = s[open_index];

            }

            // Split by + and - operators that are not inside brackets first, as they are the lowest in the BIMDAS hierarchy


            return null;
        }
        
        public static void Test()
        {
            Function f = new Function();
            f.Parse("(2+3)*5+6/9-4");
        }
    }
}
