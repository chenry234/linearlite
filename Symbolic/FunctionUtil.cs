﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Symbolic
{
    public static class FunctionUtil
    {
        /// <summary>
        /// Returns the first instance of any one of the alternatives, or, if none are present, return -1
        /// </summary>
        /// <param name="s"></param>
        /// <param name="alternatives"></param>
        /// <returns></returns>
        internal static int IndexOf(this string s, params string[] alternatives)
        {
            int minIndex = int.MaxValue;
            foreach (string alt in alternatives)
            {
                int i = s.IndexOf(alt);
                if (i >= 0 && i < minIndex)
                {
                    minIndex = i;
                }
            }

            if (minIndex == int.MaxValue)
            {
                return -1;
            }
            return minIndex;
        }

    }
}
