﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Symbolic
{
    internal static class StringUtils
    {
        internal static bool IsOpenBracket(this char c) => c == '(' || c == '[' || c == '{';
        internal static bool IsCloseBracket(this char c) => c == ')' || c == ']' || c == '}';
        internal static bool IsDigit(char c) => '0' <= c && c <= '9';

        internal static char GetComplementingBracket(this char c)
        {
            switch (c)
            {
                case '(': return ')';
                case '{': return '}';
                case '[': return ']';
                case ')': return '(';
                case '}': return '{';
                case ']': return '[';
            }
            throw new NotSupportedException();
        }
        internal static bool Complements(this char a, char b) 
        {
            return
                (a == '(' && b == ')') ||
                (a == ')' && b == '(') ||
                (a == '[' && b == ']') ||
                (a == ']' && b == '[') ||
                (a == '{' && b == '}') ||
                (a == '}' && b == '{');
        }

        /// <summary>
        /// Returns the outermost bracket set of a string s
        /// </summary>
        /// <param name="s"></param>
        /// <param name="openBracketIndex"></param>
        /// <param name="closeBracketIndex"></param>
        internal static void OutermostBracketSet(this string s, int startIndex, out int openBracketIndex, out int closeBracketIndex)
        {
            int len = s.Length, i;
            for (i = startIndex; i < len; ++i)
            {
                if (s[i].IsOpenBracket())
                {
                    break;
                }
            }

            if (i == len)
            {
                openBracketIndex = -1;
                closeBracketIndex = -1;
            }
            else
            {
                openBracketIndex = i;
                closeBracketIndex = CloseBracketIndex(s, openBracketIndex);
            }
        }

        /// <summary>
        /// Returns the index of the corresponding close bracket, given the opening bracket's index
        /// </summary>
        /// <param name="s"></param>
        /// <param name="openingBracketIndex"></param>
        /// <returns></returns>
        internal static int CloseBracketIndex(this string s, int openingBracketIndex)
        {
            char c = s[openingBracketIndex];
            if (!c.IsOpenBracket())
            {
                return -1;
            }

            List<char> open_brackets = new List<char>();
            int len = s.Length, i;
            for (i = openingBracketIndex + 1; i < len; ++i)
            {
                if (s[i].IsOpenBracket())
                {
                    open_brackets.Add(s[i]);
                }
                else if (s[i].IsCloseBracket())
                {
                    int last = open_brackets.Count - 1;
                    if (last >= 0 && s[i].Complements(open_brackets[last]))
                    {
                        open_brackets.RemoveAt(last);

                        if (last == 0)
                        {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }
    }
}
