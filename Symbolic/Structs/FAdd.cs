﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearLite.Structs;

namespace LinearLite.Symbolic.Structs
{
    internal class FAdd : IFunction
    {
        internal FAdd(int count = 2) : base("add", count)
        {

        }
        protected override double evaluate_inner(double[] param)
        {
            double sum = 0.0;
            foreach (double p in param)
            {
                sum += p;
            }
            return sum;
        }

        protected override Complex evaluate_inner(Complex[] param)
        {
            Complex sum = Complex.Zero;
            foreach (Complex p in param)
            {
                sum.IncrementBy(p);
            }
            return sum;
        }
        public override string ToString()
        {
            throw new NotImplementedException();
        }

    }
}
