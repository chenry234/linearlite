﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Symbolic.Structs
{
    public class FConstant : IFunction
    {
        private double _value;

        public FConstant(double value) : base("const", 0)
        {

        }

        public override string ToString()
        {
            throw new NotImplementedException();
        }

        protected override double evaluate_inner(double[] param) => _value;

        protected override Complex evaluate_inner(Complex[] param)
        {
            throw new NotImplementedException();
        }
    }
}
