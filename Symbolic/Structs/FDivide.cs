﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearLite.Structs;

namespace LinearLite.Symbolic.Structs
{
    internal class FDivide : IFunction
    {
        internal FDivide() : base("divide", 2) { }

        public override string ToString()
        {
            throw new NotImplementedException();
        }
        
        protected override double evaluate_inner(double[] param) => param[0] / param[1];
        protected override Complex evaluate_inner(Complex[] param) => param[0] / param[1];
    }
}
