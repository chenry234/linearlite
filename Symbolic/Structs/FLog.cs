﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearLite.Structs;

namespace LinearLite.Symbolic.Structs
{
    internal class FLog : IFunction
    {
        internal FLog() : base("log", 1) { }

        public override string ToString()
        {
            throw new NotImplementedException();
        }

        protected override double evaluate_inner(double[] param)
        {
            double x = param[0];
            if (x <= 0)
            {
                return double.NaN;
            }
            return Math.Log(x);
        }

        protected override Complex evaluate_inner(Complex[] param)
        {
            Complex x = param[0];
            if (x.Modulus() == 0)
            {
                return Complex.NaN;
            }
            return Complex.Log(x);
        }
    }
}
