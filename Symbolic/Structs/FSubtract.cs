﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearLite.Structs;

namespace LinearLite.Symbolic.Structs
{
    internal class FSubtract : IFunction
    {
        internal FSubtract() : base("subtract", 2)
        {

        }

        public override string ToString()
        {
            throw new NotImplementedException();
        }
        
        protected override double evaluate_inner(double[] param) => param[0] - param[1];
        protected override Complex evaluate_inner(Complex[] param) => param[0].Subtract(param[1]);
    }
}
