﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinearLite.Structs;

namespace LinearLite.Symbolic.Structs
{
    internal class FTan : IFunction
    {
        internal FTan() : base("tan", 1) { }

        public override string ToString()
        {
            throw new NotImplementedException();
        }

        protected override double evaluate_inner(double[] param) => Math.Tan(param[0]);

        protected override Complex evaluate_inner(Complex[] param)
        {
            throw new NotImplementedException();
        }
    }
}
