﻿using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Symbolic.Structs
{
    public abstract class IFunction
    {
        public string Name { get; private set; }
        public int ParameterCount { get; private set; }

        protected IFunction(string name, int parameters)
        {
            Name = name;
            ParameterCount = parameters;
        }
        
        public double Evaluate(params double[] param)
        {
            if (param.Length != ParameterCount) throw new InvalidOperationException();
            return evaluate_inner(param);
        }
        public Complex Evaluate(params Complex[] param)
        {
            if (param.Length != ParameterCount) throw new InvalidOperationException();
            return evaluate_inner(param);
        }
        
        protected abstract double evaluate_inner(double[] param);
        protected abstract Complex evaluate_inner(Complex[] param);

        public new abstract string ToString();
    }
}
