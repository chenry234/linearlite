﻿using LinearLite.BLAS;
using LinearLite.Structs.Tensors;
using LinearLite.Structs.Vectors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tensors.Decompositions
{
    public static class CanonicalPolyadicDecomposition
    {
        /// <summary>
        /// Requires a debug because of access issues 
        /// </summary>
        /// <param name="T"></param>
        /// <param name="u"></param>
        /// <param name="L2RegularisationFactor"></param>
        public static void CanonicalPolyadicDecompose(this DenseTensor<double> T, out List<DenseVector<double>> u, double L2RegularisationFactor = 0.0)
        {
            if (T == null)
            {
                throw new ArgumentNullException();
            }

            // ### parameters --------------------------------------------------------------------------------

            int checkLossEvery = 5;

            /// Exit the ALS iterative loop when the absolute scaled norm of | A - uv | / (mn) < threshold
            double absoluteNormTerminationThreshold = 1e-20;
            /// Exit the ALS iterative loop when the absolute scaled norm of | A - uv | / mn changes by less
            /// than this amount in between two measures.
            double deltaNormTerminationThreshold = 1e-20;

            // ### end parameters ----------------------------------------------------------------------------

            int order = T.Order;
            List<int> dimensions = T.GetDimensions();
            List<int> orderedDimensions = GetDecreasingOrder(dimensions);

            u = new List<DenseVector<double>>();
            // Cache the L2 norms to avoid re-computation
            double[] vectorL2 = new double[order];
            for (int i = 0; i < order; ++i)
            {
                // TODO: think of a better initialisation scheme than 1 / n (the very least you could do is 
                // multiply it so it's of the same order as the tensor's elements...
                double[] init = new double[dimensions[i]];
                double x = Math.Pow(1.0 / init.Length, 1.0 / order);
                for (int j = 0; j < init.Length; ++j)
                {
                    init[j] = x;
                }
                u.Add(new DenseVector<double>(init));
                vectorL2[i] = init.Dot(init);
            }

            double lastNormMeasurement = double.MaxValue;

            // Iterate until convergence
            for (int t = 0; t < 100; ++t)
            {
                for (int j = 0; j < order; ++j)
                {
                    int stationaryDimension = orderedDimensions[j];

                    List<int> remainingDimensions = new List<int>();
                    remainingDimensions.AddRange(orderedDimensions);

                    DenseTensor<double> U = T;
                    for (int i = 0; i < order; ++i)
                    {
                        int dimension = orderedDimensions[i];
                        if (dimension == stationaryDimension) continue;

                        int index = remainingDimensions.IndexOf(dimension);
                        U = U.NModeProduct(u[dimension], index);
                        remainingDimensions.RemoveAt(index);
                    }

                    // U is now reduced to order 1 
                    double[] num = U.Values;

                    // Calculate the denominator, which is the product over all
                    // sums of squares of L2 norms, over all dimensions except j.
                    double den = 1;
                    for (int i = 0; i < order; ++i)
                    {
                        int dimension = orderedDimensions[i];
                        if (dimension != stationaryDimension)
                        {
                            den *= vectorL2[dimension];
                        }
                    }
                    den += L2RegularisationFactor;

                    // Update the stationary vector
                    double[] vector = u[stationaryDimension].Values;
                    for (int i = 0; i < vector.Length; ++i)
                    {
                        vector[i] = num[i] / den;
                    }
                    // Recalculate the norm of this vector, and cache
                    vectorL2[stationaryDimension] = vector.Dot(vector);
                }

                //Debug.WriteLine(t + "\t" + CalculateL1Norm(T, dimensions, u));

                if (t % checkLossEvery == 0)
                {
                    double norm = CalculateL1Norm(T, dimensions, u);
                    if (norm < absoluteNormTerminationThreshold)
                    {
                        break;
                    }
                    if ((lastNormMeasurement - norm) < deltaNormTerminationThreshold)
                    {
                        break;
                    }
                    lastNormMeasurement = norm;
                }
            }
        }
        private static double CalculateL1Norm(DenseTensor<double> T, List<int> dimensions, List<DenseVector<double>> u)
        {
            double norm = 0.0;

            // Calculate the denominator term
            double den = 1;
            foreach (int dim in dimensions)
            {
                den *= dim;
            }
            
            int order = dimensions.Count;
            int[] index = new int[order];
            while (true)
            {
                double prod = 1;
                for (int i = 0; i < order; ++i)
                {
                    prod *= u[i][index[i]];
                }
                norm += Math.Abs(T.Get(index) - prod);

                index[0]++;
                int j = 0;
                while (j < order && dimensions[j] <= index[j])
                {
                    index[j] = 0;
                    j++;
                    if (j < order)
                    {
                        index[j]++;
                    }
                    else
                    {
                        return norm / den;
                    }
                }
            }
        }
        private static List<int> GetDecreasingOrder(List<int> dimensions)
        {
            int order = dimensions.Count;

            // Sort the dimension sizes, so that we begin by contracting by the largest dimension
            List<Tuple<int, int>> tuples = new List<Tuple<int, int>>();
            for (int i = 0; i < order; ++i)
            {
                tuples.Add(new Tuple<int, int>(i, dimensions[i]));
            }
            tuples.Sort((a, b) => b.Item2.CompareTo(a.Item2));

            List<int> orderedDimensions = new List<int>();
            for (int i = 0; i < order; ++i)
            {
                orderedDimensions.Add(tuples[i].Item1);
            }

            return orderedDimensions;
        }

        /// <summary>
        /// Calculate the CPD of a tensor T, returning normalized dense vectors u with the property that the p-norm of each vector is 1. 
        /// The tensor will be a product of the scalar factor and each of the vectors u. 
        /// </summary>
        /// <param name="T"></param>
        /// <param name="p"></param>
        /// <param name="factor"></param>
        /// <param name="u"></param>
        public static void NormalizedCanonicalPolyadicDecompose(this DenseTensor<double> T, int p, out double factor, out List<DenseVector<double>> u)
        {
            if (p < 1)
            {
                throw new ArgumentOutOfRangeException();
            }

            CanonicalPolyadicDecompose(T, out u);

            IBLAS<double> blas = new DoubleBLAS();

            double norm = 1.0;
            foreach (DenseVector<double> v in u)
            {
                double p_norm = v.Values.Norm(p);
                blas.SCAL(v.Values, 1.0 / p_norm, 0, v.Dimension);
                norm *= p_norm;
            }

            factor = norm;
        }
    }
}
