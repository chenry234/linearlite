﻿using LinearLite.Structs.Tensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tensors
{
    /// <summary>
    /// Iterate over all entries of a dense tensor
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DenseTensorIterator<T> where T : new()
    {
        private int _order;
        private int[] _index;
        private List<int> _dimensions;
        private DenseTensor<T> _tensor;
        public bool HasNext { get; private set; }

        public int[] Index { get { return _index; } }

        public DenseTensorIterator(DenseTensor<T> tensor)
        {
            _tensor = tensor ?? throw new ArgumentException();
            _order = tensor.Order;
            _index = new int[Math.Max(1, _order)];
            _dimensions = tensor.GetDimensions();

            // not true if any of the dimensions of the tensor = 0
            HasNext = true;
            foreach (int dim in _dimensions)
            {
                if (dim == 0)
                {
                    HasNext = false;
                    break;
                }
            }
        }
        
        public T Next()
        {
            if (!HasNext)
            {
                return default(T);
            }

            T obj = _tensor.get_unsafe(_index);

            _index[0]++;

            int j = 0;
            while (j < _order && _index[j] >= _dimensions[j])
            {
                _index[j] = 0;
                j++;

                if (j < _order)
                {
                    _index[j]++;
                }
            }

            if (j >= _order)
            {
                HasNext = false;
            }

            return obj;
        }
    }
}
