﻿using LinearLite.BLAS;
using LinearLite.Matrices;
using LinearLite.Structs;
using LinearLite.Structs.Tensors;
using LinearLite.Structs.Vectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tensors
{
    public static class DenseTensorOperationsExtensions
    {
        internal static T[][][] ToJagged<T>(this T[,,] tensor)
        {
            int m = tensor.GetLength(0), n = tensor.GetLength(1), p = tensor.GetLength(2), i, j, k;
            T[][][] s0 = new T[m][][];
            for (i = 0; i < m; ++i)
            {
                T[][] s1 = new T[n][];
                for (j = 0; j < n; ++j)
                {
                    T[] s2 = new T[p];
                    for (k = 0; k < p; ++k)
                    {
                        s2[k] = tensor[i, j, k];
                    }
                    s1[j] = s2;
                }
                s0[i] = s1;
            }
            return s0;
        }
        internal static T[][][][] ToJagged<T>(this T[,,,] tensor)
        {
            int m = tensor.GetLength(0), n = tensor.GetLength(1), p = tensor.GetLength(2), q = tensor.GetLength(3), i, j, k, l;

            T[][][][] s0 = new T[m][][][];
            for (i = 0; i < m; ++i)
            {
                T[][][] s1 = new T[n][][];
                for (j = 0; j < n; ++j)
                {
                    T[][] s2 = new T[p][];
                    for (k = 0; k < p; ++k)
                    {
                        T[] s3 = new T[q];
                        for (l = 0; l < q; ++l)
                        {
                            s3[l] = tensor[i, j, k, l];
                        }
                        s2[k] = s3;
                    }
                    s1[j] = s2;
                }
                s0[i] = s1;
            }
            return s0;
        }
        
        internal static G[][] DirectSum<G>(this G[][] S, G[][] T)
        {
            if (S == null || T == null) throw new ArgumentNullException();

            int m1 = S.Length, n1 = S[0].Length, m2 = T.Length, n2 = T[0].Length, i, j;
            G[][] sum = MatrixInternalExtensions.JMatrix<G>(m1 + m2, n1 + n2);
            for (i = 0; i < m1; ++i)
            {
                G[] S_i = S[i], sum_i = sum[i];
                for (j = 0; j < n1; ++j)
                {
                    sum_i[j] = S_i[j];
                }
            }
            for (i = 0; i < m2; ++i)
            {
                G[] T_i = T[i], sum_i = sum[i + m1];
                for (j = 0; j < n2; ++j)
                {
                    sum_i[j + n1] = T_i[j];
                }
            }
            return sum;
        }
        internal static G[][][] DirectSum<G>(this G[][][] S, G[][][] T)
        {
            if (S == null || T == null) throw new ArgumentNullException();

            int s1 = S.Length, s2 = S[0].Length, s3 = S[0][0].Length, t1 = T.Length, t2 = T[0].Length, t3 = T[0][0].Length, i, j, k;
            G[][][] sum = TensorInternalExtensions.JTensor<G>(s1 + t1, s2 + t2, s3 + t3);
            for (i = 0; i < s1; ++i)
            {
                G[][] S_i = S[i], sum_i = sum[i];
                for (j = 0; j < s2; ++j)
                {
                    G[] S_ij = S_i[j], sum_ij = sum_i[j];
                    for (k = 0; k < s3; ++k)
                    {
                        sum_ij[k] = S_ij[k];
                    }
                }
            }
            for (i = 0; i < t1; ++i)
            {
                G[][] T_i = T[i], sum_i = sum[i + s1];
                for (j = 0; j < t2; ++j)
                {
                    G[] T_ij = T_i[j], sum_ij = sum_i[j + s2];
                    for (k = 0; k < t3; ++k)
                    {
                        sum_ij[k + s3] = T_ij[k];
                    }
                }
            }
            return sum;
        }
        internal static G[][][][] DirectSum<G>(this G[][][][] S, G[][][][] T)
        {
            if (S == null || T == null) throw new ArgumentNullException();

            int s1 = S.Length, s2 = S[0].Length, s3 = S[0][0].Length, s4 = S[0][0][0].Length, 
                t1 = T.Length, t2 = T[0].Length, t3 = T[0][0].Length, t4 = T[0][0][0].Length, i, j, k, l;
            G[][][][] sum = TensorInternalExtensions.JTensor<G>(s1 + t1, s2 + t2, s3 + t3, s4 + t4);
            for (i = 0; i < s1; ++i)
            {
                G[][][] S_i = S[i], sum_i = sum[i];
                for (j = 0; j < s2; ++j)
                {
                    G[][] S_ij = S_i[j], sum_ij = sum_i[j];
                    for (k = 0; k < s3; ++k)
                    {
                        G[] S_ijk = S_ij[k], sum_ijk = sum_ij[k];
                        for (l = 0; l < s4; ++l)
                        {
                            sum_ijk[l] = S_ijk[l];
                        }
                    }
                }
            }
            for (i = 0; i < t1; ++i)
            {
                G[][][] T_i = T[i], sum_i = sum[i + s1];
                for (j = 0; j < s2; ++j)
                {
                    G[][] T_ij = T_i[j], sum_ij = sum_i[j + s2];
                    for (k = 0; k < s3; ++k)
                    {
                        G[] T_ijk = T_ij[k], sum_ijk = sum_ij[k + s3];
                        for (l = 0; l < s4; ++l)
                        {
                            sum_ijk[l + s4] = T_ijk[l];
                        }
                    }
                }
            }
            return sum;
        }

        /// <summary>
        /// <para>Given two tensors $\mathcal{S}, \mathcal{T}$ of the same dimensions, returns their elementwise sum.</para>
        /// <para>Throws <txt>InvalidOperationException</txt> if the dimensions of the tensors don't match.</para>
        /// <para>Also see: <a href="#ElementwiseOperateTensor"><txt>ElementwiseOperate</txt></a>.</para>
        /// </summary>
        /// <name>AddElementwise</name>
        /// <proto>ITensor<F> AddElementwise(this ITensor<F> S, ITensor<F> T)</proto>
        /// <cat>la</cat>
        /// <param name="S"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        public static DenseTensor<int> AddElementwise(this DenseTensor<int> S, DenseTensor<int> T) => S.ElementwiseOperate(T, (a, b) => a + b);
        public static DenseTensor<long> AddElementwise(this DenseTensor<long> S, DenseTensor<long> T) => S.ElementwiseOperate(T, (a, b) => a + b);
        public static DenseTensor<float> AddElementwise(this DenseTensor<float> S, DenseTensor<float> T) => S.ElementwiseOperate(T, (a, b) => a + b);
        public static DenseTensor<double> AddElementwise(this DenseTensor<double> S, DenseTensor<double> T) => S.ElementwiseOperate(T, (a, b) => a + b);
        public static DenseTensor<decimal> AddElementwise(this DenseTensor<decimal> S, DenseTensor<decimal> T) => S.ElementwiseOperate(T, (a, b) => a + b);
        public static DenseTensor<G> AddElementwise<G>(this DenseTensor<G> S, DenseTensor<G> T) where G : AdditiveGroup<G>, new() => S.ElementwiseOperate(T, (a, b) => a.Add(b));

        /// <summary>
        /// <para>
        /// Given two tensors $\mathcal{S}, \mathcal{T}$ of the same dimensions, returns their elementwise difference.</para>
        /// <para>Throws <txt>InvalidOperationException</txt> if the dimensions of the tensors don't match.</para>
        /// <para>Also see: <a href="#ElementwiseOperateTensor"><txt>ElementwiseOperate</txt></a>.</para>
        /// </summary>
        /// <name>SubtractElementwise</name>
        /// <proto>ITensor<F> SubtractElementwise(this ITensor<F> S, ITensor<F> T)</proto>
        /// <cat>la</cat>
        /// <param name="S"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        public static DenseTensor<int> SubtractElementwise(this DenseTensor<int> S, DenseTensor<int> T) => S.ElementwiseOperate(T, (a, b) => a - b);
        public static DenseTensor<long> SubtractElementwise(this DenseTensor<long> S, DenseTensor<long> T) => S.ElementwiseOperate(T, (a, b) => a - b);
        public static DenseTensor<float> SubtractElementwise(this DenseTensor<float> S, DenseTensor<float> T) => S.ElementwiseOperate(T, (a, b) => a - b);
        public static DenseTensor<decimal> SubtractElementwise(this DenseTensor<decimal> S, DenseTensor<decimal> T) => S.ElementwiseOperate(T, (a, b) => a - b);
        public static DenseTensor<double> SubtractElementwise(this DenseTensor<double> S, DenseTensor<double> T) => S.ElementwiseOperate(T, (a, b) => a - b);
        public static DenseTensor<G> SubtractElementwise<G>(this DenseTensor<G> S, DenseTensor<G> T) where G : AdditiveGroup<G>, new() => S.ElementwiseOperate(T, (a, b) => a.Subtract(b));

        /// <summary>
        /// <para>
        /// Given two tensors $\mathcal{S}, \mathcal{T}$ of the same dimensions, returns their elementwise product.</para>
        /// <para>Throws <txt>InvalidOperationException</txt> if the dimensions of the tensors don't match.</para>
        /// <para>Also see: <a href="#ElementwiseOperateTensor"><txt>ElementwiseOperate</txt></a>.</para>
        /// </summary>
        /// <name>MultiplyElementwise</name>
        /// <proto>ITensor<F> MultiplyElementwise(this ITensor<F> S, ITensor<F> T)</proto>
        /// <cat>la</cat>
        /// <param name="S"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        public static DenseTensor<int> MultiplyElementwise(this DenseTensor<int> S, DenseTensor<int> T) => S.ElementwiseOperate(T, (a, b) => a * b);
        public static DenseTensor<long> MultiplyElementwise(this DenseTensor<long> S, DenseTensor<long> T) => S.ElementwiseOperate(T, (a, b) => a * b);
        public static DenseTensor<float> MultiplyElementwise(this DenseTensor<float> S, DenseTensor<float> T) => S.ElementwiseOperate(T, (a, b) => a * b);
        public static DenseTensor<decimal> MultiplyElementwise(this DenseTensor<decimal> S, DenseTensor<decimal> T) => S.ElementwiseOperate(T, (a, b) => a * b);
        public static DenseTensor<double> MultiplyElementwise(this DenseTensor<double> S, DenseTensor<double> T) => S.ElementwiseOperate(T, (a, b) => a * b);
        public static DenseTensor<F> MultiplyElementwise<F>(this DenseTensor<F> S, DenseTensor<F> T) where F : Ring<F>, new() => S.ElementwiseOperate(T, (a, b) => a.Multiply(b));

        /// <summary>
        /// <para>Returns the product between a tensor and a scalar. The original tensor is unchanged.</para>
        /// </summary>
        /// <name>MultiplyTensorScalar</name>
        /// <proto>ITensor<T> Multiply(this ITensor<T> S, T scalar)</proto>
        /// <cat>la</cat>
        /// <param name="S"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        public static DenseTensor<int> Multiply(this DenseTensor<int> T, int s) => T.Multiply(s, new IntBLAS());
        public static DenseTensor<long> Multiply(this DenseTensor<long> T, long s) => T.Multiply(s, new LongBLAS());
        public static DenseTensor<float> Multiply(this DenseTensor<float> T, float s) => T.Multiply(s, new FloatBLAS());
        public static DenseTensor<double> Multiply(this DenseTensor<double> T, double s) => T.Multiply(s, new DoubleBLAS());
        public static DenseTensor<decimal> Multiply(this DenseTensor<decimal> T, decimal s) => T.Multiply(s, new DecimalBLAS());
        public static DenseTensor<Complex> Multiply(this DenseTensor<Complex> T, Complex s) => T.Multiply(s, new ComplexBLAS());
        public static DenseTensor<F> Multiply<F>(this DenseTensor<F> T, F s) where F : Ring<F>, new() => T.Multiply(s, new RingBLAS<F>());

        /// <summary>
        /// <para>Returns the tensor product between the tensors $\mathcal{S}\in\mathbb{F}^{d_1 \times d_2 \times ... \times d_n }$
        /// and $\mathcal{T}\in\mathbb{F}^{e_1 \times e_2 \times ... \times e_n }$.</para>
        /// The result will be a tensor of dimensions $d_1 e_1 \times d_2 e_1 \times ... \times d_n e_n$, where 
        /// $$(\mathcal{S}\otimes\mathcal{T})_{i_1j_i, i_2j_2, ..., i_nj_n} = \mathcal{S}_{i_1, i_2, ..., i_n} \mathcal{T}_{j_1, j_2, ..., j_n}$$
        /// for all $1\le{i_k}\le{d_k}$ and $1\le{j_k}\le{e_k}$ for all $1\le{k}\le{n}$.
        /// </summary>
        /// <name>TensorProduct</name>
        /// <proto>ITensor<F> TensorProduct(this ITensor<F> S, ITensor<F> T)</proto>
        /// <cat>la</cat>
        /// <param name="S"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        public static DenseTensor<int> TensorProduct(this DenseTensor<int> S, DenseTensor<int> T) => S.TensorProduct(T, (a, b) => a * b);
        public static DenseTensor<long> TensorProduct(this DenseTensor<long> S, DenseTensor<long> T) => S.TensorProduct(T, (a, b) => a * b);
        public static DenseTensor<float> TensorProduct(this DenseTensor<float> S, DenseTensor<float> T) => S.TensorProduct(T, (a, b) => a * b);
        public static DenseTensor<double> TensorProduct(this DenseTensor<double> S, DenseTensor<double> T) => S.TensorProduct(T, (a, b) => a * b);
        public static DenseTensor<decimal> TensorProduct(this DenseTensor<decimal> S, DenseTensor<decimal> T) => S.TensorProduct(T, (a, b) => a * b);
        public static DenseTensor<F> TensorProduct<F>(this DenseTensor<F> S, DenseTensor<F> T) where F : Ring<F>, new() => S.TensorProduct(T, (a, b) => a.Multiply(b));

        /// <summary>
        /// Returns the $n$-mode product of a tensor $\mathcal{T}\in\mathbb{F}^{d_1 \times d_2 \times ... \times d_m}$ and matrix $U\in\mathbb{F}^{k \times d_n}$
        /// ($1\le{n}\le{m}$), which is a tensor $\mathcal{T} \times_n U$ of size $d_1 \times ... \times d_{n-1} \times k \times d_{n + 1} \times ... \times d_m$, such that
        /// 
        /// $$(\mathcal{T} \times_n U)_{i_1...i_{n-1} j i_{n+1}...i_m} = \sum_{i_n = 1}^{d_n} {\mathcal{T}_{i_1i_2...i_m} U_{j i_n}}$$
        /// for each $1\le{i_l}\le{d_l}$ (for $1\le{l}\le{m}$) and $1\le{j}\le{k}$.
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>NModeProduct</name>
        /// <proto>ITensor<F> NModeProduct(this ITensor<F> T, IMatrix<F> U, int n)</proto>
        /// <cat>la</cat>
        /// <param name="T">A order-$m$ tensor of dimension $d_1 \times d_2 \times ... \times d_m$.</param>
        /// <param name="U">A matrix of dimension $k \times d_n$.</param>
        /// <param name="n">The mode of the product, $n$, where $1\le{n}\le{m}$.</param>
        /// <returns>
        /// A order-$m$ tensor of dimension $d_1 \times ... \times d_{n-1} \times k \times d_{n + 1} \times ... \times d_m$.
        /// </returns>
        public static DenseTensor<int> NModeProduct(this DenseTensor<int> T, DenseMatrix<int> U, int n) => T.NModeProduct(n, U, (A, x) => A.Multiply(x));
        public static DenseTensor<long> NModeProduct(this DenseTensor<long> T, DenseMatrix<long> U, int n) => T.NModeProduct(n, U, (A, x) => A.Multiply(x));
        public static DenseTensor<float> NModeProduct(this DenseTensor<float> T, DenseMatrix<float> U, int n) => T.NModeProduct(n, U, (A, x) => A.Multiply(x));
        public static DenseTensor<double> NModeProduct(this DenseTensor<double> T, DenseMatrix<double> U, int n) => T.NModeProduct(n, U, (A, x) => A.Multiply(x));
        public static DenseTensor<decimal> NModeProduct(this DenseTensor<decimal> T, DenseMatrix<decimal> U, int n) => T.NModeProduct(n, U, (A, x) => A.Multiply(x));
        public static DenseTensor<R> NModeProduct<R>(this DenseTensor<R> T, DenseMatrix<R> U, int n) where R : Ring<R>, new() => T.NModeProduct(n, U, (A, x) => A.Multiply(x));

        /// <summary>
        /// Returns the $n$-mode product of a tensor $\mathcal{T}\in\mathbb{F}^{d_1 \times d_2 \times ... \times d_m}$ and a vector $v\in\mathbb{F}^{d_n}$
        /// ($1\le{n}\le{m}$), which is a tensor $(\mathcal{T} \times_n v)$ of size $d_1 \times ... \times d_{n-1} \times d_{n + 1} \times ... \times d_m$, such that
        /// 
        /// $$(\mathcal{T} \times_n v)_{i_1...i_{n-1} i_{n+1}...i_m} = \sum_{i_n = 1}^{d_n} {\mathcal{T}_{i_1i_2...i_m} v_{i_n}}$$
        /// for each $1\le{i_l}\le{d_l}$ (for $1\le{l}\le{m}$).
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>NModeVectorProduct</name>
        /// <proto>ITensor<F> NModeProduct(this ITensor<F> T, IVector<F> v, int n)</proto>
        /// <cat>la</cat>
        /// <param name="T">A order-$m$ tensor of dimension $d_1 \times d_2 \times ... \times d_m$.</param>
        /// <param name="v">A vector of dimension $d_n$.</param>
        /// <param name="n">The mode of the product, $n$, where $1\le{n}\le{m}$.</param>
        /// <returns>
        /// A order $m - 1$ tensor of dimension $d_1 \times ... \times d_{n-1} \times d_{n + 1} \times ... \times d_m$.
        /// </returns>
        public static DenseTensor<int> NModeProduct(this DenseTensor<int> T, DenseVector<int> v, int n) => T.NModeProduct(n, v, (x, y) => x.Dot(y));
        public static DenseTensor<long> NModeProduct(this DenseTensor<long> T, DenseVector<long> v, int n) => T.NModeProduct(n, v, (x, y) => x.Dot(y));
        public static DenseTensor<float> NModeProduct(this DenseTensor<float> T, DenseVector<float> v, int n) => T.NModeProduct(n, v, (x, y) => x.Dot(y));
        public static DenseTensor<double> NModeProduct(this DenseTensor<double> T, DenseVector<double> v, int n) => T.NModeProduct(n, v, (x, y) => x.Dot(y));
        public static DenseTensor<decimal> NModeProduct(this DenseTensor<decimal> T, DenseVector<decimal> v, int n) => T.NModeProduct(n, v, (x, y) => x.Dot(y));
        public static DenseTensor<R> NModeProduct<R>(this DenseTensor<R> T, DenseVector<R> v, int n) where R : Ring<R>, new() => T.NModeProduct(n, v, (x, y) => x.Dot(y));

        /// <summary>
        /// <para>Returns the outer product between a $n$-th order tensor $\mathcal{T}\in\mathbb{F}^{d_1 \times ... \times d_n}$ and vector $v\in\mathbb{F}^k$, 
        /// $\mathcal{T} \otimes v \in\mathbb{F}^{d_1 \times ... \times d_n \times k}.$</para>
        /// <para>The returned object will be a $(n + 1)$-th order tensor of dimension $d_1 \times ... \times d_n \times k$, where 
        /// $$(\mathcal{T} \otimes v)_{i_1...i_nj} = \mathcal{T}_{i_1...i_n}v_j$$
        /// for every $1\le{i_l}\le{d_n}$ for $1\le{l}\le{n}$ and $1\le{j}\le{k}$.</para>
        /// 
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>OuterProductTensorVector</name>
        /// <proto>ITensor<F> OuterProduct(this ITensor<F> T, IVector<F> v)</proto>
        /// <cat>la</cat>
        /// <param name="T">A $n$-order tensor of dimension $d_1 \times d_2 \times ... \times d_n$.</param>
        /// <param name="v">A $k$-dimensional vector.</param>
        /// <returns>A $(n + 1)$-th order tensor of dimension $d_1 \times d_2 \times ... \times d_n \times k$.</returns>
        public static DenseTensor<int> OuterProduct(this DenseTensor<int> T, DenseVector<int> v) => T.OuterProduct(v, new IntBLAS());
        public static DenseTensor<long> OuterProduct(this DenseTensor<long> T, DenseVector<long> v) => T.OuterProduct(v, new LongBLAS());
        public static DenseTensor<float> OuterProduct(this DenseTensor<float> T, DenseVector<float> v) => T.OuterProduct(v, new FloatBLAS());
        public static DenseTensor<double> OuterProduct(this DenseTensor<double> T, DenseVector<double> v) => T.OuterProduct(v, new DoubleBLAS());
        public static DenseTensor<decimal> OuterProduct(this DenseTensor<decimal> T, DenseVector<decimal> v) => T.OuterProduct(v, new DecimalBLAS());
        public static DenseTensor<Complex> OuterProduct(this DenseTensor<Complex> T, DenseVector<Complex> v) => T.OuterProduct(v, new ComplexBLAS());
        public static DenseTensor<R> OuterProduct<R>(this DenseTensor<R> T, DenseVector<R> v) where R : Ring<R>, new() => T.OuterProduct(v, new RingBLAS<R>());

        /// <summary>
        /// Given a $m$-th order tensor $\mathcal{T}\in\mathbb{F}^{d_1 \times ... \times d_m}$ and an integer $n$ with $1\le{n}\le{m}$, returns a vector $v\in\mathbb{F}^{d_n}$ formed by 
        /// summing over all dimensions of $\mathcal{T}$ except the $n$-th dimension. Explicitly, the vector can be defined elementwise by
        /// $$v_i = \sum_{i_1=1}^{d_1} ... \sum_{i_{n-1}=1}^{d_{n-1}} \sum_{i_{n+1}=1}^{d_{n+1}} ... \sum_{i_m=1}^{d_m} {\mathcal{T}_{i_1...i_{n-1}i_{n+1}...i_m}}$$
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>SumOverAllExcept</name>
        /// <proto>DenseVector<F> SumOverAllExcept(this ITensor<F> T, int n)</proto>
        /// <cat>la</cat>
        /// <param name="T">A $m$-th order tensor of dimension $d_1 \times ... \times d_m$.</param>
        /// <param name="n">The dimension to retain, $n$.</param>
        /// <returns>A vector of dimension $d_n$.</returns>
        public static DenseVector<int> SumOverAllExcept(this DenseTensor<int> T, int n) => T.SumOverAllExcept(n, 0, (a, b) => a + b);
        public static DenseVector<long> SumOverAllExcept(this DenseTensor<long> T, int n) => T.SumOverAllExcept(n, 0, (a, b) => a + b);
        public static DenseVector<float> SumOverAllExcept(this DenseTensor<float> T, int n) => T.SumOverAllExcept(n, 0, (a, b) => a + b);
        public static DenseVector<double> SumOverAllExcept(this DenseTensor<double> T, int n) => T.SumOverAllExcept(n, 0, (a, b) => a + b);
        public static DenseVector<decimal> SumOverAllExcept(this DenseTensor<decimal> T, int n) => T.SumOverAllExcept(n, 0, (a, b) => a + b);
        public static DenseVector<R> SumOverAllExcept<R>(this DenseTensor<R> T, int n) where R : AdditiveGroup<R>, new() => T.SumOverAllExcept(n, new R().AdditiveIdentity, (a, b) => a.Add(b));

        /// <summary>
        /// Given a $m$-th order tensor $\mathcal{T}\in\mathbb{F}^{d_1 \times ... \times d_m}$ and integers $n, p$ where $1\le{n}<{p}\le{m}$, returns a matrix $v\in\mathbb{F}^{d_n \times d_p}$ formed by 
        /// summing over all dimensions of $\mathcal{T}$ except the $n$-th and $p$-th dimensions.
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>SumOverAllExceptMatrix</name>
        /// <proto>DenseMatrix<F> SumOverAllExcept(this ITensor<F> T, int n, int p)</proto>
        /// <cat>la</cat>
        /// <param name="T">A $m$-th order tensor of dimension $d_1 \times ... \times d_m$.</param>
        /// <param name="n">The first dimension to retain, $n$.</param>
        /// <param name="p">The second dimension to retain, $p$.</param>
        /// <returns>A matrix of dimension $d_n \times d_p$.</returns>
        public static DenseMatrix<int> SumOverAllExcept(this DenseTensor<int> T, int m, int n) => T.SumOverAllExcept(m, n, 0, (a, b) => a + b);
        public static DenseMatrix<long> SumOverAllExcept(this DenseTensor<long> T, int m, int n) => T.SumOverAllExcept(m, n, 0, (a, b) => a + b);
        public static DenseMatrix<float> SumOverAllExcept(this DenseTensor<float> T, int m, int n) => T.SumOverAllExcept(m, n, 0, (a, b) => a + b);
        public static DenseMatrix<double> SumOverAllExcept(this DenseTensor<double> T, int m, int n) => T.SumOverAllExcept(m, n, 0, (a, b) => a + b);
        public static DenseMatrix<decimal> SumOverAllExcept(this DenseTensor<decimal> T, int m, int n) => T.SumOverAllExcept(m, n, 0, (a, b) => a + b);
        public static DenseMatrix<R> SumOverAllExcept<R>(this DenseTensor<R> T, int m, int n) where R: AdditiveGroup<R>, new() => T.SumOverAllExcept(m, n, new R().AdditiveIdentity, (a, b) => a.Add(b));

        /// <summary>
        /// Given a $m$-order tensor $\mathcal{T}\in\mathbb{F}^{d_1 \times ... \times d_m}$ and an integer $n$ where $1\le{n}\le{m}$, 
        /// returns a $(m-1)$-order tensor $\mathcal{S}$ formed by summing over all terms in dimension $n$. Elementwise,
        /// $$\mathcal{S}_{i_1...i_{n-1}i_{n+1}...i_m} = \sum_{i_n=1}^{d_n}{\mathcal{T}_{i_1...i_n}}$$
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>SumOver</name>
        /// <proto>DenseTensor<F> SumOver(this ITensor<F> T, int n)</proto>
        /// <cat>la</cat>
        /// <param name="T">A $m$-order tensor of dimension $d_1 \times ... \times d_m$</param>
        /// <param name="n">The dimension to sum over, $n$.</param>
        /// <returns>A tensor of dimension $d_1 \times ... \times d_{n-1} \times d_{n+1} \times ... \times d_m$</returns>
        public static DenseTensor<int> SumOver(this DenseTensor<int> T, int n) => T.SumOver(n, (a, b) => a + b);
        public static DenseTensor<long> SumOver(this DenseTensor<long> T, int n) => T.SumOver(n, (a, b) => a + b);
        public static DenseTensor<float> SumOver(this DenseTensor<float> T, int n) => T.SumOver(n, (a, b) => a + b);
        public static DenseTensor<double> SumOver(this DenseTensor<double> T, int n) => T.SumOver(n, (a, b) => a + b);
        public static DenseTensor<decimal> SumOver(this DenseTensor<decimal> T, int n) => T.SumOver(n, (a, b) => a + b);
        public static DenseTensor<R> SumOver<R>(this DenseTensor<R> T, int n) where R : AdditiveGroup<R>, new() => T.SumOver(n, (a, b) => a.Add(b));

    }
}
