﻿using LinearLite.Structs;
using LinearLite.Structs.Tensors;
using System;
using System.Collections.Generic;

namespace LinearLite.Tensors
{
    public static class SparseTensorOperationsExtensions
    {
        private static SparseTensor<F> AddElementwise<F>(this SparseTensor<F> S, SparseTensor<F> T, Func<F, F, F> Add) where F : new()
        {
            TensorChecks.CheckDimensionsMatch(S, T, out List<int> dimensions);

            SparseTensor<F> result = new SparseTensor<F>(dimensions.ToArray());
            Dictionary<long, F> v = result.Values, s = S.Values, t = T.Values;

            foreach (KeyValuePair<long, F> pair in s)
            {
                v.Add(pair.Key, pair.Value);
            }
            foreach (KeyValuePair<long, F> pair in t)
            {
                long key = pair.Key;
                if (v.ContainsKey(key))
                {
                    v[key] = Add(v[key], pair.Value);
                }
                else
                {
                    v[key] = pair.Value;
                }
            }
            return result;
        }
        public static SparseTensor<int> AddElementwise(this SparseTensor<int> S, SparseTensor<int> T) => AddElementwise(S, T, (s, t) => s + t);
        public static SparseTensor<long> AddElementwise(this SparseTensor<long> S, SparseTensor<long> T) => AddElementwise(S, T, (s, t) => s + t);
        public static SparseTensor<float> AddElementwise(this SparseTensor<float> S, SparseTensor<float> T) => AddElementwise(S, T, (s, t) => s + t);
        public static SparseTensor<double> AddElementwise(this SparseTensor<double> S, SparseTensor<double> T) => AddElementwise(S, T, (s, t) => s + t);
        public static SparseTensor<decimal> AddElementwise(this SparseTensor<decimal> S, SparseTensor<decimal> T) => AddElementwise(S, T, (s, t) => s + t);
        public static SparseTensor<G> AddElementwise<G>(this SparseTensor<G> S, SparseTensor<G> T) where G : AdditiveGroup<G>, new() => AddElementwise(S, T, (s, t) => s.Add(t));


        private static SparseTensor<F> SubtractElementwise<F>(this SparseTensor<F> S, SparseTensor<F> T, Func<F, F, F> Subtract, Func<F, F> Negate) where F : new()
        {
            TensorChecks.CheckDimensionsMatch(S, T, out List<int> dimensions);

            SparseTensor<F> result = new SparseTensor<F>(dimensions.ToArray());
            Dictionary<long, F> v = result.Values, s = S.Values, t = T.Values;

            foreach (KeyValuePair<long, F> pair in s)
            {
                v.Add(pair.Key, pair.Value);
            }
            foreach (KeyValuePair<long, F> pair in t)
            {
                long key = pair.Key;
                if (v.ContainsKey(key))
                {
                    v[key] = Subtract(v[key], pair.Value);
                }
                else
                {
                    v[key] = Negate(pair.Value);
                }
            }
            return result;
        }
        public static SparseTensor<int> SubtractElementwise(this SparseTensor<int> S, SparseTensor<int> T) => SubtractElementwise(S, T, (s, t) => s - t, s => -s);
        public static SparseTensor<long> SubtractElementwise(this SparseTensor<long> S, SparseTensor<long> T) => SubtractElementwise(S, T, (s, t) => s - t, s => -s);
        public static SparseTensor<float> SubtractElementwise(this SparseTensor<float> S, SparseTensor<float> T) => SubtractElementwise(S, T, (s, t) => s - t, s => -s);
        public static SparseTensor<double> SubtractElementwise(this SparseTensor<double> S, SparseTensor<double> T) => SubtractElementwise(S, T, (s, t) => s - t, s => -s);
        public static SparseTensor<decimal> SubtractElementwise(this SparseTensor<decimal> S, SparseTensor<decimal> T) => SubtractElementwise(S, T, (s, t) => s - t, s => -s);
        public static SparseTensor<G> SubtractElementwise<G>(this SparseTensor<G> S, SparseTensor<G> T) where G : AdditiveGroup<G>, new() => SubtractElementwise(S, T, (s, t) => s.Subtract(t), s => s.AdditiveInverse());

        private static SparseTensor<F> MultiplyElementwise<F>(this SparseTensor<F> S, SparseTensor<F> T, Func<F, F, F> Multiply) where F : new()
        {
            TensorChecks.CheckDimensionsMatch(S, T, out List<int> dimensions);

            SparseTensor<F> result = new SparseTensor<F>(dimensions.ToArray());
            Dictionary<long, F> v = result.Values, s = S.Values, t = T.Values;
            foreach (KeyValuePair<long, F> pair in s)
            {
                if (t.ContainsKey(pair.Key))
                {
                    v.Add(pair.Key, Multiply(pair.Value, t[pair.Key]));
                }
            }
            return result;
        }
        public static SparseTensor<int> MultiplyElementwise(this SparseTensor<int> S, SparseTensor<int> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<long> MultiplyElementwise(this SparseTensor<long> S, SparseTensor<long> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<float> MultiplyElementwise(this SparseTensor<float> S, SparseTensor<float> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<double> MultiplyElementwise(this SparseTensor<double> S, SparseTensor<double> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<decimal> MultiplyElementwise(this SparseTensor<decimal> S, SparseTensor<decimal> T) => MultiplyElementwise(S, T, (s, t) => s * t);
        public static SparseTensor<G> MultiplyElementwise<G>(this SparseTensor<G> S, SparseTensor<G> T) where G : Ring<G>, new() => MultiplyElementwise(S, T, (s, t) => s.Multiply(t));

        public static SparseTensor<int> TensorProduct(this SparseTensor<int> S, SparseTensor<int> T) => S.TensorProduct(T, (s, t) => s * t);
        public static SparseTensor<long> TensorProduct(this SparseTensor<long> S, SparseTensor<long> T) => S.TensorProduct(T, (s, t) => s * t);
        public static SparseTensor<float> TensorProduct(this SparseTensor<float> S, SparseTensor<float> T) => S.TensorProduct(T, (s, t) => s * t);
        public static SparseTensor<double> TensorProduct(this SparseTensor<double> S, SparseTensor<double> T) => S.TensorProduct(T, (s, t) => s * t);
        public static SparseTensor<decimal> TensorProduct(this SparseTensor<decimal> S, SparseTensor<decimal> T) => S.TensorProduct(T, (s, t) => s * t);
        public static SparseTensor<G> TensorProduct<G>(this SparseTensor<G> S, SparseTensor<G> T) where G : Ring<G>, new() => S.TensorProduct(T, (s, t) => s.Multiply(t));
    }
}
