﻿using LinearLite.Structs;
using LinearLite.Structs.Tensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tensors
{
    public static class TensorBooleanExtensions
    {
        public static bool ExactlyEquals(this DenseTensor<int> S, DenseTensor<int> T) => S.Equals(T, (a, b) => a == b);
        public static bool ExactlyEquals(this DenseTensor<long> S, DenseTensor<long> T) => S.Equals(T, (a, b) => a == b);
        public static bool ApproximatelyEquals(this DenseTensor<float> S, DenseTensor<float> T, float tolerance = Precision.FLOAT_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this DenseTensor<double> S, DenseTensor<double> T, double tolerance = Precision.DOUBLE_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this DenseTensor<decimal> S, DenseTensor<decimal> T, decimal tolerance = Precision.DECIMAL_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals<F>(this DenseTensor<F> S, DenseTensor<F> T, double tolerance = Precision.DOUBLE_PRECISION) where F : Algebra<F>, new() => S.Equals(T, (a, b) => a.ApproximatelyEquals(b, tolerance));


        public static bool ExactlyEquals(this SparseTensor<int> S, SparseTensor<int> T) => S.Equals(T, (a, b) => a == b);
        public static bool ExactlyEquals(this SparseTensor<long> S, SparseTensor<long> T) => S.Equals(T, (a, b) => a == b);
        public static bool ApproximatelyEquals(this SparseTensor<float> S, SparseTensor<float> T, float tolerance = Precision.FLOAT_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this SparseTensor<double> S, SparseTensor<double> T, double tolerance = Precision.DOUBLE_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this SparseTensor<decimal> S, SparseTensor<decimal> T, decimal tolerance = Precision.DECIMAL_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals<F>(this SparseTensor<F> S, SparseTensor<F> T, double tolerance = Precision.DOUBLE_PRECISION) where F : Algebra<F>, new() => S.Equals(T, (a, b) => a.ApproximatelyEquals(b, tolerance));


        public static bool ExactlyEquals(this SparseTensor<int> S, DenseTensor<int> T) => S.Equals(T, (a, b) => a == b);
        public static bool ExactlyEquals(this SparseTensor<long> S, DenseTensor<long> T) => S.Equals(T, (a, b) => a == b);
        public static bool ApproximatelyEquals(this SparseTensor<float> S, DenseTensor<float> T, float tolerance = Precision.FLOAT_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this SparseTensor<double> S, DenseTensor<double> T, double tolerance = Precision.DOUBLE_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this SparseTensor<decimal> S, DenseTensor<decimal> T, decimal tolerance = Precision.DECIMAL_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals<F>(this SparseTensor<F> S, DenseTensor<F> T, double tolerance = Precision.DOUBLE_PRECISION) where F : Algebra<F>, new() => S.Equals(T, (a, b) => a.ApproximatelyEquals(b, tolerance));


        public static bool ExactlyEquals(this DenseTensor<int> S, SparseTensor<int> T) => S.Equals(T, (a, b) => a == b);
        public static bool ExactlyEquals(this DenseTensor<long> S, SparseTensor<long> T) => S.Equals(T, (a, b) => a == b);
        public static bool ApproximatelyEquals(this DenseTensor<float> S, SparseTensor<float> T, float tolerance = Precision.FLOAT_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this DenseTensor<double> S, SparseTensor<double> T, double tolerance = Precision.DOUBLE_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this DenseTensor<decimal> S, SparseTensor<decimal> T, decimal tolerance = Precision.DECIMAL_PRECISION) => S.Equals(T, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals<F>(this DenseTensor<F> S, SparseTensor<F> T, double tolerance = Precision.DOUBLE_PRECISION) where F : Algebra<F>, new() => S.Equals(T, (a, b) => a.ApproximatelyEquals(b, tolerance));

    }
}
