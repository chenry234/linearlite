﻿using LinearLite.Structs.Tensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tensors
{
    public static class TensorChecks
    {
        /// <summary>
        /// Checks if the tensors' dimensions are equal. 
        /// Note that this method only performs shallow checking. Only use for internal methods where 
        /// the tensors are guaranteed to be rectangular (or 'cuboid')
        /// </summary>
        /// <typeparam name="K"></typeparam>
        /// <param name="S"></param>
        /// <param name="T"></param>
        internal static void CheckDimensionsMatch<K>(K[][][] S, K[][][] T)
        {
            if (S.Length != T.Length || S[0].Length != T[0].Length || S[0][0].Length != T[0][0].Length)
            {
                throw new InvalidOperationException(
                    $"Tensor dimensions don't match: {S.Length} x {S[0].Length} x {S[0][0].Length} vs {T.Length} x {T[0].Length} x {T[0][0].Length}");
            }
        }
        internal static void CheckDimensionsMatch<T>(ITensor<T> t1, ITensor<T> t2, out List<int> dimensions)
        {
            if (t1 == null || t2 == null)
            {
                throw new ArgumentNullException("Tensors cannot be null.");
            }

            List<int> dim1 = t1.GetDimensions(), dim2 = t2.GetDimensions();
            if (dim1.Count != dim2.Count)
            {
                throw new InvalidOperationException("Dimensions of tensors don't match.");
            }

            int order = dim1.Count, i;
            for (i = 0; i < order; ++i)
            {
                if (dim1[i] != dim2[i])
                {
                    throw new InvalidOperationException("Dimensions of tensors don't match.");
                }
            }

            dimensions = dim1;
        }
    }
}
