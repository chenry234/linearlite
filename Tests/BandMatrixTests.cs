﻿using LinearLite.Matrices;
using LinearLite.Matrices.Band;
using LinearLite.Structs;
using LinearLite.Structs.Matrix;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tests
{
    public static class BandMatrixTests
    {
        public static void BandMatrixCreateTests(bool verbose = false)
        {
            if (verbose)
            {
                BandMatrix<double> matrix1 = BandMatrix.Random<double>(10, 10, 2, 2);
                matrix1.Print();

                BandMatrix<double> matrix2 = BandMatrix.Random<double>(10, 15, 2, 3);
                matrix2.Print();

                BandMatrix<double> matrix3 = BandMatrix.Random<double>(15, 10, 2, 3);
                matrix3.Print();

                BandMatrix<double> matrix4 = BandMatrix.Random<double>(13, 15, 12, 10);
                matrix4.Print();
            }
        }
        public static void BandMatrixAdditionTest(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 10, 2, 2);
            BandMatrix<double> B = BandMatrix.Random<double>(10, 10, 2, 2);

            DenseMatrix<double> _A = A.ToDense();
            DenseMatrix<double> _B = B.ToDense();

            DenseMatrix<double> _A_B = _A.Add(_B);
            DenseMatrix<double> A_B = A.Add(B).ToDense();
            
            if (verbose)
            {
                _A_B.Print();
                A_B.Print();
            }
            Debug.Assert(_A_B.ApproximatelyEquals(A_B));
        }
        public static void BandMatrixSubtractionTest(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 10, 2, 2);
            BandMatrix<double> B = BandMatrix.Random<double>(10, 10, 2, 2);

            DenseMatrix<double> _A = A.ToDense();
            DenseMatrix<double> _B = B.ToDense();

            DenseMatrix<double> _A_B = _A.Subtract(_B);
            DenseMatrix<double> A_B = A.Subtract(B).ToDense();

            if (verbose)
            {
                _A_B.Print();
                A_B.Print();
            }
            Debug.Assert(_A_B.ApproximatelyEquals(A_B));
        }
        public static void BandMatrixMultiplicationTest(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 10, 3, 2);
            BandMatrix<double> B = BandMatrix.Random<double>(10, 15, 2, 2);

            DenseMatrix<double> _AB = A.ToDense().Multiply(B.ToDense());
            DenseMatrix<double> AB = A.Multiply(B).ToDense();
            DenseMatrix<double> pAB = A.MultiplyParallel(B).ToDense();

            if (verbose)
            {
                _AB.Print();
                AB.Print();
                pAB.Print();
            }
            Debug.Assert(AB.ApproximatelyEquals(_AB));
            if (!AB.ApproximatelyEquals(pAB))
            {
                Debug.WriteLine("A");
                A.Print();
                Debug.WriteLine("B");
                B.Print();
                Debug.WriteLine("AB");
                AB.Print();
                Debug.WriteLine("AB");
                pAB.Print();

                Debug.Assert(AB.ApproximatelyEquals(pAB));
            }

        }
        public static void BandMatrixMultiplicationVectorTest(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 15, 3, 2);
            double[] v = RectangularVector.Random<double>(15);

            double[] Av = A.Multiply(v);
            double[] _Av = A.ToDense().Multiply(v);

            if (verbose)
            {
                Av.Print();
                _Av.Print();
            }
            Debug.Assert(Av.ApproximatelyEquals(_Av));
        }
        public static void BandMatrixTranspositionTest(bool verbose = false)
        {
            BandMatrix<double> A = BandMatrix.Random<double>(10, 15, 2, 3);

            DenseMatrix<double> _At = A.ToDense().Transpose();
            DenseMatrix<double> At = A.Transpose().ToDense();

            if (verbose)
            {
                At.Print();
                _At.Print();
            }
            Debug.Assert(_At.ApproximatelyEquals(At));
        }
        public static void BandMatrixRowTest(bool verbose = false)
        {
            if (verbose)
            {
                BandMatrix<double> A = BandMatrix.Random<double>(10, 15, 3, 2);
                A.Print();

                for (int i = 0; i < 10; ++i)
                {
                    A.Row(i).Print();
                }
            }
        }
    }
}
