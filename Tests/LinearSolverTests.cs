﻿using LinearLite.Solvers;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tests
{
    public static class LinearSolverTests
    {
        public static void RealLinearSystemTest()
        {
            int nEquations = 100, variables = 100;

            double[,] A = RectangularMatrix.Random<double>(nEquations, variables);
            double[] b = RectangularVector.Random<double>(nEquations);

            Solution<double[]> solution = LinearSolver.Solve(A, b);
            if (solution.Success)
            {
                Debug.Assert(A.Multiply(solution.Result).ApproximatelyEquals(b));
            }
        }
        public static void ComplexLinearSystemTest()
        {
            int nEquations = 300, variables = 300;

            Complex[,] A = RectangularMatrix.Random<Complex>(nEquations, variables);
            Complex[] b = RectangularVector.Random<Complex>(nEquations);

            Solution<Complex[]> solution = LinearSolver.Solve(A, b);
            if (solution.Success)
            {
                Debug.Assert(A.Multiply(solution.Result).ApproximatelyEquals(b));   
            }
        }
    }
}
