﻿using LinearLite.Solvers;
using LinearLite.Structs;
using LinearLite.Structs.Rings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tests
{
    public static class PolynomialTests
    {
        public static void RootFinderTests()
        {
            int n = 20;

            ComplexPolynomial px = new ComplexPolynomial(1);
            for (double root = -2; root < 2; root += 2.0 / n)
            {
                px = (ComplexPolynomial)px.Multiply(new ComplexPolynomial(1, root));
            }

            Debug.WriteLine(px);

            AberthMethodSolver solver = new AberthMethodSolver(10000);

            Stopwatch sw = new Stopwatch();
            sw.Start();
            Complex[] roots = solver.Solve(px);
            sw.Stop();

            foreach (Complex root in roots)
            {
                Debug.WriteLine(root);
            }
            Debug.WriteLine("Time:\t" + sw.ElapsedMilliseconds);
        }
    }
}
