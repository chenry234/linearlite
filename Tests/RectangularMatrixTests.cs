﻿using LinearLite.Decomposition;
using LinearLite.Matrices;
using LinearLite.Matrices.Strassen;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace LinearLite.Tests
{
    public class RectangularMatrixTests
    {
        public static void RealMatrixInversionTest(bool verbose = false)
        {
            int n = 20;

            DenseMatrix<float> A_f = RectangularMatrix.RandomFloat(n, n, -100.0f, 100.0f);

            DenseMatrix<float> I_f = A_f.Invert().Multiply(A_f);
            Debug.Assert(I_f.ApproximatelyEquals(RectangularMatrix.Identity<float>(n), 1e-2f));
            if (verbose)
            {
                I_f.Print();
                Debug.WriteLine("float[,]\t|AA^T - I|:\t" + I_f.Subtract(RectangularMatrix.Identity<float>(n)).ElementwiseNorm(1, 1));
            }

            DenseMatrix<double> A_d = RectangularMatrix.RandomDouble(n, n, -100.0, 100.0);
            DenseMatrix<double> I_d = A_d.Invert().Multiply(A_d);
            Debug.Assert(I_d.ApproximatelyEquals(RectangularMatrix.Identity<double>(n)));
            if (verbose)
            {
                I_d.Print();
                Debug.WriteLine("double[,]\t|AA^T - I|:\t" + I_d.Subtract(RectangularMatrix.Identity<double>(n)).ElementwiseNorm(1, 1));
            }

            DenseMatrix<decimal> A_m = RectangularMatrix.RandomDecimal(n, n, -100.0m, 100.0m);
            DenseMatrix<decimal> I_m = A_m.Invert().Multiply(A_m);
            Debug.Assert(I_m.ApproximatelyEquals(RectangularMatrix.Identity<decimal>(n), 1e-8m));
            if (verbose)
            {
                I_m.Print();
                Debug.WriteLine("decimal[,]\t|AA^T - I|:\t" + I_m.Subtract(RectangularMatrix.Identity<decimal>(n)).ElementwiseNorm(1, 1));
            }
        }
        public static void RealStrassenInversionTest(bool verbose = false)
        {
            int n = 8;

            DenseMatrix<double> A = DenseMatrix.Random<double>(n, n);
            DenseMatrix<double> A_inv = A.Invert();

            StrassenInversionAlgorithm<double> strassen = new StrassenInversionAlgorithm<double>(n, new DoubleStrassenInstructionSet());

            DenseMatrix<double> _A = A.Clone() as DenseMatrix<double>;
            double[][] _A_inv = MatrixInternalExtensions.JMatrix<double>(n, n);
            strassen.Invert(_A.Values, _A_inv);

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("A^-1");
                A_inv.Print();

                Debug.WriteLine("A * A^-1");
                A.Multiply(A_inv).Print();

                // The last 

                Debug.WriteLine("_A");
                _A.Print();

                Debug.WriteLine("_A^-1");
                _A_inv.ToRectangular().Print();

                Debug.WriteLine("_A * _A^-1");
                _A.Multiply(_A_inv).Print();
            }

            Debug.Assert(A.Multiply(A_inv).ApproximatelyEquals(RectangularMatrix.Identity<double>(n)));
            Debug.Assert(_A.Multiply(_A_inv).ApproximatelyEquals(RectangularMatrix.Identity<double>(n)));
        }
        public static void ComplexMatrixInversionTest(bool verbose)
        {
            int n = 200;
            DenseMatrix<Complex> matrix = RectangularMatrix.Random<Complex>(n, n);
            DenseMatrix<Complex> inverse = matrix.Invert();
            DenseMatrix<Complex> identity = RectangularMatrix.Identity<Complex>(n);

            if (verbose)
            {
                Debug.WriteLine("Matrix");
                matrix.Print();

                Debug.WriteLine("Inverse");
                inverse.Print();

                Debug.WriteLine("Product");
                matrix.Multiply(inverse).Print();
            }

            Debug.Assert(inverse.Multiply(matrix).ApproximatelyEquals(identity));
            Debug.Assert(matrix.Multiply(inverse).ApproximatelyEquals(identity));
        }

        private static void SquareStrassenMultiplicationTest<T>(int n, Func<T[,], T[,], T[,]> Multiply, Func<T[,], T[,], bool> Equals, IStrassenInstructionSet<T> instructions, bool verbose = false) where T : new()
        {
            T[,] A = RectangularMatrix.Random<T>(n, n), B = RectangularMatrix.Random<T>(n, n);

            T[,] AB_strassen = new T[n, n], AB_winograd = new T[n, n];
            T[,] AB = Multiply(A, B);

            var strassen = new SquareStrassenMultiplication<T>(instructions, n, false);
            strassen.Multiply(A, B, AB_strassen);

            var winograd = new SquareStrassenMultiplication<T>(instructions, n, true);
            winograd.Multiply(A, B, AB_winograd);

            Debug.WriteLine($"Testing Strassen Multiplication for {typeof(T).ToString()}[,]");
            if (verbose)
            {
                Debug.WriteLine("AB");
                AB.Print();

                Debug.WriteLine("AB strassen - original");
                AB_strassen.Print();

                Debug.WriteLine("AB strassen - winograd");
                AB_winograd.Print();
            }

            Debug.Assert(Equals(AB, AB_strassen), "AB != AB Strassen");
            Debug.Assert(Equals(AB, AB_winograd), "AB != AB Winograd");
        }
        public static void SquareStrassenMultiplicationTest(bool verbose = false)
        {
            int n = 128;

            List<Type> types = new List<Type>()
            {
                typeof(int),
                typeof(long),
                typeof(double),
                typeof(float),
                typeof(decimal),
                typeof(Complex),
                typeof(Rational)
            };

            if (types.Contains(typeof(float)))
            {
                SquareStrassenMultiplicationTest<float>(n, (A, B) => A.Multiply(B), (A, B) => A.ApproximatelyEquals(B, 1e-1f), new FloatStrassenInstructionSet(), verbose);
            }
            if (types.Contains(typeof(double)))
            {
                SquareStrassenMultiplicationTest<double>(n, (A, B) => A.Multiply(B), (A, B) => A.ApproximatelyEquals(B, 1e-6), new DoubleStrassenInstructionSet(), verbose);
            }
            if (types.Contains(typeof(decimal)))
            {
                SquareStrassenMultiplicationTest<decimal>(n, (A, B) => A.Multiply(B), (A, B) => A.ApproximatelyEquals(B, 1e-8m), new DecimalStrassenInstructionSet(), verbose);
            }
            if (types.Contains(typeof(int)))
            {
                SquareStrassenMultiplicationTest<int>(n, (A, B) => A.Multiply(B), (A, B) => A.ExactlyEquals(B), new Int32StrassenInstructionSet(), verbose);
            }
            if (types.Contains(typeof(long)))
            {
                SquareStrassenMultiplicationTest<long>(n, (A, B) => A.Multiply(B), (A, B) => A.ExactlyEquals(B), new Int64StrassenInstructionSet(), verbose);
            }
            if (types.Contains(typeof(Complex)))
            {
                SquareStrassenMultiplicationTest<Complex>(n, (A, B) => A.Multiply(B), (A, B) => A.ApproximatelyEquals(B, 1e-6), new ComplexStrassenInstructionSet(), verbose);
            }
        }
        public static void ParallelStrassenMultiplicationTest()
        {
            throw new NotImplementedException();
        }

        public static void BidiagonalFormGKLTest(bool verbose = false)
        {
            int m = 5, n = 5;
            bool upper = true;
            List<Type> tests = new List<Type>() {
                typeof(double),
                typeof(float),
                typeof(decimal),
                typeof(Complex)
            };

            if (tests.Contains(typeof(double)))
            {
                // double type
                double[,] matrix = RectangularMatrix.Random<double>(m, n);
                matrix.ToBidiagonalForm(out double[,] U, out double[,] B, out double[,] V, upper, BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrix.Print();

                    Debug.WriteLine("U^TBV");
                    U.Transpose().Multiply(B).Multiply(V).Print();

                    Debug.WriteLine("U^TU");
                    U.Transpose().Multiply(U).Print();

                    Debug.WriteLine("V^TV");
                    V.Transpose().Multiply(V).Print();

                    Debug.WriteLine("B");
                    B.Print();
                }

                Debug.Assert(U.Transpose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<double>(m), 1e-4));
                Debug.Assert(V.Transpose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<double>(n), 1e-4));
                Debug.Assert(U.Transpose().Multiply(B).Multiply(V).ApproximatelyEquals(matrix, 1e-4));
            }
            
            if (tests.Contains(typeof(float)))
            {
                // float type
                float[,] matrixf = RectangularMatrix.Random<float>(m, n);
                matrixf.ToBidiagonalForm(out float[,] Uf, out float[,] Bf, out float[,] Vf, upper, BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrixf.Print();

                    Debug.WriteLine("U^TBV");
                    Uf.Transpose().Multiply(Bf).Multiply(Vf).Print();

                    Debug.WriteLine("U^TU");
                    Uf.Transpose().Multiply(Uf).Print();

                    Debug.WriteLine("V^TV");
                    Vf.Transpose().Multiply(Vf).Print();

                    Debug.WriteLine("B");
                    Bf.Print();
                }

                Debug.Assert(Uf.Transpose().Multiply(Uf).ApproximatelyEquals(RectangularMatrix.Identity<float>(m), 1e-1f));
                Debug.Assert(Vf.Transpose().Multiply(Vf).ApproximatelyEquals(RectangularMatrix.Identity<float>(n), 1e-1f));
                Debug.Assert(Uf.Transpose().Multiply(Bf).Multiply(Vf).ApproximatelyEquals(matrixf, 1e-1f));
            }
            
            if (tests.Contains(typeof(decimal)))
            {
                // decimal type
                decimal[,] matrixd = RectangularMatrix.Random<decimal>(m, n);
                matrixd.ToBidiagonalForm(out decimal[,] Ud, out decimal[,] Bd, out decimal[,] Vd, upper, BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrixd.Print();

                    Debug.WriteLine("U^TBV");
                    Ud.Transpose().Multiply(Bd).Multiply(Vd).Print();

                    Debug.WriteLine("U^TU");
                    Ud.Transpose().Multiply(Ud).Print();

                    Debug.WriteLine("V^TV");
                    Vd.Transpose().Multiply(Vd).Print();

                    Debug.WriteLine("B");
                    Bd.Print();
                }

                Debug.Assert(Ud.Transpose().Multiply(Ud).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(m), 1e-2m));
                Debug.Assert(Vd.Transpose().Multiply(Vd).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(n), 1e-2m));
                Debug.Assert(Ud.Transpose().Multiply(Bd).Multiply(Vd).ApproximatelyEquals(matrixd, 1e-2m));
            }
            
            if (tests.Contains(typeof(Complex)))
            {
                // complex type
                Complex[,] matrixc = RectangularMatrix.Random<Complex>(m, n);
                matrixc.ToBidiagonalForm(out Complex[,] Uc, out Complex[,] Bc, out Complex[,] Vc, upper, BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS);

                if (verbose)
                {
                    Debug.WriteLine("A");
                    matrixc.Print();

                    Debug.WriteLine("U^TBV");
                    Uc.ConjugateTranspose().Multiply(Bc).Multiply(Vc).Print();

                    Debug.WriteLine("U^TU");
                    Uc.ConjugateTranspose().Multiply(Uc).Print();

                    Debug.WriteLine("V^TV");
                    Vc.ConjugateTranspose().Multiply(Vc).Print();

                    Debug.WriteLine("B");
                    Bc.Print();
                }

                Debug.Assert(Uc.ConjugateTranspose().Multiply(Uc).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(m), 1e-4));
                Debug.Assert(Vc.ConjugateTranspose().Multiply(Vc).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(n), 1e-4));
                Debug.Assert(Uc.ConjugateTranspose().Multiply(Bc).Multiply(Vc).ApproximatelyEquals(matrixc, 1e-4));
            }
        }
        public static void BidiagonalHouseholderFormTest(bool verbose = false)
        {
            int m = 10, n = 7;

            List<Type> tests = new List<Type>()
            {
                typeof(double),
                //typeof(float),
                typeof(decimal),
                typeof(Complex)
            };

            if (tests.Contains(typeof(double)))
            {
                double[,] matrix = RectangularMatrix.Random<double>(m, n);
                matrix.ToBidiagonalForm(out double[,] U, out double[,] B, out double[,] V, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Starting tests for type: " + typeof(double));
                    Debug.WriteLine("matrix:");
                    matrix.Print();

                    Debug.WriteLine("U^TBV");
                    U.Transpose().Multiply(B).Multiply(V).Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("U^TU");
                    U.Transpose().Multiply(U).Print();

                    Debug.WriteLine("V^TV");
                    V.Transpose().Multiply(V).Print();
                }

                Debug.Assert(matrix.ApproximatelyEquals(U.Transpose().Multiply(B).Multiply(V)));
                Debug.Assert(U.Transpose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<double>(m)));
                Debug.Assert(V.Transpose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<double>(n)));
            }
            if (tests.Contains(typeof(float)))
            {
                float[,] matrix = RectangularMatrix.Random<float>(m, n);
                matrix.ToBidiagonalForm(out float[,] U, out float[,] B, out float[,] V, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Starting tests for type: " + typeof(float));
                    Debug.WriteLine("matrix:");
                    matrix.Print();

                    Debug.WriteLine("U^TBV");
                    U.Transpose().Multiply(B).Multiply(V).Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("U^TU");
                    U.Transpose().Multiply(U).Print();

                    Debug.WriteLine("V^TV");
                    V.Transpose().Multiply(V).Print();
                }

                Debug.Assert(matrix.ApproximatelyEquals(U.Transpose().Multiply(B).Multiply(V), 0.5f));
                Debug.Assert(U.Transpose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<float>(m), 0.5f));
                Debug.Assert(V.Transpose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<float>(n), 0.5f));
            }
            if (tests.Contains(typeof(decimal)))
            {
                decimal[,] matrix = RectangularMatrix.Random<decimal>(m, n);
                matrix.ToBidiagonalForm(out decimal[,] U, out decimal[,] B, out decimal[,] V, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Starting tests for type: " + typeof(decimal));
                    Debug.WriteLine("matrix:");
                    matrix.Print();

                    Debug.WriteLine("U^TBV");
                    U.Transpose().Multiply(B).Multiply(V).Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("U^TU");
                    U.Transpose().Multiply(U).Print();

                    Debug.WriteLine("V^TV");
                    V.Transpose().Multiply(V).Print();
                }

                Debug.Assert(matrix.ApproximatelyEquals(U.Transpose().Multiply(B).Multiply(V), 1e-12m));
                Debug.Assert(U.Transpose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(m), 1e-12m));
                Debug.Assert(V.Transpose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(n), 1e-12m));
            }
            if (tests.Contains(typeof(Complex)))
            {
                Complex[,] matrix = RectangularMatrix.Random<Complex>(m, n);
                matrix.ToBidiagonalForm(out Complex[,] U, out Complex[,] B, out Complex[,] V, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Starting tests for type: " + typeof(Complex));
                    Debug.WriteLine("matrix:");
                    matrix.Print();

                    Debug.WriteLine("U*BV");
                    U.ConjugateTranspose().Multiply(B).Multiply(V).Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("U*U");
                    U.ConjugateTranspose().Multiply(U).Print();

                    Debug.WriteLine("V*V");
                    V.ConjugateTranspose().Multiply(V).Print();
                }

                Debug.Assert(matrix.ApproximatelyEquals(U.ConjugateTranspose().Multiply(B).Multiply(V), 1e-8));
                Debug.Assert(U.ConjugateTranspose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(m), 1e-8));
                Debug.Assert(V.ConjugateTranspose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(n), 1e-8));
            }
        }
        public static void BidiagonalHouseholderFormParallelTest(bool verbose = false)
        {
            int m = 15, n = 10;

            List<Type> tests = new List<Type>()
            {
                typeof(double),
                typeof(float),
                typeof(decimal),
                typeof(Complex)
            };

            if (tests.Contains(typeof(double)))
            {
                double[,] matrix = RectangularMatrix.Random<double>(m, n);
                matrix.ToBidiagonalFormParallel(out double[,] U, out double[,] B, out double[,] V, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Starting tests for type: " + typeof(double));
                    Debug.WriteLine("matrix:");
                    matrix.Print();

                    Debug.WriteLine("U^TBV");
                    U.Transpose().Multiply(B).Multiply(V).Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("U^TU");
                    U.Transpose().Multiply(U).Print();

                    Debug.WriteLine("V^TV");
                    V.Transpose().Multiply(V).Print();
                }

                Debug.Assert(matrix.ApproximatelyEquals(U.Transpose().Multiply(B).Multiply(V)));
                Debug.Assert(U.Transpose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<double>(m)));
                Debug.Assert(V.Transpose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<double>(n)));
            }
            if (tests.Contains(typeof(float)))
            {
                float[,] matrix = RectangularMatrix.Random<float>(m, n);
                matrix.ToBidiagonalFormParallel(out float[,] U, out float[,] B, out float[,] V, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Starting tests for type: " + typeof(float));
                    Debug.WriteLine("matrix:");
                    matrix.Print();

                    Debug.WriteLine("U^TBV");
                    U.Transpose().Multiply(B).Multiply(V).Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("U^TU");
                    U.Transpose().Multiply(U).Print();

                    Debug.WriteLine("V^TV");
                    V.Transpose().Multiply(V).Print();
                }

                Debug.Assert(matrix.ApproximatelyEquals(U.Transpose().Multiply(B).Multiply(V), 1e-3f));
                Debug.Assert(U.Transpose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<float>(m), 1e-3f));
                Debug.Assert(V.Transpose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<float>(n), 1e-3f));
            }
            if (tests.Contains(typeof(decimal)))
            {
                decimal[,] matrix = RectangularMatrix.Random<decimal>(m, n);
                matrix.ToBidiagonalFormParallel(out decimal[,] U, out decimal[,] B, out decimal[,] V, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Starting tests for type: " + typeof(decimal));
                    Debug.WriteLine("matrix:");
                    matrix.Print();

                    Debug.WriteLine("U^TBV");
                    U.Transpose().Multiply(B).Multiply(V).Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("U^TU");
                    U.Transpose().Multiply(U).Print();

                    Debug.WriteLine("V^TV");
                    V.Transpose().Multiply(V).Print();
                }

                Debug.Assert(matrix.ApproximatelyEquals(U.Transpose().Multiply(B).Multiply(V), 1e-12m));
                Debug.Assert(U.Transpose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(m), 1e-12m));
                Debug.Assert(V.Transpose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(n), 1e-12m));
            }
            if (tests.Contains(typeof(Complex)))
            {
                Complex[,] matrix = RectangularMatrix.Random<Complex>(m, n);
                matrix.ToBidiagonalFormParallel(out Complex[,] U, out Complex[,] B, out Complex[,] V, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Starting tests for type: " + typeof(Complex));
                    Debug.WriteLine("matrix:");
                    matrix.Print();

                    Debug.WriteLine("U*BV");
                    U.ConjugateTranspose().Multiply(B).Multiply(V).Print();

                    Debug.WriteLine("B");
                    B.Print();

                    Debug.WriteLine("U*U");
                    U.ConjugateTranspose().Multiply(U).Print();

                    Debug.WriteLine("V*V");
                    V.ConjugateTranspose().Multiply(V).Print();
                }

                Debug.Assert(matrix.ApproximatelyEquals(U.ConjugateTranspose().Multiply(B).Multiply(V), 1e-8));
                Debug.Assert(U.ConjugateTranspose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(m), 1e-8));
                Debug.Assert(V.ConjugateTranspose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(n), 1e-8));
            }
        }
        public static void ComplexBidiagonalFormTest()
        {
            Complex[,] matrix = RectangularMatrix.Random<Complex>(5, 5);
            matrix.ToBidiagonalForm(out Complex[,] U, out Complex[,] B, out Complex[,] V, false, BidiagonalizationMethod.GOLUB_KAHAN_LANCZOS);

            Debug.WriteLine("Matrix");
            matrix.Print();

            Debug.WriteLine("B");
            B.Print();

            Debug.WriteLine("U*U");
            U.ConjugateTranspose().Multiply(U).Print();

            Debug.WriteLine("VV*");
            V.Multiply(V.ConjugateTranspose()).Print();

            Debug.WriteLine("U*BV");
            U.ConjugateTranspose().Multiply(B).Multiply(V).Print();
        }
        public static void ComplexHouseholderBidiagonalFormTest(bool verbose = false)
        {
            int m = 20, n = 10;
            Complex[,] matrix = RectangularMatrix.Random<Complex>(m, n);
            matrix.ToBidiagonalForm(out Complex[,] U, out Complex[,] B, out Complex[,] V, true, BidiagonalizationMethod.HOUSEHOLDER_TRANSFORM);

            if (verbose)
            {
                Debug.WriteLine("matrix:");
                matrix.Print();
                Debug.WriteLine("U*BV");
                U.ConjugateTranspose().Multiply(B).Multiply(V).Print();
                Debug.WriteLine("B");
                B.Print();
                Debug.WriteLine("U*U");
                U.ConjugateTranspose().Multiply(U).Print();
                Debug.WriteLine("V");
                V.Print();
                Debug.WriteLine("V*V");
                V.ConjugateTranspose().Multiply(V).Print();
            }

            Debug.Assert(matrix.ApproximatelyEquals(U.ConjugateTranspose().Multiply(B).Multiply(V)));
            Debug.Assert(U.ConjugateTranspose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(m)));
            Debug.Assert(V.ConjugateTranspose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(n)));
        }

        public static void RealTridiagonalFormTest(bool verbose = false)
        {
            int m = 15, n = 10;

            /*
            double[,] A = Matrix.Random(m, n);
            A.ToTridiagonalForm(out double[,] U, out double[,] D, out double[,] V);
            */

            float[,] A = RectangularMatrix.Random<float>(m, n);
            A.ToTridiagonalForm(out float[,] U, out float[,] D, out float[,] V);

            if (verbose)
            {
                Debug.WriteLine("D");
                D.Print();

                Debug.WriteLine("U^TU");
                U.Transpose().Multiply(U).Print();

                Debug.WriteLine("V^TV");
                V.Transpose().Multiply(V).Print();

                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("U^TDV");
                U.Transpose().Multiply(D).Multiply(V).Print();
            }

            float tolerance = 1e-3f;

            Debug.Assert(U.Transpose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<float>(m), tolerance));
            Debug.Assert(V.Transpose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<float>(n), tolerance));
            Debug.Assert(U.Transpose().Multiply(D).Multiply(V).ApproximatelyEquals(A, tolerance));
        }
        public static void ComplexTridiagonalFormTest(bool verbose = false)
        {
            int m = 15, n = 10;
            Complex[,] A = RectangularMatrix.Random<Complex>(m, n);
            A.ToTridiagonalForm(out Complex[,] U, out Complex[,] D, out Complex[,] V);

            if (verbose)
            {
                Debug.WriteLine("D");
                D.Print();

                Debug.WriteLine("U*U");
                U.ConjugateTranspose().Multiply(U).Print();

                Debug.WriteLine("V*V");
                V.ConjugateTranspose().Multiply(V).Print();

                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("U*DV");
                U.ConjugateTranspose().Multiply(D).Multiply(V).Print();
            }

            Debug.Assert(U.ConjugateTranspose().Multiply(U).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(m)));
            Debug.Assert(V.ConjugateTranspose().Multiply(V).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(n)));
            Debug.Assert(U.ConjugateTranspose().Multiply(D).Multiply(V).ApproximatelyEquals(A));
        }

        public static void RealHessenbergFormTest(bool verbose = false)
        {
            int n = 10;

            // double test
            double[,] A_d = RectangularMatrix.Random<double>(n, n);
            A_d.ToHessenbergForm(out double[,] Q_d, out double[,] H_d, true);
            if (verbose)
            {
                Debug.WriteLine("Q");
                Q_d.Print();

                Debug.WriteLine("H");
                H_d.Print();

                Debug.WriteLine("A");
                A_d.Print();

                Debug.WriteLine("Q^THQ");
                Q_d.Transpose().Multiply(H_d).Multiply(Q_d).Print();
            }
            Debug.Assert(Q_d.Transpose().Multiply(Q_d).ApproximatelyEquals(RectangularMatrix.Identity<double>(n)));
            Debug.Assert(Q_d.Transpose().Multiply(H_d).Multiply(Q_d).ApproximatelyEquals(A_d));

            // float test
            float[,] A_f = RectangularMatrix.Random<float>(n, n);
            A_f.ToHessenbergForm(out float[,] Q_f, out float[,] H_f);
            if (verbose)
            {
                Debug.WriteLine("Q");
                Q_f.Print();

                Debug.WriteLine("H");
                H_f.Print();

                Debug.WriteLine("A");
                A_f.Print();

                Debug.WriteLine("Q^THQ");
                Q_f.Transpose().Multiply(H_f).Multiply(Q_f).Print();
            }
            Debug.Assert(Q_f.Transpose().Multiply(Q_f).ApproximatelyEquals(RectangularMatrix.Identity<float>(n), 1e-3f));
            Debug.Assert(Q_f.Transpose().Multiply(H_f).Multiply(Q_f).ApproximatelyEquals(A_f, 1e-3f));

            // decimal test
            decimal[,] A_m = RectangularMatrix.Random<decimal>(n, n);
            A_m.ToHessenbergForm(out decimal[,] Q_m, out decimal[,] H_m);
            if (verbose)
            {
                Debug.WriteLine("Q");
                Q_m.Print();

                Debug.WriteLine("H");
                H_m.Print();

                Debug.WriteLine("A");
                A_m.Print();

                Debug.WriteLine("Q^THQ");
                Q_m.Transpose().Multiply(H_m).Multiply(Q_m).Print();
            }
            Debug.Assert(Q_m.Transpose().Multiply(Q_m).ApproximatelyEquals(RectangularMatrix.Identity<decimal>(n)));
            Debug.Assert(Q_m.Transpose().Multiply(H_m).Multiply(Q_m).ApproximatelyEquals(A_m));
        }
        public static void ComplexHessenbergFormTest(bool verbose = false)
        {
            int n = 10;
            Complex[,] A = RectangularMatrix.Random<Complex>(n, n);
            A.ToHessenbergForm(out Complex[,] Q, out Complex[,] H, true);

            if (verbose)
            {
                Debug.WriteLine("Q");
                Q.Print();

                Debug.WriteLine("H");
                H.Print();

                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("Q^THQ");
                Q.ConjugateTranspose().Multiply(H).Multiply(Q).Print();
            }

            A.ToHessenbergForm(out Complex[,] H0);

            if (verbose)
            {
                H0.Print();
            }

            Debug.Assert(Q.ConjugateTranspose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(n)));
            Debug.Assert(Q.ConjugateTranspose().Multiply(H).Multiply(Q).ApproximatelyEquals(A));
            Debug.Assert(H.ApproximatelyEquals(H0));
        }

        public static void LUDecompositionTest(bool verbose = false)
        {
            int n = 30;

            List<Type> types = new List<Type>()
            {
                typeof(float),
                typeof(double),
                typeof(decimal),
                typeof(Complex)
            };

            if (types.Contains(typeof(float)))
            {
                float[,] A = RectangularMatrix.Random<float>(n, n);
                A.LUDecompose(out float[,] L, out float[,] U);

                float[,] LU = L.Multiply(U);
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("LU");
                    LU.Print();

                    Debug.WriteLine("|LU - A|:\t" + LU.Subtract(A).ElementwiseNorm(1, 1));
                }

                Debug.Assert(L.IsLowerTriangular(1e-3f));
                Debug.Assert(U.IsUpperTriangular(1e-3f));
                Debug.Assert(LU.ApproximatelyEquals(A, 1e-2f));
            }
            if (types.Contains(typeof(double)))
            {
                double[,] A = RectangularMatrix.Random<double>(n, n);
                A.LUDecompose(out double[,] L, out double[,] U);

                double[,] LU = L.Multiply(U);
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("LU");
                    LU.Print();

                    Debug.WriteLine("|LU - A|:\t" + LU.Subtract(A).ElementwiseNorm(1, 1));
                }

                Debug.Assert(L.IsLowerTriangular());
                Debug.Assert(U.IsUpperTriangular());
                Debug.Assert(LU.ApproximatelyEquals(A));
            }
            if (types.Contains(typeof(decimal)))
            {
                decimal[,] A = RectangularMatrix.Random<decimal>(n, n);
                A.LUDecompose(out decimal[,] L, out decimal[,] U);

                decimal[,] LU = L.Multiply(U);
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("LU");
                    LU.Print();

                    Debug.WriteLine("|LU - A|:\t" + LU.Subtract(A).ElementwiseNorm(1, 1));
                }

                Debug.Assert(L.IsLowerTriangular());
                Debug.Assert(U.IsUpperTriangular());
                Debug.Assert(LU.ApproximatelyEquals(A));
            }
            if (types.Contains(typeof(Complex)))
            {
                Complex[,] A = RectangularMatrix.Random<Complex>(n, n);
                A.LUDecompose(out Complex[,] L, out Complex[,] U);

                Complex[,] LU = L.Multiply(U);
                if (verbose)
                {
                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("L");
                    L.Print();

                    Debug.WriteLine("U");
                    U.Print();

                    Debug.WriteLine("LU");
                    LU.Print();

                    Debug.WriteLine("|LU - A|:\t" + LU.Subtract(A).ElementwiseNorm(1, 1));
                }

                Debug.Assert(L.IsLowerTriangular());
                Debug.Assert(U.IsUpperTriangular());
                Debug.Assert(LU.ApproximatelyEquals(A));
            }
        }
        public static void RealSVDecompositionTest()
        {
            double[,] matrix = RectangularMatrix.Random<double>(10, 10);

            matrix.SingularValueDecompose(out double[,] U, out double[,] D, out double[,] V);

            matrix.Print();

            U.Print();

            D.Print();

            V.Print();

            Debug.WriteLine("U^TU");
            U.Transpose().Multiply(U).Print();

            Debug.WriteLine("V^TV");
            V.Transpose().Multiply(V).Print();

            Debug.WriteLine("UDV");
            U.Multiply(D).Multiply(V).Print();

        }

        public static void RealQRDecompositionGramSchmidtTest()
        {
            int m = 5, n = 5;

            double[,] A = RectangularMatrix.Random<double>(m, n);
            A.QLDecompose(out double[,] Q, out double[,] R, false, QRDecompositionMethod.GRAM_SCHMIDT);

            Debug.WriteLine("Q");
            Q.Print();

            Debug.WriteLine("Q*Q");
            Q.Transpose().Multiply(Q).Print();

            Debug.WriteLine("R");
            R.Print();

            Debug.WriteLine("A");
            A.Print();

            Debug.WriteLine("QR");
            Q.Multiply(R).Print();

            Debug.Assert(A.ApproximatelyEquals(Q.Multiply(R)));
            Debug.Assert(Q.Transpose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<double>(m)));
        }
        public static void RealQRDecompositionTest(bool verbose = false)
        {
            int m = 15, n = 10;
            bool testQR = true, testQL = true;
            double[,] A = RectangularMatrix.Random<double>(m, n);

            if (testQR)
            {
                A.QRDecompose(out double[,] Q, out double[,] R, true, QRDecompositionMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Q");
                    Q.Print();

                    Debug.WriteLine("Q*Q");
                    Q.Transpose().Multiply(Q).Print();

                    Debug.WriteLine("R");
                    R.Print();

                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("QR");
                    Q.Multiply(R).Print();
                }
                Debug.Assert(A.ApproximatelyEquals(Q.Multiply(R)));
                Debug.Assert(Q.Transpose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<double>(m)));
            }
            if (testQL)
            {
                A.QLDecompose(out double[,] Q, out double[,] L, true, QRDecompositionMethod.HOUSEHOLDER_TRANSFORM);

                if (verbose)
                {
                    Debug.WriteLine("Q");
                    Q.Print();

                    Debug.WriteLine("Q*Q");
                    Q.Transpose().Multiply(Q).Print();

                    Debug.WriteLine("R");
                    L.Print();

                    Debug.WriteLine("A");
                    A.Print();

                    Debug.WriteLine("QR");
                    Q.Multiply(L).Print();
                }
                Debug.Assert(A.ApproximatelyEquals(Q.Multiply(L)));
                Debug.Assert(Q.Transpose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<double>(m)));
            }
        }
        public static void RealQRDecompositionParallelTest(bool verbose = false)
        {
            int m = 10, n = 5;

            double[,] A = RectangularMatrix.Random<double>(m, n);
            A.QRDecomposeParallel(out double[,] Q, out double[,] R, true);

            if (verbose)
            {
                Debug.WriteLine("Q");
                Q.Print();

                Debug.WriteLine("Q*Q");
                Q.Transpose().Multiply(Q).Print();

                Debug.WriteLine("R");
                R.Print();

                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("QR");
                Q.Multiply(R).Print();
            }

            Debug.Assert(A.ApproximatelyEquals(Q.Multiply(R)));
            Debug.Assert(Q.Transpose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<double>(m)));
        }
        public static void ComplexQRDecompositionTest(bool verbose = false)
        {
            int m = 10, n = 5;

            Complex[,] A = RectangularMatrix.Random<Complex>(m, n);

            A.QRDecompose(out Complex[,] Q, out Complex[,] R, true, QRDecompositionMethod.HOUSEHOLDER_TRANSFORM);

            if (verbose)
            {
                Debug.WriteLine("Q");
                Q.Print();

                Debug.WriteLine("Q*Q");
                Q.ConjugateTranspose().Multiply(Q).Print();

                Debug.WriteLine("R");
                R.Print();

                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("QR");
                Q.Multiply(R).Print();
            }

            Debug.Assert(A.ApproximatelyEquals(Q.Multiply(R)));
            Debug.Assert(Q.ConjugateTranspose().Multiply(Q).ApproximatelyEquals(RectangularMatrix.Identity<Complex>(m)));
        }

        public static void RealCharacteristicPolynomialTest(bool verbose = false)
        {
            double[,] A = RectangularMatrix.Random<double>(10, 10);
            Polynomial<double> cp = A.FaddeevLeVerrierAlgorithm();
            Complex[] eigenvalues = A.Eigenvalues();

            if (verbose)
            {
                Debug.WriteLine(cp);
            }

            foreach (Complex eig in eigenvalues)
            {
                if (eig.IsReal(1e-3))
                {
                    if (verbose)
                    {
                        Debug.WriteLine($"P({eig.Real}) = {cp.Evaluate(eig.Real)}");
                    }
                    Debug.Assert(Util.ApproximatelyEquals(cp.Evaluate(eig.Real), 0, 1e-6));
                }
                else
                {
                    if (verbose)
                    {
                        Debug.WriteLine($"{eig} is complex - not tested.");
                    }
                }
            }
        }

        public static void RealEigenvaluesTest()
        {
            double[,] A = RectangularMatrix.Random<double>(10, 10);

            A.Print();

            Complex[] e1 = A.Eigenvalues(true, EigenvalueAlgorithm.NAIVE_QR_ALGORITHM);
            Complex[] e2 = A.Eigenvalues(true, EigenvalueAlgorithm.SHIFTED_QR_ALGORITHM);
            Complex[] e3 = A.Eigenvalues(true, EigenvalueAlgorithm.DOUBLE_SHIFTED_QR_ALGORITHM);
            Complex[] e4 = A.Eigenvalues(true, EigenvalueAlgorithm.FRANCIS_ALGORITHM);

            for (int i = 0; i < e1.Length; ++i)
            {
                Debug.WriteLine(
                    e1[i].Real + "\t" + e1[i].Imaginary + "\t" + 
                    e2[i].Real + "\t" + e2[i].Imaginary + "\t" + 
                    e3[i].Real + "\t" + e3[i].Imaginary + "\t" + 
                    e4[i].Real + "\t" + e4[i].Imaginary);
            }
        }

        public static void KroneckerSumTest(bool verbose = false)
        {
            double[,] A = RectangularMatrix.Random<double>(2, 2);
            double[,] B = RectangularMatrix.Random<double>(2, 2);

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("B");
                B.Print();

                A.KroneckerProduct(B).Print();

                Debug.WriteLine("A (+) B");
                A.KroneckerSum(B).Print();
            }
        }
    }
}
