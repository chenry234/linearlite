﻿using LinearLite.Matrices;
using LinearLite.Matrices.Sparse;
using LinearLite.Structs;
using LinearLite.Structs.Matrix;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tests
{
    public static class SparseMatrixTests
    {
        public static void SparseMatrixCreationTest(bool verbose = false)
        {
            if (verbose)
            {
                SparseMatrix<double> T0 = SparseMatrix.RandomTriangular<double>(10, 10, 1000, true);
                Debug.WriteLine("Upper triangular matrix");
                T0.Print();

                SparseMatrix<double> T1 = SparseMatrix.RandomTriangular<double>(10, 15, 1000, true);
                Debug.WriteLine("Upper triangular matrix");
                T1.Print();

                SparseMatrix<double> T2 = SparseMatrix.RandomTriangular<double>(15, 10, 1000, true);
                Debug.WriteLine("Upper triangular matrix");
                T2.Print();

                SparseMatrix<double> T3 = SparseMatrix.RandomTriangular<double>(10, 15, 1000, false);
                Debug.WriteLine("Lower triangular matrix");
                T3.Print();

                SparseMatrix<double> T4 = SparseMatrix.RandomTriangular<double>(15, 10, 1000, false);
                Debug.WriteLine("Lower triangular matrix");
                T4.Print();

                SparseMatrix<double> D = SparseMatrix.RandomDiagonal<double>(15, 10, 100);
                Debug.WriteLine("Diagonal matrix");
                D.Print();

                SparseMatrix<double> B1 = SparseMatrix.RandomBidiagonal<double>(15, 10, 1000, true);
                Debug.WriteLine("Bidiagonal matrix");
                B1.Print();

                SparseMatrix<double> B2 = SparseMatrix.RandomBidiagonal<double>(10, 15, 1000, true);
                Debug.WriteLine("Bidiagonal matrix");
                B2.Print();

                SparseMatrix<double> B3 = SparseMatrix.RandomBidiagonal<double>(15, 10, 1000, false);
                Debug.WriteLine("Bidiagonal matrix");
                B3.Print();

                SparseMatrix<double> B4 = SparseMatrix.RandomBidiagonal<double>(10, 15, 1000, false);
                Debug.WriteLine("Bidiagonal matrix");
                B4.Print();

                SparseMatrix<double> TR1 = SparseMatrix.RandomTridiagonal<double>(10, 15, 1000);
                Debug.WriteLine("Tridiagonal matrix");
                TR1.Print();

                SparseMatrix<double> TR2 = SparseMatrix.RandomTridiagonal<double>(10, 10, 1000);
                Debug.WriteLine("Tridiagonal matrix");
                TR2.Print();

                SparseMatrix<double> TR3 = SparseMatrix.RandomTridiagonal<double>(15, 10, 1000);
                Debug.WriteLine("Tridiagonal matrix");
                TR3.Print();

                SparseMatrix<double> H1 = SparseMatrix.RandomHessenberg<double>(10, 1000, true);
                Debug.WriteLine("Hessenberg matrix");
                H1.Print();

                SparseMatrix<double> H2 = SparseMatrix.RandomHessenberg<double>(10, 1000, false);
                Debug.WriteLine("Hessenberg matrix");
                H2.Print();

                SparseMatrix<double> I = SparseMatrix.Identity<double>(10);
                Debug.WriteLine("Identity matrix");
                I.Print();
            }
            
        }
        public static void SparseMatrixCastingTest(bool verbose = false)
        {
            if (verbose)
            {
                TriangularMatrix<double> matrix = TriangularMatrix.Random<double>(10, 10, true);
                matrix.Print();

                SparseMatrix<double> sparse = (SparseMatrix<double>)matrix;
                sparse.Print();
            }
        }
        public static void SparseMatrixMultiplicationTest(bool verbose = false)
        {
            SparseMatrix<double> A = SparseMatrix.Random<double>(10, 15, 200);
            SparseMatrix<double> B = SparseMatrix.Random<double>(15, 20, 300);

            DenseMatrix<double> _A = A.ToDense();
            DenseMatrix<double> _B = B.ToDense();

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print(x => x.ToString("n6"));

                Debug.WriteLine("B");
                B.Print(x => x.ToString("n6"));

                Debug.WriteLine("AB (using sparse multiplication)");
                A.Multiply(B).Print(x => x.ToString("n6"));

                Debug.WriteLine("AB (using dense multiplication)");
                _A.Multiply(_B).Print();
            }

            Debug.Assert(_A.Multiply(_B).ApproximatelyEquals(A.Multiply(B).ToDense()));
        }
        public static void SparseMatrixTransposeTest(bool verbose = false)
        {
            if (verbose)
            {
                SparseMatrix<double> matrix = SparseMatrix.Random<double>(10, 10, 20);
                Debug.WriteLine("A");
                matrix.Print();

                matrix = matrix.Transpose();
                Debug.WriteLine("A^T");
                matrix.Print();

                SparseMatrix<Complex> B = SparseMatrix.Random<Complex>(10, 10, 20);
                Debug.WriteLine("B");
                B.Print();

                B = B.ConjugateTranspose();
                Debug.WriteLine("B^T");
                B.Print();
            }
        }
        public static void SparseMatrixDeterminantTest(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(10, 10);
            SparseMatrix<double> _A = (SparseMatrix<double>)A;

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("Dense det:\t" + A.Determinant(FinitePrecisionDeterminantMethod.ECHELON_ROW_REDUCTION));
                Debug.WriteLine("Sparse det:\t" + _A.Determinant());
            }
        }
        public static void SparseMatrixDirectSumTest(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(10, 10);
            DenseMatrix<double> B = DenseMatrix.Random<double>(10, 10);
            DenseMatrix<double> A_B = A.DirectSum(B);

            SparseMatrix<double> _A = A.ToSparse();
            SparseMatrix<double> _B = B.ToSparse();
            SparseMatrix<double> _A_B = _A.DirectSum(_B);

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("B");
                B.Print();

                Debug.WriteLine("A (+) B");
                A_B.Print();

                Debug.WriteLine("_A");
                _A.Print();

                Debug.WriteLine("_B");
                _B.Print();

                Debug.WriteLine("_A (+) _B");
                _A_B.Print();
            }

            Debug.Assert(_A_B.ToDense().ApproximatelyEquals(A_B));
        }
        public static void SparseMatrixKroneckerProductTest(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(10, 10);
            DenseMatrix<double> B = DenseMatrix.Random<double>(10, 10);
            DenseMatrix<double> A_B = A.KroneckerProduct(B);

            SparseMatrix<double> _A = A.ToSparse();
            SparseMatrix<double> _B = B.ToSparse();
            SparseMatrix<double> _A_B = _A.KroneckerProduct(_B);

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("B");
                B.Print();

                Debug.WriteLine("A (+) B");
                A_B.Print();

                Debug.WriteLine("_A");
                _A.Print();

                Debug.WriteLine("_B");
                _B.Print();

                Debug.WriteLine("_A (+) _B");
                _A_B.Print();
            }

            Debug.Assert(_A_B.ToDense().ApproximatelyEquals(A_B));
        }
    }
}
