﻿using LinearLite.Symbolic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tests
{
    public static class SymbolicTests
    {
        public static void ExpressionParserTest()
        {
            string s = "1 + 5(2 + 3(1 + 5))(1+2) + exp5 + log(4)";
            Debug.WriteLine(s);

            var p = new ExpressionParser2();
            p.Parse(s);
        }
    }
}
