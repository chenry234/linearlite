﻿using LinearLite.Matrices;
using LinearLite.Structs;
using LinearLite.Structs.Tensors;
using LinearLite.Structs.Vectors;
using LinearLite.Tensors;
using LinearLite.Tensors.Decompositions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Tests
{
    public static class TensorTests
    {
        public static void TensorCreateTest()
        {
            double[,] m1 = RectangularMatrix.Random<double>(5000, 5000);
            double[,] m2 = RectangularMatrix.Random<double>(5000, 5000);
            Stopwatch sw = new Stopwatch();

            sw.Restart();
            double[,] r1 = m1.ElementwiseOperation(m2, (double a, double b) => a + b);
            sw.Stop();
            Debug.WriteLine("Using func:\t" + sw.ElapsedMilliseconds);

            sw.Restart();
            double[,] r2 = m1.Add(m2);
            sw.Stop();
            Debug.WriteLine("Using native double multiplication:\t" + sw.ElapsedMilliseconds);

        }
        public static void SparseTensorCreateTest(bool verbose = false)
        {
            SparseTensor<double> sparse = SparseTensor.Random<double>(10, 10);

            if (verbose)
            {
                sparse.PrintDimensions();
                sparse.Print();
            }
        }
        public static void TensorDimensionTest()
        {
            DenseTensor<double> tensor = new DenseTensor<double>(4, 3, 2, 2, 2, 2);
            List<int> dimensions = tensor.GetDimensions();
            dimensions.ToArray().Print();
        }
        public static void TensorConversionTest(bool verbose = false)
        {
            if (verbose)
            {
                DenseTensor<double> tensor = DenseTensor.Random<double>(10, 10);
                Debug.WriteLine("Dense 2");
                tensor.Print();

                SparseTensor<double> sparse = tensor.ToSparse();
                Debug.WriteLine("Sparse 2");
                sparse.Print();


                DenseTensor<double> tensor2 = DenseTensor.Random<double>(5, 10, 10);
                Debug.WriteLine("Dense 3");
                tensor2.Print();

                SparseTensor<double> sparse2 = tensor2.ToSparse();
                Debug.WriteLine("Sparse 3");
                sparse2.Print();
            }
        }
        public static void TensorOverflowTest(bool run = false)
        {
            if (run)
            {
                SparseTensor<double> tensor = new SparseTensor<double>(16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16);

            }
        }

        public static void DenseTensorDirectSumTest()
        {
            DenseTensor<double> S = DenseTensor.Random<double>(10, 10), T = DenseTensor.Random<double>(10, 10);

            Debug.WriteLine("S");
            S.Print(a => a.ToString("n6"));

            Debug.WriteLine("T");
            T.Print(a => a.ToString("n6"));

            Debug.WriteLine("S (+) T");
            S.DirectSum(T).Print(a => a.ToString("n6"));
        }
        public static void DenseTensorProductTest()
        {
            DenseTensor<double> S = DenseTensor.Random<double>(4, 5, 6), T = DenseTensor.Random<double>(4, 4, 4);

            Debug.WriteLine("S");
            S.Print(a => a.ToString("n6"));

            Debug.WriteLine("T");
            T.Print(a => a.ToString("n6"));

            Debug.WriteLine("S (+) T");
            S.TensorProduct(T).Print(a => a.ToString("n6"));

        }
        public static void SparseTensorProductTest(bool verbose = false)
        {
            DenseTensor<double> S = DenseTensor.Random<double>(3, 2, 3), T = DenseTensor.Random<double>(2, 3, 3);
            SparseTensor<double> _S = S.ToSparse(), _T = T.ToSparse();

            DenseTensor<double> ST = S.TensorProduct(T);
            SparseTensor<double> _ST = _S.TensorProduct(_T);

            if (verbose)
            {
                Debug.WriteLine("S");
                S.Print();

                Debug.WriteLine("_S");
                _S.Print();

                Debug.WriteLine("T");
                T.Print();

                Debug.WriteLine("_T");
                _T.Print();

                Debug.WriteLine("S (+) T (dense)");
                ST.Print();

                Debug.WriteLine("S (+) T (sparse)");
                _ST.Print();
            }

            Debug.Assert(ST.ApproximatelyEquals(_ST));
        }
        public static void SparseTensorIndexTest(bool verbose = false)
        {
            SparseTensor<double> S = DenseTensor.Random<double>(2, 3, 3).ToSparse();

            if (verbose)
            {
                S.PrintDimensions();
                S.Print();

                foreach (long key in S.Values.Keys)
                {
                    int[] index = new int[3];
                    S.get_index(key, index, true);

                    int[] index2 = new int[3];
                    //S.get_index(key, index, false);

                    Debug.WriteLine(key + "\t" + string.Join(",", index) + "\t" + string.Join(",", index2) + "\t" + S.Values[key]);
                }
            }
            
        }

        public static void NModeFlattenTest(bool verbose = false)
        {
            DenseTensor<double> tensor = new DenseTensor<double>(3, 4, 2);

            DenseTensor<double> layer1 = (DenseTensor<double>)new double[4, 2] { { 1, 13 }, { 4, 16 }, { 7, 19 }, { 10, 22 } };
            tensor[0] = layer1;
            tensor[1] = (DenseTensor<double>)new double[4, 2] { { 2, 14 }, { 5, 17 }, { 8, 20 }, { 11, 23 } };
            tensor[2] = (DenseTensor<double>)new double[4, 2] { { 3, 15 }, { 6, 18 }, { 9, 21 }, { 12, 24 } };

            if (verbose)
            {
                Debug.WriteLine("Tensor");
                tensor.Print();

                Debug.WriteLine("Mode 0");
                tensor.Flatten(0).Print();

                Debug.WriteLine("Mode 1");
                tensor.Flatten(1).Print();

                Debug.WriteLine("Mode 2");
                tensor.Flatten(2).Print();
            }
        }
        public static void NModeProductTest(bool verbose = false)
        {
            DenseTensor<double> tensor = DenseTensor.Random<double>(3, 4, 5);
            DenseMatrix<double> matrix = DenseMatrix.Random<double>(7, 3);

            if (verbose)
            {
                tensor.NModeProduct(0, matrix, (A, x) => A.Multiply(x)).PrintDimensions();
            }
        }
        public static void HOSVDTest(bool verbose = false)
        {
            DenseTensor<double> A = DenseTensor.Random<double>(4, 4, 4);
            A.HOSVD(out DenseTensor<double> S, out List<DenseMatrix<double>> U);

            DenseTensor<double> product = S;
            for (int i = 0; i < U.Count; ++i)
            {
                product = product.NModeProduct(U[i], i);
            }
            
            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("S");
                S.Print();

                Debug.WriteLine("Us");
                foreach (DenseMatrix<double> u in U)
                {
                    u.Print();
                }

                Debug.WriteLine("Product");
                product.Print();
            }
            
        }

        public static void TensorVectorizationTest(bool verbose = false)
        {
            if (verbose)
            {
                DenseTensor<double> dense = DenseTensor.Random<double>(3, 3, 3);
                SparseTensor<double> sparse = dense.ToSparse();

                DenseVector<double> dvector = dense.Vectorize();
                SparseVector<double> svector = sparse.Vectorize();

                Debug.WriteLine("Tensor (dense)");
                dense.Print();

                Debug.WriteLine("Tensor (sparse)");
                sparse.Print();

                Debug.WriteLine("Vector (dense)");
                dvector.Print();

                Debug.WriteLine("Vector (sparse)");
                svector.Print();
            }
        }
        public static void SparseTensorDirectSum(bool verbose = false)
        {
            DenseMatrix<double> A = DenseMatrix.Random<double>(10, 10);
            DenseMatrix<double> B = DenseMatrix.Random<double>(5, 5);

            DenseTensor<double> tensorA = (DenseTensor<double>)A;
            DenseTensor<double> tensorB = (DenseTensor<double>)B;

            DenseMatrix<double> matrixSum = A.DirectSum(B);
            DenseTensor<double> tensorSum = tensorA.DirectSum(tensorB);

            if (verbose)
            {
                Debug.WriteLine("A");
                A.Print();

                Debug.WriteLine("B");
                B.Print();

                Debug.WriteLine("A (+) B");
                matrixSum.Print();

                Debug.WriteLine("A (+) B (version 2)");
                tensorSum.Print();
            }

            Debug.Assert(((DenseTensor<double>)matrixSum).ApproximatelyEquals(tensorSum));
        }

        public static void TensorSummationTest(bool verbose = false)
        {
            DenseTensor<double> tensor = DenseTensor.Random<double>(10, 10);
            DenseVector<double> rowSums = tensor.SumOverAllExcept(0, 0, (a, b) => a + b);
            DenseVector<double> colSums = tensor.SumOverAllExcept(1, 0, (a, b) => a + b);

            DenseTensor<double> tensor3 = DenseTensor.Random<double>(10, 10, 10);

            if (verbose)
            {
                Debug.WriteLine("tensor");
                tensor.Print();

                Debug.WriteLine("row sums");
                rowSums.Print();

                Debug.WriteLine("col sums");
                colSums.Print();

                Debug.WriteLine("Tensor 3");
                tensor3.Print();

                Debug.WriteLine("Tensor sum over 0");
                tensor3.SumOver(0).Print();

                Debug.WriteLine("Tensor sum over 1");
                tensor3.SumOver(1).Print();

                Debug.WriteLine("Tensor sum over 2");
                tensor3.SumOver(2).Print();
            }


        }

        public static void CPDecompositionTest(bool verbose = false)
        {
            int order = 6, firstDim = 5;

            DenseTensor<double> tensor = new DenseTensor<double>(RectangularVector.Random<double>(firstDim));
            for (int i = 1; i < order; ++i)
            {
                DenseVector<double> vect = new DenseVector<double>(RectangularVector.Random<double>(firstDim));
                tensor = tensor.OuterProduct(vect);
            }

            tensor.NormalizedCanonicalPolyadicDecompose(2, out double factor, out List<DenseVector<double>> u);

            DenseTensor<double> reconstruction = (DenseTensor<double>)u[0];
            for (int i = 1; i < u.Count; ++i) reconstruction = reconstruction.OuterProduct(u[i]);
            reconstruction = reconstruction.Multiply(factor);

            if (verbose)
            {
                Debug.WriteLine("Tensor");
                tensor.Print();

                Debug.WriteLine("Reconstruction");
                reconstruction.Print();

                Debug.WriteLine("Factor:\t" + factor);
                foreach (DenseVector<double> _u in u)
                {
                    Debug.WriteLine("Vector:");
                    _u.Print();
                }
            }

            Debug.Assert(tensor.ApproximatelyEquals(reconstruction));
        }
    }
}
