﻿using LinearLite.Structs;
using LinearLite.Structs.Vectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Vectors
{
    public static class DenseVectorOperations
    {
        public static DenseMatrix<int> OuterProduct(this DenseVector<int> u, DenseVector<int> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<long> OuterProduct(this DenseVector<long> u, DenseVector<long> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<float> OuterProduct(this DenseVector<float> u, DenseVector<float> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<double> OuterProduct(this DenseVector<double> u, DenseVector<double> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<decimal> OuterProduct(this DenseVector<decimal> u, DenseVector<decimal> v) => OuterProduct(u, v, (a, b) => a * b);
        public static DenseMatrix<T> OuterProduct<T>(this DenseVector<T> u, DenseVector<T> v) where T : Ring<T>, new() => OuterProduct(u, v, (a, b) => a.Multiply(b));
        private static DenseMatrix<T> OuterProduct<T>(DenseVector<T> u, DenseVector<T> v, Func<T, T, T> Multiply) where T : new()
        {
            if (u == null || v == null) throw new ArgumentNullException();

            int m = u.Dimension, n = v.Dimension, i, j;

            DenseMatrix<T> matrix = new DenseMatrix<T>(m, n);
            T[][] prod = matrix.Values;
            for (i = 0; i < m; ++i)
            {
                T[] row = prod[i];
                T u_i = u[i];
                for (j = 0; j < n; ++j)
                {
                    row[j] = Multiply(u_i, v[j]);
                }
            }
            return matrix;
        }

        public static DenseVector<T> ElementwiseOperate<F, T>(DenseVector<F> v, Func<F, T> Operation)
        {
            if (v == null || Operation == null)
            {
                throw new ArgumentNullException();
            }
            return new DenseVector<T>(v.Values.ElementwiseOperate(Operation));
        }
    }
}
