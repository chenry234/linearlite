﻿using LinearLite.Helpers;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class RectangularVector
    {
        public static T[] Random<T>(int n, Func<T> Sample)
        {
            T[] rand = new T[n];
            for (int i = 0; i < n; i++)
            {
                rand[i] = Sample();
            }
            return rand;
        }
        public static T[] Random<T>(int n)
        {
            return Random(n, DefaultGenerators.GetRandomGenerator<T>());
        }
        public static T[] Repeat<T>(T value, int n)
        {
            T[] vector = new T[n];
            for (int i = 0; i < n; ++i)
            {
                vector[i] = value;
            }
            return vector;
        }
    }
}
