﻿using LinearLite.Helpers;
using LinearLite.Structs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite
{
    public static class RectangularVectorOperations
    {
        private static void CheckDimensions<T>(T[] a, T[] b)
        {
            if (a == null || b == null)
            {
                throw new ArgumentNullException("Vector is null.");
            }

            if (a.Length != b.Length)
            {
                throw new InvalidOperationException($"Vector dimensions don't match: {a.Length} vs {b.Length}.");
            }
        }
        private static void CheckNotNull<T>(T[] v)
        {
            if (v == null || v.Length == 0)
            {
                throw new ArgumentNullException("Vector is null or empty.");
            }
        }

        public static void Clear(this double[] x)
        {
            CheckNotNull(x);

            int len = x.Length, i;
            for (i = 0; i < len; ++i)
            {
                x[i] = 0.0;
            }
        }

        /// <summary>
        /// <para>
        /// Given two vectors $u, v\in\mathbb{F}^m$ and a binary function $f:\mathbb{F}\times\mathbb{F}\to\mathbb{F}$,
        /// returns a new vector $x\in\mathbb{F}^m$ where
        /// $$x_i = f(u_i, v_i)$$
        /// for all $1 \le i \le {m}$.
        /// </para>
        /// <para>Throws <txt>InvalidOperationException</txt> if the dimensions of the vectors don't match.</para>
        /// </summary>
        /// <name>BinaryOperationVector</name>
        /// <proto>IVector<T> BinaryOperation(this IVector<T> u, IVector<T> v, Func<T, T, T> Operation)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="u">A vector of dimension $n$.</param>
        /// <param name="v">A vector of dimension $n$.</param>
        /// <param name="Operation">A vector of dimension $n$.</param>
        /// <returns></returns>
        public static T[] BinaryOperation<T>(this T[] u, T[] v, Func<T, T, T> Operation)
        {
            CheckDimensions(u, v);

            int len = u.Length, i;
            T[] result = new T[len];
            for (i = 0; i < len; ++i)
            {
                result[i] = Operation(u[i], v[i]);
            }
            return result;
        }

        /// <summary>
        /// Apply the function $f:F\to T$ on every element of a vector $u\in F^m$, returning a vector $v\in T^m$, where
        /// $$v_i = f(u_i)$$ for each $1 \le {i} \le {m}.$
        /// <!--inputs-->
        /// 
        /// </summary>
        /// <name>ElementwiseOperateVector</name>
        /// <proto>IVector<T> ElementwiseOperate(this IVector<F> v, Func<F, T> Operation)</proto>
        /// <cat>la</cat>
        /// <typeparam name="F"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="v">Any vector over the type <txt>F</txt>.</param>
        /// <param name="Operation">
        /// A function $f:$ <txt>F</txt> $\to$ <txt>T</txt>. The function to evaluate on each element of the vector $u$.</param>
        /// <returns></returns>
        public static T[] ElementwiseOperate<F, T>(this F[] v, Func<F, T> Operation)
        {
            if (v == null)
            {
                throw new ArgumentNullException();
            }

            int len = v.Length, i;
            T[] result = new T[len];
            for (i = 0; i < len; ++i)
            {
                result[i] = Operation(v[i]);
            }
            return result;
        }

        /// <summary>
        /// Given 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <returns></returns>
        public static T[] Copy<T>(this T[] src)
        {
            CheckNotNull(src);

            T[] copy = new T[src.Length];
            Array.Copy(src, 0, copy, 0, src.Length);
            return copy;
        }
        public static double[] Copy(this double[] src)
        {
            CheckNotNull(src);

            double[] copy = new double[src.Length];
            Array.Copy(src, 0, copy, 0, src.Length);
            return copy;
        }
        public static Complex[] ToComplex(this double[] a)
        {
            CheckNotNull(a);

            int d = a.Length, i;
            Complex[] c = new Complex[d];
            for (i = 0; i < d; ++i) c[i] = new Complex(a[i]);
            return c;
        }

        /// <summary>
        /// Given two vectors of the same dimension, return their vector sum. The original vectors are unchanged.
        /// <para>Defined for vectors over all primitive types, and all types <txt>T</txt> implementing <txt>AdditiveGroup&lt;T&gt;</txt>.</para>
        /// <para>Throws <txt>InvalidOperationException</txt> if the dimensions of the vectors don't match.</para>
        /// <para>Also see: <a href="#BinaryOperationVector"><txt><b>BinaryOperation</b></txt></a>.</para>
        /// </summary>
        /// <name>AddVector</name>
        /// <proto>IVector<T> Add(this IVector<T> u, IVector<T> v)</proto>
        /// <cat>la</cat>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int[] Add(this int[] a, int[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            int[] sum = new int[d];
            for (i = 0; i < d; i++) sum[i] = a[i] + b[i];
            return sum;
        }
        public static long[] Add(this long[] a, long[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            long[] sum = new long[d];
            for (i = 0; i < d; i++) sum[i] = a[i] + b[i];
            return sum;
        }
        public static float[] Add(this float[] a, float[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            float[] sum = new float[d];
            for (i = 0; i < d; i++) sum[i] = a[i] + b[i];
            return sum;
        }
        /// <summary>
        /// Adds one vector to another. The original vectors are unchanged.
        /// The dimensionality of input vectors must be the same.
        /// </summary>
        /// <param name="a">The vector to subtract from.</param>
        /// <param name="b">The vector to subtract.</param>
        /// <returns>The sum of those two vectors.</returns>
        public static double[] Add(this double[] a, double[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            double[] sum = new double[d];
            for (i = 0; i < d; i++) sum[i] = a[i] + b[i];
            return sum;
        }
        public static decimal[] Add(this decimal[] a, decimal[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            decimal[] sum = new decimal[d];
            for (i = 0; i < d; i++) sum[i] = a[i] + b[i];
            return sum;
        }
        public static T[] Add<T>(this T[] a, T[] b) where T : AdditiveGroup<T>, new()
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            T[] sum = new T[d];
            for (i = 0; i < d; i++) sum[i] = a[i].Add(b[i]);
            return sum;
        }

        public static Complex[] Add(this double[] a, Complex[] b)
        {
            return Add(a.ToComplex(), b);
        }
        public static Complex[] Add(this Complex[] a, double[] b)
        {
            return Add(a, b.ToComplex());
        }

        /// <summary>
        /// Given vectors $u, v\in\mathbb{F}^m$, returns the vector difference $u - v\in\mathbb{F}^m$. The original vectors are unchanged.
        /// <para>Defined for vectors over all primitive types, and all types <txt>T</txt> implementing <txt>AdditiveGroup&lt;T&gt;</txt>.</para>
        /// <para>Throws <txt>InvalidOperationException</txt> if the dimensions of the vectors don't match.</para>
        /// <para>Also see: <a href="#BinaryOperationVector"><txt><b>BinaryOperation</b></txt></a>.</para>
        /// </summary>
        /// <name>SubtractVector</name>
        /// <proto>IVector<T> Subtract(this IVector<T> u, IVector<T> v)</proto>
        /// <cat>la</cat>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int[] Subtract(this int[] a, int[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            int[] diff = new int[d];
            for (i = 0; i < d; i++) diff[i] = a[i] - b[i];
            return diff;
        }
        public static long[] Subtract(this long[] a, long[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            long[] diff = new long[d];
            for (i = 0; i < d; i++) diff[i] = a[i] - b[i];
            return diff;
        }
        public static float[] Subtract(this float[] a, float[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            float[] diff = new float[d];
            for (i = 0; i < d; i++) diff[i] = a[i] - b[i];
            return diff;
        }
        /// <summary>
        /// Subtract one vector from another. The original vectors are unchanged.
        /// The dimensionality of the two vectors must be the same.
        /// </summary>
        /// <param name="a">The vector to subtract from.</param>
        /// <param name="b">The vector to subtract.</param>
        /// <returns>The difference of those vectors.</returns>
        public static double[] Subtract(this double[] a, double[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            double[] diff = new double[d];
            for (i = 0; i < d; i++) diff[i] = a[i] - b[i];
            return diff;
        }
        public static decimal[] Subtract(this decimal[] a, decimal[] b)
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            decimal[] diff = new decimal[d];
            for (i = 0; i < d; i++) diff[i] = a[i] - b[i];
            return diff;
        }
        public static T[] Subtract<T>(this T[] a, T[] b) where T : AdditiveGroup<T>, new()
        {
            CheckDimensions(a, b);

            int d = a.Length, i;

            T[] diff = new T[d];
            for (i = 0; i < d; i++) diff[i] = a[i].Subtract(b[i]);
            return diff;
        }
        public static Complex[] Subtract(this Complex[] a, double[] b)
        {
            return a.Subtract(b.ToComplex());
        }
        public static Complex[] Subtract(this double[] a, Complex[] b)
        {
            return a.ToComplex().Subtract(b);
        }

        /// <summary>
        /// Given a vector $u$, return the opposite vector $-u$. The original vector is unchanged.
        /// </summary>
        /// <name>NegateVector</name>
        /// <proto>IVector<T> Negate(this IVector<T> u)</proto>
        /// <cat>la</cat>
        /// <param name="u"></param>
        /// <returns></returns>
        public static int[] Negate(this int[] u)
        {
            CheckNotNull(u);

            int len = u.Length, i;
            int[] neg = new int[len];
            for (i = 0; i < len; ++i)
            {
                neg[i] = -u[i];
            }
            return neg;
        }
        public static long[] Negate(this long[] u)
        {
            CheckNotNull(u);

            int len = u.Length, i;
            long[] neg = new long[len];
            for (i = 0; i < len; ++i)
            {
                neg[i] = -u[i];
            }
            return neg;
        }
        public static float[] Negate(this float[] u)
        {
            CheckNotNull(u);

            int len = u.Length, i;
            float[] neg = new float[len];
            for (i = 0; i < len; ++i)
            {
                neg[i] = -u[i];
            }
            return neg;
        }
        public static double[] Negate(this double[] u)
        {
            CheckNotNull(u);

            int len = u.Length, i;
            double[] neg = new double[len];
            for (i = 0; i < len; ++i)
            {
                neg[i] = -u[i];
            }
            return neg;
        }
        public static decimal[] Negate(this decimal[] u)
        {
            CheckNotNull(u);

            int len = u.Length, i;
            decimal[] neg = new decimal[len];
            for (i = 0; i < len; ++i)
            {
                neg[i] = -u[i];
            }
            return neg;
        }
        public static T[] Negate<T>(this T[] u) where T : AdditiveGroup<T>, new()
        {
            CheckNotNull(u);

            int len = u.Length, i;
            T[] neg = new T[len];
            for (i = 0; i < len; ++i)
            {
                neg[i] = u[i].AdditiveInverse();
            }
            return neg;
        }

        /// <summary>
        /// <para>Returns the dot product (scalar product) of two real or complex vectors. Both original vectors are unchanged.</para>
        /// <para>Defined for vectors over <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and <txt>Complex</txt>.</para>
        /// <para>Throws <txt>InvalidOperationException</txt> if the dimensions of the vectors don't match.</para></summary>
        /// <name>Dot</name>
        /// <proto>T Dot(this IVector<T> u, IVector<T> v)</proto>
        /// <cat>la</cat>
        /// <param name="a">The first vector to be multiplied.</param>
        /// <param name="b">The second vector the be multiplied.</param>
        /// <returns>The dot product (scalar product) of the 2 vectors.</returns>
        public static int Dot(this int[] a, int[] b)
        {
            CheckDimensions(a, b);

            int N = a.Length, n, sum = 0;
            for (n = 0; n < N; n++) sum += a[n] * b[n];
            return sum;
        }
        public static long Dot(this long[] a, long[] b)
        {
            CheckDimensions(a, b);
            int N = a.Length, n;
            long sum = 0;
            for (n = 0; n < N; n++) sum += a[n] * b[n];
            return sum;
        }
        public static float Dot(this float[] a, float[] b)
        {
            CheckDimensions(a, b);

            int N = a.Length, n;

            float sum = 0.0f;
            for (n = 0; n < N; n++) sum += a[n] * b[n];
            return sum;
        }
        public static double Dot(this double[] a, double[] b)
        {
            CheckDimensions(a, b);

            int N = a.Length, n;

            double sum = 0.0;
            for (n = 0; n < N; n++) sum += a[n] * b[n];
            return sum;
        }
        public static decimal Dot(this decimal[] a, decimal[] b)
        {
            CheckDimensions(a, b);

            int N = a.Length, n;

            decimal sum = 0.0m;
            for (n = 0; n < N; n++) sum += a[n] * b[n];
            return sum;
        }
        /// <summary>
        /// Calculates the Hermitian inner product, defined as u * v = sum(u[i] * conjugate(v[i]))
        /// </summary>
        /// <param name="u">Complex vector</param>
        /// <param name="v">Complex vector</param>
        /// <returns>The hermitian inner product of u and v, u * v</returns>
        public static Complex Dot(this Complex[] u, Complex[] v)
        {
            CheckDimensions(u, v);

            int dim = u.Length, i;

            double re = 0.0, im = 0.0;
            for (i = 0; i < dim; i++)
            {
                Complex z = u[i], w = v[i];
                re += z.Real * w.Real + z.Imaginary * w.Imaginary;
                im += z.Imaginary * w.Real - z.Real * w.Imaginary;
            }
            return new Complex(re, im);
        }
        public static T Dot<T>(this T[] u, T[] v) where T : Ring<T>, new()
        {
            CheckDimensions(u, v);

            int N = u.Length, n;

            T sum = new T().AdditiveIdentity;
            for (n = 0; n < N; n++) sum = sum.Add(u[n].Multiply(v[n]));
            return sum;
        }

        /// <summary>
        /// <para>Returns the $p$-norm of a vector. </para>
        /// <para>This method may be numerically unstable for large values of $p$. In such cases, consider using 
        /// <a href="#InfinityNorm"><txt>InfinityNorm</txt></a> as an approximation instead.</para>
        /// <!--inputs-->
        /// </summary>
        /// <name>Norm</name>
        /// <proto>T Norm(this IVector<T> u, int p = 2)</proto>
        /// <cat>la</cat>
        /// <param name="x">A vector over <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> or <txt>Complex</txt>.</param>
        /// <param name="p">
        /// <b>Optional</b>, defaults to 2.<br/>
        /// The order of the norm. Must be positive or <txt>InvalidOperationException</txt> will be thrown.
        /// </param>
        /// <returns>The p-norm of the vector a.</returns>
        public static double Norm(this int[] x, double p = 2.0)
        {
            CheckNotNull(x);
            double sum = 0;
            foreach (int d in x) sum += Math.Pow(Math.Abs(d), p);
            return Math.Pow(sum, 1 / p);
        }
        public static double Norm(this long[] x, double p = 2.0)
        {
            CheckNotNull(x);
            double sum = 0;
            foreach (int d in x) sum += Math.Pow(Math.Abs(d), p);
            return Math.Pow(sum, 1 / p);
        }
        public static float Norm(this float[] x, float p = 2.0f)
        {
            CheckNotNull(x);

            double sum = 0.0;
            foreach (float d in x) sum += Math.Pow(Math.Abs(d), p);
            return (float)Math.Pow(sum, 1 / p);
        }
        public static double Norm(this double[] x, double p = 2.0)
        {
            CheckNotNull(x);
            double sum = 0;
            foreach (double d in x) sum += Math.Pow(Math.Abs(d), p);
            return Math.Pow(sum, 1 / p);
        }
        public static decimal Norm(this decimal[] x, decimal p = 2.0m)
        {
            CheckNotNull(x);
            decimal sum = 0.0m;
            foreach (decimal d in x) sum += DecimalMath.Pow(Math.Abs(d), p);
            return DecimalMath.Pow(sum, 1.0m / p);
        }
        /// <summary>
        /// Calculates the norm of a complex vector derived from the Hermitian inner product
        /// </summary>
        /// <param name="u">A non-degenerate vector u.</param>
        /// <returns>The norm of vector u.</returns>
        public static double Norm(this Complex[] u)
        {
            CheckNotNull(u);
            int dim = u.Length, i;
            double sum = 0;
            for (i = 0; i < dim; i++)
            {
                Complex c = u[i];
                sum += c.Real * c.Real + c.Imaginary * c.Imaginary; // equal to c . conj(c)
            }
            return Math.Sqrt(sum);
        }

        /// <summary>
        /// Calculates the $\infty$-norm, or maximum norm, of a $n$-dimensional real or complex vector $x$, where 
        /// $$||x||_{\infty}:= \max\{ |x_1|, |x_2|, ..., |x_n| \}.$$
        /// Also see: <a href="#Norm"><txt>Norm</txt></a>.
        /// </summary>
        /// <name>InfinityNorm</name>
        /// <proto>T InfinityNorm(this IVector<T> u)</proto>
        /// <cat>la</cat>
        /// <param name="x">The vector</param>
        /// <returns>The infinity-norm of vector x.</returns>
        public static double InfinityNorm(this double[] x)
        {
            if (x == null || x.Length == 0)
            {
                throw new ArgumentNullException("Vector is null or empty.");
            }

            double max = 0;
            foreach (double d in x)
            {
                double abs = Math.Abs(d);
                if (abs > max)
                {
                    max = abs;
                }
            }
            return max;
        }

        /// <summary>
        /// <para>Multiply a vector by a scalar. The original vector is unchanged.</para>
        /// <para>Throws <txt>ArgumentNullException</txt> if the vector is null.</para>
        /// </summary>
        /// <name>MultiplyVectorScalar</name>
        /// <proto>IVector<T> Multiply(this IVector<T> u, T scalar)</proto>
        /// <cat>la</cat>
        /// <param name="v">The vector to be multiplied.</param>
        /// <param name="s">The scalar to be multiplied.</param>
        /// <returns>The product vector.</returns>
        public static double[] Multiply(this double[] v, double s)
        {
            CheckNotNull(v);

            int N = v.Length, i;
            double[] prod = new double[N];
            for (i = 0; i < N; i++) prod[i] = v[i] * s;
            return prod;
        }
        public static int[] Multiply(this int[] v, int s)
        {
            CheckNotNull(v);

            int N = v.Length, i;
            int[] prod = new int[N];
            for (i = 0; i < N; i++) prod[i] = v[i] * s;
            return prod;
        }
        public static float[] Multiply(this float[] v, float s)
        {
            CheckNotNull(v);

            int N = v.Length, i;
            float[] prod = new float[N];
            for (i = 0; i < N; i++) prod[i] = v[i] * s;
            return prod;
        }
        public static decimal[] Multiply(this decimal[] v, decimal s)
        {
            CheckNotNull(v);

            int N = v.Length, i;
            decimal[] prod = new decimal[N];
            for (i = 0; i < N; i++) prod[i] = v[i] * s;
            return prod;
        }

        public static Complex[] Multiply(this double[] v, Complex s)
        {
            CheckNotNull(v);

            int N = v.Length, i;

            Complex[] prod = new Complex[N];
            for (i = 0; i < N; i++) prod[i] = s * v[i];

            return prod;
        }
        public static T[] Multiply<T>(this T[] v, T s) where T : Ring<T>, new()
        {
            CheckNotNull(v);

            int N = v.Length, i;

            T[] prod = new T[N];
            for (i = 0; i < N; i++) prod[i] = v[i].Multiply(s);

            return prod;
        }
        public static Complex[] Multiply(this Complex[] v, double s)
        {
            CheckNotNull(v);

            int N = v.Length, i;

            Complex[] prod = new Complex[N];
            for (i = 0; i < N; i++) prod[i] = v[i] * s;

            return prod;
        }

        public static double[] Multiply(this double[] v, double[,] a)
        {
            // v is horizontal
            CheckNotNull(v);
            MatrixChecks.CheckNotNull(a);

            if (v.Length != a.GetLength(0))
            {
                throw new InvalidOperationException($"Cannot multiply vector of size [1 x {a.Length}] and matrix of size [{a.GetLength(0)} x {a.GetLength(1)}]");
            }

            double[] prod = new double[a.GetLength(1)];
            for (int c = 0; c < prod.Length; c++)
            {
                for (int r = 0; r < v.Length; r++)
                {
                    prod[c] += v[r] * a[r, c];
                }
            }
            return prod;
        }

        /// <summary>
        /// <para>Given two vectors $u, v\in\mathbb{R}^3$, return their cross product $u \times v$.</para>
        /// <para>Supported for vectors over <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt>.</para>
        /// <para>Throws <txt>InvalidOperationException</txt> if either vector is not 3-dimensional.</para>
        /// </summary>
        /// <name>Cross</name>
        /// <proto>T Cross(this IVector<T> u, IVector<T> v)</proto>
        /// <cat>la</cat>
        /// <param name="u"></param>
        /// <param name="v"></param>
        /// <returns>The vector cross product between u and v (u x v).</returns>
        public static double[] Cross(this double[] u, double[] v)
        {
            CheckDimensions(u, v);
            if (u.Length != 3)
            {
                throw new InvalidOperationException("Cross products are only defined for 3-dimensional vectors.");
            }

            return new double[]
            {
                u[1] * v[2] - u[2] * v[1],
                u[2] * v[0] - u[0] * v[2],
                u[0] * v[1] - u[1] * v[0]
            };
        }

        /// <summary>
        /// Returns the angle (in radians) between two real-valued vectors $u, v\in\mathbb{R}^n$ where $n \ge 2$. 
        /// </summary>
        /// <name>AngleTo</name>
        /// <proto>double AngleTo(this IVector<T> u,  IVector<T> v)</proto>
        /// <cat>la</cat>
        /// <param name="u">A real-valued vector.</param>
        /// <param name="v">A real-valued vector.</param>
        /// <returns>The angle, in radians, between the two vectors.</returns>
        public static float AngleTo(this float[] u, float[] v)
        {
            CheckDimensions(u, v);
            double x = u.Dot(v) / Math.Sqrt(Math.Max(0, u.Dot(u) * v.Dot(v)));
            return (float)safe_acos(x);
        }
        public static double AngleTo(this double[] u, double[] v)
        {
            CheckDimensions(u, v);
            double x = u.Dot(v) / Math.Sqrt(Math.Max(0, u.Dot(u) * v.Dot(v)));
            return safe_acos(x);
        }
        public static decimal AngleTo(this decimal[] u, decimal[] v)
        {
            CheckDimensions(u, v);
            decimal x = u.Dot(v) / DecimalMath.Sqrt(Math.Max(0, u.Dot(u) * v.Dot(v)));
            return DecimalMath.Acos(x);
        }
        private static double safe_acos(double x)
        {
            if (double.IsNaN(x)) return double.NaN;
            if (x >= 1)
            {
                return 0;
            }
            if (x <= -1)
            {
                return -Math.PI;
            }
            return Math.Acos(x);
        }

        /// <summary>
        /// Calculate the orthogonal projection of $u$ onto $v$, where $u$ and $v$ are vectors in the same space.
        /// </summary>
        /// <name>ProjectOnto</name>
        /// <proto>IVector<T> ProjectOnto(this IVector<T> u, IVector<T> v)</proto>
        /// <cat>la</cat>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double[] ProjectOnto(this int[] u, int[] v)
        {
            double alpha = u.Dot(v) / (double)v.Dot(v);
            int len = u.Length, i;
            double[] proj = new double[len];
            for (i = 0; i < len; ++i)
            {
                proj[i] = alpha * v[i];
            }
            return proj;
        }
        public static double[] ProjectOnto(this long[] u, long[] v)
        {
            double alpha = u.Dot(v) / (double)v.Dot(v);
            int len = u.Length, i;
            double[] proj = new double[len];
            for (i = 0; i < len; ++i)
            {
                proj[i] = alpha * v[i];
            }
            return proj;
        }
        public static float[] ProjectOnto(this float[] u, float[] v) => v.Multiply(u.Dot(v) / v.Dot(v));
        public static double[] ProjectOnto(this double[] u, double[] v) => v.Multiply(u.Dot(v) / v.Dot(v));
        public static decimal[] ProjectOnto(this decimal[] u, decimal[] v) => v.Multiply(u.Dot(v) / v.Dot(v));
        public static Complex[] ProjectOnto(this Complex[] u, Complex[] v) => v.Multiply(u.Dot(v) / v.Dot(v));

        /// <summary>
        /// Returns the $n \times n$ Householder matrix $H$ of a real or complex-valued vector $v\in\mathbb{F}^n$, with
        /// $$H = I_n - 2 \frac{vv^*}{||v||^2},$$
        /// where $I_n$ is the $n \times n$ identity matrix.
        /// </summary>
        /// <name>HouseholderMatrix</name>
        /// <proto>IMatrix<T> HouseholderMatrix(this IVector<T> v)</proto>
        /// <cat>la</cat>
        /// <param name="A"></param>
        public static double[,] HouseholderMatrix(this double[] z)
        {
            z = z.Multiply(Math.Sqrt(2) / z.Norm(2));

            int length = z.Length, r, c;
            double[,] H = RectangularMatrix.Identity<double>(length);
            for (c = 0; c < length; c++)
            {
                double z_c = z[c];
                for (r = 0; r < length; r++)
                {
                    H[r, c] -= z[r] * z_c;
                }
            }
            return H;
        }
        /// <summary>
        /// Calculates the Householder matrix of a complex-valued vector v, where 
        /// X = I - 2 * v^H * v / (v . v)
        /// where v is a n-dimensional vector and I is the (n x n) identity matrix, 
        /// and X is the (n x n) Householder matrix. 
        /// </summary>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Complex[,] HouseholderMatrix(this Complex[] z)
        {
            z = z.Multiply(Math.Sqrt(2.0) / z.Norm());

            int length = z.Length, r, c;
            Complex[,] H = RectangularMatrix.Identity<Complex>(length);
            for (c = 0; c < length; c++)
            {
                for (r = 0; r < length; r++)
                {
                    H[r, c] -= z[r] * z[c].Conjugate();
                }
            }
            return H;
        }

        /// <summary>
        /// <para>Given two vectors $u\in\mathbb{F}^m$ and $v\in\mathbb{F}^n$, returns the order-2 tensor product $(u \otimes v)\in\mathbb{F}^{m \times n}$.</para>
        /// <para>Implemented for vectors over <txt>int</txt>, <txt>long</txt>, <txt>float</txt>, <txt>double</txt>, <txt>decimal</txt> and all types <txt>T</txt> implementing <txt>Ring&lt;T&gt;</txt>.</para>
        /// <!--inputs-->
        /// <!--returns-->
        /// </summary>
        /// <name>OuterProductVectorVector</name>
        /// <proto>IMatrix<T> OuterProduct(this IVector<T> u, IVector<T> v)</proto>
        /// <cat>la</cat>
        /// <param name="u">A vector of dimension $m$.</param>
        /// <param name="v">A vector of dimension $n$.</param>
        /// <returns>A matrix of dimension $m \times n$.</returns>
        public static int[,] OuterProduct(this int[] u, int[] v) => OuterProduct(u, v, (x, y) => x * y);
        public static long[,] OuterProduct(this long[] u, long[] v) => OuterProduct(u, v, (x, y) => x * y);
        public static float[,] OuterProduct(this float[] u, float[] v) => OuterProduct(u, v, (x, y) => x * y);
        public static double[,] OuterProduct(this double[] u, double[] v) => OuterProduct(u, v, (x, y) => x * y);
        public static decimal[,] OuterProduct(this decimal[] u, decimal[] v) => OuterProduct(u, v, (x, y) => x * y);
        public static T[,] OuterProduct<T>(this T[] u, T[] v) where T : Ring<T>, new() => OuterProduct(u, v, (x, y) => x.Multiply(y));
        internal static T[,] OuterProduct<T>(this T[] u, T[] v, Func<T, T, T> Multiply)
        {
            if (u == null || v == null) throw new ArgumentNullException();

            int m = u.Length, n = v.Length, i, j;

            T[,] prod = new T[m, n];
            for (i = 0; i < m; ++i)
            {
                T u_i = u[i];
                for (j = 0; j < n; ++j)
                {
                    prod[i, j] = Multiply(u_i, v[j]);
                }
            }
            return prod;
        }


        /// <summary>
        /// Given two vectors $u\in\mathbb{F}^m$ and $v\in\mathbb{F}^n$, returns the (tensorial) direct sum $u \oplus v\in\mathbb{F}^{m + n}$.
        /// </summary>
        /// <name>DirectSumVector</name>
        /// <proto>IVector<T> DirectSum(this IVector<T> u, IVector<T> v)</proto>
        /// <cat>la</cat>
        /// <typeparam name="T"></typeparam>
        /// <param name="u"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static T[] DirectSum<T>(this T[] u, T[] v)
        {
            if (u == null || v == null) throw new ArgumentNullException();

            T[] sum = new T[u.Length + v.Length];

            int i, j;
            for (i = 0; i < u.Length; ++i)
            {
                sum[i] = u[i];
            }
            for (i = 0, j = u.Length; i < v.Length; ++i, ++j)
            {
                sum[j] = v[i];
            }
            return sum;
        }

        public static bool Equals<T>(this T[] u, T[] v, Func<T, T, bool> Equals)
        {
            if (u == null || v == null) return false;
            if (u.Length != v.Length) return false;

            int len = u.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!Equals(u[i], v[i])) return false;
            }
            return true;
        }
        public static bool Equals(this int[] u, int[] v) => Equals(u, v, (a, b) => a == b);
        public static bool Equals(this long[] u, long[] v) => Equals(u, v, (a, b) => a == b);
        public static bool Equals<T>(this T[] u, T[] v) where T : Algebra<T>, new() => Equals(u, v, (a, b) => a.Equals(b));
        public static bool ApproximatelyEquals(this float[] u, float[] v, float tolerance = Precision.FLOAT_PRECISION) 
            => Equals(u, v, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this double[] u, double[] v, double tolerance = Precision.DOUBLE_PRECISION) 
            => Equals(u, v, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals(this decimal[] u, decimal[] v, decimal tolerance = Precision.DECIMAL_PRECISION)
            => Equals(u, v, (a, b) => Util.ApproximatelyEquals(a, b, tolerance));
        public static bool ApproximatelyEquals<T>(this T[] u, T[] v, double tolerance = Precision.DOUBLE_PRECISION) where T : Algebra<T>, new()
            => Equals(u, v, (a, b) => a.ApproximatelyEquals(b, tolerance));

        /// <summary>
        /// Returns whether the length-$N$ <txt>int32</txt> array <txt>P</txt> is a permutation of $\{0, ..., N - 1\}$.
        /// </summary>
        /// <name>IsPermutation</name>
        /// <proto>bool IsPermutation(this int[] P)</proto>
        /// <cat>la</cat>
        /// <param name="P"></param>
        /// <returns></returns>
        public static bool IsPermutation(this int[] P)
        {
            if (P == null)
            {
                return false;
            }

            int len = P.Length, i;
            bool[] present = new bool[len];
            for (i = 0; i < len; ++i)
            {
                int p = P[i];
                if (p < 0 || p >= len)
                {
                    return false;
                }
                if (present[p])
                {
                    return false;
                }
                present[p] = true;
            }

            foreach (bool p in present)
            {
                if (!p) return false;
            }
            return true;
        }
        #region Internal methods 
        /// <summary>
        /// Calculates a + b then stores the values in 'result', without checking the dimensions
        /// for compatability
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="result"></param>
        internal static void add_unsafe(this double[] a, double[] b, double[] result)
        {
            int len = a.Length, i;
            for (i = 0; i < len; ++i)
            {
                result[i] = a[i] + b[i];
            }
        }
        internal static void add_unsafe<T>(this T[] a, T[] b, T[] result) where T : AdditiveGroup<T>, new()
        {
            int len = a.Length, i;
            for (i = 0; i < len; ++i)
            {
                result[i] = a[i].Add(b[i]);
            }
        }

        /// <summary>
        /// Calculates a - b then stores value in 'result', without checking dimensions of a and b
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="result"></param>
        internal static void subtract_unsafe<T>(this T[] a, T[] b, T[] result) where T : AdditiveGroup<T>, new()
        {
            int len = a.Length, i;
            for (i = 0; i < len; ++i)
            {
                result[i] = a[i].Subtract(b[i]);
            }
        }
        internal static void subtract_unsafe(this double[] a, double[] b, double[] result)
        {
            int len = a.Length, i;
            for (i = 0; i < len; ++i)
            {
                result[i] = a[i] - b[i];
            }
        }

        internal static float Norm(this float[] z, int start, int end)
        {
            float sum = 0.0f;
            for (int i = start; i < end; ++i)
            {
                float d = z[i];
                sum += d * d;
            }
            return (float)Math.Sqrt(sum);
        }
        internal static double Norm(this double[] z, int start, int end)
        {
            double sum = 0.0;
            for (int i = start; i < end; ++i)
            {
                double d = z[i];
                sum += d * d;
            }
            return Math.Sqrt(sum);
        }
        internal static decimal Norm(this decimal[] z, int start, int end)
        {
            decimal sum = 0.0m;
            for (int i = start; i < end; ++i)
            {
                decimal d = z[i];
                sum += d * d;
            }
            return Util.Sqrt(sum);
        }
        internal static double Norm(this Complex[] z, int start, int end)
        {
            double sum = 0.0, real, imaginary;
            for (int i = start; i < end; ++i)
            {
                Complex c = z[i];
                real = c.Real;
                imaginary = c.Imaginary;
                sum += real * real + imaginary * imaginary; // equal to c . conj(c)
            }
            return Math.Sqrt(sum);
        }

        internal static void MultiplyOverwrite(this float[] z, float s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                z[i] *= s;
            }
        }
        internal static void MultiplyOverwrite(this double[] z, double s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                z[i] *= s;
            }
        }
        internal static void MultiplyOverwrite(this decimal[] z, decimal s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                z[i] *= s;
            }
        }
        internal static void MultiplyOverwrite(this Complex[] z, double s, int start, int end)
        {
            for (int i = start; i < end; ++i)
            {
                z[i].MultiplyEquals(s);
            }
        }

        public static bool IsZero(this double[] z, double tolerance = Precision.DOUBLE_PRECISION)
        {
            int len = z.Length, i;
            for (i = 0; i < len; ++i)
            {
                if (!Util.ApproximatelyEquals(z[i], 0.0, tolerance))
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

    }
}
