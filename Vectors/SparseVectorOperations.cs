﻿using LinearLite.Structs;
using LinearLite.Structs.Vectors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinearLite.Vectors
{
    public static class SparseVectorOperations
    {
        private static SparseMatrix<T> OuterProduct<T>(SparseVector<T> u, SparseVector<T> v, Func<T, T, T> Multiply) where T : new()
        {
            if (u == null || v == null) throw new ArgumentNullException();

            int m = u.Dimension, n = v.Dimension;
            Dictionary<long, T> _u = u.Values, _v = v.Values, product = new Dictionary<long, T>();

            foreach (KeyValuePair<long, T> uPair in _u)
            {
                long x = uPair.Key;
                foreach (KeyValuePair<long, T> vPair in _v)
                {
                    long y = vPair.Key;
                    product[x * n + y] = Multiply(uPair.Value, vPair.Value);
                }
            }
            return new SparseMatrix<T>(m, n, product);
        }
        public static SparseMatrix<int> OuterProduct(SparseVector<int> u, SparseVector<int> v) => OuterProduct(u, v, (x, y) => x * y);
        public static SparseMatrix<long> OuterProduct(SparseVector<long> u, SparseVector<long> v) => OuterProduct(u, v, (x, y) => x * y);
        public static SparseMatrix<float> OuterProduct(SparseVector<float> u, SparseVector<float> v) => OuterProduct(u, v, (x, y) => x * y);
        public static SparseMatrix<double> OuterProduct(SparseVector<double> u, SparseVector<double> v) => OuterProduct(u, v, (x, y) => x * y);
        public static SparseMatrix<decimal> OuterProduct(SparseVector<decimal> u, SparseVector<decimal> v) => OuterProduct(u, v, (x, y) => x * y);
        public static SparseMatrix<T> OuterProduct<T>(SparseVector<T> u, SparseVector<T> v) where T : Ring<T>, new() => OuterProduct(u, v, (x, y) => x.Multiply(y));
    }
}
